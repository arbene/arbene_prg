unit SevenPDFComObj_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 8291 $
// File generated on 03.06.2011 10:41:54 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\SevenPDFComObj\SevenPDFComObj.dll (1)
// LIBID: {346FF39A-5683-4244-ABF4-856A68F07D94}
// LCID: 0
// Helpfile: 
// HelpString: SevenPDFComObj Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SevenPDFComObjMajorVersion = 1;
  SevenPDFComObjMinorVersion = 0;

  LIBID_SevenPDFComObj: TGUID = '{346FF39A-5683-4244-ABF4-856A68F07D94}';

  IID_ISevenPDFObjConverter: TGUID = '{04809A35-7598-4170-AB6B-5CCF6FDACEA8}';
  CLASS_SevenPDFObjConverter: TGUID = '{2505FEC4-614F-4D3E-A9EC-CA2510323BA5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISevenPDFObjConverter = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SevenPDFObjConverter = ISevenPDFObjConverter;


// *********************************************************************//
// Interface: ISevenPDFObjConverter
// Flags:     (256) OleAutomation
// GUID:      {04809A35-7598-4170-AB6B-5CCF6FDACEA8}
// *********************************************************************//
  ISevenPDFObjConverter = interface(IUnknown)
    ['{04809A35-7598-4170-AB6B-5CCF6FDACEA8}']
    function convertToPdf(inputFilepath: PWideChar; outputFilePath: PWideChar; 
                          overwriteIfExists: Integer): SYSINT; stdcall;
    function setUseTaggedPDF(value: SYSINT): HResult; stdcall;
    function setExportNotes(value: SYSINT): HResult; stdcall;
    function setExportNotesPages(value: SYSINT): HResult; stdcall;
    function setExportBookmarks(value: SYSINT): HResult; stdcall;
    function setSelectPDFVersion(value: SYSINT): HResult; stdcall;
    function setPageRange(value: PWideChar): HResult; stdcall;
    function setUseLosslessCompression(value: SYSINT): HResult; stdcall;
    function setQuality(value: SYSINT): HResult; stdcall;
    function setReduceImageResolution(value: SYSINT): HResult; stdcall;
    function setMaxImageResolution(value: SYSINT): HResult; stdcall;
    function setEncryptFile(value: SYSINT): HResult; stdcall;
    function setDocumentOpenPassword(value: PWideChar): HResult; stdcall;
    function setRestrictPermissions(value: SYSINT): HResult; stdcall;
    function setPrinting(value: SYSINT): HResult; stdcall;
    function setChanges(value: SYSINT): HResult; stdcall;
    function setEnableCopyingOfContent(value: SYSINT): HResult; stdcall;
    function setEnableTextAccessForAccessibilityTools(value: SYSINT): HResult; stdcall;
    function setPermissionPassword(value: PWideChar): HResult; stdcall;
    function Init: HResult; stdcall;
    function UnlockKey(value: PWideChar): HResult; stdcall;
    function setDefaultSettings: HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoSevenPDFObjConverter provides a Create and CreateRemote method to          
// create instances of the default interface ISevenPDFObjConverter exposed by              
// the CoClass SevenPDFObjConverter. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSevenPDFObjConverter = class
    class function Create: ISevenPDFObjConverter;
    class function CreateRemote(const MachineName: string): ISevenPDFObjConverter;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TSevenPDFObjConverter
// Help String      : SevenPDFObjConverter
// Default Interface: ISevenPDFObjConverter
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TSevenPDFObjConverterProperties= class;
{$ENDIF}
  TSevenPDFObjConverter = class(TOleServer)
  private
    FIntf: ISevenPDFObjConverter;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TSevenPDFObjConverterProperties;
    function GetServerProperties: TSevenPDFObjConverterProperties;
{$ENDIF}
    function GetDefaultInterface: ISevenPDFObjConverter;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ISevenPDFObjConverter);
    procedure Disconnect; override;
    function convertToPdf(inputFilepath: PWideChar; outputFilePath: PWideChar; 
                          overwriteIfExists: Integer): SYSINT;
    function setUseTaggedPDF(value: SYSINT): HResult;
    function setExportNotes(value: SYSINT): HResult;
    function setExportNotesPages(value: SYSINT): HResult;
    function setExportBookmarks(value: SYSINT): HResult;
    function setSelectPDFVersion(value: SYSINT): HResult;
    function setPageRange(value: PWideChar): HResult;
    function setUseLosslessCompression(value: SYSINT): HResult;
    function setQuality(value: SYSINT): HResult;
    function setReduceImageResolution(value: SYSINT): HResult;
    function setMaxImageResolution(value: SYSINT): HResult;
    function setEncryptFile(value: SYSINT): HResult;
    function setDocumentOpenPassword(value: PWideChar): HResult;
    function setRestrictPermissions(value: SYSINT): HResult;
    function setPrinting(value: SYSINT): HResult;
    function setChanges(value: SYSINT): HResult;
    function setEnableCopyingOfContent(value: SYSINT): HResult;
    function setEnableTextAccessForAccessibilityTools(value: SYSINT): HResult;
    function setPermissionPassword(value: PWideChar): HResult;
    function Init: HResult;
    function UnlockKey(value: PWideChar): HResult;
    function setDefaultSettings: HResult;
    property DefaultInterface: ISevenPDFObjConverter read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TSevenPDFObjConverterProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TSevenPDFObjConverter
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TSevenPDFObjConverterProperties = class(TPersistent)
  private
    FServer:    TSevenPDFObjConverter;
    function    GetDefaultInterface: ISevenPDFObjConverter;
    constructor Create(AServer: TSevenPDFObjConverter);
  protected
  public
    property DefaultInterface: ISevenPDFObjConverter read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = '(none)';

  dtlOcxPage = '(none)';

implementation

uses ComObj;

class function CoSevenPDFObjConverter.Create: ISevenPDFObjConverter;
begin
  Result := CreateComObject(CLASS_SevenPDFObjConverter) as ISevenPDFObjConverter;
end;

class function CoSevenPDFObjConverter.CreateRemote(const MachineName: string): ISevenPDFObjConverter;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SevenPDFObjConverter) as ISevenPDFObjConverter;
end;

procedure TSevenPDFObjConverter.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{2505FEC4-614F-4D3E-A9EC-CA2510323BA5}';
    IntfIID:   '{04809A35-7598-4170-AB6B-5CCF6FDACEA8}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TSevenPDFObjConverter.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ISevenPDFObjConverter;
  end;
end;

procedure TSevenPDFObjConverter.ConnectTo(svrIntf: ISevenPDFObjConverter);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TSevenPDFObjConverter.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TSevenPDFObjConverter.GetDefaultInterface: ISevenPDFObjConverter;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TSevenPDFObjConverter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TSevenPDFObjConverterProperties.Create(Self);
{$ENDIF}
end;

destructor TSevenPDFObjConverter.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TSevenPDFObjConverter.GetServerProperties: TSevenPDFObjConverterProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TSevenPDFObjConverter.convertToPdf(inputFilepath: PWideChar; outputFilePath: PWideChar; 
                                            overwriteIfExists: Integer): SYSINT;
begin
  Result := DefaultInterface.convertToPdf(inputFilepath, outputFilePath, overwriteIfExists);
end;

function TSevenPDFObjConverter.setUseTaggedPDF(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setUseTaggedPDF(value);
end;

function TSevenPDFObjConverter.setExportNotes(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setExportNotes(value);
end;

function TSevenPDFObjConverter.setExportNotesPages(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setExportNotesPages(value);
end;

function TSevenPDFObjConverter.setExportBookmarks(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setExportBookmarks(value);
end;

function TSevenPDFObjConverter.setSelectPDFVersion(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setSelectPDFVersion(value);
end;

function TSevenPDFObjConverter.setPageRange(value: PWideChar): HResult;
begin
  Result := DefaultInterface.setPageRange(value);
end;

function TSevenPDFObjConverter.setUseLosslessCompression(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setUseLosslessCompression(value);
end;

function TSevenPDFObjConverter.setQuality(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setQuality(value);
end;

function TSevenPDFObjConverter.setReduceImageResolution(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setReduceImageResolution(value);
end;

function TSevenPDFObjConverter.setMaxImageResolution(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setMaxImageResolution(value);
end;

function TSevenPDFObjConverter.setEncryptFile(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setEncryptFile(value);
end;

function TSevenPDFObjConverter.setDocumentOpenPassword(value: PWideChar): HResult;
begin
  Result := DefaultInterface.setDocumentOpenPassword(value);
end;

function TSevenPDFObjConverter.setRestrictPermissions(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setRestrictPermissions(value);
end;

function TSevenPDFObjConverter.setPrinting(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setPrinting(value);
end;

function TSevenPDFObjConverter.setChanges(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setChanges(value);
end;

function TSevenPDFObjConverter.setEnableCopyingOfContent(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setEnableCopyingOfContent(value);
end;

function TSevenPDFObjConverter.setEnableTextAccessForAccessibilityTools(value: SYSINT): HResult;
begin
  Result := DefaultInterface.setEnableTextAccessForAccessibilityTools(value);
end;

function TSevenPDFObjConverter.setPermissionPassword(value: PWideChar): HResult;
begin
  Result := DefaultInterface.setPermissionPassword(value);
end;

function TSevenPDFObjConverter.Init: HResult;
begin
  Result := DefaultInterface.Init;
end;

function TSevenPDFObjConverter.UnlockKey(value: PWideChar): HResult;
begin
  Result := DefaultInterface.UnlockKey(value);
end;

function TSevenPDFObjConverter.setDefaultSettings: HResult;
begin
  Result := DefaultInterface.setDefaultSettings;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TSevenPDFObjConverterProperties.Create(AServer: TSevenPDFObjConverter);
begin
  inherited Create;
  FServer := AServer;
end;

function TSevenPDFObjConverterProperties.GetDefaultInterface: ISevenPDFObjConverter;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TSevenPDFObjConverter]);
end;

end.
