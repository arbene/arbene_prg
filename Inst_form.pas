unit Inst_form;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ExtCtrls, Buttons,shellapi,FileCtrl,registry,GIPlugins, CUIApi;


type
	TForm_main = class(TForm)
	  Panel2: TPanel;
	  BitBtn_weiter: TBitBtn;
	  BitBtn_cancel: TBitBtn;
	  Notebook: TNotebook;
	  CheckBox_mysql_start: TCheckBox;
	  Memo2: TMemo;
	  Memo_log: TMemo;
    Memo1: TMemo;
    CheckBox1: TCheckBox;
	  procedure FormCreate(Sender: TObject);
	  procedure BitBtn_weiterClick(Sender: TObject);
	  procedure BitBtn_cancelClick(Sender: TObject);
	private
    procedure mysql_ini;

	  { Private-Deklarationen }
	public
	  { Public-Deklarationen }
	  Betriebssystem:integer;
	  command:string;
	end;

var
	Form_main: TForm_main;

   Engine: TPluginStartupInfo;
   trigger: boolean;


implementation
{$R *.DFM}


procedure TForm_main.FormCreate(Sender: TObject);
var
	versionsinfo: OSVERSIONINFO;
begin
	Top:=(screen.height div 2)-(Height div 2);
	Left:= (screen.width div 2) - (width div 2);
	notebook.PageIndex:=0;
    beep;
    beep;
	versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;
	if betriebssystem=VER_PLATFORM_WIN32_WINDOWS then
	begin
		command:='command.com' ;
		checkbox_mysql_start.Caption:=
		'Automatisches Starten von MariaDB beim Systemstart in die Registry eintragen ';
	end;
	if betriebssystem=VER_PLATFORM_WIN32_NT	 then
	begin
		command:='cmd.exe' ;
		checkbox_mysql_start.Caption:=
		'MariaDB wird als Dienst beim Systemstart gestartet.';
	end;
end;

procedure TForm_main.mysql_ini;
var
	cmd_path,befehl,parameter,pfad,ip1, ip2, ip3:string;
	reg: Tregistry;
   t:ttime;
begin
      application.ProcessMessages;
     application.BringToFront;
     application.ProcessMessages;
      befehl:='net.exe';
       parameter:='stop mysql';

        shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_hide);
        memo_log.Lines.Add('Der Dienst Mysql wird angehalten');
        application.ProcessMessages;
        t:=time();
        while t+0.0001 >time() do;
        memo_log.Lines.Add('Der Dienst Mysql wurde angehalten');

    if checkbox1.Checked then
    begin
       if fileexists('c:\mysql\bin\mysqld-nt.exe') then
       begin
        befehl:='c:\mysql\bin\mysqld-nt.exe';

        parameter:='--remove';
        shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_hide);
        memo_log.Lines.Add('Der Dienst Mysql wurde entfernt');
        application.ProcessMessages;
         t:=time();
        while t+0.0001 >time() do;
      end;
    end;
    
   if  fileexists('c:\my.cnf') then
	begin
   	memo_log.Lines.Add('die Datei C:\my.cnf wird umbenannt in c:\old_my.cnf ');
   	RenameFile('c:\my.cnf','c:\old_my.cnf');
   	deletefile('c:\my.cnf');
	end;


    ip1:='"c:\mariadb55\bin\mysqld.exe"';
    ip2:='"c:\mariadb55\data\my.ini"';
 if betriebssystem=VER_PLATFORM_WIN32_WINDOWS then
	  begin
		 befehl:=ip1;
		 parameter:=format('--install MySQL --defaults-file=%s',[ip2]);
		  shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

		if checkbox_mysql_start.Checked then
		try
		 reg := Tregistry.create;
		 reg.RootKey:=hkey_local_machine;
		 reg.openkey('software\microsoft\windows\currentversion\run',true);
		 reg.writestring('mysql', 'c:\mariadb55\bin\mysqld.exe');

		 memo_log.lines.Add('Der Start von MarisDB bei dem Systemstart wurde in die Registry eingetragen (software\microsoft\windows\currentversion\run\mysql) ');
		 memo_log.lines.Add('');
		finally
		 reg.Free;
		end;

	  end;

	  if betriebssystem=VER_PLATFORM_WIN32_NT then
	  begin
		 befehl:=ip1;//'c:\mysql Server 5.6\bin\mysqld.exe';   //nt
       parameter:=format('--install MySQL --defaults-file=%s',[ip2]);  //C:\mariadb55\bin\mysqld.exe --defaults-file=c:\mariadb55\data\my.ini MySQL;
       shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);
       //memo_log.Lines.Add(befehl);
       //memo_log.Lines.Add(parameter);
		 memo_log.lines.Add('Der Start von MariaDB beim Systemstart wurde als Dienst eingetragen');

	  end;


	  memo_log.lines.Add('Die Einrichtung ist beendet');
	  memo_log.lines.Add('');
	  memo_log.lines.Add('Bitte starten Sie ARBENE aus dem Ordner "Programme".');

	  memo_log.lines.Add('');
	  memo_log.lines.Add('Name/Passwort: arzt/aaaaa ');
    memo_log.lines.Add('');
    memo_log.lines.Add('Hilfe erhalten Sie mit der Funktionstaste F1');
end;

procedure TForm_main.BitBtn_weiterClick(Sender: TObject);
var
	cmd_path,befehl,parameter,pfad:string;
	reg: Tregistry;
begin
case notebook.PageIndex of

0:
	begin
   bitbtn_weiter.Caption:='Ich habe die Lizenz von MariaDB gelesen und bin damit einverstanden';
   notebook.PageIndex:=1;
	end;
1:
	begin
    notebook.PageIndex:=2;
    mysql_ini;
    bitbtn_weiter.Caption:='Beenden';
	 //notebook.PageIndex:=2;
	end;
  
2: begin
    BitBtn_cancelClick(Sender);
   end;
end;
end;

procedure TForm_main.BitBtn_cancelClick(Sender: TObject);
begin
form_main.Visible:=false;
end;

function SendMsg(Msg, InParams, OutParams: longint): longint; stdcall;
begin
  Result:=Engine.SendMsg(Engine.HPlugin,Msg,InParams,OutParams,false,false);
end;



{function EventsHandler(Msg: Longint; MsgName: PChar;
	Param1, Param2: Longint): Longint; stdcall;
begin
	Result:=0;
	case Msg of
	  //EV_SETSTARTUPINFO:
      ev_start:
		 begin
			Engine:=PPluginStartupInfo(Param1)^;
			Application.CreateForm(TForm_main,Form_main);
		 end;

	  EV_afterinstall:
		 begin
		 Form_main.Show;
		 end;

	  EV_UNLOAD:
		// Form_main.Close;
	end;

end;  }

end.
