unit a_timer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ExtCtrls, Mask,DateUtils;

type
  TForm_timer = class(TForm)
    Timer1: TTimer;
    Label2: TLabel;
    Label5: TLabel;
    SpeedButton_1: TSpeedButton;
    BitBtn_1: TBitBtn;
    Edit_start_1: TEdit;
    Edit_dauer_1: TEdit;
    Label1: TLabel;
    SpeedButton_2: TSpeedButton;
    BitBtn_2: TBitBtn;
    Edit_start_2: TEdit;
    Edit_dauer_2: TEdit;
    Label3: TLabel;
    SpeedButton_3: TSpeedButton;
    BitBtn_3: TBitBtn;
    Edit_start_3: TEdit;
    Edit_dauer_3: TEdit;
    Label4: TLabel;
    Timer3: TTimer;
    Timer2: TTimer;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton_1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedButton_1DblClick(Sender: TObject);
    procedure BitBtn_1Click(Sender: TObject);
    procedure SpeedButton_2Click(Sender: TObject);
    procedure SpeedButton_3Click(Sender: TObject);
    procedure BitBtn_2Click(Sender: TObject);
    procedure BitBtn_3Click(Sender: TObject);
  private
    { Private-Deklarationen }
    modus:integer;
    me: array [1..6]  of tedit;
    sek:array [1..3] of longint;
    t_start: array[1..3] of ttime;
    t_pause: array[1..3] of longint;
    procedure aktiv(timer_nr:integer;aktiv:boolean);
    procedure anzeigen(timer_nr:integer);
    procedure ruecksetzen(t_nr:integer);
    procedure auswahl(t_nr:integer);

  public
    { Public-Deklarationen }
     stunden,minuten:integer;
    t_beginn:ttime;
    procedure art(m:string);

  end;

var
  Form_timer: TForm_timer;

implementation

uses a_main;

{$R *.dfm}

procedure TForm_timer.FormCreate(Sender: TObject);
begin
  form_main.form_positionieren(tform(sender));
  me[1]:=edit_start_1;
  me[2]:=edit_dauer_1;
  me[3]:=edit_start_2;
  me[4]:=edit_dauer_2;
  me[5]:=edit_start_3;
  me[6]:=edit_dauer_3;
end;

procedure TForm_timer.SpeedButton_1Click(Sender: TObject);
begin
  timer1.Enabled:=not timer1.Enabled;
  aktiv(1,timer1.enabled);
end;

procedure TForm_timer.aktiv(timer_nr:integer;aktiv:boolean);
var
i:integer;
begin
  for i:=1 to 2 do
  begin
    if aktiv then me[i+timer_nr*2-2].Color:=cl_entsperrt else me[i+timer_nr*2-2].color:=cl_gesperrt;
  end;
  if aktiv then t_start[timer_nr]:=now();
  anzeigen(timer_nr);
end;

procedure TForm_timer.Timer1Timer(Sender: TObject);
var
t:integer;
begin
t:=tmaskedit(sender).tag;
inc(sek[t]);
anzeigen(t);
end;

procedure TForm_timer.anzeigen(timer_nr:integer);
var
i,j:integer;
t1,t2:ttime;
begin
  j:=1+timer_nr*2-2;
  me[j].Text:=formatdatetime('hh:mm:ss',t_start[timer_nr]);
  me[j+1].text:=formatdatetime('hh:mm:ss',IncSecond(strtotime('00:00:00'),sek[timer_nr])	);
              //formatdatetime(
end;

procedure TForm_timer.SpeedButton_1DblClick(Sender: TObject);
begin
  t_start[1]:=0;
  sek[1]:=0;
end;


procedure TForm_timer.ruecksetzen(t_nr:integer);
begin

  t_start[t_nr]:=0;
  sek[t_nr]:=0;
  anzeigen(t_nr);
end;

procedure TForm_timer.auswahl(t_nr:integer);
var
s:longint;
begin

  t_beginn:=t_start[t_nr];

  s:=round(sek[t_nr]/60);
  stunden:=s div 60;
  minuten:=s mod 60;
  modalresult:=mrok;
end;

procedure TForm_timer.BitBtn_1Click(Sender: TObject);
begin
if modus=1 then auswahl(1) else ruecksetzen(1) ;

end;

procedure TForm_timer.SpeedButton_2Click(Sender: TObject);
begin
  timer2.Enabled:=not timer2.Enabled;
  aktiv(2,timer2.enabled);
end;

procedure TForm_timer.SpeedButton_3Click(Sender: TObject);
begin
  timer3.Enabled:=not timer3.Enabled;
  aktiv(3,timer3.enabled);
end;

procedure TForm_timer.BitBtn_2Click(Sender: TObject);
begin
  if modus=1 then auswahl(2) else ruecksetzen(2) ;;
end;

procedure TForm_timer.BitBtn_3Click(Sender: TObject);
begin
  if modus=1 then auswahl(3) else ruecksetzen(3) ;
end;

procedure tform_timer.art(m:string);
var
anzeige:string;
begin
  if m='auswahl' then
  begin
    bitbtn_1.Caption:='Einfügen';
    bitbtn_2.Caption:='Einfügen';
    bitbtn_3.Caption:='Einfügen';
    modus:=1;
  end
  else
  begin
    bitbtn_1.Caption:='Zurücksetzen';
    bitbtn_2.Caption:='Zurücksetzen';
    bitbtn_3.Caption:='Zurücksetzen';
    modus:=2;
  end;

end;

end.
