�
 TFORM_UEB_ARBUNFALL 0X  TPF0Tform_ueb_arbunfallform_ueb_arbunfallLeftTop� Width�Height�HelpContext|.Caption   Übersicht ArbeitsunfälleColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TSpeedButtonSpeedButton1LeftpTop$WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  TSpeedButtonSpeedButton2Left� Top$WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton2Click  TLabelLabel1Left(TopWidth4HeightCaption	Datum von  TLabelLabel2Left� TopWidth/HeightCaption	Datum bis  TLabelLabel4Left%TopHWidthEHeightCaptionSortieren nach  TLabelLabel5Left%TopxWidth� HeightCaption"   Hierarchieebene für  Körperteile  TLabelLabel3Left%Top� Width� HeightCaption%   Hierarchieebene für Verletzungsarten  	TComboBoxComboBox_sortierenLeft%TopXWidth� Height
ItemHeightTabOrderTextComboBox_sortierenItems.Strings   KörperteilVerletzungsart   	TMaskEditMaskEdit_vonLeft&Top%WidthAHeightEditMask!90.90.0000;1;_	MaxLength
TabOrder Text
  .  .      	TMaskEditMaskEdit_bisLeft� Top%WidthAHeightEditMask!90.90.0000;1;_	MaxLength
TabOrderText
  .  .      	TComboBoxComboBox_koerperteilLeft(Top� WidthqHeight
ItemHeightTabOrderItems.StringsStufe 0Stufe 1Stufe 2Stufe 3Stufe 4Stufe 5   	TCheckBoxCheckBox_alleLeft>Top� Width� HeightCaption   einzelne Unfälle auflistenTabOrder  	TCheckBoxCheckBox_anonymLeft>Top� Width`HeightCaptionanonymisiertTabOrder  	TComboBoxComboBox_verletzungsartLeft&Top� WidthsHeight
ItemHeightTabOrderItems.StringsStufe 0Stufe 1Stufe 2Stufe 3Stufe 4Stufe 5   TBitBtnBitBtn_uebersicht_excelLeftTop�Width� HeightCaption!   Erstellen der Übersicht in ExcelTabOrderOnClickBitBtn_uebersicht_excelClick
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� �(((((((��������(������(�����((�(򂂂�(��((/(/�(��(��(/((/�(����(��/(((��(�򂂂�(��((�(/�(����(��������((((((((��������  TMemoMemoLeft(Top� Width�Height� ReadOnly	
ScrollBars
ssVerticalTabOrderWantTabs	  TBitBtnBitBtn_uebersicht_memoLeft(Top�Width� HeightCaption   Erstellen der ÜbersichtTabOrder	OnClickBitBtn_uebersicht_excelClick  TBitBtnBitBtn2Left(Top�Width� HeightCaptionAbbruchTabOrder
OnClickBitBtn2ClickKindbkCancel  TProgressBarProgressBarLeft� Top�WidthQHeightStepTabOrder  TBitBtnBitBtn_graphikLeft�Top�Width� HeightCaption   Graphik mit KörperteilenTabOrderVisibleOnClickBitBtn_uebersicht_excelClick   