�
 TFORM_KARTEI_DRUCK 0�  TPF0TForm_kartei_druckForm_kartei_druckLeft�Top� WidthHeight�HelpContext)Caption<Bescheinigungen erstellen / drucken  / verschicken per emailColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TSpeedButtonSpeedButton_2Left� TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_2Click  TSpeedButtonSpeedButton_1Left,TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_2Click  TLabelLabel1Left� Top%WidthHeightCaptionbis  TLabelLabel3Left!Top%WidthHeightCaptionvon  TLabelLabel_firmaLeftLTopHWidthHeight  TLabelLabel2LeftTopHWidthHeightCaption-Die folgenden Bescheinigungen werden erstelltFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4LeftTopWidth6HeightCaption	Zeitraum:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel6LeftTop� WidthiHeightCaptionOptionen ErstellenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel7Left:Top� Width7HeightCaption>   (Die Bescheinigungen für den Probanden werden immer gedruckt)  TLabelLabel8LeftTopSWidthiHeightCaptionOptionen AusgabeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel9LeftTopWidthqHeightCaptionOptionen SpeichernFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TSpeedButtonSpeedButton_3Left�Top WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_2Click  TLabelLabel5Left�TopWidthFHeightCaptionErstelldatumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TPanelPanel2Left TopcWidthHeight(AlignalBottom
BevelOuterbvNoneTabOrder TBitBtnBitBtn_druckLeftnTopWidth� HeightCaption"Erstelllen / Drucken / VerschickenDefault	TabOrder OnClickBitBtn_druckClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      7wwwwwws�������3333337��������������        wwwwwwww�������333333��������3333377��������������        wwwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�3s3330�� 33337��w33330  33337wws333	NumGlyphs  TBitBtnBitBtn2LeftMTopWidth� HeightCaption	abbrechenTabOrderOnClickBitBtn2ClickKindbkAbort   	TMaskEditMaskEdit_datLeft6Top!WidthBHeightEditMask!99/99/9999;1;_	MaxLength
TabOrder Text
  .  .      	TMaskEditMaskEdit_dat_bisLeft� Top!WidthBHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  .  .      	TCheckBoxCheckBox_closeLeft%Top�Width0HeightCaption%   Fenster nach dem Erstellen schließenChecked	State	cbCheckedTabOrder
  	TCheckBoxCheckBox_vo_proLeft%TopbWidth� HeightCaptionVorsorge ProbandChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontState	cbCheckedTabOrder  	TCheckBoxCheckBox_vo_fiLeft%TopyWidth� HeightCaptionVorsorge ArbeitgeberChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontState	cbCheckedTabOrder  	TCheckBoxCheckBox_ei_proLeft� TopaWidth{HeightCaptionEignung ProbandChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontState	cbCheckedTabOrder  	TCheckBoxCheckBox_ei_fiLeft� Top{Width� HeightCaptionEignung ArbeitgeberChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontState	cbCheckedTabOrder  	TCheckBoxCheckBox_n_dLeft%Top� Width�HeightCaptionENur die Bescheinigungen drucken, die bisher noch nicht gedruckt sind.TabOrder	  	TCheckBoxCheckBox_nicht_sdruckLeft&Top� Width	HeightHint9   Schließen Sie das Wordfenster nach dem Druck mit STRG+F4Caption&Bescheinigung vor dem Drucken anzeigenParentShowHintShowHint	TabOrder  	TCheckBox"CheckBox_bescheinigung_druck_immerLeft%Top� WidthHeightCaptione   Bescheinigungen für den Arbeitgeber auch dann drucken, wenn keine Erlaubnis zur Weitergabe vorliegt.TabOrderOnClick'CheckBox_bescheinigung_druck_immerClick  	TCheckBoxCheckBox_druckenLeft%TopnWidthbHeightCaption"Bescheinigungen werden ausgedrucktChecked	State	cbCheckedTabOrder  	TCheckBoxCheckBox_speichern_dateiLeft%Top:Width� HeightCaption%Speichern der Bescheinigung als DateiTabOrder  	TCheckBoxCheckBox_speichern_karteibaumLeft%Top%Width�HeightCaption)Speichern der Bescheinigung im KarteibaumTabOrder  	TCheckBoxCheckBox_emailLeft%Top�Width�HeightCaptionZ   Bescheinigungen werden als  Email-Anhang verschickt (über das ausgewählte Mail-Programm)TabOrder  TButtonButton_nur_probandLeft�Top]WidthbHeightCaptionnur ProbandTabOrderOnClickButton_nur_probandClick  TButtonButton_nur_firmaLeft�TopwWidthbHeightCaptionnur ArbeitgeberTabOrderOnClickButton_nur_firmaClick  	TMaskEditMaskEdit_erstelldatumLeft�Top"WidthBHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  .  .       