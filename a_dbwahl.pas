unit a_dbwahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  Tform_dbwahl = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn_ok: TBitBtn;
    Edit_db: TEdit;
    ComboBox_host: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ComboBox_dbversion: TComboBox;
    CheckBox: TCheckBox;
    Label4: TLabel;
    Edit_passwort: TEdit;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  form_dbwahl: Tform_dbwahl;

implementation

uses a_main;

{$R *.DFM}

procedure Tform_dbwahl.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
end;

end.
