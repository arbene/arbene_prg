object Form_weiter: TForm_weiter
  Left = 690
  Top = 353
  Width = 480
  Height = 265
  BorderIcons = []
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 176
    Width = 193
    Height = 13
    Caption = 'Nach 60 sek. geht es automatisch weiter'
  end
  object Panel1: TPanel
    Left = 0
    Top = 195
    Width = 464
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object BitBtn_ok: TBitBtn
      Left = 27
      Top = 3
      Width = 199
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn_esc: TBitBtn
      Left = 254
      Top = 3
      Width = 199
      Height = 25
      Caption = 'Abbruch'
      ModalResult = 3
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object Memo: TMemo
    Left = 24
    Top = 24
    Width = 409
    Height = 121
    Color = clMenuBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      'Memo')
    ParentFont = False
    TabOrder = 1
  end
  object ProgressBar: TProgressBar
    Left = 24
    Top = 160
    Width = 409
    Height = 17
    Min = 1
    Max = 60
    Position = 1
    TabOrder = 2
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 432
    Top = 8
  end
end
