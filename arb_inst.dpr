library arb_inst;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  GIPlugins,
  CUIApi,
  dialogs,
  forms,
  Inst_form in 'Inst_form.pas' {Form_main},
  arb_inst_komplett_first in 'arb_inst_komplett_first.pas' {Form_komplett_first};

type dword=longword;
var
  Engine: TPluginStartupInfo;
  trigger: boolean;

function EventsHandler(Msg: dword; MsgName: PChar;
  InParam, OutParam: dword): dword; stdcall;
var a:DWORD;
    b: string;
begin
//beep;

    a:=Engine.CreateParamsBlock;
    Engine.AddParamStr(a,'arb_inst');
      if (MsgName=SEV_CUIAFTERCREATEWINDOW) and (not trigger) then begin
        trigger:=true;
        Engine.SendMsgName(Engine.HPlugin,SCM_CUISHOWWINDOW,a,0,false,false);
    end;
    if trigger then begin
        Engine.SetParamCount(a,1);
        //b:='SetProp("Sample.List.Items[1000].Value","'+MsgName+'")';
        //Engine.SetParamStr(a,0,PChar(b));
        Engine.SendMsgName(Engine.HPlugin,SCM_CUIDOACTION,a,0,false,false);
    end;

  if MsgName='EV_CALCFUNCTION' then
    if Engine.GetParamStr(InParam,1)='MyFunction' then
    begin
      if Engine.GetParamStr(InParam,4)='Close' then
      begin
        a:=Engine.CreateParamsBlock();
        Engine.AddParamStr(a,'Sample');
        Engine.SendMsgName(Engine.HPlugin,'CM_CUICLOSEWINDOW',a,0,false,false);
        Engine.DestroyParamsBlock(a);
      end;
      if Engine.GetParamStr(InParam,4)='Abort' then
        Engine.SendMsgName(Engine.HPlugin,'CM_ABORTINSTALL',0,0,false,false);
    end;

  Case Msg of
     EV_BEFOREINSTALL :
      begin
        Application.CreateForm(TForm_komplett_first, Form_komplett_first);
        Form_komplett_first.ShowMODAL;
        //form_komplett_first.Release;
      end;
    EV_LOAD:begin
        trigger:=false;
        a:=Engine.CreateParamsBlock();
        Engine.AddParamStr(a,'MyFunction');
        Engine.AddParamInt(a,-1);
        Engine.SendMsg(Engine.HPlugin,CM_REGISTERFUNCTION,a,0,false,false);
        Engine.DestroyParamsBlock(a);
    end;
    EV_AFTERINSTALL:
    begin
      Application.CreateForm(TForm_main,Form_main);
      Form_main.Show;
    end;
    EV_START: begin
    end;
    EV_UNLOAD: begin
    end;
  end;
  Result:=0;
end;

procedure GetPluginInfo(var PluginInfo: TPluginInfo); stdcall;
const
  {CPluginDescription = 'Ghost Installer plug-in sample for Delphi.';
  CPluginID          = 'Sample';
  CPluginName        = 'Sample';
  CPluginAuthor      = 'Ethalone Solutions, Inc.';
  CPluginCopyright   = 'Copyright � 1999-2004 Ethalone Solutions, Inc. and its licensors. All rights reserved.';}
  CPluginDescription = 'abene_install';
	CPluginID          = 'arb_inst';
	CPluginName        = 'arbene_install';
	CPluginAuthor      = 'klawitter';
  CPluginCopyright   = 'Copyright � 2017 Arbene';
begin
 //beep;
 //beep;
  with PluginInfo do begin
    if InfoVersion >= VER_PLUGININFO_100 then begin
      RequiredGIVersion:=VER_GI_400;
      Flags:=0;
      Move(CPluginID,PluginID,length(CPluginID));
      Move(CPluginName,PluginName,length(CPluginName));
      PluginVersion:=$10000;
      Move(CPluginAuthor,PluginAuthor,length(CPluginAuthor));
      Move(CPluginDescription,PluginDescription,length(CPluginDescription));
      Move(CPluginCopyright,PluginCopyright,length(CPluginCopyright));
      ProcessMsg:=@EventsHandler;
    end;
  end;
end;

function SetStartupInfo (StartupInfo: PPluginStartupInfo):longint; stdcall;
begin
  Engine.InfoVersion:=(Startupinfo).InfoVersion;
  Engine.GIVersion:=(StartupInfo).GIVersion;
  Engine.HPlugin:=(StartupInfo).HPlugin;
  Engine.RegisterMsg:=(StartupInfo).RegisterMsg;
  Engine.SendMsg:=(StartupInfo).SendMsg;
  Engine.SendMsgName:=(StartupInfo).SendMsgName;
  Engine.SendPrivateMsg:=(StartupInfo).SendPrivateMsg;
  Engine.SendPrivateMsgName:=(StartupInfo).SendPrivateMsgName;
  Engine.CreateParamsBlock:=(StartupInfo).CreateParamsBlock;
  Engine.DestroyParamsBlock:=(StartupInfo).DestroyParamsBlock;
  Engine.GetParamCount:=(StartupInfo).GetParamCount;
  Engine.SetParamCount:=(StartupInfo).SetParamCount;
  Engine.GetParamType:=(StartupInfo).GetParamType;
  Engine.GetParamStr:=(StartupInfo).GetParamStr;
  Engine.GetParamInt:=(StartupInfo).GetParamInt;
  Engine.GetParamBool:=(StartupInfo).GetParamBool;
  Engine.SetParamStr:=(StartupInfo).SetParamStr;
  Engine.SetParamInt:=(StartupInfo).SetParamInt;
  Engine.SetParamBool:=(StartupInfo).SetParamBool;
  Engine.AddParamStr:=(StartupInfo).AddParamStr;
  Engine.AddParamInt:=(StartupInfo).AddParamInt;
  Engine.AddParamBool:=(StartupInfo).AddParamBool;
  Result:=0;
end;




exports
  GetPluginInfo,
  SetStartupInfo;
begin
end.
