unit a_repo_neu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Edit_waehrung, Mask,dbcombo_number,db,comctrls,
  Spin;

type
  TForm_repo_neu = class(TForm)
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    SpeedButton_besonderheiten: TSpeedButton;
    MaskEdit_dat: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edit_rete: TEdit;
    Label_preis: TLabel;
    Edit_preis: TEdit_waehrung;
    Label3: TLabel;
    Label4: TLabel;
    Edit_faktor: TEdit_waehrung;
    BitBtn_liste: TBitBtn;
    Edit_goae: TEdit;
    Label5: TLabel;
    Edit_goae_nn: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    BitBtn_goae: TBitBtn;
    Label8: TLabel;
    Edit_proband: TEdit;
    BitBtn_proband: TBitBtn;
    BitBtn_zuordnung_weg: TBitBtn;
    Label9: TLabel;
    ComboBox_ust: TComboBox;
    Label10: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure SpeedButton_besonderheitenClick(Sender: TObject);
    procedure BitBtn_listeClick(Sender: TObject);
    procedure BitBtn_goaeClick(Sender: TObject);
    procedure BitBtn_probandClick(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
    procedure BitBtn_zuordnung_wegClick(Sender: TObject);
  private
    { Private-Deklarationen }

  public
    { Public-Deklarationen }
    ma_nr:int64;
  end;

var
  Form_repo_neu: TForm_repo_neu;

implementation

uses  a_kalender, a_data, a_unt_wahl,a_main, a_tabellen, a_namsuch;

{$R *.DFM}

procedure TForm_repo_neu.FormCreate(Sender: TObject);
begin
ma_nr:=0;
combobox_ust.Items.Add('0');
combobox_ust.Items.Add(inttostr(mws_reduziert));
combobox_ust.Items.Add(inttostr(mws));

form_main.form_positionieren(tform(sender));
end;

procedure TForm_repo_neu.SpeedButton_besonderheitenClick(Sender: TObject);
begin
  form_kalender.showmodal;
	maskedit_dat.text :=datetostr(int(form_kalender.auswahlkalender.date));
end;



procedure TForm_repo_neu.BitBtn_listeClick(Sender: TObject);
var
		 i,bereich,i_typ:integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
         s_befund,s_typ,query:string;
         felder :array [0..2] of string;
         goae_res:t5stringarray;

begin
////////////////
with datamodul do
begin
try
// befunde
	form_untwahl:=tform_untwahl.Create(self);
	form_untwahl.tag:=1;
	form_untwahl.notebook.pageindex:=4;
	form_untwahl.TabSheet_2.TabVisible:=false;
	form_untwahl.notebook.enabled:=false;
	form_untwahl.multiselect:=false;
	form_untwahl.parent_markieren:=false;
	form_untwahl.ToolBar.Visible:=false;
	form_untwahl.Panel_botttom.Visible:=true;
    form_untwahl.DBRadioGroup_erscheinungsbild.visible:=false;
	form_untwahl.caption:='Untersuchungs-Befunde';
	//  form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.tree.mysqlquery:=datamodul.q_bereich;
	form_untwahl.tree.mysql_feld:='name';
	form_untwahl.tree.liste_lesen;



	if form_untwahl.showmodal<>mrok then exit;

	if form_untwahl.Tree.Selected=nil then exit;

	//s_typ:=q_2.findfield('nummer').asstring;

  knoten :=form_untwahl.Tree.Selected;
  daten:=knoten.Data;
  s_befund:=inttostr(daten^.nummer);



  query:=format('select name, goae,euro  from bereich where nummer=%s',[s_befund]);     // eurocent
  mysql_d.feldarray(query,[0,1,2],felder);
  edit_rete.Text:=felder[0];
  edit_goae.text:=felder[1];
  edit_preis.text:=floattostr(strtofloat(felder[2]) ); // /100

  if felder[1]<>'' then
  begin
     goae_res:=preis_von_goae(felder[1]);
     edit_preis.Text:=floattostr (betrag( strtofloat(goae_res[0])*g_punktwert));
     edit_goae_nn.Text:=goae_res[2];
     i_typ:=strtoint(goae_res[3]);
     case i_typ of
       0: edit_faktor.Text:=datamodul.q_firma.findfield('fa_unt').asstring;
     	1: edit_faktor.Text:=datamodul.q_firma.findfield('fa_impf').asstring;
     	2: edit_faktor.Text:=datamodul.q_firma.findfield('fa_lab').asstring;
    end;
  end
  else edit_faktor.Text:='1';


except
  showmessage('Die GO� '+felder[1]+ 'ist nicht in der GO��-Liste vorhanden');
end;

	form_untwahl.release;

end;
end;


procedure TForm_repo_neu.BitBtn_goaeClick(Sender: TObject);
var
		 i,bereich,punkte,goae_typ:integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
     s_befund,s_typ,query:string;
     felder :array [0..2] of string;
     goae_res:t5stringarray;
begin
////////////////
with datamodul do
begin
try
// befunde
	form_tabellen:=tform_tabellen.Create(self);
    //form_tabellen.HelpContext:=10840;
    
    form_tabellen.Caption:='GO�-Liste';
    form_tabellen.Notebook.PageIndex:=11;
    form_tabellen.helpcontext:=49;  //2010
    query:= 'select * from goae where storno=0 order by lPAD(left(goae_nummer,4),4,"0")';
    datamodul.sql(false,datamodul.q_1,query,'');

    form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='goae_nummer';
    form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='goae_text';
    form_tabellen.DBGrid_tabellen.Columns[0].width:=60;
    form_tabellen.DBEdit_goae_nr.DataField:='goae_nummer';
    form_tabellen.DBEdit_goae_text.DataField:='goae_text';
    form_tabellen.DBEdit_goae_nn.DataField:='goae_nn';
    form_tabellen.DBEdit_goae_punkte.DataField:='punkte';
    form_tabellen.DBRadioGroup_goae_kategorie.DataField:='goae_typ';

    form_tabellen.ToolBar.visible:=false;
    form_tabellen.Panel_b.visible:=true;
    form_tabellen.button_goae.visible:=false;
	if form_tabellen.showmodal<>mrok then exit;

	//if form_untwahl.Tree.Selected=nil then exit;

	//s_typ:=q_2.findfield('nummer').asstring;




  edit_rete.Text:=datamodul.q_1.findfield('goae_text').asstring;
  edit_goae.text:=datamodul.q_1.findfield('goae_nummer').asstring;
  punkte:=datamodul.q_1.findfield('punkte').asinteger;

  edit_preis.Text:=floattostr (betrag( punkte*g_punktwert));
  edit_goae_nn.Text:=datamodul.q_1.findfield('goae_nn').asstring;
  goae_typ:=datamodul.q_1.findfield('goae_typ').asinteger;

  edit_faktor.Text:='1';
  case goae_typ of
     0: edit_faktor.Text:=datamodul.q_firma.findfield('fa_unt').asstring;
     1: edit_faktor.Text:=datamodul.q_firma.findfield('fa_impf').asstring;
     2: edit_faktor.Text:=datamodul.q_firma.findfield('fa_lab').asstring;
     3: edit_faktor.Text:='1';
  end;
except
  showmessage('Fehler bei der GO�-Auswahl');
end;

	form_tabellen.release;

end;
end;

procedure TForm_repo_neu.BitBtn_probandClick(Sender: TObject);
var
n,query,i_ma:string;
i:int64;

begin
  //i_ma:=datamodul.q_repositionen.findfield('i_mitarbeiter').asstring;

  n:=ma_name(ma_nr,2);
	form_namsuch.Edit1.Text:=n;
	form_namsuch.CheckBox_email_f.Checked:=false;
	form_namsuch.CheckBox_email_p.Checked:=false;
	if form_namsuch.showmodal= mrok then
  begin
     ma_nr:=getbigint(datamodul.q_mitarbeiter_such,'nummer');
     edit_proband.text:=ma_name(ma_nr,3);
  end;
end;



procedure TForm_repo_neu.BitBtn_okClick(Sender: TObject);
begin
  if strtofloat(edit_preis.text)>10000000 then
  begin
     showmessage('Der Einzelpreis ist zu hoch');
     exit;

  end;
  modalresult:=mrok;
end;
procedure TForm_repo_neu.BitBtn_zuordnung_wegClick(Sender: TObject);
begin
   ma_nr:=0;
   edit_proband.text:='';
end;

end.
