unit a_namsuch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, dbgridEXT, StdCtrls, Buttons, ExtCtrls,a_soundex,a_data,db,
  DBCtrls, ComCtrls,Variants, Mask;

type
  TForm_namsuch = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBgridEXT1: TDBgridEXT;
    StatusBar: TStatusBar;
    Panel_top: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    BitBtn4: TBitBtn;
    CheckBox_email_f: TCheckBox;
    CheckBox_email_p: TCheckBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Edit_vorname: TEdit;
    Label3: TLabel;
    MaskEdit_datum: TMaskEdit;
    Label4: TLabel;
    Edit_name: TEdit;
    Button_zurueck: TButton;
    RadioGroup_f: TRadioGroup;
    procedure Edit1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DBgridEXT1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
	  procedure Button_soundexClick(Sender: TObject);
	  procedure FormShow(Sender: TObject);
    procedure ComboBox_firmaChange(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBgridEXT1KeyPress(Sender: TObject; var Key: Char);
    procedure DBgridEXT1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RadioGroup_fClick(Sender: TObject);
    procedure CheckBox_email_fClick(Sender: TObject);
    procedure Button_zurueckClick(Sender: TObject);
  private
    { Private-Deklarationen }
	  procedure soundexneu;
  public
	  { Public-Deklarationen }
    procedure filtern;

  end;

var
  Form_namsuch: TForm_namsuch;

implementation

uses a_main, a_termin_wahl;

{$R *.DFM}

procedure TForm_namsuch.Edit1Change(Sender: TObject);
var
	wort,nname, sname, vorname:string;
	position:integer;
	//name_vorname : variant;
	//anv:array [0..1] of string;
begin
	wort:=edit1.text;
	position:=pos(',',wort);
	if position >0 then
	begin
		nname:=copy(wort,1,position-1);
		vorname:=copy(wort,position+1,length(wort));
	end
	else
	begin
		nname:=wort;
		vorname:=''
	end;
	nname:=trim(nname);
	vorname:=trim(vorname);
	sname:=soundex.soundex(pchar(nname)) ;
	if radiogroup_f.ItemIndex=1 then
	  datamodul.q_mitarbeiter_such.locate('name,vorname',VarArrayOf([nname,vorname]),[loCaseInsensitive,loPartialKey])
	else
	  datamodul.q_mitarbeiter_such.locate('s_name,vorname',VarArrayOf([sname,vorname]),[loCaseInsensitive,loPartialKey ]);
	dbgridext1.Eingabetext:=edit1.text;  
end;


procedure TForm_namsuch.soundexneu;
var
	wort, s_name:string;
begin
  with datamodul do
	begin
		//q_mitarbeiter_such.IndexName:='';
     q_mitarbeiter_such.First;
     while not q_mitarbeiter_such.Eof do
     begin
        if q_mitarbeiter_such['name']<>null then wort:=q_mitarbeiter_such['name'];
			s_name:=soundex.soundex(wort);
			//if q_mitarbeiter_such['vorname']<>null then wort:=q_mitarbeiter_such['vorname'];
			//s_vorname:=soundex.soundex(wort);

        q_mitarbeiter_such.edit;
        q_mitarbeiter_such['s_name']:=s_name;
        //q_mitarbeiter_such['s_vorname']:=s_vorname;
        direktspeichern:=true;
        q_mitarbeiter_such.Next;
        application.ProcessMessages;
     end;
    q_mitarbeiter.refresh;
   end;

end;

procedure TForm_namsuch.Button1Click(Sender: TObject);
begin
	soundexneu;
end;

procedure TForm_namsuch.DBgridEXT1DblClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_namsuch.FormCreate(Sender: TObject);
begin
	grid_einstellen(dbgridext1);
	form_main.form_positionieren(tform(sender));
	filtern;
end;


procedure TForm_namsuch.FormActivate(Sender: TObject);
begin
	edit1.SetFocus;
end;


procedure TForm_namsuch.Button_soundexClick(Sender: TObject);
begin
	  soundexneu;
end;


procedure TForm_namsuch.FormShow(Sender: TObject);
begin
	//if b.b_string_silent('system')>-1 then form_namsuch.TabSheet2.TabVisible:=true else form_namsuch.TabSheet2.TabVisible:=false; ;
  //pagecontrol.ActivePage:=tabsheet1;
end;

procedure TForm_namsuch.ComboBox_firmaChange(Sender: TObject);
begin
 filtern;
end;

procedure tform_namsuch.filtern;
var
where,sqldat:string;
d:tdate;
begin

where:=format('where i_firma in (%s) and mitarbeiter.storno=0',[firmen_filter]);
if trim(edit_name.Text)<>'' then where :=where +format(' and name like %s',['"%'+trim(edit_name.text)+'%"']);
if trim(edit_vorname.Text)<>'' then where :=where +format(' and vorname like %s',['"%'+trim(edit_vorname.text)+'%"']);

if pos('_',maskedit_datum.EditText)=0 then
begin
try
d:=strtodate( maskedit_datum.EditText);
sqldat:=sql_datetostr(d);
where :=where +format(' and geb_dat=%s',[sqldat]);
except
  showmessage('Datum nicht korrekt');
end;
end;
if checkbox_email_f.Checked then where :=where +' and email_f<>""';
if checkbox_email_p.Checked then where :=where +' and email_p<>""';

try
 if radiogroup_f.ItemIndex=1 then
		datamodul.sql(false,datamodul.q_mitarbeiter_such,'select mitarbeiter.nummer,upper(name) as name,upper(vorname) as vorname ,i_firma, s_name, upper(name) as grname,geb_dat,archiv,firma.kuerzel,firma.firma,email_f,email_p from mitarbeiter left join firma on(firma.nummer=mitarbeiter.i_firma) '+where+' order by grname, vorname','')
	else
		datamodul.sql(false,datamodul.q_mitarbeiter_such,'select mitarbeiter.nummer,upper(name) as name,upper(vorname) as vorname,i_firma, s_name, upper(s_name) as grname,geb_dat,archiv,firma.kuerzel ,firma.firma,email_f,email_p from mitarbeiter left join firma on(firma.nummer=mitarbeiter.i_firma) '+where+' order by grname, vorname','');
except
  edit_name.Text:='';
  edit_vorname.Text:='';
  maskedit_datum.Text:='__.__.____'
end;
end;

procedure TForm_namsuch.BitBtn4Click(Sender: TObject);
begin
	//datamodul.q_mitarbeiter_such.Refresh;
  application.ProcessMessages;
  filtern;
end;

procedure TForm_namsuch.Edit1KeyDown(Sender: TObject; var Key: Word;
	Shift: TShiftState);
begin
case key of
	VK_up: begin
				datamodul.q_mitarbeiter_such.Prior ;
				edit1.Text:=datamodul.q_mitarbeiter_such['name']+', '+datamodul.q_mitarbeiter_such['vorname'];
			 end;
	vk_down: begin
					datamodul.q_mitarbeiter_such.Next;
				edit1.Text:=datamodul.q_mitarbeiter_such['name']+', '+datamodul.q_mitarbeiter_such['vorname'];
				end;
end;
end;

procedure TForm_namsuch.DBgridEXT1KeyPress(Sender: TObject; var Key: Char);
var ordkey: integer;
begin
ordkey:=ord(key);
case

  ordkey of
	13: modalresult:=mrok; //enter
  48..256 :
		begin
		edit1.text:=DBgridEXT1.eingabetext;

		dbgridext1.SetFocus;
     end;
end;

end;

procedure TForm_namsuch.DBgridEXT1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   		edit1.text:=DBgridEXT1.eingabetext;

end;

procedure TForm_namsuch.RadioGroup_fClick(Sender: TObject);
begin
  filtern;

end;

procedure TForm_namsuch.CheckBox_email_fClick(Sender: TObject);
begin
 filtern;
end;

procedure TForm_namsuch.Button_zurueckClick(Sender: TObject);
begin
  datamodul.q_mitarbeiter_such.Refresh;
  application.ProcessMessages;
  edit_name.text:='';
  edit_vorname.text:='';
  maskedit_datum.EditText:='__.__.____' ;
  filtern;

end;

end.
