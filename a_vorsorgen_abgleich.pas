unit a_vorsorgen_abgleich;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, Mask;

type
  TForm_vorsorge_abgleich = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit_ende: TMaskEdit;
    MaskEdit_beginn: TMaskEdit;
    Label3: TLabel;
    MaskEdit_plan: TMaskEdit;
    GroupBox2: TGroupBox;
    BitBtn1: TBitBtn;
    Splitter1: TSplitter;
    GroupBox3: TGroupBox;
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    CheckBox_eintragen: TCheckBox;
    CheckBox_storno: TCheckBox;
    Label4: TLabel;
    BitBtn_neu: TBitBtn;
    BitBtn_hinfaellig: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure BitBtn_hinfaelligClick(Sender: TObject);
    procedure BitBtn_neuClick(Sender: TObject);
  private
    { Private-Deklarationen }
    tab_ein, tab_aus:string;
    procedure probelauf(modus:integer);
    procedure probelauf_ap(i_ap:int64;abt_name,ap_name:string);
    procedure umsetzen;
    procedure ini;
  public
    { Public-Deklarationen }
    ebene:integer;
  end;

var
  Form_vorsorge_abgleich: TForm_vorsorge_abgleich;

implementation

uses a_main,a_data;

{$R *.dfm}

procedure TForm_vorsorge_abgleich.FormCreate(Sender: TObject);
begin
      Width:=form_main.Width;
     Height:=form_main.Height;
     form_main.form_positionieren(tform(sender));

     ini;
end;

procedure TForm_vorsorge_abgleich.BitBtn_okClick(Sender: TObject);
begin
try
  umsetzen;
  Probelauf(ebene);
finally
  //modalresult:=mrok;
end;
end;

procedure TForm_vorsorge_abgleich.BitBtn1Click(Sender: TObject);
begin
Probelauf(ebene);
bitbtn_neu.Enabled:=true;
bitbtn_hinfaellig.Enabled:=true;
end;

procedure TForm_vorsorge_abgleich.probelauf(modus:integer);
var
query:string;
i_abteilung, i_ap,i_firma:int64;
ap_name, ab_name:string;
begin
  query:=format('delete from %s',[tab_ein]);
  mysql_d.sql_exe(query);
  query:=format('delete from  %s',[tab_aus]);
  mysql_d.sql_exe(query);

  datamodul.q_14.Refresh;
  datamodul.q_15.Refresh;
  i_firma:=datamodul.q_firma.findfield('nummer').AsInteger;
  i_abteilung:=getbigint(datamodul.q_ap3,'nummer');
  i_ap:=getbigint(datamodul.q_ap4,'nummer');
  ab_name:=datamodul.q_ap3.findfield('kuerzel').asstring;
  ap_name:=datamodul.q_ap4.findfield('name').asstring;


  if modus= 2 then probelauf_ap(i_ap,ab_name,ap_name);

  if modus =1 then
  begin
    //alle arbeitspl�tze der Abteilung
    query:=format('select * from a_platz where i_abteilung=%s',[inttostr(i_abteilung)]);
    datamodul.sql_new(false,datamodul.q_20,query,'');
    datamodul.q_20.First;
    while not datamodul.q_20.eof do
    begin
       i_ap:=getbigint( datamodul.q_20,'nummer');
       ap_name:=datamodul.q_20.findfield('name').asstring;
       probelauf_ap(i_ap,ab_name,ap_name);
       datamodul.q_20.Next;
    end;
  end;

  if modus =-1 then
  begin
    //alle arbeitspl�tze aller  Abteilungen der Firma

     query:=format('select a_platz.nummer, a_platz.name , abteilung.kuerzel from a_platz left join abteilung on (a_platz.i_abteilung=abteilung.nummer) where abteilung.i_firma=%s',[inttostr(i_firma)]);
    datamodul.sql_new(false,datamodul.q_20,query,'');
    datamodul.q_20.First;
    while not datamodul.q_20.eof do
    begin
       i_ap:=getbigint( datamodul.q_20,'nummer');
       ap_name:=datamodul.q_20.findfield('name').asstring;
        ab_name:=datamodul.q_20.findfield('kuerzel').asstring;
       probelauf_ap(i_ap,ab_name,ap_name);
       datamodul.q_20.Next;
    end;
  end;

end;

procedure tform_vorsorge_abgleich.probelauf_ap(i_ap:int64;abt_name,ap_name:string);
var query:string;
i,j,i_typ:integer;
s_ma,d_an,d_en:string;
begin
with datamodul do
begin
  d_an:=sql_datetostr(strtodate(maskedit_beginn.EditText));
  d_en:=sql_datetostr(strtodate(maskedit_ende.EditText));

  query:='select *  from gefaehrdung_vorsorge  left join a_gefaehrdung on (a_gefaehrdung.i_gef3=gefaehrdung_vorsorge.i_gef3) left join a_gefaehrdungsfaktor on (a_gefaehrdungsfaktor.nummer=a_gefaehrdung.i_gefaehrdungsfaktor)';
  query:=query+ ' left join a_platz on (a_platz.nummer=a_gefaehrdungsfaktor.i_ap) left join abteilung on (abteilung.nummer=a_platz.i_abteilung) left join typ on (gefaehrdung_vorsorge.i_typ=typ.nummer)';
  query:= format('%s where  a_gefaehrdung.storno=0 and i_ap=%s ',[query, inttostr(i_ap)]) ;
  datamodul.sql_new(false,datamodul.q_16,query,'');  //alle vorsorgen f�r die ap
//probanden suchen
  query:= format('select * from mitarbeiter where i_arbeitsplatz=%s',[inttostr(i_ap)]);
  datamodul.sql_new(false,datamodul.q_17,query,'');  //alle vorsorgen f�r die ap

  //alle probanden checken und evtl neu vorsorge eintragen
  q_17.First;
  while not  q_17.eof do
  //repeat
  //if q_17.FindField('nummer')<>nil then
  begin
    s_ma:=q_17.findfield('nummer').asstring;

    //suche alle vorsorgenvom probanden ->negativliste
    query:='select untersuchung.*, typ.untersuchung as b_name from untersuchung left join typ on (untersuchung.i_typ=typ.nummer) ';
    query:=query+ format('where (untersuchung.i_mitarbeiter=%s and untersuchung.datum>=%s and untersuchung.datum<=%s and untersuchung.i_status=1 and untersuchung.storno=0)',[s_ma,d_an,d_en]);
    sql_new(false,q_18,query,'');
    q_18.first;
    while not q_18.Eof do
    //repeat
    //if q_18.FindField('nummer')<>nil then
    begin
      //(nummer INT(11) NOT NULL AUTO_INCREMENT primary key, abteilung char(70), ap char(70), name char(70), vorname char(70), geb_dat datetime,i_mitarbeiter bigint(19), bescheinigung char(70), i_typ int(11), plan_datum datetime ) ';
      q_15.AppendRecord([nil,abt_name,ap_name,q_17.FindField('name').asstring,q_17.FindField('vorname').asstring,q_17.FindField('geb_dat').asdatetime,q_17.FindField('nummer').asstring,q_18.FindField('b_name').asstring,q_18.FindField('i_typ').asinteger,
                        q_18.findfield('nummer').asstring, q_18.FindField('datum').asdatetime]);


      q_18.next;
    end;
    //until q_18.Eof;

    //positivliste  wenn in der negativliste dann dort l�schen, wenn nicht vorhanden dann neu anlegen
    q_16.First;
    while not q_16.Eof do
    //repeat
    //if q_16.FindField('nummer')<>nil then
    begin
       i_typ:=q_16.findfield('i_typ').asinteger;
       if q_15.locate('i_mitarbeiter,i_typ',vararrayof([s_ma,i_typ]),[])
       then
        q_15.Delete
       else
        q_14.AppendRecord([nil,abt_name,ap_name,q_17.FindField('name').asstring,q_17.FindField('vorname').asstring,q_17.FindField('geb_dat').asdatetime,q_17.FindField('nummer').asstring,q_16.FindField('untersuchung').asstring,q_16.FindField('i_typ').asinteger,
                         //q_16.FindField('i_paw').asinteger,date()]);
                         0,strtodate(maskedit_plan.Edittext)]);
    q_16.Next;
    end;
    //until q_16.Eof;

    q_17.Next;
  end;
  //until q_17.eof;

end;//with
end;



procedure tform_vorsorge_abgleich.umsetzen;
var
query:string;
s_typ,sql_dat,s_mitarbeiter,s_untersuchung,s_paw,n,v,gd,b,bd:string;
i_mitarbeiter: int64;
i_typ,i_paw:integer;
dat:tdatetime;
begin
with datamodul do
begin
  //storno
     q_15.First;
     while not q_15.eof do
     //repeat
     //if q_15.FindField('nummer')<>nil then
     begin

        s_typ:=q_15.findfield('i_typ').asstring;
        s_mitarbeiter:= q_15.findfield('i_mitarbeiter').asstring;
        s_untersuchung:= q_15.findfield('i_untersuchung').asstring;
        n:=q_15.findfield('name').asstring;
        v:=q_15.findfield('vorname').asstring;
        gd:=datetostr(q_15.findfield('geb_dat').asdatetime);
        b:=q_15.findfield('bescheinigung').asstring;
        bd:=datetostr(q_15.findfield('plan_datum').asdatetime);

        query:=format('update untersuchung set storno=1 where (nummer=%s ) ' ,[s_untersuchung]);

        if checkbox_storno.Checked then
        begin
          if (messagedlg(format ('Soll die geplante Vorsorge: %s  %s, %s %s; %s ( %s ) %s storniert werden?',[#13+#10,n,v,gd,b,bd,#13+#10]),mtConfirmation, mbOkCancel	,0)=mrOK ) then
            mysql_d.sql_exe(query);
        end;
         q_15.Next;
     end;

     //until q_15.eof;

  //neu
     q_14.First;
     while not q_14.eof do
     //repeat
     //if q_14.FindField('nummer')<>nil then
     begin
        i_typ:=q_14.findfield('i_typ').asinteger;
        query:=format('select i_paw, i_arbmedvv from typ where nummer=%d',[i_typ]);
        i_paw:=strtoint(mysql_d.Feldinhalt(query,0));
        //i_paw:=q_14.findfield('i_paw').asinteger;
        s_mitarbeiter:= q_14.findfield('i_mitarbeiter').asstring;
        dat:=strtodate(maskedit_plan.Edittext);
        query:=format('select * from untersuchung where i_mitarbeiter=%s',[s_mitarbeiter]);
        datamodul.sql_new(false,q_19,query,'');

        if checkbox_eintragen.checked then
        begin
         neuer_datensatz(q_19,['nummer','i_mitarbeiter','datum','i_typ','i_status','i_uart','i_paw'],[null,s_mitarbeiter,dat,i_typ,1,1,i_paw]);
          q_19.Post;
        end;
        q_14.Next;
     end;

     //until q_14.eof;


end;
end;


procedure TForm_vorsorge_abgleich.ini;
var query,s1:string;
begin
// temp dateien erstellen bzw l�schen
tab_ein:=format('temp_besch_ein_%d',[user_id]);
tab_aus:=format('temp_besch_aus_%d',[user_id]);
query:=format('drop table if exists %s',[tab_ein]);
  mysql_d.sql_exe(query);
  query:=format('drop table if exists %s',[tab_aus]);
  mysql_d.sql_exe(query);

s1:='(nummer INT(11) NOT NULL AUTO_INCREMENT primary key, Abteilung char(70), ap char(70), Name char(70), Vorname char(70), geb_dat datetime,i_mitarbeiter bigint(19), Bescheinigung char(70),  ';
s1:=s1+ ' i_typ int(11),i_untersuchung bigint(19), plan_datum datetime )';
query:=format('create table if not exists %s %s',[tab_ein,s1]);
mysql_d.sql_exe(query);
query:=format('select * from %s',[tab_ein]);
datamodul.sql_new(false,datamodul.q_14,query,'');

query:=format('create table if not exists %s %s',[tab_aus,s1]);
mysql_d.sql_exe(query);
query:=format('select * from %s',[tab_aus]);
datamodul.sql_new(false,datamodul.q_15,query,'');

maskedit_plan.EditText:=datetostr(now());
maskedit_ende.EditText:= datetostr(now()+1068);
maskedit_beginn.edittext:=datetostr(now()-1068);



end;

procedure TForm_vorsorge_abgleich.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
query:string;
begin
  query:=format('drop table %s',[tab_ein]);
  mysql_d.sql_exe(query);
  query:=format('drop table %s',[tab_aus]);
  mysql_d.sql_exe(query);
  datamodul.q_untersuchung.Refresh;
  
end;

procedure TForm_vorsorge_abgleich.FormShow(Sender: TObject);
begin
if ebene=-1 then caption:='Bescheinigung planen f�r alle Probanden der aktuellen Firma '+ datamodul.q_firma.findfield('firma').asstring;;
if ebene=1 then caption:='Bescheinigung planen f�r alle Probanden in der Abteilung: ' +datamodul.q_ap3.findfield('kuerzel').asstring ;
if ebene=2 then caption:='Bescheinigung planen f�r alle Probanden an dem Arbeitsplatz: '+ datamodul.q_ap4.findfield('name').asstring;;
end;

procedure TForm_vorsorge_abgleich.BitBtn_hinfaelligClick(Sender: TObject);
var
a:TIntSet ;
begin
   a:=[0,1,2,3,4,5,7,10];
  tab_exportieren(datamodul.q_15,a);
                          end;

procedure TForm_vorsorge_abgleich.BitBtn_neuClick(Sender: TObject);
var
a:TIntSet ;
begin
    a:=[0,1,2,3,4,5,7,10];
    tab_exportieren(datamodul.q_14,a);

end;

end.
