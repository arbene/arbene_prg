unit myback_main;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, Mask, HCMngr, ComCtrls, Menus,registry, ExtCtrls,zip,
	Spin,shellapi,FileCtrl;

type
  TForm_main = class(TForm)
    StatusBar: TStatusBar;
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    N1: TMenuItem;
    Beenden1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
	  TabSheet_optionen: TTabSheet;
	  Host: TLabel;
	  Datenbank: TLabel;
    Label5: TLabel;
    Label6: TLabel;
	  Cipher: TCipherManager;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit_zeit: TMaskEdit;
	  Edit_backup: TEdit;
    Edit_password: TEdit;
    Edit_user: TEdit;
    Edit_db: TEdit;
	  Edit_host: TEdit;
    OpenDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    Memo: TMemo;
	  Edit_bin: TEdit;
	  Label4: TLabel;
	  Label7: TLabel;
	  Edit_data: TEdit;
	  BitBtn1: TBitBtn;
	  BitBtn3: TBitBtn;
	  BitBtn2: TBitBtn;
	  Timer: TTimer;
	  SchreibeBackup1: TMenuItem;
    Timer_dat: TTimer;
    Label8: TLabel;
    Edit_cmd: TEdit;
    BitBtn4: TBitBtn;
    SpinEdit_anzahl: TSpinEdit;
    Label11: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Edit_restore: TEdit;
    BitBtn_restore: TBitBtn;
    BitBtnw2: TBitBtn;
    GroupBox3: TGroupBox;
    Button_s1: TButton;
    Button2: TButton;
    Button3: TButton;
    GroupBox4: TGroupBox;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
	  Button_s2: TButton;
    Button_s3: TButton;
    Label12: TLabel;
	  procedure BitBtn1Click(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure TimerTimer(Sender: TObject);
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer_datTimer(Sender: TObject);
	  procedure BitBtn_restoreClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure MaskEdit_zeitChange(Sender: TObject);
    procedure Button_s1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button_s3Click(Sender: TObject);
    procedure Button_s2Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure SchreibeBackup1Click(Sender: TObject);
	private
	  { Private-Deklarationen }
   datei,zipdat,zipdat_crypt,command:string;
    f: file;
    betriebssystem:integer;
    dat:tdate;
    backuplist:tstringlist;
		procedure reglesen;
		procedure regschreiben;
		procedure erzeuge_backup;
     procedure dat_kompress;
     function dateiendung(datei: string) : string;


	public
	  { Public-Deklarationen }
	end;
	function stringinvert(str:string): string;
	function sql_pfad(pfad:string):string;
	function sql_datetostr(datum:tdate):string;
	function sql_timetostr(zeit:ttime):string;
var
	Form_main: TForm_main;

implementation

{$R *.DFM}

function sql_pfad(pfad:string):string;
var
position:integer;
begin
position:=pos('\',pfad);
while position>0 do
begin
	delete(pfad,position,1);
	insert('/',pfad,position);
  position:=pos('\',pfad);
end;
result:=pfad;
end;


procedure TForm_main.BitBtn1Click(Sender: TObject);
var
pfad:string;
position:integer;
begin

if not form_main.OpenDialog.Execute then exit;
pfad:=form_main.OpenDialog.FileName;
pfad:=stringinvert(pfad);
position:=pos('\',pfad);
pfad:=copy(pfad,position,length(pfad));
pfad:=stringinvert(pfad);
if sender=bitbtn1 then edit_backup.Text:=pfad;
if sender=bitbtn2 then edit_bin.Text:=pfad;
if sender=bitbtn3 then edit_data.Text:=pfad;
if sender=bitbtn4 then edit_cmd.Text:=pfad;
if sender=bitbtn_restore then edit_restore.Text:=form_main.OpenDialog.FileName;


end;


function stringinvert(str:string): string;
var l,i:integer;
begin
	result:='';
  l:=Length(str);
	for i:= l downto 1 do
	begin
		result:=result+str[i];
	end;
end;

procedure tform_main.reglesen;
var
	reg: Tregistry;
begin
try
	reg := Tregistry.create;
	reg.RootKey:=hkey_current_user;
	reg.openkey('software\Klawitter\mybackup',false);
	Edit_backup.text:=reg.readstring('pfad_backup');
	edit_bin.text:=reg.readstring('pfad_bin');
	edit_data.text:=reg.readstring('pfad_data');
  edit_cmd.text:=reg.readstring('pfad_cmd');
  spinedit_anzahl.value:=reg.ReadInteger('backup_zahl');
	maskedit_zeit.text:=reg.readstring('zeit');
	edit_host.text:=reg.readstring('host');
	edit_db.text:=reg.readstring('db');
	edit_user.text:=Cipher.decodeString(reg.readstring('user'));
	edit_password.text:=Cipher.decodeString(reg.readstring('passwort'));
  PageControl1Change(Self);
except
end;
end;


procedure tform_main.regschreiben;
var
	reg: Tregistry;
begin
try
	reg := Tregistry.create;
	reg.RootKey:=hkey_current_user;
	reg.openkey('software\Klawitter\mybackup',true);
	reg.writestring('pfad_backup', edit_backup.text);
	reg.writestring('pfad_bin',edit_bin.text);
	reg.writestring('pfad_data',edit_data.text);
	reg.writestring('pfad_cmd',edit_cmd.text);
  reg.WriteInteger('backup_zahl',spinedit_anzahl.value);
	reg.writestring('zeit',maskedit_zeit.text);
	reg.writestring('host',edit_host.text);
	reg.writestring('db',edit_db.text);
	reg.writestring('user',Cipher.encodeString(edit_user.text));
	reg.writestring('passwort',Cipher.encodeString(edit_password.text));
except
end;
end;

procedure TForm_main.FormCreate(Sender: TObject);
var s1,s2:string;
	versionsinfo: OSVERSIONINFO;
begin
 dateseparator:='.';
 timeseparator:=':';
 dat:=strtodate('01.01.1000');
 backuplist:=tstringlist.Create;
 s1:= '13tdatenbankasdf kann sdfnicht geoeffnet werden bz7eqianahuaor9isadfa9dieser svorgang ist verbotensddh98239al93 bitte abbrechenassie sind hierzu nicht berechtigtdj1af';
 s2:=copy(s1,15,20)+copy(s1,5,20)+copy(s1,55,20)+copy(s1,101,20)+copy(s1,33,20)+copy(s1,77,20)+copy(s1,121,8);
 cipher.InitKey(s2,nil);
 reglesen();
 versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;
 	if betriebssystem=VER_PLATFORM_WIN32_WINDOWS then command:='command.com' ;
	if betriebssystem=VER_PLATFORM_WIN32_NT	 then command:='cmd.exe' ;
end;

procedure TForm_main.TimerTimer(Sender: TObject);
begin
	if (dat<> date()) and (strtotime(maskedit_zeit.Text)<time()  )then erzeuge_backup;
end;

procedure tform_main.erzeuge_backup;
var cmd_path,pfad, parameter:string;
s:integer;
begin
	while memo.Lines.Count> 100 do memo.Lines.Delete(0);
	cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);
	dat:=date();
	datei:=edit_backup.text+sql_datetostr(date);
	if fileexists(datei) then deletefile(datei);
	datei:=sql_pfad(datei);
	zipdat:=datei+'.zip';
	zipdat_crypt:=datei+'.arb';
	datei:=datei+'.sql';
	//pfad:='c:\command.com';   // c: /K '+ trim(edit_bin.Text) +'mysqldump.exe';
	pfad:=trim(edit_cmd.text)+command;
	parameter:=format('%s /C %smysqldump.exe -h "%s" -u "%s" --password="%s" %s >"%s"',
					  [cmd_path, trim(edit_bin.Text),edit_host.text,edit_user.text,edit_password.text, edit_db.text, datei]);

	shellexecute(form_main.handle,'open',pchar(pfad),pchar(parameter),pchar('c:\windows'),SW_normal);
	timer_dat.enabled:=true; //kompression nach 5 sekunden

end;


procedure TForm_main.FormClose(Sender: TObject; var Action: TCloseAction);
begin
regschreiben;
end;

function sql_datetostr(datum:tdate):string;
var
s_datum:string;
begin
s_datum:=datetostr(datum);
result:=copy(s_datum,7,4)+'-'+copy(s_datum,4,2)+'-'+copy(s_datum,1,2);
end;

function sql_timetostr(zeit:ttime):string;
var
s_time:string;
begin
s_time:=timetostr(zeit);
result:=s_time;//'"'+copy(s_datum,7,4)+'-'+copy(s_datum,4,2)+'-'+copy(s_datum,1,2)+'"';
end;

procedure TForm_main.Timer_datTimer(Sender: TObject);
var
s:integer;
begin
try
	assignfile(f,datei);
	reset(f);
	s:=filesize(f);
	closefile(f);
	if s<10 then exit;

	timer_dat.enabled:=false;
	dat_kompress;
except
 	s:=0;
end;
end;

procedure tform_main.dat_kompress;
var
filelist:tstringlist;
begin
try
	filelist:=tstringlist.Create;
	filelist.Add(datei);
	memo.Lines.Add('Backup nach '+datei );
	zipfiles( zipdat,filelist,memo);
	//deletefile(datei);
	memo.Lines.Add('Komprimieren nach '+zipdat );
	cipher.encodeFile(zipdat,zipdat_crypt);
	memo.Lines.Add('Verschl�sseln nach '+zipdat_crypt);

	deletefile(zipdat);
	deletefile(datei);
  backuplist.Add(zipdat_crypt);
  while backuplist.count>spinedit_anzahl.Value do
	begin
  	if fileexists(backuplist[0]) then deletefile(backuplist[0]);
		backuplist.Delete(0);
  end;

finally
 filelist.free;
end;
end;
procedure TForm_main.BitBtn_restoreClick(Sender: TObject);
var
cmd_path,datname, befehl,parameter:string;

begin
	 cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);
	if dateiendung(edit_restore.text)<>'arb' then
	begin
		showmessage('keine g�ltige Datei');
		exit;
	end;


	if DirectoryExists(edit_data.text+'arbmed') then
	begin
	showmessage ('Bitte das Verzeichnis '+edit_data.text+'arbmed l�schen.');
  exit;
  end;
  ForceDirectories(edit_data.text+'arbmed');

  datname:=edit_backup.text+'arbmed.zip';
  cipher.decodeFile(edit_restore.text,datname);
 	unzipfile(datname,edit_backup.text,memo);
  deletefile(datname);
	datname:=stringinvert(edit_restore.text);
	datname:=copy(datname,5,10);
	datname:=edit_backup.text+stringinvert(datname)+'.sql';
	datname:=sql_pfad(datname);

	//befehl:='c:\command.com';   // c: /K '+ trim(edit_bin.Text) +'mysqldump.exe';
	befehl:= trim(edit_cmd.text)+command;
	parameter:=format('%s /C %smysql.exe --host="%s" --user="%s" --password="%s" %s <"%s"',
					  [cmd_path,trim(edit_bin.Text),edit_host.text,edit_user.text,edit_password.text, edit_db.text, datname]);

	shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);
	showmessage('Bitte abwarten bis Dos-Fenster geschlossen');
end;

function TForm_main.dateiendung(datei: string) : string;
var
position: integer;
begin
	result:='';
	position:=pos('.',datei);
	while position>0 do
	begin
		datei:=copy(datei,position+1,length(datei));
     result:=datei;
     position:=pos('.',datei);
	end;
end;


procedure TForm_main.PageControl1Change(Sender: TObject);
begin
if pagecontrol1.ActivePage=tabsheet1 then
begin
	timer.Enabled:=true;
  statusbar.Panels[0].text:='Automatisches Backup aktiv';
end
else
begin
	timer.Enabled:=false;
  statusbar.Panels[0].text:='Automatisches Backup angehalten';
end;
end;

procedure TForm_main.MaskEdit_zeitChange(Sender: TObject);
begin
try
 strtotime(maskedit_zeit.text)
except
 maskedit_zeit.text:='00:00'
end;
end;

procedure TForm_main.Button_s1Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);

befehl:= trim(edit_cmd.text)+command;

parameter:=format('%s /C %smyisamchk.exe -r %sarbmed\*.myi',
					  [cmd_path,trim(edit_bin.Text),trim(edit_data.Text)]);
shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

end;

procedure TForm_main.Button2Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);

befehl:= trim(edit_cmd.text)+command;
parameter:=format('%s /C %smysqladmin.exe shutdown',
					  [cmd_path,trim(edit_bin.Text)]);
shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

end;

procedure TForm_main.Button3Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);

befehl:= trim(edit_cmd.text)+command;
parameter:=format('%s /C %smysqld-opt.exe',
					  [cmd_path,trim(edit_bin.Text)]);
shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

end;

procedure TForm_main.Button_s3Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);

befehl:= trim(edit_cmd.text)+command;

parameter:=format('%s /C %smyisamchk.exe -o %sarbmed\*.myi',
					  [cmd_path,trim(edit_bin.Text),trim(edit_data.Text)]);
shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);



end;

procedure TForm_main.Button_s2Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);

befehl:= trim(edit_cmd.text)+command;

parameter:=format('%s /C %smyisamchk.exe -r -f %sarbmed\*.myi',
					  [cmd_path,trim(edit_bin.Text),trim(edit_data.Text)]);
shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

end;

procedure TForm_main.Button6Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
cmd_path:=copy(edit_cmd.text,1,length(edit_cmd.text)-1);

befehl:= trim(edit_cmd.text)+command;

parameter:=format('%s /C %smyisamchk.exe -S %sarbmed\*.myi',
					  [cmd_path,trim(edit_bin.Text),trim(edit_data.Text)]);
shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);


end;

procedure TForm_main.SchreibeBackup1Click(Sender: TObject);
begin
erzeuge_backup;
end;

end.
