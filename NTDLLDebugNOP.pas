unit NTDLLDebugNOP;

interface

implementation

uses
   SysUtils,
   Windows;

procedure PatchINT3;
var
    NOP : Byte;
    BytesWritten : DWORD;
    NtDll : THandle;
    P : Pointer;
begin
    if Win32Platform <> VER_PLATFORM_WIN32_NT then
        Exit;

    NtDll := GetModuleHandle('NTDLL.DLL');
    if NtDll = 0 then
        Exit;

    P := GetProcAddress(NtDll, 'DbgBreakPoint');
    if P = nil then
        Exit;

    try
        if Char(P^) <> #$CC then
            Exit;

        NOP := $90;
        if WriteProcessMemory(GetCurrentProcess, P, @NOP, 1,
                              BytesWritten) and
           (BytesWritten = 1) then
            FlushInstructionCache(GetCurrentProcess, P, 1);
    except
        //Do not panic if you see an EAccessViolation here,
        // it is perfectly harmless!
        on EAccessViolation do ;
        else raise;
    end;
end;

initialization

    {$ifopt D+}
    PatchINT3;
    {$endif}

end.
