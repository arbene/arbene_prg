unit l_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Db,     ExtCtrls, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, ZConnection, ZSqlProcessor, ComCtrls;

type
	TForm_main = class(TForm)
    BitBtn_loesch: TBitBtn;
    Memo: TMemo;
    Connection: TZConnection;
    q: TZQuery;
    sql: TZQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    RadioGroup: TRadioGroup;
    Edit_query: TEdit;
    CheckBox_nk: TCheckBox;
    CheckBox_ht: TCheckBox;
    Edit_db: TEdit;
    Label2: TLabel;
    SQLProcessor: TZSQLProcessor;
    procedure BitBtn_loeschClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
	Form_main: TForm_main;

implementation

{$R *.DFM}

procedure TForm_main.BitBtn_loeschClick(Sender: TObject);
var i:integer;
	  db_name,query:string;
	  tabellen: array[0..3] of string;
begin
if messagedlg('query ausf�hren?',
	  mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
connection.disConnect;
connection.database:=edit_db.text;
connection.Connect;
memo.Lines.Clear;
q.Active:=false;
q.Sql.Clear;

{if radiogroup.ItemIndex=0 then
	q.sql.add('select * from export_import')
	else
	q.sql.add('select * from nummern_kreis');

q.active:=true;

q.First;
while not q.eof do
begin
db_name:=q['tab_name'];

if db_name<>'abfragen' then
begin
if checkbox_nk.Checked then
begin
	query:=format('update nummern_kreis set aktuell=1 where tab_name="%s"  ',[db_name]);
  
	sqlprocessor.script.Clear;
	sqlprocessor.script.add(query);
	sqlprocessor.Execute;
end;

try
	query:=format(edit_query.text ,[db_name]);
	sqlprocessor.script.Clear;
	sqlprocessor.script.add(query);
	sqlprocessor.Execute;
	except
		query:=query+ ' FEHLER';
end;
	memo.Lines.Add(query);
end;
q.next;
end;

if radiogroup.ItemIndex=1 then  //bei nummern_kreis auch arbeitsunfall bearbeiten
begin
tabellen[0]:='arbeitsunfall';
tabellen[1]:='taetigkeiten';
tabellen[2]:='abteilung';
for i:=0 to 2 do
	begin
	  try
	  query:=format(edit_query.text ,[tabellen[i]]);
	  sqlprocessor.script.Clear;
	sqlprocessor.script.add(query);
	sqlprocessor.Execute;
	  except
		  query:=query+ ' FEHLER';
	  end;
	  memo.Lines.Add(query);
	end;

end;   }


if checkbox_ht.Checked then
begin
	q.Active:=false;
	q.Sql.Clear;
	q.sql.add('select * from haupt_tabelle');
	q.active:=true;
	q.edit;
	q['haupt']:=1;
	q['s1']:='';
	q['hs1']:='';
	q['hs2']:='';
	q['ln']:='';
	q['nummern_kreis']:=0;
	q['pfad_temp']:='c:\';
  q['last_export']:='1900-01-01';
	q.post;
	memo.Lines.Add('haupt-tabelle zur�ck gesetzt');


	query:='delete from satelliten' ;
	sqlprocessor.script.Clear;
	sqlprocessor.script.add(query);
	sqlprocessor.Execute;
	memo.Lines.Add(query);

end;

end;

end.
