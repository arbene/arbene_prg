unit a_termine_drag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask,math,Variants;

type
  TForm_terminieren_drag = class(TForm)
	  Label_Name: TLabel;
	  MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Panel1: TPanel;
    Memo: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label_termin: TLabel;
    RadioGroup: TRadioGroup;
    Label_typ: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit1Change(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_terminieren_drag: TForm_terminieren_drag;

implementation

uses a_main,  a_data;

{$R *.DFM}

procedure TForm_terminieren_drag.FormActivate(Sender: TObject);
begin

	maskedit1.SetFocus;
end;

procedure TForm_terminieren_drag.FormShow(Sender: TObject);
begin
with datamodul do
begin
  if not email_send then  radiogroup.ItemIndex:=max(1,radiogroup.ItemIndex);
	form_main.form_positionieren(tform(sender));

end;
end;

procedure TForm_terminieren_drag.MaskEdit1Change(Sender: TObject);
var
time1, time2:tdatetime;
begin
time1:=strtotime(maskedit1.edittext);

if  datamodul.q_unt_filter['dauer']<>null
	then time2:=datamodul.q_unt_filter['dauer']//*0.00069444444444  in mysql ohne faktor warum ???
  else time2:=0;
maskedit2.Text:=timetostr(time1+time2);
//datamodul.q_typ
end;

end.
