unit a_wartefenster;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, jpeg, StdCtrls, Buttons;

type
  Tform_wartefenster = class(TForm)
    Panel_bottom: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Image1: TImage;
    Timer1: TTimer;
    Panel4: TPanel;
    Panel5: TPanel;
    Memo1: TMemo;
    ProgressBar1: TProgressBar;
    Panel6: TPanel;
    Panel7: TPanel;
    BitBtn1: TBitBtn;
    Label_spenden: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    spenden:boolean;
  end;

var
  form_wartefenster: Tform_wartefenster;

implementation

uses a_main;

{$R *.dfm}

procedure Tform_wartefenster.Timer1Timer(Sender: TObject);
begin
  progressbar1.position:=progressbar1.position+1;
  if progressbar1.position>=60 then modalresult:=mrok;
  application.ProcessMessages;
end;

procedure Tform_wartefenster.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

procedure Tform_wartefenster.BitBtn1Click(Sender: TObject);
begin
form_wartefenster.spenden:=true;
label_spenden.Caption:='Danke, Sie werden gleich auf das Spendenformular weitergeleitet.';
end;

end.
