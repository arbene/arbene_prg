unit a_unt_plan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ComCtrls, ToolWin, ExtCtrls, Grids, DBGrids,
  dbgridEXT, Buttons,a_data,db,Variants, Menus;

type
  TForm_unt_plan = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel_right: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    SpeedButton1: TSpeedButton;
    Button_frist: TButton;
    Panel2: TPanel;
    Panel_li_mi: TPanel;
    GroupBox2: TGroupBox;
    StringGrid: TStringGrid;
    GroupBox_info: TGroupBox;
    DBMemo_info: TDBMemo;
    DBMemo_untersuchung: TDBMemo;
    RadioGroup_abfolge: TRadioGroup;
    PopupMenu_stringgrid: TPopupMenu;
    m_ausgewhlteZeilelschen: TMenuItem;
    procedure ToolButton5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button_fristClick(Sender: TObject);
    procedure frist(dat:tdate);
    procedure StringGridRowMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure StringGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure RadioGroup_abfolgeClick(Sender: TObject);
    procedure nachuntersuchung_einlesen;
    procedure m_ausgewhlteZeilelschenClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
  private
    { Private-Deklarationen }
public
    { Public-Deklarationen }
    //nr_liste,art_liste:tstringlist;
    uil,paw,i_nachuntersuchung:array of integer;
    datum_heute,dat_default:tdate;
    i_m_typ,a_nachuntersuchung:integer;
    i_m_proband:int64;

  end;

var
  Form_unt_plan: TForm_unt_plan;

implementation

uses a_main, a_kalender;

{$R *.DFM}

procedure TForm_unt_plan.ToolButton5Click(Sender: TObject);
var
i:integer;
begin
	if Messagedlg('Soll der aktuelle Datensatz gel�scht werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
  i:=stringgrid.row;
  //stringgrid.Rows[i];
  //uil:=delete(uil,i,1);
end;

procedure TForm_unt_plan.SpeedButton1Click(Sender: TObject);
var
i:integer;
begin
i:=stringgrid.Row;

form_kalender.auswahlkalender.date:=strtodate(stringgrid.Cells[1,i]);
if form_kalender.showmodal =mrok then
	begin
   stringgrid.Cells[1,i]:=datetostr(form_kalender.auswahlkalender.date);
  end;
end;

procedure TForm_unt_plan.FormShow(Sender: TObject);
begin
  form_main.form_positionieren(tform(sender));
end;

procedure TForm_unt_plan.FormCreate(Sender: TObject);
var
query:string;
begin


Caption:='Nachfolgetermine planen';
datum_heute:=datum_heute;
setlength(i_nachuntersuchung,1);
setlength(uil,1);
setlength(paw,1);
setlength(i_nachuntersuchung,22);
setlength(uil,22);
setlength(paw,22);

query:='select frist, i_frist from fristen  where storno=0 order by i_frist';
datamodul.sql(false,datamodul.q_7,query,'');
stringgrid.ColWidths[0]:=stringgrid.Width-stringgrid.DefaultColWidth*1-2;



end;

procedure TForm_unt_plan.Button_fristClick(Sender: TObject);
var
i,tage:integer;
dat:tdate;
y,m,d:word;
y1,m1,d1:word;
sd:string;
begin
i:=stringgrid.Row;
tage:=datamodul.q_7.findfield('i_frist').asinteger;
{dat:=strtodate(stringgrid.Cells[1,i]);
decodedate(dat,y,m,d ); }
dat:=datum_heute+tage;

   stringgrid.Cells[1,i]:=datetostr(dat);

end;

procedure TForm_unt_plan.frist(dat:tdate);
var
r:boolean;
suchfeld:string;
begin
r:=datamodul.q_7.Locate( 'i_frist',dat,[]) ;
if not r then
begin
  datamodul.q_7.first;


    while not datamodul.q_7.eof do
    begin
      if dat<=datamodul.q_7.findfield('i_frist').asinteger then exit;
      datamodul.q_7.next;
    end;


end;
end;

procedure TForm_unt_plan.StringGridRowMoved(Sender: TObject; FromIndex, ToIndex: Integer);
var
f:tdate;
begin
f:=   strtodate(stringgrid.Cells[1,toindex])-datum_heute;
frist(f);
end;

procedure TForm_unt_plan.StringGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
f:tdate;
begin
if stringgrid.Cells[1,arow]<>'' then
  f:=   strtodate(stringgrid.Cells[1,arow])-datum_heute
else
  f:=1095;  
frist(f);
end;

procedure TForm_unt_plan.RadioGroup_abfolgeClick(Sender: TObject);
var
f:tdate;
begin
nachuntersuchung_einlesen;

f:=   strtodate(stringgrid.Cells[1,stringgrid.row])-datum_heute;
frist(f);

end;


procedure tform_unt_plan.nachuntersuchung_einlesen;
var
unt,suil,query,zeitfeld:string;
uil,paw:integer;
datum:tdatetime;
felder :array [0..1] of string;
i:integer;
cs:boolean;
begin
with datamodul do
begin
if radiogroup_abfolge.ItemIndex=1 then zeitfeld:='frist' else zeitfeld:='vorgezogen';

i:=0;
q_nachuntersuchung.First;
 if q_nachuntersuchung.Eof then  //kein Datensatz
  begin
    // wenn keine Vorgabe dann gleiche Untersuchung nach 3 Jahren
    query:='select untersuchung,bereich from typ where nummer='+inttostr(i_m_typ);
    mysql_d.Feldarray(query,[0,1],felder );
    unt:=felder[0];
    suil:=felder[1];
    uil:=strtoint(suil);
    paw:= q_untersuchung.findfield('i_paw').asinteger;
    datum:=datum_heute+ 1068;
     //unt:=mysql_d.Feldinhalt(query,0);
     form_unt_plan.StringGrid.cells[0,i]:=unt;
     form_unt_plan.StringGrid.cells[1,i]:=datetostr(datum);
     form_unt_plan.i_nachuntersuchung[i]:=i_m_typ;

     form_unt_plan.StringGrid.Rowcount:=i+1;
  end;

  while not q_nachuntersuchung.eof do
  begin
     a_nachuntersuchung:=q_nachuntersuchung.findfield('i_nachuntersuchung').asinteger;
     uil:=q_nachuntersuchung.findfield('uil').asinteger;
     paw:= q_nachuntersuchung.findfield('i_paw').asinteger;
     query:='select untersuchung from typ where nummer='+inttostr(a_nachuntersuchung);
     if (q_nachuntersuchung[zeitfeld]<>null)  then
     datum:=datum_heute+ q_nachuntersuchung.findfield(zeitfeld).asinteger;
     if datum=datum_heute then datum:=datum+365;
     unt:=mysql_d.Feldinhalt(query,0);
     form_unt_plan.StringGrid.cells[0,i]:=unt;
     form_unt_plan.StringGrid.cells[1,i]:=datetostr(datum);
     form_unt_plan.i_nachuntersuchung[i]:=a_nachuntersuchung;
     form_unt_plan.uil[i]:=uil;
     form_unt_plan.paw[i]:=paw;

     inc(i);
     form_unt_plan.StringGrid.Rowcount:=i;
     if i>21 then break;  //nicht sch�n aber selten wie verl�ngert man ein array???????????????
     q_nachuntersuchung.next;
  end;

  form_unt_plan.StringGridSelectCell( self,1,0,cs);
end;
end;

procedure TForm_unt_plan.m_ausgewhlteZeilelschenClick(Sender: TObject);
var
RowNumber,i:integer;
begin
  if stringgrid.RowCount<=1 then
  begin
    showmessage('Die letzte Zeile kann nicht gel�scht werden');
    exit;
  end;
  RowNumber:=stringgrid.Row;

  for i := RowNumber to stringgrid.RowCount - 2 do
  begin
    stringgrid.Rows[i].Assign(stringgrid.Rows[i+ 1]);
    uil[i]:=uil[i+1];
    paw[i]:=paw[i+1];
    i_nachuntersuchung[i]:=(i_nachuntersuchung[i+1]);
  end;

  stringgrid.Rows[stringgrid.RowCount-1].Clear;
  stringgrid.RowCount := stringgrid.RowCount - 1;

end;



procedure TForm_unt_plan.DBGrid1CellClick(Column: TColumn);
begin
 Button_fristClick(self);
end;

end.
