unit abackup_main;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, Mask, HCMngr, ComCtrls, Menus,registry, ExtCtrls,zip,
	Spin,shellapi,FileCtrl, {db_tools_datamodul,}DateUtils, mysql_direkt_small,  ShlObj,
  Grids, DBGrids;

type
TAutorunKind = (akUserRun, akUserRunOnce, akRun, akRunOnce, akRunServices, akRunServicesOnce);

type
  TaForm_main = class(TForm)
    StatusBar: TStatusBar;
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    N1: TMenuItem;
    Beenden1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
	  TabSheet_optionen: TTabSheet;
	  Host: TLabel;
	  Edit_host: TEdit;
    OpenDialog: TOpenDialog;
    GroupBox1: TGroupBox;
	  Edit_bin: TEdit;
	  Label4: TLabel;
	  Label7: TLabel;
	  Edit_data: TEdit;
	  BitBtn3: TBitBtn;
	  BitBtn2: TBitBtn;
	  Timer: TTimer;
	  SchreibeBackup1: TMenuItem;
    GroupBox2: TGroupBox;
    Datenbank: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    Edit_password: TEdit;
    Edit_user: TEdit;
    Edit_db: TEdit;
    Label1: TLabel;
    Panel1: TPanel;
    Label8: TLabel;
    TabSheet3: TTabSheet;
    GroupBox4: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Datenbankherunterfahren1: TMenuItem;
    Datenbankstarten1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Datenbankinitialisieren1: TMenuItem;
    Timer_back: TTimer;
    GroupBox3: TGroupBox;
    Button_repair: TButton;
    Label_rep: TLabel;
    GroupBox5: TGroupBox;
    Label3: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    FileListBox: TFileListBox;
    Memo: TMemo;
    Button_opt: TButton;
    weiteres: TTabSheet;
    Label17: TLabel;
    Button9: TButton;
    Label18: TLabel;
    Label19: TLabel;
    Button_rep3: TButton;
    Label23: TLabel;
    ComboBox_mv: TComboBox;
    GroupBox6: TGroupBox;
    FileListBox_backup: TFileListBox;
    Panel2: TPanel;
    BitBtn_start: TBitBtn;
    BitBtn_stop: TBitBtn;
    Timer_wdh: TTimer;
    label_wdh: TLabel;
    GroupBox8: TGroupBox;
    Memo_wdh: TMemo;
    Button_ini: TButton;
    Label25: TLabel;
    Panel3: TPanel;
    Label24: TLabel;
    Edit_restore: TEdit;
    BitBtn_wahl: TBitBtn;
    BitBtnw2: TBitBtn;
    Button1: TButton;
    Edit_neu: TEdit;
    CheckBox_ren: TCheckBox;
    CheckBox_p: TCheckBox;
    button_abbruch: TButton;
    Checkc: TCheckBox;
    Edit_p: TEdit;
    Label_p: TLabel;
    HashManager: THashManager;
    Cipher: TCipherManager;
    TabSheet_history: TTabSheet;
    Button_hist_tab_erzeugen: TButton;
    Button_hist_tab_loeschen: TButton;
    DBGrid_history: TDBGrid;
    Memo_history: TMemo;
    Label27: TLabel;
    Label_trigger: TLabel;
    Label28: TLabel;
    TabSheet_backup: TTabSheet;
    GroupBox9: TGroupBox;
    Label22: TLabel;
    Label26: TLabel;
    Edit_passwort: TEdit;
    Label29: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    Label9: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    MaskEdit_zeit: TMaskEdit;
    SpinEdit_anzahl: TSpinEdit;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    TabSheet_dbkrypt: TTabSheet;
    GroupBox10: TGroupBox;
    Button10: TButton;
    Button11: TButton;
    GroupBox11: TGroupBox;
    Edit_key_pfad: TEdit;
    Label30: TLabel;
    Label32: TLabel;
    edit_key_name: TEdit;
    Label33: TLabel;
    GroupBox12: TGroupBox;
    Label31: TLabel;
    Edit_key: TEdit;
    Button12: TButton;
    Button13: TButton;
    GroupBox13: TGroupBox;
    Memo_status: TMemo;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Button_tl: TButton;
    Label6: TLabel;
    Edit_backup: TEdit;
    BitBtn1: TBitBtn;
    Button_db: TButton;
    Label38: TLabel;
    Label39: TLabel;
    Timer_autbackup: TTimer;
    GroupBox14: TGroupBox;
    Memo1: TMemo;
    PageControl_wiederherstellung: TPageControl;
    TabSheet_protokoll: TTabSheet;
    TabSheet_fehlerprotokoll: TTabSheet;
    script: TMemo;
    Memo_fehler: TMemo;
    Button14: TButton;
    Button15: TButton;
    Labe_rep_h: TLabel;
	  procedure BitBtn1Click(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure TimerTimer(Sender: TObject);
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
	  procedure BitBtn_restoreClick(Sender: TObject);
    procedure MaskEdit_zeitChange(Sender: TObject);
    procedure Button_repairClick(Sender: TObject);
    procedure Button_db_herunterfahrenClick(Sender: TObject);
    procedure Button_db_startenClick(Sender: TObject);
    procedure Button_s3Click(Sender: TObject);
    procedure Button_s2Click(Sender: TObject);
	  procedure Button6Click(Sender: TObject);
    procedure SchreibeBackup1Click(Sender: TObject);
    procedure Datenbankinitialisieren1Click(Sender: TObject);
    procedure Beenden1Click(Sender: TObject);
    procedure BitBtn_wahlClick(Sender: TObject);
    procedure Timer_backTimer(Sender: TObject);
    procedure Button_optClick(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure db_herunterfahren;
    procedure db_starten;
    procedure Button_rep3Click(Sender: TObject);
    function dumpdb(db:string):string;
    procedure wiederherstellen;
    procedure wiederherstellen_alt;
    procedure backup;
    procedure RenameDir(DirFrom, DirTo: string);
    procedure BitBtn_startClick(Sender: TObject);
    procedure BitBtn_stopClick(Sender: TObject);
    procedure Timer_wdhTimer(Sender: TObject);
    procedure ComboBox_mvChange(Sender: TObject);
    procedure button_abbruchClick(Sender: TObject);
    procedure Button_iniClick(Sender: TObject);
    
    function getCurrentVersion: String;
    function  showserver:string;
    procedure refresh_trigger;
    procedure PageControl1Change(Sender: TObject);
    procedure Button_hist_tab_erzeugenClick(Sender: TObject);
    procedure Button_hist_tab_loeschenClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure edit_key_nameExit(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button_tlClick(Sender: TObject);
    procedure Button_dbClick(Sender: TObject);
    procedure Timer_autbackupTimer(Sender: TObject);
    procedure CipherProgress(Sender: TObject; Current, Maximal: Integer);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
	private
	  { Private-Deklarationen }
    mysql_d:tmysql_d;
   datei,zipdat,zipdat_crypt,command:string;
    f_1: file;
    betriebssystem:integer;
    dat:tdate;
    abbruch:boolean;
    //backupdatei,backupdatei_c:string;
    	f_a,f_c:textfile;
      p_alt:integer;
    //backuplist:tstringlist;
		procedure reglesen;
		procedure regschreiben;
     function dateiendung(datei: string) : string;
     procedure inilesen;
     function stringwenden(wort: string): string;
     function wurzelpfad(pfad: string): string;
     procedure reptable;
     procedure wiederherstellen_p(datei:string);

     procedure  test_pfade;
     procedure loesche_backup;
        procedure show_status;
     function dat_verschluesseln(source,dest:string):boolean;
     function dat_entschluesseln(source, dest:string):boolean;
     procedure mysqld(datei,info:string) ;
     function CleanText(S: String): String;
     procedure keyschreiben(pfad,key:string);
     procedure mysqlini_schreiben(verschluesseln:boolean);
     function mysql_verschluesselung_pruefen:boolean;
	public
	  { Public-Deklarationen }
      procedure verbinden_mit_db;

	end;
	function stringinvert(str:string): string;
	function sql_pfad(pfad:string):string;
	function sql_datetostr(datum:tdate):string;
	function sql_timetostr(zeit:ttime):string;
   //function GetFileSize(const szFile: String): Int64;
   function getFileSize(fn: TFileName): Int64;
   function DeleteFileWithUndo(sFileName: string): Boolean;
  function GetSpecialFolderPath(Folder: Integer; CanCreate: Boolean): string;
  function CreateAutorunEntry(const AName, AFilename: string; const AKind: TAutorunKind): Boolean;
var
	aForm_main: TaForm_main;

implementation

uses dbtools_pfadwahl, db_tools_datamodul;


{$R *.DFM}

function GetSpecialFolderPath(Folder: Integer; CanCreate: Boolean): string;

// Gets path of special system folders
//
// Call this routine as follows:
// GetSpecialFolderPath (CSIDL_PERSONAL, false)
//        returns folder as result
//
var
   FilePath: array [0..255] of char;

begin
 SHGetSpecialFolderPath(0, @FilePath[0], FOLDER, CanCreate);
 Result := FilePath;
end;

function sql_pfad(pfad:string):string;
var
position:integer;
begin
position:=pos('\',pfad);
while position>0 do
begin
	delete(pfad,position,1);
	insert('/',pfad,position);
  position:=pos('\',pfad);
end;
result:=pfad;
end;





function stringinvert(str:string): string;
var l,i:integer;
begin
	result:='';
  l:=Length(str);
	for i:= l downto 1 do
	begin
		result:=result+str[i];
	end;
end;




procedure taForm_main.reglesen;
var
	reg: Tregistry;
  s:string;
  i:integer;
  b:boolean;
const
 KEY_WOW64_64KEY = $0100;
 KEY_WOW64_32KEY = $0200;
begin
try

	edit_user.text:='Arbene';//'Cipher.decodeString(reg.readstring('user'));
	edit_password.text:='nix'+'reinkommen';//Cipher.decodeString(reg.readstring('passwort'));
   edit_backup.text:='c:\arbene_daten\backup\';
   edit_bin.text:='c:\mariadb55\bin\';
   edit_data.text:='c:\mariadb55\data\';
    maskedit_zeit.text:='12:00';
   edit_db.text:='arbene' ;
   edit_host.text:='localhost';


	reg := Tregistry.create;
	reg.RootKey:=hkey_current_user;
	if not reg.KeyExists('software\arbene\datenbanktools') then exit;

	reg.openkey('software\arbene\datenbanktools',false);
	//if reg.readstring('pfad_backup')='' then exit;
	Edit_backup.text:=reg.readstring('pfad_backup');
  if edit_backup.text='' then edit_backup.text:='c:\arbene_daten\backup\';


	edit_bin.text:=reg.readstring('pfad_bin');
	if edit_bin.text='' then edit_bin.text:='c:\mariadb55\bin\';
	edit_data.text:=reg.readstring('pfad_data');
	if edit_data.text='' then edit_data.text:='c:\mariadb55\data\';

	spinedit_anzahl.value:=reg.ReadInteger('backup_zahl');
	maskedit_zeit.text:=reg.readstring('zeit');
	if maskedit_zeit.text='  :  ' then maskedit_zeit.text:='12:00';

	s:=reg.readstring('host');
  if s<>'' then edit_host.text:=s else edit_host.text:='localhost' ;


  s:=reg.readstring('db');
  if s<>'' then edit_db.text:=s else edit_db.text:='arbene';


   i:=reg.ReadInteger('protokoll');
   combobox_mv.ItemIndex:=i;
   try
   checkbox1.checked:=reg.ReadBool('backup_1');
   checkbox2.checked:=reg.ReadBool('backup_2');
   except
   end;
   s:=reg.readstring('passwort_backup');
   if s<>'' then edit_passwort.text:=s;

   s:=reg.readstring('schluesseldatei');
   if s<>'' then edit_key_name.text:=s;

   s:=reg.readstring('schluesselpfad');
   if s<>'' then edit_key_pfad.text:=s;


  reg.CloseKey;
  reg.Free;

  //Reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg := Tregistry.create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  if Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', false)   then
  begin
    s:=reg.readstring('Arbene_Datenbank_Tools');
    if s<>'' then statusbar.panels[7].Text:='Autostart aktiv';
    reg.CloseKey;
  end;
  reg.Free;
   filelistbox_backup.Directory:= edit_backup.Text;
   filelistbox_backup.update;
  	//PageControl1Change(Self);

except
end;
end;


procedure taForm_main.regschreiben;
var
	reg: Tregistry;
begin
try
	reg := Tregistry.create;
	//reg.RootKey:=hkey_local_machine;
  reg.RootKey:=hkey_current_user;
	reg.openkey('software\arbene\datenbanktools',true);
	reg.writestring('pfad_backup', edit_backup.text);
	reg.writestring('pfad_bin',edit_bin.text);
	reg.writestring('pfad_data',edit_data.text);
  reg.WriteInteger('backup_zahl',spinedit_anzahl.value);
	reg.writestring('zeit',maskedit_zeit.text);
	reg.writestring('host',edit_host.text);
  reg.writestring('db',edit_db.text);
  reg.writeinteger('protokoll',combobox_mv.ItemIndex);
  reg.WriteBool('backup_1',checkbox1.Checked);
  reg.WriteBool('backup_2',checkbox2.Checked);
  reg.writestring('passwort_backup',edit_passwort.text);
  reg.writestring('schluesseldatei',edit_key_name.text);
  reg.writestring('schluesselpfad',edit_key_pfad.text);
  reg.CloseKey;
  reg.Free;
except
	 showmessage('Sie haben keine Schreib-Berechtigung f�r die Registry "localmachine/arbene/klawitter/Datenbanktools" (regedt32.exe ausf�hren und entsprechende Berechtigung einstellen.)');
end;

end;

procedure taForm_main.inilesen;
var
	f:textfile;
	dat,s,s1,w_pfad:string;
begin
  dat:=GetSpecialFolderPath(28,false)+'\arbene';  //user
   dat:=dat+'\arbene.ini';
   if not fileexists(dat) then
   begin
      showmessage('F�hren sie Arbene unter diesem Win-User aus');
      dat:=GetSpecialFolderPath(35,false)+'\arbene';   //common
      dat:=dat+'\arbene.ini';
      if not fileexists(dat) then dat:=w_pfad+'\arbene.ini';
   end;
//

	if fileexists(dat) then
	try
	  AssignFile(F,dat );   {globales }
	  Reset(F);
	  Readln(F, S);
    readln(f,s1); //datenbank
    edit_host.text:=s;
    edit_db.text:=s1;


	finally
	  CloseFile(F);
	end
end;

procedure TaForm_main.FormCreate(Sender: TObject);
var s1,s2:string;
	versionsinfo: OSVERSIONINFO;
  i,h,w:integer;
begin
h :=aform_main.Height;
w:=aform_main.Width;
 aform_main.Top:=(screen.Height-h)div 2;
 aform_main.Left:=(screen.Width-w) div 2;
 dateseparator:='.';
 timeseparator:=':';
 dat:=strtodate('01.01.1000');
 //backuplist:=tstringlist.Create;
 s1:= '13tdatenbankasdf kann sdfnicht geoeffnet werden bz7eqianahuaor9isadfa9dieser svorgang ist verbotensddh98239al93 bitte abbrechenassie sind hierzu nicht berechtigtdj1af';
 s2:=copy(s1,15,20)+copy(s1,5,20)+copy(s1,55,20)+copy(s1,101,20)+copy(s1,33,20)+copy(s1,77,20)+copy(s1,121,8);
 //cipher.InitKey(s2,nil);
 inilesen;
 reglesen();

 versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;
  if betriebssystem=VER_PLATFORM_WIN32_WINDOWS then command:='command.com' ;
	if betriebssystem=VER_PLATFORM_WIN32_NT	 then command:='cmd.exe' ;
	//label1.Caption:=inttostr(betriebssystem);
	pagecontrol1.ActivePage:=tabsheet1;
	timer.Enabled:=true;
  statusbar.Panels[0].text:='Automatisches Backup ist aktiv';
  statusbar.Panels[5].Text:=getcurrentversion;
  for I := 1 to ParamCount do
	begin
    if lowercase(ParamStr(I))='s' then timer_back.Enabled:=true;
	end;
  //statusbar.Panels[1].Text:='Datenbankserver: '+combobox_mv.items[combobox_mv.itemindex];
  //Button_verbindenClick(self);
  //ComboBox_mvChange(self);

  mysql_d:=tmysql_d.create();//(edit_host.text,'mysql','3306','mysql','Arbene','nixreinkommen');
  //verbinden_mit_db;
  application.BringToFront;
  application.ProcessMessages;
end;

procedure TaForm_main.TimerTimer(Sender: TObject);
begin

  try
     //timer.Enabled:=false;
     BitBtn_stopClick(Sender);

    if (dat<> date()) and (strtotime(maskedit_zeit.Text)<time()  )then
    begin
      backup;
      loesche_backup;
      show_status;
    end;
  finally
    //timer.Enabled:=true;
     BitBtn_startClick(Sender);
  end;
end;


procedure taForm_main.backup;
var
bd,datei:string;
filelist:tstringlist;
t,t1:ttime;
i,j,d,tag,heute,numread:integer;
d1,d2: tdatetime;
dg,size:int64;
//f: file of byte;
fs: tfilestream;
buffer: array [1..1000] of char;
zeile:string;
sicher,g:boolean;

begin
pagecontrol1.ActivePageIndex:=0;
  if pos(' ',trim(edit_backup.text))>0 then
  begin
    memo.Lines.Add('Leerzeichen sind im Pfad oder dem Dateinamen nicht erlaubt.');
    memo.Lines.Add('');
    memo.Lines.Add('Es wird kein Backup geschrieben!!!!!!!!!!!!!!!!!!!');
    memo.Lines.Add('');
    exit;
  end;

  if lowercase(edit_backup.text)='c:\' then
  begin
    memo.Lines.Add('c:\ ist als Backup-Pfad nicht erlaubt.');
    memo.Lines.Add('');
    memo.Lines.Add('Es wird kein Backup geschrieben!!!!!!!!!!!!!!!!!!!');
    memo.Lines.Add('');
    exit;
  end;
	bd:=dumpdb(trim(edit_db.text));    //schreibe backup
   memo.Lines.Add('Backup nach '+bd );


   t:=time();
   while (time()<t+0.005) do //warrten auf backup
   begin
   	if fileexists(bd) then  break;
   end;

   //
     if not fileexists(bd) then
   begin
    memo.Lines.Add('ACHTUNG! Die Datei  '+bd+' wurde nicht erzeugt' );
      exit;
   end;

   memo.Lines.Add('Die Datei  '+bd+' wird geschrieben' );

  g:=true;
  while  g do     //warten bis backup geschrieben
  begin
    try
      application.ProcessMessages;
      fs:=tfilestream.Create(bd,fmOpenRead );
      g:=false;
    except
      g:=true;
    end;
  end;


   dg:=getfilesize(bd);
   //

   memo.Lines.Add('Die Datei  '+bd+' ist erstellt. Dateigr��e: '+inttostr((dg div 1000000))+' MB' );
   if dg>4000 then memo.Lines.Add('Die Backupdatei erscheint aufgrund der Dateigr��e vollst�ndig' ) else
   memo.Lines.Add('ACHTUNG! Die Backupdatei erscheint aufgrund der Dateigr��e unvollst�ndig' ) ;
   memo.Lines.Add('' );

  numread:=fs.Seek(-1001,soFromEnd	);
  inc(numread);

   numread:=fs.Read( buffer,1000);

   zeile:='';
   for i:=1 to numread do
   begin
   	zeile:=zeile+buffer[i];
   end;


   //setstring( zeile,buffer,255);
   //if pos('LOCK TABLES zzz WRITE',zeile)>0 then sicher:=true else sicher:=false;

   if (pos('CREATE TABLE `zzz`',zeile)>0) or (pos('CREATE TABLE zzz',zeile)>0) then sicher:=true else sicher:=false;        //   CREATE TABLE `zzz` mariadb

   if sicher then memo.Lines.Add('Die Backupdatei enth�lt die letzte Tabelle "zzz"' );

   if sicher then memo.Lines.Add('Das Backup ist erfolgreich abgeschlossen.' ) else   memo.Lines.Add('ACHTUNG, Die Backupdatei scheint nicht alle Tabellen zu enthalten' );



   //db_starten;
   fs.Free;

   if trim(edit_passwort.text)<>'' then
   begin
      memo.Lines.Add('Die Backup-Datei wird verschl�sselt');
      memo.Lines.Add('');

      dat_verschluesseln(bd,bd+'_crypt');

      if fileexists(bd+'_crypt')then
      begin
        memo.Lines.Add('Die Backup-Datei wurde nach '+bd+'_crypt verschl�sselt');
        deletefile(bd);
      end
      else showmessage('Die Verschl�sselung hat nicht funktioniert');
   end;



   dat:=date();
   application.ProcessMessages;  ;


   
end;

procedure TaForm_main.loesche_backup;
var
filelist:tstringlist;
t,t1:ttime;
tage,i,j,d,heute:integer;
d1,d2: tdatetime;
l:boolean;
datei:string;
begin
try
  application.bringtofront;
  screen.Cursor:=crhourglass;
  application.ProcessMessages;
  t:=time;
  while t+0.000015 >time do;

  while memo.Lines.Count> 100 do memo.Lines.Delete(0);
  statusbar.Panels[0].text:='l�sche altes Backup';
  memo.Lines.Add('');
  //memo.Lines.Add('Die gel�schten Dateien werden in den Papierkorb verschoben.');
 filelistbox_backup.update;
 tage:=spinedit_anzahl.Value+45;
 if tage=45 then exit;
 
 j:=filelistbox_backup.Items.Count-1;
 for i:=0 to j do
 begin
   filelistbox_backup.ItemIndex:=i;
   datei:=filelistbox_backup.FileName;
   d:=trunc(fileage(datei));
   d1:=FileDateToDateTime(d);
   heute:=trunc(date());
   if d1 <= heute-tage then
   begin
      l:=true;
      tag:=dayofthemonth(d1);

      if checkbox1.Checked and (tag=1) then l:=false;
      if checkbox2.Checked and (tag=15) then l:=false;
      if tage=10 then l:=false;

   	if l then
    begin

      if DeleteFileWithUndo(datei) then  memo.Lines.Add('Verschiebe in den Papierkorb: '+datei) else
      begin

        if deletefile(datei)then memo.Lines.Add('L�sche: '+datei) else memo.Lines.Add(datei+ ' konnte nicht gel�scht werden');
      end;
    end;
   end;
 end;

finally
 screen.Cursor:=crdefault;
 if timer.Enabled then statusbar.Panels[0].text:='Automatisches Backup ist aktiv';
  filelistbox_backup.update;
  application.ProcessMessages;
  memo.Lines.Add('' );

end;
end;

function taform_main.dat_verschluesseln(source, dest:string):boolean;
var
s:string;
begin
  p_alt:=0;
  s:=edit_passwort.text;
  cipher.Tag:=0;
  cipher.InitKey (s,nil);
  Cipher.decodeFile(source,dest);
  deletefile(source);
end;


function taform_main.dat_entschluesseln(source, dest:string):boolean;
var
s:string;
begin
  p_alt:=1;
  s:=edit_p.text;
  cipher.Tag:=1;
  cipher.InitKey(s,nil);
  Cipher.encodeFile( source,dest);
end;

procedure TaForm_main.FormClose(Sender: TObject; var Action: TCloseAction);
begin
regschreiben;
mysql_d.disconnect;
end;

function sql_datetostr(datum:tdate):string;
var
s_datum:string;
begin
s_datum:=datetostr(datum);
result:=copy(s_datum,7,4)+'-'+copy(s_datum,4,2)+'-'+copy(s_datum,1,2);
end;

function sql_timetostr(zeit:ttime):string;
var
s_time:string;
begin
s_time:=timetostr(zeit);
result:=s_time;//'"'+copy(s_datum,7,4)+'-'+copy(s_datum,4,2)+'-'+copy(s_datum,1,2)+'"';
end;




procedure TaForm_main.BitBtn_restoreClick(Sender: TObject);
begin
  Timer_autbackup.Enabled:=false;

	wiederherstellen;

  Timer_autbackup.Enabled:=true;
end;

procedure TaForm_main.wiederherstellen;
var
	dbname,dirnew,serv: string;
	cmd_path,befehl,parameter, pm, mypfad,backuppfad,backupdatei,backupdatei_c,backupdatei_e,d:string;
	//f,f_c:textfile;
	dat,s,s1,w_pfad,ss:string;
   dbversion,i,p,dg:integer;
   k:boolean;
begin
abbruch:=false;
dbname:=trim(edit_neu.text);
showserver;
dbversion:=combobox_mv.itemindex;
if dbname ='' then
begin
	showmessage('Bitte den neuen Datenbanknamen eingeben.');
   exit;
end;



if directoryexists(edit_data.text+dbname) then
begin
   if checkbox_ren.Checked then
   begin
      try
         Datenbankinitialisieren1Click(Self);
         dirnew:=dbname+sql_datetostr(now())+'_'+timetostr(now());
         dirnew:=stringreplace(dirnew,'.','_', [rfreplaceall]);
         dirnew:=stringreplace(dirnew,':','_', [rfreplaceall]);
         dirnew:=edit_data.text+dirnew;
         renamedir(edit_data.text+dbname,dirnew);
         showmessage (edit_data.text+dbname + ' wurde umbenannt in : ' +dirnew );
       except
       	showmessage('Die Datenbank l�sst sich nicht umbenennen, PC neu booten oder neuen Namen w�hlen.');
      	exit;
       end;

   end
   else
   begin
      showmessage('Die Datenbank besteht schon bitte anderen Namen verwenden');
      exit;
   end;
end;
if directoryexists(edit_data.text+dbname) then
begin
      showmessage('Die Datenbank besteht schon bitte anderen Namen verwenden oder das Verzeichnis '+edit_data.text+dbname+' umbenennen.');
      exit;
   end;
backupdatei:=trim(edit_restore.Text);

if copy(backupdatei,length(backupdatei)-5,6)='_crypt' then
begin
   memo_wdh.Lines.Add('Entschl�sseln von '+backupdatei);
   memo_wdh.Lines.Add('');
   backupdatei_e:=copy(backupdatei,1,length(backupdatei)-6);
   dat_entschluesseln(backupdatei, backupdatei_e);
   backupdatei:=backupdatei_e;
 
end;

if not fileexists(backupdatei) then
begin
	showmessage('Die angegebene Datei existiert nicht');
   exit;
end;

if dateiendung(backupdatei)<>'sql' then
begin
	showmessage('Die Dateiendung muss "sql" sein');
   exit;
end;


test_pfade;

backupdatei_c:= stringwenden(backupdatei);
backupdatei_c:=copy(backupdatei_c,5,length(backupdatei_c));
backupdatei_c:= stringwenden(backupdatei_c);
//if checkc.Checked then backupdatei_c:=backupdatei_c+'.sql' else backupdatei_c:=backupdatei_c+'_c.sql';
backupdatei_c:=backupdatei_c+'_c.sql';
if fileexists (backupdatei_c) then  DeleteFile(backupdatei_c);

{if checkc.checked then
begin
if not fileexists (backupdatei_c) then
begin
  showmessage('keine konvertierte Datei vorhanden');
  checkc.checked:=false;
  exit;
end; 
end;}



mypfad:=trim(edit_bin.Text);

//datenbank muss laufen und Pfade stimmen




// datei anpassen
// datenbankname einf�gen:

//drop database if exists dbname
//set names latin1;
//CREATE DATABASE if not exists `arbene3` character set latin1;
//USE arbene3;
//wenn:    -- Server version	3.23.49-nt  dann  ersetzen "ts timestamp(14) NOT NULL" durch "ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
//if not checkc.checked then
try
	  AssignFile(F_a,backupdatei );
	  Reset(F_a);
     AssignFile(F_c,backupdatei_c );
	  rewrite(F_c);

     if dbversion=1 then  //5.6
     begin
        memo_wdh.Lines.Add('Schreibe Hilfsdatei f�r MariaDB  nach '+backupdatei_c);
        //s:='set names latin1;';
        s:='/*!40101 SET NAMES latin1 */;' ;
        Writeln (F_c,S);
        //s:=format('CREATE DATABASE if not exists %s character set latin1;',[dbname]);
        s:=format('CREATE DATABASE if not exists `%s` /*!40100 COLLATE "latin1_german2_ci" */;',[dbname]);
        Writeln (F_c,S);
        s:=format('use %s;',[dbname]);
        Writeln (F_c,S);
        Writeln (F_c,'');
     end
     else
     begin
			memo_wdh.Lines.Add('Schreibe Hilfsdatei f�r mysql nach '+backupdatei_c);

        s:=format('CREATE DATABASE  %s ;',[dbname]);
        Writeln (F_c,S);
        s:=format('use %s;',[dbname]);
        Writeln (F_c,S);
        Writeln (F_c,'');
     end;

     timer_wdh.Enabled:=true;
     //repeat
     // Readln(F_a, S);
     //until (pos('DROP TABLE',s)>0) or eof(f_a) ;   //hier ende

     //Writeln (F_c,S);
     memo_wdh.Lines.Add('');
     memo_wdh.Lines.Add('');
     i:=0;

     if dbversion=1 then
     begin    //mariadb
        while not eof(f_a) do   //hier kopieren
        begin
           application.ProcessMessages;
           Readln(F_a, S);
            if lowercase(copy(s,1,6))<>'insert' then
             begin
                if pos('timestamp(14)',s)>0 then s:='ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,' ;
                if pos('TYPE=MyISAM',s)>0 then s:=stringreplace(s,'TYPE=MyISAM','ENGINE=ARIA',[rfreplaceall]);
                if pos('ENGINE=MyISAM',s)>0 then s:=stringreplace(s,'ENGINE=MyISAM','ENGINE=ARIA',[rfreplaceall]);
                if (pos('text,',s)>0) and (pos('_text,',s)=0) then s:=stringreplace(s,'text,','TEXT Null,',[rfreplaceall]);
                if pos('text NOT NULL,',s)>0 then s:=stringreplace(s,'text NOT NULL,','TEXT Null,',[rfreplaceall]);
                if pos('KEY Primary Key',s)>0then  s:=stringreplace( s,'KEY Primary Key','Primary Key',[rfreplaceall]);
                p:=pos('PAGE_CHECKSUM=1',s);
                if p >0 then
                begin
                  insert( ' COLLATE="latin1_german2_ci" ',s,p);// s:=stringreplace(s,'TYPE=MyISAM','ENGINE=ARIA',[rfreplaceall]);
                 //s:=stringreplace(s,'text NOT NULL,','TEXT Null,',[rfreplaceall]);
              // COLLATE='latin1_german1_ci'
                end;
             end;
           if (copy(s,1,2))<>'--' then Writeln (F_c,S); //kommentar

           inc(i);
           application.ProcessMessages;
        end;
     end
     else
     begin
        while not eof(f_a) do
        begin
          application.ProcessMessages;
           Readln(F_a, S);
           if lowercase(copy(s,1,6))<>'insert' then
           begin
              if pos('KEY Primary Key',s)>0then  s:=stringreplace( s,'KEY Primary Key','Primary Key',[rfreplaceall]);
            end;
           Writeln (F_c,S);
           inc(i);
           application.ProcessMessages;
        end;
     end;


	finally
	  CloseFile(F_a);
     CloseFile(F_c);
     s:=format('geschrieben: %d Prozent',[100]);
     memo_wdh.Lines.Delete(memo_wdh.Lines.count-1);
     memo_wdh.Lines.Append(s);
     memo_wdh.Lines.Append('Anzahl der Zeilen: '+inttostr(i));
     //label_wdh.Caption:=s;
     memo_wdh.Lines.Add('Hilfsdatei ist gechrieben');
     timer_wdh.Enabled:=false;
    	application.BringToFront;
      application.ProcessMessages;
	end;

  dg:=getfilesize(backupdatei_c);
   if dg<getfilesize(backupdatei) div 2 then
   begin
    showmessage ('Das Entschl�ssen bzw. Umwandeln ist fehlgeschlagen, vermutlich ist der Schl�ssel falsch.');
    exit;
   end;

   if checkbox_p.Checked then
    wiederherstellen_p(backupdatei_c)  //protokolliert
   else
   begin
      backupdatei:=stringreplace(backupdatei,'\','/', [rfreplaceall]);
      befehl:=command;
      parameter:=format('/C "%smysql.exe" --user=Arbene --password=nixreinkommen  < %s',[mypfad,  backupdatei_c]) ;
      pm:=format('/C "%smysql.exe" --user=Arbene --password=xxxxxxx  < %s',[mypfad,  backupdatei_c]) ;
      memo_wdh.Lines.Add('Datenbank wird jetzt erstellt');
      memo_wdh.Lines.Add('Das schwarze Fenster nicht schlie�en');
      memo_wdh.Lines.Add(pm);
      shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);
      //shellexecute(aform_main.handle,'runas',pchar(befehl),pchar(parameter),nil,SW_normal);
      memo_wdh.Lines.Add('Wiederherstellung  der Datenbank wurde angesto�en');
      memo_wdh.Lines.Add('Bitte abwarten, bis das DOS-Fenster geschlossen wurde. (C:\windows\system\cmd.exe)');
      memo_wdh.Lines.Add('Sollte das DOS-Fenster sich nur kurz �ffnen (Kein Erfolg) starten Sie die Datenbanktools als Administrator!');
   end;

end;

procedure TaForm_main.wiederherstellen_p (datei:string);
var
dbname,s,query,sma,snr,tabelle :string;
n,fc,p,p2:integer;
begin
button_abbruch.Visible:=true;
BitBtn_stopClick(self);
dbname:=trim(edit_neu.text);
 ForceDirectories(edit_data.text+dbname);
 edit_db.text:=dbname;
 verbinden_mit_db;
 AssignFile(F_c,datei );
 Reset(F_c);
 n:=0;
 fc:=0;
 memo_fehler.lines.Clear;
 while ((not eof(f_c)) and (not abbruch)) do
 begin
            //set names latin1;
            //CREATE DATABASE if not exists peg_maria character set latin1;
            //use peg_maria;
           //while script.lines.Count>6 do script.lines.Delete(0);
        try
           application.ProcessMessages;
           Readln(F_c, S);
           inc(n);
           script.Lines.Clear;
           script.Lines.Append(inttostr(n)+': ');
           script.Lines.Append('');

  

           datamodule1.ZSQLProcessor_db.Script.Clear;
           if not ((copy(s,1,2)='--')
           			or(copy(s,1,2)='/*')
                  or(copy(s,1,15)='CREATE DATABASE')
                    or(copy(s,1,3)='use')) then
           datamodule1.ZSQLProcessor_db.Script.Append(s);

           application.ProcessMessages;
           if (copy(s,1,12)='CREATE TABLE') then
           repeat
             Readln(F_c, S);
             inc(n);
             datamodule1.ZSQLProcessor_db.Script.Append(s);

           until ((pos(';',s)>0) or eof(f_c)) ;
           script.Text:=script.Text+ datamodule1.ZSQLProcessor_db.script.Text;
           application.ProcessMessages;

           if datamodule1.ZSQLProcessor_db.script.Count>0 then datamodule1.ZSQLProcessor_db.Execute;
           script.Lines.Append('');
           script.Lines.Append('*erfolgt*');
           application.ProcessMessages;
        except
          inc(fc);
          memo_fehler.lines.Add('Fehler in Zeile: '+inttostr(n));
          memo_fehler.lines.Add('nicht ausgef�hrt: '+datamodule1.ZSQLProcessor_db.script.Text);
          query:=datamodule1.ZSQLProcessor_db.script.Text;
          p:=pos('insert',lowercase(query));
          if p >0 then
          begin
            insert (' ignore ',query,p+6);
            datamodule1.ZSQLProcessor_db.Script.Clear;
            datamodule1.ZSQLProcessor_db.Script.Append(query);
            try
             if datamodule1.ZSQLProcessor_db.script.Count>0 then datamodule1.ZSQLProcessor_db.Execute;
              memo_fehler.lines.Add('korrigiert ausgef�hrt: '+datamodule1.ZSQLProcessor_db.script.Text);
               {p:=pos('into',lowercase(query));  //tabelle mitarbeiter noch nicht da
                p2:=pos('values',lowercase(query));
               tabelle:=trim(copy(query,p+4,p2-p-4));
              p:=pos('(',query);
              p2:=pos(',',query);
              snr:=trim(copy(query,p+1,p2-p-1));
              query:=format('select i_mitarbeiter from mitarbeiter where nummer=%s',[snr]);
              try
                datamodule1.q1.SQL.text:=query;
                datamodule1.q1.ExecSQL;
              except
                memo_fehler.lines.Add(datamodule1.q1.findfield('name').asstring+ ' '+ datamodule1.q1.findfield('vorname').asstring+ ' '+datamodule1.q1.findfield('geb_dat').asstring);
              end; }


            except
              memo_fehler.lines.Add('nicht ausgef�hrt'+datamodule1.ZSQLProcessor_db.script.Text);
            end;
          end;
          memo_fehler.lines.Add('############################################');
          //showmessage('SQL-Fehler in Zeile '+inttostr(n));
        end;
 end;


  button_abbruch.Visible:=false;

  CloseFile(F_c);
  script.Lines.Append('');
  script.Lines.Append('Anzahl der Zeilen: '+inttostr(n));
  script.Lines.Append('Anzahl der Fehler: '+inttostr(fc));
  script.Lines.Append('Die Wiederherstellung ist abgeschlossen.');
end;

procedure TaForm_main.wiederherstellen_alt;
var
cmd_path,datname, befehl,parameter,v_neu,backup_pfad:string;
myindex,p,i:integer;
f : file;
t:ttime;
begin
  if edit_restore.text='' then
  begin
       showmessage('bitte Datei ausw�hlen');
       exit;
  end;
  memo_wdh.Lines.Clear;
  backup_pfad:=edit_data.text+edit_db.text+'\';

  if DirectoryExists(backup_pfad) then
  begin
    v_neu:=edit_db.text+datetostr(date)+'_'+timetostr(time);
    //v_neu:=edit_data.text+'Arbene333\';
    v_neu:=stringreplace(v_neu,'.','_', [rfreplaceall]);
    v_neu:=stringreplace(v_neu,':','_', [rfreplaceall]);
    v_neu:=edit_data.text+v_neu;
    assignfile(f,backup_pfad);
    try
    rename(  f, v_neu);
    except
       showmessage('Datenbank beenden und neu starten');
       exit;   
    end;
     memo_wdh.Lines.add('Verzeichnis "'+edit_data.text+edit_db.text+'" in '+v_neu+ ' umgenannt')
  end;

	if DirectoryExists(backup_pfad) then
	begin
	     showmessage ('Bitte das Verzeichnis '+edit_data.text+edit_db.text+' umbenennen / l�schen.');
       exit;
  end;

  ForceDirectories(backup_pfad);
  memo_wdh.Lines.add('Verzeichnis "'+edit_data.text+edit_db.text+'" erzeugt');
  backup_pfad:=sql_pfad(backup_pfad);
  memo.clear;
 	unzipfile( edit_restore.text,backup_pfad,memo);
  for i:=0 to memo.lines.count-1 do
  memo_wdh.Lines.Add(memo.lines[i]);
  memo_wdh.Lines.Add('');
  memo_wdh.Lines.Add(backup_pfad+' wurde wieder hergestellt');
  memo_wdh.Lines.Add('');

end;

function TaForm_main.dateiendung(datei: string) : string;
var
position: integer;
begin
	result:='';
	position:=pos('.',datei);
	while position>0 do
	begin
		datei:=copy(datei,position+1,length(datei));
     result:=datei;
     position:=pos('.',datei);
	end;
end;



procedure TaForm_main.MaskEdit_zeitChange(Sender: TObject);
begin
try
 strtotime(maskedit_zeit.text)
except
 maskedit_zeit.text:='00:00'
end;
end;

procedure TaForm_main.Button_repairClick(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin
Button_optClick(Sender);
reptable;

end;

procedure TaForm_main.Button_db_herunterfahrenClick(Sender: TObject);
begin
db_herunterfahren;


end;

procedure TaForm_main.Button_db_startenClick(Sender: TObject);
begin
db_starten;

end;

procedure TaForm_main.Button_s3Click(Sender: TObject);
var
cmd_path,befehl,parameter,db:string;
begin

befehl:= {trim(edit_cmd.text)+}command;
db:=edit_db.text;
parameter:=format('/C "%smyisamchk.exe" -o -f %s%s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);

    case combobox_mv.ItemIndex of
       0:  parameter:=format('/C "%smyisamchk.exe" -o -f %s%s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);
       1: parameter:=format('/C "%saria_chk.exe" --safe-recover --zerofill --page_buffer_size=2G %s%s\*.mai',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);
    end;


    //parameter:=format('/C "%saria_chk.exe" -o -f %s%s\*.mai',
		//			  [trim(edit_bin.Text),trim(edit_data.Text),db]);
    //aria_chk --safe-recover --zerofill --page_buffer_size=2G */*.MAI
shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

end;

procedure TaForm_main.Button_s2Click(Sender: TObject);
var
cmd_path,befehl,parameter:string;
begin

befehl:= {trim(edit_cmd.text)+}command;
parameter:=format('/C "%smyisamchk.exe" -r -f %s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text)]);

shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

end;

procedure TaForm_main.Button6Click(Sender: TObject);
var
cmd_path,befehl,parameter,db:string;
begin

befehl:= {trim(edit_cmd.text)+}command;
db:=edit_db.text;
parameter:=format('/C "%smyisamchk.exe" -r -n -f %s%s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);

case combobox_mv.ItemIndex of
   0: parameter:=format('/C "%smyisamchk.exe" -r -n -f %s%s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);
   1: parameter:=format('/C "%smyisamchk.exe" -r -n -f %s%s\*.mai',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);
end;
shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);


end;

procedure TaForm_main.SchreibeBackup1Click(Sender: TObject);
begin
//erzeuge_backup;
//dumpdb(edit_db.text);

backup;
loesche_backup;
show_status;
end;

procedure TaForm_main.Datenbankinitialisieren1Click(Sender: TObject);
var
t:ttime;
query:string;
begin
 try
	query:='delete from pcs'; //tabelle pcs mit den angeschlossenen pcs l�schen
	datamodule1.ZSQLProcessor_db.script.Clear;
	datamodule1.ZSQLProcessor_db.Script.Add(query);
	datamodule1.ZSQLProcessor_db.Execute;
  statusbar.Panels[0].text:='Bitte warten!';
 except
  //
 end;
db_herunterfahren;

t:=time;
while time<t+0.0001 do
begin
application.ProcessMessages;
end;


db_starten;

application.ProcessMessages;

 t:=time();
 while t+0.0001 >time() do
 begin
 application.ProcessMessages;
end;
if timer.Enabled then statusbar.Panels[0].text:='Automatisches Backup ist aktiv';
end;

procedure TaForm_main.Beenden1Click(Sender: TObject);
begin
aform_main.close;
end;

procedure TaForm_main.BitBtn_wahlClick(Sender: TObject);
var
pfad,t:string;
position:integer;
begin

  if opendialog.Execute=true then
  begin

      edit_restore.Text:=aform_main.OpenDialog.FileName;
   end;
   t:= copy(edit_restore.Text,length(edit_restore.Text)-5,6);
  if t='_crypt' then
  begin
    label_p.Visible:=true;
    edit_p.Visible:=true;
    edit_p.text:= edit_passwort.text;
  end
  else
  begin
    label_p.Visible:=false;
    edit_p.Visible:=false;
  end;
end;

procedure TaForm_main.Timer_backTimer(Sender: TObject);
begin
timer_back.Enabled:=false;
//dumpdb(trim(edit_db.text));
backup;
loesche_backup;
show_status;
//aform_main.close;
end;


function TaForm_main.wurzelpfad(pfad: string): string;
var
back: integer;
begin
pfad:=copy(pfad, 1, length(pfad)-1);
pfad:=stringwenden(pfad);
back:=pos('\', pfad);
pfad:=copy(pfad, back+1, length(pfad));
result:=stringwenden(pfad);
if  pos('\', result)=0 then result:=result+'\';
end;

function TaForm_main.stringwenden(wort: string): string;
var
 laenge, i: integer;
begin
laenge:=length(wort);
result:='';
for i:=laenge downto 1 do
begin
	result:=result+copy(wort,i,1);
end;
end;

procedure taForm_main.reptable;
var i:integer;
query,datei,pfad:string;
	l:tstringlist;
	p:string;
begin
try

	  pfad:=edit_data.text+edit_db.text+'\';
	  //pfad:='d:\mysql\data\arbene\';

	  filelistbox.Directory:=pfad;

    Labe_rep_h.Visible:=true;

	  for i:=0 to filelistbox.Items.count-1 do
	  begin
			datei:=filelistbox.Items[i];
			datei:=copy(datei,1,length(datei)-4);
			query:='repair table ' + datei +' EXTENDED';
			label_rep.Caption:=query;
			application.processmessages;
		  //query:='repair table mitarbeiter'  ;
			datamodule1.ZSQLProcessor_db.script.Clear;
			datamodule1.ZSQLProcessor_db.Script.Add(query);
			datamodule1.ZSQLProcessor_db.Execute;
        //q_1.SQL.Text:=query;
        //q_1.ExecSQL;
    end;
    Labe_rep_h.Visible:=false;
    label_rep.Caption:='Reparatur abgeschlossen';
finally
      // connection_main.Disconnect;
end;
end;





procedure TaForm_main.Button_optClick(Sender: TObject);
var i:integer;
query,datei,pfad:string;
	l:tstringlist;
	p:string;
begin
try
   //l:=tstringlist.Create;
	  pfad:=edit_data.text+edit_db.text+'\';
	  filelistbox.Directory:=pfad;
    {case combobox_mv.ItemIndex of
       0:  filelistbox.Mask:='*.myd';
       1: filelistbox.Mask:='*.mai';
    end; }
   filelistbox.Mask:='*.frm';
   Labe_rep_h.Visible:=true;
    filelistbox.Refresh;
   	for i:=0 to filelistbox.Items.count-1 do
	  begin
			datei:=filelistbox.Items[i];
			datei:=copy(datei,1,length(datei)-4);
			query:='OPTIMIZE TABLE ' + datei ;
      //l.Clear;
      //l.Append(query);
			label_rep.Caption:=query;
      application.processmessages;
		        //query:='repair table mitarbeiter'  ;
      try
        datamodule1.ZSQLProcessor_db.Script.Clear;
        datamodule1.ZSQLProcessor_db.script.Add(query);
        //hier thread starten, der nach 1 min die DB herunterf�hrt 
        datamodule1.ZSQLProcessor_db.Execute;
        
      except
        showmessage(query +' konnte nicht ausgef�hrt werden');
      end;
      
    end;
    Labe_rep_h.Visible:=false;
    label_rep.Caption:='Optimierung abgeschlossen';
finally
    //l.Free;
      // connection_main.Disconnect;
end;

end;

procedure TaForm_main.Button9Click(Sender: TObject);

var i:integer;
query,dat,pfad:string;
	l:tstringlist;
	p:string;
begin

if messagedlg('Achtung der folgende Schritt ver�ndert das System unwiederruflich',mtConfirmation, [mbabort,mbok] ,0)=mrabort then exit;

try


			query:='update haupt_tabelle set haupt=1' ;

			datamodule1.ZSQLProcessor_db.script.Clear;
			datamodule1.ZSQLProcessor_db.Script.Add(query);
			datamodule1.ZSQLProcessor_db.Execute;
        //q_1.SQL.Text:=query;
        //q_1.ExecSQL;

    showmessage ('das System ist jetzt ein Haupt-System, Bitte dort den Nummernkreis auf 1 zur�ckstellen');
finally
      // connection_main.Disconnect;
end;

end;






procedure TaForm_main.db_herunterfahren;
var
cmd_path,befehl,parameter:string;
begin
try
  memo.Lines.Add('Datenbank-Server wird beendet');
  memo.Lines.Add('');
  befehl:= command;
  if betriebssystem=VER_PLATFORM_WIN32_NT then
      begin
       befehl:='net.exe';
         if combobox_mv.ItemIndex=0 then parameter:='stop mysql' else parameter:='stop mysql' ;
      end
  else
    begin
       if not fileexists(trim(edit_bin.Text)+'mysqladmin.exe') then
       begin
            showmessage('Pfad f�r die MySql-Programmdateien nicht korrekt - bitte unter Optionen einrichten');
            exit;
       end;
      befehl:= command;

      parameter:=format('/C "%smysqladmin.exe" shutdown',[trim(edit_bin.Text)]);

    end;

  //shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,9	);
  shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,9	);
  verbinden_mit_db;
except
end;

end;

procedure TaForm_main.db_starten;
var
cmd_path,befehl,parameter:string;
begin
try
  memo.Lines.Add('Datenbank wird gestartet');
  memo.Lines.Add('');
  if betriebssystem=VER_PLATFORM_WIN32_NT then
      begin
       befehl:='net.exe';
       if combobox_mv.ItemIndex=0 then parameter:='start mysql' else parameter:='start mysql' ;
      end
  else
    begin
       if not fileexists(trim(edit_bin.Text)+'mysql-opt.exe') then
       begin
            showmessage('Pfad f�r die MySql-Programmdateien nicht korrekt - bitte unter Optionen einrichten');
            exit;
       end;
      befehl:= {trim(edit_cmd.text)+}command;
      parameter:=format('/C "%smysqld-opt.exe"',[trim(edit_bin.Text)]);
    end;

  shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,9	);
  verbinden_mit_db;
except
end;
  mysql_verschluesselung_pruefen;

end;


procedure TaForm_main.verbinden_mit_db;
begin
try
datamodule1.disconnect;
except
end;
case combobox_mv.ItemIndex of
   0: datamodule1.ZConnection_db.Protocol:='mysql';
   1: datamodule1.ZConnection_db.Protocol:='MariaDB-5';
end;
//loadlibrary('libmariadb.dll');
 datamodule1.ZConnection_db.Database:= edit_db.Text;
datamodule1.connect;
test_pfade;
refresh_trigger;
showserver;

end;

procedure TaForm_main.Button_rep3Click(Sender: TObject);
var
cmd_path,befehl,parameter,db:string;
begin

befehl:= {trim(edit_cmd.text)+}command;
db:=edit_db.text;
parameter:=format('/C "%smyisamchk.exe" -r -e -f %s%s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);

case combobox_mv.ItemIndex of
   0: parameter:=format('/C "%smyisamchk.exe" -r -e -f %s%s\*.myi',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);
   1: parameter:=format('/C "%smyisamchk.exe" -r -e -f %s%s\*.mai',
					  [trim(edit_bin.Text),trim(edit_data.Text),db]);
end;
shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);


end;


function TaForm_main.dumpdb(db:string):string;
var
cmd_path,befehl,parameter, dbname, mypfad,backuppfad,backupdatei,d,parameter_anzeige:string;
begin
d:=sql_datetostr(now())+'_'+timetostr(time());

mypfad:=trim(edit_bin.Text);
dbname:=trim(db);
backuppfad:=trim(edit_backup.text);

d:=dbname+'_'+d;
d:=stringreplace(d,'.','_', [rfreplaceall]);
d:=stringreplace(d,':','_', [rfreplaceall]);
d:=stringreplace(d,'-','_', [rfreplaceall]);
backupdatei:=backuppfad+d+'.sql';
if fileexists(backupdatei) then
begin
showmessage('Datei existiert schon - Abbruch');
exit;
end;

{if ((combobox_mv.itemindex=1) and fileexists('c:\my.cnf')) then
begin
   showmessage('die Datei C:\my.cnf wird umbenannt in c:\old_my.cnf');
   RenameFile('c:\my.cnf','c:\old_my.cnf');
   deletefile('c:\my.cnf');
end; }
backupdatei:=stringreplace(backupdatei,'\','/', [rfreplaceall]);
befehl:=command;
if combobox_mv.itemindex=0 then
	//parameter:=format('%s /C %smysqldump.exe --user=Arbene --password=nixreinkommen --add-drop-table --add-locks --all --quick --lock-tables %s > %s',[cmd_path,mypfad, dbname, backupdatei])
  parameter:=format('%s /C %smysqldump.exe --user=Arbene --password=nixreinkommen  --all   %s > %s',[cmd_path,mypfad, dbname, backupdatei])
else  //mariadb
	//parameter:=format('/C "%smysqldump.exe"  --user=Arbene --password=nixreinkommen --default-character-set=latin1 --max-allowed-packet=64M --add-drop-table --add-locks --lock-tables %s > %s',[mypfad, dbname, backupdatei]) ;
  parameter:=format('/C "%smysqldump.exe"  --user=Arbene --password=nixreinkommen --default-character-set=latin1 --max-allowed-packet=64M --skip-extended-insert  %s > %s',[mypfad, dbname, backupdatei]) ;

//parameter:=format('%s /C %smyisamchk.exe -o -f %s%s\*.myi',
//					  [cmd_path,trim(edit_bin.Text),trim(edit_data.Text),db]);
//showmessage (befehl);
//showmessage (parameter);
 parameter_anzeige:=stringreplace(parameter,'nixreinkommen','*********', [rfreplaceall]);
 memo.Lines.Add(befehl +parameter_anzeige);

shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);

result:=backupdatei ;
end;

procedure TaForm_main.RenameDir(DirFrom, DirTo: string);
var
  shellinfo: TSHFileOpStruct;
begin
  with shellinfo do
  begin
    Wnd    := 0;
    wFunc  := FO_RENAME;
    pFrom  := PChar(DirFrom);
    pTo    := PChar(DirTo);
    fFlags := FOF_FILESONLY or FOF_ALLOWUNDO or
              FOF_SILENT or FOF_NOCONFIRMATION;
  end;
  SHFileOperation(shellinfo);
end;

function getFileSize(fn: TFileName): Int64;
var
  sr: TSearchRec;
begin
  if FindFirst(fn, faArchive, sr) = 0 then
  begin
    with sr.FindData do
      Result := Int64(nFileSizeHigh) shl 32 + nFileSizeLow;
    FindClose(sr);
  end
  else
    Result := 0;
end;



procedure TaForm_main.BitBtn_startClick(Sender: TObject);
begin
	timer.Enabled:=true;
	statusbar.Panels[0].text:='Automatisches Backup ist aktiv';
   filelistbox_backup.Directory:= edit_backup.Text;
   filelistbox_backup.update;
   bitbtn_stop.Enabled:=true;
   bitbtn_start.Enabled:=false;
    pagecontrol1.ActivePageIndex:=0;
end;

procedure TaForm_main.BitBtn_stopClick(Sender: TObject);
begin
   bitbtn_stop.Enabled:=false;
   bitbtn_start.Enabled:=true;
	timer.Enabled:=false;

  statusbar.Panels[0].text:='Automatisches Backup angehalten';
end;


procedure TaForm_main.Timer_wdhTimer(Sender: TObject);
var

l1,l2,p:integer;
s: string;
begin
//l1:=filesize( f_c);
//l2:=filesize( f_a);
p:=round(filesize( f_c) / filesize( f_a)*100);
s:=format('geschrieben: %d Prozent',[p]);
memo_wdh.Lines.Delete(memo_wdh.Lines.count-1);
memo_wdh.Lines.Append(s);
application.ProcessMessages;
//label_wdh.Caption:=s;
end;

procedure TaForm_main.ComboBox_mvChange(Sender: TObject);
begin
//statusbar.Panels[1].Text:='Datenbankserver: '+combobox_mv.items[combobox_mv.itemindex];
if combobox_mv.itemindex=0 then button_ini.Visible:=true else button_ini.Visible:=false;
end;

procedure TaForm_main.button_abbruchClick(Sender: TObject);
begin
Abbruch:=true;
end;

procedure TaForm_main.Button_iniClick(Sender: TObject);
var
pfad:string;
  mem : TMemoryStream;
  FilePathA, FilePathB : string;
begin
pfad:=ParamStr(0);
pfad:=ExtractFilePath(pfad);
if ((combobox_mv.itemindex=1) and fileexists('c:\my.cnf')) then
begin
   showmessage('die Datei C:\my.cnf wird umbenannt in c:\old_my.cnf');
   RenameFile('c:\my.cnf','c:\old_my.cnf');
   deletefile('c:\my.cnf');
end;

if ((combobox_mv.itemindex=0) and (not fileexists('c:\windows\my.ini'))) then
begin

  mem := TMemoryStream.Create;
  try
    mem.LoadFromFile(pfad+'my.ini');
    mem.SaveToFile('C:\windows\my.ini');
  finally
    mem.Free;
  end;
   if fileexists('c:\windows\my.ini') then  showmessage('die Datei my.ini wurde nach C:\window\my.ini kopiert') else showmessage('Kopieren von my.ini wurde nach C:\window\my.ini ist fehlgeschlagen');;
   Datenbankinitialisieren1Click(self);
end
else showmessage('Die my.ini datei befindet sich schon im C:\windows -Ordner');

end;

function TaForm_main.GetCurrentVersion: String;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do begin
    Result := 'Version '+IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);

    //Und in Delphi 7 hat man 4 stellige Versionen. Daher kann man ab Delphi 7 noch folgende letzte Result-Zeile hinzuf�gen:
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);

  end;
  FreeMem(VerInfo, VerInfoSize);
end;




function  TaForm_main.showserver:string;
var s:string;
begin
try
  try
    s:='nicht verbunden';
    mysql_d.connect(edit_host.text,'mysql','3306','mysql','Arbene','nixreinkommen');
    s:= mysql_d.Feldinhalt('select version()',0);
  finally
    statusbar.Panels[1].text:='Server: '+s;
    if pos('Maria',s)>0 then combobox_mv.itemindex:=1 else combobox_mv.itemindex:=0;
  end;
except

end;
    result:=s;
end;

procedure  TaForm_main.test_pfade;
var serv:string;
begin
 serv:=showserver;
if pos('Maria',serv)>0 then
begin
  if pos('maria',lowercase(edit_bin.Text))=0 then showmessage('Der Pfad f�r MariaDB\bin ist nicht plausibel');
  if pos('maria',lowercase(edit_data.Text))=0 then showmessage('Der Pfad f�r MariaDB\data ist nicht plausibel');
end;
end;

function DeleteFileWithUndo(sFileName: string): Boolean;
var 
  fos: TSHFileOpStruct; 
begin 
  FillChar(fos, SizeOf(fos), 0); 
  with fos do 
  begin
    wFunc  := FO_DELETE;
    pFrom  := PChar(sFileName); 
    fFlags := FOF_ALLOWUNDO or FOF_NOCONFIRMATION or FOF_SILENT; 
  end; 
  Result := (0 = ShFileOperation(fos)); 
end;

procedure TaForm_main.show_status;
begin
if timer.Enabled  then  statusbar.Panels[0].text:='Automatisches Backup ist aktiv' else statusbar.Panels[0].text:='Automatisches Backup ist angehalten'
end;

procedure TaForm_main.refresh_trigger;
begin
try
  datamodule1.ZQuery_trigger.Active:=false;
  datamodule1.ZQuery_trigger.Active:=true;
  //datamodule1.ZQuery_trigger.refresh;
  if datamodule1.ZQuery_trigger.eof and   datamodule1.ZQuery_trigger.bof then
  begin
  label_trigger.Caption:='Es wird keine Historie aufgezeichnet';
  statusbar.panels[4].Text:='Historie: AUS';
  end
  else
  begin
    label_trigger.Caption:='Die Historie von obigen Tabellen wird aufgezeichnet';
    statusbar.panels[4].Text:='Historie: EIN';
  end;
  
except
end;  


end;

procedure TaForm_main.PageControl1Change(Sender: TObject);
begin
if BitBtn_stop.Enabled then
begin
  BitBtn_stopClick(Sender);
  Timer_autbackup.Enabled:=true;
  showmessage('Achtung, das automatische Backup wurde angehalten.');
end;

if pagecontrol1.ActivePage=tabsheet_history then
begin
  memo_history.Clear;
  refresh_trigger;
end;
if pagecontrol1.ActivePage=tabsheet_dbkrypt then
begin
  if edit_key_pfad.text='' then edit_key_pfad.Text:=edit_data.Text;
end;

end;

procedure TaForm_main.mysqld(datei,info:string);
 var
 befehl,parameter,pm,mypfad,exepfad,db,mw_pfad:string;
 begin
      db:=trim(edit_db.text);
      mypfad:=trim(edit_bin.Text);
      exepfad:=ExtractFilePath(ParamStr(0));
      mw_pfad:=wurzelpfad(mypfad);


      datei:=mw_pfad+'\sql\'+datei;


      if not fileexists(datei) then
      begin
        showmessage('Die Datei '+datei+' ist nicht vorhanden');
        exit;
      end;
      datei:=stringreplace(datei,'\','/', [rfreplaceall]);
      befehl:=command;
      parameter:=format('/C "%smysql.exe" --user=Arbene --password=nixreinkommen --database=%s < %s',[mypfad,db,  datei]) ;
      //parameter:=format('"%smysql.exe" --user=Arbene --password=nixreinkommen --database=%s < %s',[mypfad,db,  datei]) ;
      pm:=format('/C "%smysql.exe" --user=Arbene --password=xxxxxxx --database=%s < "%s"',[mypfad, db, datei]) ;
      memo_history.Lines.Add(info);
      memo_history.Lines.Add(pm);
      shellexecute(aform_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_normal);
   end;

procedure TaForm_main.Button_hist_tab_erzeugenClick(Sender: TObject);
var
t:ttime;
begin

  mysqld('create_tables.sql','Log-Tabellen werden erzeugt');
  t:=time;
  while t+0.00002 >time do;
  mysqld('create_trigger.sql','Trigger werden erzeugt');
   t:=time;
  while t+0.00002 >time do;
  refresh_trigger;
  application.ProcessMessages;

end;

procedure TaForm_main.Button_hist_tab_loeschenClick(Sender: TObject);
var
t:ttime;
begin
  if messagedlg('Achtung, es wird keine weitere Historie aufgezeichnet!',mtConfirmation, [mbabort,mbok] ,0)=mrOK then
    mysqld('drop_trigger.sql','Trigger werden gel�scht')
  else
    exit;

  if messagedlg('Achtung, die bisherige Historie wird gel�scht! ',mtConfirmation, [mbabort,mbok] ,0)=mrOK then
  if messagedlg('Soll die bisherige Historie wirklich gel�scht werden?',mtConfirmation, [mbabort,mbok] ,0)=mrOK then
    mysqld('drop_tables.sql','Log-Tabellen werden gel�scht');

  t:=time;
  while t+0.00002 >time do;
  refresh_trigger;
  application.ProcessMessages;
end;

procedure TaForm_main.FormShow(Sender: TObject);
begin
  refresh_trigger;
  mysql_verschluesselung_pruefen;

end;



procedure TaForm_main.Button12Click(Sender: TObject);
var
i,j,z,r,k:integer;
b:byte;
s : array[1..32] of byte;
begin
randomize;

 edit_key.text:='';
  for i := 1 to 32 do
  begin
    s[i] :=Random(16);
  end;

randomize;
k:=(random(5000000) +500000+random(7000000));
For i:=1 to k do
begin
  j:=random(32);
  s[j]:=Random(16);
end;

for z:= 1 to 32 do
  begin
    b:=s[z];
    edit_key.text:= edit_key.text + inttohex(b,1);
  end;
end;


procedure TaForm_main.edit_key_nameExit(Sender: TObject);

begin
edit_key_name.Text:=cleantext(edit_key_name.Text)+'.txt';

end;



function Taform_Main.CleanText(S: String): String;
var i,p:Integer;
begin
 Result := '';
 s:=lowercase(s);
 s:=stringwenden(s);
 p:=pos('.',s);
 if p>0 then s:=copy(s,p+1,length(s));
 s:=stringwenden(s);
 if Length(S)>0
  then for i := 1 to Length(S)
        do if (Ord(S[i]) in [48..57, 97..122])// >= 97) and (Ord(S[i]) <=122)
            then Result := Result+S[i];
 //Result:=Trim(StringReplace(Result,'"','��',[rfReplaceAll]));
end;

procedure taform_main.keyschreiben(pfad,key:string);
var
	f:textfile;
	dat:string;
  //i:integer;
begin
  try
	//dat:=w_pfad+'\'+application.title+'.ini';
	AssignFile(F,pfad );   {globales }
	rewrite(F);
  writeln(f,key);
    finally
    CloseFile(F);
  end;
end;

procedure TaForm_main.Button13Click(Sender: TObject);
begin
if fileexists( (edit_key_pfad.Text+edit_key_name.Text)) then
begin
  showmessage('Die Schl�sseldatei existiert schon');
  //edit_key_name.text:='';
  exit;
end;
keyschreiben(edit_key_pfad.Text+edit_key_name.Text,'1;'+edit_key.Text);
memo_status.Lines.Add('Schl�sseldatei '+edit_key_pfad.Text+edit_key_name.Text+' wurde erstellt');
end;

procedure TaForm_main.Button10Click(Sender: TObject);
begin
  mysqlini_schreiben(true);
  Datenbankinitialisieren1Click(self);
  mysql_verschluesselung_pruefen;
  memo_status.Lines.Add('Datenbankverschl�sselung wurde aktiviert');
end;
procedure TaForm_main.Button11Click(Sender: TObject);
begin
  mysqlini_schreiben(false);
  Datenbankinitialisieren1Click(self);
  mysql_verschluesselung_pruefen;
  memo_status.Lines.Add('Datenbankverschl�sselung wurde deaktiviert');
end;

procedure taform_main.mysqlini_schreiben(verschluesseln:boolean);
var
	f,n:textfile;
	dat_schluessel,dat_ini,dat_ini_neu,s:string;
  zeilen:array[1..4] of string;
begin
  try

    zeilen[1]:='plugin-load-add=file_key_management.dll';
    zeilen[2]:='file-key-management';
    zeilen[3]:='file_key_management_filename';
    zeilen[4]:='aria-encrypt-tables=1';
    dat_schluessel:=edit_key_pfad.Text+edit_key_name.Text;
    dat_schluessel:=stringreplace(dat_schluessel,'\','/',[rfreplaceall]);
    dat_ini:=edit_data.Text+'my.ini';
    dat_ini_neu:=dat_ini+'neu' ;

    //memo_status.Lines.add('Folgendes wird in die Maria-DB.ini '+dat_ini+' Datei eingetragen');
    AssignFile(F,dat_ini );
    assignfile(n,dat_ini_neu);
    Reset(F);
    rewrite(n);
    while not eof(f) do
    begin
      Readln(F, S);
      if verschluesseln then
      begin
        if pos(zeilen[1],s)>0 then s:=zeilen[1];
        if pos(zeilen[2],s)>0 then s:=zeilen[2];
        if pos(zeilen[3],s)>0 then s:=zeilen[3]+' = '+dat_schluessel;
        if pos(zeilen[4],s)>0 then s:=zeilen[4];
      end
      else
      begin
        if pos(zeilen[1],s)>0 then s:='#'+s;
        if pos(zeilen[2],s)>0 then s:='#'+s;
        if pos(zeilen[3],s)>0 then s:='#'+s;
        if pos(zeilen[4],s)>0 then s:='#'+s;
      end;
      //memo_status.Lines.Add(s);
      writeln(n,s);
    end;
  finally
    CloseFile(F);
    closefile(n);
  end;
  deletefile(dat_ini);
  renamefile(dat_ini_neu,dat_ini);

end;

function taform_main.mysql_verschluesselung_pruefen:boolean;
var
	f,n:textfile;
	dat_schluessel,dat_ini,dat_ini_neu,s:string;
  zeilen:array[1..4] of string;
begin

    result:=false;
    zeilen[1]:='plugin-load-add=file_key_management.dll';
    zeilen[2]:='file-key-management';
    zeilen[3]:='file-key_management-filename';
    zeilen[4]:='aria-encrypt-tables=1';
    dat_ini:=edit_data.Text+'my.ini';

    //memo_status.Lines.add('Folgendes wird in die Maria-DB.ini '+dat_ini+' Datei eingetragen');
    if not fileexists(dat_ini) then
    begin
      showmessage('Der Pfad zu MariaDB\data  ist nicht korrekt (Kartenreiter Datenbankeinstellungen)');

      exit;
    end;
try
    AssignFile(F,dat_ini );
    Reset(F);
    while not eof(f) do
    begin
      Readln(F, S);
      begin
        if (pos(zeilen[1],s)>0) and (pos('#'+zeilen[1],s)=0 ) then result:=true;
        if (pos(zeilen[2],s)>0) and (pos('#'+zeilen[2],s)=0) then result:=result and true;
        if (pos(zeilen[3],s)>0) and (pos('#'+zeilen[3],s)=0) then result:=result and true;
        if (pos(zeilen[4],s)>0) and (pos('#'+zeilen[4],s)=0) then result:=result and true;
      end

    end;
  finally
    CloseFile(F);
  end;
  if result then statusbar.Panels[6].Text:='Datenbank-Verschl�sselung aktiv'
  else statusbar.Panels[6].Text:='Datenbank-Verschl�sselung inaktiv';
end;

procedure TaForm_main.Button_tlClick(Sender: TObject);

var
tn,query:string;
begin
tn:=datamodule1.ZQuery_trigger.findfield('trigger').asstring;
  if messagedlg('Soll der Trigger '+tn+' entfernt werden?',mtConfirmation, [mbabort,mbok] ,0)=mrOK then
  begin
     query:=format('drop trigger if exists %s',[tn]);
     datamodule1.ZSQLProcessor_db.script.Clear;
	    datamodule1.ZSQLProcessor_db.Script.Add(query);
	    datamodule1.ZSQLProcessor_db.Execute;
      refresh_trigger;
  end;


end;

procedure TaForm_main.BitBtn1Click(Sender: TObject);
var
pfad:string;
position:integer;
begin
  form_dbpfadwahl.Edit_uv.Visible:=true;
  form_dbpfadwahl.Button_anlegen.Visible:=true;
  if form_dbpfadwahl.showmodal =mrok then
  begin
       pfad:=form_dbpfadwahl.DirectoryListBox.Directory;
       if copy(pfad,length(pfad),1)<>'\'then pfad:=pfad+'\';
       if lowercase(pfad)='c:\' then showmessage('Der Pfad darf nicht c:\ sein.');
       if pos(' ',pfad)>0 then  showmessage('Leerzeichen sind im Pfad nicht erlaubt');

		  if sender=bitbtn1 then edit_backup.Text:=pfad;
      if sender=bitbtn2 then edit_bin.Text:=pfad;
      if sender=bitbtn3 then edit_data.Text:=pfad;

   end;
  filelistbox_backup.update;
end;

procedure TaForm_main.Button_dbClick(Sender: TObject);
var
pfad,db:string;
position:integer;
ok:boolean;
begin
ok:=true;
  form_dbpfadwahl.Edit_uv.Visible:=false;
  form_dbpfadwahl.Button_anlegen.Visible:=false;
  form_dbpfadwahl.DirectoryListBox.Directory:=edit_data.Text;
  if form_dbpfadwahl.showmodal =mrok then
  begin
      db:=form_dbpfadwahl.DirectoryListBox.Items[form_dbpfadwahl.DirectoryListBox.ItemIndex];
      //db:=lowercase(db);
      pfad:=form_dbpfadwahl.DirectoryListBox.Directory;
      //pfad:=lowercase(pfad);
      if copy(pfad,length(pfad),1)<>'\'then pfad:=pfad+'\';
      if pos(edit_data.Text,pfad)<=0  then
      begin
        showmessage('Der Wurzelpfad muss sein: '+edit_data.text);
        //ok:=false;
      end;
      
       if pos('\',db)>0 then
       begin
         showmessage('Sie haben keine Datenbank ausgew�hlt');
         ok:=false;
       end;
       if pos(' ',db)>0 then
       begin
        showmessage('Leerzeichen sind nicht erlaubt');
        ok:=false;
      end;
       if lowercase(db)='mysql' then
       begin
        showmessage('Die Datenbank mysql ist nicht erlaubt');
        ok:=false;
      end;
		  if ok then
      begin
        edit_db.text:=db;
          verbinden_mit_db;
       end;
   end;


end;



procedure TaForm_main.Timer_autbackupTimer(Sender: TObject);
begin
BitBtn_startClick(Sender);  //alle 24 h pflichtm��ig
end;

procedure TaForm_main.CipherProgress(Sender: TObject; Current,
  Maximal: Integer);
var
s:string;
p:integer;

begin
if maximal=0 then exit;
p:=round(current / maximal*100);
if p>p_alt then
begin
  if cipher.Tag=1 then
  begin
    s:=format('entschl�sselt: %d Prozent',[p]);
    memo_wdh.Lines.Delete(memo_wdh.Lines.count-1);
    memo_wdh.Lines.Append(s);
  end
  else
  begin
    s:=format('verschl�sselt: %d Prozent',[p]);
    memo.Lines.Delete(memo.Lines.count-1);
    memo.Lines.Append(s);
  end;
  p_alt:=p;
  application.ProcessMessages;
end;
end;


function CreateAutorunEntry(const AName, AFilename: string; const AKind: TAutorunKind): Boolean;
var
  Reg: TRegistry;
const
 KEY_WOW64_64KEY = $0100;
 KEY_WOW64_32KEY = $0200;
begin

  Result:=False;
  Reg := TRegistry.create;
  //Reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  try
    if (AKind=akUserRun) or (AKind=akUserRunOnce) then
      Reg.Rootkey:= HKEY_CURRENT_USER
    else
      Reg.RootKey := HKEY_LOCAL_MACHINE;

    case AKind of
     akRun, akUserRun        : Result:=Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', True);
     akRunOnce, akUserRunOnce: Result:=Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\RunOnce', True);
     akRunServices           : Result:=Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', True);
     akRunServicesOnce       : Result:=Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\RunOnce', True);
    end;
    if result then     Reg.WriteString(AName, AFilename)
    else showmessage('konnnte den Eintrag nicht durchf�hren');
  finally
    Reg.Free;
  end;

end;

procedure TaForm_main.Button14Click(Sender: TObject);
begin
try
  CreateAutorunEntry('Arbene_Datenbank_Tools', ParamStr(0), akRunServices);
  statusbar.panels[7].Text:='Autostart aktiv';
except
  showmessage('Datenbanktools als Administrator starten (re. Maustaste im Explorer)');
end;
end;

procedure TaForm_main.Button15Click(Sender: TObject);
begin
try
  createAutorunEntry('Arbene_Datenbank_Tools', '', akRunServices);
  statusbar.panels[7].Text:='';
except
  showmessage('Datenbanktools als Administrator starten (re. Maustaste im Explorer)');
end;
end;

end.


