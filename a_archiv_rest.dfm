object Form_archiv_rest: TForm_archiv_rest
  Left = 595
  Top = 215
  Width = 958
  Height = 675
  Caption = 'Archivieren der nicht eingelesenen Probanden'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object DBGrid1: TDBGrid
    Left = 73
    Top = 73
    Width = 796
    Height = 471
    Align = alClient
    DataSource = DataModul.ds_6
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'f_name'
        Title.Caption = 'Firma'
        Width = 221
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'p_name'
        Title.Caption = 'Name'
        Width = 235
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'p_vorname'
        Title.Caption = 'Vorname'
        Width = 198
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'p_geb_dat'
        Title.Caption = 'Geburtsdatum'
        Width = 87
        Visible = True
      end>
  end
  object Panel_bottom: TPanel
    Left = 0
    Top = 544
    Width = 942
    Height = 93
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 72
      Top = 12
      Width = 347
      Height = 16
      Caption = 'mit OK werden die oben aufgef'#252'hrten Probanden archiviert.'
    end
    object BitBtn1: TBitBtn
      Left = 156
      Top = 39
      Width = 185
      Height = 31
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 598
      Top = 39
      Width = 185
      Height = 31
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 73
    Width = 73
    Height = 471
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 869
    Top = 73
    Width = 73
    Height = 471
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 942
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Label2: TLabel
      Left = 72
      Top = 40
      Width = 675
      Height = 16
      Caption = 
        'Die folgenden Probanden sind evtl. aus dem Betrieb ausgeschieden' +
        ', da sie sich nicht in der Importdatei befanden.'
    end
  end
end
