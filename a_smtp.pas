unit a_smtp;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ComCtrls,  ExtCtrls,a_main, Buttons, a_email, MimeMess ,SMTPsend,POP3send,
  IdSMTP, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdMessageClient, IdPOP3, OleServer, OutlookXP,ComObj,mapi,shellApi;

type
  TForm_smtp = class(TForm)
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StatusBar: TStatusBar;
    Panel2: TPanel;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EditSubject: TEdit;
    ComboBoxAttachedFiles: TComboBox;
    ButtonAddAttachedFile: TButton;
    ButtonClearAttachedFiles: TButton;
	  Memo_to: TMemo;
    ComboBox_adressbuch: TComboBox;
    Label3: TLabel;
    PageControl: TPageControl;
    TabSheet_text: TTabSheet;
    TabSheet_anhang: TTabSheet;
    Memo: TMemo;
    Panel_send: TPanel;
	  RichEdit: TRichEdit;
    BitBtn3: TBitBtn;
    SpeedButton1: TSpeedButton;
    Button_anzeigen: TButton;
	  procedure ButtonAddAttachedFileClick(Sender: TObject);
	  procedure ButtonClearAttachedFilesClick(Sender: TObject);
	  procedure BitBtn1Click(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure ComboBox_adressbuchClick(Sender: TObject);
	  procedure PageControlChange(Sender: TObject);
	  procedure ComboBoxAttachedFilesChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    function send_outlook:boolean;
    function SendMail_mapi(Subject,Mailtext,
                   FromName,FromAdress:string;
                   ToName,ToAdress,
                   AttachedFileName,
                   DisplayFileName:tstringlist;
                   ShowDialog:boolean):boolean;
    procedure Button_anzeigenClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	private
  
	  { Private declarations }
	  sendto:string;
	  function email_ansprechpartner() :string;
	  procedure richedit_laden;
	 function zeile_normieren(zeile:string):string;
  function is_email_adr(zeile:string):boolean;
  function send_smtp(sichtbar:boolean):boolean;
    function send_mapi(sichtbar:boolean):boolean;

	public
	  { Public declarations }
	  mime:TMIMEMess;
	  pop:tpop3send;
	  function send(sichtbar:boolean):boolean;

	  function mime_add_adressen:boolean;
	  function test_connect:boolean;

	end;

var
	Form_smtp: TForm_smtp;

implementation

uses a_data, a_treewahl, a_unt_wahl, a_namsuch, a_richedit_menue;

{$R *.DFM}
//type SMTPError = class( Exception);

procedure TForm_smtp.ButtonAddAttachedFileClick(Sender: TObject);
begin
	 if openDialog1.execute then
	 begin
		ComboBoxAttachedFiles.items.add( OpenDialog1.FileName);
		ComboBoxAttachedFiles.itemIndex := 0;
	 end;
end;

procedure TForm_smtp.ButtonClearAttachedFilesClick(Sender: TObject);
var
i:integer;
begin
	i:=ComboBoxAttachedFiles.ItemIndex;
	ComboBoxAttachedFiles.Items.Delete(i);
	comboboxattachedfiles.Refresh;

end;


function tform_smtp.send(sichtbar:boolean):boolean;
begin
if email_via_mapi then
  send_mapi(sichtbar)
else
  send_smtp(sichtbar);
end;


function tform_smtp.send_mapi(sichtbar:boolean):boolean;
var
Subject,Mailtext,FromName,FromAdress:string;
ToName,ToAdress, AttachedFileName,DisplayFileName:tstringlist;
ShowDialog:boolean ;
i:integer;
begin

 toname:=tstringlist.Create;
 ToAdress:=tstringlist.Create;
  AttachedFileName:=tstringlist.Create;
  DisplayFileName:=tstringlist.create;
{
Aufrufparameter:

Subject: 	  Die Betreffzeile
Mailtext: 	  Der eigentliche Text der eMail
FromName: 	  Der Name des Absenders
FromAdress: 	  Die EMail-Adresse des Absenders
ToName: 	  Der Name des Empf�ngers
ToAdress: 	  Die EMail-Adresse des Empf�ngers
AttachedFileName: Der Dateiname der angeh�ngten Datei
FileDisplayName:  Der in der Mail angezeigte Name der angeh�ngten Datei
ShowDialog:	  true: Die Mail wird vor dem Absenden angezeigt
		  false: Die Mail wird "stumm" verschickt}

try
if not isrunning('outlook.exe') and ProgIDExists('outlook.application') then shellexecute (0,'open','outlook.exe','','',sw_show);
except
end;

try

    subject:=editsubject.Text;
    mailtext:=memo.Text;
    fromname:=email_useraus;
    Fromadress:=email_adresse;
    //for i:=0 to memo_to.Lines.Count-1 do toname.Add(memo_to.Lines[i]);
    //toname.AddStrings(memo_to.Lines);
    for i:=0 to memo_to.Lines.Count-1 do
    if is_email_adr(memo_to.Lines[i]) then
     begin
       toadress.Add(memo_to.Lines[i]);
       toname.Add(memo_to.Lines[i]);
       //showmessage(memo_to.Lines[i]);
      end;

    //toadress.AddStrings(memo_to.Lines);

    for i:=0 to ComboBoxAttachedFiles.Items.Count-1 do  attachedfilename.Add (ComboBoxAttachedFiles.items[i]);
    for i:=0 to ComboBoxAttachedFiles.Items.Count-1 do  DisplayFileName.Add (ComboBoxAttachedFiles.items[i]);



    if sendmail_mapi( subject,mailtext,fromname,fromadress,toname,toadress,attachedfilename,DisplayFileName,sichtbar)
    then
    form_email.addmail(now(),memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items)
    else
    form_email.addmail(0,memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items) ;
finally
 toname.Free;
 ToAdress.Free;
  AttachedFileName.Free;
 DisplayFileName.Free;
end;
end;

function tform_smtp.send_smtp(sichtbar:boolean):boolean;

procedure keineverbindung;
begin
	showmessage('Keine Verbindung zum Mail-Server');
	if sichtbar then form_email.addmail(0,memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items);
	result:=false;
end;


var
i:integer;
begin
	 result:=false;

	 if sichtbar then
	 begin

	 end;
	 //form_email.popini;
	 screen.cursor := crHourGlass;

	 if  not mime_add_adressen then exit;

	 if trim(sendto)='' then
	 begin
		showmessage('Mail kann nicht verschickt werden -keine korrekte email-adresse vorhanden');
		exit;
	 end;
	 statusbar.Panels[0].text:='Verbindung POP3-Server';
	 application.ProcessMessages;
	 result:=test_connect;
	 if not result then
	 begin
		keineverbindung;
		exit;
	 end;
		 statusbar.Panels[0].text:='Mail vorbereiten';
	 application.ProcessMessages;
	 mime.Clear;
	 mime.PartList.clear;
	 mime.Header.Subject := EditSubject.text;
	 mime.Header.From:= email_useraus;//'arbene';
	 mime.Header.ToList.Add(sendto);// :=sendto;
	 mime.Header.Date:=now();
	 //mime.Header.CCList.Add('');

	 mime.AddPartText(tstringlist(memo.lines));

	 for i:=0 to ComboBoxAttachedFiles.Items.Count-1 do  mime.AddPartBinary(ComboBoxAttachedFiles.Items[i]);
	 mime.EncodeMessage;
	 mime.FinalizeHeaders;

	 statusbar.Panels[0].text:='Versenden';
	 application.ProcessMessages;


     //function SendToRaw(const MailFrom, MailTo, SMTPHost: string;
     //const MailData: TStrings; const Username, Password: string): Boolean;  email_userein, email_passwortein
   result:=false;
	 if SendToRaw(email_name, sendto, email_smtp, mime.Lines, '', '') then //email_useraus
	 begin
		  //	form_email.Sb.Panels[0].Text := '';
			result:=true;
			if sichtbar then form_email.addmail(now(),memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items)
			else
				begin
				datamodul.q_email.edit;
				datamodul.q_email['datum']:=now;
				datamodul.q_email.post;
				end;
		end
		else
    begin
    form_email.addmail(0,memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items);
    showmessage ('konnte mail nicht verschicken Problem beim Mailausgang');
    end;
	 screen.cursor := crDefault;
	 statusbar.Panels[0].text:='Versenden abgeschlossen';
end;


function tform_smtp.mime_add_adressen:boolean;
var
i:integer;
zeile:string;
begin
result:=true;
sendto:='';
 i:=0;
	 while i< memo_to.Lines.Count do
	 begin
		zeile:=memo_to.Lines[i];
		zeile:=zeile_normieren(zeile);
		if zeile <>'' then
		begin
			if is_email_adr(zeile) then sendto:=SendTo+zeile+ ' , '
			else
			begin
				showmessage('"'+zeile +'" ist keine korrekte email-Adresse');
				result:=false;
			end;
			inc(i);
		end
		else
			 memo_to.lines.Delete(i);
	end;
	if memo_to.lines.count=0 then
	begin
		showmessage('Keine g�ltige email-Adresse vorhanden');
		result:=false;
	end;

end;

procedure TForm_smtp.BitBtn1Click(Sender: TObject);
var
i:integer;
begin
 for i:=0 to memo_to.Lines.Count-1 do
    if not is_email_adr(memo_to.Lines[i]) then  memo_to.Lines.Delete(i);
 	panel_send.Visible:=true;
	if send(true) then modalresult:=mrok
	else
	begin
	showmessage('Die mail konnte nicht verschickt werden, verschicken Sie sie aus der Mailbox.');
	modalresult:=mrcancel;
	end;
	panel_send.Visible:=false;
end;

procedure TForm_smtp.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
ComboBox_adressbuch.ItemIndex:=0;
sendto:='';
mime:=TMIMEMess.create;
pop:=tpop3send.create;
end;

function TForm_smtp.email_ansprechpartner() :string;
var
	sql_felder,query: string;
	i,j:integer;
	knoten: ttreenode;
	daten:pnodepointer;
	nummer,nummer_first:int64;
begin
with datamodul do
try
	 sql_felder:='firma_ansprech.nummer as nummer, firma_ansprech.name as name, firma_ansprech.email as email, firma.firma as firma';
	 query:=format('select %s from firma_ansprech left join firma on (firma.nummer=firma_ansprech.i_firma) where (firma_ansprech.email <> "" and firma_ansprech.i_firma in (%s) ) and firma_ansprech.storno="" order by firma.firma, name',[sql_felder,firmen_berechtigung]);
	 sql_new(false,q_1,query,'');

//	 sql_new(true,q_1,'select '+sql_felder+ ' from firma_ansprech left  join firma on (firma.nummer=firma_ansprech.i_firma) where (firma_ansprech.email <> "" and firma_ansprech.i_firma in () )  order by firma.firma ','');
	 form_treewahl:=tform_treewahl.create(self);
	 form_treewahl.multiselect:=true;
	 form_treewahl.PopupMenu.AutoPopup:=false;
	 form_treewahl.treeview.dragmode:=dmmanual;
	 form_treewahl.treeview.ReadOnly:=true;
	 form_treewahl.treeview.mysqlquery:=q_1;
	 form_treewahl.treeview.mysql_feld:='name';
	 form_treewahl.treeview.liste_lesen_parent('firma','name');
	 if form_treewahl.showmodal<>mrok then exit;
	 //editto.Text:=q_1['email'];

	if form_treewahl.Treeview.Selected=nil then exit;
	knoten :=form_treewahl.Treeview.selected;
	daten:=knoten.Data;
	nummer_first:=daten^.nummer;
	j:=0;
	for i:=0 to  form_treewahl.Treeview.Items.Count-1 do
	begin
		if form_treewahl.Treeview.Items[i].stateindex>0 then //ausgew�hlt
		begin
		 knoten :=form_treewahl.Treeview.items[i];
		 daten:=knoten.Data;
		 nummer:=daten^.nummer;
		 //if q_1.Locate('nummer',inttostr(nummer),[]) then
     if q_1.Locate('nummer',(nummer),[]) then
		 begin
			 memo_to.Lines.Insert(0,q_1['email']);
			 inc(j);
		  end;
		end;
	end;
	if j=0 then
		begin
		 //if q_1.Locate('nummer',inttostr(nummer_first),[]) then result:=q_1['email'];
     if q_1.Locate('nummer',(nummer_first),[]) then result:=q_1['email'];
		end;

finally
	form_treewahl.release;
end;
end;



procedure TForm_smtp.ComboBox_adressbuchClick(Sender: TObject);
begin

form_namsuch.edit1.text:='';
case combobox_adressbuch.ItemIndex of
		0:begin
			form_namsuch.CheckBox_email_f.Checked:=true;
			form_namsuch.CheckBox_email_p.Checked:=false;

        if form_namsuch.showmodal= mrok then
        memo_to.Lines.insert(0,datamodul.q_mitarbeiter_such['email_f']);
		 end;
     1:begin
      form_namsuch.CheckBox_email_f.Checked:=false;
      form_namsuch.CheckBox_email_p.Checked:=true;
      if form_namsuch.showmodal= mrok then
        memo_to.Lines.insert(0,datamodul.q_mitarbeiter_such['email_p']);
      end;
		2: memo_to.Lines.Insert(0,email_ansprechpartner);
     3:	email_von_ma_abt(	memo_to.Lines,getbigint(datamodul.q_mitarbeiter,'nummer'));
end;
end;

procedure TForm_smtp.PageControlChange(Sender: TObject);
begin
if pagecontrol.ActivePage=tabsheet_anhang then richedit_laden;
end;

procedure tform_smtp.richedit_laden;
var dat:string;
    i:integer;
begin
     i:= ComboBoxAttachedFiles.itemindex;
		if i<0 then exit;
		dat:=ComboBoxAttachedFiles.Items[i];
		if not fileexists(dat) then exit;
		richedit.Lines.Clear;

		if form_main.dateiendung(dat)='rtf' then
		  richedit.Lines.LoadFromFile(dat)
		else richedit.Lines.Add('Keine RTF-Datei');
end;

procedure TForm_smtp.ComboBoxAttachedFilesChange(Sender: TObject);
begin
   richedit_laden;
end;

procedure TForm_smtp.BitBtn3Click(Sender: TObject);
begin
 form_email.addmail(0,memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items);
 modalresult:=mrok;
end;

procedure TForm_smtp.FormDestroy(Sender: TObject);
begin
mime.free;
pop.free;
end;


function tform_smtp.test_connect:boolean;
var s:string;
begin
	 result:=true;
	 if email_pop+email_userein='' then exit;
	 pop.Password:=email_passwortein;
	 pop.Username:= email_userein;
	 pop.POP3Host:=email_pop;
	 pop.Timeout:=10000;
	 result:= pop.Login;
	 pop.Logout;
end;

function tform_smtp.zeile_normieren(zeile:string):string;
 var
	 position:integer;
 begin
		position:=pos(',',zeile);
		 while position >0 do begin
		  delete(zeile,position,1);
		  insert(' ',zeile,position);
		  position:=pos(' ',zeile);
		 end;
		result:=trim(zeile);
 end;

function tform_smtp.is_email_adr(zeile:string):boolean;
var p:integer;
begin
	result:=false;
	p:=pos('@',zeile);
	if p<1 then exit;
	zeile:=copy(zeile,p+1,length(zeile));
	p:=pos('.',zeile);
	if p<2 then exit;
	zeile:=copy(zeile,p+1,length(zeile));
	if length(zeile)=0 then exit;
	result:=true;
end;

function tform_smtp.send_outlook:boolean;  //wird nicht verwendet
const olMailItem = 0;
var Outlook: OLEVariant;
     MailItem: Variant;
begin
  try
   Outlook:=GetActiveOleObject('Outlook.Application') ;
  except
   Outlook:=CreateOleObject('Outlook.Application') ;
  end;
  MailItem := Outlook.CreateItem(olMailItem) ;
  {with MailItem do begin
   Recipients.Add('delphi@aboutguide.com
   Subject := 'Subject: Outlook Mail From Delphi';
   Body := 'Welcome to my homepage:
http://delphi.about.com';
   Attachments.Add('C:\Windows\Win.ini
   Send;
  end; with}
   //Outlook := Unassigned;
end;

function TForm_smtp.SendMail_mapi(Subject,Mailtext,
                   FromName,FromAdress:string;
                   ToName,ToAdress,
                   AttachedFileName,
                   DisplayFileName:tstringlist;
                   ShowDialog:boolean):boolean;
var
  MapiMessage : TMapiMessage;
  MError      : Cardinal;
  Empfaenger  : Array[0..9] of TMapiRecipDesc;
  Absender    : TMapiRecipDesc;
  Datei       : Array[0..9] of TMapiFileDesc;
  i:integer;
begin

  with MapiMessage do begin
    ulReserved := 0;

    // Betreff
    lpszSubject := PChar(Subject);

    // Body
    lpszNoteText := PChar(Mailtext);

    lpszMessageType := nil;
    lpszDateReceived := nil;
    lpszConversationID := nil;
    flFlags := 0;

    // Absender festlegen
    Absender.ulReserved:=0;
    Absender.ulRecipClass:=MAPI_ORIG;
    Absender.lpszName:= PChar(FromName);
    Absender.lpszAddress:= PChar(FromAdress);
    Absender.ulEIDSize:=0;
    Absender.lpEntryID:=nil;
    lpOriginator := @Absender;

    // Empf�nger festlegen (Hier: nur 1 Empf�nger)
    nRecipCount := ToAdress.Count; //1;
    for i:=0 to nRecipCount-1 do
    begin
    Empfaenger[i].ulReserved:=0;
    Empfaenger[i].ulRecipClass:=MAPI_TO;
    Empfaenger[i].lpszName:= PChar(ToName[i]);
    Empfaenger[i].lpszAddress:= PChar(ToAdress[i]);
    Empfaenger[i].ulEIDSize:=0;
    Empfaenger[i].lpEntryID:=nil;
    end;
    lpRecips := @Empfaenger;

    // Dateien anh�ngen (Hier: nur 1 Datei)
    nFileCount := AttachedFileName.Count;//1;
    for i:=0 to nFileCount-1 do
    begin
    // Name der Datei auf der Festplatte
    Datei[i].lpszPathName:= PChar(AttachedFilename[i]);

    // Name, der in der Email angezeigt wird
    Datei[i].lpszFileName:= PChar(DisplayFilename[i]);
    Datei[i].ulReserved:=0;
    Datei[i].flFlags:=0;
    Datei[i].nPosition:=Cardinal(-1);
    Datei[i].lpFileType:=nil;
    end;
    lpFiles := @Datei;

  end;
  result:=false;
  // Senden
  if ShowDialog then
    MError := MapiSendMail(0, Application.Handle,
      MapiMessage, MAPI_DIALOG or MAPI_LOGON_UI, 0)
  else
    // Wenn kein Dialogfeld angezeigt werden soll:
    MError := MapiSendMail(0, Application.Handle, MapiMessage, 0, 0);

  Case MError of
    MAPI_E_AMBIGUOUS_RECIPIENT:
      MessageDlg('Empf�nger nicht eindeutig. (Nur m�glich, wenn Emailadresse nicht angegeben.)',mterror,[mbok],0); 
    
    MAPI_E_ATTACHMENT_NOT_FOUND: 
      MessageDlg('Datei zum Anh�ngen nicht gefunden',mterror,[mbok],0); 

    MAPI_E_ATTACHMENT_OPEN_FAILURE:
      MessageDlg('Datei zum Anh�ngen konnte nicht ge�ffnet werden.',mterror,[mbok],0); 

    MAPI_E_BAD_RECIPTYPE:
      MessageDlg('Empf�ngertyp nicht MAPI_TO, MAPI_CC oder MAPI_BCC.',mterror,[mbok],0);

    MAPI_E_FAILURE:
      MessageDlg('Die Email wurde nicht verschickt - l�uft das Email-Programm?',mterror,[mbok],0);

    MAPI_E_INSUFFICIENT_MEMORY:
      MessageDlg('Nicht genug Speicher.',mterror,[mbok],0);

    MAPI_E_LOGIN_FAILURE:
      MessageDlg('Benutzerlogin (z.B. bei Outlook) fehlgeschlagen.',mterror,[mbok],0);

    MAPI_E_TEXT_TOO_LARGE:
      MessageDlg('Text zu gro�.',mterror,[mbok],0);

    MAPI_E_TOO_MANY_FILES:
      MessageDlg('Zu viele Dateien zum Anh�ngen.',mterror,[mbok],0);

    MAPI_E_TOO_MANY_RECIPIENTS:
      MessageDlg('Zu viele Empf�nger angegeben.',mterror,[mbok],0);

    MAPI_E_UNKNOWN_RECIPIENT: MessageDlg('Empf�nger nicht in Adressbuch gefunden. '+
     '(Nur m�glich, wenn Emailadresse nicht angegeben.)',mterror,[mbok],0);

    MAPI_E_USER_ABORT:
      MessageDlg('Benutzer hat Senden abgebrochen oder MAPI nicht installiert.',mterror,[mbok],0);

    SUCCESS_SUCCESS:
    begin
      //MessageDlg('Erfolgreich !!! (Aber Absenden nicht garantiert.)',mtinformation,[mbok],0);
      result:=true;
    end;

  End;
end; {Christian "NineBerry" Schwarz}


procedure TForm_smtp.Button_anzeigenClick(Sender: TObject);
var s:string;
i:integer;
begin

	i:=ComboBoxAttachedFiles.ItemIndex;
  for i:=0 to  ComboBoxAttachedFiles.Items.Count-1 do
  begin
     s:=comboboxattachedfiles.Items[i];
     if fileexists(s) then shellexecute_se(0,'open',pchar(s),'','',sw_shownormal);
  end;
end;

procedure TForm_smtp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
 form_richedit_menue.Parent:=nil;
end;

end.
