unit a_texte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, dbgridEXT, StdCtrls, ComCtrls, ToolWin, ExtCtrls,
  OleCtnrs, olecontainerext, Mask, DBCtrls, a_data;

type
  TForm_texte = class(TForm)
    Panel_edit: TPanel;
    GroupBox_firma_text_top: TGroupBox;
    Label1: TLabel;
    Label58: TLabel;
    DBEdit_text_datum: TDBEdit;
    DBEdit_text_titel: TDBEdit;
    ScrollBox1: TScrollBox;
    OLeContainer: TOLeContainerext;
    Panel2: TPanel;
    GroupBox_firma_text: TGroupBox;
    DBgridEXT1: TDBgridEXT;
    RadioGroup_firm_ma: TRadioGroup;
    ToolBar_main: TToolBar;
    ToolButton21: TToolButton;
    ToolButton_neu: TToolButton;
    ToolButton9: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton43: TToolButton;
    ToolButton_sichern: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_delete: TToolButton;
    ToolButton2: TToolButton;
    ToolButton1: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure OLeContainerLButtonDown(Sender: TObject);
    procedure RadioGroup_firm_maClick(Sender: TObject);
    procedure filtern;
    procedure ToolButton_neuClick(Sender: TObject);
    procedure ToolButton_sichernClick(Sender: TObject);
    procedure ToolButton_deleteClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_texte: TForm_texte;

implementation

uses a_main, a_vorlagen;

{$R *.DFM}

procedure TForm_texte.FormCreate(Sender: TObject);
begin
 form_main.form_positionieren(tform(sender));
 form_main.sperren(panel_edit);
 filtern;
end;

procedure TForm_texte.OLeContainerLButtonDown(Sender: TObject);
begin
		olecontainer.DoVerb(ovshow);
		com.wordobjekterstellen(olecontainer);
end;

procedure TForm_texte.RadioGroup_firm_maClick(Sender: TObject);
begin
	filtern;
end;

procedure TForm_texte.filtern;
begin
with datamodul do
begin
 if radiogroup_firm_ma.ItemIndex=0 then
    	sql(q_texte,'select * from texte where i_firma ='+inttostr(q_firma['nummer']),'datum')
  else
 		sql(q_texte,'select * from texte where i_mitarbeiter ='+inttostr(q_mitarbeiter['nummer']),'datum');

end;
end;

procedure TForm_texte.ToolButton_neuClick(Sender: TObject);
begin
 form_vorlagen:=tform_vorlagen.create(self);
 form_vorlagen.ToolBar_main.visible:=false;
 form_vorlagen.radiogroup_speziell.visible:=false;
 form_vorlagen.OLeContainer.Ctl3D:=true;
 form_vorlagen.OLeContainer.Enabled:=false;
       if radiogroup_firm_ma.ItemIndex=0 then
       begin   //firma
         datamodul.sql(datamodul.q_vorlagen,'select * from vorlagen where i_filter=0','');
       end
       else
       begin
         datamodul.sql(datamodul.q_vorlagen,'select * from vorlagen where i_filter=3','');
       end;
		  if form_vorlagen.ShowModal=mrok then
		  begin
       if radiogroup_firm_ma.ItemIndex=0 then
       		  datamodul.q_texte.AppendRecord([nil,datamodul.q_Firma['nummer'],0,'TITEL',tdate(now())])
		  else
       		  datamodul.q_texte.AppendRecord([nil,0,datamodul.q_mitarbeiter['nummer'],'TITEL',tdate(now())]);

       datamodul.q_texte.edit;
       form_main.entsperren(panel_edit);
       form_main.olecontainer_laden(datamodul.q_vorlagen,olecontainer);

       olecontainer.Modified:=true;
       form_main.olecontainer_anzeigen;
       com.bookmarks_lesen;
       com.dbbookmarks_einfuegen;

       application.BringToFront;

		 end;
		 form_vorlagen.release;
		 form_vorlagen:=nil;
end;

procedure TForm_texte.ToolButton_sichernClick(Sender: TObject);
begin
		form_main.olecontainer_speichern(true,datamodul.q_texte,olecontainer);
     datamodul.sql_post(datamodul.q_texte);
  	form_main.sperren(panel_edit);
end;

procedure TForm_texte.ToolButton_deleteClick(Sender: TObject);
begin
	dateiloeschen(datamodul.q_texte,false);
end;

procedure TForm_texte.ToolButton_editClick(Sender: TObject);
begin
	form_main.entsperren(panel_edit);
end;

procedure TForm_texte.ToolButton1Click(Sender: TObject);
begin
	modalresult:=mrok;
end;

end.
