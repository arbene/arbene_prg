�
 TFORM_TEXTE 0�  TPF0TForm_texte
Form_texteLeftyTop� WidthHeight�CaptionTexteColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TPanel
Panel_editLeft� TopWidth.Height�AlignalClient
BevelOuterbvNoneCaption
Panel_editTabOrder  	TGroupBoxGroupBox_firma_text_topLeft Top Width.Height)AlignalTopTabOrder  TLabelLabel1LeftTopWidthHeightCaptionDatum  TLabelLabel58Left� TopWidthHeightCaptionTitel  TDBEditDBEdit_text_datumLeft8TopWidthyHeight	DataFieldDatum
DataSourceDataModul.ds_texteTabOrder   TDBEditDBEdit_text_titelLeft� TopWidth!Height	DataFieldtitel
DataSourceDataModul.ds_texteTabOrder   
TScrollBox
ScrollBox1Left Top)Width.HeightsAlignalClientTabOrder TOLeContainerextOLeContainerLeft TopWidthWHeightAllowInPlaceAllowActiveDocAutoActivateaaManualAutoVerbMenuCaptionOLeContainer
CopyOnSaveCtl3DParentCtl3DTabOrder OnLButtonDownOLeContainerLButtonDown    TPanelPanel2Left TopWidth� Height�AlignalLeft
BevelOuterbvNoneCaptionPanel2TabOrder 	TGroupBoxGroupBox_firma_textLeft TopaWidth� Height;AlignalClientTabOrder  
TDBgridEXT
DBgridEXT1LeftTopWidth� Height*AlignalClientColorclInfoBk
DataSourceDataModul.ds_texteDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamedatumWidthCVisible	 Expanded	FieldNametitelTitle.CaptionTitelWidth� Visible	     TRadioGroupRadioGroup_firm_maLeft Top Width� HeightaAlignalTop	ItemIndex Items.StringsFirmaMitarbeiter TabOrderOnClickRadioGroup_firm_maClick   TToolBarToolBar_mainLeft Top WidthHeightAutoSize	ButtonWidthCaptionToolBarColor	clBtnFaceImagesForm_main.ImageList_mainParentColorTabOrder TToolButtonToolButton21Left TopWidthCaptionToolButton21
ImageIndexStyletbsSeparator  TToolButtonToolButton_neuLeftTopHintDatensatz neu anlegenCaptionToolButton_neu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonToolButton9LeftTopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeft$TopCaptionToolButton_edit
ImageIndexOnClickToolButton_editClick  TToolButtonToolButton43Left<TopWidthCaptionToolButton43
ImageIndexStyletbsSeparator  TToolButtonToolButton_sichernLeftDTopHintDatensatz speichernCaptionToolButton_sichern
ImageIndexParentShowHintShowHint	OnClickToolButton_sichernClick  TToolButtonToolButton7Left\TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeftdTopHintDatensatz l�schenCaptionToolButton_delete
ImageIndexParentShowHintShowHint	OnClickToolButton_deleteClick  TToolButtonToolButton2Left|TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton1Left� TopCaptionToolButton1
ImageIndexOnClickToolButton1Click    