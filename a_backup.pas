unit a_backup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,shellapi, ComCtrls, ZipMstr19;

type
  TForm_backup = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Memo: TMemo;
    Label16: TLabel;
    Edit_pfad: TEdit;
    BitBtn_temp: TBitBtn;
    BitBtn_backupschreiben: TBitBtn;
    BitBtn_abbruch: TBitBtn;
    Timer_ende: TTimer;
    ProgressBar1: TProgressBar;
    Timer_progress: TTimer;
    BitBtn_ok: TBitBtn;
    CheckBox_ende: TCheckBox;
    procedure BitBtn_tempClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_backupschreibenClick(Sender: TObject);
    procedure BitBtn_abbruchClick(Sender: TObject);
    procedure Timer_progressTimer(Sender: TObject);
  private
    { Private-Deklarationen }
        command:string;
    procedure db_herunterfahren;
    procedure db_starten;
    procedure erzeuge_backup;
    function dumpdb(db:string):string;
  public
   f_sicher:boolean;
    procedure Datenbankinitialisieren;
    { Public-Deklarationen }
  end;

var
  Form_backup: TForm_backup;

implementation

uses a_main, a_data;

{$R *.DFM}

procedure TForm_backup.BitBtn_tempClick(Sender: TObject);
begin
 form_main.SaveDialog.Execute;
 edit_pfad.text:=form_main.Savedialog.FileName;
end;


procedure TForm_backup.FormCreate(Sender: TObject);
var
versionsinfo: OSVERSIONINFO;
t:string;
begin
form_main.form_positionieren(tform(sender));
versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;
	if betriebssystem=VER_PLATFORM_WIN32_WINDOWS then command:='command.com' else command:='cmd.exe' ;
  //t:=datetimetostr(now);
  t:=sql_datetostr(now);
  t:=t+'_'+timetostr(time);
  t:=stringreplace(t,'.','_',[rfreplaceall]);
  t:=stringreplace(t,':','_',[rfreplaceall]);
  t:=stringreplace(t,' ','_',[rfreplaceall]);
  t:=stringreplace(t,'-','_',[rfreplaceall]);
  t:=stringreplace(t,'"','',[rfreplaceall]);
  //t:=stringdatum;
  form_main.SaveDialog.DefaultExt:='.sql';
  form_main.SaveDialog.InitialDir:=pfad_backupverzeichnis;
  // form_main.Savedialog.FileName:=db_datenbank+'-'+dat_delphi_to_sql(datetimetostr(now))+'.zip';
   form_main.Savedialog.FileName:=db_datenbank+'_'+t+'.sql';
   form_main.Savedialog.Filter:='SQL-Datei|*.sql';
  edit_pfad.text:=pfad_backupverzeichnis+form_main.Savedialog.FileName;


  timer_progress.Enabled:=false;
end;

procedure TForm_backup.Datenbankinitialisieren;
var
t:ttime;
begin

db_herunterfahren;

t:=time;
while time<t+0.00008 do
begin
application.ProcessMessages;
end;

db_starten;

t:=time;
while time<t+0.00008 do
begin
application.ProcessMessages;
end;
end;

procedure TForm_backup.db_herunterfahren;
var
cmd_path,befehl,parameter:string;
begin
try
memo.Lines.Add('Datenbankanwendung wird gestoppt');
memo.Lines.Add('');
befehl:= command;
if betriebssystem=VER_PLATFORM_WIN32_NT then
	  begin
		 befehl:='net.exe';
		 parameter:='stop mysql';
	  end
else
	begin
     if not fileexists(pfad_mysql_prg+'mysqladmin.exe') then
     begin
          showmessage('Pfad f�r die MySql-Programmdateien nicht korrekt - bitte unter Optionen einrichten');
          exit;
     end;
		befehl:= command;
		parameter:=format('%s /C %smysqladmin.exe shutdown',[cmd_path,trim(pfad_mysql_prg)]);
	end;

  //executefile(befehl,parameter,'',true);

  shellexecute_se(0,'open',pchar(befehl),pchar(parameter),nil,SW_MINIMIZE	);
except
end;

end;

procedure TForm_backup.db_starten;
var
cmd_path,befehl,parameter:string;
begin
try
memo.Lines.Add('Datenbankanwendung wird gestartet');
memo.Lines.Add('');
if betriebssystem=VER_PLATFORM_WIN32_NT then
	  begin
		 befehl:='net.exe';
		 parameter:='start mysql';
	  end
else
	begin
     if not fileexists(pfad_mysql_prg+'mysql-opt.exe') then
     begin
          showmessage('Pfad f�r die MySql-Programmdateien nicht korrekt - bitte unter Optionen einrichten');
          exit;
     end;
		befehl:= {trim(edit_cmd.text)+}command;
		parameter:=format('%s /C %smysqld-opt.exe',[cmd_path,trim(pfad_mysql_prg)]);
	end;

  //executefile(befehl,parameter,'',true);

  shellexecute_se(0,'open',pchar(befehl),pchar(parameter),nil,SW_MINIMIZE	);
except
end;

end;



procedure TForm_backup.erzeuge_backup;
var
cmd_path,pfad, parameter,quelle,datei,zipdat:string;
filelist:tstringlist;
t:ttime;
dat:tdate;
begin
try

  //application.bringtofront;
  //application.ProcessMessages;
  arbene_vor;
  screen.Cursor:=crhourglass;
  application.ProcessMessages;
  form_main.ZipMaster.FSpecArgs.Clear;

	while memo.Lines.Count> 100 do memo.Lines.Delete(0);
	dat:=date();
	zipdat:=edit_pfad.text;

  case prg_typ of
       1,3,4:quelle:=pfad_mysql_daten+db_datenbank+'\*.*';
       2:quelle:=pfad_mysql_daten+'zeus\*.*';
  end;

  memo.Lines.Add('Beginne mit der Datensicherung von');
  memo.Lines.Add(quelle);
  memo.Lines.Add('nach');
  memo.Lines.Add(zipdat );
  memo.Lines.Add('' );

	filelist:=tstringlist.Create;
	filelist.Add(quelle);

    form_main.ZipMaster.AddOptions:= form_main.ZipMaster.AddOptions-[addmove];
    form_main.ZipMaster.FSpecArgs.Add(quelle);

    form_main.ZipMaster.ZipFileName:=zipdat;
    form_main.ZipMaster.RootDir:= 'c:\';

    try
    	form_main.ZipMaster.Add;  //hier zippen
    except
    	showmessage('Fehler beim Zippen');
    end;
	//zipfiles( zipdat,filelist,memo);
  	memo.Lines.Add('' );
  if (pos('zip warning',memo.text)>0) or (pos('zip error',memo.text)>0) then
  	memo.Lines.Add('ACHTUNG Datensicherung konnte nicht durchgef�hrt werden' )
  else
  begin
	     memo.Lines.Add('Datensicherung nach von ' );
         memo.Lines.add(quelle);
         memo.Lines.Add('nach ');
         memo.Lines.Add(zipdat);
         memo.Lines.Add(' ist abgeschlossen.' );
         memo.Lines.Add( '');
  end;



finally
 filelist.free;
end;
end;


function TForm_backup.dumpdb(db:string):string;
var
cmd_path,befehl,parameter, dbname, mypfad,backuppfad,backupdatei,d,parameter_anzeige:string;
begin

d:=sql_datetostr(now())+'_'+timetostr(now());

mypfad:=pfad_mysql_prg;// �ndern  in dbtools
dbname:=db_datenbank;  //�ndern in dbtools
backuppfad:=pfad_backupverzeichnis;//�ndern in dbtools

{d:=dbname; //�ndern in dbtools
d:=stringreplace(d,'.','_', [rfreplaceall]);
d:=stringreplace(d,':','_', [rfreplaceall]);
d:=stringreplace(d,'-','_', [rfreplaceall]); }
backupdatei:=db; //backuppfad+d+'.sql';



if ((idb_protocol=1) and fileexists('c:\my.cnf')) then
begin
   showmessage('die Datei C:\my.cnf wird umbenannt in c:\old_my.cnf');
   RenameFile('c:\my.cnf','c:\old_my.cnf');
   deletefile('c:\my.cnf');
end;

backupdatei:=stringreplace(backupdatei,'\','/', [rfreplaceall]);
befehl:=command;

if idb_protocol=0 then
parameter:=format('%s /C %smysqldump.exe --user=Arbene --password=nixreinkommen --add-drop-table --add-locks --all --quick --lock-tables %s > %s',[cmd_path,mypfad, dbname, backupdatei])
else //mariadb
//parameter:=format('/C "%smysqldump.exe" --user=Arbene --password=nixreinkommen --default-character-set=latin1 --max-allowed-packet=64M --add-drop-table --add-locks --lock-tables %s > %s',[mypfad, dbname, backupdatei]) ;
parameter:=format('/C "%smysqldump.exe" --user=Arbene --password=nixreinkommen --default-character-set=latin1 --max-allowed-packet=64M --skip-extended-insert %s > %s',[mypfad, dbname, backupdatei]) ;

//memo.Lines.Add(parameter );

  {befehl:=format('%smysqldump.exe',[mypfad]);
  parameter:=format('--user=Arbene --password=nixreinkommen --default-character-set=latin1 --max-allowed-packet=64M  %s > %s',[ dbname, backupdatei]) ;

  executefile(befehl,parameter,'',false);}

  parameter_anzeige:=stringreplace(parameter,'nixreinkommen','*********', [rfreplaceall]);
  memo.Lines.Add(befehl +parameter_anzeige);
  //shellexecute_se(0,'open',pchar(befehl),pchar(parameter),nil,SW_normal);
  //runasadmin(application.Handle,befehl,parameter);
  if run_app(befehl,parameter) then
result:=backupdatei ;
end;

procedure TForm_backup.BitBtn_backupschreibenClick(Sender: TObject);
var
t,t1:ttime;
query,bd,zeile:string;
weiter,sicher:boolean;
dg,size:int64;
//f:file of char;
fs: tfilestream;
Buffer: array[1..1000] of char;
numread,i:integer;
g:boolean;
begin
  weiter:=true;
  timer_progress.Interval:=200;
  progressbar1.position:=0;
  BitBtn_backupschreiben.Enabled:=false;

  if fileexists(edit_pfad.text) then
  begin
       showmessage('Die Backup-Datei besteht schon - Bitte anderne Namen w�hlen');
       exit;
  end;

  if pos(' ',edit_pfad.text)>0 then
  begin
    showmessage('Leerzeichen sind im Pfad und dem Dateinamen nicht erlaubt.');
    exit;
  end;

  if not directoryexists(pfad_mysql_daten+db_datenbank) then
  begin
  	 showmessage('Der Pfad zu den zu sichernden Dateien ist nicht korrekt, bitte unter Optionen den Pfad mysql-Daten angleichen');
    exit;
  end;

try

  timer_progress.Enabled:=true;
  //bitbtn_abbruch.visible:=false;
  bitbtn_abbruch.Tag:=12;
  //screen.Cursor:=crhourglass;
  application.ProcessMessages;


  memo.Lines.Add('Beginne mit der Datensicherung von');
  memo.Lines.Add(db_datenbank);
  memo.Lines.Add('nach');
  memo.Lines.Add(edit_pfad.text );
  memo.Lines.Add('');
  //memo.Lines.Add('Warten Sie mit dem Herunterfahren des PCs bis das DOS Fenster geschlossen ist' );

  if weiter then bd:=dumpdb(trim(edit_pfad.text));

  //###############
  t:=time();
   while (time()<t+0.005) do
   begin
    application.ProcessMessages;
   	if fileexists(bd) then break;
    if bitbtn_abbruch.tag=13 then break;
   end;

   if not fileexists(bd) then
   begin
    memo.Lines.Add('ACHTUNG! Die Datei  '+bd+' wurde nicht erzeugt' );
      bitbtn_abbruch.visible:=false;

      exit;
   end;

   memo.Lines.Add('Die Datei  '+bd+' wird geschrieben' );

  //warte_bis_datei_lesbar(bd);
  g:=true;
  while  g do
  begin
    try
      application.ProcessMessages;
      fs:=tfilestream.Create(bd,fmOpenRead );
      g:=false;
    except
      pause(100);
      g:=true;
       try
        fs.Free; //weis nicht ob n�tig
       except
       end;
    end;
  end;


   dg:=filesize(bd);
   memo.Lines.Add('Die Datei  '+bd+' ist erstellt. Dateigr��e: '+inttostr((dg div 1000000))+' MB' );
   memo.Lines.Add('' );
   if dg>4000 then memo.Lines.Add('Die Backupdatei erscheint aufgrund der Dateigr��e vollst�ndig' ) else
   memo.Lines.Add('ACHTUNG! Die Backupdatei erscheint aufgrund der Dateigr��e unvollst�ndig' ) ;
   memo.Lines.Add('' );
   sicher:=false;
   application.ProcessMessages;
   //AssignFile(F,bd );   { Datei ausgew�hlt }

   FileMode := 0;


  numread:=fs.Seek(-1001,soFromEnd	);
  inc(numread);

   numread:=fs.Read( buffer,1000);

   zeile:='';
   for i:=1 to numread do
   begin
   	zeile:=zeile+buffer[i];
   end;

   if (pos('CREATE TABLE `zzz`',zeile)>0) or (pos('CREATE TABLE zzz',zeile)>0) then sicher:=true else sicher:=false;        //   CREATE TABLE `zzz` mariadb

   if sicher then memo.Lines.Add('Die Backupdatei enth�lt die letzte Tabelle "zzz"' );

   if sicher then memo.Lines.Add('Das Backup ist erfolgreich abgeschlossen.' ) else   memo.Lines.Add('ACHTUNG, Die Backupdatei scheint nicht alle Tabellen zu enthalten' );

   if sicher then f_sicher:=true else f_sicher:=false; // erfolgreiches Backup
  last_backup:=datetimetostr(now);

  form_main.inischreiben(28);
  form_main.inischreiben(35);
  screen.Cursor:=crdefault;
  application.ProcessMessages;
  //application.bringtofront;
  fs.Free;

  finally
    arbene_vor;
    self.BringToFront;
    bitbtn_abbruch.Visible:=false;
    bitbtn_abbruch.Tag:=0;
    bitbtn_ok.visible:=true;
    timer_progress.Enabled:=true;
    timer_progress.Interval:=50;
    if checkbox_ende.Checked then 	timer_ende.enabled:=true else timer_progress.Enabled:=false;
    progressbar1.position:=0;
  end;


end;

procedure TForm_backup.BitBtn_abbruchClick(Sender: TObject);
begin

  if bitbtn_abbruch.tag=12 then
  begin
     bitbtn_abbruch.tag:=13;
  end
  else
	modalresult:=mrAbort
end;

procedure TForm_backup.Timer_progressTimer(Sender: TObject);
begin
  progressbar1.position:=progressbar1.position+1;
  if progressbar1.position>=100 then progressbar1.position:=0;
  application.ProcessMessages;
end;

end.
