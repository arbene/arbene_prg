library horus_inst;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
	SysUtils,
	Classes,
	GIPlugins,
	Inst_form in 'Inst_form.pas' {Form_main};


procedure GetPluginInfo(var PluginInfo: TPluginInfo); stdcall;
const
	CPluginDescription = 'abene_install';
	CPluginID          = 'arb_inst';
	CPluginName        = 'arbene_install';
	CPluginAuthor      = 'klawitter';
  CPluginCopyright   = 'Copyright � 2002 Arbene';
begin
  with PluginInfo do begin
    if InfoVersion >= VER_PLUGININFO_100 then begin
      Flags:=0;
      RequiredGIVersion:=VER_GI_200;
      Move(CPluginID,PluginID,length(CPluginID));
      Move(CPluginName,PluginName,length(CPluginName));
		 PluginType:=RUNTIME_PLUGIN;
		 PluginVersion:=$10000;
      Move(CPluginAuthor,PluginAuthor,length(CPluginAuthor));
      Move(CPluginDescription,PluginDescription,length(CPluginDescription));
      Move(CPluginCopyright,PluginCopyright,length(CPluginCopyright));
      ProcessMsg:=@EventsHandler;
    end;
  end;
end;

exports
  GetPluginInfo;

begin
end.
