unit a_bereich_edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,a_data, Grids, DBGrids, dbgridEXT,Bersonderheit_edit,
  DBCtrls, ComCtrls, audiogramm, ToolWin,db;

type
  TForm_bereich = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit_name: TEdit;
    RG: TRadioGroup;
    Panel2: TPanel;
    GroupBox_memo: TGroupBox;
    Notebook: TNotebook;
    Label2: TLabel;
    Einheit: TLabel;
    Edit_bez: TEdit;
    Edit_bez_dim: TEdit;
    DBgridEXT_ber: TDBgridEXT;
    Panel3: TPanel;
    BitBtn3: TBitBtn;
    BitBtn_veraendern: TBitBtn;
    audiogramm1: Taudiogramm;
    RichEdit_bereich: TRichEdit;
    BitBtn4: TBitBtn;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton_up: TSpeedButton;
    SpeedButton_down: TSpeedButton;
    ToolBar1: TToolBar;
    ToolButton9: TToolButton;
    ToolButton_cancel: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_quit: TToolButton;
    Panel1: TPanel;
    procedure RGClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn_veraendernClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure SpeedButton_upClick(Sender: TObject);
    procedure SpeedButton_downClick(Sender: TObject);
    procedure ToolButton_quitClick(Sender: TObject);
    procedure ToolButton_cancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen }
    procedure reihenfolge(richtung:integer);
  public
    { Public-Deklarationen }
  end;

var
  Form_bereich: TForm_bereich;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_bereich.RGClick(Sender: TObject);
var
box1,box2: tgroupbox;
begin
	notebook.PageIndex:= rg.ItemIndex;

end;

procedure TForm_bereich.BitBtn3Click(Sender: TObject);
var
nodedata: pnodepointer;
bernum,nummer,rf_last:integer;
begin
form_bes_edit:=tform_bes_edit.Create(self);

nodedata:=form_main.treeview_ext_bereich.Selected.data;
bernum:=nodedata^.nummer;
Form_bes_edit.CheckBox.Visible:=false;

if Form_bes_edit.showmodal=mrok then
begin
  datamodul.q_berlist.Last;
  if datamodul.q_berlist['nummer']<>null then rf_last:=datamodul.q_berlist['reihenfolge']+1
  		else rf_last:=1;
	if (neuer_datensatz(datamodul.q_berlist,['nummer','i_bereich','listenfeld','reihenfolge']
  		,[null,bernum,form_bes_edit.Edit.text,rf_last])=-1) then exit;
	datamodul.q_berlist.post;
  datamodul.q_berlist.refresh;
  datamodul.q_berlist.last;
end;
form_bes_edit.Release;
end;

procedure TForm_bereich.FormActivate(Sender: TObject);
begin
RGClick(self);
end;

procedure TForm_bereich.BitBtn_veraendernClick(Sender: TObject);
begin
form_bes_edit:=tform_bes_edit.Create(self);

Form_bes_edit.CheckBox.Visible:=false;
IF datamodul.q_berlist['listenfeld']<>null then
	Form_bes_edit.Edit.text:=datamodul.q_berlist['listenfeld']
	else  Form_bes_edit.Edit.text:='';
if Form_bes_edit.showmodal=mrok then
	begin
	datamodul.q_berlist.Edit;
	datamodul.q_berlist['listenfeld']:=form_bes_edit.Edit.text;
	datamodul.q_berlist.post;
	end;
form_bes_edit.Release;
end;

procedure TForm_bereich.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
grid_suchen(sender);
end;

procedure TForm_bereich.FormShow(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
	notebook.PageIndex:= rg.ItemIndex;
	edit_name.SetFocus;
end;

procedure TForm_bereich.BitBtn4Click(Sender: TObject);
begin
			dateiloeschen(datamodul.q_berlist,false);
end;

procedure Tform_bereich.reihenfolge(richtung:integer);
var
reihenfolge1,reihenfolge2:integer;
nummer1:integer;
begin
with datamodul do
begin
	{if datamodul.q_berlist.State=dsedit then
			begin
			q_berlist.Post;
			exit;
		  end;      }
	if (richtung=0) and (q_berlist.Bof) then exit;
	if (richtung=1) and (q_berlist.eof) then exit;
	nummer1:=q_berlist['nummer'];
	reihenfolge1:=q_berlist['reihenfolge'];
	if (richtung=0) then q_berlist.Prior else q_berlist.Next;

	reihenfolge2:=q_berlist['reihenfolge'];
	q_berlist.edit;
	q_berlist['reihenfolge']:=reihenfolge1;
	q_berlist.post;
	//q_berlist.first;
	q_berlist.locate('nummer',nummer1,[]);
	q_berlist.edit;
	q_berlist['reihenfolge']:=reihenfolge2;
	q_berlist.post;
	q_berlist.refresh;
  q_berlist.Locate( 'nummer', nummer1,[]);
end;
end;

procedure TForm_bereich.SpeedButton_upClick(Sender: TObject);
begin
  reihenfolge(0);
end;

procedure TForm_bereich.SpeedButton_downClick(Sender: TObject);
begin
 reihenfolge(1);
end;

procedure TForm_bereich.ToolButton_quitClick(Sender: TObject);
begin
if datamodul.q_berlist.State in [dsedit,dsinsert] then datamodul.q_berlist.Post;
modalresult:=mrok;
end;

procedure TForm_bereich.ToolButton_cancelClick(Sender: TObject);
begin
if datamodul.q_berlist.State in [dsedit,dsinsert] then datamodul.q_berlist.cancel;
modalresult:=mrcancel;
end;

procedure TForm_bereich.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
ToolButton_quitClick(Sender);
end;

end.
