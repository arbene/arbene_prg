unit a_arbeitsplatz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, ToolWin, dbTreeView, ImgList, StdCtrls, Mask, DBCtrls,
  Buttons, dbtreeview_kaskade,db, OleCtnrs, olecontainerext,zdataset,
  Menus, OleCtrls, PdfLib_TLB,a_report,shellapi,Variants, dbcombo_number,
  Grids, DBGrids, dbgridEXT;

type
  TForm_ap = class(TForm)
    Panel_li: TPanel;
    Splitter: TSplitter;
    ToolBar: TToolBar;
    ToolButton_del: TToolButton;
    ToolButton7: TToolButton;
    dbtreeview_k1: Tdbtreeview_k;
    PageControl: TPageControl;
    TabSheet_main: TTabSheet;
    TabSheet_word: TTabSheet;
    Notebook: TNotebook;
    Label4: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit1: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit3: TDBEdit;
    DBEdit_a_name: TDBEdit;
    DBEdit_a_vorname: TDBEdit;
    BitBtn_vorges_wahl: TBitBtn;
    DBEdit_a_telefon: TDBEdit;
    DBEdit_a_mail: TDBEdit;
    ScrollBox_2: TScrollBox;
    Arbene_Ap: TOLeContainerext;
    ToolButton5: TToolButton;
    Label17: TLabel;
    Label18: TLabel;
    ToolButton6: TToolButton;
    ToolButton_save: TToolButton;
    ToolButton_new: TToolButton;
    ToolButton_change: TToolButton;
    ToolButton3: TToolButton;
    ToolButton_ok: TToolButton;
    ToolButton_abort: TToolButton;
    ToolButton4: TToolButton;
    Panel1: TPanel;
    Edit_such: TEdit;
    Label_suchen: TLabel;
    SpeedButton1: TSpeedButton;
    DBMemo_ap_speziell: TDBMemo;
    MainMenu: TMainMenu;
    m_Datei: TMenuItem;
    m_Report: TMenuItem;
    alleGefhrdungsfaktorenmitHandlungsbedarf1: TMenuItem;
    GefhrdungsfaktorenPriorittA1: TMenuItem;
    GefhrdungsfaktorenPriorittB1: TMenuItem;
    GefhrdungsfaktorenPriorittC1: TMenuItem;
    m_Neu: TMenuItem;
    ndern1: TMenuItem;
    Sichern1: TMenuItem;
    Lschen1: TMenuItem;
    N1: TMenuItem;
    Abbrechen1: TMenuItem;
    Verlassen1: TMenuItem;
    m_Tools: TMenuItem;
    neueAbteilungskrzellesen1: TMenuItem;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label5: TLabel;
    DBEdit_ap_name: TDBEdit;
    BitBtn_ap_wahl: TBitBtn;
    Panel18: TPanel;
    DBMemo_gef_speziell: TDBMemo;
    Panel19: TPanel;
    Label1: TLabel;
    DBEdit_gef_name: TDBEdit;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Label3: TLabel;
    Label16: TLabel;
    alleAbteilungenArbeitspltzeGefhrdungen1: TMenuItem;
    Ansicht: TMenuItem;
    m_alleProjekte: TMenuItem;
    BitBtn_adobe: TBitBtn;
    Panel24: TPanel;
    Label7: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBEdit7: TDBEdit;
    Panel25: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    DBMemo_projekt: TDBMemo;
    ImageList_ap: TImageList;
    SpeedButton_expand: TSpeedButton;
    SpeedButton_collaps: TSpeedButton;
    N2: TMenuItem;
    m_Unfall_in_Abteilung: TMenuItem;
    m_Unfall_in_firma: TMenuItem;
    DBCombo_num_texte_user: TDBCombo_num;
    Label2: TLabel;
    SpeedButton_firna_text_wdv: TSpeedButton;
    TabSheet_gef_memo: TTabSheet;
    GroupBox_massnahmen: TGroupBox;
    GroupBox_gef_vorgabe: TGroupBox;
    DBMemo_gef3: TDBMemo;
    DBMemo2: TDBMemo;
    GroupBox3: TGroupBox;
    DBgridEXT1: TDBgridEXT;
    GroupBox4: TGroupBox;
    DBgridEXT2: TDBgridEXT;
    GroupBox5: TGroupBox;
    DBMemo_gef_bel: TDBMemo;
    BitBtn_gefwahl: TBitBtn;
    PopupMenu: TPopupMenu;
    m_Vorsorge_pruef: TMenuItem;
    m_Untersuchungenplanen: TMenuItem;
    AuswertungmitausgehlterAbteilungArbeitsplatz1: TMenuItem;
    procedure dbtreeview_k1Change(Sender: TObject; Node: TTreeNode);
    procedure BitBtn_ap_neuClick(Sender: TObject);
    procedure BitBtn_gef_neuClick(Sender: TObject);
    procedure dbtreeview_k1Changing(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn_vorges_wahlClick(Sender: TObject);
    procedure BitBtn_abt_neuClick(Sender: TObject);
    procedure DBCheckBox_allMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ToolButton_delClick(Sender: TObject);
    procedure BitBtn_ap_wahlClick(Sender: TObject);
    procedure BitBtn_abtlesenClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure arbene_apLButtonDown(Sender: TObject);
    procedure Arbene_gefaehrdungsfaktorLButtonDown(Sender: TObject);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton_newClick(Sender: TObject);
    procedure ToolButton_okClick(Sender: TObject);
    procedure ToolButton_abortClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ToolButton_changeClick(Sender: TObject);
    procedure alleGefhrdungsfaktorenmitHandlungsbedarf1Click(
      Sender: TObject);
    procedure GefhrdungsfaktorenPriorittA1Click(Sender: TObject);
    procedure GefhrdungsfaktorenPriorittB1Click(Sender: TObject);
    procedure GefhrdungsfaktorenPriorittC1Click(Sender: TObject);
    procedure dbtreeview_k1DblClick(Sender: TObject);
    procedure Edit_suchEnter(Sender: TObject);
    procedure alleAbteilungenArbeitspltzeGefhrdungen1Click(
      Sender: TObject);
    procedure dbtreeview_k1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure m_alleProjekteClick(Sender: TObject);
    procedure BitBtn_adobeClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dbtreeview_k1DragDrop(Sender, Source: TObject; X,
      Y: Integer);
    procedure SpeedButton_expandClick(Sender: TObject);
    procedure SpeedButton_collapsClick(Sender: TObject);
    procedure m_Unfall_in_AbteilungClick(Sender: TObject);
    procedure m_Unfall_in_firmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton_firna_text_wdvClick(Sender: TObject);
    procedure DBEdit6Change(Sender: TObject);
    procedure BitBtn_gefwahlClick(Sender: TObject);
    procedure m_Vorsorge_pruefClick(Sender: TObject);
    function a_neu():int64;
    procedure a_change();
    procedure a_save();
    procedure a_delete();
    procedure Unfall_in_Abteilung();
    procedure Unfall_in_Firma();
    procedure report(n:integer);
    procedure vorsorge_pruefen();
    procedure AuswertungmitausgehlterAbteilungArbeitsplatz1Click(
      Sender: TObject);
  private
    { Private-Deklarationen }
    pdf_load, pfad_pdf:string;
    p_firma,p_modus:integer;
    p_ab_nummer:int64;
    function abt_neu(kuerzel:string):int64;
    function ap_neu:int64;
   function gef_neu(gefaehrdungen:int64;modus:string):int64;
    procedure gefaehrdungen_einfuegen;
    function gefaehrdungsfaktor_neu(i_ap,i_gefaehrdungsfaktor:int64):int64;

    function projekt_neu(i_nummer:int64;e:integer):int64;
    procedure bescheinigungen_aktualisieren;
  public
    { Public-Deklarationen }
    modus:string;
    akt_tzquery:tzquery;
    procedure ini(i_firma,modus:integer; ab_nummer:int64);

  end;

var
  Form_ap: TForm_ap;

implementation

uses a_main, a_data, a_ap_infowahl, a_tabellen, a_namsuch, a_gefaehrdungen,
  a_u_wahl, a_uebersicht_arbeitsunfall, a_kalender, a_vorsorgen_abgleich;

{$R *.DFM}

procedure tform_ap.ini(i_firma,modus:integer;ab_nummer:int64);
var
query,s_proband:string;
begin
//modus 0: alle werden angezeigt
//modus 1: nur s_abt_nr wird angezeigt
//wenn s_abt_numm <>0 dann wird darauf positioniert
p_firma:=i_firma;
p_modus:=modus;
p_ab_nummer:=ab_nummer;
with datamodul do
begin

  s_proband:= getbigint_str(q_mitarbeiter,'nummer');
  query:=format('select nummer,name,vorname,telefon_f,email_f from mitarbeiter where i_firma=%d ',[i_firma]);
  sql_new(true,q_2,query,'');

  case modus of

       0:  query:=format('select *  from abteilung where i_firma=%d and storno=0 order by abteilung.kuerzel,abteilung.reihenfolge',[i_firma]);     //abteilung.kuerzel
       1:  query:=format('select *  from abteilung where nummer=%s and storno=0 order by abteilung.kuerzel,abteilung.reihenfolge',[inttostr(ab_nummer)]);
  end;
  sql_new(false,q_ap3,query,'');

  //query:='select a_platz.* from a_platz  where a_platz.storno=0 order by a_platz.i_abteilung,a_platz.name,a_platz.reihenfolge';  //i_abteilung
    query:='select a_platz.* from a_platz left join abteilung on (a_platz.i_abteilung=abteilung.nummer) ';
    query:=query+ format(' left join firma on (abteilung.i_firma=firma.nummer) where (firma.nummer=%d) and ( a_platz.storno=0 ) order by a_platz.i_abteilung,a_platz.name,a_platz.reihenfolge',[i_firma]);  //i_abteilung

  sql_new(false,q_ap4,query,'');

  query:='select a_gefaehrdungsfaktor.nummer,a_gefaehrdungsfaktor.i_ap,a_gefaehrdungsfaktor.i_gefaehrdungsfaktor,a_gefaehrdungsfaktor.storno,a_gefaehrdungsfaktor.datum,'+
   ' a_gefaehrdungsfaktor.memo_speziell,a_gefaehrdungsfaktor.ole  '+
   ', gefaehrdungen_2.name, gefaehrdungen_2.i_gef1,gefaehrdungen_2.pdf_seite as pdf_seite,gefaehrdungen_2.pdf_datei as pdf_datei ,  a_platz.i_arbeitsplaetze from a_gefaehrdungsfaktor left join gefaehrdungen_2 on ';
   query:=query+ ' (a_gefaehrdungsfaktor.i_gefaehrdungsfaktor=gefaehrdungen_2.nummer) left join a_platz on (a_gefaehrdungsfaktor.i_ap=a_platz.nummer) ';
   query:=query+ format('left join abteilung on (a_platz.i_abteilung=abteilung.nummer)  left join firma on (abteilung.i_firma=firma.nummer) where (firma.nummer=%d) and ( a_gefaehrdungsfaktor.storno=0 ) order by i_ap, gefaehrdungen_2.name ',[i_firma]);
  sql_new(false,q_ap5,query,'');

  query:='select a_gefaehrdung.*, left(gefaehrdungen_3.memo_gef,100) as gefaehrdung from a_gefaehrdung left join gefaehrdungen_3 on (gefaehrdungen_3.nummer=a_gefaehrdung.i_gef3) ';
  query:=query+  ' left join a_gefaehrdungsfaktor on(a_gefaehrdung.i_gefaehrdungsfaktor=a_gefaehrdungsfaktor.nummer) left join a_platz on (a_gefaehrdungsfaktor.i_ap=a_platz.nummer) ';
  query:=query+ format( ' left join abteilung on (a_platz.i_abteilung=abteilung.nummer)  left join firma on (abteilung.i_firma=firma.nummer)  where (firma.nummer=%d) and (a_gefaehrdung.storno =0) order by  i_gefaehrdungsfaktor,reihenfolge ',[i_firma]);
  //query:='select a_gefaherdung.nummer,a_gefaehrdung.memo_gef, a_gefaehrdung.memo_massnahme from a_gefaehrdung where  storno =0  ';
  sql_new(false,q_ap6,query,'');

  //query:='select a_projekte.* from a_projekte left join a_platz on (a_projekte.i_nummer=a_platz.nummer) left join abteilung on (a_platz.i_abteilung=abteilung.nummer)  left join firma on (abteilung.i_firma=firma.nummer) ';
  //query:=query+ format( ' where (firma.nummer=%d) and (a_projekte.storno=0) and (a_projekte.i_ebene=2) order by a_projekte.i_nummer,a_projekte.reihenfolge',[i_firma]);  //i_nummer
  query:='select a_projekte.* from a_projekte left join a_platz on (a_projekte.i_nummer=a_platz.nummer) left join abteilung on (a_platz.i_abteilung=abteilung.nummer)  left join firma on (abteilung.i_firma=firma.nummer) ';
  query:=query+ format( ' where (firma.nummer=%d) and (a_projekte.storno=0) order by a_projekte.i_nummer,a_projekte.reihenfolge',[i_firma]);  //i_nummer
  sql_new(false,q_ap10,query,'');

  bescheinigungen_aktualisieren;


  dbtreeview_k1.liste_lesen(1);
  dbtreeview_k1.gehe_zu_datensatz(1,ab_nummer);

  dbtreeview_k1.FullCollapse;
end;
end;

procedure tform_ap.gefaehrdungen_einfuegen;
var
nr,i:int64;
query:string;
begin
i:=getbigint(datamodul.q_ap4,'i_arbeitsplaetze');
query:=format('select * from a_platz left join a_gefaehrdungsfaktor on(a_platz.nummer=a_gefaehrdungsfaktor.i_ap)where i_arbeitsplaetze=%s and b_all=1 group by i_gefaehrdungsfaktor',[inttostr(i)]);
datamodul.sql_new(false,datamodul.q_7,query,'');
while not datamodul.q_7.Eof do
begin
 nr:=getbigint(datamodul.q_7,'i_gefaehrdungsfaktor');
 i:=getbigint(datamodul.q_ap4,'nummer');
 gefaehrdungsfaktor_neu(i,nr);

 datamodul.q_7.Next;
end;

end;

procedure TForm_ap.dbtreeview_k1Change(Sender: TObject; Node: TTreeNode);
var
i:int64;
dat:string;
begin
if not dbtreeview_k1.change_allowed then exit;
if not assigned(form_ap) then exit;
form_ap.akt_tzquery:=nil;

if dbtreeview_k1.a_tabelle=dbtreeview_k1.tab_v then //variable tabelle
begin
     form_ap.akt_tzquery:=datamodul.q_ap10;
     form_ap.caption:='Projekt';
     tabsheet_main.Caption:='Projekt';
     tabsheet_word.TabVisible:=true;
     notebook.PageIndex:=4;
     toolbutton_new.Enabled:=false;
     m_neu.Enabled:=false;
     tabsheet_word.TabVisible:=false;
     if datamodul.q_ap10['ole']<>'' then
     tabsheet_word.Caption:='**Word-Dokument zum aktuellen Projekt**'
       else tabsheet_word.Caption:='Das Word-Dokument zum aktuellen Projekt ist leer';
end
else
begin
toolbutton_new.Enabled:=true;
tabsheet_gef_memo.TabVisible:=false;
tabsheet_word.TabVisible:=false;
m_neu.Enabled:=true;
dbtreeview_k1.DragMode:=dmmanual;
case dbtreeview_k1.a_ebene of
-1: begin
      notebook.PageIndex:=5; //leer
      m_Vorsorge_pruef.Enabled:=true;
    end;
1:begin
       form_ap.akt_tzquery:=datamodul.q_ap3;
       form_ap.caption:='Abteilung';
       tabsheet_main.Caption:='Abteilung';
       tabsheet_word.TabVisible:=false;
       m_Vorsorge_pruef.Enabled:=true;

       {if datamodul.q_ap3['ole']<>'' then
       tabsheet_2.Caption:='**Word-Dokument zur aktellen Abteilung**'
       else tabsheet_2.Caption:='Word-Dokument zur aktellen Abteilung leer';}

       notebook.PageIndex:=0;
       i:=getbigint(datamodul.q_ap3,'i_mitarbeiter');
       //if (i=0) or (not datamodul.q_2.Locate('nummer',inttostr(i),[])) then
       if (i=0) or (not datamodul.q_2.Locate('nummer',(i),[])) then
       begin
          dbedit_a_name.visible:=false;
          dbedit_a_vorname.visible:=false;
          dbedit_a_telefon.visible:=false;
          dbedit_a_mail.visible:=false;
       end
       else
       begin
          dbedit_a_name.visible:=true;
          dbedit_a_vorname.visible:=true;
          dbedit_a_telefon.visible:=true;
          dbedit_a_mail.visible:=true;
       end;
  end;
2:begin
       form_ap.akt_tzquery:=datamodul.q_ap4;
       form_ap.caption:='Arbeitsplatz';
       tabsheet_main.caption:='Arbeitsplatz';
       m_Vorsorge_pruef.Enabled:=true;
       //tabsheet_word.TabVisible:=true;
       if datamodul.q_ap4['ole']<>'' then
       tabsheet_word.Caption:='**Word-Dokument zum aktuellen Arbeitsplatz**'
       else tabsheet_word.Caption:='Das Word-Dokument zum aktuellen Arbeitsplatz ist leer';
       notebook.PageIndex:=1;
       dbtreeview_k1.DragMode:=dmautomatic;
  end;
3:begin
       form_ap.akt_tzquery:=datamodul.q_ap5;
       form_ap.caption:='Gefährdungsgruppe';
       tabsheet_main.caption:='Gefährdungsgruppe';
       tabsheet_word.TabVisible:=false;
       m_Vorsorge_pruef.Enabled:=false;
       if datamodul.q_ap5['ole']<>'' then //lesend
       tabsheet_word.Caption:='**Word-Dokument zur aktuellen Gefährdungsgruppe**'
       else tabsheet_word.Caption:='Das Word-Dokument zur aktuellen Gefährdungsgruppe ist leer';

       notebook.PageIndex:=2;

  end;
4:begin
       form_ap.akt_tzquery:=datamodul.q_ap6;
       form_ap.caption:='Gefährdung / Belastung';
       tabsheet_main.caption:='Gefährdung / Belastung (Vorgaben)';
       tabsheet_word.TabVisible:=false;
       tabsheet_gef_memo.TabVisible:=true;
       m_Vorsorge_pruef.Enabled:=false;

       notebook.PageIndex:=3;

       bescheinigungen_aktualisieren;

       end;
else
  begin
       notebook.PageIndex:=5;
       tabsheet_word.TabVisible:=false;
       m_Vorsorge_pruef.Enabled:=false;

  end;
end;
end;
END;


procedure tform_ap.bescheinigungen_aktualisieren;
var
query,s_g3:string;
begin
with datamodul do
begin
  s_g3:=q_ap6.findfield('i_gef3').AsString;

if s_g3<>'' then
begin

  query:=format('select * from gefaehrdungen_3 where nummer=%s',[s_g3]);
  sql_new(false,q_ap11,query,'');

  query:='select left(typ.untersuchung,150) as bescheinigung, case typ.i_arbmedvv when 0 then "Eignung" when 1 then "ArbmedVV" end grundlage, gefaehrdung_vorsorge.nummer as nummer from gefaehrdung_vorsorge left join typ on (gefaehrdung_vorsorge.i_typ=typ.nummer)';
  query:=query+format(' where (i_gef3=%s and modus=0 and gefaehrdung_vorsorge.storno=0)',[s_g3]);

  sql_new(false,q_ap12,query,'');

  query:='select left(typ.untersuchung,150) as bescheinigung, case typ.i_arbmedvv when 0 then "Eignung" when 1 then "ArbmedVV" end grundlage, gefaehrdung_vorsorge.nummer as nummer from gefaehrdung_vorsorge left join typ on (gefaehrdung_vorsorge.i_typ=typ.nummer)';
  query:=query+format(' where (i_gef3=%s and modus=1 and gefaehrdung_vorsorge.storno=0)',[s_g3]);

  sql_new(false,q_ap13,query,'');
end;

end;
end;

function tform_ap.abt_neu(kuerzel:string):int64;
var
i_firma:integer;

begin
     //kuerzel:='xaz';
    i_firma:=datamodul.q_firma['nummer'];
    result:=neuer_datensatz(datamodul.q_ap3,['nummer','i_firma','kuerzel','langtext'],[null,i_firma,kuerzel,'']);  //abteilung
    //datamodul.q_ap3.post;
    dbtreeview_k1.Selected:=dbtreeview_k1.parent_einfuegen(dbtreeview_k1.akt_knoten[1] ,datamodul.q_ap3,result,'',1);
    
end;

procedure TForm_ap.BitBtn_ap_neuClick(Sender: TObject);
begin
 ap_neu;
end;

function tform_ap.ap_neu:int64;
var
   f_tab:tform_tabellen;
   query:string;
   ap_nr,i:int64;
begin
 try

 if dbtreeview_k1.a_ebene=1 then i:=getbigint(datamodul.q_ap3,'nummer')   //abteilung
 else i:=getbigint(datamodul.q_ap4,'i_abteilung'); //ap

 result:=neuer_datensatz(datamodul.q_ap4,['nummer','name','i_abteilung','datum'],[null,'',inttostr(i),date]);
 datamodul.q_ap4.post;
 dbtreeview_k1.Selected:=dbtreeview_k1.kind_einfuegen(dbtreeview_k1.akt_knoten[1] ,datamodul.q_ap4,result,'',2,1);
 datamodul.q_ap4.Refresh;

 dbtreeview_k1.change(dbtreeview_k1.selected);
 dbtreeview_k1.name_neu(dbtreeview_k1.selected);
 //gefaehrdungen_einfuegen;
 finally
 //f_tab.release;
 end;
end;

procedure TForm_ap.BitBtn_gef_neuClick(Sender: TObject);
begin
gef_neu(0,'neu');
end;

function tform_ap.gef_neu(gefaehrdungen:int64;modus:string):int64;
var
   f_gef:tform_gefaehrdungen;
   query,s1,s2,s3:string;
   nr,ap_nr,i:int64;
   knoten_gef_gr_neu,knoten,knoten_such,knoten_sel:ttreenode;
   daten:pnodepointer;
   gef_vor:boolean;
begin
 try
   knoten_gef_gr_neu:=nil;
   knoten_such:=nil;


   query:='select * from gefaehrdungen_1 where storno=0 order by reihenfolge,name';
     datamodul.sql_new(false,datamodul.q_7,query,'');
      query:='select * from gefaehrdungen_2 where storno=0 order by i_gef1,reihenfolge, name';
     datamodul.sql_new(false,datamodul.q_8,query,'');
      query:='select * from gefaehrdungen_3 where storno = 0 order by i_gef2,reihenfolge, memo_gef ';
     datamodul.sql_new(false,datamodul.q_9,query,'');

   f_gef:=tform_gefaehrdungen.create(self);
   f_gef.Splitter.Left:=splitter_gefaehrdungen;

   //f_gef.toolbutton_ok.Caption:='Auswählen';
   //f_gef.ShowModal;
   if gefaehrdungen>0 then f_gef.dbtreeview.gehe_zu_datensatz(2,gefaehrdungen);
   f_gef.Width:=form_main.Width-40;
   f_gef.Height:=form_main.Height-140;
   if f_gef.ShowModal<> mrok then exit;

   //suche ob gefährdungsfaktor für ap vorhanden
      //wenn nicht dann neu anlegen
    nr:=getbigint(datamodul.q_8,'nummer');  //nummer für gefährdungsfaktor

    knoten:=dbtreeview_k1.selected;
    daten:=knoten.Data;
    while daten^.mysqlquery<>datamodul.q_ap4 do // zurück zum Arbeitsplatz
    begin
       knoten:=knoten.Parent;
       daten:=knoten.Data;
    end;

    ap_nr:=daten^.nummer;
    query:=format('select nummer from a_gefaehrdungsfaktor where i_ap=%s and i_gefaehrdungsfaktor=%s and storno=0 ',[inttostr(ap_nr),inttostr(nr)]);
    gef_vor:= mysql_d.sql_exe_rowsvorhanden(query); //gefährdungsgruppe vorhanden?
    knoten_sel:=dbtreeview_k1.Selected;

   case f_gef.dbtreeview.a_ebene of //was wurde augewählt
   1: showmessage('Sie haben eine Gefährdungsklasse gewählt. Bitte wählen Sie eine Gefährdungsgruppe oder Gefährdung. (Ebene 2 oder 3)');
   2:begin
      if not gef_vor then
      begin
            nr:=getbigint(datamodul.q_8,'nummer');
            i:=getbigint(datamodul.q_ap4,'nummer');
            gefaehrdungsfaktor_neu(i,nr);
       end
       else
       showmessage('Gefährdungsgruppe schon vorhanden');
     end;
   3:begin //gefährdung gewählt
      if gef_vor then  //gefährdungsgruppe ist vorhanden, muss also gesucht werden
      begin
        knoten:=knoten.getFirstChild;// GetNext ; //erster gefährdungsfaktor  oder projekt
        daten:=knoten.Data;
        while (knoten<>nil) and (knoten_such=nil) do
        begin
           daten:=knoten.data;
           if daten^.mysqlquery=datamodul.q_ap5 then // gefährdungsgruppe
           begin
             i:=daten^.nummer;
             query:=format('select i_gefaehrdungsfaktor from a_gefaehrdungsfaktor where nummer=%s ',[inttostr(i)]);
             if (strtoint64(mysql_d.Feldinhalt(query,0))=nr) then //nr = gefährdungsgruppe
             begin
               knoten_such:=knoten;
               //exit;
             end;
           end;
           knoten:=knoten.getNextSibling; //gleiche Ebene
        end;
      end;

      if knoten_such<>nil then  //(dbtreeview_k1.a_ebene=2) and  (nr<>i) then
      begin
        dbtreeview_k1.Selected:=knoten_such;
        dbtreeview_k1.change(dbtreeview_k1.selected);
      end
      else
      begin //gef

         i:=getbigint(datamodul.q_ap4,'nummer');
         gefaehrdungsfaktor_neu(i,nr);

      end;

      nr:=getbigint(datamodul.q_9,'nummer');
      i:=getbigint(datamodul.q_ap5,'nummer');

      //if datamodul.q_9['memo_gef']<>null then s1:=datamodul.q_9['memo_gef'] else s1:='';
      //if datamodul.q_9['memo_massnahme']<>null then s2:=datamodul.q_9['memo_massnahme'] else s2:='';
      s1:='';
      s2:='';
      s3:=datamodul.q_9.findfield('nummer').asstring;
      if modus='neu' then
      result:=neuer_datensatz(datamodul.q_ap6,['nummer','i_gefaehrdungsfaktor','datum','memo_gef','memo_massnahme','i_gef3'],[null,inttostr(i),date,s1,s2,s3])
      else
      begin
        datamodul.q_ap6.edit;
        datamodul.q_ap6.findfield('i_gef3').asstring:=s3;
        datamodul.q_ap6.findfield('i_gefaehrdungsfaktor').asstring:=inttostr(result);
      end;
      datamodul.q_ap6.post;
      datamodul.q_ap6.Refresh;
      if modus='neu' then
        dbtreeview_k1.Selected:= dbtreeview_k1.kind_einfuegen(  dbtreeview_k1.akt_knoten[3] ,datamodul.q_ap6,result,s1,4,1)
      else
        dbtreeview_k1.Selected:= knoten_sel;
      dbtreeview_k1.change(dbtreeview_k1.selected);
      dbtreeview_k1.name_neu(dbtreeview_k1.selected);

      //if knoten_gef_gr_neu<>nil then dbtreeview_k1.selected:=knoten_gef_gr_neu; //gefährdungsgruppe
     end;
   end;

 finally
        splitter_gefaehrdungen:=f_gef.Splitter.Left;
        f_gef.release;

 end;
end;




procedure TForm_ap.dbtreeview_k1Changing(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
 //ToolButton_saveClick(Sender);
 //a_save();
 if (not assigned(form_ap)) then exit;
 form_main.speichern;
end;

procedure TForm_ap.FormClose(Sender: TObject; var Action: TCloseAction);
VAR
a:boolean;
begin
  {a:=false;       //wird bei destroy aufgerufen
  dbtreeview_k1Changing ( Sender, nil, a);
  splitter_ap:=splitter.left;//   panel_li.width;  }
end;

procedure TForm_ap.BitBtn_vorges_wahlClick(Sender: TObject);
var
nummer_ma:int64;
begin
with datamodul do
begin
  
	if form_namsuch.showmodal= mrok then
	begin
		nummer_ma:=getbigint(datamodul.q_mitarbeiter_such,'nummer');
		q_ap3.edit;
		setbigint(datamodul.q_ap3,'i_mitarbeiter',nummer_ma);
		q_ap3['name']:=q_mitarbeiter_such['name'];
		q_ap3['vorname']:=q_mitarbeiter_such['vorname'];
		datamodul.q_ap3.post;
    // datamodul.q_2.Locate('nummer',inttostr(nummer_ma),[]);
    datamodul.q_2.Locate('nummer',(nummer_ma),[]);
      dbedit_a_name.visible:=true;
      dbedit_a_vorname.visible:=true;
      dbedit_a_telefon.visible:=true;
      dbedit_a_mail.visible:=true;
	end;
end;
end;
procedure TForm_ap.BitBtn_abt_neuClick(Sender: TObject);
begin
     abt_neu('');
end;

procedure TForm_ap.DBCheckBox_allMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
s1,s2,query:string;
i,j:integer;
begin

end;

procedure TForm_ap.ToolButton_delClick(Sender: TObject);
begin
  a_delete();
end;


procedure Tform_ap.a_delete();
var
s1,s2,query,nr_str:string;
begin
try
if dbtreeview_k1.selected=nil then exit;

if (MessageDlg( Format('Soll " %s " gelöscht werden?',
           [dbtreeview_k1.selected.text]),mtConfirmation, mbYesNoCancel, 0)<>mryes)then exit;


if dbtreeview_k1.a_tabelle=datamodul.q_ap3 then
       begin
       if  dbtreeview_k1.selected.HasChildren then
         begin
              showmessage('Bitte zuerst "Arbeitsplätze" oder "Projekte" löschen');
              exit;
         end;


         datamodul.q_ap3.edit;
         datamodul.q_ap3['storno']:=1;
         datamodul.q_ap3.post;
         dbtreeview_k1.selected:=dbtreeview_k1.knoten_entfernen(dbtreeview_k1.selected);
         dbtreeview_k1.change(dbtreeview_k1.Selected);
       end;
if dbtreeview_k1.a_tabelle=datamodul.q_ap4 then
       begin
         if  dbtreeview_k1.selected.HasChildren then
         begin
              showmessage('Bitte zuerst "Gefährdungsgruppen" oder "Projekte" löschen');
              exit;
         end;
         datamodul.q_ap4.edit;
         datamodul.q_ap4['storno']:=1;
         datamodul.q_ap4.post;
         dbtreeview_k1.selected:=dbtreeview_k1.knoten_entfernen(dbtreeview_k1.selected);
         dbtreeview_k1.change(dbtreeview_k1.Selected);
       end;
if dbtreeview_k1.a_tabelle=datamodul.q_ap5 then
       begin
        if  dbtreeview_k1.selected.HasChildren then
         begin
              showmessage('Bitte zuerst "Gefährdungen" oder "Projekte" löschen');
              exit;
         end;
        s1:=getbigint_str(datamodul.q_ap5,'i_gefaehrdungsfaktor');
        s2:=getbigint_str(datamodul.q_ap5,'i_ap');
        nr_str:=getbigint_str(datamodul.q_ap5,'nummer');
        query:='update a_gefaehrdungsfaktor set storno=1 where nummer='+nr_str;
        mysql_d.sql_exe(query);

        //datamodul.q_ap5.edit;
        //datamodul.q_ap5['storno']:=1;
        //datamodul.q_ap5.post;

        dbtreeview_k1.change_allowed:=false;
        datamodul.q_ap5.Refresh;
        dbtreeview_k1.change_allowed:=true;
        dbtreeview_k1.selected:=dbtreeview_k1.knoten_entfernen(dbtreeview_k1.selected);
        dbtreeview_k1.change(dbtreeview_k1.Selected);
       end;
if dbtreeview_k1.a_tabelle=datamodul.q_ap6 then
       begin
         datamodul.q_ap6.edit;
         datamodul.q_ap6['storno']:=1;
         datamodul.q_ap6.post;
         dbtreeview_k1.selected:=dbtreeview_k1.knoten_entfernen(dbtreeview_k1.selected);
         dbtreeview_k1.change(dbtreeview_k1.Selected);
       end;
if dbtreeview_k1.a_tabelle=datamodul.q_ap10 then
       begin
         datamodul.q_ap10.edit;
         datamodul.q_ap10['storno']:=1;
         datamodul.q_ap10.post;
         dbtreeview_k1.selected:=dbtreeview_k1.knoten_entfernen(dbtreeview_k1.selected);
         dbtreeview_k1.change(dbtreeview_k1.Selected);
       end;
finally
  toolbutton_save.enabled:=false;
  toolbutton_del.enabled:=false;
end;
end;

procedure TForm_ap.BitBtn_ap_wahlClick(Sender: TObject);
var
   f_tab:tform_tabellen;
   ap_nr,query:string;
   i:int64;
begin
 try
 ap_nr:=getbigint_str(datamodul.q_ap4,'i_arbeitsplaetze');
 query:=format('select * from arbeitsplaetze where nummer=%s ',[ap_nr ]);
 datamodul.sql_new(false,datamodul.q_1,query,'');
 f_tab:=tform_tabellen.create(self);
 f_tab.Notebook.PageIndex:=14;
 f_tab.DBgrid_tabellen.DataSource:=datamodul.ds_1;
 f_tab.DBgrid_tabellen.Columns.clear;
 f_tab.DBgrid_tabellen.Columns.add;
 f_tab.DBgrid_tabellen.Columns[0].fieldname:='name';
 f_tab.DBgrid_tabellen.Columns[0].width:=f_tab.DBgrid_tabellen.width-25;
 f_tab.DBEdit_gef.DataSource:=datamodul.ds_1;
 f_tab.DBmemo_gef.DataSource:=datamodul.ds_1;

 f_tab.ToolButton_neu.Visible:=false;
 f_tab.ToolButton_del.Visible:=false;
 if f_tab.ShowModal<>mrok then exit;

 datamodul.q_ap4.Refresh;

 dbtreeview_k1.change(dbtreeview_k1.selected);
 dbtreeview_k1.name_neu(dbtreeview_k1.selected);
 //gefaehrdungen_einfuegen;
 finally
 f_tab.release;
 end;
end;

procedure TForm_ap.BitBtn_abtlesenClick(Sender: TObject);
var i_firma:integer;
	  kuerzel:string;
begin
with datamodul do
begin
  screen.cursor:=crhourglass;
	i_firma:= getbigint(q_firma,'nummer');
	sql_new(true,q_7,format('select abteilung from mitarbeiter where i_firma=%d order by abteilung',[i_firma]),'');
  q_7.first;
  while not q_7.eof do
  begin
	 if q_7['abteilung']<>null then
		begin
		kuerzel:=q_7['abteilung'];
		if not q_ap3.Locate('i_firma,kuerzel',vararrayof([i_firma,kuerzel]),[loCaseInsensitive]) then
		begin
        abt_neu(kuerzel);
		end;
	 end;
	 q_7.next;
	end;
  q_ap3.refresh;

  form_main.abteilungskuerzel_eintragen;

  dbtreeview_k1.liste_lesen(1);
  dbtreeview_k1.change(dbtreeview_k1.selected);
  dbtreeview_k1.name_neu(dbtreeview_k1.selected);

 screen.cursor:=crdefault;
end;

end;

procedure TForm_ap.PageControlChange(Sender: TObject);
var
   tab:tzquery;
   dat:string;
begin

     

     if pagecontrol.ActivePage=tabsheet_word then
     case notebook.PageIndex of
     0:begin
            tab:=datamodul.q_ap3;
            if tab['ole']<>'' then form_main.olecontainer_laden(tab,'ole',arbene_ap);
       end;
     1:begin
            tab:=datamodul.q_ap4;
            if tab['ole']<>'' then form_main.olecontainer_laden(tab,'ole',arbene_ap);
       end;
     2:
       begin
            //tab:=datamodul.q_ap5;
            //if tab['ole']<>'' then form_main.olecontainer_laden(tab,'ole',arbene_ap);
       end;
     4:
       begin
            tab:=datamodul.q_ap10;
            if tab['ole']<>'' then form_main.olecontainer_laden(tab,'ole',arbene_ap);
       end;

     end;


end;

procedure TForm_ap.arbene_apLButtonDown(Sender: TObject);

begin
     if  arbene_ap.State in [osEmpty	] then
     begin
        //arbene_ap.Modified:=true;
        //speichern;
     case notebook.PageIndex of
       0:form_main.dok_erstellen( arbene_ap,12,'a_gefaehrdungsfaktor',datamodul.q_ap3,[''],['']);
       1:form_main.dok_erstellen( arbene_ap,12,'a_gefaehrdungsfaktor',datamodul.q_ap4,[''],['']);
       2:form_main.dok_erstellen( arbene_ap,12,'a_gefaehrdungsfaktor',datamodul.q_ap5,[''],['']);
       4:form_main.dok_erstellen( arbene_ap,12,'a_gefaehrdungsfaktor',datamodul.q_ap10,[''],['']);
     end;
      com.doku_drucken;
      arbene_ap.DestroyObject;
     end
     else oleanzeigen(arbene_ap);// arbene_ap.DoVerb(ovshow);
end;


procedure TForm_ap.Arbene_gefaehrdungsfaktorLButtonDown(Sender: TObject);
begin
{if  arbene_gefaehrdungsfaktor.State in [osEmpty	] then
     begin
         //form_main.dok_erstellen( arbene_ap,12,false,'drucken','nicht_schliessen','','a_gefaehrdungsfaktor',datamodul.q_ap5,[''],['']);
         //datamodul.q_vorlagen
         //olecontainer_laden(datamodul.q_vorlagen,'ole',arbene_ap);
     end
else arbene_gefaehrdungsfaktor.DoVerb(ovshow);}
end;

function tform_ap.gefaehrdungsfaktor_neu(i_ap,i_gefaehrdungsfaktor:int64):int64;
var
query:string;
begin
       query:='select* from a_gefaehrdungsfaktor where nummer=0';
       datamodul.sql_new(false,datamodul.q_17,query,'');
       result:=neuer_datensatz(datamodul.q_17,['nummer','i_ap','i_gefaehrdungsfaktor','datum'],[null,inttostr(i_ap),inttostr(i_gefaehrdungsfaktor),date]);
       datamodul.q_17.post;
       //i:=neuer_datensatz(datamodul.q_ap5,['nummer','i_ap','i_gefaehrdungsfaktor','datum'],[null,inttostr(i_ap),inttostr(i_gefaehrdungsfaktor),date]);
       //datamodul.q_ap5.post;
       datamodul.q_ap5.Refresh;
       dbtreeview_k1.Selected:= dbtreeview_k1.kind_einfuegen(dbtreeview_k1.akt_knoten[2] ,datamodul.q_ap5,result,'',3,1);
       dbtreeview_k1.change(dbtreeview_k1.selected);
       dbtreeview_k1.name_neu(dbtreeview_k1.selected);
end;


procedure TForm_ap.ToolButton_saveClick(Sender: TObject);
begin
  //a_save();
  form_main.speichern;
end;

procedure Tform_ap.a_save();
begin
with datamodul do
  try
       if arbene_ap.Modified then
       case notebook.PageIndex of
         0:form_main.olecontainer_speichern(true,datamodul.q_ap3,'ole',arbene_ap);
         1:form_main.olecontainer_speichern(true,datamodul.q_ap4,'ole',arbene_ap);
         2:form_main.olecontainer_speichern(true,datamodul.q_ap5,'ole',arbene_ap);
         4:form_main.olecontainer_speichern(true,datamodul.q_ap10,'ole',arbene_ap);
       end;

       arbene_ap.DestroyObject;
       pagecontrol.ActivePage:=tabsheet_main;
       if q_2.State in [dsedit] then
       begin
            q_2.Post;
            q_mitarbeiter.refresh;
       end;
       if q_ap3.State in [dsedit, dsinsert] then
       begin
            q_ap3.Post;
            //q_ap3.Refresh;
       end;
       if q_ap4.State in [dsedit, dsinsert] then q_ap4.Post;
       if q_ap5.State in [dsedit, dsinsert] then q_ap5.Post;
       if q_ap6.State in [dsedit, dsinsert] then q_ap6.Post;
       if q_ap10.state in [dsedit, dsinsert] then q_ap10.Post;

       dbtreeview_k1.name_neu(dbtreeview_k1.selected);// akt_knoten[dbtreeview_k1.a_ebene]);
       form_main.sperren(notebook);
       //form_main.speichern;
       {form_ap.toolbutton_del.Enabled:=false;
       form_ap.toolbutton_save.enabled:=false;
       form_ap.toolbutton_change.enabled:=true;}
  except
    if modus_debug then showmessage ('Fehler beim Abspeichern Abteilung index:'+ inttostr(notebook.PageIndex)) ;    
  end;
end;



procedure TForm_ap.Edit_suchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key<>vk_return then exit;
dbtreeview_k1.suche_datensatz(edit_such.text);
end;

procedure TForm_ap.ToolButton_newClick(Sender: TObject);
begin
  a_neu();
end;

function TForm_ap.a_neu():int64;
var
nr:int64;
begin
   result:=0;
   if not show_gefaehrdungen then
   begin
        result:=abt_neu('');
        //ToolButton_changeClick(Sender);
        a_change();
        exit;
   end;

    if dbtreeview_k1.a_tabelle=datamodul.q_ap10 then exit; // projekt kann keine kinder haben
    a_save();
    
   try
   //ToolButton_saveClick(Sender);
   form_u_wahl:=tform_u_wahl.Create(self);
   form_u_wahl.RadioGroup.Columns:=1;
   form_u_wahl.Panel_date.visible:=false;
   case dbtreeview_k1.a_ebene of
   -1: begin
          if modus<>'ma' then form_u_wahl.RadioGroup.Items.Add('Abteilung'); //
        end;
   0,1:begin
          if modus<>'ma' then form_u_wahl.RadioGroup.Items.Add('Abteilung'); //
          if dbtreeview_k1.Items.Count>0 then
              form_u_wahl.RadioGroup.Items.Add('Arbeitsplatz');
     end;
   2:begin
          form_u_wahl.RadioGroup.Items.Add('Arbeitsplatz');
          form_u_wahl.RadioGroup.Items.Add('Gefährdung');
     end;
   3,4:begin
          form_u_wahl.RadioGroup.Items.Add('Gefährdung');
     end;
   end;
   //if (dbtreeview_k1.a_ebene<=3) and (dbtreeview_k1.Items.count>0) then form_u_wahl.RadioGroup.Items.Add('Projekt');
   if (dbtreeview_k1.a_ebene=2) and (dbtreeview_k1.Items.count>0) then form_u_wahl.RadioGroup.Items.Add('Projekt');

   form_u_wahl.RadioGroup.itemindex:=0;
   if form_u_wahl.ShowModal<>mrok then
   begin
    result:=0;
    exit;
   end;
   result:=1;
   pagecontrol.ActivePage:=tabsheet_main;


   case dbtreeview_k1.a_ebene of
   -1: begin
           if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Abteilung' then  result:=abt_neu('');
        end;
   0,1:begin
             if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Arbeitsplatz' then  result:=ap_neu;
             if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Abteilung' then  result:=abt_neu('');
             if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Gefährdung' then  result:=gef_neu(0,'neu');
     end;
   2:begin
             if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Arbeitsplatz' then  result:=ap_neu;
             if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Abteilung' then  result:=abt_neu('');
             if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Gefährdung' then  result:=gef_neu(0,'neu');
              if form_u_wahl.RadioGroup.Items[form_u_wahl.RadioGroup.Itemindex]='Projekt' then  result:=projekt_neu(dbtreeview_k1.a_nummer,dbtreeview_k1.a_ebene);
     end;
   3,4:begin
          if form_u_wahl.RadioGroup.Itemindex=0 then
          begin
            nr:=getbigint(datamodul.q_ap5,'i_gefaehrdungsfaktor');
            result:=gef_neu(nr,'neu');
          end;
     end;
   end;
   a_change();
finally
form_u_wahl.release;
end;
end;

procedure TForm_ap.ToolButton_okClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_ap.ToolButton_abortClick(Sender: TObject);
begin
modalresult:=mrabort;
end;

procedure TForm_ap.SpeedButton1Click(Sender: TObject);
begin
dbtreeview_k1.suche_datensatz(edit_such.text);
end;

procedure TForm_ap.ToolButton_changeClick(Sender: TObject);
begin
  a_change();
end;

procedure TForm_ap.a_change();
begin
form_main.entsperren(notebook);
toolbutton_del.Enabled:=true;
toolbutton_save.enabled:=true;
if pagecontrol.TabIndex=1 then arbene_apLButtonDown(self);
{case dbtreeview_k1.a_ebene of
  1: datamodul.q_ap3.Refresh;//CurrRow;
  2: datamodul.q_ap4.Refresh;//CurrRow;
  3: datamodul.q_ap5.Refresh;//CurrRow;
  4: datamodul.q_ap6.refresh;//currrow;
end;}
end;

procedure TForm_ap.report(n:integer);
var
f_name, html_name:string;
us:thtml_gef_report;
begin
try
if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.14', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';

	us:=thtml_gef_report.create( f_name);
	us.report(dbtreeview_k1.Items,n);
finally
	us.free;
end;
	html_name:='file:\\'+f_name;
	shellexecute_se(0,'open',pchar(f_name),'','',sw_shownormal);
  dbtreeview_k1.change(dbtreeview_k1.selected);
end;


procedure TForm_ap.alleGefhrdungsfaktorenmitHandlungsbedarf1Click(
  Sender: TObject);
begin
  report(0);
end;


procedure TForm_ap.GefhrdungsfaktorenPriorittA1Click(Sender: TObject);
begin
  report(3);
end;

procedure TForm_ap.GefhrdungsfaktorenPriorittB1Click(Sender: TObject);
begin
    report(2);
end;


procedure TForm_ap.GefhrdungsfaktorenPriorittC1Click(Sender: TObject);
begin
     report(1);
end;

procedure TForm_ap.dbtreeview_k1DblClick(Sender: TObject);
var nr:int64;
begin
  case dbtreeview_k1.a_ebene of
  1:  modalresult:=mrok;
  3,4: begin
          nr:=getbigint(datamodul.q_ap5,'i_gefaehrdungsfaktor');
          gef_neu(nr,'neu');
     end;
  end;
end;

procedure TForm_ap.Edit_suchEnter(Sender: TObject);
begin
//dbtreeview_k1.Selected:=dbtreeview_k1.Items[0];
//dbtreeview_k1.change(dbtreeview_k1.selected);
end;

procedure TForm_ap.alleAbteilungenArbeitspltzeGefhrdungen1Click(Sender: TObject);
begin
    report(4);
end;



procedure TForm_ap.m_alleProjekteClick(Sender: TObject);
begin
if m_alleprojekte.Checked then m_alleprojekte.Checked:=false else m_alleprojekte.Checked:=true;
ini(p_firma,p_modus,p_ab_nummer);
end;

procedure TForm_ap.BitBtn_adobeClick(Sender: TObject);
var
   p,seite:string;
begin
        if datamodul.q_ap5['pdf_datei']=null then exit;
        if datamodul.q_ap5['pdf_datei']='' then exit;
        p:=pfad_pdf+datamodul.q_ap5['pdf_datei'];
        seite:=datamodul.q_ap5.findfield('pdf_seite').AsString;
        //p:='acrord32 /A page='+seite+' '+p;
        shellexecute_se(0,'open',pchar(p),pchar(''),nil,SW_showmaximized);
        //ShellExecute_se(handle, 'open','acrord32', PChar( p), nil, SW_showmaximized);
end;

function tform_ap.projekt_neu(i_nummer:int64;e:integer):int64;
var
p_name:string;
begin

     result:=neuer_datensatz(datamodul.q_ap10,['nummer','i_nummer','i_ebene','datum','projektname','dat_erledigen','i_handlungsbedarf','w_user'],[null,inttostr(i_nummer),e,date,'Projekt','1900-01-01',3,0]);
     datamodul.q_ap10.post;
     if datamodul.q_ap10['projektname']<>null then p_name:=datamodul.q_ap10['projektname'] else p_name:='Projekt';
     dbtreeview_k1.Selected:=dbtreeview_k1.kind_einfuegen( dbtreeview_k1.akt_knoten[e] ,datamodul.q_ap10,result,p_name,e,0);
end;


procedure TForm_ap.FormDestroy(Sender: TObject);
VAR
a:boolean;
begin
  a:=false;
  //dbtreeview_k1Changing ( Sender, nil, a);
  splitter_ap :=panel_li.width;
  dbtreeview_k1.liste_loeschen;
  DBCombo_num_texte_user.loeschen;
end;

procedure TForm_ap.dbtreeview_k1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var
anitem:ttreenode;
data:pnodepointer;
e:integer;
begin
  accept:=false;
  exit;// noch nicht ok

  data:=dbtreeview_k1.selected.data;

  e:=data^.ebene;

  if not (data.mysqlquery=datamodul.q_ap4) then exit;
  //if  (data.mysqlquery=datamodul.q_ap10) then exit;

  AnItem := dbtreeview_k1.GetNodeAt(X, Y);
  data:=anitem.Data;
  //if ((data.mysqlquery=datamodul.q_ap3) or (data.mysqlquery=datamodul.q_ap4) or (data.mysqlquery=datamodul.q_ap5))
  if (data.mysqlquery=datamodul.q_ap3)
  then Accept :=true else  accept:=false;

end;

procedure TForm_ap.dbtreeview_k1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
   daten_sender, daten_source,d:pnodepointer;
   e_sender, e_source:integer;
   nr:int64;
  AnItem,p: TTreeNode;
  AttachMode: TNodeAttachMode;
  HT: THitTests;
begin
inherited;
try
  if dbtreeview_k1.Selected = nil then Exit;
  HT := dbtreeview_k1.GetHitTestInfoAt(X, Y);
  AnItem := dbtreeview_k1.GetNodeAt(X, Y);
  if (HT - [htOnItem, htOnIcon, htNowhere, htOnIndent] <> HT) then
  begin

    attachmode:=naAddChildFirst	;
    daten_sender:=anitem.data;
    e_sender:=daten_sender^.ebene;
    daten_source:=dbtreeview_k1.selected.data;

    daten_source^.ebene:=daten_sender^.ebene;
    nr:=daten_sender^.nummer;
    datamodul.q_ap10.edit;
    datamodul.q_ap10.FindField('i_nummer').AsString := inttostr(nr);
    datamodul.q_ap10.FindField('i_ebene').Asinteger := daten_sender^.ebene;
    datamodul.q_ap10.post;
    dbtreeview_k1.Selected.MoveTo(AnItem, AttachMode);


  end;
    {if dbtreeview_k1.selected.parent<>anitem then
    begin
      ftabellen[e_source].edit;
      ftabellen[e_source].FindField(fkey[e_source]).AsString := inttostr(nr);
      ftabellen[e_source].post;
      Selected.MoveTo(AnItem, AttachMode);
    end;}
    //if (htOnItem in HT) or (htOnIcon in HT) then AttachMode := naAddChild

    //else if htNowhere in HT then AttachMode := naAdd
    //else if htOnIndent in HT then AttachMode := naInsert;

    //Selected.MoveTo(AnItem, AttachMode);
finally

end;

end;

procedure TForm_ap.SpeedButton_expandClick(Sender: TObject);
begin
dbtreeview_k1.fullexpand;// .Expand_e1;
end;

procedure TForm_ap.SpeedButton_collapsClick(Sender: TObject);
begin
dbtreeview_k1.FullCollapse;
end;

procedure TForm_ap.m_Unfall_in_AbteilungClick(Sender: TObject);
begin
  Unfall_in_Abteilung();
end;

procedure TForm_ap.Unfall_in_Abteilung();
begin
  form_ueb_arbunfall:=tform_ueb_arbunfall.create(self);
  form_ueb_arbunfall.nur_abt:=true;
  form_ueb_arbunfall.ShowModal;
  form_ueb_arbunfall.Release;
end;

procedure TForm_ap.m_Unfall_in_firmaClick(Sender: TObject);
begin
  Unfall_in_Firma();
end;

procedure TForm_ap.Unfall_in_firma();
begin
  form_ueb_arbunfall:=tform_ueb_arbunfall.create(self);
  form_ueb_arbunfall.nur_abt:=false;
  form_ueb_arbunfall.ShowModal;
  form_ueb_arbunfall.Release;
end;

procedure TForm_ap.FormCreate(Sender: TObject);
var
   dat,query:string;
begin
     Width:=form_main.Width;
     Height:=form_main.Height;
     form_main.form_positionieren(tform(sender));
     //dat:=vorlagenverzeichnis+'\gefanalyse.pdf';
     //if fileexists(dat) then pdf_gef.LoadFile(dat);
     panel_li.width:=splitter_ap;

     if prg_typ=2 then
     begin
          m_unfall_in_abteilung.visible:=false;
          m_unfall_in_firma.visible:=false;

     end;
     query:='select * from gefaehrdungen_3 where nummer=0';
     datamodul.sql_new(false, datamodul.q_ap13,query,'');
     m_report.Visible:=show_gefaehrdungen;
     pagecontrol.ActivePage:=tabsheet_main;

     dbtreeview_k1Change(Sender,dbTreeview_k1.selected);
     DBCombo_num_texte_user.lookup_laden;
     pfad_pdf:=wurzelpfad(application.ExeName)+'\infotexte\';

end;

procedure TForm_ap.SpeedButton_firna_text_wdvClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_ap10.edit;
		datamodul.q_ap10['dat_erledigen'] :=int(form_kalender.auswahlkalender.date);
      if datamodul.q_ap10['w_user']=0 then datamodul.q_ap10['w_user']:=akt_untersucher;
      //DBCombo_num_texte_user.Text:=
   end;
end;

procedure TForm_ap.DBEdit6Change(Sender: TObject);
begin
if tdbedit(sender).text='01.01.1900' then
	begin
   	tdbedit(sender).visible:=false;
      label2.Visible:=false;
      DBCombo_num_texte_user.Visible:=false;
   end
   else
   begin
   	tdbedit(sender).visible:=true;
      label2.Visible:=true;
      DBCombo_num_texte_user.Visible:=true;
   end;
end;

procedure TForm_ap.BitBtn_gefwahlClick(Sender: TObject);
var
gf,nr:int64;
begin
gf:=getbigint(datamodul.q_ap6,'i_gefaehrdungsfaktor');
nr:=getbigint(datamodul.q_ap6,'nummer');
gef_neu(gf,'update');
dbtreeview_k1.liste_lesen(1);
dbtreeview_k1.gehe_zu_datensatz(4,nr);
end;

procedure TForm_ap.m_Vorsorge_pruefClick(Sender: TObject);
begin
  vorsorge_pruefen();
end;

procedure tform_ap.vorsorge_pruefen();
begin

  form_vorsorge_abgleich:=tform_vorsorge_abgleich.create(self);

  form_vorsorge_abgleich.ebene:=dbtreeview_k1.a_ebene;

  form_vorsorge_abgleich.ShowModal;

  form_vorsorge_abgleich.release;
  form_main.dbTreeView.liste_lesen;  ///?????
end;

procedure TForm_ap.AuswertungmitausgehlterAbteilungArbeitsplatz1Click(
  Sender: TObject);
begin
  form_main.AuswertungberdieAbteilung1Click(Sender);
end;

end.
