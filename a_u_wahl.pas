unit a_u_wahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, radiogroup_ext;


type
  TForm_u_wahl = class(TForm)
    RadioGroup: TRadioGroup;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel_date: TPanel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    MaskEdit_date: TMaskEdit;
    GroupBox: TGroupBox;
    ComboBox: TComboBox;
    Panel2: TPanel;
    Panel3: TPanel;
    Timer: TTimer;
    procedure RadioGroupClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    { Private-Deklarationen }
    t:ttime;
  public
    { Public-Deklarationen }
    procedure auswahl_einstellen(i_alt,modus:integer);
  end;

var
  Form_u_wahl: TForm_u_wahl;

implementation

uses a_kalender, a_main;

{$R *.DFM}

procedure TForm_u_wahl.RadioGroupClick(Sender: TObject);
var auswahl:string;
var
tn:ttime;
begin

tn:=time();
if (tn-t<0.00005) and (tn-t>0.000005) then
begin
//timer.Enabled:=true;
end;
t:=tn;

 
auswahl:=radiogroup.Items[radiogroup.itemindex];
if auswahl='Befund' then
   begin
        form_main.kapitel_lesen(combobox.items,0);
        if combobox.items.count>=0 then combobox.ItemIndex:=0;
        case prg_typ of
             4:  combobox.Visible:=true;
        end;
   end
else combobox.Visible:=false;
end;

procedure TForm_u_wahl.SpeedButton1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
   begin
	  maskedit_date.text:=datetostr(int(form_kalender.auswahlkalender.date));
   end;
 
end;

procedure TForm_u_wahl.auswahl_einstellen(i_alt,modus:integer);
begin
	//maskedit_date.text:=datetostr(date               );
   radiogroup.Items.Clear;
   if modus=0 then //0 neu aus main, 1: neu aus termine also nur impfung labor untersuchung
   begin
     radiogroup.ItemIndex:=-1;
     if m_Befunde_C and (b.b_string_silent('proband-befunde')>=2) then form_u_wahl.radiogroup.items.Add('Befund');
     if m_Vorsorge_c and ((b.b_string_silent('proband-bescheinigung')>=2) or (b.b_string_silent('proband-bescheinigung-ag')>=2))  then
      case prg_typ  of
            1:form_u_wahl.radiogroup.items.Add('Bescheinigung');
            2:form_u_wahl.radiogroup.items.Add('Bescheinigung');
            3:form_u_wahl.radiogroup.items.Add('Behandlung');
            4:form_u_wahl.radiogroup.items.Add('Behandlung');
      end;

      case prg_typ of
      1,2,3: begin
        if m_Impfung_c and (b.b_string_silent('proband-impfung')>=2) then form_u_wahl.radiogroup.items.Add('Impfung');
        if m_Labor_c and (b.b_string_silent('proband-labor')>=2) then  form_u_wahl.radiogroup.items.Add('Labor');
        if m_Ambulanz_c and ((b.b_string_silent('proband-ambulanz')>=2) or (b.b_string_silent('proband-arbeitsunfall')>=2)) then  form_u_wahl.radiogroup.items.Add('Ambulanz');
        if m_Diagnosen_c and (b.b_string_silent('proband-diagnosen')>=2) then  form_u_wahl.radiogroup.items.Add('Diagnose');
        if m_Besonderheiten_c and (b.b_string_silent('proband-besonderheiten')>=2) then form_u_wahl.radiogroup.items.Add('Besonderheit');
        if m_Dokumente_c and (b.b_string_silent('proband-dokumente-arzt')>=2) then form_u_wahl.radiogroup.items.Add('Dokument');
        if m_artikel_c and (b.b_string_silent('proband-artikel')>=2) then form_u_wahl.radiogroup.items.Add('Artikel');
        if m_Rechnungen_c and (b.b_string_silent('proband-rechnung')>=2) then form_u_wahl.radiogroup.items.Add('Rechnung');
        if m_kontakte_c then form_u_wahl.radiogroup.items.Add('Kontakte');
        if m_gdt_c and (b.b_string_silent('proband-dokumente-arzt')>=2) then form_u_wahl.radiogroup.items.Add('GDT');

      end;
      4: begin
         if m_Dokumente_c and (b.b_string_silent('proband-dokumente')>=2) then form_u_wahl.radiogroup.items.Add('Dokument');
         if m_Diagnosen_c and (b.b_string_silent('proband-diagnosen')>=2) then  form_u_wahl.radiogroup.items.Add('Diagnose');
         if m_Besonderheiten_c and (b.b_string_silent('proband-besonderheiten')>=2) then form_u_wahl.radiogroup.items.Add('Besonderheit');
         if m_Labor_c and (b.b_string_silent('proband-labor')>=2) then  form_u_wahl.radiogroup.items.Add('Labor');
         if m_kontakte_c then form_u_wahl.radiogroup.items.Add('Kontakte');
        end;
      end;
      if i_alt<=radiogroup.items.count-1 then radiogroup.ItemIndex:=i_alt;
     end
   else
   begin
       form_u_wahl.radiogroup.items.Add('Vorsorgeuntersuchung');
       if show_impfung then form_u_wahl.radiogroup.items.Add('Impfung');
       if show_labor then form_u_wahl.radiogroup.items.Add('Labor');
       panel_date.Visible:=false;
       radiogroup.Columns:=1;
       radiogroup.ItemIndex:=0;
   end;
end;

procedure TForm_u_wahl.FormShow(Sender: TObject);
begin
		 form_main.form_positionieren(tform(sender));
      groupbox.Visible:=false;
end;

procedure TForm_u_wahl.FormCreate(Sender: TObject);
var i:integer;
begin
i:=0;
i:=i+1;
tag:=i;
t:=time();
end;

procedure TForm_u_wahl.FormDblClick(Sender: TObject);
begin
  modalresult:=mrok;
end;

procedure TForm_u_wahl.TimerTimer(Sender: TObject);
begin
timer.Enabled:=false;
modalresult:=mrok;

end;

end.
