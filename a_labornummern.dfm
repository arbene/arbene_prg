object Form_labornummer: TForm_labornummer
  Left = 452
  Top = 381
  Width = 526
  Height = 251
  Caption = 'Labornummer ausw'#228'hlen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 40
    Top = 49
    Width = 58
    Height = 16
    Caption = 'Vorspann'
  end
  object Label2: TLabel
    Left = 203
    Top = 51
    Width = 51
    Height = 16
    Caption = 'Nummer'
  end
  object edit_vorspann: TEdit
    Left = 40
    Top = 72
    Width = 121
    Height = 24
    TabOrder = 0
  end
  object UpDown1: TUpDown
    Left = 407
    Top = 68
    Width = 26
    Height = 33
    TabOrder = 1
    OnClick = UpDown1Click
  end
  object Edit_nummer: TEdit
    Left = 200
    Top = 72
    Width = 200
    Height = 24
    TabOrder = 2
  end
  object Panel1: TPanel
    Left = 0
    Top = 154
    Width = 510
    Height = 59
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object BitBtn1: TBitBtn
      Left = 242
      Top = 16
      Width = 129
      Height = 31
      TabOrder = 1
      Kind = bkCancel
    end
    object BitBtn2: TBitBtn
      Left = 99
      Top = 16
      Width = 110
      Height = 31
      TabOrder = 0
      Kind = bkOK
    end
  end
end
