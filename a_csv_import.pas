unit a_csv_import;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, CSVReader, StdCtrls, Buttons, ExtCtrls, Menus, ComCtrls;

type
  TForm_csv_import = class(TForm)
    CSVReader: TCSVReader;
    StringGrid: TStringGrid;
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    OpenDialog: TOpenDialog;
    MainMenu1: TMainMenu;
    m_Importieren: TMenuItem;
    Timer1: TTimer;
    ProgressBar: TProgressBar;
    procedure m_ImportierenClick(Sender: TObject);
    procedure CSVReaderBeforeCopyToGrid(row, col: Integer;
      var Value: String);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure StringGridClick(Sender: TObject);
  private
    { Private-Deklarationen }
    dat:string;
    procedure ClearGrid;
  public
    { Public-Deklarationen }
    function csv_importieren(dat:string):boolean;
    procedure spaltentitel_angleichen(l_csv_arbene,l_csv:tstringlist );
  end;

var
  Form_csv_import: TForm_csv_import;

implementation

uses a_main;

{$R *.dfm}

procedure TForm_csv_import.m_ImportierenClick(Sender: TObject);
begin
  opendialog.Execute;
  dat:=opendialog.FileName;
  if fileexists(dat) then csv_importieren(dat);
end;



function TForm_csv_import.csv_importieren(dat:string):boolean;
begin
  if not fileexists(dat) then exit;
  ClearGrid;
  csvreader.LoadFromFilename(dat);
end;

procedure TForm_csv_import.ClearGrid;
var
  i,j : integer;
begin
  StringGrid.RowCount := 4;
  StringGrid.ColCount := 4;

  for i := 0 to 3 do
    for j := 0 to 3 do
      StringGrid.Cells[j,i] := '';

end;

procedure TForm_csv_import.CSVReaderBeforeCopyToGrid(row, col: Integer; var Value: String);
begin
  if row = 0 then
    Value := Value;
end;

procedure TForm_csv_import.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));

end;

procedure tform_csv_import.spaltentitel_angleichen(l_csv_arbene,l_csv:tstringlist );
var
  i,z : integer;
  s:string;
begin
  l_csv.CaseSensitive:=false;
  for i := 0 to stringgrid.ColCount-1 do
  begin
      s:=StringGrid.Cells[i,0] ;
      z:=l_csv.IndexOf(s);
      if z>-1 then StringGrid.Cells[i,0]:=l_csv_arbene[z];
  end;
end;

procedure TForm_csv_import.Timer1Timer(Sender: TObject);
begin
progressbar.Position:=progressbar.Position+1;
if progressbar.Position>=60 then modalresult:=mrok;
end;

procedure TForm_csv_import.StringGridClick(Sender: TObject);
begin
timer1.Enabled:=false;
end;

end.
