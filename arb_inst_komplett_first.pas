unit arb_inst_komplett_first;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,ShellApi,Registry;

type
  TForm_komplett_first = class(TForm)
    BitBtn1: TBitBtn;
    memo: TMemo;
    procedure FormCreate(Sender: TObject);

  private
      dt:string;
    { Private-Deklarationen }
    procedure pruefe_arbenedb;
    procedure pruefe_arbeneexe;
    procedure stopp_db;

  public
    { Public-Deklarationen }
  end;

function CopyDir(const fromDir, toDir: string): Boolean;


var
  Form_komplett_first: TForm_komplett_first;

implementation

uses Inst_form;

{$R *.dfm}

procedure TForm_komplett_first.FormCreate(Sender: TObject);
begin
  Top:=(screen.height div 2)-(Height div 2);
	Left:= (screen.width div 2) - (width div 2);

	dt:=datetimetostr(now());
  dt:=stringreplace(dt,' ','_',[rfReplaceAll	]);
  dt:=stringreplace(dt,'.','_',[rfReplaceAll	]);
  dt:=stringreplace(dt,':','_',[rfReplaceAll	]);
  stopp_db;
  pruefe_arbeneexe;
  pruefe_arbenedb;
  if memo.lines.Count=0 then modalresult:=mrok;
end;

procedure TForm_komplett_first.stopp_db;
var
befehl,parameter:string;
t:ttime;
begin
      befehl:='net.exe';
       parameter:='stop mysql';

        shellexecute(Form_komplett_first.handle,'open',pchar(befehl),pchar(parameter),nil,SW_hide);
        memo.lines.Add(' ');
        memo.Lines.Add('Der Dienst MYSQL wird angehalten -bitte warten');
         application.ProcessMessages;
              application.BringToFront;
        application.ProcessMessages;
        t:=time();
        while t+0.0001 >time() do;
        memo.Lines.Add('Der Dienst MYSQL wurde angehalten');
         application.ProcessMessages;
              application.BringToFront;
end;

procedure TForm_komplett_first.pruefe_arbeneexe;
var
pfad:string;
reg:tregistry;
 f : file;

begin
try

  reg := Tregistry.create;
//key="HKCU\Software\klawitter\arbene" param="version" value="%version%"/>
//<reg key="HKCU\Software\klawitter\arbene" param="path" value="%InstallPath%"/>
		 reg.RootKey:=hkey_current_user;
		 reg.openkey('software\klawitter\arbene\',false);
		 pfad:=reg.readstring('path');

     //
     if fileexists(pfad+'\arbene.exe') then
     begin
        if messagedlg('Soll die bestehende Programmdatei umbenannt werden:'+#10+pfad+'\arbene.exe',mtconfirmation,[mbok,mbabort],0)=mrok   then
        begin
            memo.lines.Add(' ');
            try
              AssignFile(f, pfad+'\arbene.exe');
              rename( f,pfad+'\arbene'+dt+'.exe');
              memo.Lines.Add('Arbene.exe wurde in ' +pfad+'\arbene'+dt+'.exe'+' umbenannt');

            except
              memo.Lines.Add('Arbene.exe konnte nicht umbenannt werden - bitte manuell l�schen');
            end;

            try
              AssignFile(f, pfad+'\change_db.txt');
              rename( f,pfad+'\change_db'+dt+'.txt');
              memo.Lines.Add('change_db.txt wurde in ' +pfad+'\change_db'+dt+'.txt'+' umbenannt');
            except
              memo.Lines.Add('change_db.txt konnte nicht umbenannt werden - bitte manuell l�schen');
            end;
            application.ProcessMessages;
              application.BringToFront;
        end;
     end;  //
finally
		 reg.Free;
end;

end;


procedure TForm_komplett_first.pruefe_arbenedb;
begin
if fileexists('c:\mariadb55\data\arbene\zzz.frm ')
then
begin

    COPYdir('c:\mariadb55\data\arbene','c:\mariadb55\data\arbene'+dt);
    memo.lines.Add(' ');
    memo.lines.Add('Die Datenbank "arbene" wurde gesichert nach "arbene'+dt+'"');
     application.ProcessMessages;
     application.BringToFront;
end;

end;

function CopyDir(const fromDir, toDir: string): Boolean;
var
  fos: TSHFileOpStruct;
begin
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc  := FO_COPY;
    fFlags := FOF_FILESONLY;
    pFrom  := PChar(fromDir + #0);
    pTo    := PChar(toDir)
  end;
  Result := (0 = ShFileOperation(fos));
end;

end.
