{$A8,B-,C-,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$WARN SYMBOL_DEPRECATED ON}
{$WARN SYMBOL_LIBRARY ON}
{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_LIBRARY ON}
{$WARN UNIT_PLATFORM OFF}
{$WARN UNIT_DEPRECATED ON}
{$WARN HRESULT_COMPAT ON}
{$WARN HIDING_MEMBER ON}
{$WARN HIDDEN_VIRTUAL ON}
{$WARN GARBAGE ON}
{$WARN BOUNDS_ERROR OFF}
{$WARN ZERO_NIL_COMPAT ON}
{$WARN STRING_CONST_TRUNCED ON}
{$WARN FOR_LOOP_VAR_VARPAR ON}
{$WARN TYPED_CONST_VARPAR OFF}
{$WARN ASG_TO_TYPED_CONST ON}
{$WARN CASE_LABEL_RANGE ON}
{$WARN FOR_VARIABLE ON}
{$WARN CONSTRUCTING_ABSTRACT ON}
{$WARN COMPARISON_FALSE ON}
{$WARN COMPARISON_TRUE ON}
{$WARN COMPARING_SIGNED_UNSIGNED ON}
{$WARN COMBINING_SIGNED_UNSIGNED ON}
{$WARN UNSUPPORTED_CONSTRUCT OFF}
{$WARN FILE_OPEN ON}
{$WARN FILE_OPEN_UNITSRC ON}
{$WARN BAD_GLOBAL_SYMBOL ON}
{$WARN DUPLICATE_CTOR_DTOR ON}
{$WARN INVALID_DIRECTIVE ON}
{$WARN PACKAGE_NO_LINK OFF}
{$WARN PACKAGED_THREADVAR OFF}
{$WARN IMPLICIT_IMPORT ON}
{$WARN HPPEMIT_IGNORED ON}
{$WARN NO_RETVAL ON}
{$WARN USE_BEFORE_DEF ON}
{$WARN FOR_LOOP_VAR_UNDEF ON}
{$WARN UNIT_NAME_MISMATCH ON}
{$WARN NO_CFG_FILE_FOUND OFF}
{$WARN MESSAGE_DIRECTIVE ON}
{$WARN IMPLICIT_VARIANTS OFF}
{$WARN UNICODE_TO_LOCALE OFF}
{$WARN LOCALE_TO_UNICODE OFF}
{$WARN IMAGEBASE_MULTIPLE OFF}
{$WARN SUSPICIOUS_TYPECAST ON}
{$WARN PRIVATE_PROPACCESSOR ON}
{$WARN UNSAFE_TYPE OFF}
{$WARN UNSAFE_CODE OFF}
{$WARN UNSAFE_CAST ON}
unit a_main;

{um mit locate auf int64 zu positionieren muss in zdatasetutils zwei mal stLong ausgesternt werden,
wird dann als String konvertiert}

interface

uses
	Windows, Messages, SysUtils,StrUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, DBCtrls, ComCtrls, ToolWin, ExtCtrls, Buttons,  Mask,
	Menus, Grids, DBGrids, dbgridEXT, Db, DBTables, a_data, ImgList,
	editor,listen, Spin,  OleCtrls, olectnrs, ZipMstr19, DCPcrypt2, DCPsha1,
  DdeMan, DCPblockciphers, DCPtwofish, ExtDlgs, LZRW1, Aligrid,
  Edit_waehrung, xygraph, audiogramm, dbTreeView, DBEdit_time,
  olecontainerext, dbcombo_number  , registry,
	CheckLst,math,  a_com,	a_soundex,
  	mysql_direkt, TreeView_ext,	a_berechtigung,  HCMngr,  
	 ActnList, dbTreeView_stufe, {sgr_def,}shellapi,
	 excel2000, dbtreeview_kaskade, {PdfLib_TLB,} 
  	FileCtrl, Clipbrd,  ZDataset,
    a_hdnummer, HH, HH_FUNCS, D6OnHelpFix,arbene_help,  variants,types,SqlTimSt,shlobj,
     SevenPDFComObj_TLB,ComObj, IdBaseComponent, IdVCard, ACEVCard,activex,Tlhelp32,AxCtrls,WinSvc,NTDLLDebugNOP ,ZDbcIntfs,inifiles;

type
Tseiten = (links, rechts);

type IntArray = array[1..10] of Integer;
type stringArray = array[1..10] of string;


type
		pnodepointer=^nodewert;
		nodewert = record
		nummer: int64; //nummer des datensatzes
		bereich:int64;  //
    ebene:int64;  //  integer
		rubrik:int64; //integer
    datum:tdate;
		mysqlquery:tzquery; //tabelle
    liste: tstringlist;
end;

type
		pstrpointer=^wert;
		wert = record
		s: string; //nummer des datensatzes
end;

type
		pkontaktpointer=^kontakte;
		kontakte = record
		 nummer: int64; //nummer des datensatzes
     p_name:string;
		 name:string;
     vorname:string;
     titel:string;
		 zhd:string;
     strasse:string;
     plz:string;
     ort:string;
     fach:string;
     email:string;
     telefon:string;
     fax:string;
		www:string;
     memo:string;
end;

type  ptabvergleich=^tabvergleich;
      tabvergleich= record
      tabelle:string;
      main:integer;
      query: string;
      count_sat:int64;
      count_hs:int64;
end;

type
  PCardinalArray= ^TCardinalArray;
  TCardinalArray= array[0..4] of cardinal;

type PtrUInt = DWord;

type ptrint=^integer;

type TIntSet = set of 1..250;

type  twoint64= array [0..1] of int64;

type t5stringarray=array [0..4] of string;

type
  TExecuteWaitEvent = procedure(const ProcessInfo: TProcessInformation;
                                    var ATerminate: Boolean) of object;

type  thist_set=^history_set;
      history_set= record
      zquery:tzquery;
      datasource:tdatasource;
      grid:tdbgridext;
      groupbox:tgroupbox;
      tab_name:string;
      query:string;
      anzeigetext:string;
end;




type
	TForm_main = class(TForm)  //Hilfeindex 1--99
	  Panel_main: TPanel;
    pagecontrol_main: TPageControl;
	  TabSheet_stammdaten: TTabSheet;
	  MainMenu: TMainMenu;
	  m_einstellungen: TMenuItem;
	  menue_ende: TMenuItem;
	  Auswertungen1: TMenuItem;
	  StatusBar: TStatusBar;
	  FontDialog1: TFontDialog;
	  OpenDialog: TOpenDialog;
	  ColorDialog: TColorDialog;
    m_Terminuebersicht: TMenuItem;
	  PopupMenu_bereich: TPopupMenu;
    Panel_t: TPanel;
    m_Untersuchung: TMenuItem;
    m_Impfungen: TMenuItem;
    m_lab: TMenuItem;
    m_Fristen: TMenuItem;
    m_bediener: TMenuItem;
	  m_KBesonderheiten: TMenuItem;
	  OpenPictureDialog: TOpenPictureDialog;
	  Label14: TLabel;
	  TabSheet_firma: TTabSheet;
	  PageControl_firma: TPageControl;
	  TabSheet_FirmenAdresse: TTabSheet;
	  GroupBox_firma: TGroupBox;
	  Panel_ausw: TPanel;
	  TabSheet_Ansprechpartner: TTabSheet;
	  GroupBox_ansprech: TGroupBox;
	  DBgridEXT_ansprechpartner: TDBgridEXT;
	  Panel_ansprechpartner_re: TPanel;
	  GroupBox_ansp_anmerk: TGroupBox;
	  TabSheet_sollist: TTabSheet;
    Panel_zeitabrechnung_re: TPanel;
    GroupBox_fsi_4: TGroupBox;
	  Panel_zeitabrechnung_li: TPanel;
	  GroupBox_sollist: TGroupBox;
    DBgridEXT_zeitabrechnung: TDBgridEXT;
    m_Importieren: TMenuItem;
	  PageControl_stammdaten: TPageControl;
    TabSheet_ma_person: TTabSheet;
	  TabSheet_ma_firma: TTabSheet;
    Abteilung: TLabel;
	  Telefon: TLabel;
	  Label37: TLabel;
	  DBEdit_telefon: TDBEdit;
	  DBEdit_mailfirma: TDBEdit;
	  plz: TLabel;
	  Strasse: TLabel;
	  Geburtsdatum: TLabel;
	  Vorname: TLabel;
    NName: TLabel;
	  Ort: TLabel;
	  Label21: TLabel;
	  Label38: TLabel;
	  Label47: TLabel;
	  Label48: TLabel;
	  Label49: TLabel;
	  DBEdit_name: TDBEdit;
    dbedit_vorname: TDBEdit;
	  DBEdit_gebdat: TDBEdit;
    DBEdit_plz: TDBEdit;
	  DBEdit_ort: TDBEdit;
	  DBEdit_strasse: TDBEdit;
	  DBEdit_teleprivat: TDBEdit;
	  DBEdit_mailprivat: TDBEdit;
	  DBEdit_geburtsname: TDBEdit;
	  Label50: TLabel;
	  Label51: TLabel;
	  Label52: TLabel;
    Label53: TLabel;
	  DBEdit_beginn: TDBEdit;
	  DBEdit_ende: TDBEdit;
	  Label54: TLabel;
	  DBEdit_titel: TDBEdit;
	  Datei: TMenuItem;
	  menue_edit: TMenuItem;
    menue_save: TMenuItem;
	  menue_delete: TMenuItem;
	  N1: TMenuItem;
	  Ansicht1: TMenuItem;
	  Menue_New: TMenuItem;
	  TabSheet_firma_texte: TTabSheet;
	  GroupBox_firma_text: TGroupBox;
    DBgridEXT_texte: TDBgridEXT;
    Panel_firma_text: TPanel;
	  GroupBox_firma_text_top: TGroupBox;
    ScrollBox_firma_text: TScrollBox;
	  DBEdit_firma_text_datum: TDBEdit;
	  DBEdit_firma_text_titel: TDBEdit;
	  Label1: TLabel;
	  Label58: TLabel;
	  Textvorlagen1: TMenuItem;
    m_NamenSuchen: TMenuItem;
	  m_Berechtigungen_bearb: TMenuItem;
	  Hilfe1: TMenuItem;
	  hilfe_inhalt: TMenuItem;
    m_maverschieben: TMenuItem;
    Arbeitsmedizin_Firma: TOLeContainerext;
	  m_mazusammenfuehren: TMenuItem;
	  Nationen1: TMenuItem;
    m_Impfabfolge: TMenuItem;
	  Panel_tl: TPanel;
	  Label_firma: TLabel;
	  PrintDialog: TPrintDialog;
	  Personalnummer: TLabel;
    DBEdit_persnummer: TDBEdit;
    m_probandenbearb: TMenuItem;
	  Listen1: TMenuItem;
    DBEdit_schicht_s_datum: TDBEdit;
	  Label70: TLabel;
    Label_schicht_s_datum: TLabel;
	  Label86: TLabel;
	  Label69: TLabel;
    DBEdit_gef_tarif: TDBEdit;
    m_Schichtmodelle: TMenuItem;
    m_dbabgleich: TMenuItem;
    Berufsgenossenschaften1: TMenuItem;
	  email_email: TMenuItem;
    m_Drucken: TMenuItem;
    N5: TMenuItem;
	  email: TMenuItem;
	  Timer_icd: TTimer;
	  SerienbriefFelder1: TMenuItem;
	  Gewerbeaufsichtsmter1: TMenuItem;
	  m_taetigkeiten: TMenuItem;
	  m_VerteilteSysteme: TMenuItem;
    m_SystemTyp: TMenuItem;
	  m_Satellitschluessel: TMenuItem;
	  m_Satellitini: TMenuItem;
    UntersuchungsBefunde1: TMenuItem;
	  Normalbefund1: TMenuItem;
	  Auffllig1: TMenuItem;
	  Zurcksetzen1: TMenuItem;
    BitBtn_abteilung: TBitBtn;
	  Panel17: TPanel;
	  Panel18: TPanel;
    arztelist: TMenuItem;
	  Panel_tr: TPanel;
    ComboBox_history: TComboBox;
	  TabSheet_main: TTabSheet;
	  Panel_tree: TPanel;
    notebook_main: TNotebook;
    Panel_befunde: TPanel;
	  Panel_untersuchung: TPanel;
    Panel_impfung: TPanel;
    Panel_labor: TPanel;
	  Splitter2: TSplitter;
	  GroupBox_unt_oben: TGroupBox;
    Untersuchungsdatum: TLabel;
    Untersuchungstyp: TLabel;
    SpeedButton_dat: TSpeedButton;
    Label_arzt: TLabel;
	  DBEdit_u_date: TDBEdit;
	  BitBtn_untwahl: TBitBtn;
    GroupBox_anmerkung_arbeitgeber: TGroupBox;
	  Panel8: TPanel;
    Panel9: TPanel;
	  GroupBox_unt_stat: TGroupBox;
	  GroupBox_impfungmain: TGroupBox;
	  Label4: TLabel;
    Impfstoff: TLabel;
    Label6: TLabel;
	  Label7: TLabel;
	  Label8: TLabel;
	  SpeedButton2: TSpeedButton;
	  Label46: TLabel;
    Label10: TLabel;
	  Label90: TLabel;
	  DBEdit_impf_datum: TDBEdit;
	  DBEdit_impf_dosis: TDBEdit;
	  DBEdit_impf_charge: TDBEdit;
	  DBLookupComboBox_impfung_stoff: TDBLookupComboBox;
    BitBtn_labaendern: TBitBtn;
	  GroupBox14: TGroupBox;
	  GroupBox_impf_status: TGroupBox;
    PageControl_labor: TPageControl;
    TabSheet_lab_wert: TTabSheet;
	  GroupBox_lab_stat: TGroupBox;
    GroupBox_lab_anmerkung: TGroupBox;
	  TabSheet_lab_graph: TTabSheet;
	  GroupBox_graph: TGroupBox;
    xygraph: txygraph;
	  Panel_ambulanz: TPanel;
	  pagecontrol_ambulanz: TPageControl;
	  TabSheet_u_daten: TTabSheet;
	  GroupBox_u_main: TGroupBox;
	  Label18: TLabel;
    Label19: TLabel;
	  Label105: TLabel;
    SpeedButton14: TSpeedButton;
    DBEdit_u_datum: TDBEdit;
	  DBCheckBox_aunfall: TDBCheckBox;
	  GroupBox_verletzung: TGroupBox;
	  Panel14: TPanel;
    Panel10: TPanel;
	  BitBtn_u_lokal: TBitBtn;
    Panel15: TPanel;
	  BitBtn_u_versorg: TBitBtn;
	  Panel16: TPanel;
	  BitBtn_u_art: TBitBtn;
	  Panel1: TPanel;
	  GroupBox_behandlung: TGroupBox;
	  GroupBox_vitaldaten: TGroupBox;
	  Label95: TLabel;
	  Label96: TLabel;
    Label97: TLabel;
	  Label98: TLabel;
    DBEdit_rrs: TDBEdit;
	  DBEdit_rrd: TDBEdit;
	  DBEdit_freq: TDBEdit;
	  DBEdit_bz: TDBEdit;
    GroupBox_icd_diagnose: TGroupBox;
    Label101: TLabel;
	  DBEdit_icd: TDBEdit;
    DBEdit7: TDBEdit;
	  TabSheet_u_hergang: TTabSheet;
	  GroupBox21: TGroupBox;
	  DBMemo_unfallhergang: TDBMemo;
	  GroupBox22: TGroupBox;
    DBEdit_uzuegen: TDBEdit;
    DBCheckBox_augenzeuge: TDBCheckBox;
	  TabSheet_u_arbeit_I: TTabSheet;
	  GroupBox_u_ort: TGroupBox;
    Label67: TLabel;
	  Label68: TLabel;
	  DBEdit_gebaeude: TDBEdit;
	  DBEdit_maschine: TDBEdit;
    GroupBox_u_zeit: TGroupBox;
	  Label71: TLabel;
	  Label72: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label89: TLabel;
    Label100: TLabel;
	  SpeedButton_a_ein: TSpeedButton;
    SpeedButton_a_auf: TSpeedButton;
    SpeedButton13: TSpeedButton;
	  DBEdit_u_ein_dat: TDBEdit;
	  DBComboBox_u_ein: TDBComboBox;
	  DBEdit1: TDBEdit;
	  DBEdit_u_auf_dat: TDBEdit;
    DBComboBox_u_aus: TDBComboBox;
    TabSheet_u_arbeit_II: TTabSheet;
	  GroupBox_unt_akt: TGroupBox;
	  Panel_bef_auff: TPanel;
    RadioGroup_auffaellig: TRadioGroup;
	  GroupBox_diagnosen: TGroupBox;
    Labelicd_sch: TLabel;
	  Label_icd_text: TLabel;
	  Label16: TLabel;
	  DBEdit_icd_text: TDBEdit;
    DBEdit_icdschl: TDBEdit;
    BitBtn_icd_wahl: TBitBtn;
	  DBEdit_dat_icdbeginn: TDBEdit;
    GroupBox7: TGroupBox;
	  GroupBox_besonderheiten: TGroupBox;
	  GroupBox8: TGroupBox;
    ScrollBox2: TScrollBox;
	  Panel_texte: TPanel;
	  ScrollBox3: TScrollBox;
    Arbeitsmedizin_Proband: TOLeContainerext;
    dbTreeView: TdbTreeView;
    lzr: Tlzrw1;
	  GroupBox_lang_text: TGroupBox;
	  Label65: TLabel;
    Label66: TLabel;
    DBEdit_text_datum: TDBEdit;
	  DBEdit_text_titel: TDBEdit;
	  Panel4: TPanel;
	  Panel6: TPanel;
	  Label28: TLabel;
	  Label29: TLabel;
	  Label30: TLabel;
	  Label43: TLabel;
	  Label44: TLabel;
    DBEdit_funktion: TDBEdit;
	  DBEdit5: TDBEdit;
	  DBEdit_an_telefon: TDBEdit;
	  DBEdit_an_fax: TDBEdit;
	  DBEdit_an_mail: TDBEdit;
	  DBRadioGroup1: TDBRadioGroup;
    Splitter_ansprech: TSplitter;
	  Splitter1: TSplitter;
    Splitter3: TSplitter;
    GroupBox_mitarbeiter: TGroupBox;
    DBgridEXT_namen: TDBgridEXT;
    Panel7: TPanel;
	  SpeedButton5: TSpeedButton;
	  SpeedButton7: TSpeedButton;
	  unsichtbares1: TMenuItem;
	  baumrunter1: TMenuItem;
    baumhoch1: TMenuItem;
	  DBEdit_vedat: TDBEdit;
	  Label_vedat: TLabel;
	  DBEdit_iedat: TDBEdit;
    Label_iedat: TLabel;
	  DBEdit_ledat: TDBEdit;
    Label_ledat: TLabel;
	  BitBtn_b_zurueck: TBitBtn;
	  Panel22: TPanel;
	  Panel23: TPanel;
	  Panel24: TPanel;
	  Panel25: TPanel;
	  Panel26: TPanel;
    Panel27: TPanel;
    Panel_wbr: TPanel;
    Label76: TLabel;
    Label79: TLabel;
    DBEdit_u_aerst: TDBEdit;
	  DBEdit_u_aKH: TDBEdit;
    Panel28: TPanel;
	  SpeedButton_u_erst: TSpeedButton;
	  SpeedButton_u_kh: TSpeedButton;
	  Panel29: TPanel;
    SpeedButton_u_wa: TSpeedButton;
	  Panel30: TPanel;
	  SpeedButton12: TSpeedButton;
	  Panel_wbl: TPanel;
	  Label78: TLabel;
	  Label77: TLabel;
	  DBEdit_u_adz: TDBEdit;
	  ImageList_main: TImageList;
    ImageList_bereich: TImageList;
	  ImageList_editor: TImageList;
    ImageList_uil: TImageList;
	  Panel31: TPanel;
	  BitBtn_impf_planen: TBitBtn;
	  Panel32: TPanel;
	  BitBtn_unters_planen: TBitBtn;
	  m_maloeschen: TMenuItem;
    Panel34: TPanel;
	  PopupMenu_kartei: TPopupMenu;
    Neu1: TMenuItem;
    m_aendern_kartei: TMenuItem;
    N7: TMenuItem;
    DBMemo_beurteilung: TDBMemo;
	  Branchen1: TMenuItem;
    FirmenGruppen1: TMenuItem;
    m_Firmaauswhlen: TMenuItem;
	  SpeedButton16: TSpeedButton;
	  SpeedButton_icd: TSpeedButton;
	  Panel_befunde_datum: TPanel;
	  Label113: TLabel;
    SpeedButton15: TSpeedButton;
	  DBEdit_befund_datum: TDBEdit;
	  m_maArchivieren: TMenuItem;
	  m_madearchivieren: TMenuItem;
	  Passwortchange: TMenuItem;
    emailListe1: TMenuItem;
	  m_FirmenListen: TMenuItem;
	  m_DokumentenVorlagen: TMenuItem;
	  m_Berechtigungen: TMenuItem;
    Kartei1: TMenuItem;
	  m_Tools: TMenuItem;
	  DBEdit_ha: TDBEdit;
    Label_ha: TLabel;
    SpeedButton_ha: TSpeedButton;
    GroupBox_nurarbeitssicherheit: TGroupBox;
    DBCheckBox_bg: TDBCheckBox;
    DBCheckBox_ab: TDBCheckBox;
	  GroupBox_nurambulanz: TGroupBox;
	  DBCheckBox_intern: TDBCheckBox;
    BitBtn3: TBitBtn;
	  N2: TMenuItem;
	  Info1: TMenuItem;
	  m_Lizenz: TMenuItem;
    m_registrieren: TMenuItem;
    BitBtn_taetigkeit: TBitBtn;
	  import_excel: TMenuItem;
    m_LaborLDT: TMenuItem;
	  DBMemo_anmerk: TDBMemo;
    Panel_fsi_oben: TPanel;
	  Label41: TLabel;
	  Label36: TLabel;
    Label20: TLabel;
	  Stunden: TLabel;
	  Minuten: TLabel;
	  SpeedButton6: TSpeedButton;
    DBEdit_zeit_date: TDBEdit;
	  DBEdit_Stunden: TDBEdit;
    DBEdit_minuten: TDBEdit;
    DBMemo_firma_sollist: TDBMemo;
    Panel_firmen_adresse: TPanel;
    Panel_firma_main: TPanel;
    Label3: TLabel;
    Label13: TLabel;
	  DBEdit_firma: TDBEdit;
	  DBEdit_firmenkuerzel: TDBEdit;
	  GroupBox6: TGroupBox;
    Tabsheet_Rechnungen: TTabSheet;
	  Label149: TLabel;
    m_UntersuchungsSchritte: TMenuItem;
    Notebook_befund: TNotebook;
    Label_bezeichner: TLabel;
	  Label_bezeichner_dim: TLabel;
    Edit_bef: TEdit;
    ComboBox: TComboBox;
	  G: TLabel;
	  Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
	  Label120: TLabel;
	  Edit_groesse: TEdit;
    Edit_gewicht: TEdit;
	  Edit_bmi: TEdit;
	  Label122: TLabel;
	  Label123: TLabel;
    Label121: TLabel;
	  Label124: TLabel;
	  SpinEdit_sy: TSpinEdit;
    SpinEdit_di: TSpinEdit;
    SpinEdit_pu: TSpinEdit;
	  Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
	  Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
	  Label135: TLabel;
	  Label136: TLabel;
	  Label137: TLabel;
	  Label138: TLabel;
    l_fev1: TLabel;
    l_fvc: TLabel;
    l_fev1fvc: TLabel;
    l_pev: TLabel;
	  Label139: TLabel;
	  l_fev1g: TLabel;
    l_fvcg: TLabel;
    l_fev1fvcg: TLabel;
    l_pevg: TLabel;
    Edit_fev1: TEdit;
    Edit_fvc: TEdit;
    Edit_fev_fvc: TEdit;
    Edit_pev: TEdit;
    PageControl_st: TPageControl;
	  TabSheet_st_weber: TTabSheet;
    Panel_weber_sisi: TPanel;
    RadioGroup_weber: TRadioGroup;
	  GroupBox_sisi: TGroupBox;
	  Label60: TLabel;
	  Label61: TLabel;
    Label62: TLabel;
	  Label63: TLabel;
	  Label64: TLabel;
    Edit_sfr: TEdit;
    Edit_sfl: TEdit;
    Edit_sdbr: TEdit;
	  Edit_sdbl: TEdit;
	  Edit_spr: TEdit;
	  Edit_spl: TEdit;
    TabSheet_st_re: TTabSheet;
    audiogramm_re: Taudiogramm;
    TabSheet_st_li: TTabSheet;
	  audiogramm_li: Taudiogramm;
	  PageControl_befunde: TPageControl;
    TabSheet_be_anmerkung: TTabSheet;
    RichEdit_befund: TRichEdit;
    TabSheet_be_doku: TTabSheet;
	  ScrollBox_ole_befund: TScrollBox;
    Arbeitsmedizin_Befund: TOLeContainerext;
    TabSheet_be_graph: TTabSheet;
    xygraph_befund: txygraph;
	  RadioGroup_parameter: TRadioGroup;
    GroupBox_unt_abrechnung: TGroupBox;
    GroupBox26: TGroupBox;
    GroupBox_lab_abrechnung: TGroupBox;
	  Label156: TLabel;
	  Label157: TLabel;
    DBCheckBox_lab_abrechnen: TDBCheckBox;
    DBEdit_l_goae: TDBEdit;
    DBEdit_l_preis: TDBEdit;
    Label159: TLabel;
    Label153: TLabel;
	  Label154: TLabel;
    Label155: TLabel;
    DBCheckBox_impfung_abrechnen: TDBCheckBox;
	  DBEdit_I_goae: TDBEdit;
	  DBEdit_I_preis: TDBEdit;
    DBEdit_i_faktor: TDBEdit;
	  Label160: TLabel;
	  Label148: TLabel;
    DBCheckBox_vors_ab: TDBCheckBox;
    Label161: TLabel;
    RichEdit: TRichEdit;
	  DBMemo_impfung: TDBMemo;
	  DBMemo_diagnosen: TDBMemo;
    DBMemo_besonderheiten: TDBMemo;
    m_GOaeListe: TMenuItem;
    GroupBox_bef_abr: TGroupBox;
    Label_goae: TLabel;
    Label17: TLabel;
	  Label165: TLabel;
	  CheckBox_b_abr: TCheckBox;
	  Edit_b_goae: TEdit;
    m_Personalien_drucken: TMenuItem;
    SaveDialog: TSaveDialog;
	  DBCombo_num_zeitabr: TDBCombo_num;
	  DBCombo_num_famstand: TDBCombo_num;
	  DBCombo_num_geschlecht: TDBCombo_num;
    DBCombo_num_staat: TDBCombo_num;
    DBCombo_num_vor_status: TDBCombo_num;
    DBCombo_num_impf_unters: TDBCombo_num;
	  DBCombo_num_lab_status: TDBCombo_num;
    DBCombo_num_weiter: TDBCombo_num;
    DBCombo_num_amb_unters: TDBCombo_num;
    DBCombo_num_impf_status: TDBCombo_num;
    DBCombo_num_impfabfolge: TDBCombo_num;
	  ImageList_status: TImageList;
	  DBCombo_num_schicht: TDBCombo_num;
	  m_storno_show: TMenuItem;
    GroupBox_u_meldung: TGroupBox;
    Label91: TLabel;
    DBCheckBox_azubi: TDBCheckBox;
	  DBCheckBox_leiharbeiter: TDBCheckBox;
	  DBComboBox_verwand: TDBComboBox;
    Label73: TLabel;
    DBEdit_a_entgelt: TDBEdit;
	  Label74: TLabel;
	  GroupBox_u_art: TGroupBox;
    Label92: TLabel;
    DBCheckBox_u_tot: TDBCheckBox;
    DBComboBox_unfall_art: TDBComboBox;
    Panel21: TPanel;
	  Label75: TLabel;
    DBComboBox_a_schilderung: TDBComboBox;
    Label_arb_ende: TLabel;
    BitBtn_audiogramm_export: TBitBtn;
    Panel_rechnung_ma: TPanel;
    GroupBox_rechnungkopf: TGroupBox;
    GroupBox_rechnungunten: TGroupBox;
	  DBgridEXT_re_positionen: TDBgridEXT;
    Panel_firma_daten: TPanel;
    Label12: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
	  Label25: TLabel;
	  Label26: TLabel;
	  Label27: TLabel;
    Label59: TLabel;
    Label112: TLabel;
    DBEdit_firm_telefon: TDBEdit;
    DBEdit_homepage: TDBEdit;
    DBEdit_firm_plz: TDBEdit;
    DBEdit_firm_str: TDBEdit;
    DBEdit_firm_ort: TDBEdit;
	  DBEdit_fax: TDBEdit;
    DBEdit_email: TDBEdit;
    DBEdit_postfach: TDBEdit;
	  DBEdit_z_hd: TDBEdit;
    Lieferanten1: TMenuItem;
    Material1: TMenuItem;
    Panel_verkauf: TPanel;
    DBEdit_art_name: TDBEdit;
    BitBtn_art_wahl: TBitBtn;
    DBEdit_art_menge: TDBEdit;
    Label39: TLabel;
	  DBEdit_art_preis: TDBEdit;
    Label40: TLabel;
    DBEdit_art_vkp: TDBEdit;
    Label55: TLabel;
	  DBEdit_art_rabatt: TDBEdit;
	  Label57: TLabel;
	  Label176: TLabel;
    DBEdit_art_endpreis: TDBEdit;
    Label177: TLabel;
	  Label178: TLabel;
    Edit_brutto: TEdit;
    Edit_rabatt_proz: TEdit;
    BitBtn_artplus: TBitBtn;
    BitBtn_artminus: TBitBtn;
    Label173: TLabel;
    DBEdit_pr_netto: TDBEdit;
    m_Artikel1: TMenuItem;
    ComboBox_rechts: TComboBox;
    Label_li: TLabel;
    Label_re: TLabel;
    Optionen1: TMenuItem;
	  Label188: TLabel;
    Label189: TLabel;
    Label190: TLabel;
	  Label191: TLabel;
    Label192: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    m_Gefaehrdungen: TMenuItem;
    Edit_waehrung_bef: TEdit_waehrung;
    m_uebersicht_kartei: TMenuItem;
    N10: TMenuItem;
    Panel2: TPanel;
    Label_suchen: TLabel;
	  SpeedButton1: TSpeedButton;
    Edit_such: TEdit;
    Label201: TLabel;
    Label202: TLabel;
	  Label205: TLabel;
    Label206: TLabel;
    Label109: TLabel;
    Label_gdb: TLabel;
    DBEdit_gdb: TDBEdit;
    Panel38: TPanel;
    Label207: TLabel;
    SpeedButton_firma_such: TSpeedButton;
    Edit_firma_such: TEdit;
    Label209: TLabel;
    SpeedButton_datum_artikel: TSpeedButton;
    DBEdit_material: TDBEdit;
	  N12: TMenuItem;
    SpeedButton_besonderheiten: TSpeedButton;
    BitBtn_bes_auswahl: TBitBtn;
	  DBEdit_dat_bes_beg: TDBEdit;
    SpeedButton_bes_erledigt: TSpeedButton;
    DBEdit_bes_erledigt_dat: TDBEdit;
    Label210: TLabel;
    N6: TMenuItem;
	  N13: TMenuItem;
    m_druck_alleBesch: TMenuItem;
    BitBtn_beurt_einf: TBitBtn;
    GroupBox_anmerkung_proband: TGroupBox;
    Panel19: TPanel;
    Panel40: TPanel;
    DBMemo_beurteilung_ma: TDBMemo;
    SpeedButton9: TSpeedButton;
    DBEdit9: TDBEdit;
    m_druck_bescheinigung_kartei: TMenuItem;
    GroupBox_rechnung_bottom: TGroupBox;
    SpeedButton_verschickt: TSpeedButton;
    Label211: TLabel;
	  SpeedButton_bezahlt: TSpeedButton;
    Label213: TLabel;
    DBEdit_reversch: TDBEdit;
    DBEdit_rebezahlt: TDBEdit;
	  m_Ambulanz_main: TMenuItem;
	  Lokalisation1: TMenuItem;
    Verletzungsart1: TMenuItem;
    Versorgung1: TMenuItem;
	  ScrollBox1: TScrollBox;
    Panel_wai: TPanel;
    Label215: TLabel;
    Label216: TLabel;
    Label217: TLabel;
    Label218: TLabel;
    Edit_d1: TEdit;
    Edit_d2: TEdit;
    Edit_d3: TEdit;
    Edit_d4: TEdit;
    Edit_d5: TEdit;
    Label219: TLabel;
    Label220: TLabel;
    Label221: TLabel;
    Label222: TLabel;
    Label223: TLabel;
    Label224: TLabel;
	  Label225: TLabel;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
	  ComboBox5: TComboBox;
    ComboBox6: TComboBox;
    ComboBox7: TComboBox;
	  ComboBox8: TComboBox;
    ComboBox9: TComboBox;
	  SpeedButton_d1: TSpeedButton;
    SpeedButton_d2: TSpeedButton;
    SpeedButton_d3: TSpeedButton;
    SpeedButton_d4: TSpeedButton;
    SpeedButton_d5: TSpeedButton;
    Label_wai: TLabel;
    Label226: TLabel;
    ComboBox_arbeit: TComboBox;
    BitBtn_wai: TBitBtn;
    Edit_WAI: TEdit;
    MaskEdit_d2: TMaskEdit;
    MaskEdit_d3: TMaskEdit;
	  MaskEdit_d4: TMaskEdit;
    MaskEdit_d5: TMaskEdit;
    MaskEdit_d1: TMaskEdit;
    BitBtn8: TBitBtn;
	  Edit_impf_typ: TEdit;
    cipher: TDCP_twofish;
    DBEdit_time1: TDBEdit_time;
    DBEdit_time2: TDBEdit_time;
    DBEdit_time3: TDBEdit_time;
    DBEdit_time_arb_ende: TDBEdit_time;
    DBEdit_time5: TDBEdit_time;
    Edit_taetigkeit: TEdit;
	  Panel_anamnese: TPanel;
    ToolBar_anamnese: TToolBar;
    Openbutton: TToolButton;
    savebutton: TToolButton;
    ToolButton14: TToolButton;
	  Undobutton: TToolButton;
    Copybutton: TToolButton;
    Pastebutton: TToolButton;
	  ToolButton3: TToolButton;
    FontName: TComboBox;
    ToolButton11: TToolButton;
    FontSize: TEdit;
    UpDown1: TUpDown;
	  ToolButton5: TToolButton;
    BoldButton: TToolButton;
    ItalicButton: TToolButton;
	  UnderlineButton: TToolButton;
    ToolButton_color: TToolButton;
    ToolButton16: TToolButton;
    LeftAlign: TToolButton;
    CenterAlign: TToolButton;
    RightAlign: TToolButton;
    ToolButton8: TToolButton;
    BulletsButton: TToolButton;
    ToolButton2: TToolButton;
    akt_editor: TRichEdit;
    Label31: TLabel;
    DBEdit_dia_ende: TDBEdit;
	  SpeedButton_diagn_bis: TSpeedButton;
    Panel_reuebersicht: TPanel;
    GroupBox4: TGroupBox;
	  Panel39: TPanel;
	  DBgridEXT_repo: TDBgridEXT;
    GroupBox_re_firma: TGroupBox;
	  SpeedButton_f_verschickt: TSpeedButton;
    Label212: TLabel;
    SpeedButton_f_bezahlt: TSpeedButton;
    Label214: TLabel;
    DBEdit_dat_verschickt: TDBEdit;
    DBEdit_dat_bezahlt: TDBEdit;
    GroupBox5: TGroupBox;
    DBgridEXT_rechnung: TDBgridEXT;
    Label_kapitel: TLabel;
    Label_befunde_kapitel: TLabel;
    Label34: TLabel;
	  m_kapitel: TMenuItem;
    Panel_kontakte: TPanel;
    Label35: TLabel;
    Label85: TLabel;
	  Label128: TLabel;
	  Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    Label227: TLabel;
    Label228: TLabel;
	  Label229: TLabel;
    Label230: TLabel;
    Label231: TLabel;
	  Label232: TLabel;
    DBEdit_avorname: TDBEdit;
    DBEdit_astrasse: TDBEdit;
    DBEdit_aplz: TDBEdit;
    DBEdit_aort: TDBEdit;
    DBEdit_atelefon: TDBEdit;
    DBEdit_afax: TDBEdit;
    DBEdit_aemail: TDBEdit;
    DBMemo_amemo: TDBMemo;
    DBEdit_awww: TDBEdit;
    DBEdit_aname: TDBEdit;
    DBEdit_azhd: TDBEdit;
    DBEdit_afach: TDBEdit;
    BitBtn_kontaktimport: TBitBtn;
	  Label233: TLabel;
    DBEdit_atitel: TDBEdit;
    Label234: TLabel;
	  DBEdit_sternnummer: TDBEdit;
    Label235: TLabel;
	  DBEdit_zusatznummer: TDBEdit;
    system: TDdeServerConv;
    ddeserveritem: TDdeServerItem;
    m_Textbausteine: TMenuItem;
    Textbausteine1: TMenuItem;
    DBCombo_num_besonderheiten: TDBCombo_num;
    DBCombo_num_dokumente: TDBCombo_num;
    DBCombo_num_befunde: TDBCombo_num;
    ToolBar: TToolBar;
    ToolButtonneu: TToolButton;
    ToolButton1: TToolButton;
    ToolButtonedit: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButtonsichern: TToolButton;
    ToolButton7: TToolButton;
    ToolButtonloeschen: TToolButton;
    ToolButton9: TToolButton;
    ToolButtonsuchen: TToolButton;
    ToolButton10: TToolButton;
    ToolButtonauswertung: TToolButton;
    ToolButton12: TToolButton;
	  ToolButtontermine: TToolButton;
	  ToolButton13: TToolButton;
    ToolButton_timer: TToolButton;
    DCP_sha11: TDCP_sha1;
    m_Export_Probandenbersicht: TMenuItem;
    DBCombo_num_texte: TDBCombo_num;
    Label32: TLabel;
    DBComboBox_anrede: TDBComboBox;
    m_Abrechnungsanlaesse: TMenuItem;
    Edit_abteilung: TEdit;
    DBCheckBox_lab_mws: TDBCheckBox;
    CheckBox_bef_ust: TCheckBox;
    DBCheckBox_untersuchung_ust: TDBCheckBox;
    DBCheckBox_impfung_ust: TDBCheckBox;
    DBEdit_pr_brutto: TDBEdit;
    GroupBox_fsi_5: TGroupBox;
    DBMemo_sollist_anmerkung: TDBMemo;
    m_erbrachteZeiten: TMenuItem;
    ImportberBDT1: TMenuItem;
    Label88: TLabel;
    Label142: TLabel;
    Label145: TLabel;
    Label152: TLabel;
    Edit_bauchumfang: TEdit;
    Label158: TLabel;
    Edit_whtr: TEdit;
    Label162: TLabel;
    Edit_arbeitsplatz: TEdit;
    BitBtn2: TBitBtn;
    BitBtn_f_rechnung_ph: TBitBtn;
    BitBtn_f_rechnung_pl: TBitBtn;
    BitBtn_f_rechnung_pae: TBitBtn;
    BitBtn_pos_prob_neu: TBitBtn;
    BitBtn_repositionen_loesch_proband: TBitBtn;
    Label163: TLabel;
    DBComboBox_GCS: TDBComboBox;
    Label164: TLabel;
    DBEdit_temp: TDBEdit;
    DBComboBox_trop_t: TDBComboBox;
    DBComboBox_ekg: TDBComboBox;
    Label166: TLabel;
    Label167: TLabel;
    DBEdit_o2: TDBEdit;
    Label168: TLabel;
    DBComboBox_rythmus: TDBComboBox;
    Label169: TLabel;
    m_Abrechnungskriterium: TMenuItem;
    DBCombo_num_firma_land: TDBCombo_num;
    Label33: TLabel;
    DBCombo_num_proband_land: TDBCombo_num;
    Label172: TLabel;
    DBEdit_geburtsort: TDBEdit;
    Label174: TLabel;
    DBEdit_redat: TDBEdit;
    Label175: TLabel;
    BitBtn_redurckma: TBitBtn;
    BitBtn_redruck_fi: TBitBtn;
    DBEdit_labornummer: TDBEdit;
    Label179: TLabel;
    SpeedButton4: TSpeedButton;
    ZipMaster: TZipMaster19;
    TabSheet_firma_filter: TTabSheet;
    GroupBox10: TGroupBox;
    DBgridEXT2: TDBgridEXT;
    FirmenFilter1: TMenuItem;
    Panel_firma_filter: TPanel;
    Label194: TLabel;
    SpeedButton8: TSpeedButton;
    DBEdit11: TDBEdit;
    SpeedButton_doceigenschaften1: TSpeedButton;
    Splitter4: TSplitter;
    FirmenTools1: TMenuItem;
    ZeiterfassungfrmehrereFirmen1: TMenuItem;
    DBComboBox_zeitansatz: TDBComboBox;
    Label195: TLabel;
    Label196: TLabel;
    Button1: TButton;
    m_Ber_ges_Firma: TMenuItem;
    N3: TMenuItem;
    Image_paw: TImage;
    GroupBox_zeit_filtereinstellungen: TGroupBox;
    SpeedButton_z1: TSpeedButton;
    SpeedButton_z2: TSpeedButton;
    SpeedButton_zfiltern: TSpeedButton;
    Label9: TLabel;
    Label15: TLabel;
    Label236: TLabel;
    Label242: TLabel;
    Label187: TLabel;
    Label193: TLabel;
    Label197: TLabel;
    MaskEdit_z1: TMaskEdit;
    MaskEdit_z2: TMaskEdit;
    ComboBox_zeiterfassung: TComboBox;
    Edit_z_gel: TEdit;
    Edit_z_fehl: TEdit;
    BitBtn_zeituebersicht_anzeigen: TBitBtn;
    ComboBox_zeit_kriterium: TComboBox;
    ComboBox_zeit_konto: TComboBox;
    CheckBox_zeit_ausgabe_rest: TCheckBox;
    DBCheckBox_archiv: TDBCheckBox;
    Label131: TLabel;
    Label245: TLabel;
    DBCheckBox_zustimmung_besch: TDBCheckBox;
    DBEdit_arbmed_besch_ert: TDBEdit;
    DBEdit_arbmed_besch_bis: TDBEdit;
    Label_rv: TLabel;
    Krankenkasse: TLabel;
    DBEdit_rvnummer: TDBEdit;
    DBEdit_krankenkasse: TDBEdit;
    SpeedButton_ffiltern: TSpeedButton;
    m_Abrechnungskonto: TMenuItem;
    SpeedButton10: TSpeedButton;
    m_Firmenzusammenfgen: TMenuItem;
    Panel_sehtest: TPanel;
    grid_sehtest: TStringGrid;
    SpeedButton11: TSpeedButton;
    SpeedButton17: TSpeedButton;
    grid_ambulanz: TStringAlignGrid;
    GDTProgramme1: TMenuItem;
    m_GDT_schreiben: TMenuItem;
    DBCheckBox_zeit_ueberstunden: TDBCheckBox;
    GroupBox2: TGroupBox;
    Label99: TLabel;
    Label110: TLabel;
    Label115: TLabel;
    DBCombo_num_branche: TDBCombo_num;
    DBCombo_num_f_gruppe: TDBCombo_num;
    Label106: TLabel;
    Label108: TLabel;
    Label111: TLabel;
    Label114: TLabel;
    DBEdit_anumm: TDBEdit;
    DBCombo_num_gewerbeaufsichtsamt: TDBCombo_num;
    Label82: TLabel;
    Label107: TLabel;
    DBEdit_bgnr: TDBEdit;
    DBCombo_num_bg: TDBCombo_num;
    Label80: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    DBEdit_firma_reempfaenger: TDBEdit;
    BitBtn_rechnungsempfaenger: TBitBtn;
    DBgridEXt_firma: TDBgridEXT;
    Panel_re_bottom1: TPanel;
    Label199: TLabel;
    Label200: TLabel;
    Label203: TLabel;
    Label204: TLabel;
    DBEdit_ab_netto: TDBEdit;
    DBEdit_firma_rechnung_brutto: TDBEdit;
    GroupBox17: TGroupBox;
    DBMemo_rechnung_parameter: TDBMemo;
    Label_fi_entfernung: TLabel;
    Label56: TLabel;
    Label249: TLabel;
    DBEdit_km_entfernung: TDBEdit;
    Zeitvorgabenbernehmen1: TMenuItem;
    PopupMenu_firma_rechnung: TPopupMenu;
    pm_f_rechnung_wiederherstellen: TMenuItem;
    Pm_zeiterfassung: TPopupMenu;
    pm_zeit_verschieben: TMenuItem;
    CheckBox_zeit_ansatz: TCheckBox;
    Label251: TLabel;
    DBEdit_druck: TDBEdit;
    SpeedButton_beschdruck_proband: TSpeedButton;
    m_Anrede: TMenuItem;
    Panel3: TPanel;
    BitBtn_zeit_mwahl: TBitBtn;
    mAbrechnungTaetigkeit: TMenuItem;
    DBEdit_time_sollistbeginn: TDBEdit_time;
    DBEdit_time_sollistende: TDBEdit_time;
    Label252: TLabel;
    Label253: TLabel;
    Panel_firma_sollist_unten: TPanel;
    Label_abr_kmgefahren: TLabel;
    Label_abr_km: TLabel;
    Label250: TLabel;
    BitBtn_zeit_km: TBitBtn;
    DBEdit_abr_kmgefahren: TDBEdit;
    DBCheckBox_abr_privat: TDBCheckBox;
    DBCombo_num_zeit_ust: TDBCombo_num;
    Panel_fsi_2: TPanel;
    Label170: TLabel;
    Label184: TLabel;
    DBCombo_num_abrechnungskriterium: TDBCombo_num;
    DBCombo_num_zeitkonto: TDBCombo_num;
    GroupBox_fsi_3: TGroupBox;
    DBMemo_sollist_anlass: TDBMemo;
    Panel11: TPanel;
    BitBtn_leistungswahl: TBitBtn;
    SpeedButton_firmasollist_zeit: TSpeedButton;
    Panel5: TPanel;
    AnmeldeProtokoll1: TMenuItem;
    AnzeigeKartei1: TMenuItem;
    Panel_wiedervorlage: TPanel;
    Splitter_agenda: TSplitter;
    TreeView_agenda: TTreeView;
    SpeedButton_firna_text_wdv: TSpeedButton;
    Label5: TLabel;
    DBEdit_firma_text_wdv: TDBEdit;
    DBCombo_num_besonderheit_user: TDBCombo_num;
    DBCombo_num_texte_user: TDBCombo_num;
    Label258: TLabel;
    Wiedervorlage1: TMenuItem;
    BitBtn_zeituebersicht_anzeigen_word: TBitBtn;
    Memo_unt_typ: TMemo;
    m_Weiterversorgung: TMenuItem;
    Notebook_vorsorge: TNotebook;
    DBCombo_num_vor_untersucher: TDBCombo_num;
    Panel_vorsorge0: TPanel;
    DBRadioGroup_beurteilung_eignung: TDBRadioGroup;
    DBCombo_num_vor_art: TDBCombo_num;
    Label259: TLabel;
    Panell_vorsorge1: TPanel;
    DBCombo_num_unt_art2: TDBCombo_num;
    Label246: TLabel;
    DBRadioGroup_beurteilung_arbmedvv: TDBRadioGroup;
    SpeedButton_wdv_dokumente: TSpeedButton;
    Label260: TLabel;
    Label261: TLabel;
    DBEdit_wdv_dokumente: TDBEdit;
    DBCombo_num_wdv_dokumente: TDBCombo_num;
    Label_bescheinigung: TLabel;
    m_Folgebescheinigung_kartei: TMenuItem;
    Zeitabrechnung1: TMenuItem;
    m_AbrechnungTtigkeitalphabetisch: TMenuItem;
    m_Beurteilung: TMenuItem;
    Jahresabschluss1: TMenuItem;
    N4: TMenuItem;
    m_Buchungsperiode: TMenuItem;
    Spenden1: TMenuItem;
    roubleshooting1: TMenuItem;
    N8: TMenuItem;
    PopupMenu_richedit: TPopupMenu;
    dropdown_richedit_Fett: TMenuItem;
    dropdown_richedit_Kursiv: TMenuItem;
    dropdown_richedit_Unterstrichen: TMenuItem;
    dropdown_richedit_zurcksetzen: TMenuItem;
    DBCheckBox_art_ust: TDBCheckBox;
    Label241: TLabel;
    Panel_erlaubnis_AG: TPanel;
    DBCheckBox_weitergabe_ag: TDBCheckBox;
    Label262: TLabel;
    PM_f_rechnung_rechts: TPopupMenu;
    m_f_Positionhinzufgen: TMenuItem;
    m_f_Positionndern: TMenuItem;
    m_f_Positionlschen: TMenuItem;
    PM_p_rechnung_rechts: TPopupMenu;
    m_p_Positionhinzufgen: TMenuItem;
    m_p_Positionndern: TMenuItem;
    m_p_Positionlschen: TMenuItem;
    SpeedButton_labornummer: TSpeedButton;
    m_LaborErgebnistexte: TMenuItem;
    m_word_pdf_anzeige_kartei: TMenuItem;
    PopupMenu_texte: TPopupMenu;
    Neu2: TMenuItem;
    ndern1: TMenuItem;
    alsPDFanzeigen1: TMenuItem;
    m_RechnungDruckenundSpeichern: TMenuItem;
    CheckBox_zeit: TCheckBox;
    m_SummederZeitenanzeigen: TMenuItem;
    ImageList_tabs: TImageList;
    ImageList_tabs_firma: TImageList;
    ImageList_buttons: TImageList;
    TabSheet_firma_einstellungen: TTabSheet;
    PageControl_firma_einstellungen: TPageControl;
    TabSheet_firma_einstellungen_rechnung: TTabSheet;
    TabSheet_firma_einstellungen_zeitabrechnung: TTabSheet;
    Panel12: TPanel;
    GroupBox16: TGroupBox;
    DBCheckbox_abr_befunde: TDBCheckBox;
    DBCheckBoxabr_Untersuchungen: TDBCheckBox;
    DBCheckBox_Impfungen: TDBCheckBox;
    DBCheckBox_abr_labor: TDBCheckBox;
    DBCheckBox_artikel: TDBCheckBox;
    GroupBox24: TGroupBox;
    Label147: TLabel;
    Label150: TLabel;
    Label151: TLabel;
    Label171: TLabel;
    Label255: TLabel;
    Label256: TLabel;
    Label257: TLabel;
    DBEdit_fa_bef: TDBEdit;
    DBEdit_fa_tech: TDBEdit;
    DBEdit_fa_lab: TDBEdit;
    DBEdit_fa_unt: TDBEdit;
    GroupBox_abr_pauschal: TGroupBox;
    Label140: TLabel;
    Label180: TLabel;
    Label181: TLabel;
    Label263: TLabel;
    DBEdit12: TDBEdit;
    DBRadioGroup2: TDBRadioGroup;
    DBEdit3: TDBEdit;
    Panel_firma_abrechnung_pausch: TPanel;
    Label198: TLabel;
    Label208: TLabel;
    Label239: TLabel;
    Label240: TLabel;
    Label243: TLabel;
    Label244: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit21: TDBEdit;
    Button_firma_zeitpauschale_berechnen: TButton;
    DBEdit_re_bezeichnung_pauschale: TDBEdit;
    GroupBox12: TGroupBox;
    Label141: TLabel;
    Label182: TLabel;
    Label183: TLabel;
    DBEdit13: TDBEdit;
    DBEdit4: TDBEdit;
    GroupBox18: TGroupBox;
    Label81: TLabel;
    Label186: TLabel;
    DBEdit6: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    GroupBox1: TGroupBox;
    GroupBox15: TGroupBox;
    Panel_firma_zeitvorgaben: TPanel;
    Label237: TLabel;
    Label238: TLabel;
    SpeedButton_z_von: TSpeedButton;
    SpeedButton_z_bis: TSpeedButton;
    Label185: TLabel;
    Label247: TLabel;
    Label248: TLabel;
    DBEdit_z_von: TDBEdit;
    DBEdit_z_bis: TDBEdit;
    DBEdit_z_stunden1: TDBEdit;
    DBCombo_num_firma_zeiten_soll_kriterium: TDBCombo_num;
    DBCombo_num_firma_zeiten_soll_konto: TDBCombo_num;
    GroupBox11: TGroupBox;
    DBgridEXT3: TDBgridEXT;
    Label143: TLabel;
    Label42: TLabel;
    Label_privatfahrzeug: TLabel;
    m_erweiterteFehlermeldungen: TMenuItem;
    PageControl_wdv: TPageControl;
    TabSheet_wdv_wiedervorlage: TTabSheet;
    TabSheet_wdv_freitextsuche: TTabSheet;
    CheckBox_ag_all: TCheckBox;
    SpeedButton18: TSpeedButton;
    SpeedButton19: TSpeedButton;
    SpeedButton_ag_refresh: TSpeedButton;
    ComboBox_ag_bis: TComboBox;
    ComboBox_ag_von: TComboBox;
    ComboBox_ag_user: TComboBox;
    Edit_wv_suchen: TEdit;
    CheckBox_wv_freitext: TCheckBox;
    SpeedButton_wv_zu_freitext: TSpeedButton;
    SpeedButton_wv_auf_freitext: TSpeedButton;
    SpeedButton_wv_freitext: TSpeedButton;
    lzr1: Tlzrw1;
    DBEdit_art_einheit: TDBEdit;
    Label87: TLabel;
    TabSheet1: TTabSheet;
    GroupBox_ma_freitext: TGroupBox;
    daten1: TMenuItem;
    Pane_prob_count: TPanel;
    Label_prob_count: TLabel;
    Panel_firma_count: TPanel;
    Label_firma_count: TLabel;
    DBEdit_druck_besch_firma: TDBEdit;
    SpeedButton20: TSpeedButton;
    Label254: TLabel;
    Label264: TLabel;
    DBCombo_num_firma_arzt: TDBCombo_num;
    Label265: TLabel;
    DBCombo_num_prob_arzt: TDBCombo_num;
    BitBtn_f_rechnung_pdhl: TBitBtn;
    m_f_dauerhaftloesch: TMenuItem;
    BitBtn_repo_dauerhaftentfernen: TBitBtn;
    Timer_untplan: TTimer;
    BitBtn_repoaendern_prob: TBitBtn;
    m_p_Positiondauerhaftentfernen: TMenuItem;
    N9: TMenuItem;
    m_f_allegelschtenPositionenwiederaktivieren: TMenuItem;
    N11: TMenuItem;
    m_p_geloeschte_positionen: TMenuItem;
    PopupMenu_ole: TPopupMenu;
    m_properties: TMenuItem;
    m_iconic: TMenuItem;
    ToolButton15: TToolButton;
    ToolButton_history: TToolButton;
    GroupBox_tree_hist: TGroupBox;
    DBgridEXT_tree_hist: TDBgridEXT;
    HistoriedesDatensatzes1: TMenuItem;
    F21: TMenuItem;
    f31: TMenuItem;
    f41: TMenuItem;
    f51: TMenuItem;
    f61: TMenuItem;
    f71: TMenuItem;
    f81: TMenuItem;
    Timer_datumswechsel: TTimer;
    m_allemarkieren: TMenuItem;
    m_rechnung_f_allemarkieren: TMenuItem;
    PopupMenu_oleinfo: TPopupMenu;
    InfozumDokument1: TMenuItem;
    OleSpeichern1: TMenuItem;
    Oleladen1: TMenuItem;
    TabSheet_abteilung: TTabSheet;
    label_prob_ap: TLabel;
    label_prob_abt: TLabel;
    m_Abteilung: TMenuItem;
    m_projektmithb: TMenuItem;
    m_ProjektemitPrioA: TMenuItem;
    m_ProjektemitPrioB: TMenuItem;
    m_ProjektemitPrioC: TMenuItem;
    N14: TMenuItem;
    m_UnfallverteilunginderAbteilung: TMenuItem;
    N15: TMenuItem;
    m_UnfallverteilunginderFirma: TMenuItem;
    m_ueberpruefenderBescheinigungen: TMenuItem;
    AuswertungberdieAbteilung1: TMenuItem;
    N16: TMenuItem;
    m_AuswertungmitausgewhlterFirma: TMenuItem;
    PopupMenu_firma: TPopupMenu;
    m_AuswertungmitausgewhlterFirma1: TMenuItem;
    m_DokumentohneSpeichernerstellen: TMenuItem;
    m_DokumentohneSpeichernerstellen_firma: TMenuItem;
    m_FirmenDokumentohneSpeichernerstellen1: TMenuItem;
    m_ProbandenDokumentohneSpeichernerstellen: TMenuItem;
    SpeedButton21: TSpeedButton;
    SpeedButton22: TSpeedButton;
    Edit_leer: TEdit;
    Label266: TLabel;
    DBEdit_bestellnummer: TDBEdit;
    DBComboBox_konto_f: TDBComboBox;
    Label267: TLabel;
    Label268: TLabel;
    DBComboBox_konto_p: TDBComboBox;
    Arbeitsmedizin: TOLeContainerext;
    RichEdit_firma: TRichEdit;
    RichEdit_firma_ansprech: TRichEdit;
    RichEdit_zeiten_soll: TRichEdit;
    RichEdit_proband: TRichEdit;
    Label269: TLabel;
    DBEdit_anrede: TDBEdit;
    Splitter_zeitabrechnung: TSplitter;
    Bullet: TMenuItem;
    Font1: TMenuItem;
    Font2: TMenuItem;
    m_CSVImport: TMenuItem;
    Timer_csv: TTimer;
    m_archiv_all_ausgeschieden: TMenuItem;
    Label272: TLabel;
    DBEdit_kostenstelle: TDBEdit;
    ToolButton_abort: TToolButton;
    ToolButton18: TToolButton;
    m_PDFneueinlesen: TMenuItem;
    m_PDFneueinlesen1: TMenuItem;
    m_InfomationzumDokument: TMenuItem;
    m_rechnungspositonen_wieder_freigeben: TMenuItem;
    Panel_re_vorgabe_re: TPanel;
    GroupBox9: TGroupBox;
    RichEdit_revorgabe: TRichEdit;
    GroupBox_rechnung_automatisch: TGroupBox;
    Label273: TLabel;
    DBEdit2: TDBEdit;
    SpeedButton_wdv_rechnung_fi: TSpeedButton;
    DBCombo_num_refi: TDBCombo_num;
    Label274: TLabel;
    Label275: TLabel;
    DBEdit8: TDBEdit;
    SpeedButton23: TSpeedButton;
    Label276: TLabel;
    DBCombo_num_repro: TDBCombo_num;
    m_rechnungs_all: TMenuItem;
    Panel_labor_werte_top: TPanel;
    GroupBox_labor_werte: TGroupBox;
    SpeedButton3: TSpeedButton;
    Label2: TLabel;
    Label11: TLabel;
    Label45: TLabel;
    Label129: TLabel;
    Label_labor_wert: TLabel;
    DBText_einheit: TDBText;
    Label130: TLabel;
    Label144: TLabel;
    Label146: TLabel;
    SpeedButton_lab_ergebnis: TSpeedButton;
    Label270: TLabel;
    Label271: TLabel;
    DBEdit_lab_datum: TDBEdit;
    BitBtn_labwahl: TBitBtn;
    DBEdit_labor_wert: TDBEdit;
    DBEdit_grenzwert: TDBEdit;
    DBMemo_norm: TDBMemo;
    DBCombo_num_lab_unters: TDBCombo_num;
    DBMemo_testbezogenerhinweis: TDBMemo;
    Edit_lab_typ: TEdit;
    DBMemo_ergebnistext: TDBMemo;
    DBEdit_labor_lab_id: TDBEdit;
    DBEdit_labor_doc_id: TDBEdit;
    TabSheet_labor_hinweis: TTabSheet;
    GroupBox_labor_hinweise: TGroupBox;
    Panel_labor_hinweise_top: TPanel;
    BitBtn_labor_hinweise_wahl: TBitBtn;
    DBGrid_labor_hinweise: TDBGrid;
    Panel_labor_hinweis_left: TPanel;
    Panel_labor_hinweis_right: TPanel;
    GroupBox13: TGroupBox;
    Panel13: TPanel;
    Panel20: TPanel;
    Panel33: TPanel;
    Panel_labor_hinweis_bottom: TPanel;
    Panel35: TPanel;
    DBMemo_labor_hinweis: TDBMemo;
    BitBtn_laborhinweis_automatisch: TBitBtn;
    BitBtn_labor_hinweise_loeschen: TBitBtn;
    Panel_labor_hinweise_all: TPanel;
    GroupBox_labor_hinweise_all: TGroupBox;
    Panel36: TPanel;
    BitBtn_laborhinweis_automatisch_all: TBitBtn;
    DBGrid_labor_hinweise_all: TDBGrid;
    Panel37: TPanel;
    Panel41: TPanel;
    Panel42: TPanel;
    BitBtn_labor_hinweise_loeschen_all: TBitBtn;
    DBGrid_rechnung_regeln: TDBGrid;
    GroupBox_rechnung_regeln_einstellungen: TGroupBox;
    DBCheckBox_anonym: TDBCheckBox;
    Label277: TLabel;
    Label279: TLabel;
    Label280: TLabel;
    DBComboBox_re_zeitraum: TDBComboBox;
    DBCheckBox_rechnung_regel_pauschcal: TDBCheckBox;
    DBCheckBox_rechnung_regel_stunde: TDBCheckBox;
    DBCheckBox_rechnung_regel_fahrstrecke: TDBCheckBox;
    DBCheckBox_re_vorsorge: TDBCheckBox;
    DBCheckBox_rechnung_regel_impfung: TDBCheckBox;
    DBCheckBox_rechnung_regel_labor: TDBCheckBox;
    Panel_rechnung_regeln_oben: TPanel;
    BitBtn_rechnung_regeln_add: TBitBtn;
    DBEdit_rechnung_regeln_name: TDBEdit;
    BitBtn_rechnung_regel_del: TBitBtn;
    DBCheckBox_rechnung_regel_befund: TDBCheckBox;
    DBCheckBox_rechnung_regel_artikel: TDBCheckBox;
    DBCombo_num_rechnung_regeln_ust: TDBCombo_num;
    Label278: TLabel;
    Panel43: TPanel;
    Panel44: TPanel;
    DBCombo_num_rechnung_regeln_monat: TDBCombo_num;
    BitBtn_zeit_timer: TBitBtn;
    N17: TMenuItem;
    m_ArchivierteProbandenanzeigen: TMenuItem;
    CheckBox_suchen_wortanfang: TCheckBox;
    Label281: TLabel;
    DBCombo_num_besonderheiten_untersucher: TDBCombo_num;
    PopupMenu_dbmemo: TPopupMenu;
    m_textbausteineinfgen_in_memo: TMenuItem;

	  procedure BitBtn_beurt_ueberClick(Sender: TObject);
	  procedure BitBtn_artminusClick(Sender: TObject);
	  procedure SpeedButton_datClick(Sender: TObject);
	  procedure SpeedButton2Click(Sender: TObject);
	  procedure SpeedButton3Click(Sender: TObject);
	  procedure OpenbuttonClick(Sender: TObject);
	  procedure closebuttonClick(Sender: TObject);
	  procedure ToolButton8Click(Sender: TObject);
	  procedure UndobuttonClick(Sender: TObject);
	  procedure CopybuttonClick(Sender: TObject);
	  procedure PastebuttonClick(Sender: TObject);
	  procedure FontNameChange(Sender: TObject);
	  procedure FontSizeChange(Sender: TObject);
	  procedure BoldButtonClick(Sender: TObject);
	  procedure ItalicButtonClick(Sender: TObject);
	  procedure UnderlineButtonClick(Sender: TObject);
	  procedure ToolButton_colorClick(Sender: TObject);
	  procedure LeftAlignClick(Sender: TObject);
	  procedure CenterAlignClick(Sender: TObject);
	  procedure RightAlignClick(Sender: TObject);
	  procedure BulletsButtonClick(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
    procedure events_zuordnen;
    procedure wartefenster_zeigen;

		function dateiendung(datei: string) : string;
	  procedure BitBtn_unters_planenClick(Sender: TObject);
	  procedure BitBtn_impf_planenClick(Sender: TObject);
	  //function  db_planen(table: tzquery;modus:integer):boolean;
	  procedure labor_zeichnen;
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
	  procedure pagecontrol_mainChange(Sender: TObject);
	  procedure ToolButton_neuClick(Sender: TObject);
	  procedure loeschen;
	  procedure ToolButton_abbrechenClick(Sender: TObject);
	  procedure m_TerminuebersichtClick(Sender: TObject);
	  procedure BitBtn_bes_auswahlClick(Sender: TObject);
	  procedure bereich_speichern;
	  procedure BitBtn_icd_wahlClick(Sender: TObject);
	  procedure BitBtn_beurt_einfClick(Sender: TObject);
	  procedure PageControl_untersuchungChange(Sender: TObject);
	  procedure ToolButton_editClick(Sender: TObject);
	  procedure ToolButton25Click(Sender: TObject);
	  procedure DBgridEXT_namenKeyPress(Sender: TObject; var Key: Char);
	  procedure BitBtn_bef_delClick(Sender: TObject);
	  procedure RadioGroup_auffaelligClick(Sender: TObject);
	  procedure dbrichedit_befundDblClick(Sender: TObject);
	  procedure pagecontrol_mainChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure m_UntersuchungClick(Sender: TObject);
	  procedure m_ImpfungenClick(Sender: TObject);
    procedure m_labClick(Sender: TObject);
	  procedure VtChartClick(Sender: TObject);
	  procedure VtChartDblClick(Sender: TObject);
	  procedure m_KBesonderheitenClick(Sender: TObject);
	  procedure m_bedienerClick(Sender: TObject);
	  procedure m_FristenClick(Sender: TObject);
	  procedure m_StatusClick(Sender: TObject);
    procedure m_terminartClick(Sender: TObject);
    procedure PageControl_langfristChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure Memo_befundChange(Sender: TObject);
	  procedure ComboBoxClick(Sender: TObject);
	  procedure PageControl_firmaChange(Sender: TObject);
	  procedure PageControl_firmaChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure SpeedButton6Click(Sender: TObject);
	  procedure SpeedButton_z_vonClick(Sender: TObject);
	  procedure DBgridEXT_anwesenheitsplanungDblClick(Sender: TObject);
	  procedure Tree_bereichChanging(Sender: TObject; Node: TTreeNode;
		 var AllowChange: Boolean);
	  procedure ana_toolbar_sperren;
	  procedure ana_toolbar_entsperren;
	  procedure ToolButton_closeClick(Sender: TObject);
	  procedure DBLookupComboBox_impfung_stoffEnter(Sender: TObject);
	  procedure DBLookupComboBox_impfung_stoffExit(Sender: TObject);
	  procedure ToolButton_termineClick(Sender: TObject);
	  procedure MaskEdit2Change(Sender: TObject);
	  procedure DBgridEXT_firmaKeyPress(Sender: TObject; var Key: Char);
	  procedure SpeedButton5Click(Sender: TObject);
	  procedure SpeedButton7Click(Sender: TObject);
	  procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	  procedure menue_endeClick(Sender: TObject);
	  procedure RichEdit_befundExit(Sender: TObject);
	  procedure PageControl_laborChange(Sender: TObject);
	  procedure xygraphResize(Sender: TObject);
	  procedure Auswertungen1Click(Sender: TObject);
	  procedure ToolButton_auswertungClick(Sender: TObject);
	  procedure editClick(Sender: TObject);
	  procedure menue_saveClick(Sender: TObject);
	  procedure Menue_NewClick(Sender: TObject);
	  procedure menue_deleteClick(Sender: TObject);
      procedure ComboBox_historyChange(Sender: TObject);
	  procedure Arbeitsmedizin_FirmaLButtonDown(Sender: TObject);
	  procedure Textvorlagen1Click(Sender: TObject);
	  procedure ToolButton_ma_suchClick(Sender: TObject);
	  procedure m_Berechtigungen_bearbClick(Sender: TObject);
      procedure hilfe_inhaltClick(Sender: TObject);
	  procedure DBgridEXT_untersuchungCellClick(Column: TColumn);
	  procedure PageControl_untersuchungExit(Sender: TObject);
	  procedure m_maverschiebenClick(Sender: TObject);
	  procedure m_BilderListeClick(Sender: TObject);
	  procedure m_SystemClick(Sender: TObject);
	  procedure Arbeitsmedizin_ProbandLButtonDown(Sender: TObject);
	  procedure savebuttonClick(Sender: TObject);
	  procedure m_mazusammenfuehrenClick(Sender: TObject);
	  procedure DBLookupComboBox_unters_statusClick(Sender: TObject);
	  //procedure einladung_drucken(untersuchung,art,modus,i_typ:integer;s_datum,s_zeit:string) ;
	  procedure einladung_drucken(nummer:int64;modus:integer);
	  procedure Nationen1Click(Sender: TObject);
	  procedure natnew1Click(Sender: TObject);
	  procedure m_ImpfabfolgeClick(Sender: TObject);
	  procedure BitBtn_labwahlClick(Sender: TObject);
	  procedure BitBtn_untwahlClick(Sender: TObject);
	  procedure BitBtn_labaendernClick(Sender: TObject);
	  procedure Tree_laborChange(Sender: TObject; Node: TTreeNode);
	  procedure PageControl_stammdatenChange(Sender: TObject);
	  procedure DBCheckBox_aunfallClick(Sender: TObject);
	  procedure pagecontrol_ambulanzChange(Sender: TObject);
	  procedure DBCheckBox_aunfallEnter(Sender: TObject);
	  procedure SpeedButton_u_erstClick(Sender: TObject);
	  procedure SpeedButton_u_waClick(Sender: TObject);
	  procedure SpeedButton_u_daClick(Sender: TObject);
    procedure SpeedButton_u_khClick(Sender: TObject);
	  procedure BitBtn_u_lokalClick(Sender: TObject);
	  procedure BitBtn_u_artClick(Sender: TObject);
    procedure BitBtn_u_versorgClick(Sender: TObject);
    procedure ToolButton_schichtplanClick(Sender: TObject);
	  procedure m_SchichtmodelleClick(Sender: TObject);
	  procedure m_dbabgleichClick(Sender: TObject);
	  procedure m_SchichtPlanClick(Sender: TObject);
	  procedure Berufsgenossenschaften1Click(Sender: TObject);
	  procedure Panel_ambulanzResize(Sender: TObject);
	  procedure grid_ambulanzAfterEdit(Sender: TObject; col, row: Integer);
    procedure grid_ambulanzExit(Sender: TObject);
	  procedure email_emailClick(Sender: TObject);
	  procedure Panel_befund_sehtestResize(Sender: TObject);
	  procedure Edit_s_horizontalChange(Sender: TObject);
	  procedure SpeedButton_firma_suchClick(Sender: TObject);
	  procedure DBCheckBox_abMouseUp(Sender: TObject; Button: TMouseButton;
		 Shift: TShiftState; X, Y: Integer);
	  procedure SpeedButton12Click(Sender: TObject);
	  procedure Timer_icdTimer(Sender: TObject);
    procedure Edit_befChange(Sender: TObject);
    procedure Edit_befKeyDown(Sender: TObject; var Key: Word;
		 Shift: TShiftState);
	  procedure SpinEdit_sChange(Sender: TObject);
	  procedure SerienbriefFelder1Click(Sender: TObject);
	  procedure Gewerbeaufsichtsmter1Click(Sender: TObject);
	  procedure ToolButton_abteilungClick(Sender: TObject);
    procedure m_taetigkeitenClick(Sender: TObject);
	  procedure DBLookupComboBox_taeitigkeitEnter(Sender: TObject);
	  procedure DBLookupComboBox_taeitigkeitExit(Sender: TObject);
    procedure VerteilteSysteme1Click(Sender: TObject);
	  procedure m_SystemTypClick(Sender: TObject);
	  procedure m_SatellitschluesselClick(Sender: TObject);
    procedure m_SatellitiniClick(Sender: TObject);
	  procedure UntersuchungsBefunde1Click(Sender: TObject);
	  procedure Normalbefund1Click(Sender: TObject);
	  procedure Auffllig1Click(Sender: TObject);
	  procedure SpeedButton_kalender1Click(Sender: TObject);
	  procedure DBComboBox_u_einClick(Sender: TObject);
	  procedure SpeedButton_a_einClick(Sender: TObject);
	  procedure SpeedButton_a_aufClick(Sender: TObject);
	  procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure BitBtn_b_zurueckClick(Sender: TObject);
    procedure BitBtn_abteilungClick(Sender: TObject);
	  procedure grid_ambulanzEnter(Sender: TObject);
	  procedure arztelistClick(Sender: TObject);
	  procedure DBgridEXT_namenDblClick(Sender: TObject);
    procedure dbTreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure dbTreeViewChanging(Sender: TObject; Node: TTreeNode;
		 var AllowChange: Boolean);
	  procedure CheckBox_impfungClick(Sender: TObject);
    procedure dbTreeViewDblClick(Sender: TObject);
	  procedure Befunde1Click(Sender: TObject);
    procedure Panel_tResize(Sender: TObject);
	  procedure DBLookupComboBox1CloseUp(Sender: TObject);
	  procedure baumrunter1Click(Sender: TObject);
	  procedure baumhoch1Click(Sender: TObject);
	  procedure BitBtn3Click(Sender: TObject);
	  procedure m_maloeschenClick(Sender: TObject);
	  procedure Grid_sehtestKeyDown(Sender: TObject; var Key: Word;
		 Shift: TShiftState);
	  procedure Splitter3Moved(Sender: TObject);
    procedure FormShow(Sender: TObject);
	  procedure Branchen1Click(Sender: TObject);
    procedure FirmenGruppen1Click(Sender: TObject);
	  procedure ToolButton_firmaClick(Sender: TObject);
	  procedure SpeedButton15Click(Sender: TObject);
	  procedure SpeedButton16Click(Sender: TObject);
	  procedure SpeedButton_besonderheitenClick(Sender: TObject);
	  procedure m_maArchivierenClick(Sender: TObject);
	  procedure PasswortchangeClick(Sender: TObject);
    procedure emailListe1Click(Sender: TObject);
	  procedure Edit_groesseChange(Sender: TObject);
	  procedure Edit_fev1Change(Sender: TObject);
    procedure SpeedButton_haClick(Sender: TObject);
	  procedure pagecontrol_mainDrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
	  procedure Arbeitsmedizin_BefundLButtonDown(Sender: TObject);
	  procedure Info1Click(Sender: TObject);
	  procedure m_registrierenClick(Sender: TObject);
    procedure BitBtn_taetigkeitClick(Sender: TObject);
	  procedure import_excelClick(Sender: TObject);
	  procedure m_LaborLDTClick(Sender: TObject);
	  procedure PageControl_befundeChange(Sender: TObject);
    procedure RadioGroup_parameterClick(Sender: TObject);
    procedure BitBtn_f_rechnung_plClick(Sender: TObject);
	  procedure PageControl_rechnungenChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure BitBtn_redruckClick(Sender: TObject);
	  procedure m_UntersuchungsSchritteClick(Sender: TObject);
	  procedure RichEdit_befundChange(Sender: TObject);
	  procedure BitBtn_fallmonitorClick(Sender: TObject);
      procedure Zeituebersicht_anzeigen(Sender: TObject);
	  procedure m_GOaeListeClick(Sender: TObject);
	  procedure akt_editorSelectionChange(Sender: TObject);
	  procedure m_Personalien_druckenClick(Sender: TObject);
	  procedure DBCombo_num_vor_statusClick(Sender: TObject);
	  procedure DBComboBox_u_ausClick(Sender: TObject);
	  procedure BitBtn_audiogramm_exportClick(Sender: TObject);
	  procedure redruck(dat: integer);
	  procedure BitBtn_re_maClick(Sender: TObject);
    procedure SpinEdit_diChange(Sender: TObject);
	  procedure Edit_bmiChange(Sender: TObject);
    procedure ComboBoxChange(Sender: TObject);
	  procedure ToolButton_materialClick(Sender: TObject);
	  procedure Lieferanten1Click(Sender: TObject);
    procedure BitBtn_art_wahlClick(Sender: TObject);
    procedure DBEdit_art_mengeChange(Sender: TObject);
    procedure DBEdit_art_rabattChange(Sender: TObject);
    procedure DBEdit_art_endpreisChange(Sender: TObject);
    procedure Edit_rabatt_prozChange(Sender: TObject);
    procedure ToolButton_sichernClick(Sender: TObject);
	  procedure ToolButton_loeschenClick(Sender: TObject);
    procedure BitBtn_artplusClick(Sender: TObject);
    procedure m_arbeitsplatzClick(Sender: TObject);
    procedure PageControl_befundeChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure m_GefaehrdungenClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	  procedure Edit_suchEnter(Sender: TObject);
    procedure ComboBox_historyKeyDown(Sender: TObject; var Key: Word;
		 Shift: TShiftState);
    procedure Edit_firma_suchKeyDown(Sender: TObject; var Key: Word;
		 Shift: TShiftState);
	  procedure dbTreeViewKeyDown(Sender: TObject; var Key: Word;
		 Shift: TShiftState);
	  procedure SpeedButton_datum_artikelClick(Sender: TObject);
	  procedure SpeedButton_bes_erledigtClick(Sender: TObject);
	  procedure m_druck_alleBeschClick(
		 Sender: TObject);
	  procedure m_druck_bescheinigung_karteiClick(Sender: TObject);
	  procedure SpeedButton_verschicktClick(Sender: TObject);
	  procedure BitBtn_f_rechnung_phClick(Sender: TObject);
	  procedure Lokalisation1Click(Sender: TObject);
	  procedure Verletzungsart1Click(Sender: TObject);
	  procedure Versorgung1Click(Sender: TObject);
	  procedure SpeedButton_d1Click(Sender: TObject);
	  procedure BitBtn_waiClick(Sender: TObject);
	  procedure MaskEdit_d1Change(Sender: TObject);
	  procedure Edit_WAIChange(Sender: TObject);
	  procedure DBEdit_StundenChange(Sender: TObject);
	  procedure DBEdit15Change(Sender: TObject);
	  procedure Optionen1Click(Sender: TObject);
	  procedure DBEdit_reverschChange(Sender: TObject);
	  procedure SpeedButton_diagn_bisClick(Sender: TObject);
	  procedure m_kapitelClick(Sender: TObject);
	  procedure BitBtn_kontaktimportClick(Sender: TObject);
	  procedure systemExecuteMacro(Sender: TObject; Msg: TStrings);
    procedure m_TextbausteineClick(Sender: TObject);
    procedure Textbausteine1Click(Sender: TObject);
    procedure m_Export_ProbandenbersichtClick(
      Sender: TObject);
    procedure DBgridEXT_namenColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure BitBtn_leistungswahlClick(Sender: TObject);
    procedure m_AbrechnungsanlaesseClick(Sender: TObject);
    procedure SpeedButton_z_bisClick(Sender: TObject);
    procedure SpeedButton_z1Click(Sender: TObject);
    procedure SpeedButton_zfilternClick(Sender: TObject);
    procedure ImportberBDT1Click(Sender: TObject);
    procedure BitBtn_f_rechnung_paeClick(Sender: TObject);
    procedure notebook_mainPageChanged(Sender: TObject);
    procedure ComboBox_zeiterfassungChange(Sender: TObject);
    procedure m_AbrechnungskriteriumClick(Sender: TObject);
    procedure BitBtn_redurckmaClick(Sender: TObject);
    procedure BitBtn_redruck_fiClick(Sender: TObject);
    procedure DBRadioGroup_beurteilung_eignungChange(Sender: TObject);
    procedure DBCombo_num_vor_statusChange(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure DBEdit3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FirmenFilter1Click(Sender: TObject);
    procedure Edit_suchKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton_doceigenschaften1Click(Sender: TObject);
    procedure Splitter4Moved(Sender: TObject);
    procedure ZeiterfassungfrmehrereFirmen1Click(Sender: TObject);
    procedure DBComboBox_zeitansatzKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button_firma_zeitpauschale_berechnenClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure m_Ber_ges_FirmaClick(Sender: TObject);
    procedure DBCombo_num_unt_art2Change(Sender: TObject);
    procedure DBCheckBox_zustimmung_beschMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBCombo_num_unt_art2MeasureItem(Control: TWinControl;
      Index: Integer; var Height: Integer);
    procedure DBCombo_num_unt_art2DrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure DBCombo_num_unt_art2Click(Sender: TObject);
    procedure DBgridEXT_texteDblClick(Sender: TObject);
    procedure PageControl_firmaDrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
    procedure DBEdit_z_stunden1Exit(Sender: TObject);
    procedure DBEdit_z_vonExit(Sender: TObject);
    procedure DBCombo_num_firma_zeiten_soll_kriteriumClick(
      Sender: TObject);
    procedure DBCombo_num_firma_zeiten_soll_kontoClick(Sender: TObject);
    procedure m_AbrechnungskontoClick(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure m_FirmenzusammenfgenClick(Sender: TObject);
    procedure grid_sehtestKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton11Click(Sender: TObject);
    procedure GDTProgramme1Click(Sender: TObject);
    procedure m_GDT_schreibenClick(Sender: TObject);
    procedure BitBtn_zeit_kmClick(Sender: TObject);
    procedure km_verbergen ;
    procedure km_anzeigen ;
    procedure DBgridEXt_firmaColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure BitBtn_rechnungsempfaengerClick(Sender: TObject);
    procedure DBgridEXT_repoDblClick(Sender: TObject);
    procedure DBgridEXT_rechnungDblClick(Sender: TObject);
    procedure DBgridEXt_firmaDblClick(Sender: TObject);
    procedure Zeitvorgabenbernehmen1Click(Sender: TObject);
    procedure pm_f_rechnung_wiederherstellenClick(Sender: TObject);
    procedure pm_zeit_verschiebenClick(Sender: TObject);
    procedure SpeedButton_beschdruck_probandClick(Sender: TObject);
    procedure DBEdit_druckChange(Sender: TObject);
    procedure m_AnredeClick(Sender: TObject);
    procedure BitBtn_zeit_mwahlClick(Sender: TObject);
    procedure mAbrechnungTaetigkeitClick(Sender: TObject);
    procedure tbs_auswahl(modus:string;tab_nr:integer ;snr,suche:string);
    procedure hinweise_einfuegen(i_tab:integer;s_tab_nummer:string);
    procedure tateigkeit_auswahl(modus,snr:string );
    procedure SpeedButton_firmasollist_zeitClick(Sender: TObject);
    procedure DBComboBox_zeitansatzChange(Sender: TObject);
    procedure AnmeldeProtokoll1Click(Sender: TObject);
    procedure AnzeigeKartei1Click(Sender: TObject);
    procedure SpeedButton_ag_refreshClick(Sender: TObject);
    procedure SpeedButton_firna_text_wdvClick(Sender: TObject);
    procedure DBEdit_firma_text_wdvChange(Sender: TObject);
    procedure TreeView_agendaDblClick(Sender: TObject);
    procedure Wiedervorlage1Click(Sender: TObject);
    procedure Splitter_agendaMoved(Sender: TObject);
    procedure SpeedButton18Click(Sender: TObject);
    procedure SpeedButton19Click(Sender: TObject);
    procedure DBComboBox_anredeClick(Sender: TObject);
    procedure DBCombo_num_geschlechtClick(Sender: TObject);
    procedure SpeedButton_wdv_dokumenteClick(Sender: TObject);
    procedure DBEdit_wdv_dokumenteChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure m_WeiterversorgungClick(Sender: TObject);
    procedure m_Folgebescheinigung_karteiClick(Sender: TObject);
    procedure m_AbrechnungTtigkeitalphabetischClick(Sender: TObject);
    procedure m_BeurteilungClick(Sender: TObject);
    procedure berechtigung_firma;
    procedure Panel_zeitabrechnung_reResize(Sender: TObject);
    procedure Jahresabschluss1Click(Sender: TObject);
    procedure m_BuchungsperiodeClick(Sender: TObject);
    procedure Spenden1Click(Sender: TObject);
    procedure dropdown_richedit_FettClick(Sender: TObject);
    procedure dropdown_richedit_KursivClick(Sender: TObject);
    procedure dropdown_richedit_UnterstrichenClick(Sender: TObject);
    procedure dropdown_richedit_zurcksetzenClick(Sender: TObject);
    procedure SpeedButton_labornummerClick(Sender: TObject);
    procedure m_LaborErgebnistexteClick(Sender: TObject);
    procedure SpeedButton_lab_ergebnisClick(Sender: TObject);
    procedure DBComboBox_zeitansatzExit(Sender: TObject);
    procedure DBgridEXT_repoLButtonDown(Sender: TObject);
    procedure m_word_pdf_anzeige_karteiClick(Sender: TObject);
    procedure alsPDFanzeigen1Click(Sender: TObject);
    procedure m_SummederZeitenanzeigenClick(Sender: TObject);
    procedure PageControl_firma_einstellungenChange(Sender: TObject);
    procedure m_erweiterteFehlermeldungenClick(Sender: TObject);
    procedure SpeedButton_wv_freitextClick(Sender: TObject);
    procedure PageControl_wdvChange(Sender: TObject);
    procedure Timer_untplanTimer(Sender: TObject);
    procedure daten1Click(Sender: TObject);
    procedure SpeedButton20Click(Sender: TObject);
    procedure m_f_allegelschtenPositionenwiederaktivierenClick(
      Sender: TObject);
    procedure m_propertiesClick(Sender: TObject);
    procedure m_iconicClick(Sender: TObject);
    procedure history_erstellen;
    procedure history_entfernen;
    function  aktuelle_tabelle:thist_set;
    procedure ToolButton_historyClick(Sender: TObject);
    procedure Timer_datumswechselTimer(Sender: TObject);
    procedure m_allemarkierenClick(Sender: TObject);
    procedure m_rechnung_f_allemarkierenClick(Sender: TObject);
    procedure InfozumDokument1Click(Sender: TObject);
    procedure OleSpeichern1Click(Sender: TObject);
    procedure Oleladen1Click(Sender: TObject);
    procedure m_projektmithbClick(Sender: TObject);
    procedure m_UnfallverteilunginderFirmaClick(Sender: TObject);
    procedure m_UnfallverteilunginderAbteilungClick(Sender: TObject);
    procedure m_ProjektemitPrioAClick(Sender: TObject);
    procedure m_ProjektemitPrioBClick(Sender: TObject);
    procedure m_ProjektemitPrioCClick(Sender: TObject);
    procedure m_ueberpruefenderBescheinigungenClick(Sender: TObject);
    procedure speichern;
    procedure AuswertungberdieAbteilung1Click(Sender: TObject);
    procedure m_AuswertungmitausgewhlterFirmaClick(Sender: TObject);
    procedure m_AuswertungmitausgewhlterFirma1Click(Sender: TObject);
    procedure m_DokumentohneSpeichernerstellenClick(Sender: TObject);
    procedure m_DokumentohneSpeichernerstellen_firmaClick(Sender: TObject);
    procedure BitBtn_pos_prob_neuClick(Sender: TObject);
    procedure PageControl_firma_einstellungenChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure Splitter_zeitabrechnungMoved(Sender: TObject);
    procedure Panel_zeitabrechnung_liResize(Sender: TObject);
    procedure RichEdit_Enter(Sender: TObject);
    procedure BulletClick(Sender: TObject);
    procedure Font1Click(Sender: TObject);
    procedure Font2Click(Sender: TObject);
    procedure m_CSVImportClick(Sender: TObject);
    procedure Timer_csvTimer(Sender: TObject);
    procedure m_archiv_all_ausgeschiedenClick(Sender: TObject);
    procedure db_komp_onchange(Sender: TObject);
    procedure db_edit_change(Sender: TObject);
    procedure ToolButton_abortClick(Sender: TObject);
    procedure m_PDFneueinlesenClick(Sender: TObject);
    procedure m_PDFneueinlesen1Click(Sender: TObject);
    procedure m_InfomationzumDokumentClick(Sender: TObject);

    procedure m_rechnungspositonen_wieder_freigebenClick(Sender: TObject);
    procedure SpeedButton_wdv_rechnung_fiClick(Sender: TObject);
    procedure SpeedButton23Click(Sender: TObject);
    procedure m_rechnungs_allClick(Sender: TObject);
    procedure BitBtn_labor_hinweise_wahlClick(Sender: TObject);
    procedure BitBtn_laborhinweis_automatischClick(Sender: TObject);
    procedure BitBtn_labor_hinweise_loeschenClick(Sender: TObject);
    procedure BitBtn_laborhinweis_automatisch_allClick(Sender: TObject);
    procedure BitBtn_labor_hinweise_loeschen_allClick(Sender: TObject);
    procedure BitBtn_rechnung_regeln_addClick(Sender: TObject);
    procedure BitBtn_rechnung_regel_delClick(Sender: TObject);
    procedure BitBtn_zeit_timerClick(Sender: TObject);
    procedure DBCombo_num_abrechnungskriteriumEnter(Sender: TObject);
    procedure DBCombo_num_abrechnungskriteriumExit(Sender: TObject);
    procedure m_ArchivierteProbandenanzeigenClick(Sender: TObject);
    procedure DBEdit_bes_erledigt_datChange(Sender: TObject);
    procedure m_textbausteineinfgen_in_memoClick(Sender: TObject);
    procedure DBComboBox_konto_fClick(Sender: TObject);
	private
	  { Private-Deklarationen}
	  firma_gefiltert,baum_neu,form_textbausteine_CheckBox_alpha_checked:boolean;
    konto_gewaehlt:integer;
	  //drag,altknoten:TTreeNode;
    g_erstelldatum_besch:string;
	  kart_index:integer; //untersuchung neu index
	  //b_gesperrt : array [1..8] of boolean;
	  version:string;


	  function last_nr_change_db:integer;
	  procedure dateitest(dat:string);

	  procedure parent_state(node: ttreenode; aufindex:integer);
	  procedure Bereich_del;
	  procedure unt_eintragen(tabelle,i_typ,i_paw:integer;i_mitarbeiter:int64; datum:tdatetime);
	  function zeit_str(stunden, minuten:integer):string;
	  procedure history_delete(index:integer);
	  function gehezu_stammdaten(nummer: int64):boolean;
	  function ma_suchen(n:string):boolean;

	  function optionen_show(db:integer) : boolean;
     function vorlage_waehlen(vorlage:integer;fspez:boolean):string;
	  function text_neu(modus:integer;datum:tdate;ansicht:integer;pdf_loeschen:boolean ):int64;
	  procedure anamnese_drucken(sender:tobject);
	  procedure labor_drucken_alle(sender:tobject);
	  procedure labor_drucken_speziell(sender:tobject);
	  procedure Unfallanzeige_drucken(sender:tobject);
	  procedure Unfallanzeige_drucken_seite(sender:tobject);
	  procedure ambulanz_drucken_vital(sender:tobject);

	  function ambulanz_neu(datum:tdate;modus:integer):int64;
	  function befunde_neu(datum:tdate;kapitel:string):int64;
	  function rechnung_neu(datum:tdate):int64;
	  function verkauf_neu(datum:tdate):int64;
	  function kontakt_neu(datum:tdate):int64;
	  procedure untersuchung_einzelwahl(bereich:integer);
	  procedure untersuchung_drucken_speziell(sender:tobject);
	  procedure untersuchung_drucken_alle(sender:tobject);
	  procedure impfung_drucken_alle(sender:tobject);
	  procedure arbeitsunfall_time_show;
		procedure aerzteliste(feld:string);
		procedure ambulanz_memo_add(bereich:integer;fz:boolean);
		procedure vorlage_laden(i_filter, i_firma:integer); //bookmarks werden gelesen einf�gen selber
		procedure bescheinigung_drucken(sender:tobject);
		procedure bescheinigung_drucken_Proband(sender:tobject);
		procedure bescheinigung_drucken_Firma_Proband(sender:tobject);
		procedure bescheinigung_emailen(sender:tobject);
		procedure Kurz_Anzeige_mailen(sender:tobject);
		procedure Anhang_mailen(sender:tobject);
		procedure adressat_ausdruck(sender:tobject);

		procedure Kurz_Anzeige_drucken(sender:tobject);
		procedure personalien_druck(sender:tobject);

		procedure sichtbarkeiten_einstellen;
		procedure prg_typ_einstellen;

		procedure vert_syst_menue;
		function  aerzteanzeigen(edit:boolean;var index:int64):pkontaktpointer;
		procedure tree_befunde_filtern(checken:boolean);
		function  grant:boolean;
		function  ist_db_da:boolean;
		procedure form_tabellen_berechtigung(ber:integer);
		procedure form_untersuchung_berechtigung(ber:integer);
		procedure toolbar_berechtigung(bar:ttoolbar;ber:integer);
		function neue_felder_abfragen: boolean;
    function neue_felder_abfragen_once: boolean;
    function kleine_aenderungen:boolean;
      procedure varchar_ex(tab_name:string);
		procedure lufu_normwert;
		procedure storno_sql(zeig_st:boolean);
		procedure parameter_abfrage;
		procedure einstellungen_lesen;
		procedure einstellungen_schreiben;
    procedure einstellungen_lesen_global;
		procedure einstellungen_schreiben_global;
		procedure inilesen;
    procedure iniloeschen;



		procedure ana_speichern;
		procedure dbcombo_laden;
		function makepfad(pf:string):string;
		procedure tabellen_material_einstellen;
		procedure Edit_bef_check(Sender: TObject);
		function bruttos(netto,mws:string):string;
		procedure baum_sichtbarkeiten(check:boolean);
		procedure temp_tabellen;

		function pcid(mc:string):integer;
     procedure repo_aendern(neu,fipr:boolean);
     procedure repo_aendern_mehrere(grid:tdbgridext);
     function repositionen_verweismarkiert: boolean;
     procedure rechnung_refresh;
     //procedure rechnung_aktualisieren;

     procedure einsatzZeiteneinstellen;
     procedure einsatzzeiten_filtern;

     procedure firma_zeitvorgaben_berechnen;
     function GDT_schreiben(datei,programm,id_empfaenger: string; parameter: integer; gdt_version:integer):boolean;
     procedure anlass_aufwand_einfuegen(snr,saufwand:string);
     procedure tabelle_tbs_einfuegen(i_tabelle,i_tab_nummer,i_tbs:string;liste:tstringlist);
     procedure leistungsauswahl(parameter:string);
     procedure leistung_edit;
     Procedure kartei_aktualisieren;

     procedure Zeituebersicht_ausgabe(modus:integer);
     procedure spenden;

     procedure aktualisiere_wiedervorlage;
     procedure aktualisiere_freitextsuche;
     procedure dokumente_texte_komprimieren;
     procedure zeitansatz_lesen;
     function untersucher_stempel(nr:integer):string;
     procedure reihenfolge_schreiben( tabelle:tzquery;gruppenwechsel:string);
     procedure ha_einfuegen;
     function ServiceGetStatus(sMachine, sService: PChar): DWORD;
     procedure fehler();
     procedure auswertung_abt(modus:boolean);
     procedure gehe_zu_history(modus:boolean);
     procedure check_tables(modus:integer);
     procedure CSVImport(datei:string);
     procedure menue_aktivieren;
     procedure menue_deaktivieren;
     //procedure db_komp_onchange(Sender: TObject);

	public
	  { Public-Deklarationen}

    procedure berechtigungen_einstellen;
		procedure anamnese_speichern(flag:integer);
    procedure anamnese_laden(flag:integer);
    procedure HandleOnMessage(var Msg: TMsg; var Handled: Boolean);
      procedure AppException(Sender: TObject; E: Exception);
      procedure Appactivate(Sender: TObject);
      function GetCurrentVersion: String;
      procedure tbs(modus:integer;te:tobject);
		function bescheinigung_erstellen(dat_erstellt,udat,dat_bis:tdate;modus:integer;typ,adressat:string;n_d,druck_immer,drucken,nicht_sofort_druck,speichern_im_karteibaum:boolean;datname_email:string):boolean;
		function dateistruktur( dat: string):string;
		procedure abteilungskuerzel_eintragen;
	  //function dok_erstellen(container:tolecontainerext;vorlage:integer;anhang:boolean;ausgabe, druckmodus:string;word_schliessen:boolean;adressat, subject, tab_name: string; tabelle:tzquery;namen,ersatz:array of string;speicher_pfad:string ):boolean; //modus 1 email
    function dok_erstellen(container:tolecontainerext;vorlage:integer;tab_name: string; tabelle:tzquery;namen,ersatz:array of string ):boolean; //
    function email_erstellen(container:tolecontainerext;anhang:boolean;ausgabe,adressat,subject:string):boolean;
    function leeres_dokument_einhaengen(titel:string;datum:tdate;i_kapitel:integer):int64;
    function  leeren_text_erzeugen(titel:string):int64;
		procedure tab_reihenfolge(tabelle:tdataset;richtung:integer);
	  function formular_entsperren(refresh,nicht_ole:boolean):boolean;
	  procedure formular_sperren;
	  function liste_datasuchen(liste:tstrings;wert:integer):integer;
	  procedure listedeaktivieren(liste: tstrings);
	  procedure treedeaktivieren(tree: ttreeview);
	  function combowert(combo:tcombobox):int64;
	  procedure combogehezu(combo: tcombobox; wert:integer);
	  procedure combodeaktivieren(combo: tcombobox);
	  procedure combolesen(combo:tcombobox;tab_name, feldname,order,alle:string);
  	  procedure combolesen_s(combo:tcombobox;tab_name, feldname,order,alle:string);
	  procedure combolesen_q(combo:tcombobox;table: tzquery; feldname,alle:string);
	  procedure itemslesen(items:tstrings;table: tzquery; feldname,alle:string);
	  procedure itemsdeaktivieren(items: tstrings);
	  procedure sperren(komponente: tcomponent);
	  procedure p_sperren(sperrobjekt,tabelle:tzquery; user_id:integer;komponente: array of tcomponent);
	  procedure entsperren(komponente: tcomponent);
	  function p_entsperren(sperrobjekt,tabelle:tzquery; user_id:integer;komponente: array of tcomponent;refresh:boolean):boolean;
	  procedure table_focusieren;
	  procedure dbspeichern(direkt:boolean);
	  procedure namenanzeigen;
	  procedure mitarbeiter_filtern;

    function gehezu_firma(i_firma:integer):boolean;
    function gehezu_firma_ansprechpartner(i_firma:integer;i_nummer:int64):boolean;
    function gehezu_firma_sollist(i_firma:integer;i_nummer:int64):boolean;
    function gehezu_aktuntersuchung(i_stammdaten,i_nummer,i_bereich:int64):boolean;
    function gehezu_diagnosen(i_stammdaten,i_nummer:int64):boolean;
    function gehezu_kontakte(i_stammdaten,i_nummer:int64):boolean;

	  function gehezuuntersuchung(i_art:integer; i_stammdaten,i_nummer:int64{;i_firma:integer; table:tzquery}):boolean;
     function gehezubesonderheit(i_stammdaten,i_nummer:int64):boolean;
     function gehezudokument_proband(i_stammdaten,i_nummer:int64):boolean;
     function gehezutext(i_firma:integer;i_nummer:int64):boolean;
     function gehezu_rechnung_firma(i_firma:integer;i_nummer:int64):boolean;
     function gehezuprojekt(i_firma:integer;i_nummer:int64):boolean;
     function gehezu_ambulanz(i_stammdaten,i_nummer:int64):boolean;
     function gehezu_anamnese(i_stammdaten,i_nummer:int64):boolean;
	  procedure unt_planen(dataset:tdataset);
	  procedure firmaanzeigen;
	  procedure impfstofffiltern;
	  procedure bereicheinlesen(tree:ttreeview;tabelle:tzquery; index, feldname:string);
	  procedure listeeinlesen(liste:tstrings;tabelle:tzquery;feld:string);
	  procedure winrun(pfad, parameter: string);
	  procedure tool_aktiv(tabelle: tzquery);
	  procedure tool_inaktiv;
	  procedure olecontainer_word;
    function stream_komprimieren(tabelle:tzquery;feld:string):boolean;
	  procedure olecontainer_speichern(direkt:boolean;tabelle:tzquery;feld:string;ole:tolecontainerext);
	  FUNCTION  olecontainer_laden(tabelle:tzquery;feld:string;ole:tolecontainerext):boolean;
      function oleausdatei(OLeContainer: TOLeContainerext;dat:string): boolean;
      function oleausobjekt(OLeContainer: TOLeContainerext):boolean;
      procedure olecontainer_destroy(ole:tolecontainerext);
      procedure alle_olecontainer_schliessen;

	  procedure datei_speichern(tabelle:tzquery;feld, datname:string);
	  procedure  datei_laden(tabelle:tzquery;feld,datname:string);
	  procedure strings_speichern(tabelle:tzquery;feld:string;liste:tstrings);
	  procedure strings_laden(tabelle:tzquery;feld:string; const liste:tstrings);
    //function strings_laden_txt(tabelle:tzquery;feld:string):string;

	  function schluessel(wort1,wort2: string): string;
	  function nationalitaet(land:string):integer;
	  function i_taetigkeit(i_branche:integer;taetigkeit:string):int64;
      function i_abteilung(i_firma:integer;kuerzel:string;modus:integer):int64;
	  procedure datensatz_neu;
	  procedure termin_typ_update(tabelle: tzquery);
	  procedure arbeitsunfall_show;
	  procedure form_positionieren(formular: tform);
	  function  firma_ort(bereich:integer):string;
	  function schutzzmassnahmen(bereich:integer):string ;
	  function gehezu_name_Stammdaten(name, vorname: string;geb_dat:tdate):boolean;
	  //procedure abteilungsinfo(nummer:int64);
	  function trotz_satellit_weiter: boolean;
	  function lab_neu(datum:tdate;i_typ,i_status:integer;i_mitarbeiter:int64;i_untersucher:integer;speichern,still:boolean):int64;
	  function unt_neu(datum:tdate;i_typ,i_paw,i_status:integer;i_mitarbeiter:int64;i_untersucher:integer;speichern:boolean):int64;
	  function impf_neu(datum:tdate;i_typ,i_status:integer;i_mitarbeiter:int64;i_untersucher:integer;speichern:boolean):int64;
    function untersuchung_hinzufuegen(i_mitarbeiter:int64;bereich,i_status:integer; datum:tdate;multiselect:boolean):int64;
	  function besonderheit_neu(datum:tdate):int64;
	  function diagnose_neu(datum:tdate):int64;
	  procedure menue_leeren;
	  function lizenztest:boolean;
    function nicht_satellit_tab:boolean;

	  function umlaut_convert(text:string):string;
	  procedure history_add(name_string:string;nummer:int64);
	  procedure maskedit_format(me:tmaskedit);
	  procedure check_form_termine;
    function abt_manager(i_firma:integer;i_ma:int64;modus:string;zquery:tzquery; i_nummer:int64): twoint64;
    function abt_manager1(i_firma:integer;i_ma:int64;modus:string;zquery:tzquery; i_nummer:int64): twoint64;

    function string_entschluesseln(s:string):string;
    function string_entschluesseln_pr(s:string):string;
    function string_verschluesseln(s:string):string;
    function datei_entschluesseln_key(quelle, ziel:string):boolean;
    function datei_verschluesseln_key(quelle, ziel:string):boolean;
	  procedure taetigkeitanzeigen;
      procedure abteilunganzeigen;
    function sicherung(immer:boolean):boolean;
    procedure inischreiben(folder:integer);
    procedure kapitel_lesen(liste: tstrings; bereich:integer);
    procedure alle_kapitel_lesen;
    function zust_arbmed_besch_vorhanden(i_weitergabe,i_typ:integer;dat:tdatetime): boolean;
    function  sollzeiten(s_firmen: string; d1,d2: tdate;i_krit, i_zeitkonto:integer ):real;
    procedure rechnung_aktualisieren(renu:int64);
    procedure rechnung_aktualisieren_alt(renu:int64);
    procedure firmawaehlen(var fname: string; var fnummer:integer);
    procedure aktualisiere_nummern_kreis(nk:integer;modus:string);
    procedure aktualisiere_nummern_kreis_aktuell;
    function nummern_kreis_fehler:boolean;
    function  besteht_verbindung:boolean;
    procedure ueberfluessiges_loeschen;
    function zeitkonto(akt_untersucher:integer):integer;
    function   gehezu_rechnung(rnummer:string):boolean;
    function gdt_erzeugen: int64;
    procedure soundexneu;

    function word_to_pdf1(w_dat, pdf_dat:string):boolean;
    function pdf_to_txt(pdf_dat, txt_dat:string):boolean;

    function pdf_maker_pruefen:boolean;
    function dat_to_pdf(dat, pdf_dat:string):boolean;
    //function warte_bis_datei_lesbar(datei:string):boolean;

    procedure zeigedaten;
    procedure bereich_change(storno:boolean);
    procedure rechnungen_pruefen(modus:string);
    function weiter(s:string):boolean;
    procedure zeitkontoChange;
	end;

   //global g�ltige Funktionen ohnen tform.....
   function word_to_pdf(w_dat, pdf_dat:string):boolean;
    procedure olecontainer_close(ole:tolecontainerext);
   function olecontainer_anzeigen(ole:tolecontainerext):boolean;
   procedure olecontainer_to_pdf(container:tolecontainerext);
    procedure olecontainer_save_to_pdf(container:tolecontainerext;datname:string);
    procedure ole_to_text(tabelle, nummer:string);
    function SaveOleContainerPDF(OleContainer: TOleContainerext; Location: string): int64;
        function SaveOleContainer_word_PDF(OleContainer: TOleContainerext; Location: string): int64;
    function pdf_anzeigen_vers_datei(ole:TOleContainerext;modus:string):boolean;
   procedure oleinfo(container:tolecontainerext);
   function NetzlaufwerkVerbinden(Laufwerk: string; Pfad: string; Username: string; Password: string; RestoreAtLogon: Boolean): DWORD;
   function NetzlaufwerkTrennen(_locDrive: string; _showError: Boolean; _force: Boolean; _save: Boolean): DWORD;
   function FileInUse(FileName: string): Boolean;
    function warte_bis_datei_lesbar(datei:string):boolean;
    function warte_bis_datei_lesbar_handle(datei:string):boolean;
	  procedure grid_einstellen(sender:tobject);
	  procedure grid_suchen(sender: tobject);
	  function dateipost(table: tdataset): boolean;
	  procedure bildspeichern(tabelle:tdataset; picture:tpicture);
	  procedure bildladen(tabelle:tdataset; picture:tpicture);
	  procedure richspeichern(tabelle:tdataset;richedit:trichedit);
	  function kein_datensatz(table:tdataset):boolean;
    function kein_datensatz_dataset(table:tdataset;feld:string):boolean;
	  function datensatz(table:tdataset):boolean;
	  function dateiloeschen(table: tzquery; direkt,still: boolean):boolean;
	  procedure richloeschen(tabelle:tdataset);
	  function stringwenden(wort: string): string;
	  function wurzelpfad(pfad: string): string;
    function dateiname(pfad:string):string;
	  function neuer_datensatz(tabelle: tzquery;feldnamen, felder:array of variant ):int64;
 	 procedure reg_lesen; //nur pfade
	 procedure reg_schreiben;
	 function db_connect() : boolean;
	 function str_l_space(str:string; laenge:integer):string;
	 function str_r_space(str:string; laenge:integer):string;
	 function stringinvert(str: string): string;
	 function status_farbe(status:integer):integer;
	 function tabellen_name(sql:string):string;
	 function email_von_ma(zeilen:tstrings;nummer:int64):boolean;
	 function email_von_ma_abt(zeilen:tstrings;nummer:int64):boolean;
	 procedure email_von_ansprechpartner(zeilen:tstrings;i_firma:integer;funktion:integer);
   procedure string_to_liste(zeilen:tstrings;str, tz: string);
	 procedure falle(info:string);
	 function fmin(x,y:real):real;
	 function decimalseparator_change(sh:string):string;
	 function goae_nn_normieren(goae:string):string;
	 function n_tes_wort(n:integer;trenner,wort:string):string;
     function preis_von_goae(goae:string):t5stringarray;
     function betrag(r:real ):real;
     function centtobetrag(i:integer ):real;
     function myfloattocurr(r:real):real;
     procedure protokoll(art, funk, aktion: string; wf:boolean);
     function sonderzeichenraus(f:string):string;
     function sonderzeichenraus_all(f:string):string;
     function ersetzesonderzeichen(f:string):string;
     procedure pause(milliseconds:integer);
     procedure tc(tab:string);
     function untergeordnete_firmen(i_firma:integer):tlist;
     function firmenname(i_firma:integer):string;
     function tageimmonat(m,y:word): Integer;
     function quartalbeginn(m:word):integer;
     function halbjahrbeginn(m:word):integer;
     function ForceForegroundWindow(hwnd: THandle): Boolean;
     procedure arbene_vor;
     procedure bild_speichern(acontrol:TwinControl;datei:string);
     procedure xms(s: string; var gruppe, variable, wert:string);
     function GetFileSize(const szFile: String): Int64;
     function inttostr_lf(s1,s2:string): string;
     procedure liste_leeren(liste:ttreeview);
     function str_laenge (str: string; laenge: integer; hinten: string): string;
     function sql_dat:string;
     function GetDPI(var IntResult: Integer): Boolean;
     function ReadDword(RootKey: LongWord; KEY_NAME, KEY_VALUE: string; var IntResult: Integer): Boolean;
     function sperrdatum_OK(d:real;modus:integer):boolean;
     function FileSize(fn: TFileName): Int64;
     function ConnectNetworkDrive(const ADrive: string; const ADirectory, AUsername, APassword: string;
                    const ARestoreAtLogon: Boolean ): Boolean;
     function stringdatum(dat:tdate):string;
     procedure DeleteFiles(Path: string; Name: string);
     function GetEnvVarValue(const VarName: string): string;
     function GetSpecialFolderPath(Folder: Integer; CanCreate: Boolean): string;
    function nicht_satellit(hinweis:string):boolean;
     function ist_hauptsystem:boolean;
     function htmltag(f, text:string):string;
     function html_br(px:string):string;
     procedure ExecuteFile(const AFilename: String;
                 AParameter, ACurrentDir: String; AWait: Boolean;
                 AOnWaitProc: TExecuteWaitEvent=nil);
    procedure Explode(Input: String; Delim: Char;var v:tstringlist);
    procedure fehlerhafte_einstellungen;
    function IsRunning(ExeFileName: string): Boolean;
    function ProgIDExists(const ProgID:WideString):Boolean;
    function strtoint_safe(s:string):integer;
    procedure tab_exportieren(tabelle:tzquery;m:TIntSet );
    function zugeordneter_arzt:integer;
    function Run_app( wtp: string; befehl: string): Boolean;
    function RunAsAdmin(hWnd: HWND; filename: string; Parameters: string): Boolean;
    procedure e_status(text:string);
    function FileExists(const FileName: string): Boolean;
    procedure oleanzeigen(ole:tolecontainerext);
    function ma_name(i_ma:int64;c:integer):string;
   function StreamToString(aStream: TStream): string;
   procedure richeditladen(dataset:tdataset;feld:string;richedit:trichedit);
  procedure richeditspeichern(dataset:tdataset;feld:string;richedit:trichedit);
  function ShellExecuteErrMessage(int_Code: INTEGER): string;
  procedure ShellExecuteErrshow(int_code:integer);
  function shellexecute_se(hWnd:HWND; Operation: PAnsichar;FileName: PansiChar; Parameters: PAnsichar; Directory: PAnsichar; ShowCmd:integer):boolean ;
  function Hash32(Data: pointer; Len: integer): cardinal;
  function CompareStreams(const A, B : TStream): Boolean;
  function stringbin(dez,bas:integer):string;
  function stringbin_to_integer(s:string):integer;
  function copyfile_delphi(quelle, ziel:string):boolean;
  function  pdf_behandeln(pdf:string):string;
var
	Form_main: TForm_main;
    mHHelp: THookHelpSystem;
	betriebssystem,user_id,system_modus, gi_untersucher,akt_untersucher,max_mitarbeiter,pc_id,mws,mws_reduziert,scale_i1,scale_i2,scale_i3,fontsize_anmerkung,idb_protocol:integer;
	rtfedit: trtfedit;
	b:tberechtigungen;
	tkey, user_name,textverzeichnis,w_pfad,vorlagenverzeichnis, bildverzeichnis,
	firmen_berechtigung, firmen_filter,storno, and_storno,arbene_update:string;

	direktspeichern,bool, status_change_at_post,untersuchung_planen_at_post,email_send,email_abteilung_send,archiv_show,bestaet_neuz,backup_durchf,einzelplatz,druck_mit_zustimmung,druck_mit_zustimmung_eignung,druck_mit_zustimmung_besch,druck_mit_zustimmung_besch_eignung,
	storno_show,bestaet_neu,word_close,arbmed_besch_offen,rechnung_offen,abrechnen_voso,speichern_fragen,jahr_sperre,speicher_bescheinigungen,drucke_bescheinigungen,email_bescheinigungen, speicher_rechnungen,bescheinigung_in_kartei,rechnungen_in_kartei,
	abrechnen_befund,abrechnen_labor, abrechnen_impfung,show_ambulanz,show_impfung,show_labor,show_gefaehrdungen,mehrplatz,verteiltesysteme,redruck_anonym,redruck_goae,redruck_labor, redruck_impfstoff,
  wdv_besch, wdv_impfu, wdv_labor,wdv_ambul,wdv_dokfi, wdv_dokpr,wdv_beson, wdv_refi, wdv_proje  :boolean;

  f_ve_datum,f_ve_text,f_ve_art,f_ve_beurteilung,f_ve_px:string;

	not_sperren,ma_such_neu, mailslot_active,adobe_vorhanden:boolean;

	benutzer,meditdate:string;
	monats_feiertage: tstringintegerlist;
	Jahres_feiertage: tstringintegerlist;
	com: tcom;
	//excel:TAdvExcel;
  excel:variant;
	soundex: tsoundex;
	mysql_d,hmysql_d : tmysql_d;
	hk,db_host,db_server,db_passwort, db_login,db_protocol,  db_datenbank,db_port, db_mysqladmin, pfad_temp,pfad_ldt,pfad_infotexte,pfad_backupverzeichnis,pfad_vorlagen,pfad_bescheinigungen, pfad_rechnungen,pfad_pdf_converter,pfad_pdftk:string;
  pfad_mysql_daten,pfad_mysql_prg, pfad_abgleich_lokal, pfad_abgleich_haupt,abgl_iphaupt,abgl_dbhaupt:string;
	email_name,email_adresse,email_pop,email_userein,email_passwortein,email_smtp,email_useraus,makro_excel:string;
	db_typ, sql_felder_temp,sql_abfragen_temp, pfad_temp_mysql,last_backup,dbwahl,h1,h2,h3,einfache_abfragen_beginn,g_labornummer_nummer, g_labornummer_vorspann,lzr_akt_s,lzr1_akt_s,termine_startzeit,untergeordnete_firma_bez:string;
	lzr_akt,lzr1_akt,status_neu,connected,abgleich_beginn, abgleich_ende,abgleich_sat_anpassen,abgleich_neugewinnt,email_via_mapi,zeiterfassung_showauswahl:boolean;
	prg_typ,storno_zahl,ole_text, ole_tab,splitter_gefaehrdungen,splitter_ap,g_termin_filtern_tab:integer;
	g_punktwert:real;
	null_datum:tdate;
   null_datum_sql:string;
        cl_gesperrt,cl_entsperrt:tcolor;
   versionsinfo: OSVERSIONINFO;
   cent_mult:integer;
   arbene_geladen:boolean;
   berechtigung_druck_immer:boolean;
   be_druck_immer:integer;  //0 -->abfrage ob bescheinigung gedruckt wreden soll, dann 1 Druck 2 kein Druck

   m_befunde_c, m_vorsorge_c,  m_impfung_C, m_labor_C, m_ambulanz_c, m_diagnosen_c,m_besonderheiten_c, m_dokumente_C,m_artikel_c, m_rechnungen_c, m_kontakte_c,m_gdt_c :boolean ;
   jahresschluessel,vorlagenpfad:string;
    vorlagenfilterindex,i_stempel:integer;
   faktor_width, faktor_height:real;
   max_sperrdatum:real;
   modus_debug:boolean;

   ldt_nach,ldt_vor,ldt_was,hist_query:string;
   hist_daten:thist_set;
   modus_history,hist_sperren,speichert_gerade,laed_gerade:boolean;
   altes_datum:tdate;
   akt_abteilung, akt_arbeitsplatz:int64;
   last_ts:ttabsheet;
   rich_akt:trichedit;
   l_csv_arbene,l_csv,l_rechnung_format,l_rechnung_spalten,l_labor_werte,l_labor_hinweise:tstringlist;
   csv_pfad,csv_m,csv_w,csv_fl,csv_fh,csv_fg,csv_fw:string;
   csv_time,csv_datum,csv_ueberein:integer;
   csv_ueberschreiben, csv_archiv, csv_archiv_rest,pdf_via_dat,runadmin :boolean;

   //datum_dok_erstellt:string;
implementation

uses  a_kalender, a_tabellen, a_termine, a_besonderheiten,
	a_nachuntersuchung, a_icd, a_termin_wahl, a_unt_plan,
	a_auswertung, a_import, a_vorlagenwahl, a_vorlagen,
	a_namsuch, a_start,  a_verschieb, a_optionen,
	a_ma_unit, a_unt_wahl, a_smtp, a_listwahl, a_treewahl, a_schicht,
	a_abgleich,a_serienbrieffelder, a_vert_syst, a_u_wahl,
	a_f_wahl, a_email, a_opt_anwendung, a_line_edit,
		a_registrieren, a_ldt, a_rechnung, a_unterschungs_info,a_report,
  a_arbeitsplatz, a_gefaehrdungen,a_pdf_wahl, a_besch_druck, a_repo_neu, a_k_liste,
  a_backup, a_kapitel, a_dokutitel, a_arztbrief_drucken, a_textbausteine,
  a_pfadwahl, a_bdt, a_dbwahl, a_zeiterfallsung_all, a_checkbox,
  a_firmen_zusammen, a_uebernahme_jahr, a_memo, a_anzeige_kartei,
  a_spenden, a_repo_edit_m, a_suchliste, a_wartefenster, a_labornummern,
  a_rechnungsgestaltung, a_richedit_menue, a_csv_import, a_weiter,
  a_rechnung_automatisch, a_timer;

{$R *.DFM}



procedure fehlerhafte_einstellungen;

  procedure pfadpruefen (var pfad_var:string ;pfad_default:string);
  begin
  try
    if (lowercase(pfad_var)='c:\') or (pfad_var='') then pfad_var:=pfad_default;

    if (not directoryexists(pfad_var)) then
    begin
      if ForceDirectories(pfad_var) then showmessage('Das folgende Verzeichnis wurde erstellt: '+pfad_var) else showmessage('Das folgende Verzeichnis konnte nicht erstellt werden: '+pfad_var);
    end;
  except
      showmessage('Bitte �berpr�fen Sie, ob die Pfade die unter Optionen eingestellt sind existieren. ');
  end;
  end;
//################################################
begin
  //s:='pfad_temp,pfad_ldt,pfad_infotexte,pfad_backupverzeichnis,pfad_vorlagen,pfad_bescheinigungen, pfad_rechnungen,pfad_pdf_converter,pfad_mysql_daten,pfad_mysql_prg, pfad_abgleich_lokal, pfad_abgleich_haupt';


    pfadpruefen(pfad_temp,'c:\arbene_daten\temp\');
    pfadpruefen(pfad_ldt, 'c:\arbene_daten\ldt\');
    pfadpruefen(pfad_infotexte,'c:\arbene_daten\infotexte\');
    pfadpruefen( pfad_backupverzeichnis,'c:\arbene_daten\backup\');
    pfadpruefen(pfad_vorlagen,'c:\arbene_daten\import\') ;
    pfadpruefen(pfad_bescheinigungen, 'c:\arbene_daten\bescheinigungen\');
    pfadpruefen(pfad_rechnungen, 'c:\arbene_daten\rechnungen\');


    if not ProgIDExists('word.application')then showmessage('Microsoft Word ist nicht installiert. Das Programm ist notwendig zur Ausgabe von Dokumenten, bitte installieren.');

    //wordformatiereungen
    if f_ve_datum='' then f_ve_datum:='H5' ;
    if f_ve_text='' then f_ve_text:='H6';
    if f_ve_art='' then f_ve_art:='H5';
    if f_ve_beurteilung='' then f_ve_beurteilung:='H6';
    if f_ve_px='' then f_ve_px:='5';
end;


procedure TForm_main.fehler();
var i,z:real;
begin
i:=0;
z:=2/i;
showmessage(floattostr(z));
end;

procedure tform_main.db_komp_onchange(Sender: TObject);
begin

  mysql_d.ds_aenderung:=true;

end;



procedure TForm_main.HandleOnMessage(var Msg: TMsg; var Handled: Boolean);
var
s:string;
begin
   case Msg.message of
      WM_MOUSEWHEEL:
      begin
         
         if (Screen.ActiveForm.ActiveControl <> nil) and
         ( (Screen.ActiveForm.ActiveControl.ClassName = 'TDBgridEXT') or (Screen.ActiveForm.ActiveControl.ClassName = 'fTdbTreeView'))then
         begin
            // die Mousewheel Message wird in einen Tastendruck umgewandelt
            Msg.message := WM_KEYDOWN;
            Msg.lParam := 0;
            if Msg.wParam > 0 then
               Msg.wParam := VK_UP
            else
               Msg.wParam := VK_DOWN;

            // wir sind noch nicht fertig, schliesslich soll der Tastendruck ausgewertet werden
            Handled := False;
         end;
      end;
   end;
end;


procedure TForm_main.AppException(Sender: TObject; E: Exception);
begin
  application.ShowException(E);
  //showmessage('aaaaaaaaaaaaaaaaaaaa');
  if Sender is TComponent then
    MessageDlg(Format('Es ist folgender Fehler aufgetreten:%s%s%s'+
    'Fehlertyp:%s%s%sSender:%s%s [%s]', [#10#13, E.Message,
    #13#10#13#10, #10#13, E.ClassName, #10#13#10#13, #13#10,
    TComponent(Sender).Name, Sender.ClassName]), mtError, [mbOk], 0)
  else
    MessageDlg(Format('Es ist folgender Fehler aufgetreten:%s%s%s'+
    'Fehlertyp:%s%s%sSender:%s%s', [#10#13, E.Message, #13#10#13#10,
    #10#13, E.ClassName, #10#13#10#13, #13#10, Sender.ClassName]),
    mtError, [mbOk], 0);
end;


procedure Tform_main.Appactivate(Sender: TObject);
begin
  //17-05-25
  //if form_main.arbeitsmedizin_firma.state in [osopen] then form_main.arbeitsmedizin_Firma.UpdateObject;
  //if form_main.arbeitsmedizin_proband.state in [osopen] then form_main.arbeitsmedizin_proband.UpdateObject;
end;



function TForm_main.GetCurrentVersion: String;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do begin
    Result := 'Version '+IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);

    //Und in Delphi 7 hat man 4 stellige Versionen. Daher kann man ab Delphi 7 noch folgende letzte Result-Zeile hinzuf�gen:
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);

  end;
  FreeMem(VerInfo, VerInfoSize);
end;


function strtoint_safe(s:string):integer;
begin
  if s='' then s:='0';
  try
  result:=strtoint(s);
  except
    result:=0
  end;
end;

procedure TForm_main.BitBtn_beurt_ueberClick(Sender: TObject);
var
i_mitarbeiter:int64;
begin
//besonderheit aus memo �bernehmen
with datamodul do
begin
	if q_besonderheitenListe_tree['parent']=0 then exit;
	sql_post(q_untersuchung);
  i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');
	neuer_datensatz(q_besonderheiten,['nummer','i_mitarbeiter','i_besonderheit','datum']
	,[null,inttostr(i_mitarbeiter),q_besonderheitenListe_tree['nummer'],date()]);

	direktspeichern:=true;
	sql_post(q_besonderheiten);
	//BitBtn_beurt_ueber.Enabled:=false;
end;
end;



procedure TForm_main.SpeedButton_datClick(Sender: TObject);
var
i_stammdaten,i_nummer:int64;
begin
i_stammdaten:=getbigint(datamodul.q_mitarbeiter,'nummer');
i_nummer:=getbigint(datamodul.q_untersuchung,'nummer');
if form_kalender.showmodal =mrok then
  if sperrdatum_ok(form_kalender.auswahlkalender.date,1) then
  begin
     datamodul.q_untersuchung.edit;
     datamodul.q_untersuchung['datum'] :=int(form_kalender.auswahlkalender.date);
     baum_neu:=true;
	 end;
end;

procedure TForm_main.SpeedButton2Click(Sender: TObject);
var
i_stammdaten,i_nummer:int64;
begin
i_stammdaten:=getbigint(datamodul.q_mitarbeiter,'nummer');
i_nummer:=getbigint(datamodul.q_impfung,'nummer');
if form_kalender.showmodal =mrok then
  if sperrdatum_ok(form_kalender.auswahlkalender.date,1) then
	begin
	 datamodul.q_impfung.edit;
	 datamodul.q_impfung['datum'] :=int(form_kalender.auswahlkalender.date);
	 //datamodul.q_impfung.post;
	 //gehezuuntersuchung(2,i_stammdaten, i_nummer);
	 baum_neu:=true;
	 //dbtreeview.knoten_neuordnen(dbtreeview.selected);
	end;
end;

procedure TForm_main.SpeedButton3Click(Sender: TObject);
var
i_stammdaten,i_nummer:int64;
begin
i_stammdaten:=getbigint(datamodul.q_mitarbeiter,'nummer');
i_nummer:=getbigint(datamodul.q_labor,'nummer');
if form_kalender.showmodal =mrok then
 if sperrdatum_ok(form_kalender.auswahlkalender.date,1) then
	begin
    datamodul.q_labor.edit;
    datamodul.q_labor['datum'] :=int(form_kalender.auswahlkalender.date);
    baum_neu:=true;
	end;
end;

procedure TForm_main.OpenbuttonClick(Sender: TObject);
var
stream:tstream;
filestream: tfilestream;
begin
	opendialog.InitialDir:=vorlagenverzeichnis;//+'\+befund.doc'; pfad_anamnese;
	form_main.OpenDialog.filter:='rtf |*.rtf';
	if opendialog.execute and ( Messagedlg('Sollen der aktuelle Memotext �berschrieben werden?',mtConfirmation, mbOkCancel	,0)=mrOK ) then
	begin
  try
     filestream:=tfilestream.Create(form_main.opendialog.FileName,fmOpenRead	);

     datamodul.q_anamnese.edit;
     stream:=datamodul.q_anamnese.CreateBlobStream(datamodul.q_anamnese.FieldByName('memo'),bmReadWrite);
     stream.copyfrom(filestream,filestream.Size);
  finally
     stream.Free;
     filestream.free;
  end;
	end;
end;

procedure TForm_main.closebuttonClick(Sender: TObject);
begin
	rtfedit.FileSaveAs;
end;

procedure TForm_main.ToolButton8Click(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.FilePrint(self);
end;

procedure TForm_main.UndobuttonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.EditUndo(self);
end;

procedure TForm_main.CopybuttonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.Editcopy(self);
end;

procedure TForm_main.PastebuttonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.Editpaste(self);
end;

procedure TForm_main.FontNameChange(Sender: TObject);
begin
if assigned(rtfedit) then
try
//try //wird aufgerufen bevor rtfedit creiert.
if rtfedit.FUpdating then Exit;

rtfedit.CurrText.Name := fontname.Items[FontName.ItemIndex];
rtfedit.CurrText.Size := StrToInt(fontsize.Text);
rtfedit.CurrText.Color:=colordialog.Color;
except
end;
end;

procedure TForm_main.FontSizeChange(Sender: TObject);
begin
if assigned(rtfedit) and assigned(akt_editor) then begin
//wird aufgerufen bevor rtfedit creiert.
if rtfedit.FUpdating then Exit;

  try
    rtfedit.CurrText.Name := fontname.Items[FontName.ItemIndex];
		rtfedit.CurrText.Size := StrToInt(fontsize.Text);
		rtfedit.CurrText.Color:=colordialog.Color;
  except
	FontSize.Text:='10';
  end;
end;
end;

procedure TForm_main.BoldButtonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
if ttoolbutton(sender).Down then
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style + [fsBold]
	else
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsBold];

end;

procedure TForm_main.ItalicButtonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;

	if ttoolbutton(sender).Down then
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style + [fsItalic]
	else
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsItalic];
end;

procedure TForm_main.UnderlineButtonClick(Sender: TObject);
begin
 if rtfedit.FUpdating then Exit;

	if ttoolbutton(sender).Down then
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style + [fsUnderline]
	else
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsUnderline];
end;

procedure TForm_main.ToolButton_colorClick(Sender: TObject);
begin

if assigned(rtfedit) then begin
if rtfedit.FUpdating then Exit;

if not colordialog.Execute then exit;


rtfedit.CurrText.Name := FontName.Items[FontName.ItemIndex];
rtfedit.CurrText.Size := StrToInt(FontSize.Text);
rtfedit.CurrText.Color:=colordialog.Color;
//except
end;
end;

procedure TForm_main.LeftAlignClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;

	akt_editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
  //rtfedit.textspeichern Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure TForm_main.CenterAlignClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
	akt_editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
  //rtfedit.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure TForm_main.RightAlignClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
	akt_editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
  //rtfedit .Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;


procedure TForm_main.BulletsButtonClick(Sender: TObject);
begin
	if rtfedit.FUpdating then Exit;
	akt_editor.Paragraph.Numbering := TNumberingStyle(ttoolbutton(sender).Down);
 //rtfedit.Paragraph.Numbering := TNumberingStyle(ttoolbutton(sender).Down);
end;



function ReadDword(RootKey: LongWord; KEY_NAME, KEY_VALUE: string; var IntResult: Integer): Boolean;
var
  Reg: TRegistry;
begin

  Reg := TRegistry.Create;
  try
    Reg.RootKey := RootKey;
    if Reg.OpenKeyReadOnly(KEY_NAME) then
      if Reg.ValueExists(KEY_VALUE) then
      begin
        IntResult := Reg.ReadInteger(KEY_VALUE);
        Reg.CloseKey;
      end;
    Result := True;
  finally
    Reg.Free
  end;
end;

function GetDPI(var IntResult: Integer): Boolean;
var
  DC: hDC;
begin

  DC := GetDC(HWND_DESKTOP);
  try
    IntResult := GetDeviceCaps(DC, LOGPIXELSY);
    Result := True;
  finally
    ReleaseDC(DC, HWND_DESKTOP);
  end;

end;


procedure tform_main.iniloeschen;
var
dat:string;
begin
  if MessageDlg('Soll die Start-Datei zur�ckgesetzt werden?',mtConfirmation, mbYesNoCancel, 0) <> idYes then Exit;

  dbwahl:='1';
   dat:=GetSpecialFolderPath(28,false)+'\arbene';  //user
   dat:=dat+'\'+application.title+'.ini';

   if  fileexists(dat) then
   begin
      deletefile(dat);
      if fileexists(dat) then showmessage ('Die Datei konnte nicht gel�scht werden: '+dat);
   end;
   dat:=GetSpecialFolderPath(35,false)+'\arbene';   //common
   dat:=dat+'\'+application.title+'.ini';
   if fileexists(dat) then
   begin
     deletefile(dat);
     if fileexists(dat) then showmessage ('Die Datei konnte nicht gel�scht werden: '+dat);
   end;
   inischreiben(0);
   dat:=w_pfad+'\'+application.title+'.ini';
   showmessage('Die INI-Datei finden Sie hier: '+dat);
end;


procedure TForm_main.FormCreate(Sender: TObject);
var
s,s1,s2,query,dat,chmfile,db_string,s3:string;
versionsinfo: OSVERSIONINFO;
last_change_db,x:integer;
start,fehler:boolean;

procedure startstopp;
begin
 try
     application.ProcessMessages;
     mHHelp.Free;
     HHCloseAll;
     zipmaster.Free ;
     datamodul.sql_close;
     //if (mysql_d<>nil) then	mysql_d.Free;
     if com<>nil then	com.Free;
  except
  end;
form_main.tag:=1;
end;

//#####################
begin
try
    Application.OnMessage := self.HandleOnMessage;
    application.OnActivate:=APPactivate;
    //Application.OnException := AppException;

    //dbtreeview.change_allowed:=false;
    arbene_update:='';
    arbene_geladen:=false;
    start:=false;
    TwoDigitYearCenturyWindow :=80;
	 //Set8087CW(Default8087CW or 4);//zerodivide aus

      CurrencyFormat := 3;
      NegCurrFormat := 8;
     ThousandSeparator := '.';
     DecimalSeparator := ',';
     CurrencyDecimals := 2;
     DateSeparator := '.';
     ShortDateFormat := 'dd.MM.yyyy';
     LongDateFormat := 'dddd, d. MMMM yyyy';
     TimeSeparator := ':';
     TimeAMString := '';
     TimePMString := '';
     ShortTimeFormat := 'hh:mm';
     LongTimeFormat := 'hh:mm:ss';

	  prg_typ:=1;

    l_csv:=tstringlist.Create;
    l_csv_arbene:=tstringlist.create;
    l_rechnung_format:= tstringlist.create;
    l_rechnung_spalten:=tstringlist.Create;
    l_labor_werte:= tstringlist.create;
    l_labor_hinweise:= tstringlist.create;
//hilfe system

     mHHelp := nil;
  chmFile := ExtractFilePath(ParamStr(0))+'arbene.chm';
  if not FileExists(chmFile) then
    ShowMessage('Hilfe-Datei nicht gefunden'#13+chmFile);
      {HH 1.2 oder h�her Versionskontrolle}
  if (hh.HHCtrlHandle = 0)
    or (hh_funcs._hhMajVer < 4)
    or ((hh_funcs._hhMajVer = 4) and (hh_funcs._hhMinVer < 73)) then
      ShowMessage('Diese Anwendung erfordert die Installation der '+
      'MS HTML Help 1.2 oder h�her');

  {Hook - verwendet HH_FUNCS.pas}
  mHHelp := hh_funcs.THookHelpSystem.Create(chmfile, '', htHHAPI);


//ende Hilfesystem

   mysql_d:=tmysql_d.create(self,dbtreeview);
   hmysql_d:=tmysql_d.create(self,nil);

	 parameter_abfrage;  // Startparameter nm
   case prg_typ of
     1:application.title:='Arbene';
     2:application.title:='ZEUS';
     3:application.title:='Fuss';
     4:application.title:='Arztbrief';
   end;

   caption:=application.Title;
   ////////////////////////////////////////////////////////////////////////
   {case prg_typ of
	 1: version:='2.30 - 14.03.2012';
	 2: version:='1.1';
	 3: version:='1.0';
	 4: version:='1.0';
	 end;}

   version:= GetCurrentVersion;
     //change_db.txt
	 last_change_db:=760;
	 /////////////////////////////////////////////////////////////////////////////
	 mailslot_active:=true;
     cent_mult:=100;//00000;  aus Euro ->cent
     cl_gesperrt:=clwindow;//$00ccffff;//clyellow;  bgr
     cl_entsperrt:=$00ccffff;

   versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;
	 w_pfad:=wurzelpfad(application.ExeName);
	 textverzeichnis:=wurzelpfad(application.ExeName)+ '\text';
	 vorlagenverzeichnis:=wurzelpfad(application.ExeName)+ '\vorlagen';
	 bildverzeichnis:=wurzelpfad(application.ExeName)+'\bmp';
   pfad_backupverzeichnis:=GetSpecialFolderPath($5,false)+'\';
   //pfad_mysql_daten:='c:\mariadb55\data\';
   //pfad_mysql_prg:='c:\mariadb55\bin\';
   //idb_protocol:=1;
	 //infoverzeichnis:=wurzelpfad(application.ExeName)+'\infotexte\';

	 //toolbutton_abbrechen.Visible:=false; //vorerst nicht anzeigen da nicht funktion

	 null_datum:=2; //1.1.1900
    null_datum_sql:=sql_datetostr(null_datum);
	 if screen.Width<850 then
	 begin
		width:=screen.width;
		height:=screen.Height-50;
		groupbox_mitarbeiter.width:=225;
	 end
	 else
	 begin
		height:=570;
		width:=800;
	 end;
	 hk:=chr(39);
	 s1:= '13tdatenbank kann nicht geoeffnet werden bz7eqianahuaor9ia9dieser vorgang ist verbotensddh98239al93 bitte abbrechenassie sind hierzu nicht berechtigtdj1af';
	 s2:=copy(s1,21,20)+copy(s1,1,20)+copy(s1,41,20)+copy(s1,101,20)+copy(s1,61,20)+copy(s1,81,20)+copy(s1,121,8);
	 tkey:=s2;
	 //ciphermanager.InitKey(tkey,nil);


   //////7 skalierung

  faktor_Width := 1;
  faktor_Height := 1;
  X:=96  ;
  case ReadDword(HKEY_CURRENT_USER, 'Control Panel\Desktop\WindowMetrics', 'AppliedDPI', x) of
    True: { Registry wurde gelesen }
      begin
        faktor_width:=(x / 96);
        faktor_Height :=(x / 96);
      end;
    False: { Registry wurde nicht gelesen }
      case GetDPI(x) of
        True: { dpi wurden ermittelt }
          begin
            faktor_width:=(x / 96);
            faktor_Height :=(x / 96);
          end;
        False: { dpi wurden nicht ermittelt }
          Scaled := False;
      end;
  end;

  //SetBounds(Left, Top, Panel_Main_Holder.Width, Panel_Main_Holder.Height);
  //Panel_Main_Holder.Align := alClient;
  //Self.Borderstyle := bsNone;

   //////
   //reg_lesen; //pfade
	 inilesen; //im Pfad von arbene.exe


     if (dbwahl='1') or (not besteht_verbindung) then
     begin
        //#############dbwahl#############################################
        form_dbwahl:=tform_dbwahl.create(self);
     	  form_dbwahl.Edit_db.text:=db_datenbank;
        if db_passwort<>'nix'+'reinkommen' then form_dbwahl.edit_passwort.Text:=db_passwort else form_dbwahl.edit_passwort.Text:='';
        form_dbwahl.ComboBox_host.items.add(h1);
        form_dbwahl.ComboBox_host.items.add(h2);
        form_dbwahl.ComboBox_host.items.add(h3);
        form_dbwahl.ComboBox_host.Text:=h1;
        form_dbwahl.ComboBox_dbversion.ItemIndex:=idb_protocol;
        if form_dbwahl.ShowModal=mrok then
        begin
        	db_datenbank:=form_dbwahl.Edit_db.text;
        	db_host:=form_dbwahl.ComboBox_host.Text;
          form_dbwahl.edit_passwort.Text:=trim(form_dbwahl.edit_passwort.Text);
          if (length(form_dbwahl.edit_passwort.Text)>=1) and (length(form_dbwahl.edit_passwort.Text)<12) then showmessage('Das Passwort muss mindestens 12 Zeichen lang sein. Es wird das Standardpasswort verwendet');
          if length(form_dbwahl.edit_passwort.Text)>=12 then db_passwort:=form_dbwahl.edit_passwort.Text else db_passwort:='nix'+'reinkommen';
         idb_protocol:=form_dbwahl.ComboBox_dbversion.ItemIndex;
         if   form_dbwahl.CheckBox.Checked then dbwahl:='0';
         
        	if h1<>db_host then
        	begin
        		h3:=h2;
        		h2:=h1;
            h1:=db_host;
       	    end;
            form_dbwahl.Release;
        end
        else
        begin  //nicht sch�n aber selten
          form_dbwahl.Release;
          startstopp;
          exit;
        end;


     end;
    //db einstellen
    case  idb_protocol of
       0:db_protocol:='mysql';
       1:db_protocol:='MariaDB-5';//
    end;
    datamodul.connection_main.protocol:=db_protocol;
    datamodul.Connection_grant.Protocol:=db_protocol;
    datamodul.Connection_hs.Protocol:=db_protocol;

	 form_start:=tform_start.create(self);
	 form_start.notebook.visible:=false;
	 form_start.Notebook.pageindex:=0;
	 form_start.Show;
	 screen.Cursor:=crhourglass;
	 application.ProcessMessages;

	 Panel_ambulanzResize(Sender);

	 //abfragen ob alle dateien da
	{ dat:=wurzelpfad(application.ExeName)+'\libmysql.dll';
	 if not fileexists(dat) then
	 begin
			 showmessage( 'libmysql.dll ist nicht vorhanden, Anwendung wird beendet');
             startstopp;
			 //form_main.tag:=1;
			 //application.Terminate;
			 exit;
	 end; }

   dat:=wurzelpfad(application.ExeName)+'\libmariadb.dll';
	 if not fileexists(dat) then
	 begin
			 showmessage( 'libmariadb.dll ist nicht vorhanden, Anwendung wird beendet');
             startstopp;
			 {form_main.tag:=1;
			 application.Terminate;}
			 exit;
	 end;

  dat:=wurzelpfad(application.ExeName)+'\libmysql323.dll';
	 if  fileexists(dat) then
	 begin
			 showmessage( dat+ ' bitte l�schen');
	 end;

	 if last_nr_change_db<>last_change_db then
	 begin
	 showmessage( 'Die Datei change_db.txt geh�rt nicht zu der aktuellen Programmversion, Programm wird beendet');
     		startstopp;
			 {form_main.tag:=1;
			 application.Terminate;}
			 exit;
	 end;


	 if not besteht_verbindung then
	 begin

    if not (ServiceGetStatus(nil, 'Mysql')= SERVICE_RUNNING) then
    begin
      showmessage('Der lokale Dienst "Mysql" ist nicht gestartet.');
    end
    else

		//showmessage('Es besteht keine Verbindung zur Datenbank '+chr(13)+'falsche IP-Adresse, falsches Datenbankpasswort'+chr(13)+'oder Daten sind nicht vorhanden.'+chr(13)+chr(13)+'Die Anwendung wird beendet.');

    if  Messagedlg('Es besteht keine Verbindung zur Datenbank '+chr(13)+'falsche IP-Adresse, falsches Datenbankpasswort'+chr(13)+'oder Daten sind nicht vorhanden.'+chr(13)+#13+'Soll erweiterte Diagnostik gestartet werden?',mtConfirmation, [mbOK,mbcancel],0)=mrok then form_main.grant;
    //db_wahl:=1;
    //form_main.inischreiben(35);
    iniloeschen;
    //form_main.grant;
    startstopp;
		exit;
	 end;



	if not ist_db_da then
	 begin
     messagedlg( 'Die Datenbank '+db_datenbank+' ist nicht vorhanden'+chr(13)+'Bitte Datensicherung zur�ckspielen.',mtInformation ,[mbOK	],12000	);
     //db_wahl:=1;
    //form_main.inischreiben(35);
    iniloeschen;
     	startstopp;
		{form_main.tag:=1;
		application.Terminate; }
		exit;
	 end;

 if not db_connect() then
	 begin
		//optionen_show(1);
        // host und DB abfragen
        form_line_edit:=tform_line_edit.create(self);
     	form_line_edit.CheckBox.Visible:=false;
     	{form_line_edit.Caption:='Datenbank';
        form_line_edit.Edit.text:=db_datenbank;
        if form_line_edit.ShowModal=mrok then db_datenbank:=form_line_edit.Edit.text; }

        form_line_edit.CheckBox.Visible:=false;
        form_line_edit.Caption:='Host = Ip-Adresse vom Datenbankrechner';
        form_line_edit.Edit.text:=db_host;
        if form_line_edit.ShowModal=mrok then db_host:=form_line_edit.Edit.text;
        form_line_edit.Release;

		if not db_connect then
		begin
				showmessage('Datenbank kann nicht ge�ffnet werden.(Mysql ist nicht gestartet oder falsche Host-IP-Adresse)'+#10+'Standardeinstellung'+#10+'  Host: localhost'+#10+'  Datenbank: arbene');
        //db_wahl:=1;
        //form_main.inischreiben(35);
        iniloeschen;
				form_start.Release;
				form_main.tag:=1;
				//application.Terminate;
				exit;
		end;
	 end;

	 mysql_d.connect(db_host,db_protocol,db_port,db_datenbank,db_login,db_passwort);

   if not mysql_d.sql_exe('select nummer from mitarbeiter where nummer=0') then
   begin
        showmessage('Tabelle Mitarbeiter kann nicht gelesen werden, ggf. arbene.ini im Programmpfad anpassen.');
        iniloeschen;
        dbwahl:='1';
        inischreiben(0);
        startstopp;
        {form_main.tag:=1;
		application.Terminate; }
		exit;
   end;

	 //parameter_abfrage;  // Startparameter nm

   reg_lesen;

   neue_felder_abfragen_once ;  //individuelle �nderungen

	 if not neue_felder_abfragen then
     begin
     	startstopp;
  		exit;
     end ;

   //neue_felder_abfragen_once ;


	 datamodul.sql_open;
	 com:=tcom.Create;

	 //session.addpassword('regenspatz');
   form_start.Hide;

	 form_start.notebook.visible:=true;
	 form_start.statusbar.visible:=false;

   kleine_aenderungen;

	 //#########################startdialog zeigen ##################################################
   
	 if form_start.ShowModal<>mrok then
	 begin
        //ende gel�nde
		form_start.Release;
    startstopp;
		//form_main.tag:=1;
		exit;
	 end;
	 Benutzer:=form_start.edit1.text;
	 form_start.notebook.visible:=false;
	 form_start.statusbar.visible:=true;
	 form_start.info('Programm wird geladen.');
	 mysql_d.sql_exe(format('delete from sperren where user_id= %d and sperrobjekt<>"user"',[user_id]));
	 form_start.Show;

   db_server:=mysql_d.Feldinhalt('select version()',0);
   if pos('MariaDB',db_server)>0 then idb_protocol:=1 else idb_protocol:=0;
   case  idb_protocol of
       0:db_protocol:='mysql';
       1:db_protocol:='mysql-5';
    end;

	 screen.Cursor:=crhourglass;


try

   pagecontrol_labor.TabIndex:=0;
	 radiogroup_auffaellig.tag:=0;
	 richedit_befund.tag:=0;
	 scrollbox_ole_befund.tag:=0;
   //reg_lesen;
	 einstellungen_lesen;
   einstellungen_lesen_global;
	 form_main.form_positionieren(tform(sender));
	 meditdate:=datetostr(date);
	 storno_zahl:=1;
	 longtimeformat:='hh:mm';
	 ShortTimeFormat:='hh:mm';
	 direktspeichern:=false;
	 rtfedit:=trtfedit.create(akt_editor,toolbar_anamnese);

   form_richedit_menue:=tform_richedit_menue.Create(self);

	 b:=tberechtigungen.create(user_id);

	 application.ProcessMessages;
	  monats_feiertage:=tstringintegerlist.create;
	 jahres_feiertage:=tstringintegerlist.create;

	 application.ProcessMessages;
	 sperren(pagecontrol_main);
	 grid_suchen(sender);
     lizenztest;
	 //if not lizenztest then application.Terminate;   //zu viele Nutzer von zeus
     max_mitarbeiter:=0;
    //   firmen
    form_start.info('Firmen');

	 form_start.info('Berechtigungen');
	 berechtigung_firma;
	 berechtigungen_einstellen;
	 sichtbarkeiten_einstellen;
	 prg_typ_einstellen;
	 //sichtbarkeiten_einstellen; nach oben sonst impfungen immer sichtbar prg=4
	 vert_syst_menue;
	 form_start.info('tempor�re Tabellen');
	 temp_tabellen;

	 application.ProcessMessages;
	 pagecontrol_main.ActivePage:=tabsheet_firma;
	 pagecontrol_firma.ActivePage:=tabsheet_firmenadresse;
	 pagecontrol_stammdaten.activepage:=TabSheet_ma_person;


	 dbradiogroup_beurteilung_eignung.Values.clear;
	 dbradiogroup_beurteilung_eignung.items.clear;
   dbradiogroup_beurteilung_arbmedvv.Values.clear;
	 dbradiogroup_beurteilung_arbmedvv.items.clear;
	 datamodul.sql(false, datamodul.q_1,'select * from beurteilung','');
	 datamodul.q_1.first;
	 while not datamodul.q_1.Eof do
	 begin
		dbradiogroup_beurteilung_eignung.Values.add(datamodul.q_1['nummer']);
		dbradiogroup_beurteilung_eignung.items.add(datamodul.q_1['text']);
    if datamodul.q_1.FindField('nummer').AsInteger>=6 then
    begin
      dbradiogroup_beurteilung_arbmedvv.Values.add(datamodul.q_1['nummer']);
      dbradiogroup_beurteilung_arbmedvv.items.add(datamodul.q_1['text']);
    end;
    
		datamodul.q_1.Next;
	end;



	application.ProcessMessages;
	form_start.info('Regeln');
	query:='select * from rules';
	datamodul.sql_new(false,datamodul.q_1,query,'');
	soundex:=tsoundex.create(1,datamodul.q_1);
	application.ProcessMessages;

	user_name:=datamodul.q_schluessel['name'];
	StatusBar.Panels[1].Text:='User: '+user_name;
    StatusBar.Panels[2].Text:='Datenbank: '+db_datenbank;
    StatusBar.Panels[3].Text:='Host: '+db_host;

   StatusBar.Panels[4].Text:='Server: '+db_server;
    {case idb_protocol of
    	0: StatusBar.Panels[4].Text:='Server: MySQL 3.23';
   	1: StatusBar.Panels[4].Text:='Server: MariaDB 5.5';
    end;}
    //  StatusBar.Panels[4].Text:='Server: '+datamodul.connection_main.Protocol;
    StatusBar.Panels[5].Text:='Arbene-'+version;
	if datamodul.q_schluessel['i_untersucher']<>null then gi_untersucher:=datamodul.q_schluessel['i_untersucher']
		else gi_untersucher:=0;
	akt_untersucher:=gi_untersucher; //kann sich �ndern
	formular_sperren;
	PageControl_mainChange(pagecontrol_main);
	application.ProcessMessages;

	//alle_kapitel_lesen;

	datamodul.sql(false,datamodul.q_1,'select * from haupt_tabelle','');
	pfad_temp_mysql:=datamodul.q_1['pfad_temp'];
	g_punktwert:=datamodul.q_1['punktwert'];
	mws:=datamodul.q_1['mws'];
  mws_reduziert:=datamodul.q_1['i2'];
  if mws_reduziert=0 then mws_reduziert:=7;
	form_start.info('lade Auswahlfelder');
	dbcombo_laden;
  form_start.info('Combo geladen');
	datamodul.q_firma.First;
    einsatzZeiteneinstellen;
    //baum_sichtbarkeiten(true);

    query:=format('select untersucher from untersucher where nummer=%d',[akt_untersucher]);
    s3:=mysql_d.feldinhalt(query,0);
   if s3<>'' then combobox_ag_user.Text:=s3;
    application.ProcessMessages;
    form_start.info('aktualisiere Wiedervorlage');
    aktualisiere_wiedervorlage;
   //SpeedButton_ag_refreshClick(self);  //lang
	application.ProcessMessages;
	//form_start.info('Programm ladet 1');
    protokoll('user','start','',true);

	Splitter_agendaMoved(self);
    //treeview_ext_firma.suche_datensatz(#13);
   form_start.info('�berp�fe die Tabellen');
   check_tables(form_start.combobox_check.itemindex);

    query:='select max(sperrdatum) from abschluss where storno=0';
    s:= mysql_d.feldinhalt(query,0);
    if s='' then max_sperrdatum:=0 else max_sperrdatum:=strtodate(s);
    pdf_maker_pruefen;

    //History
    new(hist_daten);
    hist_daten.zquery:=nil;
    hist_daten.groupbox:=nil;
    hist_daten.grid:=nil;
    hist_daten.datasource:=nil;
    GroupBox_tree_hist.Height:=0;
    modus_history:=false;
    form_start.info('�berpr�fe die Rechnungen');
    rechnungen_pruefen('test');
    //if ist_hauptsystem then  //eigentlich immer
    begin
      form_start.info('�berpr�fe den Nummernkreis');
      if nummern_kreis_fehler then
      begin
        showmessage('Der Numernkreis wird aktualisiert ');
        aktualisiere_nummern_kreis_aktuell;//showmessage('Bitte unter Optionen->Problembehandlung->Nummernkreis neu einlesen.');
      end;
   end;
   if csv_time<>0 then
   begin
    timer_csv.Interval:=1000*3600*24 div csv_time ;
    timer_csv.Enabled:=true;
    Timer_csvTimer(self);
   end;


finally
   //form_ap:=tform_ap.create(self);
	 screen.Cursor:=crdefault;
   form_main.BringToFront;
   application.ProcessMessages;
   arbene_geladen:=true;
end;


finally

  if (abgleich_beginn and directoryexists(pfad_abgleich_lokal) and (system_modus=0)) then
  begin
    if (Messagedlg('Soll ein Abgleich mit dem Hauptsystem erfolgen?',mtConfirmation, mbOkCancel	,0)=mrOK ) then
    m_dbabgleichClick(self);
  end;

  fehlerhafte_einstellungen;

  events_zuordnen;
end;
end;

procedure tform_main.events_zuordnen;
begin

  DBEdit_bes_erledigt_dat.OnChange:=self.db_edit_change;
  //DBEdit_bes_erledigt_dat.OnChange:=self.DBEdit_bes_erledigt_datChange;   //event muss neu zugeordnet werden warum ????????????????????
end;


procedure tform_main.rechnungen_pruefen(modus:string);
var
query,s_rechnung,s_firma,s_renu,s_datum,s_count:string;
begin

if not ist_hauptsystem then exit;    // sonst probleme wenn keine berechtigung f�r Rechnung

//suche rechnungspositionen ohne rechnung
query:='select firma.firma as firma, re_positionen.renu as renu, re_positionen.i_rechnung as i_rechnung, re_positionen.datum as datum from re_positionen left join rechnung on';
query:=query+ ' (re_positionen.i_rechnung=rechnung.nummer) left join firma on (re_positionen.i_firma=firma.nummer) where (rechnung.nummer is null) and (re_positionen.storno=0) group by i_rechnung';
datamodul.sql_new( false,datamodul.q_b1, query,'');
datamodul.q_b1.first;

if modus='test' then
begin
  if not datamodul.q_b1.Eof then showmessage('Bitte ausf�hren: Optionen->Problembehandlung->verlorene Rechnungspositionen wiederherstellen ');
  exit;
end;



try
  form_memo:=tform_memo.create(self);
  form_memo.Caption:='Korrektur fehlerhafter Rechnungen';
  form_memo.Memo.Lines.Add('Folgende Rechnungspositionen wurden freigegeben');
  form_memo.Memo.Lines.Add('Firma      |  Datum |  Rechnungsnummer | Anzahl der Rechnungspositionen');
  while not datamodul.q_b1.Eof do
  begin
   s_rechnung:=datamodul.q_b1.findfield('i_rechnung').AsString;
   s_firma:=datamodul.q_b1.findfield('firma').AsString;
   s_datum:= datamodul.q_b1.findfield('datum').AsString;
   s_renu:=  datamodul.q_b1.findfield('renu').AsString;
   query:=format('select count(*) from re_positionen where i_rechnung=%s and storno=0',[s_rechnung]);
   s_count:=mysql_d.Feldinhalt(query,0);

  if MessageDlg(Format('Sollen %s  Rechnungspositionen ohne zugeh�rige Rechnung vom %s gel�scht werden?'+#13+'(zum Abschluss erhalten Sie eine Zusammenfassung)', [s_count, s_datum]),
			mtConfirmation, mbYesNoCancel, 0) = idYes then
      begin
        query:=format('update re_positionen set storno=1 where i_rechnung=%s and storno=0',[s_rechnung]);
        mysql_d.sql_exe(query);
        form_memo.Memo.Lines.Add(s_firma+ ' | '+s_datum+' | '+s_renu +'  |  Anzahl Positionen: '+s_count);
       end;
   datamodul.q_b1.next;
  end;
  form_memo.Memo.Lines.Add(' ');
  if form_memo.Memo.Lines.Count>1 then
  begin
  form_memo.Memo.Lines.Add('Bitte �berpr�fen Sie, ob sich f�r die obigen Zeiten Rechnungspositionen neu abrechnen lassen,');
  end
  else
  form_memo.Memo.Lines.Add(' Es wurden keine verlorenen Rechnungspositionen gefunden  ');
  //form_memo.Memo.Lines.Add('s. Men� rechte Maustaste ');

finally
  form_memo.ShowModal;

  form_memo.Release;
end;
end;


procedure tform_main.wartefenster_zeigen;
var
query,s,js:string;
i,r,z:integer;
d,y,m:word;
bspenden:boolean;
begin
cipher.InitStr(tkey,Tdcp_sha1);
//jahresschl�ssel, lizenz
try
  query:='select passwort from schluessel where name ="_&_jahr" and storno=1';
  js:=mysql_d.Feldinhalt(query,0);
  if js<>'' then
  begin
    s:=cipher.DecryptString(js);
    cipher.Burn;
    if strtoint(copy(s,2,4))>=strtoint(copy(datetostr(now),7,4)) then
    begin
      statusbar.Panels[6].Text:=format('Freigeschaltet bis %s',[copy(s,2,4)]);
      exit;
    end;
  end;

  if jahresschluessel<>'' then
  begin
    cipher.InitStr(tkey,Tdcp_sha1);
    s:=cipher.DecryptString(jahresschluessel);
    cipher.Burn;
    if strtoint(copy(s,2,4))>=strtoint(copy(datetostr(now),7,4)) then
    begin
      query:='select count(*) from schluessel where name ="_&_jahr" and storno=1';
      js:=mysql_d.Feldinhalt(query,0);
      if js='0' then
      begin
        query:='insert schluessel set name="_&_jahr", storno="1"';
        mysql_d.sql_exe(query);
      end;

      query:=format('update schluessel set passwort="%s" where name="_&_jahr" and storno=1',[jahresschluessel]);
      mysql_d.sql_exe(query);
      begin
        statusbar.Panels[6].Text:=format('Freigeschaltet bis %s',[copy(s,2,4)]);
        exit;
      end;
    end;
  end;  
except
end;

decodedate (now(),y,m,d);
Randomize;
query:=format('select count(*) from protokoll_user where funk ="start" and ts>"%d-01-01" ',[y]);
s:=mysql_d.Feldinhalt(query,0);
r:=strtoint(s);
if r<30 then exit;
Randomize;
if random (r)<15 then exit;
form_wartefenster:=tform_wartefenster.create(self);
z:=random(9)+1;
form_wartefenster.Timer1.Interval:=r*2*z;
form_wartefenster.Timer1.Enabled:=true;
form_wartefenster.Memo1.Lines.Add(format('Sie haben Arbene im Jahr %d  %d mal gestartet ',[y,r]))  ;
form_wartefenster.ShowModal;

bspenden:=form_wartefenster.spenden;
form_wartefenster.release;

if bspenden then spenden;

end;

function tform_main.pcid(mc:string):integer;
var
	query:string;
begin
try
	if mc='schreib' then
	begin
     query:=format('delete from pcs where pc=%d',  [pc_id]);
	  mysql_d.sql_exe(query);

	  query:=format('insert pcs set pc=%d',  [pc_id]);
	  mysql_d.sql_exe(query);
	end;

	if mc='loeschen' then
	begin
	  query:=format('delete from pcs where pc=%d',  [pc_id]);
	  mysql_d.sql_exe(query);
	end;
except
end;
try
	query:='select count(*) from pcs';
	result:=strtoint(mysql_d.Feldinhalt(query,0));
except
end;
end;

procedure tform_main.temp_tabellen;
var
query:string;
begin
	 sql_felder_temp:=format('sql_felder_%d_temp',[user_id]);
	 sql_abfragen_temp:=format('sql_abfragen_%d_temp',[user_id]);

   mysql_d.db_von_db('sql_felder',sql_felder_temp,false);
   //l�schen was zu viel
   if not show_ambulanz then
	 begin
		query:=format('delete from %s where feldname like "%s"',  [sql_felder_temp,'Ambulanz%']);
		mysql_d.sql_exe(query);
     query:=format('delete from %s where feldname like "%s"',  [sql_felder_temp,'Verletzung%']);
     mysql_d.sql_exe(query);
  end;
   if not show_labor then
   begin
     query:=format('delete from %s where feldname like "%s"',  [sql_felder_temp,'Labor%']);
     mysql_d.sql_exe(query);
   end;

   if not show_impfung then
   begin
     query:=format('delete from %s where feldname like "%s"',  [sql_felder_temp,'Impfung%']);
     mysql_d.sql_exe(query);
   end;

   if not m_artikel_c then
   begin
     query:=format('delete from %s where feldname like "%s"',  [sql_felder_temp,'Artikel%']);
     mysql_d.sql_exe(query);
   end;

   mysql_d.db_von_db('sql_abfragen',sql_abfragen_temp,false);




	 application.ProcessMessages;

	 query:=format(' select * from %s',[sql_abfragen_temp]);
	 datamodul.sql(false,datamodul.q_sql_abfragen,query,'name');
	 application.ProcessMessages;
end;



function tform_main.last_nr_change_db:integer;
var
	f:textfile;
	s,dat,sql_nummer:string;
  p:integer;
begin
  result:=0;
  dat:=wurzelpfad(application.ExeName)+'\change_db.txt';
	if not fileexists(dat) then exit;
try
	AssignFile(F,dat );   { Datei ausgew�hlt }
	Reset(F);
	while not eof(f) do
	begin
		Readln(F, S);
		if copy(s,1,2)='//' then continue; //kommentar
		p:=pos('|',s);
		if p=0 then continue; //leer
		sql_nummer:=copy(s,1,p-1);
  end;
  result:=strtoint(sql_nummer);
finally
   CloseFile(F);
end;
end;

procedure tform_main.dateitest(dat:string);
begin
  if not fileexists(dat) then
  begin
       showmessage( dat +' nicht vorhanden Anwendung wird beendet');
       application.Terminate;
       application.ProcessMessages;
  end;
end;

procedure tform_main.dbcombo_laden;
begin
	dbcombo_num_branche.lookup_laden;
	dbcombo_num_f_gruppe.lookup_laden;
	dbcombo_num_bg.lookup_laden;
	dbcombo_num_gewerbeaufsichtsamt.lookup_laden;
	dbcombo_num_zeitabr.lookup_laden;
   dbcombo_num_zeit_ust.lookup_laden;
	dbcombo_num_famstand.lookup_laden;
	dbcombo_num_geschlecht.lookup_laden;
	dbcombo_num_staat.lookup_laden;
	dbcombo_num_vor_art.lookup_laden;
  DBCombo_num_unt_art2.lookup_laden;
	dbcombo_num_vor_untersucher.lookup_laden;
  DBCombo_num_besonderheiten_untersucher.lookup_laden;
	dbcombo_num_vor_status.lookup_laden;
	dbcombo_num_impf_unters.lookup_laden;
	dbcombo_num_lab_unters.lookup_laden;
	dbcombo_num_lab_status.lookup_laden;
	dbcombo_num_impf_status.lookup_laden;
	dbcombo_num_weiter.lookup_laden;
	dbcombo_num_amb_unters.lookup_laden;
	dbcombo_num_impfabfolge.lookup_laden;
	dbcombo_num_schicht.lookup_laden;
  DBCombo_num_abrechnungskriterium.lookup_laden;
  DBCombo_num_zeitkonto.lookup_laden;
  DBCombo_num_firma_zeiten_soll_konto.lookup_laden;

  DBCombo_num_firma_zeiten_soll_kriterium.lookup_laden;
  DBCombo_num_rechnung_regeln_ust.lookup_laden;
  DBCombo_num_rechnung_regeln_monat.lookup_laden;

  DBCombo_num_firma_land.lookup_laden;
  DBCombo_num_proband_land.lookup_laden;
  DBCombo_num_besonderheit_user.lookup_laden;//
  DBCombo_num_texte_user.lookup_laden;//
  DBCombo_num_refi.lookup_laden;
  DBCombo_num_repro.lookup_laden;
  DBCombo_num_wdv_dokumente.lookup_laden;
  dbcombo_num_firma_arzt.lookup_laden;
  DBCombo_num_prob_arzt.lookup_laden;

  

	alle_kapitel_lesen;
   combolesen(ComboBox_zeit_kriterium,'abr_anlass_krit','name','reihenfolge','alle');
	combolesen(ComboBox_zeit_konto,'abr_zeitkonto','name','reihenfolge','alle');
   combolesen_s(tcombobox(dbcombobox_anrede),'anrede','name','name','');
	//if form_berechtigung<>nil then form_berechtigung.DBCombo_num_untersucher.lookup_laden;
   combolesen(ComboBox_ag_user, 'untersucher','untersucher','untersucher','alle');    //
	if form_termine<>nil then form_termine.DBCombo_num_art.lookup_laden;
  zeitansatz_lesen;
end;


function TForm_main.dateiendung(datei: string) : string;
var
position: integer;
begin
	result:='';
	datei:=stringinvert(datei);
	position:=pos('.',datei);
	result:=copy(datei,1,position-1);
	result:=stringinvert(result);
	result:=lowercase(result);
end;


procedure TForm_main.BitBtn_unters_planenClick(Sender: TObject);
begin
datamodul.q_untersuchung.Tag:=1;
dbspeichern(false);
unt_planen(datamodul.q_untersuchung);
application.ProcessMessages;
datamodul.q_untersuchung.Tag:=0;
end;

procedure tform_main.unt_planen(dataset:tdataset);
var
datum,datum_heute,datum_2: tdatetime;
i,i_typ,uil,uil_aktuell,paw,paw_org,i_nachuntersuchung, untersuchungstyp,untersuchungszahl:integer;
i_mitarbeiter:int64;
knoten:ttreenode;
daten:pnodepointer;
unt,suil,query,u_tabelle:string;
cs:boolean;
felder :array [0..1] of string;
i_proband:int64;
begin

//if form_unt_plan<>nil then exit;

if dataset['i_typ']=null then
	begin
		showmessage('Bitte zuerst den "Untersuchungstyp" bestimmen.');
		exit;
	end;


  datum_heute:=dataset['datum'];
  i_typ:=dataset['i_typ'];
  i_proband:= strtoint64(dataset.findfield('i_mitarbeiter').asstring);

if dataset=datamodul.q_untersuchung then paw_org:=dataset.findfield('i_paw').AsInteger; // nur bei den untersuchungen

with datamodul do
begin

	 q_typ.first;
	 q_typ.locate('nummer',i_typ,[]);

	sql_relation( 'nachuntersuchung','i_typ',q_typ,q_nachuntersuchung,'');
	q_nachuntersuchung.first;

  i:=0;

  form_unt_plan:=tform_unt_plan.create(self);
  form_unt_plan.datum_heute:=datum_heute;
  form_unt_plan.i_m_typ:=i_typ;
  form_unt_plan.i_m_proband:=i_proband;

  query:=format('select bereich from typ where nummer=%s',[inttostr(i_typ)]);
  untersuchungstyp:=strtoint(  mysql_d.Feldinhalt(query,0));

  if  untersuchungstyp =1 then
  begin
    query:=format('select count(*) from untersuchung where i_mitarbeiter=%s and i_typ=%s and i_status=4',[inttostr(i_proband),inttostr(i_typ)]);
    untersuchungszahl:= strtoint(  mysql_d.Feldinhalt(query,0));
    if untersuchungszahl>1 then form_unt_plan.RadioGroup_abfolge.ItemIndex:=1;
  end
  else
  begin
    form_unt_plan.RadioGroup_abfolge.ItemIndex:=1;
    form_unt_plan.RadioGroup_abfolge.visible:=false;
  end;
  form_unt_plan.nachuntersuchung_einlesen;
 




  if form_unt_plan.showmodal<>mrok then exit;
  knoten:=dbtreeview.selected;
  daten:=knoten.Data;
  i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter,'nummer');

  for i:=0 to form_unt_plan.StringGrid.Rowcount-1 do   //auslesen
  begin
     uil:=form_unt_plan.uil[i];
     if uil=0 then uil:=untersuchungstyp;//wenn keine vorsorge eingetragen war
     paw:=form_unt_plan.paw[i];
     if (paw=0) and (i_nachuntersuchung=i_typ) then paw:=paw_org;  //nur wenn gleicher i_typ    und nicts voreingetragen
     i_nachuntersuchung:=form_unt_plan.i_nachuntersuchung[i];
     datum:=strtodate(form_unt_plan.StringGrid.cells[1,i]);
     unt_eintragen(uil,i_nachuntersuchung,paw,i_mitarbeiter,datum);
  end;

application.processmessages;
dbtreeview.gehe_zu_datensatz(daten^.rubrik, daten^.nummer);
dbtreeview.tag:=0;
formular_sperren;
form_unt_plan.release;

end;
end;



procedure tform_main.unt_eintragen(tabelle,i_typ,i_paw:integer;i_mitarbeiter:int64; datum:tdatetime);
begin
with datamodul do
begin
case tabelle of
1: begin
    unt_neu( datum,i_typ,i_paw,1,i_mitarbeiter,0,true);

	end;
2: begin
     impf_neu(datum,i_typ,1, i_mitarbeiter, 0,true);

	end;
3: begin
		lab_neu(datum,i_typ,1,i_mitarbeiter,0,true,false);

	end;

end;
end;
end;




function tform_main.ambulanz_neu(datum:tdate;modus:integer):int64;
var i_mitarbeiter,nummer:int64;
begin
		i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');
		nummer:=neuer_datensatz(datamodul.q_ambulanz,['nummer','i_mitarbeiter','datum','urzeit','i_untersucher','rhythmus','b_arbeitsunfall']
		,[null,inttostr(i_mitarbeiter),datum, time,akt_untersucher,'',modus]);
		direktspeichern:=true;
		datamodul.sql_post(datamodul.q_ambulanz);
	  form_main.dbTreeView.selected:=dbtreeview.einhaengen(datamodul.q_ambulanz,nummer,5,-1);
	  if modus=1 then dbcheckbox_aunfall.ReadOnly:=true;
	  result:=nummer;
end;


function tform_main.lab_neu(datum:tdate;i_typ,i_status:integer;i_mitarbeiter:int64;i_untersucher:integer;speichern,still:boolean):int64;
var nummer:int64;
query,norm:string;
felder :array [0..3] of string;
abrechnen:integer;
begin
  result:=0;
	if abrechnen_voso then abrechnen:=1 else abrechnen:=0;
	query:='select goae,euro,b_group,ust from typ where nummer='+inttostr(i_typ);
	mysql_d.Feldarray(query,[0,1,2,3],felder);
	if felder[2]='1'  then exit;//gruppen�berschrift

	datamodul.q_typ.locate('nummer',i_typ,[]);
  if not still then
		if datamodul.q_mitarbeiter['geschlecht']=1 then norm:= datamodul.q_typ.findfield('norm_w').asstring else norm:= datamodul.q_typ.findfield('norm_m').asstring ;

	nummer:=neuer_datensatz(datamodul.q_labor,['nummer','i_mitarbeiter','datum','i_status','i_typ','i_untersucher','einheit','norm','wert','goae','euro','faktor','abrechnen','ust','datum_einladung']
	,[null,inttostr(i_mitarbeiter),datum,i_status,i_typ,i_untersucher,datamodul.q_typ['einheit'],norm,0,felder[0],mysql_strtofloat(felder[1]),datamodul.q_firma['fa_lab'],abrechnen,strtoint(felder[3]),null_datum]);
	datamodul.q_labor['farbe']:=status_farbe(datamodul.q_labor['i_status']);

	if speichern then
		begin
		direktspeichern:=true;
		datamodul.sql_post(datamodul.q_labor);
		end;
  if not still then
  	form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_labor,nummer,4,1);

	result:=nummer;
end;


function tform_main.unt_neu(datum:tdate;i_typ,i_paw,i_status:integer;i_mitarbeiter:int64;i_untersucher:integer;speichern:boolean):int64;
var nummer:int64;
query,uart:string;
felder :array [0..4] of string;
abrechnen:integer;
begin
with datamodul do
begin
	query:=format('select max(i_uart) from untersuchung where i_mitarbeiter=%s and i_typ=%d',[inttostr(i_mitarbeiter),i_typ]);
	uart:=mysql_d.Feldinhalt(query,0);
	if uart='1' then uart:='2';
	if uart='' then uart:='1';
	if abrechnen_voso then abrechnen:=1 else abrechnen:=0;
	query:='select goae,euro,ust,i_paw,i_arbmedvv from typ where nummer='+inttostr(i_typ);
	mysql_d.Feldarray(query,[0,1,2,3,4],felder);
  if i_paw=0 then i_paw:=strtoint(felder[3]); // es wird sonst immer paw der Vorgabe eingetragen!
	nummer:=neuer_datensatz(datamodul.q_untersuchung,['nummer','i_mitarbeiter','datum','i_status','i_typ','i_arbmedvv','i_paw','i_untersucher','i_uart','goae','euro','ust','faktor','abrechnen','datum_einladung','w_dat','dat_gedruckt']
	,[null,inttostr(i_mitarbeiter),datum,i_status,i_typ,felder[4],felder[3],i_untersucher,uart,felder[0],mysql_strtofloat(felder[1]),strtoint(felder[2]),q_firma['fa_unt'],abrechnen,null_datum,null_datum,null_datum]);

	datamodul.q_untersuchung['farbe']:=status_farbe(datamodul.q_untersuchung['i_status']);
	if speichern then
		begin
			direktspeichern:=true;
			sql_post(q_untersuchung);
		end;
	dbTreeView.selected:=form_main.dbTreeView.einhaengen( q_untersuchung,nummer,2,1);
  //dbtreeview.knoten_neuordnen(dbTreeView.selected);
	result:=nummer;
end;
end;


function tform_main.impf_neu(datum:tdate;i_typ,i_status:integer;i_mitarbeiter:int64;i_untersucher:integer;speichern:boolean):int64;
var nummer:int64;
query:string;
felder :array [0..2] of string;
abrechnen:integer;
begin
with datamodul do
begin
	query:='select goae,euro,ust from typ where nummer='+inttostr(i_typ);
	mysql_d.Feldarray(query,[0,1,2],felder);
	if abrechnen_voso then abrechnen:=1 else abrechnen:=0;

	 nummer:=neuer_datensatz(datamodul.q_impfung,['nummer','i_mitarbeiter','datum','i_status','i_typ','i_untersucher','charge','dosis','goae','euro','faktor','abrechnen','ust','datum_einladung']
		,[null,inttostr(i_mitarbeiter),datum,i_status,i_typ,i_untersucher,'','',felder[0],mysql_strtofloat(felder[1]),q_firma['fa_impf'],abrechnen,strtoint(felder[2]),null_datum]);
	 datamodul.q_impfung['farbe']:=status_farbe(datamodul.q_impfung['i_status']);
	 if speichern then
			begin
			direktspeichern:=true;
			sql_post(q_impfung);
			end;
		form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( q_impfung,nummer,3,1);
	 result:=nummer;
end;
end;



function tform_main.besonderheit_neu(datum:tdate):int64;
var
i_mitarbeiter,nummer:int64;
bes_nummer,i:integer;
knoten: ttreenode;
daten:pnodepointer;
flag:boolean;
begin
flag:=false;
with datamodul do
begin
	  datamodul.sql_new(true,datamodul.q_besonderheitenListe_tree,'select * from besonderheitenliste', 'reihenfolge');
	  form_besonderheiten:=tform_besonderheiten.Create(self);
     form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
	  form_besonderheiten.tree.DragMode:=dmmanual;
	  form_besonderheiten.multiselect:=true;
	  form_besonderheiten.ToolBar.Visible:=false;
	  form_besonderheiten.Panel_bottom.Visible:=true;
	  if form_besonderheiten.showmodal=mrok then
		 begin
			i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');

			for i:=0 to  form_besonderheiten.Tree.Items.Count-1 do
			begin
				if form_besonderheiten.Tree.Items[i].stateindex>0 then //ausgew�hlt
				begin
				 knoten :=form_besonderheiten.Tree.items[i];
				 daten:=knoten.Data;
				 bes_nummer:=daten^.nummer;
				 flag:=true;
				 nummer:=neuer_datensatz(q_besonderheiten,['nummer','i_mitarbeiter','i_besonderheit','datum','datum_bis','i_user','i_untersucher' ]
				,[null,inttostr(i_mitarbeiter),bes_nummer,datum,'1900-01-01','0',akt_untersucher]);
				direktspeichern:=true;
				datamodul.sql_post(datamodul.q_besonderheiten);
				form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( q_besonderheiten,nummer,7,-1);

				end;
			end;

		  if not flag then //nicht mehrere ausgew�hlt
		  begin
			nummer:=neuer_datensatz(q_besonderheiten,['nummer','i_mitarbeiter','i_besonderheit','datum','datum_bis','i_user' ]
			,[null,inttostr(i_mitarbeiter),q_besonderheitenListe_tree['nummer'],datum,'1900-01-01',0]);
			direktspeichern:=true;
			sql_post(q_besonderheiten);
			form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( q_besonderheiten,nummer,7,-1);
		  end;
		  result:=nummer;
		 end;
	  form_besonderheiten.release;
end;
end;

function tform_main.diagnose_neu(datum:tdate):int64;
var
i_mitarbeiter,nummer:int64;
begin
with datamodul do
begin
	 form_icd:=tform_icd.Create(self);
		 if form_icd.showmodal=mrok then
		 begin
			i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');
			nummer:= neuer_datensatz(q_diagnosen,['nummer','i_mitarbeiter','datum','datum_bis'],[null,inttostr(i_mitarbeiter),datum,'2999/12/31']);
			datamodul.q_diagnosen.edit;
			datamodul.q_diagnosen['icd']:=datamodul.q_icd_main['icd_schluessel'];
        datamodul.q_diagnosen['sternnummer']:=datamodul.q_icd_main['sternnummer'];
        datamodul.q_diagnosen['zusatznummer']:=datamodul.q_icd_main['zusatznummer'];
			datamodul.q_diagnosen['icd_text']:=datamodul.q_icd_main['icd_text'];
			datamodul.q_diagnosen['i_icd']:=datamodul.q_icd_main['nummer'];
			dbmemo_diagnosen.Text:='';
			direktspeichern:=true;
			sql_post(q_diagnosen);
			form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( q_diagnosen,nummer,6,-1);
			result:=nummer;
		 end;
		datamodul.sql(true,datamodul.q_icd_main,'select * from icd_main','');
		form_icd.Release ;

end;
end;


function tform_main.befunde_neu(datum:tdate;kapitel:string):int64;
var i_mitarbeiter,nummer:int64;
	query:string;
	f:boolean;
begin
		i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');

		query:=format('select * from befunde where i_mitarbeiter=%s and datum=%s',
				[inttostr(i_mitarbeiter),sql_datetostr(datum)]);
		datamodul.sql_new(false,datamodul.q_1,query,'');
		f:=datensatz(datamodul.q_1);

		if f   then//vorhanden 15.4.05:  es ist immer nur ein Befund pro Tag m�glich
		begin
			if datamodul.q_1['storno']=0 	then
			begin
			  showmessage('F�r dieses Datum existieren schon Befunde.');
			  result:=0;
			  exit;
			end
			else
			begin //ist gel�scht
			  datamodul.q_1.edit;
			  datamodul.q_1['storno']:=0;
			  datamodul.q_1.post;
			  datamodul.q_befunde.Refresh;
			  //datamodul.q_befunde.Locate('i_mitarbeiter,datum',vararrayof([inttostr(i_mitarbeiter),datum]),[]);
        datamodul.q_befunde.Locate('i_mitarbeiter,datum',vararrayof([(i_mitarbeiter),datum]),[]);
			  result:=getbigint( datamodul.q_befunde,'nummer');
			  if not storno_show then dbtreeview.selected:= dbtreeview.einhaengen(datamodul.q_befunde,nummer,1,-1)
			  else
			  begin
				dbtreeview.gehe_zu_datensatz(1,result);
				dbtreeview.statindex(1) ;
			  end;
			end;
		end
		else
		begin
		  result:=neuer_datensatz(datamodul.q_befunde,['nummer','i_mitarbeiter','datum','benutzer','kapitel']
		  ,[null,inttostr(i_mitarbeiter),datum, akt_untersucher,kapitel]);
		  direktspeichern:=true;
		  datamodul.q_befunde.post; //ppp
		  dbtreeview.selected:= dbtreeview.einhaengen(datamodul.q_befunde,result,1,-1);//nummer
		end;
		application.ProcessMessages;
end;



function tform_main.rechnung_neu(datum:tdate):int64;
var
nummer:int64;
begin
	try

	  form_rechnung:=tform_rechnung.create(self);
	  form_rechnung.b_f_re:=false;
	  form_rechnung.GroupBox_proband.Visible:=false;
	  form_rechnung.CheckBox_pauschal.Checked:=false;
	  form_rechnung.CheckBox_pauschal.Visible:=false;
	  form_rechnung.CheckBox_stunden.checked:=false;
	  form_rechnung.CheckBox_stunden.Visible:=false;
     form_rechnung.CheckBox_fahrstrecke.checked:=false;
	  form_rechnung.CheckBox_fahrstrecke.Visible:=false;
     Form_rechnung.groupbox_leistungen.visible:=false;
     Form_rechnung.groupbox_uf.visible:=false;
     Form_rechnung.BitBtn_f_tauschenClick(self);

	  //form_rechnung.maskedit_von.Text:=datetostr(datum);
	  //form_rechnung.maskedit_bis.Text:=datetostr(datum);
      //form_rechnung.maskedit_redat.Text:=datetostr(datum);
	  if prg_typ=3 then
	  begin
		 form_rechnung.GroupBox_leistungen.Visible:=false;
		 form_rechnung.GroupBox_proband.Visible:=false;
	  end;
	  if form_rechnung.ShowModal=mrok then
	  begin
		nummer:=getbigint(datamodul.q_rechnung,'nummer');
		form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_rechnung,nummer,10,-1);
		//formular_entsperren(true,false);
	  end;
	 finally
     form_rechnung.Release;
      result:=nummer;
	 end;

end;


function tform_main.verkauf_neu(datum:tdate):int64;
var
nummer,i_mitarbeiter:int64;
m_nummer,menge:integer;
vkp,r_menge:real;
m_name,s_einheit:string;
begin
	try
	  form_tabellen:=tform_tabellen.Create(self);
	  tabellen_material_einstellen;
	  form_tabellen.ToolBar.Visible:=false;
	  form_tabellen.Panel_b.Visible:=true;
	  if form_tabellen.showmodal<>mrok then exit;
    if (not form_tabellen.CheckBox_frei_artikel.Checked) then
	  begin    //artikel wurde ausgew�hlt
		m_nummer:=datamodul.q_1['nummer'];
		i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');
		vkp:=datamodul.q_1['verkaufspreis'];
		m_name:=datamodul.q_1['art_name'];
    r_menge:=datamodul.q_1.findfield('inhalt').asfloat;
    s_einheit:= datamodul.q_1.findfield('einheit').asstring;
		nummer:= neuer_datensatz(datamodul.q_artikel,['nummer','i_mitarbeiter','datum','i_material','einzel_netto','art_name','ust','menge','einheit'],[null,inttostr(i_mitarbeiter),datum,m_nummer,vkp,m_name,1,r_menge,s_einheit]);
		direktspeichern:=true;
		datamodul.sql_post(datamodul.q_artikel);
     form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_artikel,nummer,11,-1);
	  end
    else
    begin
    i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');
	  nummer:= neuer_datensatz(datamodul.q_artikel,['nummer','i_mitarbeiter','datum','i_material','einzel_netto','art_name'],[null,inttostr(i_mitarbeiter),datum,0,0,'']);
    direktspeichern:=true;
	  datamodul.sql_post(datamodul.q_artikel);
    form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_artikel,nummer,11,-1);

    end;
	 finally
		 form_tabellen.Release;
         result:=nummer;
         //datamodul.q_artikel.Refresh;
	 end;

end;


function tform_main.kontakt_neu(datum:tdate):int64;
var
i_mitarbeiter:int64;
begin
     i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');
	   result:= neuer_datensatz(datamodul.q_kontakte,['nummer','i_mitarbeiter','datum'],[null,inttostr(i_mitarbeiter),datum]);
     datamodul.q_kontakte.post;
     form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_kontakte,result,12,-1);
end;


procedure TForm_main.BitBtn_impf_planenClick(Sender: TObject);
begin
//db_planen(datamodul.q_impfung,2);
dbspeichern(false);
unt_planen(datamodul.q_impfung);
end;

procedure TForm_main.labor_zeichnen;
var
i_typ: integer;
wert,max:real;
norm_m,norm_w:string;
begin

try
datamodul. sql_new(true,datamodul.q_1,'select * from labor where i_mitarbeiter = '
						+getbigint_str(datamodul.q_mitarbeiter,'nummer')+
					 ' and i_typ = '+inttostr(datamodul.q_labor['i_typ']),'datum');
//i:=1;
//j:=0;
max:=0;
//i_stammdaten:=getbigint(datamodul.q_mitarbeiter,'nummer');
i_typ:=datamodul.q_labor['i_typ'] ;
if datamodul.q_1['i_typ']=null then exit;
datamodul.q_typ.first;
datamodul.q_typ.Locate('nummer',i_typ,[] );
if (datamodul.q_typ['norm_m'])<>null then norm_m:= string(datamodul.q_typ['norm_m']);
if (datamodul.q_typ['norm_w'])<>null then norm_w:= string(datamodul.q_typ['norm_w']);

xygraph.clear;
xygraph.titel:=format('%s  [%s]  Norm M: %s  Norm W: %s ',[string(datamodul.q_typ['untersuchung']),string(datamodul.q_typ['einheit']),norm_m,norm_w]); //Norm W: %s %1:s

datamodul.q_1.first;
while (not datamodul.q_1.Eof)
do begin
	if datamodul.q_1['wert']<>null then
		wert:=datamodul.q_1['wert']
	else wert:=0;
	if wert> max then max:=wert;

	xygraph.add(datetostr(datamodul.q_1['datum']),wert);

	//inc(i);
	datamodul.q_1.Next;
end;
	xygraph.paint;
	xygraph.shape_zeichnen;
except
end;
end;

procedure TForm_main.alle_olecontainer_schliessen;
begin
  olecontainer_destroy(arbeitsmedizin);
	olecontainer_destroy(arbeitsmedizin_firma);
	olecontainer_destroy(arbeitsmedizin_Proband);
	olecontainer_destroy(arbeitsmedizin_befund);
end;

procedure TForm_main.FormClose(Sender: TObject; var Action: TCloseAction);
var query,f_name,tab_name,f:string;
p:tstringlist;
begin
//if application.Terminated then exit;
application.ProcessMessages;
try
  p:=tstringlist.Create;
  if tag<>1 then
  begin
       //wartefenster_zeigen;
       if (abgleich_ende and directoryexists(pfad_abgleich_haupt) and (system_modus=0)) then
       begin
         if (Messagedlg('Soll ein Abgleich mit dem Hauptsystem erfolgen?',mtConfirmation, mbOkCancel	,0)=mrOK ) then
         m_dbabgleichClick(self);
       end;

      if com.benutzer_id_liste.count=0 then    //sperrung in nummern_kreis auf 0 setzen
      begin
        query:='update nummern_kreis set gesperrt=0 ';
        mysql_d.sql_exe(query);
      end;
      p.Add( ' Sperrungen aufgehoben, ');
        protokoll('user','stop','',true);
        reg_schreiben; // jtzt in inischreiben
        einstellungen_schreiben;  // in die datenbank
        einstellungen_schreiben_global;
        inischreiben(35) ; //common_appdata
        inischreiben(28);//  user_appdata
      //formulare schliessen und l�schen
       p.Add(' Ini geschrieben, ');
        alle_olecontainer_schliessen;
        p.Add(' Ole geschlossen ');
        com.excel_disconnect;
        p.Add(' excel disconnect, ');
      dbtreeview.liste_loeschen;

      if form_termine<>nil then form_termine.Release;
       p.Add(' form-termine geschlossen, ');
      if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.', [ pfad_temp]));
      //pfadpruefen(pfad_temp,'c:\temp\');
      f_name:=pfad_temp+'infotext.html';
      if fileexists(f_name) then deletefile(f_name);

  
      //################### l�schen tempor�rer Tabellen  und Dateien
      mysql_d.drop_table(sql_felder_temp);
      mysql_d.drop_table(sql_abfragen_temp);
       p.Add(' temp-Dateien gel�scht 1 ');
      //temp_dat:=format('temp_proband_%d',[user_id]);

      mysql_d.drop_table(format('temp_tab_%d',[user_id]));
      mysql_d.drop_table(format('temp_tab_%d_13',[user_id]));
      mysql_d.drop_table(format('temp_tab_%d_14',[user_id]));
      mysql_d.drop_table(format('temp_proband_%d',[user_id]));
      p.Add(' temp dateien gel�scht 2 ');
      {mysql_d.drop_table('temp_tab'+user_name);
      mysql_d.drop_table('temp_tab'+user_name+'13');
      mysql_d.drop_table('temp_tab'+user_name+'14');}

      //alle datens�tze l�schen wenn nummer=0

       datamodul.sql_new(false,datamodul.q_1,'select tab_name from export_import where main>=0','');
       datamodul.q_1.first;
       while not datamodul.q_1.eof do
       begin
        try
         tab_name:=datamodul.q_1['tab_name'];
         query:=format('delete from %s where nummer=0',[tab_name]);
         p.Add('wird ausgef�hrt: '+query);
         mysql_d.sql_exe_still(query);
         p.Add('wurde ausgef�hrt: '+query);
         datamodul.q_1.next;
        except
        end;
       end;

      p.Add(' nummer 0 gel�scht, ');

      pcid('loeschen');

      deletefiles(pfad_temp,'a_*.*');
      //datamodul.sql_close;
      //p.Add(' sql_close');
      mysql_d.ds_entsperren('user',user_id,1,nil);

      monats_feiertage.Free;
      Jahres_feiertage.Free;
       p.Add(' free1, ');

      //treedeaktivieren(tree_bereich);
      while combobox_history.Items.Count>0 do history_delete(0);
      soundex.free;
      b.free;
      rtfedit.Free;
      form_richedit_menue.Release;
       p.Add(' free2,');
   
      if tag<>2 then sicherung(false);
      p.Add(' sicherung ');
        //help
      HHCloseAll;
      if mhhelp<>nil then  mHHelp.Free;
      //if mysql_d<>nil then mysql_d.Free;
       p.Add(' hhfree ');
       if com<>nil then com.Free;
       if zipmaster<>nil then  zipmaster.Clear;
       p.Add(' zipfree ');
       combodeaktivieren(ComboBox_zeit_kriterium);
       combodeaktivieren(ComboBox_zeit_konto);
       combodeaktivieren(combobox);
       combodeaktivieren(combobox_rechts);
       combodeaktivieren(combobox_ag_user);
       combodeaktivieren(ComboBox_zeit_kriterium) ;
       combodeaktivieren(ComboBox_zeit_konto);
        p.Add(' combodeaktiviert, ');
        //fehler();
       liste_leeren(treeview_agenda);
       dispose(hist_daten);
        p.Add(' hist_daten ');

  end;
  application.processmessages;

  datamodul.sql_close;
  p.Add(' sql_close ');
  if (mysql_d<>nil) then mysql_d.disconnect;
  if (hmysql_d<>nil) then hmysql_d.disconnect;
  p.Add(' sql_d close ');
  Application.OnMessage := nil;
  application.OnActivate:=nil;
  application.OnException:=nil;
   p.Add(' ende ');
  application.ProcessMessages;
  p.Free;
  l_csv.Free;
  l_csv_arbene.Free;
  l_rechnung_format.free;
  l_rechnung_spalten.Free;
  l_labor_werte.Free;
  l_labor_hinweise.Free;
except
  f:=pfad_temp+'e2_'+ersetzesonderzeichen(datetimetostr(now()))+'.txt' ;
  showmessage('Fehler beim Beenden.'+#13+f);
  p.SaveToFile(f);
  p.Free;
end;
end;


procedure TForm_main.tool_aktiv(tabelle:tzquery);
begin
	//if (tabelle.bof and tabelle.Eof) then
    if kein_datensatz(tabelle) then
		tool_inaktiv
  else
  begin
		toolbuttonedit.Enabled:=true;
		menue_edit.Enabled:=true;
	 end;
end;


procedure TForm_main.tool_inaktiv;
begin
	toolbuttonedit.Enabled:=false;
	menue_edit.Enabled:=false;
end;


procedure tform_main.tree_befunde_filtern(checken:boolean);
var
i_mitarbeiter,ma:int64;
and_stono,query:string;
begin
dbTreeView.change_allowed:=false;
dbtreeview.a_storno:=storno_show;
if storno_show then and_storno:='' else and_storno := ' and storno=0';
with datamodul do
begin
	tool_aktiv(datamodul.q_untersuchung);
	i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');

  //anamnese
	sql_relation( 'anamnese','nummer',q_mitarbeiter,q_anamnese,'');
  akt_editor.Lines.Clear;
  if   (b.b_string_silent('proband-anamnese-arzt')>=0)  then anamnese_laden(0);

  //befunde
	if m_befunde_c and (b.b_string_silent('proband-befunde')>=0) then
	begin

		sql_relation_desc('befunde','i_mitarbeiter',q_mitarbeiter,q_befunde,'datum');
     sql_leer('akt_untersuchung',q_akt_untersuchung);

		if storno_show then
		begin
			query:=format('select akt_untersuchung.*, befunde.datum from akt_untersuchung left join befunde on (akt_untersuchung.i_untersuchung=befunde.nummer) where befunde.i_mitarbeiter= %s and akt_untersuchung.storno=1',
					[inttostr(i_mitarbeiter)]);
			sql(false,q_akt_untersuchung_storno,query,'')
		end
		else
			sql_leer('akt_untersuchung',q_akt_untersuchung_storno);

	end
	else
	begin
		sql_leer('befunde',q_befunde);
		sql_leer('akt_untersuchung',q_akt_untersuchung_storno);
		sql_leer('akt_untersuchung',q_akt_untersuchung);
	end;

  //vorsorge
	if checken or (m_vorsorge_c) then m_vorsorge_c:=true;
	if m_vorsorge_c and (b.b_string_silent('Proband-Bescheinigung')>=0) then ma:=i_mitarbeiter else ma:=0;
   if m_vorsorge_c and (b.b_string_silent('proband-bescheinigung-ag')>=0) then ma:=i_mitarbeiter;
	query:='select * from untersuchung  '+
		 format('where untersuchung.i_mitarbeiter=%s %s order by datum desc',[inttostr(ma),and_storno]);
	sql(false,q_untersuchung,query,'');

   {if (b.b_string_silent('proband-bescheinigung-ag')>=0) then
   begin
   	q_untersuchung.first;
      while not q_untersuchung.Eof do
      begin
        if not zust_arbmed_besch_vorhanden(q_untersuchung.findfield('i_weitergabe').asinteger,q_untersuchung.findfield('i_typ').asinteger,q_untersuchung.findfield('datum').AsDateTime  ) then ;
        q_untersuchung.Next;
      end;
   end;}

  //impfung
	if checken or (m_impfung_C)  then m_impfung_C:=true;
	if m_impfung_C and (b.b_string_silent('proband-impfung')>=0) then ma:=i_mitarbeiter else ma:=0;
	 query:=format('select * from impfung where impfung.i_mitarbeiter=%s %s order by datum desc',[inttostr(ma),and_storno]);
	 sql(false,q_impfung,query,'');

   //labor
	if checken or (m_labor_C)  then m_labor_C:=true;
	if m_labor_C and (b.b_string_silent('proband-labor')>=0) then ma:=i_mitarbeiter else ma:=0;
	  query:=format('select * from labor where labor.i_mitarbeiter=%s %s order by datum desc',[inttostr(ma),and_storno]);
	  sql(false,q_labor,query,'');


  //ambulanz
	pagecontrol_ambulanz.ActivePage:=tabsheet_u_daten;
	if b.b_objekt('proband-ambulanz')>-1 then
		query:='select * from ambulanz where i_mitarbeiter='+inttostr(i_mitarbeiter)+and_storno+' order by datum desc'
	else
	begin//nur arbeitsunf�lle sichtbar
		if b.b_objekt('proband-arbeitsunfall')>-1 then
		begin
			query:='select * from ambulanz where  i_mitarbeiter='+inttostr(i_mitarbeiter)+and_storno+' and b_arbeitsunfall=1 order by datum desc';
			groupbox_vitaldaten.Visible:=false;
			groupbox_icd_diagnose.visible:=false;
			dbcheckbox_aunfall.enabled:=false;
		end
		else query:='select * from ambulanz where nummer=-1';
	end;

	if (not m_ambulanz_c) and (b.b_string_silent('proband-ambulanz')>=0) then query:='select * from ambulanz where nummer=-1';
	 sql_new(true,q_ambulanz,query,'');


  //diagnose
	if m_diagnosen_c and (b.b_string_silent('proband-diagnosen')>=0) then
	begin
		//sql_relation_desc('diagnosen','i_mitarbeiter',q_mitarbeiter,q_diagnosen,'datum')
		query:=format('select * from diagnosen where i_mitarbeiter=%s %s order by datum desc',[inttostr(i_mitarbeiter),and_storno]);
		sql(false,q_diagnosen,query,'');
	end
	else sql_leer('diagnosen',q_diagnosen) ;

  //besonderheiten
	if m_besonderheiten_c  and (b.b_string_silent('proband-besonderheiten')>=0) then
	begin
		//sql_relation_desc('besonderheiten','i_mitarbeiter',q_mitarbeiter,q_besonderheiten,'datum')
		query:=format('select * from besonderheiten where i_mitarbeiter=%s %s order by datum desc',[inttostr(i_mitarbeiter),and_storno]);
		sql(false,q_besonderheiten,query,'');
	end
	else sql_leer('besonderheiten',q_besonderheiten) ;


  //dokumente
	if m_dokumente_C  then
	begin
    sql_leer('dokumente',q_dokumente);
		if b.b_objekt('proband-dokumente-arzt')>=0 then
      begin
        //sql_relation_desc('dokumente','i_mitarbeiter',q_mitarbeiter,q_dokumente,'datum')
        if b.b_string_silent('proband-rechnung')<=0 then
          query:=format('select * from dokumente where i_mitarbeiter=%s and modus=0 %s order by datum desc',[inttostr(i_mitarbeiter),and_storno])
        else
          query:=format('select * from dokumente where i_mitarbeiter=%s %s order by datum desc',[inttostr(i_mitarbeiter),and_storno]);

        sql(false,q_dokumente,query,'');
      end
    else
		begin
      if b.b_objekt('proband-dokumente-sani')>=0 then
      begin
            query:=format('select * from dokumente where i_mitarbeiter=%s %s and b_ambulanz=1 and modus=0 order by datum desc',
								[inttostr(i_mitarbeiter),and_storno]);
			      sql_new(true,q_dokumente,query,'');
       end;

		end;
	end
	else sql_leer('dokumente',q_dokumente) ;


  //rechnungen
	if m_rechnungen_c and (b.b_string_silent('proband-rechnung')>=0) then
	begin

    //query:=format('select * from rechnung where i_mitarbeiter=%s and i_firma=-1 %s order by datum desc',[inttostr(i_mitarbeiter),and_storno]);
    //query:=format('select * from rechnung where i_mitarbeiter=%s  %s order by datum desc, ts desc',[inttostr(i_mitarbeiter),and_storno]);     nur nach datum sortieren
    query:=format('select * from rechnung where i_mitarbeiter=%s  %s order by datum desc,nummer desc',[inttostr(i_mitarbeiter),and_storno]);


    sql(false,q_rechnung,query,'');
	end
	else sql_leer('rechnung',q_rechnung) ;

  //artikel
	if m_artikel_c and (b.b_string_silent('proband-artikel')>=0) then
	begin
     query:=format('select artikel.* from artikel where i_mitarbeiter=%s %s order by datum desc',[inttostr(i_mitarbeiter),and_storno]);
	  sql(false,q_artikel,query,'');
	end
	else sql_leer('artikel',q_artikel) ;

  //kontakte
  if m_kontakte_c then
	begin
     query:=format('select kontakte.* from kontakte where i_mitarbeiter=%s %s order by datum desc',[inttostr(i_mitarbeiter),and_storno]);
	  sql(false,q_kontakte,query,'');
	end
	else sql_leer('kontakte',q_kontakte) ;


	dbtreeview.liste_lesen;

	if dbtreeview.selected=nil then notebook_main.pageindex:=0;
end;
dbTreeView.change_allowed:=true;
end;


procedure TForm_main.pagecontrol_mainChange(Sender: TObject);
var
newmenu: tmenuitem;
c,query,last_nr:string;
i_firma:integer;
i_ap, i_abt:int64;
begin
with datamodul do
begin
//menue_leeren;
last_nr:=q_mitarbeiter.findfield('nummer').asstring;
//if assigned(form_ap) then form_ap.release;
//if (not (pagecontrol_main.ActivePage=tabsheet_firma)) and
//		(not (pagecontrol_main.ActivePage=tabsheet_stammdaten)) then namenanzeigen;

m_abteilung.Enabled:=false;
mysql_d.baum_k:=nil;
mysql_d.no_transaction:=false;

if (pagecontrol_main.ActivePage=tabsheet_firma) then
		begin
       pagecontrol_firma.ActivePage:=tabsheet_firmenadresse;
       PageControl_firmaChange(Sender);//tool_aktiv(datamodul.q_firma);
       //f�r jeden erlaubt
       tabsheet_firma.tag:=1;
       grid_suchen(tabsheet_firma);
       grid_suchen(tabsheet_firmenadresse);
       //kein Proband ausgew�hlt
       //toolbutton_abteilung.enabled:=false;
		  //toolbutton_schichtplan.Enabled:=false;
		  m_personalien_drucken.Enabled:=false;
      m_ProbandenDokumentohneSpeichernerstellen.Enabled:=false;
       //m_druck_alleBesch.Enabled:=false;
		end
else
   begin
     //Proband ausgew�hlt
     //toolbutton_abteilung.enabled:=true;
     //toolbutton_schichtplan.Enabled:=true;
     m_personalien_drucken.Enabled:=true;
     m_ProbandenDokumentohneSpeichernerstellen.Enabled:=true;
     //m_druck_alleBesch.Enabled:=true;
   end;

if (pagecontrol_main.ActivePage=tabsheet_stammdaten) then
	begin
    if (last_ts=tabsheet_firma)  or (last_ts=tabsheet_abteilung) then
    begin
      mitarbeiter_filtern();
      gehe_zu_history(false);
      //q_mitarbeiter.Locate('nummer', strtoint64(last_nr),[]) ;
    end;
    application.processmessages;
		tool_aktiv(datamodul.q_mitarbeiter);
		grid_suchen(tabsheet_stammdaten);
    //m_Export_Probandenbersicht.Visible:=true;

    label_prob_count.Caption:='Anzahl: '+mysql_d.anzahl_datensaetze(datamodul.q_mitarbeiter);
	end;

if (pagecontrol_main.ActivePage=tabsheet_abteilung) then
begin
  if b.b_string('Firma-Abteilung')>-1 then
  begin

       i_firma:=datamodul.q_firma['nummer'];
       abt_manager1(i_firma,0,'all',nil,0) ;

       if (last_ts=tabsheet_stammdaten) or (last_ts=tabsheet_main) then
       begin
         i_abt:=getbigint( datamodul.q_mitarbeiter,'i_abteilung');
         i_ap:=getbigint( datamodul.q_mitarbeiter,'i_arbeitsplatz');
         if i_ap>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(2,i_ap)
         else
         if i_abt>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(1,i_abt)
         else form_ap.dbtreeview_k1.Selected:=form_ap.dbtreeview_k1.Items[0];
         form_ap.dbtreeview_k1.SetFocus;
       end;
     end;
     m_abteilung.visible:=true;
     m_UnfallverteilunginderFirma.Visible:=true;
     m_abteilung.Enabled:=true;
     mysql_d.no_transaction:=true;
     //mysql_d.baum_k:=form_ap.dbtreeview_k1;
end;
end;

 begin
	toolbuttonneu.Enabled:=true;
	menue_new.Enabled:=true;
 end;

if pagecontrol_main.ActivePage=tabsheet_main then
	begin
    if (last_ts=tabsheet_abteilung) or (last_ts=tabsheet_firma) then
    begin
      mitarbeiter_filtern();
      gehe_zu_history(false);
      //datamodul.q_mitarbeiter.Locate('nummer',last_nr,[]) ;
      tool_aktiv(datamodul.q_mitarbeiter);
      grid_suchen(tabsheet_stammdaten);
      pagecontrol_main.ActivePage:=tabsheet_stammdaten;;
    end;
    if  (kein_datensatz(datamodul.q_mitarbeiter)) then
     begin
          showmessage('Bitte zuerst einen Probanden anlegen');
          pagecontrol_main.ActivePage:=tabsheet_stammdaten;
        exit;
     end;
     namenanzeigen;
	  tree_befunde_filtern(false);  //hier auch Anamnese laden
	  dbtreeview.Expand_e1;
     m_uebersicht_kartei.enabled:=true;
     m_uebersicht_kartei.visible:=true;
     m_GDT_schreiben.enabled:=true;
     m_GDT_schreiben.visible:=true;
	end ;

end;



procedure TForm_main.ToolButton_neuClick(Sender: TObject);
begin
  //beep();
	datensatz_neu;
end;


procedure tform_main.datensatz_neu;
var i,i_firma,f_nummer,r,i_zeitkonto:integer;
	  neu:boolean;
	  ds,query,kapitel,s1,d1,d2,tab_name:string;
	  datum,dd:tdate;
    i_mitarbeiter,nr,ma_nr:int64;
begin
if modus_history then exit;

    i:=0;
    i:=i+1;
    tab_name:='';
	dbspeichern(false);
	//if not formular_entsperren(false,true) then exit;  neuer DS muss nicht gesperrt werden

  if  (pagecontrol_main.activepage<>tabsheet_firma) and  (datamodul.q_firma.findfield('storno').asstring ='1')  then
  begin
       showmessage('Die Firma ist gel�scht, es sind keine Ver�nderungen m�glich');
       exit;
  end;
	try
      //###speichern### hier start transaction
      //start_transaction('',0); //new

		 not_sperren:=true;
		 neu:=false;
      i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter,'nummer');
      nr:=0;
		 if datamodul.q_Firma['nummer']<>null then i_firma:=datamodul.q_Firma['nummer'] else i_firma:=-1;
		 //i_mitarbeiter:= getbigint(datamodul.q_mitarbeiter,'nummer');

		 if pagecontrol_main.ActivePage=tabsheet_firma then
		 begin
			if (pagecontrol_firma.activepage=tabsheet_firmenadresse)  then
			 begin
				if  b.b_string('firma-stammdaten')<2 then exit;

           if bestaet_neu then
             if prg_typ=3 then
               begin
                    if MessageDlg('Neue Kunden-Gruppe anlegen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
               end
               else
                   if MessageDlg('Neue Firma anlegen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;

             if nr=-1 then exit; //berechtigung
             //demoversion
            query:='select count(*) as anzahl from firma where storno=0';
				 if (m_lizenz.Visible and (strtoint(mysql_d.Feldinhalt(query,0))>=10)) then
				 begin
					  showmessage('In der Demo-Version k�nnen nur zehn Firmen angelegt werden');
                exit;
            end;

            //neu anlegen
         tab_name:='firma';
				 nr:=neuer_datensatz(datamodul.q_firma,['nummer','fa_bef','fa_impf','fa_lab','fa_unt','ar_bef','ar_impf','ar_lab','ar_unt'],
             													[null,'2,3','1,8','1,15',1,1,1,1,1]);

				 datamodul.q_Firma.edit;
				 datamodul.q_Firma['firma']:='';
				 richedit_firma.Text:='';
         richedit_firma.Modified:=false;
				 direktspeichern:=true;
				 datamodul.q_firma.post;
         //gel�schten ma anlegen f�r Abfragen und Word-Texte
         ma_nr:=neuer_datensatz(datamodul.q_mitarbeiter,['nummer','i_firma','name','archiv','ende','storno'],
             																[null,inttostr(nr),'dummy','-','2999/12/31','0']);
         direktspeichern:=true;
				 datamodul.q_mitarbeiter.post;
         query:=format('update mitarbeiter set storno=1 where nummer=%s',[inttostr(ma_nr)]);
         mysql_d.sql_exe(query);
				 neu:=true;

				 query:=format('insert into berechtigung_firma ( benutzer, i_firma ) values ( %d,%d) ',[user_id,nr]);
				 mysql_d.sql_exe(query);
				 berechtigung_firma;
         //datamodul.q_firma.Locate('nummer',inttostr(nr),[]);
         datamodul.q_firma.Locate('nummer',nr,[]);
            if bestaet_neu then
              if prg_typ=1 then
              begin
                showmessage('Bitte die Berechtigungen f�r die �brigen User �berpr�fen!'+#13+' (derzeit haben nur Sie die Berechtigung f�r die neue Firma)');
                if system_modus=0 then showmessage('Nach dem n�chsten Abgleich m�ssen die Berechtigungen auf dem Hauptsystem erteilt werden, '+#13+'da diese Firma auf diesem Satelliten sonst nicht mehr sichtbar ist!'+#13+'Nach der Vergabe der Berchtigungen auf dem Hauptsystem ist ein erneuter Abgleich notwendig!');
              end;
			 end;


       //##########################################
       if (pagecontrol_firma.activepage=tabsheet_firma_filter)  then
			 begin
                 if  b.b_string('firma-stammdaten')<2 then exit;
                 form_besonderheiten:=tform_besonderheiten.create(self);
                 form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
                 form_besonderheiten.ToolBar.Visible:=false;
                 form_besonderheiten.tquery:=datamodul.q_firma_filter_vorlage;
                 form_besonderheiten.Caption:='Firmen-Filter';
                 form_besonderheiten.Tree.mysqlquery:=datamodul.q_firma_filter_vorlage;
                 form_besonderheiten.Tree.mysql_feld:='name';
                 form_besonderheiten.tree.liste_lesen;
                 if form_besonderheiten.ShowModal=mrOK then
                 begin
                  //nr:=form_besonderheiten.tree.akt_nummer;
                  f_nummer:= datamodul.q_firma_filter_vorlage.findfield('nummer').asinteger;
                   r:= datamodul.q_firma_filter_vorlage.findfield('reihenfolge').asinteger;
                   s1:= datamodul.q_firma_filter_vorlage.findfield('name').asstring;
                   tab_name:='firma_filter';
                   nr:=neuer_datensatz(datamodul.q_firma_filter,['nummer','name','i_firma','i_firma_filter_vorlage','reihenfolge'],[0,s1,i_firma,f_nummer,r]);
                   datamodul.q_firma_filter.Post;
                 end;
                 form_besonderheiten.free;

			 end;

        //##########################################
			 if (pagecontrol_firma.activepage=tabsheet_ansprechpartner)  then
			 begin
				if b.b_string('firma-ansprechpartner')<2 then exit;
          tab_name:='firma_ansprech';
          nr:=neuer_datensatz(datamodul.q_firma_ansprech,['nummer','i_firma'],[0,i_firma]);
          if nr=-1 then exit;
				 datamodul.q_Firma_ansprech.edit;
				 datamodul.q_Firma_ansprech['name']:='';
				 richedit_firma_ansprech.Text:='';
         richedit_firma_ansprech.Modified:=false;
				 neu:=true;
			 end;
        //##########################################
       if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.activepage=tabsheet_firma_einstellungen_zeitabrechnung)  then
			 begin
             	if nicht_satellit_tab then exit;
                if  b.b_string('firma-zeitabrechnung')<2 then exit;
                datamodul.q_firma_zeiten_soll.first;
                d1:=dat_delphi_to_sql(datetostr(date));
                d1:=copy(d1,0,4);
                d2:=d1+'-12-31';
                d1:=d1+'-01-01';
                tab_name:='firma_zeiten_soll';
                nr:=neuer_datensatz(datamodul.q_firma_zeiten_soll,['nummer','i_firma','von','bis'],[null,i_firma,d1,d2]);
                neu:=true;
             end;
       //##########################################
			 if (pagecontrol_firma.activepage=tabsheet_sollist)  then
			 begin
				if  b.b_string('firma-zeitabrechnung')<2 then exit;
                 //if not ((strtodate(maskedit_z1.Text)<=now()) and (strtodate(maskedit_z2.Text)>=now())) then

          combobox_zeiterfassung.ItemIndex:=0;
            combobox_zeit_kriterium.itemindex:=0;
            ComboBox_zeit_konto.itemindex:=0;
            einsatzZeiteneinstellen;
            dbgridext_zeitabrechnung.SelectedRows.clear;

        i_zeitkonto:=zeitkonto(akt_untersucher);
        if i_zeitkonto<=0 then i_zeitkonto:=1;
        application.ProcessMessages;
         tab_name:='firma_sollist';
				 nr:=neuer_datensatz(datamodul.q_firma_sollist,['nummer','i_firma','i_sollist','zeitansatz','datum','i_untersucher','i_zeitkonto','stunden','minuten']
				 ,	[null,i_firma,0,'100',date,akt_untersucher, i_zeitkonto,0,0]);   //0 war fr�her radgroup sollist.itemindex
         datamodul.q_firma_sollist.Post;
				 datamodul.q_firma_sollist.edit;

         dbmemo_sollist_anlass.setfocus;
				 neu:=true;
         if zeiterfassung_showauswahl then  leistungsauswahl('aufgabe');
			 end;
        //##########################################
			 if pagecontrol_firma.activepage=tabsheet_firma_texte then
         begin
          if  b.b_string('firma-dokumente')<2 then exit;
          f_nummer:=datamodul.q_firma['nummer'];
          query:=format('select nummer from mitarbeiter where i_firma=%d',[f_nummer] );
          if not mysql_d.sql_exe_rowsvorhanden(query) then //kein MA angelegt
          begin
            if  Messagedlg('Felder werden erst gef�llt wenn ein Proband angelgt ist trotzdem weiter?',mtConfirmation, mbOkCancel	,0)<>mrOK then exit;
          end;
          datamodul.q_texte.first;   //anzeigen am Kopf

          dbgridext_texte.Enabled:=false;
          application.processmessages;
          tab_name:='texte';
          nr:=text_neu(0,date(),1,false);
          application.ProcessMessages;
          //datamodul.q_texte.EnableControls;
          dbgridext_texte.Enabled:=true;

          dbgridext_texte.SetFocus;
         end;
        //##########################################
			 if (pagecontrol_firma.activepage=tabsheet_rechnungen)  then
			 begin
				if  b.b_string('firma-zeitabrechnung')<2 then exit;
            //if nicht_satellit ('Rechnungen m�ssen auf dem Hauptsystem erstellt werden') then exit;    //wunsch von nottelmann es spricht eigentlich nichts dagegen

				try
				  form_rechnung:=tform_rechnung.create(self);
				  form_rechnung.b_f_re:=true;
         datamodul.q_rechnung.First;
         if datensatz(datamodul.q_rechnung) then datum:=datamodul.q_rechnung['bis']+1 else datum:=date();

         if form_rechnung.ShowModal=mrok then
          begin
            //rechnung_aktualisieren;
            formular_entsperren(true,false);//es wird keine nr �bergeben
          end;

			  finally
				form_rechnung.Release;
			  end;
			 end;
		  end;

      //##########################################
     if pagecontrol_main.ActivePage=tabsheet_abteilung then
     begin
        tab_name:=''; //hier nicht unterst�tzt
        if assigned(form_ap) then nr:=form_ap.a_neu();

     end;


		 if pagecontrol_main.ActivePage=tabsheet_stammdaten then
				begin
					  if  b.b_string('proband-stammdaten')<2 then exit;

					  if max_mitarbeiter<>0 then
					  begin
						query:='select count(*) from mitarbeiter where storno =0';
						ds:=mysql_d.Feldinhalt(query,0);
						if max_mitarbeiter<=strtoint(ds) then
							begin
								showmessage('Ihre Lizenz gilt f�r '+ds+' Probanden,  bitte fordern Sie eine neue Lizenz an.' );
								m_lizenz.Visible:=true;
								exit;
							end;
					  end;
                if bestaet_neu then
                  case prg_typ of
                  1:if MessageDlg('Neuen Probanden anlegen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
                  2:if MessageDlg('Neuen Probanden anlegen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
                  3:if MessageDlg('Neuen Kunden anlegen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
                  4:if MessageDlg('Neuen Patienten anlegen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
                  end;

               tab_name:='mitarbeiter';
               nr:=neuer_datensatz(datamodul.q_mitarbeiter,['nummer','i_firma','archiv','ende','zustimmung_bescheinigung_gueltig_bis','i_abteilung','i_arbeitsplatz'],
                                                            [null,i_firma,'-','2999/12/31','2999/12/31',inttostr(akt_abteilung),inttostr(akt_arbeitsplatz)]);

              pagecontrol_stammdaten.activepage:=tabsheet_ma_person;
              application.ProcessMessages;
				end;

      //#####################################################################################
      if pagecontrol_main.ActivePage=tabsheet_main then
      begin
           if not b.b_archiv_ok(datamodul.q_untersuchung) then
           begin
                    neu:=false;
                    exit;
           end;
        try
           form_u_wahl:=tform_u_wahl.Create(self);
           form_u_wahl.auswahl_einstellen(kart_index,0);

           //dd:=date()-strtodate(meditdate);   warun????????????????????
           //if (dd<6) and (dd>0) then meditdate:=datetostr(date);
           //wenn das datum wechselt sollt hier nachgezogen werden
           form_u_wahl.maskedit_date.text:=meditdate;//datetostr(date);
           if form_u_wahl.RadioGroup.Items.count=0 then exit;



           if form_u_wahl.RadioGroup.Items.count>1 then
           begin
             if (form_u_wahl.showmodal<>mrok)  then exit;
             datum:=strtodate(form_u_wahl.MaskEdit_date.text);
             meditdate:=form_u_wahl.MaskEdit_date.text;
             kapitel:=form_u_wahl.ComboBox.Text;
             i:= form_u_wahl.RadioGroup.ItemIndex;
           end
           else
           begin
            datum:=date();
            i:=0;
           end;

           ds:=form_u_wahl.RadioGroup.Items[i];
           if ds='Befund' then
             begin
                 if b.b_string('proband-befunde')<2 then exit;
                 tab_name:='befunde';
                 nr:=befunde_neu(datum,kapitel);
                 //BitBtn_beurt_ueber.Enabled:=false;
              end;
           if (ds='Bescheinigung') or (ds='Behandlung') then
           begin
                 if b.b_string_silent('Proband-Bescheinigung')>=0 then
                 if  (b.b_string('Proband-Bescheinigung')<2)   then exit;

                 if b.b_string_silent('Proband-Bescheinigung-AG')>=0 then
                 if  (b.b_string('Proband-Bescheinigung-AG')<2)   then exit;
                 tab_name:='untersuchung';
                 nr:=untersuchung_hinzufuegen(i_mitarbeiter, 1,1,datum,true);


                 //BitBtn_beurt_ueber.Enabled:=false;
              end;
           if ds='Impfung' then
           begin
                 if b.b_string('proband-impfung')<2 then exit;
                 tab_name:='impfung';
                 nr:=untersuchung_hinzufuegen(i_mitarbeiter,2,1,datum,true);
              end;

           if ds='Labor' then
            begin
                  if  b.b_string('proband-labor')<2 then exit;
                  pagecontrol_labor.activepage:=tabsheet_lab_wert;
                  tab_name:='labor';
                 nr:=untersuchung_hinzufuegen(i_mitarbeiter,3,1,datum,true);
              end;
           if ds='Ambulanz' then
            begin
               if (b.b_string_silent('proband-ambulanz')>1)  then
               begin
                  pagecontrol_ambulanz.activepage:=tabsheet_u_daten;
                  tab_name:='ambulanz';
                  nr:=ambulanz_neu(datum,0);
                end
                else
                   if (b.b_string_silent('proband-arbeitsunfall')>1)  then
                 begin
                     pagecontrol_ambulanz.activepage:=tabsheet_u_daten;
                     tab_name:='ambulanz';
                     nr:=ambulanz_neu(datum,1);
                   end;
              end;
           if ds='Diagnose' then
            begin
               if b.b_string('proband-diagnosen')<2 then exit;
               tab_name:='diagnosen';
              nr:=Diagnose_neu(datum);
              end;
           if ds='Besonderheit' then
            begin
             if b.b_string('proband-besonderheiten')<2 then exit;
                tab_name:='besonderheiten';
                 nr:=besonderheit_neu(datum);
              end;
           if ds='Dokument' then
            begin
                if (b.b_string_silent('proband-dokumente-arzt')<2) and ((b.b_string_silent('proband-dokumente-sani')<2)) then exit;
                application.ProcessMessages;
                dbtreeview.Enabled:=false;
                 tab_name:='dokumente';
                 nr:=text_neu(1,datum,1,false);
                dbtreeview.Enabled:=true;
              end;
           if ds='Rechnung' then
           begin
              //neue rechnung mit firma=-1 und i_ma
              //if nicht_satellit ('Rechnungen m�ssen auf dem Hauptsystem erstellt werden') then exit;  //wunsch nottelmann es spricht nichts dagegen.
              tab_name:='rechnung';
              nr:=rechnung_neu(datum);

           end;
           if ds='Artikel' then
           begin
              //neue rechnung mit firma=-1 und i_ma
              tab_name:='artikel';
              nr:=verkauf_neu(datum);
           end;

           if ds='Kontakte' then
           begin
              //neue rechnung mit firma=-1 und i_ma
              tab_name:='kontakte';
              nr:=kontakt_neu(datum);
           end;
           if ds='GDT' then
           begin
              //m_GDT_schreibenClick(self);
              tab_name:='gdt';
              nr:=gdt_erzeugen;
           end;


         finally
                kart_index:=form_u_wahl.RadioGroup.Itemindex;
                form_u_wahl.Release;
                form_u_wahl:=nil ;
         end;
       end;
 finally
    not_sperren:=false;
 end;

 if nr>0 then
 begin
	//if pagecontrol_main.ActivePage=tabsheet_main then  dbTreeViewChange(dbTreeView,dbtreeview.selected);   /////////////////muss weg  sonst doppelte Men�entr�ge
  formular_entsperren(false,true) ; //false false  //ole nicht anzeigen
  
  mysql_d.ds_neu_nr:=nr;
  mysql_d.ds_neu_tab:=tab_name;
  mysql_d.ds_aenderung:=true;


  	//toolbuttonloeschen.enabled:=false;    //bei neu kein l�schen
   if (pagecontrol_main.ActivePage=tabsheet_firma) and (pagecontrol_firma.activepage=tabsheet_firmenadresse)  then
      dbedit_firma.SetFocus;
	 if pagecontrol_main.ActivePage=tabsheet_stammdaten then dbcombobox_anrede.SetFocus;// dbedit_name.SetFocus;
 end;

end;



procedure TForm_main.loeschen;
var
knoten: ttreenode;
daten:pnodepointer;
ebene:integer;
s_nummer, query:string;
begin
 if modus_history then exit;

 if mysql_d.ds_neu_nr<>0 then
 begin
  mysql_d.loeschabfrage:=true;
  formular_sperren;   //l�schabfrage und delete
  exit;
 end
 else
 begin
 mysql_d.keine_abfrage:=true;
 formular_sperren;  // l�schabfrage und storno
 end;
 if pagecontrol_main.ActivePage=tabsheet_firma then
 begin
		if pagecontrol_firma.activepage=tabsheet_firmenadresse then
		begin
         if nicht_satellit('Firmen m�ssen auf dem Hauptsystem gel�scht werden') then exit;
			dateiloeschen(datamodul.q_Firma,false,false);
		end;
      if pagecontrol_firma.activepage=tabsheet_firma_filter then
		begin
			dateiloeschen(datamodul.q_Firma_filter,false,false);
		end;

		if pagecontrol_firma.activepage=tabsheet_ansprechpartner then
		dateiloeschen(datamodul.q_firma_ansprech,false,false);

    if (pagecontrol_firma.activepage=TabSheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung ) then
		dateiloeschen(DataModul.q_firma_zeiten_soll,false,false);

		if (pagecontrol_firma.activepage=tabsheet_sollist) then
		dateiloeschen(datamodul.q_Firma_sollist,false,false);

		if pagecontrol_firma.activepage=tabsheet_firma_texte then
		begin
		  dateiloeschen(datamodul.q_texte,false,false);
		end;

		if (pagecontrol_firma.activepage=tabsheet_rechnungen)  then
		begin
        //if nicht_satellit('Rechnungen m�ssen auf dem Hauptsystem gel�scht werden') then exit;      wunsch nottelmann
        if datamodul.q_rechnung.FindField('dat_verschickt').asstring<>'01.01.1900' then
        begin
           if messagedlg('M�chten Sie die verschickte Rechnungen wirklich l�schen?',mtconfirmation, [mbyes,mbno],0)=mrno then exit;
        end;

        if datamodul.q_rechnung.FindField('dat_bezahlt').asstring<>'01.01.1900' then
        begin
           if messagedlg('M�chten Sie die bezahlte Rechnungen wirklich l�schen?',mtconfirmation, [mbyes,mbno],0)=mrno then exit;
        end;

		  dateiloeschen(datamodul.q_rechnung,false,false);
		  datamodul.q_rechnungAfterScroll(datamodul.q_rechnung);
		end;

	end;
  if (pagecontrol_main.ActivePage=tabsheet_abteilung)  then
		begin
      if assigned(form_ap) then form_ap.a_delete();
    end;

	if pagecontrol_main.ActivePage=tabsheet_stammdaten then
	begin
		if dateiloeschen(datamodul.q_mitarbeiter,false,false) then
		begin
				datamodul.q_mitarbeiter_such.Refresh;
		end;
	end;

	if pagecontrol_main.ActivePage=tabsheet_main then
	begin
	  case dbtreeview.a_rubrik of
         1: begin
            knoten:=dbtreeview.selected;
            daten:=knoten.Data;
            ebene :=daten^.ebene;
            if ebene>1 then
               bereich_del
            else
              begin
              if MessageDlg('Sollen alle aktuellen Befunde zu diesem Datum wirklich gel�scht werden?', mtConfirmation	,[mbYes, mbNo], 0)=mryes  then
              begin
                  datamodul.q_befunde.Locate('nummer',(daten^.nummer),[]);
                  s_nummer:=getbigint_str(datamodul.q_befunde,'nummer');
                  query:=format('update akt_untersuchung set storno=1  where i_untersuchung=%s and storno=0 ',[s_nummer]);
                  mysql_d.sql_exe(query);
                  datamodul.q_akt_untersuchung.Refresh;


                 if dateiloeschen(datamodul.q_befunde,true,true) then
                 begin
                     if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
                 end
                 else showmessage ('Befund�bersicht kann nicht gel�scht werden, da noch Einzelbefunde vorhanden sind.');
              end;
              end;
            end;
         2: if dateiloeschen(datamodul.q_untersuchung,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected) ;
         3:	if dateiloeschen(datamodul.q_impfung,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
         4: if dateiloeschen(datamodul.q_labor,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);//tree_labor.loeschen;
         5: if dateiloeschen(datamodul.q_ambulanz,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
         6: if dateiloeschen(datamodul.q_diagnosen,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
         7: if dateiloeschen(datamodul.q_besonderheiten,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
         8: if dateiloeschen(datamodul.q_dokumente,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
         10:
      	begin
         	//if nicht_satellit('Rechnungen m�ssen auf dem Hauptsystem gel�scht werden') then exit;   wunsch nottelmann
         	if dateiloeschen(datamodul.q_rechnung,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
         end;
         11:if dateiloeschen(datamodul.q_artikel,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
        12:if dateiloeschen(datamodul.q_kontakte,false,false) then if storno_show then dbtreeview.statindex(9) else dbtreeview.knoten_entfernen(dbtreeview.selected);
	 end;
	end;

end;

procedure TForm_main.ToolButton_abbrechenClick(Sender: TObject);
begin
if MessageDlg('�nderungen verwerfen?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;

with datamodul do
begin


	if (q_Firma.state in [dsedit, dsinsert]) then
	begin
		direktspeichern:=true;
		q_Firma.RevertRecord;  ;
	end;
	if (q_Firma_ansprech.state in [dsedit, dsinsert]) then begin
		//cancel_loeschen(q_Firma_ansprech);
		direktspeichern:=true;
		q_firma_ansprech.RevertRecord;
	end;
	if (q_Firma_sollist.state in [dsedit, dsinsert])then begin
		//cancel_loeschen(q_firma_sollist);
		direktspeichern:=true;
		q_firma_sollist.RevertRecord;
	end;

  if (q_termin.state in [dsedit, dsinsert])then begin
		//cancel_loeschen(q_firma_sollist);
     direktspeichern:=true;
		q_termin.RevertRecord;
	end;

if (q_mitarbeiter.state in [dsedit, dsinsert]) then begin
		direktspeichern:=true;
		 q_mitarbeiter.RevertRecord;
		 //cancel_loeschen(q_mitarbeiter);

	end;
 if (q_diagnosen.state in [dsedit, dsinsert]) then
 begin
	 //cancel_loeschen(q_diagnosen);
	  direktspeichern:=true;
	 q_diagnosen.RevertRecord;
 end;

 if (q_besonderheiten.state in [dsedit, dsinsert]) then
 begin
		direktspeichern:=true;	 //cancel_loeschen(q_besonderheiten);
	 q_besonderheiten.RevertRecord;
 end;
	if (q_untersuchung.state in [dsedit, dsinsert])then
	begin
		direktspeichern:=true;		//cancel_loeschen(q_untersuchung);
		q_untersuchung.RevertRecord;
	end;


 if (q_impfung.state in [dsedit, dsinsert])then
 begin
		direktspeichern:=true;	 //cancel_loeschen(q_impfung);
	 q_impfung.RevertRecord;
 end;

 if (q_labor.state in [dsedit{, dsinsert}])  then
 begin
		direktspeichern:=true;	//cancel_loeschen(q_labor);
	q_labor.RevertRecord;
 end;


 if (q_ambulanz.state in [dsedit, dsinsert])  then
 begin
		direktspeichern:=true;	//cancel_loeschen(q_labor);
		q_ambulanz.RevertRecord;
 end;
 if (q_arbeitsunfall.state in [dsedit, dsinsert])  then
 begin
		direktspeichern:=true;	//cancel_loeschen(q_labor);
		q_arbeitsunfall.RevertRecord;
 end;

 if (q_dokumente.state in [dsedit, dsinsert])  then
 begin
		direktspeichern:=true;	//cancel_loeschen(q_labor);
		q_dokumente.RevertRecord;
 end;

 if (q_akt_untersuchung.state in [dsedit, dsinsert])  then //geht nicht da keine dbkomponenten

 begin
		direktspeichern:=true;	//cancel_loeschen(q_labor);
		q_akt_untersuchung.RevertRecord;
 end;

 if (q_texte.state in [dsedit, dsinsert])  then
 begin
		direktspeichern:=true;	//cancel_loeschen(q_labor);
		q_texte.RevertRecord;
 end;

 formular_sperren;
end;
end;

procedure tform_main.speichern;
begin
 dbspeichern(true);
end;


//############################################
procedure TForm_main.formular_sperren;
//############################################
begin
//dbspeichern(false); //17.01.2001
if not_sperren then exit;
//ist �berhaupt etwas offen?

with datamodul do
begin
if pagecontrol_main.ActivePage=tabsheet_firma then
begin
		if pagecontrol_firma.activepage=tabsheet_firmenadresse then
		begin
			p_sperren(q_firma,q_firma,user_id,[panel_firmen_adresse]);
      richedit_firma.PopupMenu:=nil;
		end;
    if pagecontrol_firma.activepage=tabsheet_firma_filter then
		begin
			p_sperren(q_firma,q_firma_filter,user_id,[panel_firma_filter]);
		end;

		if pagecontrol_firma.activepage=tabsheet_ansprechpartner then
		begin
			p_sperren(q_firma,q_firma_ansprech,user_id,[panel_ansprechpartner_re]);
      RichEdit_firma_ansprech.PopupMenu:=nil;
		end;

		if (pagecontrol_firma.activepage=tabsheet_sollist)then
		begin
			//p_sperren(q_firma,user_id,[tabsheet_sollist]);
        p_sperren(q_firma,q_firma_sollist, user_id,[panel_zeitabrechnung_re]);
        groupbox_zeit_filtereinstellungen.Enabled:=true;
        pm_zeit_verschieben.Visible:=false;
		end;



		if pagecontrol_firma.activepage=tabsheet_firma_texte then
		begin
			p_sperren(q_texte,q_texte,user_id,[panel_firma_text]); //tabsheet_firma_texte
		end;

		if pagecontrol_firma.activepage=tabsheet_rechnungen then
		begin
			p_sperren(q_firma,q_rechnung,user_id,[tabsheet_rechnungen]);
      m_f_Positionhinzufgen.Enabled:=false;
      m_f_Positionndern.Enabled:=false;
      m_f_Positionlschen.Enabled:=false;
      m_f_dauerhaftloesch.Enabled:=false;
		end;

    if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung ) then
		begin
			p_sperren(q_firma, q_firma,user_id,[tabsheet_firma_einstellungen_zeitabrechnung]);
      richedit_zeiten_soll.PopupMenu:=nil;
		end;

     if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung ) then
		begin
      if q_rechnung_regeln.State in [dsedit,dsinsert] then q_rechnung_regeln.post;
			p_sperren(q_firma,q_firma,user_id,[tabsheet_firma_einstellungen_rechnung]);
      richedit_revorgabe.PopupMenu:=nil;
		end;
	 end;

if (pagecontrol_main.ActivePage=tabsheet_abteilung)  then
		begin
      if assigned(form_ap) and assigned(form_ap.akt_tzquery) then
      begin
        //form_ap.a_change();
        p_sperren(form_ap.akt_tzquery,form_ap.akt_tzquery,user_id,[tabsheet_abteilung]);
        //p_sperren(q_firma,q_firma,user_id,[tabsheet_abteilung]);
      end;  
		end;

if (pagecontrol_main.ActivePage=tabsheet_stammdaten)  then
		begin
			p_sperren(q_mitarbeiter,q_mitarbeiter,user_id,[pagecontrol_stammdaten]);
      richedit_proband.PopupMenu:=nil;
		end;

if (pagecontrol_main.ActivePage=tabsheet_main)then
	begin
	  //if b_gesperrt[dbtreeview.a_rubrik] then exit;
		case dbtreeview.a_rubrik of
			1: begin
				  p_sperren( q_mitarbeiter,q_befunde,user_id,[panel_befunde_datum,pagecontrol_befunde, panel_bef_auff, notebook_befund,groupbox_bef_abr]); //pagecontrol_befunde,richedit_befund,ole_befund,
				  //if not (ole_befund.state in [osempty]) then ole_befund.DestroyObject; ########
          richedit_befund.PopupMenu:=nil;
				end;
			2: p_sperren(q_mitarbeiter,q_untersuchung,user_id,[panel_untersuchung]);
			3:	p_sperren(q_mitarbeiter,q_impfung,user_id,[panel_impfung]);
			4: p_sperren(q_mitarbeiter,q_labor,user_id,[panel_labor]);
			5: p_sperren(q_mitarbeiter,q_ambulanz,user_id,[panel_ambulanz]);
			6: p_sperren(q_mitarbeiter,q_diagnosen,user_id,[groupbox_diagnosen]);
			7: p_sperren(q_mitarbeiter,q_besonderheiten,user_id,[groupbox_besonderheiten]);
			8: p_sperren(q_mitarbeiter,q_dokumente,user_id,[panel_texte]);
			10:begin
          p_sperren(q_mitarbeiter,q_rechnung,user_id,[panel_rechnung_ma]);
          m_p_Positionhinzufgen.Enabled:=false;
          m_p_Positionndern.Enabled:=false;
          m_p_Positionlschen.Enabled:=false;
          m_p_Positiondauerhaftentfernen.Enabled:=false;
         end;
			11:p_sperren(q_mitarbeiter,q_rechnung,user_id,[panel_verkauf]);
      12:p_sperren(q_mitarbeiter,q_kontakte,user_id,[panel_kontakte]);
      15:p_sperren(q_mitarbeiter,q_mitarbeiter,user_id,[panel_anamnese,toolbar_anamnese]);
		end;
	 end;
	//tool_aktiv
  application.processmessages;
	toolbuttonedit.enabled:=true;
	menue_edit.Enabled:=true;
	toolbuttonsichern.Enabled:=false;
  toolbutton_abort.enabled:=false;
	menue_save.Enabled:=false;
	menue_delete.Enabled:=false;
	ToolButtonloeschen.Enabled:=false;
	//toolbutton_abbrechen.Enabled:=false;
end;
end;


//##########################################################################
function tform_main.formular_entsperren(refresh,nicht_ole:boolean):boolean;   // �ffnen zum schreiben
//##########################################################################
var
sqll,ds_nr:string;
begin
result:=false;
//dbspeichern(true);  //warum?
//if modus_history then exit;
with datamodul do
begin
if pagecontrol_main.ActivePage=tabsheet_firma then
begin
	 if pagecontrol_firma.activepage=tabsheet_firmenadresse then
		begin
      if not b.b_storno_ok(q_firma) then exit;
			if b.b_string('firma-stammdaten')<1 then exit;
			if  (q_firma['nummer']<>null) then
			result:=p_entsperren(q_firma,q_firma,user_id,[tabsheet_firmenadresse],refresh);
      //richedit_firma.PopupMenu:=popupmenu_richedit;
		end;
   if pagecontrol_firma.activepage=tabsheet_firma_filter then
		begin
      if not b.b_storno_ok(q_firma) then exit;
			if b.b_string('firma-stammdaten')<1 then exit;
			if   (datensatz(q_firma_filter)) then
			result:=p_entsperren(q_firma,q_firma_filter,user_id,[tabsheet_firma_filter],refresh);
		end;

   if pagecontrol_firma.activepage=tabsheet_ansprechpartner then
		begin
        if not b.b_storno_ok(q_firma_ansprech) then exit;
			if b.b_string('firma-ansprechpartner')<1 then exit;
			if  (datensatz(q_firma_ansprech)) then
			result:=p_entsperren(q_firma,q_firma_ansprech,user_id,[tabsheet_ansprechpartner],refresh);
      RichEdit_firma_ansprech.PopupMenu:=popupmenu_richedit;;
		end;

	if (pagecontrol_firma.activepage=tabsheet_sollist)then
		begin
			if b.b_string('firma-zeitabrechnung')<1 then exit;

      if not sperrdatum_ok(datamodul.q_firma_sollist.findfield('datum').AsFloat,2) then exit;
			if   (datensatz(q_firma_sollist)) then
        begin
          groupbox_zeit_filtereinstellungen.Enabled:=false;
          result:=p_entsperren( q_firma,q_firma_sollist,user_id,[panel_zeitabrechnung_re],refresh);
        end;
        pm_zeit_verschieben.Visible:=true;
		end;

    if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung )then
		begin
			if b.b_string('firma-zeitabrechnung')<1 then exit;
      if nicht_satellit_tab then exit;
			if   (datensatz(q_firma_zeiten_soll)) then
			result:=p_entsperren( q_firma,q_firma,user_id,[tabsheet_firma_einstellungen_zeitabrechnung],refresh);
      richedit_zeiten_soll.PopupMenu:=popupmenu_richedit;
		end;

		if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung ) then
		begin
			result:=p_entsperren(q_firma,q_firma,user_id,[tabsheet_firma_einstellungen_rechnung],refresh);
      q_rechnung_regeln.Edit;
      richedit_revorgabe.PopupMenu:=popupmenu_richedit;
		end;
    if pagecontrol_firma.activepage=tabsheet_rechnungen then
		begin

				if   datensatz(q_rechnung) then
            begin

               ds_nr:=q_repositionen.findfield('nummer').asstring;
            	if refresh then q_repositionen.Refresh;
            	result:=p_entsperren(q_firma,q_rechnung,user_id,[tabsheet_rechnungen],refresh);
               q_repositionen.locate('nummer',ds_nr,[]);
               //pm_rechnung_rechts.
               m_f_Positionhinzufgen.Enabled:=true;
               m_f_Positionndern.Enabled:=true;
               m_f_Positionlschen.Enabled:=true;
               m_f_dauerhaftloesch.Enabled:=true;
            end;
		end;
		if pagecontrol_firma.activepage=tabsheet_firma_texte then
		begin
			if b.b_string('firma-dokumente')<1 then exit;
			if   (datensatz(q_texte)) then
			begin
        //refresh:=false;//20100515 eigentlich nicht n�tig und  dadurch wird das ole-lesen ausgel�st
			  result:=p_entsperren(q_texte,q_texte,user_id,[panel_firma_text],false);
			  if (not nicht_ole) and result then arbeitsmedizin_FirmaLButtonDown(nil);
			end;
		end;

end;

   if (pagecontrol_main.ActivePage=tabsheet_abteilung)  then
		begin
      if assigned(form_ap) then
      begin
         //form_ap.a_change();
			  //result:=p_entsperren(q_firma,q_firma,user_id,[tabsheet_abteilung],false);
        //result:=p_entsperren(form_ap.akt_tzquery,form_ap.akt_tzquery,user_id,[tabsheet_abteilung],false);
        result:=p_entsperren(form_ap.akt_tzquery,form_ap.akt_tzquery,user_id,[form_ap.notebook],false);
      end;
    end;


	if (pagecontrol_main.ActivePage=tabsheet_stammdaten)  then
		begin
      if not b.b_storno_ok(q_mitarbeiter) then exit;
			if b.b_string('proband-stammdaten')<1 then exit;
			if   (datensatz(datamodul.q_mitarbeiter)) then
			result:=p_entsperren(q_mitarbeiter,q_mitarbeiter,user_id,[pagecontrol_stammdaten],refresh);
      namenanzeigen;
      richedit_proband.PopupMenu:=popupmenu_richedit;
		end;


 if result then //tabsheet_main wird extra behandelt   Karteibaum
 begin
    if  (pagecontrol_firma.ActivePage=TabSheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung ) then //labor
		 begin
        menue_delete.Enabled:=false;
			  ToolButtonloeschen.Enabled:=false;
		 end
     else
     begin
        menue_save.Enabled:=true;
        menue_delete.Enabled:=true;
        ToolButtonloeschen.Enabled:=true;
     end;
     toolbuttonedit.enabled:=false;
     toolbuttonsichern.Enabled:=true;
     if not ((pagecontrol_main.ActivePage=tabsheet_firma) and ((pagecontrol_firma.ActivePage=TabSheet_firma_einstellungen) or  (pagecontrol_firma.ActivePage=TabSheet_rechnungen)
              or(pagecontrol_firma.ActivePage=TabSheet_firma_filter) or (pagecontrol_firma.ActivePage=TabSheet_firma_einstellungen))
              or (pagecontrol_main.ActivePage=TabSheet_abteilung)   )
        then  toolbutton_abort.enabled:=true;
	  exit;
 end;

if pagecontrol_main.ActivePage=tabsheet_main then
	begin
		if prg_typ=3 then
		  pagecontrol_befunde.ActivePage:=tabsheet_be_doku
		else
		  pagecontrol_befunde.ActivePage:=tabsheet_be_anmerkung;

    dbtreeview.tag:=0;
		case dbtreeview.a_rubrik of
			1:
				begin
					if not b.b_archiv_ok(q_befunde) then exit;
					if not b.b_alter_ok(q_befunde) then exit;
          if not b.b_storno_ok(q_befunde) then exit;
					if b.b_string('proband-befunde')<1 then
          begin
            showmessage('Sie haben hierzu keine Berechtigung');
            exit;
          end;
          if not sperrdatum_ok(datamodul.q_befunde.findfield('datum').AsFloat,2) then exit;
          //###speichern### ausnahme
					result:=p_entsperren(q_mitarbeiter,q_befunde,user_id,[panel_befunde_datum,pagecontrol_befunde,panel_bef_auff,notebook_befund,groupbox_bef_abr],false);
          richedit_befund.PopupMenu:=popupmenu_richedit;
          dbtreeview.tag:=1;
				end;
			2: begin
					if not b.b_archiv_ok(q_untersuchung) then exit;
					if not b.b_alter_ok(q_untersuchung) then exit;
                if not b.b_storno_ok(q_untersuchung) then exit;
                if b.b_string_silent('Proband-Bescheinigung')>=0 then
                 if  (b.b_string('Proband-Bescheinigung')<1)   then
                 begin
                  showmessage('Sie haben hierzu keine Berechtigung');
                  exit;
                 end;

                 if b.b_string_silent('Proband-Bescheinigung-AG')>=0 then
                 if  (b.b_string('Proband-Bescheinigung-AG')<1)   then
                 begin
                  showmessage('Sie haben hierzu keine Berechtigung');
                  exit;
                 end;

                //if (b.b_string('Proband-Bescheinigung')<1)  and (b.b_string('Proband-Bescheinigung')<1) then exit;

                if not sperrdatum_ok(datamodul.q_untersuchung.findfield('datum').AsFloat,2) then exit;

					sqll:= q_untersuchung.sql.text+' ';
					result:=p_entsperren(q_mitarbeiter,q_untersuchung,user_id,[panel_untersuchung],false);
          dbtreeview.tag:=2;
				end;
			3:	begin
					if not b.b_archiv_ok(q_impfung) then exit;
					if not b.b_alter_ok(q_impfung) then exit;
          if not b.b_storno_ok(q_impfung) then exit;
					if b.b_string('proband-impfung')<1 then
          begin
                  showmessage('Sie haben hierzu keine Berechtigung');
                  exit;
          end;
          if not sperrdatum_ok(datamodul.q_impfung.findfield('datum').AsFloat,2) then exit;
					sqll:= q_untersuchung.sql.text+' ';
					result:=p_entsperren(q_mitarbeiter,q_impfung,user_id,[panel_impfung],false);
				end;
			4: begin
					if not b.b_archiv_ok(q_labor) then exit;
					if not b.b_alter_ok(q_labor) then exit;
          if not b.b_storno_ok(q_labor) then exit;
					if b.b_string('proband-labor')<1 then
          begin
                  showmessage('Sie haben hierzu keine Berechtigung');
                  exit;
          end;
          if not sperrdatum_ok(datamodul.q_labor.findfield('datum').AsFloat,2) then exit;
          //###speichern### ausnahme
					result:=p_entsperren(q_mitarbeiter,q_labor,user_id,[panel_labor],false);
          dbtreeview.Tag:=4;
				end;
			5: begin
				  if not b.b_archiv_ok(q_ambulanz) then exit;
				  if not b.b_alter_ok(q_ambulanz) then exit;
          if not b.b_storno_ok(q_ambulanz) then exit;
          if not sperrdatum_ok(datamodul.q_ambulanz.findfield('datum').AsFloat,2) then exit;
				  if (b.b_string_silent('proband-ambulanz')>-1) or (b.b_string_silent('proband-arbeitsunfall')>-1) then
					 result:=p_entsperren(q_mitarbeiter,q_ambulanz,user_id,[panel_ambulanz],false)
					else showmessage('Keine Berechtigung') ;
				end;
			6: begin
					if not b.b_archiv_ok(q_diagnosen) then exit;
					if not b.b_alter_ok(q_diagnosen) then exit;
          if not b.b_storno_ok(q_diagnosen) then exit;
					if b.b_string('proband-diagnosen')<1 then
          begin
                  showmessage('Sie haben hierzu keine Berechtigung');
                  exit;
          end;
          if not sperrdatum_ok(datamodul.q_diagnosen.findfield('datum').AsFloat,2) then exit;
					result:=p_entsperren(q_mitarbeiter,q_diagnosen,user_id,[groupbox_diagnosen],false);
				 end;
			7: begin
					if not b.b_archiv_ok(q_besonderheiten) then exit;
          if not b.b_alter_ok(q_besonderheiten) then exit;
          if not b.b_storno_ok(q_besonderheiten) then exit;
					if b.b_string('proband-besonderheiten')<1 then
          begin
                  showmessage('Sie haben hierzu keine Berechtigung');
                  exit;
          end;
          if not sperrdatum_ok(datamodul.q_besonderheiten.findfield('datum').AsFloat,2) then exit;
					result:=p_entsperren(q_mitarbeiter,q_besonderheiten,user_id,[groupbox_besonderheiten],false);
          dbtreeview.Tag:=7;
				end;
			8: begin
					if not b.b_archiv_ok(q_dokumente) then exit;
					//if not b.b_alter_ok(q_dokumente) then exit; Dokumente k�nnen sonst nicht angezeigt werden
					if (b.b_string_silent('proband-dokumente-arzt')>=1) or (b.b_string_silent('proband-dokumente-sani')>=1) then
          begin
            if sperrdatum_ok(datamodul.q_dokumente.findfield('datum').AsFloat,2) then
            begin
              result:=p_entsperren(q_mitarbeiter,q_dokumente,user_id,[panel_texte],false);
              if (not nicht_ole) and result then arbeitsmedizin_ProbandLButtonDown(nil); //20121222
            end
            else
            begin

              arbeitsmedizin_ProbandLButtonDown(nil); //20121222
            end;
          end
					else showmessage('Sie haben keine Berechtigung');
				end;
			10:begin
        if not b.b_archiv_ok(q_rechnung) then exit;
					result:=p_entsperren(q_mitarbeiter,q_rechnung,user_id,[panel_rechnung_ma],false);
          m_p_Positionhinzufgen.Enabled:=true;
          m_p_Positionndern.Enabled:=true;
          m_p_Positionlschen.Enabled:=true;
          m_p_Positiondauerhaftentfernen.Enabled:=true;
				end;
			11:begin
              if not b.b_archiv_ok(q_rechnung) then exit;
					result:=p_entsperren(q_mitarbeiter,q_rechnung,user_id,[panel_verkauf],false);
				end;
        12:begin
					result:=p_entsperren(q_mitarbeiter,q_kontakte,user_id,[panel_kontakte],false);
				end;
        15: begin
              if (b.b_string_silent('proband-anamnese-arzt')<1) {and(b.b_string_silent('proband-anamnese-sani')<1)} then exit;
              if   (datensatz(q_mitarbeiter)) then
              result:=p_entsperren(q_mitarbeiter,q_mitarbeiter,user_id,[panel_anamnese,toolbar_anamnese],refresh);

            end;

		end;
		if result then
		begin
           if  ((dbtreeview.a_rubrik=4) and (dbtreeview.a_ebene<=1))   then //labor
           begin
              menue_delete.Enabled:=false;
              ToolButtonloeschen.Enabled:=false;
              menue_save.Enabled:=true;
              toolbuttonsichern.Enabled:=true;
              //toolbutton_abort.enabled:=true;
           end
           else
           begin
             //b_gesperrt[dbtreeview.a_rubrik]:=false;
             toolbuttonsichern.Enabled:=true;

             menue_save.Enabled:=true;
             toolbuttonedit.enabled:=false;
             menue_edit.Enabled:=false;
             menue_delete.Enabled:=true;
             ToolButtonloeschen.Enabled:=true;
             if not( (dbtreeview.a_rubrik=1) or (dbtreeview.a_rubrik=4) or (dbtreeview.a_rubrik=8)) then toolbutton_abort.enabled:=true;
             //if  (dbtreeview.a_rubrik=1) and (dbtreeview.a_ebene>1)  then menue_delete.Enabled:=false;
          end;

		end;


    if hist_sperren then
    begin
            toolbuttonsichern.Enabled:=false;
            toolbutton_abort.enabled:=false;
             menue_save.Enabled:=false;
             toolbuttonedit.enabled:=true;
             menue_edit.Enabled:=true;
             menue_delete.Enabled:=false;
             ToolButtonloeschen.Enabled:=false;
    end;


	end;


end;
end;


//################################################################
//hier darf kein refresh drin stehen sonst fehler stop_transaction

procedure TForm_main.dbspeichern(direkt:boolean);
var
	i_stammdaten,i_nummer:int64;
  s_nummer:string;
	rubrik:integer;
	daten:pnodepointer;
  bm:tbookmark;
begin
if speichert_gerade or laed_gerade then exit;
menue_deaktivieren();
direktspeichern:=direkt;
application.processmessages;
//arbene_geladen:=false;
try
speichert_gerade:=true;   //auswirkung auf pagecontrol_main_changing  , dbtreeview_changing


if modus_history then
begin
  //history_entfernen;
  exit;
end;



with datamodul do
begin
if pagecontrol_main.ActivePage=tabsheet_firma then
begin
		if pagecontrol_firma.activepage=tabsheet_firmenadresse then
		begin
			if sql_post(q_firma)then
         begin
            //arbene_geladen:=false;
            i_nummer:=q_firma.findfield('nummer').asinteger;
            //arbene_geladen:=true;
         end;

		end;
		if pagecontrol_firma.activepage=tabsheet_ansprechpartner then
		begin
			sql_post(q_firma_ansprech);
		end;

		if (pagecontrol_firma.activepage=tabsheet_sollist)then
		begin
            pm_zeit_verschieben.Visible:=false;
            if ds_firma_sollist.State in [dsedit, dsinsert] then
            begin

               if dbedit_stunden.text='' then  dbedit_stunden.text:='0';
               if dbedit_minuten.text='' then  dbedit_minuten.text:='0';
               if dbcombobox_zeitansatz.Text='' then  dbcombobox_zeitansatz.Text:='0';
               if ((dbedit_stunden.Text='0') and (dbedit_minuten.Text='0') ) or (dbmemo_sollist_anlass.Text='') then messagedlg('Achtung, es wurden noch keine Zeiten oder kein Anlass eingetragen',mtWarning ,[mbOK ],0);
               if q_firma_sollist.FindField('i_zeitkonto').AsInteger<=0 then showmessage('Achtung, kein Zeitkonto ausgew�hlt');
               if q_firma_sollist.FindField('i_kriterium').AsInteger<=0 then showmessage('Achtung, kein Kriterium ausgew�hlt');
               application.processmessages;
              sql_post(q_firma_sollist);
              //DBgridEXT_zeitabrechnung.Refresh;
              s_nummer:= datamodul.q_firma_sollist.findfield('nummer').asstring;
              datamodul.q_firma_sollist.Refresh;  //anzeige aufgabenfeld
              einsatzzeiten_filtern;
              datamodul.q_firma_sollist.Locate('nummer',s_nummer,[])
            end;

			//PageControl_firmaChange(self); stack-overflow
		end;

    if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung ) then
		begin
			sql_post(q_firma_zeiten_soll);
          //  q_firma_zeiten_soll.Refresh;
			//PageControl_firmaChange(self); stack-overflow
		end;

		if pagecontrol_firma.activepage=tabsheet_firma_texte then
		begin
      olecontainer_close(arbeitsmedizin_Firma);
      pause(100);
      mysql_d.ds_aenderung:=arbeitsmedizin_Firma.Modified or mysql_d.ds_aenderung;
			sql_memo_post(q_texte,arbeitsmedizin_Firma);
      olecontainer_close(arbeitsmedizin_Firma);
       pause(100);
       //if word_close then com.word_schliessen;
      //mysql_d.ds_aenderung:=true;//arbeitsmedizin_Firma.Modified;
      //wenn word dann

		end;
		if pagecontrol_firma.activepage=TabSheet_rechnungen then
		begin
			sql_post(q_firma);
			sql_post(q_rechnung);
		end;
    if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung )  then
    begin
      sql_post(q_firma);
    end;
	 end;

if (pagecontrol_main.ActivePage=tabsheet_abteilung)  then
		begin
       if assigned(form_ap) then form_ap.a_save();
    end;


if (pagecontrol_main.ActivePage=tabsheet_stammdaten)  then
		begin
			if sql_post(q_mitarbeiter) then
			begin
				ma_such_neu:=true;
				//q_mitarbeiter_such.Refresh;
			end;
		end;


if pagecontrol_main.ActivePage=tabsheet_main then
	try
   dbtreeview.change_allowed:=false;
	  case dbtreeview.a_rubrik of
		1: begin
				bereich_speichern;
				sql_post(q_befunde);
			end;
		2: begin
            //if q_untersuchung['i_untersucher']=0 then q_untersuchung['i_untersucher']:=akt_untersucher;
        	sql_post(q_untersuchung);
           end;
		3:	sql_post(q_impfung);
		4: sql_post(q_labor);
		5: begin
				sql_post(q_arbeitsunfall);
				datamodul.gridschreiben(form_main.grid_ambulanz,datamodul.q_ambulanz);
				sql_post(q_ambulanz);
			end;
		6:sql_post(q_diagnosen);
		7:sql_post(q_besonderheiten);
		8:begin
              //if  not b.b_alter_ok(q_dokumente,Arbene_Mitarbeiter) then exit;
              if  not b.b_storno_ok(q_dokumente) then exit;
              if not sperrdatum_ok(datamodul.q_dokumente.findfield('datum').AsFloat,0) then exit;
              //if arbeitsmedizin_Proband.OleClassName<>'' then DataModul.q_dokumente.findfield('titel').asstring:= arbeitsmedizin_Proband.OleClassName;
              olecontainer_close(arbeitsmedizin_Proband);
              pause(100);
              mysql_d.ds_aenderung:=arbeitsmedizin_proband.Modified or mysql_d.ds_aenderung;
              sql_memo_post(q_dokumente,arbeitsmedizin_Proband);
              olecontainer_close(arbeitsmedizin_Proband);
              pause(100);
              //if word_close then com.word_schliessen;
              //olecontainer_destroy(arbeitsmedizin_Proband);
              //olecontainer_laden(q_dokumente,'ole',form_main.arbeitsmedizin_Proband);
       end;
     10: sql_post(q_rechnung);
     11: sql_post(q_artikel);
     12: sql_post(q_kontakte);
     15: anamnese_speichern(0);
	  end;


	  if status_neu then
		begin
			status_neu:=false;
			case dbtreeview.a_rubrik of
			  2:begin
              if dbtreeview.Selected<>nil then
              begin
					dbtreeview.Selected.ImageIndex:=q_untersuchung['i_status'];
					dbtreeview.Selected.SelectedIndex:=dbtreeview.Selected.ImageIndex;
              end;
				 end;

			  3:BEGIN
              if dbtreeview.Selected<>nil then
              begin
					dbtreeview.Selected.ImageIndex:=q_impfung['i_status'];
					dbtreeview.Selected.SelectedIndex:=dbtreeview.Selected.ImageIndex;
              end;
				 end;
			  4:BEGIN
              if dbtreeview.Selected<>nil then
              begin
					dbtreeview.Selected.ImageIndex:=q_labor['i_status'];
					dbtreeview.Selected.SelectedIndex:=dbtreeview.Selected.ImageIndex;
              end;
				 end;
			 end;
			 dbtreeview.repaint;
	      end;
    finally
    //alle_olecontainer_schliessen;     100924
    dbtreeview.change_allowed:=true;

    end;



    //##############  hier auch transaktionsbehandlung
    mysql_d.keine_abfrage:=direkt;
    formular_sperren;   //evtl. besser direkt weiterzugeben bis stop_transaction
       //arbene_geladen:=true;
    //#######################################################
    //ab hier refresh m�glich  nur n�tig wenn delete

   if ((pagecontrol_main.ActivePage=tabsheet_firma) and mysql_d.ds_geloescht) then   //nur die unter dem Firmenreiter
    begin
      mysql_d.ds_geloescht:=false;

      if pagecontrol_firma.activepage=tabsheet_firmenadresse then
      begin

      end;
      if pagecontrol_firma.activepage=tabsheet_ansprechpartner then
      begin
      end;

      if (pagecontrol_firma.activepage=tabsheet_sollist)then
      begin
        q_firma_sollist.refresh;
        //dbgridext_zeitabrechnung.Refresh;

      end;

      if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung ) then
      begin

      end;

      if pagecontrol_firma.activepage=tabsheet_firma_texte then
      begin
        q_texte.refresh;  // nur bei delete n�tig 08.07.2017
      end;
      if pagecontrol_firma.activepage=TabSheet_rechnungen then
      begin

      end;
      if (pagecontrol_firma.activepage=tabsheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung )  then
      begin
      end;
	 end;

    if (pagecontrol_main.ActivePage=tabsheet_abteilung)  then
		begin
    end;
    if (pagecontrol_main.ActivePage=tabsheet_stammdaten)  then
		begin
		end;



    // ende refresh
    end;
finally
  speichert_gerade:=false;
  menue_aktivieren();
end;
end;



procedure TForm_main.m_TerminuebersichtClick(Sender: TObject);
begin
 ToolButton_termineClick(Sender);
end;

procedure TForm_main.BitBtn_bes_auswahlClick(Sender: TObject);
begin
	datamodul. sql_new(true,datamodul.q_besonderheitenListe_tree,'select * from besonderheitenliste', 'reihenfolge');
	form_besonderheiten:=tform_besonderheiten.Create(self);
   form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
	form_besonderheiten.tree.DragMode:=dmmanual;
	form_besonderheiten.multiselect:=false;
	form_besonderheiten.ToolBar.Visible:=false;
	form_besonderheiten.Panel_bottom.Visible:=true;
	if form_besonderheiten.showmodal=mrok then
	begin
	  datamodul.q_besonderheiten.edit;
	  datamodul.q_besonderheiten['i_besonderheit']:=datamodul.q_besonderheitenListe_tree['nummer'];
	  datamodul.q_besonderheiten.post;
	end;

	form_besonderheiten.release;
   kartei_aktualisieren;
end;


procedure tform_main.bereich_change(storno:boolean);
var
i,j,bernum:integer;
nummer:int64;
wort:string;
fund:boolean;
tab:tzquery ;
begin
	  if storno then
	  begin
		tab:=datamodul.q_akt_untersuchung_storno;
		datamodul.q_bereich.Locate( 'nummer',tab['i_bereich'],[]);
	  end
	  else tab:=datamodul.q_akt_untersuchung;

	  if (datamodul.q_bereich.active=false) or (dbtreeview.Items.count<1 ) then exit;
	  richedit_befund.text:='';
	  richedit_befund.Modified:=false;

	 bernum:=datamodul.q_bereich['nummer'];
	 datamodul.q_berlist.filtered:=false;
	 datamodul.q_berlist.filter:='i_bereich = '+inttostr(bernum);
	 datamodul.q_berlist.filtered:=true;
	 radiogroup_parameter.Items.Clear;

	if prg_typ=3 then
		pagecontrol_befunde.ActivePage:=tabsheet_be_doku
	else
		pagecontrol_befunde.ActivePage:=tabsheet_be_anmerkung;

	radiogroup_auffaellig.Caption:=datamodul.q_bereich['name'];
  //radiogroup_auffaellig.ItemIndex:=-1;
	case datamodul.q_bereich['modus'] of
			0:try
				notebook_befund.PageIndex:=0;
				notebook_befund.Height:=0;
				tabsheet_be_graph.TabVisible:=false;
				except
				end;
			1:	try  //Zahl
					notebook_befund.Height:=150;
					notebook_befund.PageIndex:=1;
					if varisnull(datamodul.q_bereich['bezeichner']) then
					label_bezeichner.Caption:=''
					else    label_bezeichner.Caption:=(datamodul.q_bereich['bezeichner']);

					if varisnull(datamodul.q_bereich['bezeichner_dim']) then
					label_bezeichner_dim.Caption:=''
					else label_bezeichner_dim.Caption:=(datamodul.q_bereich['bezeichner_dim']);

					radiogroup_parameter.Items.Add(label_bezeichner.Caption+' '+label_bezeichner_dim.Caption);
					radiogroup_parameter.Columns:=radiogroup_parameter.Items.Count;
					tabsheet_be_graph.TabVisible:=true;
				  if edit_bef.enabled then edit_bef.SetFocus;
				except
				end;
			2:try  //liste
					notebook_befund.Height:=150;
					notebook_befund.PageIndex:=3;
					combodeaktivieren(combobox);
          combodeaktivieren(combobox_rechts);
					tabsheet_be_graph.TabVisible:=false;

					combolesen_q(combobox,datamodul.q_berlist,'listenfeld','keine Auswahl');
          combolesen_q(combobox_rechts,datamodul.q_berlist,'listenfeld','keine Auswahl');

					if combobox.enabled then combobox.SetFocus;

				except
				end;
			 3: try    //audiogramm
				notebook_befund.Height:=groupbox_unt_akt.height-180;
				notebook_befund.pageindex:=7;
				pagecontrol_st.activepage:=tabsheet_st_weber;
				tabsheet_be_graph.TabVisible:=false;
				except
				end;
			 4: try //sehtest

				notebook_befund.Height:=200;
				notebook_befund.pageindex:=8;
				tabsheet_be_graph.TabVisible:=false;
        grid_sehtest.DefaultColWidth:=(grid_sehtest.Width-10) div  7;
        grid_sehtest.DefaultRowHeight:=(grid_sehtest.Height-12) div 4;
				if grid_sehtest.enabled then grid_sehtest.SetFocus;
				except
				end;
			  5:try
				  notebook_befund.Height:=150;
				  notebook_befund.pageindex:=5;
				  tabsheet_be_graph.TabVisible:=true;
				  radiogroup_parameter.Items.Add('systolisch');
					radiogroup_parameter.Items.Add('diastolisch');
					radiogroup_parameter.Items.Add('Puls');
					radiogroup_parameter.Columns:=radiogroup_parameter.Items.Count;
				  if spinedit_sy.enabled then spinedit_sy.SetFocus;
				 except
				 end;
			  6: try //gr�sse gewicht
					notebook_befund.Height:=150;
					notebook_befund.PageIndex:=4;
					tabsheet_be_graph.TabVisible:=true;
					radiogroup_parameter.Items.Add('Gr�sse');
					radiogroup_parameter.Items.Add('Gewicht');
					radiogroup_parameter.Items.Add('BMI');
					radiogroup_parameter.Columns:=radiogroup_parameter.Items.Count;
					if edit_groesse.enabled then edit_groesse.SetFocus;
				  except
				  end;
			  7: try //lufu
					 notebook_befund.Height:=150;
					 notebook_befund.PageIndex:=6;
					 tabsheet_be_graph.TabVisible:=true;
					 radiogroup_parameter.Items.Add('FEV1');
					 radiogroup_parameter.Items.Add('FVC');
					 radiogroup_parameter.Items.Add('FEV1/FVC');
					 radiogroup_parameter.Items.Add('PEV');
					 radiogroup_parameter.Columns:=radiogroup_parameter.Items.Count;

					 if edit_fev1.enabled then edit_fev1.SetFocus;
				  except
				  end;
           8: try //wai
                  notebook_befund.Height:=groupbox_unt_akt.Height-radiogroup_auffaellig.Height-groupbox_bef_abr.Height-15;
                  notebook_befund.PageIndex:=9;

              except
              end;

		 end;
		 //end;

		 //f�r alle gleich:

     if modus_history then
     begin
       fund:= datensatz(tab);
     end
     else
     begin
     //wenn history dann wird manuell ausgew�hlt
       if storno then fund:=true
       else fund:=tab.locate('i_bereich',bernum,[]);
     end;

	 if fund then
		 try // schon angelegt
			 try

			 radiogroup_auffaellig.ItemIndex:=tab['auffaellig'];
       richeditladen(tab,'memo',richedit_befund);
			 //strings_laden(tab,'memo_blob',richedit_befund.Lines);
       //if richedit_befund.Text='' then strings_laden(tab,'memo',richedit_befund.Lines);
       //if richedit_befund.Text='' then strings_laden(datamodul.q_bereich,'memo',richedit_befund.Lines);  //vorgaben
        if richedit_befund.Text='' then richeditladen(datamodul.q_bereich,'memo',richedit_befund);
       //memo1.text:=strings_laden_txt(tab,'memo');
			 if  olecontainer_laden(tab,'ole',arbeitsmedizin_befund)
             then Tabsheet_be_doku.Caption:='**Word-Dokument**'
             ELSE Tabsheet_be_doku.Caption:='leeres Word-Dokument';


        //memo1.Text:=tab.findfield('memo').AsString;
			  //vordefiniertern text laden

			 except
			 end;
			  edit_b_goae.text:=trim(tab['goae']);
              edit_waehrung_bef.Text:=floattostrf(tab['euro'],ffFixed,8,2);
			  //maskedit_b_faktor.editText:=floattostrf(tab['faktor'],ffFixed,4,2);
			  checkbox_b_abr.checked:=(tab['abrechnen']=1);
              checkbox_bef_ust.checked:=(tab['ust']=1);
			  //BitBtn_bef_del.enabled:=form_main.radiogroup_auffaellig.enabled ;
		  except
		  end
		  else
		  try
          Tabsheet_be_doku.Caption:='leeres Word-Dokument';
          edit_bef.text:='0';
			  combobox.ItemIndex:=0;
			  combobox_rechts.itemindex:=0;
			 radiogroup_auffaellig.ItemIndex:=-1;
			 checkbox_b_abr.Checked:=abrechnen_befund;
             //checkbox_bef_ust.checked:=abrechnen_befund_ust;
			 //strings_laden(datamodul.q_bereich,'memo',richedit_befund.Lines);   #######06.03.2017
       richeditladen(datamodul.q_bereich,'memo',richedit_befund);
			 if not (arbeitsmedizin_befund.state in [osempty]) then arbeitsmedizin_befund.DestroyObject;

			 edit_b_goae.text:=datamodul.q_bereich['goae'];
         edit_waehrung_bef.text:=floattostrf(datamodul.q_bereich['euro'],ffFixed,8,2);
			 //maskedit_b_faktor.editText:=floattostrf(datamodul.q_firma['fa_bef'],ffFixed,4,2);

			 //bitBtn_bef_del.enabled:=false;
		  except
		  end;



		case datamodul.q_bereich['modus'] of

		0,1,2://zahk, liste
		try
		  if modus_history or tab.locate('i_bereich',bernum,[]) then
		  begin //untersuchung schon angelegt
				edit_bef.text:=floattostr(tab['wert']);
				combogehezu(combobox,tab['i_berlist']);
				combogehezu(combobox_rechts,tab['i_berlist_rechts']);
		  end
		  else //noch keine Untersuchung
		  begin
           //weiter oben
				//edit_bef.text:='0';
				//combobox.ItemIndex:=0;
				//combobox_rechts.itemindex:=0;
		  end;
		  if datamodul.q_bereich['reli']=0 then
		  begin
			combobox_rechts.Visible:=false;
			label_re.Visible:=false;
			label_li.Visible:=false;
		  end
		  else
       begin
			combobox_rechts.Visible:=true;
			label_re.Visible:=true;
			label_li.Visible:=true;
		  end
		except
		end;

		3:// audiogramm
		  begin
		  try
		  if modus_history or tab.locate('i_bereich',bernum,[]) then
				begin
					wort:=tab['str'];
					radiogroup_weber.ItemIndex:=strtoint(copy(wort,1,2));
					edit_sfr.text:=trim(copy(wort,3,4));
					edit_sfl.text:=trim(copy(wort,7,4));
					edit_sdbr.text:=trim(copy(wort,11,3));
					edit_sdbl.text:=trim(copy(wort,14,3));
					edit_spr.text:=trim(copy(wort,17,3));
					edit_spl.text:=trim(copy(wort,20,3));

					audiogramm_li.luft_schreiben(copy(wort,23,36));
					audiogramm_re.luft_schreiben(copy(wort,59,36));
					audiogramm_li.knochen_schreiben(copy(wort,95,36));
					audiogramm_re.knochen_schreiben(copy(wort,131,36));
				end
				else
				begin
					radiogroup_weber.ItemIndex:=-1;
					edit_sfr.text:='';
					edit_sfl.text:='';
					edit_sdbr.text:='';
					edit_sdbl.text:='';
					edit_spr.text:='';
					edit_spl.text:='';
					audiogramm_li.luft_schreiben('-20-20-20-20-20-20-20-20-20-20-20-20');
					audiogramm_re.luft_schreiben('-20-20-20-20-20-20-20-20-20-20-20-20');
					audiogramm_li.knochen_schreiben('-20-20-20-20-20-20-20-20-20-20-20-20');
					audiogramm_re.knochen_schreiben('-20-20-20-20-20-20-20-20-20-20-20-20');
				end;
				except
				end;
           if datamodul.q_mitarbeiter['geb_dat']<>null then
				begin
					audiogramm_re.geb_dat:=datamodul.q_mitarbeiter['geb_dat'];
					audiogramm_li.geb_dat:=datamodul.q_mitarbeiter['geb_dat'];
				end;
				end;
		  4:	// sehtest
				try
        //sehtest_ini   cells[col,row]
        grid_sehtest.cells[0,1]:='Ferne';
        grid_sehtest.cells[0,2]:='Bildschirm';
        grid_sehtest.cells[0,3]:='N�he';
        grid_sehtest.cells[1,0]:='rechts';
        grid_sehtest.cells[2,0]:='links';
        grid_sehtest.cells[3,0]:='binocular';
        grid_sehtest.cells[4,0]:='Brille rechts';
        grid_sehtest.cells[5,0]:='Brille links';
        grid_sehtest.cells[6,0]:='Brille binocular';
				if modus_history or tab.locate('i_bereich',bernum,[]) then
					 begin //sehtest vorhanden

						 wort:=tab['str'];
             grid_sehtest.cells[1,1]:=copy(wort,1*3+1,3);
             grid_sehtest.cells[2,1]:=copy(wort,2*3+1,3);
             grid_sehtest.cells[3,1]:=copy(wort,0*3+1,3);
             grid_sehtest.cells[4,1]:=copy(wort,4*3+1,3);
             grid_sehtest.cells[5,1]:=copy(wort,5*3+1,3);
             grid_sehtest.cells[6,1]:=copy(wort,3*3+1,3);

             grid_sehtest.cells[1,2]:=copy(wort,13*3+1,3);
             grid_sehtest.cells[2,2]:=copy(wort,14*3+1,3);
             grid_sehtest.cells[3,2]:=copy(wort,12*3+1,3);
             grid_sehtest.cells[4,2]:=copy(wort,16*3+1,3);
             grid_sehtest.cells[5,2]:=copy(wort,17*3+1,3);
             grid_sehtest.cells[6,2]:=copy(wort,15*3+1,3);

             grid_sehtest.cells[1,3]:=copy(wort,7*3+1,3);
             grid_sehtest.cells[2,3]:=copy(wort,8*3+1,3);
             grid_sehtest.cells[3,3]:=copy(wort,6*3+1,3);
             grid_sehtest.cells[4,3]:=copy(wort,10*3+1,3);
             grid_sehtest.cells[5,3]:=copy(wort,11*3+1,3);
             grid_sehtest.cells[6,3]:=copy(wort,9*3+1,3);

          end
				 else
					begin  //sehtest neu
					  for j:=1 to grid_sehtest.RowCount-1 do
						  for i:=1 to grid_sehtest.ColCount-1 do
						  begin
							  grid_sehtest.cells[i,j]:='';
						  end;

					end;
					richedit_befund.tag:=0;
				except
				end;
			 5:// sBlutdruck
				 try
				  if modus_history or tab.locate('i_bereich',bernum,[]) then
					 begin //rr vorhanden
						 spinedit_sy.tag:=0;
						 spinedit_sy.Value:= tab['i_1'];
						 spinedit_di.Value:=tab['i_2'];
						 spinedit_pu.Value:=tab['i_3'];
					 end
				 else
					begin  //rr neu
						spinedit_sy.Value:= 0;
						spinedit_di.Value:=0;
						spinedit_pu.Value:=0;
						spinedit_sy.tag:=0;
					end;
					except
					end;
			  6: //groesse gewicht
				try
				if modus_history or tab.locate('i_bereich',bernum,[]) then
				begin // schon angelegt
					 edit_groesse.text:=floattostr(tab['f_1']);
					 edit_gewicht.text:=floattostr(tab['f_2']);
					 edit_bmi.text:=floattostr(tab['f_3']);
           if tab.findfield('f_4') <>nil then   edit_bauchumfang.text:=floattostr(tab['f_4']);
           edit_whtr.text:=floattostrf(tab['f_5'],fffixed,5,2);
				end
				else //noch keine
				begin
					 //radiogroup_auffaellig.ItemIndex:=-1;
					 edit_groesse.text:='0';
					 edit_gewicht.text:='0';
					 edit_bmi.text:='0';
                     edit_bauchumfang.text:='0';
                     edit_whtr.text:='0';
				end;
				except
				end;
			  7: //lufu

				try
				  if modus_history or tab.locate('i_bereich',bernum,[]) then
				  begin // schon angelegt
						edit_fev1.text:=floattostr(tab['f_1']);
						edit_fvc.text:=floattostr(tab['f_2']);
						edit_fev_fvc.text:=floattostr(tab['f_3']);
						edit_pev.text:=floattostr(tab['f_4']);

					end
				  else //noch keine
				  begin
						edit_fev1.text:='0';
						edit_fvc.text:='0';
						edit_fev_fvc.text:='0';
						edit_pev.text:='0';

				  end;
				  lufu_normwert;
           except
				end;
          8: try
				if modus_history or tab.locate('i_bereich',bernum,[]) then
             begin //wai vorhanden
                 //wai lesen
                 edit_wai.text:= inttostr(tab['wert']);
                 wort:=tab['str'];
                 i:=pos('|',wort);
                 combobox_arbeit.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));

                 i:=pos('|',wort);
                 combobox1.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox2.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox3.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));

                 i:=pos('|',wort);
                 maskedit_d1.text:=copy(wort,1,i-1);
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 maskedit_d2.text:=copy(wort,1,i-1);
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 maskedit_d3.text:=copy(wort,1,i-1);
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 maskedit_d4.text:=copy(wort,1,i-1);
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 maskedit_d5.text:=copy(wort,1,i-1);
                 wort:=copy(wort,i+1,length(wort));

                 i:=pos('|',wort);
                 combobox4.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox5.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox6.itemindex:=2-strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox7.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox8.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));
                 i:=pos('|',wort);
                 combobox9.itemindex:=strtoint(copy(wort,1,i-1));
                 wort:=copy(wort,i+1,length(wort));


             end
             else
             begin
                  //wai _ini
                 edit_wai.text:= '';
                 combobox_arbeit.itemindex:=0;
                 combobox1.itemindex:=0;
                 combobox2.itemindex:=0;
                 combobox3.itemindex:=0;
                 maskedit_d1.text:='';
                 maskedit_d2.text:='';
                 maskedit_d3.text:='';
                 maskedit_d4.text:='';
                 maskedit_d5.text:='';
                 edit_d1.Text:='';
                 edit_d2.text:='';
                 edit_d3.text:='';
                 edit_d4.text:='';
                 edit_d5.text:='';
                 combobox4.itemindex:=0;
                 combobox5.itemindex:=0;
                 combobox6.itemindex:=0;
                 combobox7.itemindex:=0;
                 combobox8.itemindex:=0;
                 combobox9.itemindex:=0;
             end;
             except
             end;
			end; //case

	 radiogroup_auffaellig.tag:=0;
	 richedit_befund.tag:=0;
	 scrollbox_ole_befund.tag:=0;

	 radiogroup_parameter.ItemIndex:=0;

end;



procedure tform_main.treedeaktivieren(tree: ttreeview);
var
i:integer;
daten:pnodepointer;
begin
for i:=0 to tree.Items.count -1 do
	begin
		daten:=tree.Items[i].Data;
		dispose(daten);
	end;
	tree.Items.Clear;
end;

procedure tform_main.listedeaktivieren(liste: tstrings);
var
i:integer;
daten:pnodepointer;
begin
for i:=0 to liste.count -1 do
	begin
		daten:=pnodepointer(liste.Objects[i]);
		dispose(daten);
	end;
	liste.Clear;
end;

procedure tform_main.Bereicheinlesen(tree:ttreeview; tabelle:tzquery; index, feldname:string);
var
i_parent, i_index,i: integer;
knoten: ttreenode;
daten:pnodepointer;
begin
//tabelle.IndexName:=index;

tabelle.First;

if  tabelle.Eof then exit;
	//erster datensatz von hand
	knoten:=Tree.Items.Add(tree.Selected, tabelle[feldname]);      //tree_bereich
	new(daten);
	daten^.nummer:=getbigint(tabelle,'nummer');
	daten^.bereich:=0;
  daten^.ebene:=0;
  daten^.liste:=nil;
	knoten.Data:=daten;
	knoten.SelectedIndex:=1;
	tabelle.Next;

while not tabelle.Eof do
begin
	i_parent:= tabelle['parent'];
	i_index:=-1; //#-1
	for i:=0 to tree.Items.count -1 do
	begin
		daten:=tree.Items[i].Data;
		if daten^.nummer=i_parent then
		begin
			i_index:=i;
			break;
		end;
	end;

	if i_index>-1 then
		  knoten:=Tree.Items.Addchild(tree.items[i_index], tabelle['name'])
 else
			knoten:=Tree.Items.Add(tree.items[0], tabelle['name']);

	new(daten);
	daten^.nummer:=getbigint(tabelle,'nummer');
  daten^.liste:=nil;
	knoten.Data:=daten;
	knoten.SelectedIndex:=1;
	tabelle.Next;
end;

end;

procedure tform_main.listeeinlesen(liste:tstrings;tabelle:tzquery;feld:string);
var daten:pnodepointer;
begin
listedeaktivieren(liste);


tabelle.first;
while not tabelle.eof do
begin
	new(daten); daten^.liste:=nil;
	daten^.nummer:=getbigint(tabelle,'nummer');
  daten^.liste:=nil;
	liste.AddObject(tabelle[feld],tobject(daten));
	tabelle.Next;
end;
end;

function tform_main.liste_datasuchen(liste:tstrings;wert:integer):integer;
var daten:pnodepointer;
begin
for result:=0 to liste.count-1 do
begin
	daten:=pnodepointer(liste.Objects[result]);
	if daten^.nummer=wert then exit;
end;
result:=0;
end;


procedure TForm_main.Bereich_speichern;
var i,j:integer;
wort,f:string;
i_bereich, aufindex,comboindex, berlistnummer,abrechnen: integer;
knoten: ttreenode;
daten:pnodepointer;
i_untersuchung,nr:int64;

begin
if dbtreeview.selected= nil then exit;
//es wurde nix ge�ndert
if (radiogroup_auffaellig.tag+richedit_befund.tag+scrollbox_ole_befund.tag=0) then exit;

try
	//i_untersuchung:=getbigint(datamodul.q_befunde,'nummer'); //befundvorlage wird automatisch located
	knoten:=dbtreeview.selected;
	daten:=knoten.Data;
	i_untersuchung:=daten^.nummer;
	i_bereich:=daten^.bereich;
	if i_bereich=0 then exit; // bereichs-�berschrift

	aufindex:=radiogroup_auffaellig.ItemIndex;
	if aufindex<0 then aufindex:=0;//automatisch auf unauff�llig setzen

	dbtreeview.selected.StateIndex:=aufindex+13;//+10; //+9

   //	if datamodul.q_akt_untersuchung.eof and datamodul.q_akt_untersuchung.bof then
   if kein_datensatz( datamodul.q_akt_untersuchung) then
	begin
	  if abrechnen_voso then abrechnen:=1 else abrechnen:=0;
	  direktspeichern:=true;  //on post wird ausgel�st
	  nr:=neuer_datensatz(datamodul.q_akt_untersuchung,['nummer','i_untersuchung','i_bereich','auffaellig','abrechnen']
	  ,[null,inttostr(i_untersuchung),i_bereich,aufindex,abrechnen]);
	end;

	//else
	datamodul.q_akt_untersuchung.edit;


	 //f�r alle gleich
	 datamodul.q_akt_untersuchung['auffaellig']:=aufindex;

	 if richedit_befund.tag=1 then
   richeditspeichern( datamodul.q_akt_untersuchung,'memo',richedit_befund);
	 //strings_speichern(datamodul.q_akt_untersuchung,'memo_blob',richedit_befund.Lines);

	 if scrollbox_ole_befund.tag=1 then
	 	olecontainer_speichern(true,datamodul.q_akt_untersuchung,'ole',arbeitsmedizin_befund);
     olecontainer_close(arbeitsmedizin_befund);
	 datamodul.q_akt_untersuchung['goae']:=edit_b_goae.text;
     datamodul.q_akt_untersuchung['euro']:=strtofloat(edit_waehrung_bef.text);
	 if checkbox_b_abr.checked then datamodul.q_akt_untersuchung['abrechnen']:=1 else datamodul.q_akt_untersuchung['abrechnen']:=0;
     if checkbox_bef_ust.checked then datamodul.q_akt_untersuchung['ust']:=1 else datamodul.q_akt_untersuchung['ust']:=0;
	case datamodul.q_bereich['modus'] of
		  0:begin

				end;
		  1:	begin  //Zahl
				  datamodul.q_akt_untersuchung['wert']:=strtofloat(edit_bef.text);
             end;
		  2:begin  //liste
				comboindex:=combobox.itemindex;
				if comboindex>=0 then
				begin
				  daten:=pnodepointer(combobox.Items.Objects[comboindex]);
				  if daten<>nil then berlistnummer:=daten^.nummer
				  else berlistnummer:=-1;
				  datamodul.q_akt_untersuchung['i_berlist']:= berlistnummer;
				end;
				comboindex:=combobox_rechts.itemindex;
				if comboindex>=0 then
				begin
				  daten:=pnodepointer(combobox.Items.Objects[comboindex]);
				  if daten<>nil then berlistnummer:=daten^.nummer
				  else berlistnummer:=-1;
				  datamodul.q_akt_untersuchung['i_berlist_rechts']:= berlistnummer;
				  //datamodul.q_akt_untersuchung['memo']:=richedit_befund.text;
				end;

			  end;
		  3: begin    //audiogramm
				 wort:=str_l_space(inttostr(radiogroup_weber.ItemIndex),2);
				 wort:=wort+str_l_space(edit_sfr.text,4);
				 wort:=wort+str_l_space(edit_sfl.text,4);
				 wort:=wort+str_l_space(edit_sdbr.text,3);
				 wort:=wort+str_l_space(edit_sdbl.text,3);
				 wort:=wort+str_l_space(edit_spr.text,3);
				 wort:=wort+str_l_space(edit_spl.text,3);
				 wort:=wort+str_l_space(audiogramm_li.luft_lesen,36);
				 wort:=wort+str_l_space(audiogramm_re.luft_lesen,36);
				 wort:=wort+str_l_space(audiogramm_li.knochen_lesen,36);
				 wort:=wort+str_l_space(audiogramm_re.knochen_lesen,36);
				 //datamodul.q_akt_untersuchung.edit;
				 datamodul.q_akt_untersuchung['str']:=wort;
				 //datamodul.q_akt_untersuchung['memo']:=richedit_befund_audio.text;

				end;
		  4: begin //sehtest
				 wort:='';
         wort:=wort+copy(grid_sehtest.cells[3,1]+'   ',1,3);//+:=copy(wort,0*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[1,1]+'   ',1,3);//:=copy(wort,1*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[2,1]+'   ',1,3);//:=copy(wort,2*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[6,1]+'   ',1,3);//:=copy(wort,3*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[4,1]+'   ',1,3);//:=copy(wort,4*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[5,1]+'   ',1,3);//:=copy(wort,5*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[3,3]+'   ',1,3);//:=copy(wort,6*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[1,3]+'   ',1,3);//:=copy(wort,7*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[2,3]+'   ',1,3);//:=copy(wort,8*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[6,3]+'   ',1,3);//:=copy(wort,9*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[4,3]+'   ',1,3);//:=copy(wort,10*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[5,3]+'   ',1,3);//:=copy(wort,11*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[3,2]+'   ',1,3);//:=copy(wort,12*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[1,2]+'   ',1,3);//:=copy(wort,13*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[2,2]+'   ',1,3);//:=copy(wort,14*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[6,2]+'   ',1,3);//:=copy(wort,15*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[4,2]+'   ',1,3);//:=copy(wort,16*3+1,3);
         wort:=wort+copy(grid_sehtest.cells[5,2]+'   ',1,3);//:=copy(wort,17*3+1,3);



				 datamodul.q_akt_untersuchung['str']:=wort;
				//datamodul.q_akt_untersuchung['memo']:=richedit_befund_sehtest.text;

				end;
		  5:begin  //rr
				datamodul.q_akt_untersuchung['i_1']:=spinedit_sy.Value;
				datamodul.q_akt_untersuchung['i_2']:=spinedit_di.Value;
				datamodul.q_akt_untersuchung['i_3']:=spinedit_pu.Value;
				//datamodul.q_akt_untersuchung['memo']:=richedit_befund.text;
			  end;
		  6: begin //gr�sse gewicht
				datamodul.q_akt_untersuchung['f_1']:=strtofloat(edit_groesse.text);
				datamodul.q_akt_untersuchung['f_2']:=strtofloat(edit_gewicht.text);
				datamodul.q_akt_untersuchung['f_3']:=strtofloat(edit_bmi.text);
                datamodul.q_akt_untersuchung['f_4']:=strtofloat(edit_bauchumfang.text);
				datamodul.q_akt_untersuchung['f_5']:=strtofloat(edit_whtr.text);

				//datamodul.q_akt_untersuchung['memo']:=richedit_befund.text;

			  end;
		  7: begin //lufu
				datamodul.q_akt_untersuchung['f_1']:=strtofloat(edit_fev1.text);
				datamodul.q_akt_untersuchung['f_2']:=strtofloat(edit_fvc.text);
				datamodul.q_akt_untersuchung['f_3']:=strtofloat(edit_fev_fvc.text);
				datamodul.q_akt_untersuchung['f_4']:=strtofloat(edit_pev.text);
			  end;
       8: begin
               //wai_speichern
               if edit_wai.text<>'' then datamodul.q_akt_untersuchung['wert']:=strtoint(edit_wai.text)
                                    else datamodul.q_akt_untersuchung['wert']:=0;
               wort:='';
               wort:=wort+inttostr(combobox_arbeit.ItemIndex)+'|';
               wort:=wort+inttostr(combobox1.ItemIndex)+'|';
               wort:=wort+inttostr(combobox2.ItemIndex)+'|';
               wort:=wort+inttostr(combobox3.ItemIndex)+'|';
               wort:=wort+copy(maskedit_d1.text,1,30)+'|';
               wort:=wort+copy(maskedit_d2.text,1,30)+'|';
               wort:=wort+copy(maskedit_d3.text,1,30)+'|';
               wort:=wort+copy(maskedit_d4.text,1,30)+'|';
               wort:=wort+copy(maskedit_d5.text,1,30)+'|';
               wort:=wort+inttostr(combobox4.ItemIndex)+'|';
               wort:=wort+inttostr(combobox5.ItemIndex)+'|';
               wort:=wort+inttostr(2-combobox6.ItemIndex)+'|';
               wort:=wort+inttostr(combobox7.ItemIndex)+'|';
               wort:=wort+inttostr(combobox8.ItemIndex)+'|';
               wort:=wort+inttostr(combobox9.ItemIndex)+'|';
               datamodul.q_akt_untersuchung['str']:=wort;
          end;
		 end;
	datamodul.q_akt_untersuchung.post;
  //if (datamodul.q_akt_untersuchung.FieldByName('ole')<>nil) then f:=datamodul.q_akt_untersuchung.FieldByName('ole').asstring;
  if  (datamodul.q_akt_untersuchung.FieldByName('ole')<>nil) and (datamodul.q_akt_untersuchung.FieldByName('ole').asstring<>'')  then
  begin
       dbtreeview.Selected.ImageIndex:=6;
       dbtreeview.Selected.selectedIndex:=6;
  end;

	richedit_befund.modified:=false;//rtfedited;;
	//richedit_befund.tag:=rtftag;
	dbtreeview.markiere_parent(dbtreeview.selected);

	//BitBtn_bef_del.enabled:=true;

	radiogroup_auffaellig.tag:=0;
	richedit_befund.tag:=0;
	scrollbox_ole_befund.tag:=0;
except
end;
end;


procedure tform_main.parent_state(node: ttreenode;aufindex:integer );
var
i_nummer:integer;
knoten: ttreenode;
daten:pnodepointer;
bookmark: tbookmark;
i_untersuchung,nr:int64;
begin
bookmark:=datamodul.q_akt_untersuchung.GetBookmark;
	if node.Parent<>nil then
	begin
	 i_untersuchung:=getbigint(datamodul.q_befunde,'nummer');
	 knoten:=node.parent;
	 if knoten.parent=nil then //kein weiterer �bergeordneter knoten nur der oberste soll markiert werden
	 begin
		daten:=knoten.Data;
		i_nummer:=daten^.nummer;
		knoten.StateIndex:=aufindex+3;
		//
		datamodul.q_akt_untersuchung.first;
		if not datamodul.q_akt_untersuchung.locate('nummer',i_nummer,[]) then
		begin
			 direktspeichern:=true;  //on post wird ausgel�st

         nr:=neuer_datensatz(datamodul.q_akt_untersuchung,['nummer','i_untersuchung','i_bereich','auffaellig','wert','i_berlist','i_berlist_rechts']
			 ,[null,inttostr(i_untersuchung),i_nummer,aufindex,0,-1,-1]);
         if nr=-1 then exit;
			 datamodul.q_akt_untersuchung.edit;
		end
		else
		begin
			datamodul.q_akt_untersuchung.first;
			datamodul.q_akt_untersuchung.locate('nummer',i_nummer,[]);
			datamodul.q_akt_untersuchung.edit;
			datamodul.q_akt_untersuchung['auffaellig']:=node.StateIndex-3;
			direktspeichern:=true;
			datamodul.q_akt_untersuchung.post;
		end;
	 end
	 else
		parent_state(node.parent,aufindex);
	end;
datamodul.q_akt_untersuchung.GotoBookmark(bookmark);
datamodul.q_akt_untersuchung.FreeBookmark(bookmark);
end;


procedure TForm_main.Bereich_del;
var
knoten: ttreenode;
daten:pnodepointer;
i_nummer,i_bereich:int64;
begin

	knoten:=dbtreeview.selected;
	daten:=knoten.Data;
	i_bereich:=daten^.bereich;
	i_nummer:=daten^.nummer;
	//aufindex:=radiogroup_auffaellig.ItemIndex;

	//if  datamodul.q_akt_untersuchung.locate('i_untersuchung,i_bereich',vararrayof([inttostr(i_nummer),inttostr(i_bereich)]),[]) then
  if  datamodul.q_akt_untersuchung.locate('i_untersuchung,i_bereich',vararrayof([(i_nummer),(i_bereich)]),[]) then
	begin
		dateiloeschen(datamodul.q_akt_untersuchung, true,false);
		radiogroup_auffaellig.ItemIndex:=-1;
		//spinedit.Text:='0';
		edit_bef.text:='0';
		richedit_befund.Text:='';
		if combobox.Items.Count>0 then combobox.ItemIndex:=0;
		radiogroup_auffaellig.tag:=0;
		//spinEdit.tag:=0;
		edit_bef.tag:=0;
		ComboBox.tag:=0;
		richedit_befund.tag:=0;
		scrollbox_ole_befund.tag:=0;
		radiogroup_weber.tag:=0;
		groupbox_sisi.tag:=0;
		audiogramm_li.tag:=0;
		audiogramm_re.tag:=0;
		spinedit_sy.tag:=0;
		edit_groesse.tag:=0;
		edit_gewicht.tag:=0;
		edit_bmi.tag:=0;
        edit_bauchumfang.tag:=0;
        edit_whtr.tag:=0;
		edit_fev1.tag:=0;
	end;
  datamodul.q_akt_untersuchung_storno.Refresh;

	knoten.StateIndex:=-1;
	//BitBtn_bef_del.enabled:=false;
end;


procedure TForm_main.BitBtn_icd_wahlClick(Sender: TObject);
begin
form_icd:=tform_icd.Create(self);
if form_icd.showmodal=mrok then
begin
	datamodul.q_diagnosen.edit;
	//datamodul.q_diagnosen['i_icd']:=datamodul.q_icd_main['nummer'];
	datamodul.q_diagnosen['icd']:=datamodul.q_icd_main['icd_schluessel'];
	datamodul.q_diagnosen['icd_text']:=datamodul.q_icd_main['icd_text'];

end;
form_main.timer_icd.enabled:=true;
form_icd.Release ;

end;

procedure TForm_main.BitBtn_beurt_einfClick(Sender: TObject);
var
z,query,satz:string;
i,bes_nummer:integer;
knoten: ttreenode;
daten:pnodepointer;
begin
	datamodul.sql_new(true,datamodul.q_besonderheitenliste_tree,'select * from besonderheitenliste where b_beurteilung=-1 ','reihenfolge');;
	form_besonderheiten:=tform_besonderheiten.Create(self);
   form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
	form_besonderheiten.tree.DragMode:=dmmanual;
	form_besonderheiten.ToolBar.Visible:=false;
	form_besonderheiten.Panel_bottom.Visible:=true;
  form_besonderheiten.MultiSelect:=true;
  // form_besonderheiten.Tree.Selected ;
	if form_besonderheiten.showmodal=mrok then
	begin


	  try
	  datamodul.q_untersuchung.edit;
    for i:=0 to  form_besonderheiten.Tree.Items.Count-1 do
			begin
				if (form_besonderheiten.Tree.Items[i].stateindex>0) or (form_besonderheiten.Tree.Items[i]=form_besonderheiten.Tree.Selected) then //ausgew�hlt
				begin
            knoten :=form_besonderheiten.Tree.items[i];
				    daten:=knoten.Data;
				    bes_nummer:=daten^.nummer;

            query:=format('select besonderheit from besonderheitenliste where nummer=%d',[bes_nummer]);
            satz:=mysql_d.Feldinhalt(query,0);

            if sender =bitbtn_beurt_einf then
              begin
                //dbmemo_beurteilung.lines.Add( datamodul.q_besonderheitenliste_tree['besonderheit']);
                if  datamodul.q_untersuchung.FindField('memo').asstring<>'' then z:=#13#10 else z:='';
                //datamodul.q_untersuchung.FindField('memo').asstring:= datamodul.q_untersuchung.FindField('memo').asstring+z+datamodul.q_besonderheitenliste_tree.findfield('besonderheit').asstring;
                  //datamodul.q_untersuchung.FindField('memo').asstring:= datamodul.q_untersuchung.FindField('memo').asstring+z+satz;
                  edit_leer.Text:=z+satz;
                  edit_leer.SelectAll;
                  edit_leer.CopyToClipboard;
                  application.ProcessMessages;
                  dbmemo_beurteilung.PasteFromClipboard;
                  //dbmemo_beurteilung.ins
                  //dbmemo_beurteilung.lines.Insert(0,z+satz);

                end
            else
            begin
              if  datamodul.q_untersuchung.FindField('memo_proband').asstring<>'' then z:=#13#10 else z:='';
               // datamodul.q_untersuchung.FindField('memo_proband').asstring:= datamodul.q_untersuchung.FindField('memo_proband').asstring+z+datamodul.q_besonderheitenliste_tree.findfield('besonderheit').asstring;
              //datamodul.q_untersuchung.FindField('memo_proband').asstring:= datamodul.q_untersuchung.FindField('memo_proband').asstring+z+satz;
              edit_leer.Text:=z+satz;
             edit_leer.SelectAll;
             edit_leer.CopyToClipboard;
              application.ProcessMessages;
              dbmemo_beurteilung_ma.PasteFromClipboard;
            end;
        end;
      end;  
	  except
	  end;
	  //BitBtn_beurt_ueber.Enabled:=true;
	end;
	form_besonderheiten.Release;
	form_besonderheiten:=nil;
end;

procedure TForm_main.PageControl_untersuchungChange(Sender: TObject);
begin
  //BitBtn_beurt_ueber.Enabled:=false;
//  if pagecontrol_untersuchung.ActivePage=tabsheet_unt_befunde then  baum_aktualisieren;
end;

procedure TForm_main.ToolButton_editClick(Sender: TObject);
begin
	formular_entsperren(true,false);
end;




procedure TForm_main.ToolButton25Click(Sender: TObject);
begin
if assigned(rtfedit) then
try
//try //wird aufgerufen bevor rtfedit creiert.
if rtfedit.FUpdating then Exit;
akt_editor.modified:=true;
rtfedit.CurrText.Name :='Arial';
rtfedit.CurrText.Size := 10;
rtfedit.CurrText.Color:=colordialog.Color;
except
end;

end;

procedure TForm_main.DBgridEXT_namenKeyPress(Sender: TObject; var Key: Char);
var ordkey: integer;
begin
ordkey:=ord(key);
case

	ordkey of
	13: namenanzeigen; //enter
  48..256 :
     begin
     datamodul.q_mitarbeiter.first;
     datamodul.q_mitarbeiter.locate('name',DBgridEXT_namen.eingabetext,[loCaseInsensitive,loPartialKey]);
     dbgridext_namen.SetFocus;
     end;
end;
end;

procedure tform_main.combolesen_s(combo:tcombobox;tab_name, feldname,order,alle:string);
var
	q:tzquery;
	query:string;
	//ta: pnodepointer;
    t:string;
begin
combo.Items.Clear;
if alle<>'' then
begin
	combo.items.Add(alle);
end;
try
   query:=format('select %s,%s from %s where storno=0',['nummer',feldname,tab_name]);
   if order<>'' then query:=query +' order by '+order;
   q:=tzquery.create(self);
   q.connection:=datamodul.connection_main;
   sql_normieren(query);
   q.Sql.Text:=query;
   q.Open;
   q.first;
   while not q.Eof do
   begin
      if q[feldname]<>null then
      begin
         Combo.Items.Add(q.findfield(feldname).asstring);
      end;
      q.Next;
   end;
   combo.ItemIndex:=0;

finally
	q.close;
	q.free;
end;

end;


procedure tform_main.combolesen_q(combo:tcombobox;table: tzquery; feldname,alle:string);
var
	data: pnodepointer;
	bookmark:tbookmark;
begin
combodeaktivieren(combo);
bookmark:=table.GetBookmark;
if alle<>'' then
begin
	new(data);
	data^.nummer:=-1;
	combo.items.AddObject(alle,tobject(data));
end;

table.first;
while not table.Eof do
begin
	if table[feldname]<>null then
	begin
		new(data);
		data^.nummer:=getbigint(table,'nummer');
		Combo.Items.AddObject(table[feldname],tobject(data));
	end;
	table.Next;
end;
table.GotoBookmark(bookmark);
table.FreeBookmark(bookmark);
combo.ItemIndex:=0;

end;



procedure tform_main.combolesen(combo:tcombobox;tab_name, feldname,order,alle:string);
var
	q:tzquery;
	query:string;
	data: pnodepointer;
    t:string;
begin
query:=format('select %s,%s from %s where storno=0',['nummer',feldname,tab_name]);
if order<>'' then query:=query +' order by '+order;

q:=tzquery.create(self);
q.connection:=datamodul.connection_main;
sql_normieren(query);
q.Sql.Text:=query;
q.Open;


try
	combodeaktivieren(combo);
	if alle<>'' then
	begin
	  new(data);
	  data^.nummer:=-1;
	  combo.items.AddObject(alle,tobject(data));

	end;

	q.first;
	while not q.Eof do
	begin
	  if q[feldname]<>null then
	  begin
		  new(data);
		  data^.nummer:=getbigint(q,'nummer');
		  Combo.Items.AddObject(q[feldname],tobject(data));
	  end;
	  q.Next;
	end;
	combo.ItemIndex:=0;
    //if combo.items.count>=1 then combo.Text:=combo.items[0];
finally
	q.close;
	q.free;
end;
end;


procedure tform_main.itemslesen(items:tstrings;table: tzquery; feldname,alle:string);
var
	data: pnodepointer;
	bookmark:tbookmark;
begin
itemsdeaktivieren(items);
bookmark:=table.GetBookmark;
if alle<>'' then
begin
	new(data);
	data^.nummer:=-1;
	items.AddObject(alle,tobject(data));
end;

table.first;
while not table.Eof do
begin
	if table[feldname]<>null then
	begin
		new(data);
		data^.nummer:=getbigint(table,'nummer');
		Items.AddObject(table[feldname],tobject(data));
	end;
	table.Next;
end;
table.GotoBookmark(bookmark);
table.FreeBookmark(bookmark);
end;

function tform_main.combowert(combo:tcombobox):int64;
var
	daten: pnodepointer;
	comboindex:integer;
begin
	comboindex:=combo.itemindex;
	daten:=pnodepointer(combo.Items.Objects[comboindex]);
	if daten<>nil then result:=daten^.nummer else result:=-2;
end;


procedure tform_main.combodeaktivieren(combo: tcombobox);
var
	data: pnodepointer;
	i:integer;
begin
for i:=0 to combo.Items.count-1 do
begin
	data:=pnodepointer(combo.Items.Objects[i]);
	dispose(data);
end;
combo.Items.Clear;
end;

procedure tform_main.itemsdeaktivieren(items: tstrings);
var
	data: pnodepointer;
	i:integer;
begin
for i:=0 to Items.count-1 do
begin
	data:=pnodepointer(Items.Objects[i]);
	dispose(data);
end;
Items.Clear;
end;

procedure tform_main.combogehezu(combo: tcombobox; wert:integer);
var
	data: pnodepointer;
  i:integer;
begin
combo.ItemIndex:=0;
for i:=0 to combo.Items.count-1 do
begin
	data:=pnodepointer(combo.Items.Objects[i]);
  if data^.nummer= wert then
  begin
  	combo.ItemIndex:=i;
  	exit;
  end;
end;
end;



procedure TForm_main.BitBtn_bef_delClick(Sender: TObject);
begin
bereich_del;
end;

procedure TForm_main.RadioGroup_auffaelligClick(Sender: TObject);
begin
radiogroup_auffaellig.tag:=1;
//if datamodul.q_berlist['c_auffaellig']='normal' then radiogroup_auffaellig.ItemIndex:=0 else radiogroup_auffaellig.ItemIndex:=1;
//bereich_speichern;
end;

procedure TForm_main.dbrichedit_befundDblClick(Sender: TObject);
begin
Bereich_speichern;
end;

//####################################################################################################################################
procedure tform_main.sperren(komponente: tcomponent);      //alles wird grau
//########################################################################################################################################
var
a,i,j,k: integer;
temp:tcontrol;

begin

if komponente=nil then exit;
if  twincontrol(komponente).tag=5 then exit;  //kompoenten mit tag=5 werden nicht gesperrt;


try
//twincontrol(komponente).visible:=false;

//clsilver;//clGrayytText;//clinfobk; // clInactiveCaptionText;
// if komponente=tabsheet_anamnese then ana_toolbar_sperren;

 for I := twincontrol(komponente).controlcount - 1 downto 0 do
	begin
	  Temp := twincontrol(komponente).controls[I];
	  if temp.tag<>5 then
	  begin
		 //if temp is tpagecontrol then sperren(tpagecontrol(temp).ActivePage);  s.unten sonst wird nicht alles gesperrt
     if temp is tpagecontrol then sperren(temp);
		 if temp is ttabsheet then sperren(temp);
		 if temp is tgroupbox then sperren(temp);
		 if temp is tscrollbox then sperren(temp);
		 if temp is tpanel then sperren(temp);
		 if Temp is Ttoolbar then sperren(temp);
		 if temp.name='' then temp:=twincontrol(komponente);
		 {if temp is tnotebook then
		 begin
		  a:=tnotebook(temp).pageindex;
		  for j:=0 to tnotebook(temp).pages.count-1 do
		  begin
			tnotebook(temp).pageindex:=j;
			for k:=0 to tnotebook(temp).ControlCount-1 do sperren(temp.Components[k]) ;
		  end;
		  tnotebook(temp).pageindex:=a;
		 end;}
     if temp is tnotebook then
		 begin
		  	//for k:=0 to tnotebook(temp).ControlCount-1 do sperren(temp.Components[k]) ;
        sperren(tnotebook(temp).Components[tnotebook(temp).pageindex]);
     end;

		  if (Temp is tdbedit) then
		  begin
			  tdbedit(temp).color:=cl_gesperrt;
			  tdbedit(temp).readonly:=true;
        tdbedit(temp).OnChange:=nil;
		  end;

		  if (Temp is  TDBRadioGroup) then
		  begin
				TDBRadioGroup(temp).readonly:=true;
				TDBRadioGroup(temp).OnChange:=nil;
		  end;

			if (Temp is  TDBimage) then
		  begin
				TDBimage(temp).color:=cl_gesperrt;//clInactiveCaptionText;
		  end;

		  if (Temp is  TDBmemo) then
		  begin
				TDBmemo(temp).color:=cl_gesperrt;
				TDBmemo(temp).readonly:=true;
        TDBmemo(temp).OnChange:=nil;
		  end;


		  if (Temp is  TDBrichedit) then
		  begin
				tDBrichedit(temp).color:=cl_gesperrt;
				tDBrichedit(temp).readonly:=true;
        tDBrichedit(temp).OnChange:=nil;
		  end;

		  if (Temp is  tDBLookupComboBox) then
		  begin
				tDBLookupComboBox(temp).readonly:=true;
				tDBLookupComboBox(temp).color:=cl_gesperrt;
        //tDBLookupComboBox(temp).OnChange:=nil;
		  end;

		  if (Temp is  tDBCombo_num) then
		  begin
				tDBCombo_num(temp).readonly:=true;
				tDBCombo_num(temp).color:=cl_gesperrt;
        tDBCombo_num(temp).OnChange:=nil;
		  end;


		  if (Temp is  tDBComboBox) then
		  begin
				tDBComboBox(temp).readonly:=true;
				tDBComboBox(temp).color:=cl_gesperrt;
        tDBComboBox(temp).OnChange:=nil;
		  end;

			if (Temp is  tDBcheckBox) then
		  begin
				tDBcheckBox(temp).readonly:=true;
				//tDBcheckBox(temp).OnChange:=nil;
		  end;

		  if (Temp is  trichedit) then
		  begin
        if assigned(form_richedit_menue) then
        begin
          trichedit(temp).readonly:=true; //enabled:=false;
          trichedit(temp).color:=cl_gesperrt;
          form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
          form_richedit_menue.Parent:=nil;
          form_richedit_menue.visible:=false;
        end;  
		  end;

		  if (Temp is   TBitBtn) then
		  begin
				 TBitBtn(temp).enabled:=false;
		  end;

		 if (Temp is   Tradiogroup) then
		  begin
				 Tradiogroup(temp).enabled:=false;
				 //Tradiogroup(temp).color:=cl_gesperrt;
		  end;
                  if (Temp is   Tradiobutton) then
		  begin
				 Tradiobutton(temp).enabled:=false;
		  end;

		if (Temp is   Tspinedit) then
		  begin
				 Tspinedit(temp).readonly:=true;
				 Tspinedit(temp).color:=cl_gesperrt;
			end;
		if (Temp is   Tcombobox) then
		  begin
				 Tcombobox(temp).enabled:=false;
				 Tcombobox(temp).color:=cl_gesperrt;
			end;

		if (Temp is   Tmemo) then
		  begin
				 Tmemo(temp).readonly:=true;
				 Tmemo(temp).color:=cl_gesperrt;
		  end;

		  if (Temp is   Tspeedbutton) then
		  begin
				 Tspeedbutton(temp).enabled:=false;
		  end;

		  if (Temp is   Ttoolbutton) then
		  begin
				 Ttoolbutton(temp).enabled:=false;
		  end;
		  if (Temp is   Tedit) then
		  begin
			  Tedit(temp).color:=cl_gesperrt;
			 Tedit(temp).readonly:=true;
		  end;

		  if (Temp is   Tmaskedit) then
		  begin
			 Tmaskedit(temp).color:=cl_gesperrt;
			 Tmaskedit(temp).readonly:=true;
		  end;

		  if (Temp is   Tcheckbox) then
		  begin
			  Tcheckbox(temp).enabled:=false;
			  //Tcheckbox(temp).color:=cl_gesperrt;
		  end;

		  if temp is tolecontainerext then
		  begin
			  tolecontainerext(temp).enabled:=false;
			  tolecontainerext(temp).ctl3d:=true;
			  tolecontainerext(temp).color:=cl_gesperrt;

		  end;

		  if temp is taudiogramm then
		  begin
			  taudiogramm(temp).enabled:=false;
			  taudiogramm(temp).color:=cl_gesperrt;
		  end;

		 { if temp is tstringaligngrid then
		  begin
			  tstringaligngrid(temp).enabled:=false;
			  tstringaligngrid(temp).color:=cl_gesperrt;
		  end; }

       if temp is tstringgrid then
		  begin
			  tstringgrid(temp).enabled:=false;
			  tstringgrid(temp).color:=cl_gesperrt;
		  end;

	 end; //tag=1
	 end;
except
	//twincontrol(komponente).visible:=true;
end;
end;


//########################################################################################################################################
procedure tform_main.entsperren(komponente: tcomponent);
//########################################################################################################################################
var
a,i,j,k,h: integer;
temp:tcontrol;
farbe:tcolor;
begin
farbe:=cl_entsperrt;
if  twincontrol(komponente).tag=5 then exit;  //kompoenten mit tag=5 werden nicht entgesperrt;

try
//twincontrol(komponente).visible:=false;

// if komponente=tabsheet_anamnese then ana_toolbar_entsperren;


 for I := twincontrol(komponente).controlcount - 1 downto 0 do
	begin
	  Temp := twincontrol(komponente).controls[I];
	  if temp.tag<>5 then
	  begin
		 if temp is tpagecontrol then entsperren(temp);    //12.10.17 wieder aktiviert
     //if temp is tpagecontrol then entsperren(tpagecontrol(temp).ActivePage);
		 if temp is ttabsheet then entsperren(temp);
		 if temp is tgroupbox then entsperren(temp);
		 if temp is tscrollbox then entsperren(temp);
		 if temp is tpanel then entsperren(temp);
		 if Temp is Ttoolbar then entsperren(temp);
		 if temp.name='' then temp:=twincontrol(komponente);

     if temp is tnotebook then
		  begin
		  	//for k:=0 to tnotebook(temp).ControlCount-1 do entsperren(temp.Components[k]) ;
        entsperren(tnotebook(temp).Components[tnotebook(temp).pageindex]);
		  end;


		  if (Temp is tdbedit) then
		  begin
			  tdbedit(temp).color:=cl_entsperrt;
			  tdbedit(temp).readonly:=false;
        tdbedit(temp).OnChange:=self.db_komp_onchange;

		  end;
		  if (Temp is  TDBRadioGroup) then
		  begin
				TDBRadioGroup(temp).readonly:=false;
        TDBRadioGroup(temp).OnChange:=self.db_komp_onchange;
		  end;
			 if (Temp is  TDBimage) then
		  begin
				TDBimage(temp).color:=cl_entsperrt;
		  end;
		  if (Temp is  TDBmemo) then
		  begin
				TDBmemo(temp).color:=cl_entsperrt;
				TDBmemo(temp).readonly:=false;
        tdbmemo(temp).Font.Size:=fontsize_anmerkung;
        tdbmemo(temp).OnChange:=self.db_komp_onchange;
		  end;

      if (Temp is  TDBrichedit) then
		  begin
				tDBrichedit(temp).color:=cl_entsperrt;
				tDBrichedit(temp).readonly:=false;
        if tdbrichedit(temp).text='' then tdbrichedit(temp).Font.Size:=fontsize_anmerkung;
        tDBrichedit(temp).OnChange:=self.db_komp_onchange;
		  end;
      
		  if (Temp is  Trichedit) and (assigned(temp)) then
		  begin
        trichedit(temp).readonly:=false;// enabled:=true;
			 	trichedit(temp).color:=cl_entsperrt;
        if trichedit(temp).text='' then
        begin
          //trichedit(temp).Clear;
          trichedit(temp).Font.Size:=fontsize_anmerkung;
          trichedit(temp).Font.Name:='Arial';
        end;

        form_richedit_menue.ini(trichedit(temp));
        form_richedit_menue.Parent := trichedit(temp).parent;
        form_richedit_menue.Align := altop;
        form_richedit_menue.visible:=true;
		  end;

		 if (Temp is  tDBLookupComboBox) then
		  begin
				tDBLookupComboBox(temp).readonly:=false;
				tDBLookupComboBox(temp).color:=cl_entsperrt;
        //	tDBLookupComboBox(temp).OnChange:=self.db_komp_onchange;
		  end;

		  if (Temp is  tDBComboBox) then
		  begin
				tDBComboBox(temp).readonly:=false;
				tDBComboBox(temp).color:=cl_entsperrt;
        tDBComboBox(temp).OnChange:=self.db_komp_onchange;
		  end;


		  if (Temp is  tDBCombo_num) then
		  begin
				tDBCombo_num(temp).readonly:=false;
				tDBCombo_num(temp).color:=cl_entsperrt;
        tDBCombo_num(temp).OnChange:=self.db_komp_onchange;
		  end;

		  if (Temp is  tDBcheckBox) then
		  begin
				tDBcheckBox(temp).readonly:=false;
        //tDBcheckBox(temp).OnChange:=self.db_komp_onchange;
		  end;

		 if (Temp is   TBitBtn) then
		  begin
				 TBitBtn(temp).enabled:=true;
		  end;

		 if (Temp is   Tradiogroup) then
		  begin
				 Tradiogroup(temp).enabled:=true;
			  //if temp.tag=10 then Tradiogroup(temp).setfocus;
		  end;
      if (Temp is   Tradiobutton) then
		  begin
				 Tradiobutton(temp).enabled:=true;
		  end;

		if (Temp is   Tspinedit) then
		  begin
				 Tspinedit(temp).readonly:=false;
				 Tspinedit(temp).color:=cl_entsperrt;
			  //if temp.tag=10 then Tspinedit(temp).setfocus;
		  end;
		if (Temp is   Tcombobox) then
		  begin
				 Tcombobox(temp).enabled:=true;
				 Tcombobox(temp).color:=cl_entsperrt;
			  //if temp.tag=10 then Tcombobox(temp).setfocus;
			end;

		if (Temp is   Tmemo) then
		  begin
				 Tmemo(temp).readonly:=false;
				 Tmemo(temp).color:=cl_entsperrt;
         tmemo(temp).Font.Size:=fontsize_anmerkung;
		  end;

		  if (Temp is   Tspeedbutton) then
		  begin
				 Tspeedbutton(temp).enabled:=true;
		  end;

		  if (Temp is   Ttoolbar) then
		  begin
         Ttoolbar(temp).enabled:=true;
      end;

		  if (Temp is   Tedit) then
		  begin
				Tedit(temp).color:=cl_entsperrt;
				Tedit(temp).readonly:=false;
		  end;

		  if (Temp is   Tmaskedit) then
		  begin
				Tmaskedit(temp).color:=cl_entsperrt;
				Tmaskedit(temp).readonly:=false;
		  end;

      if (Temp is   Tcheckbox) then
      begin
        Tcheckbox(temp).enabled:=true;
      end;

       if temp is tolecontainerext then
       begin
        tolecontainerext(temp).enabled:=true;
			  tolecontainerext(temp).ctl3d:=false;
        tolecontainerext(temp).color:=cl_entsperrt;
        mysql_d.ds_aenderung:=tolecontainerext(temp).Modified or mysql_d.ds_aenderung;

       end;

       if temp is taudiogramm then
       begin
          taudiogramm(temp).enabled:=true;
          taudiogramm(temp).color:=clwhite;
       end;

      { if temp is tstringaligngrid then
       begin
       	  tstringaligngrid(temp).enabled:=true;
       	  tstringaligngrid(temp).color:=clwhite;
       end;}

        if temp is tstringgrid then
       begin
        tstringgrid(temp).enabled:=true;
        tstringgrid(temp).color:=cl_entsperrt;;
       end;

        if temp is ttoolbutton then
	  begin
	  ttoolbutton(temp).enabled:=true;
	 end;

		  //if temp.tag=10 then twincontrol(temp).setfocus;
	end; //temp<>5
	end;
except
	//twincontrol(komponente).visible:=true;
end;
end;

//sperrung aus db l�schen und oberfl�che sperren (grau)
procedure tform_main.p_sperren(sperrobjekt,tabelle:tzquery; user_id:integer;komponente: array of tcomponent);
var
sperrobjekt_name:string;
i:integer;
nummer: int64;
begin
try
	if sperrobjekt.Active {and ( sperrobjekt.recordcount>=0)} then nummer:=getbigint(tabelle,'nummer') else exit;
	//sperrobjekt_name:=tabellen_name(sperrobjekt.sql.text);
  sperrobjekt_name:=tabellen_name(tabelle.sql.text);

	if  mysql_d.ds_entsperren(sperrobjekt_name,nummer,user_id,tabelle) then  //hier Transaktion   ende
	for i:=0 to high(komponente) do
	begin
		  tcontrol(komponente[i]).visible:=false;
		  sperren(komponente[i]);
		  tcontrol(komponente[i]).visible:=true;
	end;
except
end;
end;

//sperren in DB   und oberfl�che eingabebereit
function tform_main.p_entsperren(sperrobjekt,tabelle:tzquery;  user_id:integer;komponente: array of tcomponent;refresh:boolean):boolean;
var
f,tab_name,sperrobjekt_name,mess,query,nr:string;
i:integer;
nummer:int64;
bookmark: TBookmark;
begin
	result:=false;
  if tabelle=nil then exit;
  nummer:=getbigint(tabelle,'nummer');
	if ((not einzelplatz) and refresh) then
    begin
     //tabelle.DisableControls;
     tabelle.refresh;
     tabelle.Locate('nummer',nummer,[]);
     //tabelle.EnableControls;
    end;


	//sperrobjekt_name:=tabellen_name(sperrobjekt.sql.text);
  sperrobjekt_name:=tabellen_name(tabelle.sql.text);
	if mysql_d.ds_sperren(sperrobjekt_name,nummer,user_id) then  //transaktion beginn
	begin
    tabelle.edit; //091021
		for i:=0 to high(komponente) do
		begin
			tcontrol(komponente[i]).visible:=false;
			entsperren(komponente[i]);
			tcontrol(komponente[i]).visible:=true;

		end;
		result:=true;

	end
	else
	begin
		query:=format('select user_id from sperren where sperrobjekt="%s" and sperrobjekt_id=%s',[sperrobjekt_name,inttostr(nummer)]);
		f:=mysql_d.Feldinhalt(query,0);
      if f<>inttostr(user_id) then
      begin
			if f<>'' then 	mess:=  'Der Datensatz ist von dem Benutzer "'+mysql_d.get_user(strtoint(f))+'" gesperrt!'
			else  mess:=  'Der Datensatz ist von dem Benutzer gesperrt!';
         mess:=mess+#13+#10+'siehe Optionen ->Fehlerbehandlung';
			showmessage(mess);
      end;
	end;
end;



procedure TForm_main.pagecontrol_mainChanging(Sender: TObject;
	var AllowChange: Boolean);
begin
 if speichert_gerade then
 begin
  allowchange:=false;
  exit;
 end;
 if modus_history then history_entfernen;

 allowchange:=true;
 dbspeichern(false);
 menue_leeren;

 alle_olecontainer_schliessen;
 akt_abteilung:=0;
  akt_arbeitsplatz:=0;
 if (pagecontrol_main.activepage=tabsheet_main) and (dbtreeview.a_rubrik=8) then
	  begin
			Arbeitsmedizin.DestroyObject;
	  end;

 if (pagecontrol_main.ActivePage=tabsheet_firma) then
 begin
	//if (datamodul.q_Firma.Eof and datamodul.q_Firma.bof) then
  if kein_datensatz(datamodul.q_Firma) then
	 begin
		showmessage('Bitte zuerst eine Firma ausw�hlen/filtern oder neu anlegen.');
		allowchange:=false;
	 end
	 else
		 mitarbeiter_filtern;
	end;

 if (pagecontrol_main.ActivePage=tabsheet_abteilung)  then
 begin
 try

  akt_abteilung:=form_ap.dbtreeview_k1.get_index(1);
  akt_arbeitsplatz:=form_ap.dbtreeview_k1.get_index(2);
  form_ap.release;
  form_ap:=nil;
  application.processmessages;
 except
 end;
 end;
 last_ts:=pagecontrol_main.ActivePage;

//	if pagecontrol_main.ActivePage=tabsheet_anamnese then ana_speichern;
end;

procedure tform_main.mitarbeiter_filtern;
var i:integer;
	  query,ap_abt,ap,abt:string;
begin
    if akt_abteilung>0 then ap_abt:=format('and (i_abteilung=%s)',[inttostr(akt_abteilung)]);
    if akt_arbeitsplatz>0 then ap_abt :=ap_abt+format(' and (i_arbeitsplatz=%s)',[inttostr(akt_arbeitsplatz)]);

	  i:=datamodul.q_firma['nummer'];
	  if archiv_show then query:=format('select * from mitarbeiter where i_firma=%d %s order by name, vorname',[i,ap_abt])
	  else query :=format('select * from mitarbeiter where i_firma=%d %s and archiv<>"X" order by name, vorname',[i,ap_abt]);
	  datamodul.sql(true,datamodul.q_mitarbeiter,query,'');

    query:='select kuerzel from abteilung where nummer='+inttostr(akt_abteilung);
    abt:=mysql_d.Feldinhalt(query,0);

    query:='select name from a_platz where nummer='+inttostr(akt_arbeitsplatz);
    ap:=mysql_d.Feldinhalt(query,0);

    if akt_abteilung>0 then label_prob_abt.Caption:=   'Abteilung   : '+abt else label_prob_abt.Caption:='Abteilung    nicht ausgew�hlt';
    if akt_arbeitsplatz>0 then label_prob_ap.Caption:= 'Arbeitsplatz: '+ap else label_prob_ap.Caption:=  'Arbeitsplatz nicht ausgew�hlt';
end;

procedure tform_main.namenanzeigen;
var
	namestring:string;
	alter:integer;
begin
namestring:='';
if kein_datensatz(datamodul.q_mitarbeiter) then exit;
try
if datamodul.q_mitarbeiter['Name']<>null then  namestring:= string(datamodul.q_mitarbeiter['Name']);
if datamodul.q_mitarbeiter['vorname']<>null then  namestring:=namestring+ ', '+string(datamodul.q_mitarbeiter['vorname']);
if datamodul.q_mitarbeiter['geb_dat']<>null then
	 begin
		alter:=(date()-datamodul.q_mitarbeiter['geb_dat']) div 365;
		namestring:=namestring+ ', '+datetostr(datamodul.q_mitarbeiter['geb_dat'])+' ('+inttostr(alter)+'Jahre)';
	 end;

except
end;
//if namestring <>', ' then
history_add(namestring,getbigint(datamodul.q_mitarbeiter,'nummer'));
end;


procedure TForm_main.m_UntersuchungClick(Sender: TObject);
var ber:integer;
query:string;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;

speichern;
datamodul.sql(true,datamodul.q_1,'select * from typ','reihenfolge');

datamodul.sql(true,datamodul.q_2,'select * from typ where bereich=1','reihenfolge'); //untersuchubg


query:='select * from unt_befunde where unt_befunde.storno=0 and unt_befunde.i_typ=0 ';
datamodul.sql(false,datamodul.q_3,query,'');

query:='select * from unt_befunde';
datamodul.sql(false,datamodul.q_4,query,'');

query:='select typ.goae, typ.untersuchung, typ.euro,unt_labor.i_typ,unt_labor.storno,unt_labor.prioritaet,unt_labor.abrechnen, unt_labor.eu,unt_labor.nu,unt_labor.ngu,unt_labor.memo  from unt_labor '+
               ' left join typ on(unt_labor.i_typ=typ.nummer ) where  unt_labor.ii_typ=0 ';
datamodul.sql(false,datamodul.q_5,query,'');
datamodul.q_4.first;
datamodul.q_5.first;

form_untersuchungstyp:=tform_untersuchungstyp.Create(self);
form_untersuchungstyp.HelpContext:=10810;
form_untersuchungstyp.DBCombo_num_unt_art.lookup_laden;
form_untersuchungstyp.DBCombo_num_nach_paw.lookup_laden;
form_untersuchungstyp.DBCombo_num_grundlage.lookup_laden;

form_untersuchungstyp.TreeView.mysqlquery:=datamodul.q_2;
form_untersuchungstyp.treeview.mysql_feld:='untersuchung';
form_untersuchungstyp.treeview.liste_lesen;

form_untersuchung_berechtigung(ber);
Form_untersuchungstyp.HelpContext:=21;
Form_untersuchungstyp.showmodal;


form_untersuchungstyp.release;
form_untersuchungstyp:=nil;
datamodul.q_typ.refresh;
end;


procedure TForm_main.m_ImpfungenClick(Sender: TObject);
var ber:integer;
		query:string;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
speichern;
datamodul.sql(true,datamodul.q_2,'select * from typ  where bereich=2','reihenfolge');
datamodul.sql(true,datamodul.q_1,'select * from typ','untersuchung');
datamodul.q_1.first;
datamodul.q_2.first;

query:='select bereich.*, unt_befunde.prioritaet,unt_befunde.abrechnen, unt_befunde.eu,unt_befunde.nu,unt_befunde.ngu,unt_befunde.memo from bereich left join unt_befunde on(unt_befunde.i_befunde=bereich.nummer ) '+
               ' left join typ on (unt_befunde.i_typ=typ.nummer) where bereich.nummer=0 ';
datamodul.sql(false,datamodul.q_3,query,'');
query:='select typ.goae, typ.untersuchung, typ.euro,unt_labor.i_typ,unt_labor.storno,unt_labor.prioritaet,unt_labor.abrechnen, unt_labor.eu,unt_labor.nu,unt_labor.ngu,unt_labor.memo  from unt_labor '+
               ' left join typ on(unt_labor.i_typ=typ.nummer ) where  (unt_labor.ii_typ=0 ) ';  //and labor.nummer=0 ?????
datamodul.sql(false,datamodul.q_5,query,'');

form_untersuchungstyp:=tform_untersuchungstyp.Create(self);
form_untersuchungstyp.HelpContext:=10820;

form_untersuchung_berechtigung(ber);

Form_untersuchungstyp.bereich:=2;
Form_untersuchungstyp.TabSheet_unt.Caption:='Impfung';
Form_untersuchungstyp.TabSheet_impfstoff.tabVisible:=true;
Form_untersuchungstyp.TabSheet_zug_befunde.tabVisible:=false;
Form_untersuchungstyp.TabSheet_zug_labor.tabVisible:=false;
Form_untersuchungstyp.caption:='Bearbeiten der Impfungen';
form_untersuchungstyp.Notebook.PageIndex:=1;
form_untersuchungstyp.TreeView.mysqlquery:=datamodul.q_2;
form_untersuchungstyp.treeview.mysql_feld:='untersuchung';
form_untersuchungstyp.treeview.liste_lesen;
application.ProcessMessages;
Form_untersuchungstyp.HelpContext:=26;
Form_untersuchungstyp.showmodal;

form_untersuchungstyp.release;
form_untersuchungstyp:=nil;
datamodul.q_typ.filtered:=false;
datamodul.q_typ.refresh;
datamodul.q_impfstoff.filtered:=false;
datamodul.q_impfstoff.refresh;
end;

procedure TForm_main.m_labClick(Sender: TObject);
var ber:integer;
query:string;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
try
  speichern;
	form_untwahl:=tform_untwahl.Create(self);                                     
	form_untwahl.HelpContext:=10830;
	if ber<2 then
	begin
	  form_untwahl.ToolButton_new.Visible:=false;
	  form_untwahl.ToolButton_delete.Visible:=false;
	end;
	if ber<1 then
	begin
	  form_untwahl.ToolButton_edit.Visible:=false;
	  form_untwahl.ToolButton_save.Visible:=false;
	end;

{	query:='select bereich.*, unt_befunde.prioritaet, unt_befunde.eu,unt_befunde.nu,unt_befunde.ngu,unt_befunde.memo from bereich left join unt_befunde on(unt_befunde.i_befunde=bereich.nummer ) left join typ on (unt_befunde.i_typ=typ.nummer) limit 0 ';
	datamodul.sql(false,datamodul.q_3,query,'');
	query:='select typ.goae, typ.untersuchung, typ.euro,unt_labor.i_typ,unt_labor.storno,unt_labor.prioritaet, unt_labor.eu,unt_labor.nu,unt_labor.ngu,unt_labor.memo  from unt_labor left join typ on(unt_labor.i_typ=typ.nummer )  limit 0 ';
	datamodul.sql(false,datamodul.q_5,query,'');}

	datamodul. sql_new(true,datamodul.q_typ_tree,format('select * from typ where bereich = %d',[3]), 'reihenfolge');
  form_untwahl.einstellung:=true;
	form_untwahl.notebook.visible:=true;
	form_untwahl.notebook.pageindex:=2;
	form_untwahl.multiselect:=false;
	form_untwahl.ToolBar.Visible:=true;
	form_untwahl.Panel_botttom.Visible:=false;
	form_untwahl.caption:='Labor';
	form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.Tree.bereich_wert:=3;
	form_untwahl.tree.mysqlquery:=datamodul.q_typ_tree;
	form_untwahl.tree.mysql_feld:='untersuchung';
	form_untwahl.tree.liste_lesen;

  form_untwahl.HelpContext:=24;
	form_untwahl.showmodal;
	datamodul.q_typ.refresh;
finally
	form_untwahl.release;
	application.processmessages;
end;
end;

procedure TForm_main.VtChartClick(Sender: TObject);
begin
exit;
end;

procedure TForm_main.VtChartDblClick(Sender: TObject);
begin
exit;
end;




procedure TForm_main.m_KBesonderheitenClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
	ber:=b.b_string('Besonderheiten');
	if ber=-1 then exit;
  	speichern;
	datamodul. sql_new(true,datamodul.q_besonderheitenListe_tree,'select * from besonderheitenliste', 'reihenfolge');
	form_besonderheiten:=tform_besonderheiten.Create(self);
   form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
	form_besonderheiten.HelpContext:=20;
	if ber<2 then
	begin
	  form_besonderheiten.ToolButton_new.Visible:=false;
	  form_besonderheiten.ToolButton_del.Visible:=false;
	end;
	if ber<1 then
	begin
	  form_besonderheiten.ToolButton_edit.Visible:=false;
	end;

	form_besonderheiten.tree.DragMode:=dmautomatic;
	form_besonderheiten.multiselect:=false;
	form_besonderheiten.ToolBar.Visible:=true;
	form_besonderheiten.Panel_bottom.Visible:=false;
  //grid_einstellen(form_tabellen.DBgrid_tabellen);


	form_besonderheiten.showmodal;
	//datamodul.q_besonderheitenListe.refresh;
   datamodul.q_besonderheitenListe.Refresh;
    datamodul.q_besonderheitenListe_tree.Refresh;
	form_besonderheiten.release;
end;

procedure TForm_main.m_bedienerClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;

ber:=b.b_string('Berechtigungen');
if ber=-1 then exit;
speichern;
try
   form_tabellen:=tform_tabellen.Create(self);
   form_tabellen.HelpContext:=54;
   form_tabellen_berechtigung(ber);

   form_tabellen.suchfeld:='Untersucher';
   form_tabellen.Caption:='Untersucher';

   form_tabellen.Notebook.PageIndex:=1;
   form_tabellen.BitBtn_bwahl.visible:=false;

   datamodul.sql(true,datamodul.q_1,'select * from untersucher','');
   //form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='Bmp_nr';
   form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='untersucher';
   form_tabellen.DBGrid_tabellen.Columns[0].width:=500;
   form_tabellen.DBEdit_untersucher.DataField:='untersucher';
   form_tabellen.DBCombo_num_zeitkonto.DataField:='i_user';
   form_tabellen.DBEdit_untersucher2.DataField:='untersucher_2';
   form_tabellen.DBEdit_untersucher3.DataField:='untersucher_3';
   form_tabellen.DBEdit_untersucher4.DataField:='untersucher_4';
   form_tabellen.DBEdit_untersucher_email.DataField:='untersucher_email';
   form_tabellen.DBEdit_untersucher_telefon.DataField:='untersucher_telefon';
   form_tabellen.DBComboBox_u1.datafield:='f_1';
   form_tabellen.DBComboBox_u2.datafield:='f_2';
   form_tabellen.DBComboBox_u3.datafield:='f_3';
   form_tabellen.DBComboBox_u4.datafield:='f_4';
   form_tabellen.DBComboBox_u5.datafield:='f_email';
   form_tabellen.DBComboBox_u6.datafield:='f_telefon';
   form_tabellen.DBCombo_num_zeitkonto.lookup_laden;
   grid_einstellen(form_tabellen.dbgrid_tabellen);
   form_tabellen.showmodal;
finally
   form_tabellen.release;
   dbcombo_laden;
end;
end;

procedure TForm_main.m_FristenClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
speichern;
form_tabellen:=tform_tabellen.Create(self);
form_tabellen.HelpContext:=48;    //2010
form_tabellen_berechtigung(ber);

form_tabellen.suchfeld:='frist';
form_tabellen.Notebook.PageIndex:=3;
datamodul.sql_new(true,datamodul.q_1,'select * from fristen','i_frist');
form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='frist';
//form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='i_frist';
//form_tabellen.DBGrid_tabellen.Columns[0].width:=100;
form_tabellen.DBEdit_frist2.DataField:='i_frist';
form_tabellen.DBEdit_frist1.DataField:='frist';
grid_einstellen(form_tabellen.dbgrid_tabellen);
form_tabellen.caption:='Fristen';
form_tabellen.showmodal;
form_tabellen.release;
//datamodul.q_1.BeforeScroll:=nil;
//datamodul.q_fristen.refresh;
dbcombo_laden;
end;

procedure TForm_main.m_StatusClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;

form_tabellen:=tform_tabellen.Create(self);
form_tabellen_berechtigung(ber);

form_tabellen.suchfeld:='status';
form_tabellen.Caption:='Status';
form_tabellen.Notebook.PageIndex:=4;
form_tabellen.ToolButton_neu.enabled:=false;
//form_tabellen.ToolButton_del.enabled:=false;
datamodul.sql(true,datamodul.q_1,'select * from status','');

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='Bmp_nr';
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='status';

form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
form_tabellen.DBEdit_status.DataField:='status';
  grid_einstellen(form_tabellen.dbgrid_tabellen);


form_tabellen.showmodal;
form_tabellen.release;
//datamodul.q_status.refresh;
dbcombo_laden;
end;

procedure TForm_main.m_terminartClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('anwendung');
if ber=-1 then exit;
  speichern;
form_tabellen:=tform_tabellen.Create(self);
form_tabellen.HelpContext:=10800;

form_tabellen_berechtigung(ber);

form_tabellen.Notebook.PageIndex:=2;
form_tabellen.Caption:='Termin-Art';
form_tabellen.suchfeld:='name' ;
datamodul.sql(true,datamodul.q_1,'select * from art','');
form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='Bmp_nr';
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='name';

form_tabellen.DBGrid_tabellen.Columns[0].width:=17;
form_tabellen.DBEdit_untersuchung.DataField:='name';
grid_einstellen(form_tabellen.dbgrid_tabellen);


form_tabellen.showmodal;
form_tabellen.release;
dbcombo_laden;
end;

function TForm_main.gehezu_firma(i_firma:integer):boolean;
begin
   pagecontrol_main.ActivePage:=tabsheet_firma;
   pagecontrol_firma.ActivePage:=tabsheet_firmenadresse;
   application.ProcessMessages;
   datamodul.q_firma.locate('nummer',i_firma,[]);
	application.ProcessMessages;
end;



function TForm_main.gehezu_firma_ansprechpartner(i_firma:integer;i_nummer:int64):boolean;
begin
   pagecontrol_main.ActivePage:=tabsheet_firma;
   //pagecontrol_firma.ActivePage:=TabSheet_ansprechpartner;
   application.ProcessMessages;
   datamodul.q_firma.locate('nummer',i_firma,[]);
   pagecontrol_firma.ActivePage:=TabSheet_ansprechpartner;
   application.ProcessMessages;

   PageControl_firmaChange(self);
   application.ProcessMessages;
   datamodul.q_firma_ansprech.locate('nummer', i_nummer,[]);

	application.ProcessMessages;
end;


function TForm_main.gehezu_firma_sollist(i_firma:integer;i_nummer:int64):boolean;
begin
   pagecontrol_main.ActivePage:=tabsheet_firma;
   //pagecontrol_firma.ActivePage:=TabSheet_ansprechpartner;
   application.ProcessMessages;
   datamodul.q_firma.locate('nummer',i_firma,[]);
   pagecontrol_firma.ActivePage:=TabSheet_sollist;
   application.ProcessMessages;

   PageControl_firmaChange(self);
   application.ProcessMessages;
   datamodul.q_firma_sollist.locate('nummer', i_nummer,[]);

	application.ProcessMessages;
end;

function TForm_main.gehezu_aktuntersuchung(i_stammdaten,i_nummer,i_bereich:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
   tree_befunde_filtern(true);
   dbtreeview.FullExpand;
	result:=dbtreeview.gehe_zu_nummer_bereich(i_nummer,i_bereich); //hier fehler
	application.ProcessMessages;
   namenanzeigen;
end;


function TForm_main.gehezuuntersuchung(i_art:integer; i_stammdaten,i_nummer:int64{;i_firma:integer; table:tzquery}):boolean;
begin
result:= gehezu_stammdaten(i_stammdaten);
if (not result) then exit;
pagecontrol_main.ActivePage:=tabsheet_main;

if (i_art<=3) and (i_art>=1) then
begin
	tree_befunde_filtern(true);
  dbtreeview.Expand_e1;
	result:=dbtreeview.gehe_zu_datensatz(i_art+1,i_nummer); //hier fehler
	application.ProcessMessages;

end;
namenanzeigen;
end;


function TForm_main.gehezu_ambulanz(i_stammdaten,i_nummer:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
	tree_befunde_filtern(true);
   dbtreeview.Expand_e1;
	result:=dbtreeview.gehe_zu_datensatz(5,i_nummer); //hier fehler
	application.ProcessMessages;
   namenanzeigen;
end;

function TForm_main.gehezu_anamnese(i_stammdaten,i_nummer:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
	tree_befunde_filtern(true);
   dbtreeview.Expand_e1;
	dbtreeview.Selected:=dbtreeview.Items.GetFirstNode;// gehe_zu_datensatz(5,i_nummer); //hier fehler
	application.ProcessMessages;
   namenanzeigen;
   result:=true;
end;

function TForm_main.gehezubesonderheit(i_stammdaten,i_nummer:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
	tree_befunde_filtern(true);
   dbtreeview.Expand_e1;
	result:=dbtreeview.gehe_zu_datensatz(7,i_nummer); //hier fehler
	application.ProcessMessages;
   namenanzeigen;
end;

function TForm_main.gehezudokument_proband(i_stammdaten,i_nummer:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
	tree_befunde_filtern(true);
   dbtreeview.Expand_e1;
	result:=dbtreeview.gehe_zu_datensatz(8,i_nummer);
	application.ProcessMessages;
  namenanzeigen;
end;


function TForm_main.gehezu_diagnosen(i_stammdaten,i_nummer:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
	tree_befunde_filtern(true);
   dbtreeview.Expand_e1;
	result:=dbtreeview.gehe_zu_datensatz(6,i_nummer); //hier fehler
	application.ProcessMessages;
   namenanzeigen;
end;


function TForm_main.gehezu_kontakte(i_stammdaten,i_nummer:int64):boolean;
begin
   result:= gehezu_stammdaten(i_stammdaten);
   if (not result) then exit;
   pagecontrol_main.ActivePage:=tabsheet_main;
	tree_befunde_filtern(true);
   dbtreeview.Expand_e1;
	result:=dbtreeview.gehe_zu_datensatz(12,i_nummer); //hier fehler
	application.ProcessMessages;
   namenanzeigen;
end;

function TForm_main.gehezutext(i_firma:integer;i_nummer:int64):boolean;
begin
   pagecontrol_main.ActivePage:=tabsheet_firma;
   pagecontrol_firma.ActivePage:=TabSheet_FirmenAdresse;
   application.ProcessMessages;
   datamodul.q_firma.locate('nummer',i_firma,[]);
   pagecontrol_firma.ActivePage:=TabSheet_firma_texte;
   application.ProcessMessages;

   PageControl_firmaChange(self);
   application.ProcessMessages;
   datamodul.q_texte.locate('nummer', i_nummer,[]);
   dbgridext_texte.SetFocus;
	application.ProcessMessages;
end;

function TForm_main.gehezu_rechnung_firma(i_firma:integer;i_nummer:int64):boolean;
begin
   pagecontrol_main.ActivePage:=tabsheet_firma;
   pagecontrol_firma.ActivePage:=TabSheet_FirmenAdresse;
   application.ProcessMessages;
   datamodul.q_firma.locate('nummer',i_firma,[]);
   pagecontrol_firma.ActivePage:=TabSheet_rechnungen;
   application.ProcessMessages;

   PageControl_firmaChange(self);
   application.ProcessMessages;
   datamodul.q_rechnung.locate('nummer', i_nummer,[]);
   dbgridext_rechnung.SetFocus;
	application.ProcessMessages;
end;



function TForm_main.gehezuprojekt(i_firma:integer;i_nummer:int64):boolean;
begin
   pagecontrol_main.ActivePage:=tabsheet_firma;
   pagecontrol_firma.ActivePage:=TabSheet_FirmenAdresse;
   
   datamodul.q_firma.locate('nummer',i_firma,[]);
   application.ProcessMessages;
   pagecontrol_main.ActivePage:=tabsheet_abteilung;
   Form_main.pagecontrol_mainChange(self);
   application.ProcessMessages;
   //form_ap.dbtreeview_k1.gehe_zu_datensatz();
   //abt_manager(i_firma,0,'all',datamodul.q_10, i_nummer);
end;




procedure TForm_main.PageControl_langfristChanging(Sender: TObject;
	var AllowChange: Boolean);
begin
	dbspeichern(false);
end;


procedure tform_main.form_positionieren(formular: tform);
const
b=1366;
h=768;
var hoehe,breite,schrift:integer;

begin

    schrift := pixelsperinch-96;

	 hoehe:=screen.Height-30;
    breite:=screen.width;
    formular.AutoSize:=false;
    formular.scaled:=true;

    formular.PixelsPerInch := Screen.PixelsPerInch;

    if (formular.PixelsPerInch <> 96) and (formular<>form_main) then
    begin
      formular.Width := LongInt(formular.Width * PixelsPerInch) div 96;
      formular.Height := LongInt(formular.Height * PixelsPerInch) div 96;
    end;
   if scale_i1>0 then
   begin
      formular.scaleby(scale_i1,100);
      if  (formular<>form_main) then
      begin
         formular.width:=round(formular.width*scale_i2/100);
         formular.height:=round(formular.height*scale_i3/100);
      end;
   end;

	if (formular<>form_main) then
  begin
    formular.height:=min(formular.height, height-20);
    formular.Width:=min(formular.Width,Width-10);
  end;
	formular.height:=min(formular.height, hoehe);
	formular.Width:=min(formular.Width,breite);

	formular.Top:=top+(height div 2)-(formular.Height div 2);
	if formular.top+formular.Height>hoehe then 	formular.top:=hoehe-formular.Height;
	formular.top:=max(formular.top,0);
	formular.Left:= (breite div 2) - (formular.width div 2);
	if formular.left+formular.Width>screen.Width then  formular.left:=screen.width-formular.width;
	formular.left:=max( formular.left,0);



    //formular.ScaleBy(scale_i1,scale_i2);
    //changescale(scale_i1,scale_i2);
    //Realign;









 { schrift := pixelsperinch-96;
  if screen.width>b then begin
    if schrift>0 then
      //f�r gro�e Schriften
      formular.ScaleBy(round(((screen.width/b)*100)-((screen.width/b-1)*100)),100)
    else
      //f�r kleine Schriften
      formular.ScaleBy(round(((screen.width/b)*100)),100);
  end
  else begin
    if screen.width=b then
      formular.ScaleBy(round(((screen.width/b)*100)-schrift),100)
    else begin
      if schrift>0 then
        //f�r gro�e Schriften
        formular.ScaleBy(round((100-(b/screen.width-1)*100)-(b/screen.width-1)*100),100)
      else
        //f�r kleine Schriften
        formular.ScaleBy(round((100-(b/screen.width-1)*100)),100);
    end;
  end; }


{  procedure FitForm(F :TForm);
// Anpassung des Formulares an die Monitoraufl�sung
const SW: Integer = 1024;
SH: Integer = 768;
FS: Integer = 96;
FL: Integer = 120;
AR: Real = 0.625;
var X,Y,K: Integer;
Z: Real;
begin
with F do begin
X := Screen.Width;
Y := Screen.Height;
K := Font.PixelsPerInch;
Scaled := True;
Z := Y/X;
if (Z > AR) then ScaleBy(X,SW) else ScaleBy(Y,SH);
if (K <> FL) then ScaleBy(FL,K);
WindowState := wsMaximized;
end;
end;}

end;


procedure grid_einstellen(sender:tobject);
var
j,i,add,summe, rest:integer;
grid: tdbgrid;
begin
grid:=tdbgrid(sender);
summe:=0;
j:=0;
rest:=1;
try
application.ProcessMessages;
if (grid.Columns.Count=1) or (grid.Width-22<grid.Columns[0].width) then grid.Columns[0].width:=grid.Width-22;
if dgindicator in grid.Options then rest:=13;

for i:=0 to grid.Columns.Count-1 do
begin
	if grid.Columns[i].width>70 then
	begin
		inc(j);
		summe:=summe+ grid.Columns[i].width+1; //+1
	end
	else
		if grid.Columns[i].width>0 then rest:=rest+grid.Columns[i].width+1;
end;
//rest:=summe-grid.columns[0].width-rest;
//if grid.Columns[0].width<60 then inc(j); //s. n�chste zeile
if j>1 then add:=(grid.width-summe-rest-22) div (j) // j-1 erste spalte hat die festgesetzte groesse
else add:=(grid.width-summe-rest-22);

for i:=0 to grid.Columns.Count-1 do  //i:=1
begin
	if grid.Columns[i].width>70 then
   begin
			if (grid.Columns[i].width+add)>71 then
         grid.Columns[i].width:=grid.Columns[i].width+add
         else grid.Columns[i].width:=71;
    end;
end;

except
//kein column def.
end;
end;

procedure grid_suchen(sender: tobject);
var
i:integer;
temp:tcontrol;
begin
 for I := twincontrol(sender).controlcount - 1 downto 0 do
	begin
	  Temp := twincontrol(sender).controls[I];
	  if temp is tpagecontrol then grid_suchen(temp);
	  if temp is ttabsheet then grid_suchen(temp);
	  if temp is tgroupbox then grid_suchen(temp);
	  if temp is tpanel then grid_suchen(temp);

	  if (Temp is tdbgridext) then
	  begin
		grid_einstellen(temp);
	  end;
end;
end;


procedure TForm_main.Memo_befundChange(Sender: TObject);
begin
//Bereich_speichern;
end;

procedure TForm_main.ComboBoxClick(Sender: TObject);
begin
//Bereich_speichern;
end;


function tform_main.zeit_str(stunden, minuten:integer):string;
var
s_minuten:string;

begin
	stunden:=stunden+trunc(minuten/60);
  minuten:=trunc(60*frac(minuten/60));
  if (stunden>0) and (minuten<0) then
	begin
  	stunden:=stunden-1;
     minuten:=60-abs(minuten);
	end;
  begin
	  stunden:=abs(stunden);
 	  minuten:=abs(minuten);
  end;
	if minuten<=9 then s_minuten:='0'+inttostr(minuten) else s_minuten:=inttostr(minuten);
	result:=inttostr(stunden)+':'+s_minuten;
end;




procedure TForm_main.PageControl_firmaChange(Sender: TObject);
var
newmenu: tmenuitem;
query:string;
s_firma:string;

begin
try
//menue_leeren;
pagecontrol_firma.helpcontext:=5;
toolbuttonneu.Enabled:=true;
with datamodul do
begin
	if pagecontrol_firma.ActivePage=tabsheet_firmenadresse then
	begin
      pagecontrol_firma.helpcontext:=5;
	  tool_aktiv(datamodul.q_firma);
	  tabsheet_firmenadresse.tag:=max(0,tabsheet_firmenadresse.tag); //darf jeder l�e
	  grid_suchen(tabsheet_firmenadresse);
    query:='select count(*) from firma where storno=0';
    label_firma_count.Caption:='Anzahl gesamt: '+mysql_d.Feldinhalt( query,0)+', sichtbar: '+mysql_d.anzahl_datensaetze(datamodul.q_firma);
    try
      if form_main.Visible then	edit_firma_such.SetFocus;
    except
    end;
	END;

	if pagecontrol_firma.ActivePage=tabsheet_ansprechpartner then
	begin
      pagecontrol_firma.helpcontext:=5;
	  sql_relation('firma_ansprech','i_firma',q_firma,q_firma_ansprech,'funktion');
	  tool_aktiv(q_firma_ansprech);
	  grid_suchen(tabsheet_ansprechpartner);

	end;

    if pagecontrol_firma.ActivePage=tabsheet_firma_filter then
    begin
        pagecontrol_firma.helpcontext:=5;
        s_firma:=q_firma.findfield('nummer').asstring;
    	//query:= 'select firma_filter.nummer as nummer,firma_filter.i_firma, firma_filter.i_firma_filter_vorlage, firma_filter_vorlage.name as name, firma_filter_vorlage.reihenfolge as reihenfolge ';
        //query:=		query + format('from firma_filter left join firma_filter_vorlage on (firma_filter.i_firma_filter_vorlage=firma_filter_vorlage.nummer) where ((firma_filter.i_firma=%s ) and (firma_filter.storno=0))',[s_firma]);

        query:=format('select  * from firma_filter  where (i_firma=%s ) ',[s_firma]);
        sql_new(true,q_firma_filter,query,'reihenfolge');
        tool_aktiv(q_firma_filter);

    end;

	if pagecontrol_firma.ActivePage=tabsheet_firma_texte then
		begin
       pagecontrol_firma.helpcontext:=5;
       olecontainer_destroy(arbeitsmedizin_firma);
      s_firma:=q_firma.findfield('nummer').asstring;

      if b.b_string_silent('firma-rechnung')<=0 then
        query:=format('select * from texte where i_master=%s and modus=0 order by datum desc',[s_firma])
      else
        query:=format('select * from texte where i_master=%s order by datum desc',[s_firma]);

      sql_new(true,datamodul.q_texte,query,'');

			//sql_relation('texte','i_master',q_firma,q_texte,'datum desc');  Rechnungen m�ssen ausgeblendet werden
			tool_aktiv(datamodul.q_texte);
			//if not (q_texte.eof and q_texte.bof) then
         if datensatz(q_texte) then
			begin
			  newmenu:=tmenuitem.Create(self);
			  newmenu.Caption:='Dokument als Anhang verschicken';
			  newmenu.Name:='e_firma';
			  newmenu.OnClick:=Anhang_mailen;
			  email.Add(newmenu);
			end;

		end;


    if (pagecontrol_firma.ActivePage=tabsheet_firma_einstellungen) {and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung )}   then
    begin
        //pagecontrol_firma.helpcontext:=5;

        s_firma:=q_firma.findfield('nummer').asstring;
        query:=format('select  * from firma_zeiten_soll where (i_firma=%s ) and firma_zeiten_soll.storno=0 order by von desc',[s_firma]);
        sql_new(true,datamodul.q_firma_zeiten_soll,query,'');
        tool_aktiv(q_firma_zeiten_soll);

        PageControl_firma_einstellungenChange(self);

    end;


	if pagecontrol_firma.ActivePage=tabsheet_sollist then
      begin
          pagecontrol_firma.helpcontext:=4;
          //zeiterfassung;
          dbgridext_zeitabrechnung.SelectedRows.Clear;
          m_erbrachteZeiten.Visible:=true;
          einsatzzeiten_filtern;
         tool_aktiv(q_firma_sollist);
         toolbuttonneu.Enabled:=true;
      end
    else
      begin
          m_erbrachteZeiten.Visible:=false;
    end;


  if (pagecontrol_firma.ActivePage=TabSheet_firma_einstellungen) and (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung ) then
	begin
        //toolbuttonneu.Enabled:=false;
        PageControl_firma_einstellungenChange(self);
	end;


	if pagecontrol_firma.ActivePage=tabsheet_rechnungen then
	begin
		//sql_relation('rechnung','i_firma',q_firma,q_rechnung,'datum desc');
      query:='select * from re_positionen where nummer=0' ;
       sql_new(false,q_repositionen,query,'');
      s_firma:=q_firma.findfield('nummer').asstring;

      query:=format('select  * from rechnung  where (i_firma=%s ) and storno=0 order by datum desc, nummer desc ',[s_firma]);
      if storno_show then query:=format('select  * from rechnung  where (i_firma=%s ) order by datum desc, nummer desc ',[s_firma]);
      sql_new(false,q_rechnung,query,'');

      tool_aktiv(q_rechnung);
      application.ProcessMessages;
      menue_leeren;
      try
        newmenu:=tmenuitem.Create(self);
        if speicher_rechnungen then
          newmenu.Caption:='Rechnung drucken und speichern'
        else
          newmenu.Caption:='Rechnung drucken';
          newmenu.Name:='rechnung1';
          newmenu.OnClick:=BitBtn_redruckClick;

        m_drucken.Add(newmenu);
      except
        newmenu.Free;
      end;
      application.ProcessMessages;
      grid_einstellen(dbgridext_rechnung);
		grid_einstellen(dbgridext_repo);

	end;

   if tpagecontrol(sender).activepage<>nil then
      if tpagecontrol(sender).activepage.tag>=0 then
        begin  //berechtigt
           tpagecontrol(sender).tag:=tpagecontrol(sender).ActivePage.TabIndex; //PageIndex        //###fehler
        end
      else
        begin
         tpagecontrol(sender).activepage:=tpagecontrol(sender).pages[tpagecontrol(sender).tag];
         showmessage('Sie haben keine Berechtigung f�r diese Seite');
        end;


end;
except
//wenn schnell zru rechnung gewechselt wird wird rechnung1 evtl 2 mal angelegt
end;
end;


procedure TForm_main.PageControl_firmaChanging(Sender: TObject;  var AllowChange: Boolean);
begin
 if modus_history then history_entfernen;

 dbspeichern(false);
 menue_leeren;

 //if (datamodul.q_Firma.Eof and datamodul.q_Firma.bof) then
 if kein_datensatz( datamodul.q_Firma) then
	 begin
		showmessage('Bitte zuerst eine Firma ausw�hlen/filtern oder neu anlegen.');
		allowchange:=false;
	 end;
 if pagecontrol_firma.ActivePage=tabsheet_sollist then
 begin
   dbgridext_zeitabrechnung.SelectedRows.Clear;
   //for I := 0 to dbgridext_zeitabrechnung.SelectedRows.Count - 1 do
  //zeiterfassung;
 end;
end;

procedure TForm_main.SpeedButton6Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
  if sperrdatum_OK(form_kalender.auswahlkalender.date,1) then
	begin
     datamodul.q_firma_sollist.edit;
     datamodul.q_firma_sollist['datum'] :=int(form_kalender.auswahlkalender.date);
  end;
end;

procedure TForm_main.SpeedButton_z_vonClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	begin
     if not (datamodul.q_firma_zeiten_soll.state in [dsedit, dsinsert])  then  datamodul.q_firma_zeiten_soll.Edit;
     datamodul.q_firma_zeiten_soll['von'] :=(form_kalender.auswahlkalender.date);
     firma_zeitvorgaben_berechnen;
  end;
end;

procedure TForm_main.DBgridEXT_anwesenheitsplanungDblClick(Sender: TObject);
begin
	if kein_datensatz(datamodul.q_termin) then exit;
	formular_sperren;
	check_form_termine;
	if form_termine.gehezutermin(getbigint(datamodul.q_termin,'nummer'))
	then form_termine.ShowModal;

end;

procedure bildspeichern(tabelle:tdataset; picture:tpicture);
var
datei:string;
begin
  if (tabelle=nil) or (picture=nil) then exit;
	tabelle.edit;
  tabelle['bmp_pfad']:=inttostr(tabelle['nummer'])+'.'+tabelle['bmp_endung'];
  direktspeichern:=true;
  tabelle.post;
  //datei:=bildverzeichnis+'\'+inttostr(tabelle['nummer'])+'.'+tabelle['bmp_endung'];
	datei:=bildverzeichnis+'\'+tabelle['bmp_pfad'];
	picture.bitmap.SaveToFile(datei);

end;

procedure bildladen(tabelle:tdataset; picture:tpicture);
var
datei:string;
begin
  if tabelle['bmp_pfad']<>null then
		datei:=bildverzeichnis+'\'+tabelle['bmp_pfad']
  else datei:='';
  if fileexists(datei) then picture.Bitmap.LoadFromFile(datei)
  else picture.Bitmap.FreeImage ;
end;


procedure richspeichern(tabelle:tdataset;richedit:trichedit);
var
datei:string;
begin
	if  (richedit=nil) or kein_datensatz(tabelle) or (not richedit.modified)  then exit;
	datei:=textverzeichnis+'\'+tabelle['memo_endung']+'\'+inttostr(tabelle['nummer'])+'.'+tabelle['memo_endung'];
  richedit.lines.savetoFile(datei);
  richedit.Modified:=false;
end;




procedure richloeschen(tabelle:tdataset);
var
datei:string;
begin
   if (tabelle.FindField('memo_endung')<>nil) and (tabelle['memo_endung']<>null) then
		datei:=textverzeichnis+'\'+tabelle['memo_endung']+'\'+inttostr(tabelle['nummer'])+'.'+tabelle['memo_endung']
  	else datei:='';
	  if fileexists(datei) then deletefile(datei);
end;



function dateipost(table: tdataset): boolean; //abfrage Speichern
var dsp: boolean;
begin
result:=false;
dsp:=(not speichern_fragen) or direktspeichern;
direktspeichern:=false;

 //datamodul.q_firma.update
if  (table.state=dsedit) then
begin
  {if not (dsp or (MessageDlg('Speichern des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ))
  then
   begin
		tzquery(table).cancelUpdates;
     //tzquery(table).updaterecord;
     abort;
	 end;}

end;
end;

function datensatz(table:tdataset):boolean;
begin
  result:=not kein_datensatz(table);
end;

{var
sql_liste:tstrings;
sql,e:string;
p:integer;
begin
   //muss �ber count(*) abgewickelt werden, unerkl�rliche Fehler
   if table.state=dsinsert then
   	begin
    	result:=true; //wenn eingef�gt gibt es einen
   	end
   else
   try
    sql_liste:=tzquery(table).sql;
    sql:=sql_liste[0];
    p:=pos('from',sql);
    sql:=copy(sql,p,length(sql));
    sql:='select count(*) '+sql;
    e:=mysql_d.Feldinhalt(sql,0);
    if e='0' then result:=false else result:=true;

    except
	result:=not (table.Eof and table.bof);
   end;
   end;}

function kein_datensatz_dataset(table:tdataset;feld:string):boolean;
begin
 if table.state=dsinactive then exit;
  table.Last;
  if table.FindField(feld).Asstring='' then result:=true else result:=false; ;
end;

function kein_datensatz(table:tdataset):boolean;
var
sql_liste:tstrings;
sql,e:string;
p:integer;
begin
   //muss �ber count(*) abgewickelt werden, unerkl�rliche Fehler
   if table.state=dsinactive then exit;

   if table.state=dsinsert then
   	begin
    	result:=false; //wenn eingef�gt gibt es einen
   	end
   else
   try
    sql_liste:=tzquery(table).sql;
    sql:=sql_liste[0];
    p:=pos('from',sql);
    sql:=copy(sql,p,length(sql));
    sql:='select count(*) as anzahl '+sql;
    e:=mysql_d.Feldinhalt(sql,0);
    if (e='0') or (e='') then result:=true
    else result:=false;

    {e:=table.findfield('nummer').asstring;
    if e='' then result :=true else result:=false;}

   except
   	result:=(table.Eof and table.bof);
   end;
end;



function dateiloeschen(table: tzquery; direkt,still: boolean):boolean;
var loesch_ok,loeschen:boolean;
	  tab_name,s,s1,s2,query:string;
	  before_postev:TDataSetNotifyEvent;
    l_berechtigung:boolean;
begin
//###########################################
// hier m�ssen noch die Tabellen angepasst werden aus denen eine zeile gel�scht wird
with datamodul do
begin
tab_name:=lowercase(tabellen_name(table.sql.text));

loesch_ok:=false;
loeschen:=false; //wenn false dann stornieren sonst hart l�schen
l_berechtigung:=true; //wird entsprechend der Berechtigungen gesetzt
if not (direkt or (MessageDlg('L�schen des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes )) then
begin
  result:=false;
  exit;
end;
//      loesch_ok:= (not sql_beinhaltet(table, ''{tabelle wo},''{feld in tab wo}));

  if tab_name='abr_zeitkonto' then
   begin
      s1:=table.FindField('nummer').asstring;
      query:=format('update abr_konto_kriterium set storno=1 where i_konto=%s',[s1]);
      mysql_d.sql_exe(query);
		  loesch_ok:= true;
	  end;

  if tab_name='abfragen' then
	 begin
		s1:='a_'+getbigint_str(datamodul.q_1,'nummer')+'ab';
		s2:='a_'+getbigint_str(datamodul.q_1,'nummer')+'fe';

     mysql_d.drop_table(s1);

     mysql_d.drop_table(s2);

		loesch_ok:=true;
     loeschen:=true;

	 end;

   if tab_name='abschluss' then
   begin
		loesch_ok:= true;
	  end;

	 if tab_name='abteilung' then
	  begin // abteilung.kuerzel
		s1:=table['kuerzel'];
     s2:=inttostr(table['i_firma']);
		query:=format('select nummer from mitarbeiter where kuerzel = "%s" and i_firma=%s',[s1,s2]);
		loesch_ok:=( not mysql_d.sql_exe_rowsvorhanden(query));
		//loesch_ok:= (not sql_beinhaltet(table, 'mitarbeiter','abteilung'));
	  end;

	 if tab_name='aerzteliste' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'mitarbeiter','i_hausarzt'));
	  end;

    if tab_name='ambulanz_insert' then
	  begin
		loesch_ok:= true;
	  end;

	  if tab_name='akt_untersuchung' then
	  begin
		loesch_ok:= true;
	  end;

	  if tab_name='ambulanz' then
	  begin
		if  getbigint(q_ambulanz,'nummer')=getbigint(q_arbeitsunfall,'nummer') then
		begin
			{q_ambulanz.Edit;
			q_ambulanz['storno']:=storno;
			q_ambulanz.post; //kein direktspeichern notw.}
			if q_arbeitsunfall['storno']<>1 then dateiloeschen(q_arbeitsunfall,true,true);

		end;
		loesch_ok:=true;
    l_berechtigung:=b.b_darf_loeschen('proband-ambulanz') or b.b_darf_loeschen('proband-arbeitsunfall');;
	  end;

	  if tab_name='arbeitsunfall' then
	  begin
		loesch_ok:= true;
	  end;

	  if tab_name='art' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'untersuchung','i_uart'));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'impfung','i_uart'));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'labor','i_uart'));
	  end;

	  if tab_name='artikel' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-artikel');
	  end;

	  if tab_name='berlist' then
	  begin
		loesch_ok := (not sql_beinhaltet(q_berlist,'akt_untersuchung','i_bereich'));
	  end;

	  if tab_name='befunde' then
	  begin

		 loesch_ok:=(not sql_beinhaltet(q_befunde, 'akt_untersuchung','i_untersuchung'));
		 //loesch_ok:=loesch_ok and (not sql_beinhaltet(q_befunde, 'akt_untersuchung_storno','i_untersuchung'));
     l_berechtigung:=b.b_darf_loeschen('Proband-Befunde');
	  end;
    if tab_name='berechtigung_firma' then
	  begin
		 loesch_ok:= true;
	  end;

    if tab_name='berechtigung_objekt' then
	  begin
		 loesch_ok:= true;
	  end;

	  if tab_name='bereich' then
	  begin
			loesch_ok:= (not sql_beinhaltet(q_bereich, 'akt_untersuchung','i_bereich'));
	  end;

    if tab_name='berlist' then
	  begin
         loesch_ok:=(not sql_beinhaltet(q_berlist, 'akt_untersuchung','i_berlist')) and (not sql_beinhaltet(q_berlist, 'akt_untersuchung','i_berlist_rechts')) ;

	  end;

	  if tab_name='besonderheiten' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-besonderheiten');
	  end;

	  if tab_name='besonderheitenListe' then
	  begin
		loesch_ok:= (not sql_beinhaltet(q_besonderheitenListe, 'besonderheiten','i_besonderheit'));
	  end;

	 if tab_name='berufsgenossenschaft' then
	  begin
		 loesch_ok:= (not sql_beinhaltet(table, 'firma'{tabelle wo},'i_bg'{feld in tab wo}));
	  end;


	  if tab_name='bitmaps' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'termine'{tabelle wo},'bmp_nr'{feld in tab wo}));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'art'{tabelle wo},'bmp_nr'{feld in tab wo}));
	  end;

	  if tab_name='branche' then
	  begin
		 loesch_ok:= (not sql_beinhaltet(table, 'firma'{tabelle wo},'i_branche'{feld in tab wo}));
	  end;

	  if tab_name='diagnosen' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-diagnosen');
	  end;

	  if tab_name='dokumente' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-dokumente-arzt');
	  end;

	  if tab_name='email' then
	  begin
		 loesch_ok:= true;
		 loeschen :=true;
	  end;

	  if tab_name='firma' then
	  begin
		 loesch_ok:=(not sql_beinhaltet(q_firma, 'mitarbeiter','i_firma'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_firma, 'firma_sollist','i_firma'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_firma, 'firma_ansprech','i_firma'));
      loesch_ok:=loesch_ok and (not sql_beinhaltet(q_firma, 'rechnung','i_firma'));
      l_berechtigung:=b.b_darf_loeschen('firma-stammdaten');
	  end;

      
	  if tab_name='firma_filter' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('firma-kriterien');
	  end;

	  if tab_name='firma_ansprech' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('firma-ansprechpartner');
	  end;

	  if tab_name='firma_sollist' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('firma-zeitabrechnung');
	  end;

     if tab_name='firma_zeiten_soll' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('firma-zeitabrechnung');
	  end;

     if tab_name='abr_anlass_krit' then
	  begin
		 loesch_ok:= (not sql_beinhaltet(table, 'firma_sollist','i_kriterium'));
	  end;

	 if tab_name='fristen' then
	 begin
		loesch_ok:= (not sql_beinhaltet(table, 'nachuntersuchung','i_nachuntersuchung'));
	 end;


	  if tab_name='f_gruppe' then
	  begin
	  loesch_ok:= (not sql_beinhaltet(table, 'firma'{tabelle wo},'i_f_gruppe'{feld in tab wo}));
	  end;

	if tab_name='gewerbeaufsicht' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'firma'{tabelle wo},'i_gewerbeaufsicht'{feld in tab wo}));
	  end;

		 if tab_name='goae' then
	  begin
		 loesch_ok:= true;
	  end;

    if tab_name='gdt' then
	  begin
		 loesch_ok:= true;
	  end;

	if tab_name='impfstoff' then
	begin
		loesch_ok:= (not sql_beinhaltet(table, 'impfung','i_stoff'));
	end;


	if tab_name='impfabfolge' then
	begin
	  loesch_ok:= (not sql_beinhaltet(table, 'impfung'{tabelle wo},'i_impfabfolge'{feld in tab wo}));
	end;


	 if tab_name='impfung' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-impfung');
	  end;

    if tab_name='kontakte' then
	  begin
		 loesch_ok:= true;
	  end;
     
	 if tab_name='labor' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-labor');
	  end;

	 if tab_name='materialstamm' then
	begin
	  loesch_ok:= (not sql_beinhaltet(table, 'artikel'{tabelle wo},'i_material'{feld in tab wo}));
	end;


	if tab_name='mitarbeiter' then     //achtung nur korrekt wenn q_mitarbeiter DS stimmt.
	  begin
		 loesch_ok:=(not sql_beinhaltet(q_mitarbeiter, 'Diagnosen','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'besonderheiten','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'untersuchung','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'impfung','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'labor','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'befunde','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'dokumente','i_mitarbeiter'));
		 loesch_ok:=loesch_ok and (not sql_beinhaltet(q_mitarbeiter, 'ambulanz','i_mitarbeiter'));
     l_berechtigung:=b.b_darf_loeschen('proband-stammdaten');
		end;

	  if tab_name='nachuntersuchung' then
	  begin
		 loesch_ok:= true;
	  end;


	  if tab_name='nationen' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'mitarbeiter'{tabelle wo},'i_nation'{feld in tab wo}));
	  end;

	  if tab_name='rechnung' then
	  begin
      if getbigint(q_rechnung,'i_parent')>0 then
      begin

      	//showmessage('Die Rechnung geh�rt zu einer �bergeordnenten Rechnung und kann nicht gel�scht werden' );
         //exit;
         query:=format('update re_positionen set storno = 1 where i_link_rechnung=%s',[q_rechnung.findfield('nummer').asstring]);
         mysql_d.sql_exe(query);
         form_main.rechnung_aktualisieren(getbigint(q_rechnung,'i_parent'));


      end;

      // wenn abh�ngige Rechnungen vorhanden darf auch nicht gel�scht werden
      //s:=q_rechnung.findfield('nummer').asstring;
      query:=format('select nummer from re_positionen where i_rechnung=%s and i_link_rechnung>0 and storno=0',[q_rechnung.findfield('nummer').asstring]);
      if mysql_d.Feldinhalt(query,0)<>'' then
      begin
      	showmessage('Zu dieser Rechnung geh�ren untergeordnenten Rechnungen - bitte diese zuerst l�schen' );
         exit;
      end;

    l_berechtigung:=b.b_darf_loeschen('firma-rechnung') or b.b_darf_loeschen('proband-rechnung');
    if l_berechtigung then
    begin
      sql_new(false,q_1,'select * from haupt_tabelle','');

      s1:= inttostr(q_1['renu']-1);
      s2:=table['renu'];
      if (copy(s2,length(s2)-length(s1)+1,length(s1))=s1) and (getbigint(q_rechnung,'i_parent')=0) then
      begin
        q_1.edit;
        q_1['renu']:=datamodul.q_1['renu']-1;
        q_1.post;
        showmessage('Die Rechnungsnummer wurde zur�ckgestellt');
      end;

      //query:=format('delete from re_positionen where renu="%s"',[q_rechnung['renu']]);
        query:=format('update re_positionen set storno=1 where i_rechnung="%s"',[q_rechnung.findfield('nummer').asstring]);
      mysql_d.sql_exe(query);
      q_repositionen.refresh;
    end;
		loesch_ok:=true;
	  end;


	  if tab_name='schicht' then
	  begin
		 loesch_ok:= (not sql_beinhaltet(table, 'mitarbeiter'{tabelle wo},'i_schichtmodell'{feld in tab wo}));
	  end;

	  if tab_name='schluessel' then
	  begin
		 loesch_ok:= true;
	  end;

	  if tab_name='serienfelder' then
	  begin
		 loesch_ok:= true;
	  end;

	  if tab_name='serienfeldinhalte' then
	  begin
		 loesch_ok:= true;
	  end;


	 if tab_name='status' then
	 begin
		loesch_ok:= (not sql_beinhaltet(table, 'untersuchung','i_status'));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'impfung','i_status'));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'labor','i_status'));
	 end;

	  if tab_name='taetigkeiten' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'mitarbeiter'{tabelle wo},'i_taetigkeit'{feld in tab wo}));
	  end;

	  if tab_name='termine' then
	  begin
		 loesch_ok:= true;
		 loeschen:=true; //aus der datenbank l�schen
	  end;

	  if tab_name='texte' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('firma-dokumente');
	  end;

     if tab_name='textbausteine' then
	  begin
		 loesch_ok:= true;
	  end;

	  if tab_name='typ' then
	  begin
      loesch_ok:= (not sql_beinhaltet(q_typ, 'untersuchung','i_typ'));
      loesch_ok:= loesch_ok and (not sql_beinhaltet(q_typ, 'impfung','i_typ'));
      loesch_ok:= loesch_ok and (not sql_beinhaltet(q_typ, 'labor','i_typ'));
	  end;

	  if tab_name='unt_befunde' then
	  begin
		 loesch_ok:= true;
	  end;

	  if tab_name='unt_labor' then
	  begin
		 loesch_ok:= true;
	  end;
	  if tab_name='untersuchung' then
	  begin
		 loesch_ok:= true;
     l_berechtigung:=b.b_darf_loeschen('proband-bescheinigung') or b.b_darf_loeschen('proband-bescheinigung-ag');;
	  end;

	  if tab_name='untersucher' then
	  begin
		loesch_ok:= (not sql_beinhaltet(table, 'untersuchung','i_untersucher'));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'impfung','i_untersucher'));
		loesch_ok:= loesch_ok and (not sql_beinhaltet(table, 'labor','i_untersucher'));
	  end;

	  if tab_name='vorlagen' then
	  begin
		 loesch_ok:= true;
	  end;
      if tab_name='abrechnung_anlass' then
	  begin
		 loesch_ok:= true;
	  end;

// ende tabellen
//##########################################################
   if l_berechtigung then
	  if loesch_ok then
	  begin
    if loeschen or (table.FindField('storno')=nil) then
       begin
       //nur abfrage und termine
        table.Delete;
       end
       else
         begin  //storno setzen
          if not (table.State in [dsedit, dsinsert]) then  table.edit;
          table['storno']:=storno_zahl;
          //	if table.FindField('farbe')<>nil then table['farbe']:=2;
          before_postev:=table.BeforePost;
          table.BeforePost:=nil;
          table.Post;
          table.BeforePost:=before_postev;
          //table.delete;
         end;

         if table<> q_akt_untersuchung then form_main.tool_aktiv(tzquery(table));
         result:=true;
         before_postev:=table.BeforeScroll;
         table.BeforeScroll:=nil;
         table.refresh;
         if table=q_texte then table.first;  //wegen olecontainer
         table.BeforeScroll:= before_postev;
        end
	  else
      begin
        if not still then showmessage('Bitte zuerst die abh�ngigen Datens�tze l�schen!');
        result:=false;
      end;
   end
	//else result:=false;
//end;
end;


procedure tform_main.firmaanzeigen;
var
query:string;
begin
with datamodul do
begin
 Label_firma.Caption:='';
 if kein_datensatz(q_firma) then exit;
 if q_firma['firma']<> null then form_main.Label_firma.Caption:=q_firma.findfield('firma').asstring;
 if q_firma['ort']<> null then form_main.Label_firma.Caption:=form_main.Label_firma.Caption +
 ', '+ q_firma['ort'];

 //query:='select * from abteilung where i_firma='+inttostr(q_firma['nummer']);
 //sql(true,q_abteilung,query,'kuerzel');
 //if q_mitarbeiter.Active =true then label_count.caption:=inttostr( q_mitarbeiter.RecordCount);
end;
end;


procedure TForm_main.Tree_bereichChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
//bereich_speichern;
////dateispeichern(datamodul.q_akt_untersuchung, richedit_befund, true);
end;

procedure tform_main.impfstofffiltern;
begin
with datamodul do
begin
	q_impfstoff.filtered:=false;
	if q_impfung['i_typ']<> null then
		q_impfstoff.filter:='i_typ='+inttostr(q_impfung['i_typ']);
	q_impfstoff.filtered:=true;
end;
end;


procedure tform_main.ana_toolbar_sperren;
begin
	  savebutton.enabled:=false;
	  openbutton.Enabled:=false;
	  copybutton.Enabled:=false;
	  pastebutton.Enabled:=false;
    undobutton.Enabled:=false;
    boldbutton.Enabled:=false;
	  italicbutton.Enabled:=false;
    underlinebutton.Enabled:=false;
    toolbutton_color.Enabled:=false;
    leftalign.Enabled:=false;
    rightalign.Enabled:=false;
    centeralign.Enabled:=false;
    bulletsbutton.Enabled:=false;
    fontname.Enabled:=false;
    fontsize.Enabled:=false;
    rtfedit.SelectionChange(Self);

  {CurrText.Name := DefFontData.Name;
  DC := GetDC(0);
  ScreenLogPixels := GetDeviceCaps(DC, LOGPIXELSY);
  CurrText.Size := -MulDiv(DefFontData.Height, 72, ScreenLogPixels);}
end;

procedure tform_main.ana_toolbar_entsperren;

begin
	  savebutton.enabled:=true;
	  openbutton.Enabled:=true;
	  copybutton.Enabled:=true;
    pastebutton.Enabled:=true;
    undobutton.Enabled:=true;
    boldbutton.Enabled:=true;
    italicbutton.Enabled:=true;
    underlinebutton.Enabled:=true;
    toolbutton_color.Enabled:=true;
    leftalign.Enabled:=true;
    rightalign.Enabled:=true;
    centeralign.Enabled:=true;
    bulletsbutton.Enabled:=true;
    fontname.Enabled:=true;
    fontsize.Enabled:=true;
end;

procedure TForm_main.ToolButton_closeClick(Sender: TObject);
begin
	form_main.close;
end;

procedure TForm_main.DBLookupComboBox_impfung_stoffEnter(Sender: TObject);
begin
	form_main.impfstofffiltern;
end;

procedure TForm_main.DBLookupComboBox_impfung_stoffExit(Sender: TObject);
begin
	datamodul.q_impfstoff.filtered:=false;
  datamodul.q_impfstoff.filter:='';
   DBLookupComboBox_impfung_stoff.Refresh;
end;

procedure TForm_main.ToolButton_termineClick(Sender: TObject);
begin
 if  b.b_string('kalender')=-1 then exit;
  dbspeichern(false); //17.01.2001
	formular_sperren;
	check_form_termine;
  form_termine.neu_lesen:=true;
	form_termine.showmodal;
	//PageControl_mainChange(pagecontrol_main);
end;

procedure TForm_main.MaskEdit2Change(Sender: TObject);

begin
with datamodul do
begin
	q_firma_sollist.edit;
	q_firma_sollist['zeit']
end;
end;



procedure TForm_main.DBgridEXT_firmaKeyPress(Sender: TObject; var Key: Char);
begin
     edit_firma_such.text:=key;
     edit_firma_such.SetFocus;
     edit_firma_such.SelStart:=99;
end;

procedure TForm_main.SpeedButton5Click(Sender: TObject);
begin
  dbtreeview.Expand_e1;
end;

procedure TForm_main.SpeedButton7Click(Sender: TObject);
begin
dbtreeview.FullCollapse;
end;

procedure TForm_main.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  pagecontrol_main.ActivePage:=tabsheet_firma;
  application.ProcessMessages;
	canclose :=true;
	if tag=1 then exit;
  speichern;
	//dbspeichern(false);
	bereich_speichern;
 	if tag <>2 then
  	if (MessageDlg('Wollen Sie '+application.title+' beenden?', mtConfirmation	,[mbYes, mbNo], 0)=mryes )
		then canclose:=true
		else canclose :=false;
end;

procedure TForm_main.menue_endeClick(Sender: TObject);
begin
//application.terminate;
form_main.close;
end;

procedure TForm_main.RichEdit_befundExit(Sender: TObject);
begin
Bereich_speichern;
end;

procedure TForm_main.PageControl_laborChange(Sender: TObject);
begin
if pagecontrol_labor.ActivePage=tabsheet_lab_graph then labor_zeichnen;
dbgrid_labor_hinweise.Columns[1].Width:= dbgrid_labor_hinweise.Width-230;
end;

procedure TForm_main.xygraphResize(Sender: TObject);
begin
labor_zeichnen;
end;


procedure TForm_main.Auswertungen1Click(Sender: TObject);
begin
 ToolButton_auswertungClick(sender);
end;


procedure TForm_main.ToolButton_auswertungClick(Sender: TObject);
begin
 //datamodul.sql(true,datamodul.q_1,'select nummer,name,vorname,firma, upper(name) as grname from mitarbeiter order by grname','');
 if  not ((b.b_string_silent('Auswertung-Gesamt')>-1) or (b.b_string_silent('Auswertung-Arbeitssicherheit')>-1) or (b.b_string_silent('Proband-Bescheinigung')>-1) or (b.b_string_silent('Proband-Impfung')>-1) ) then
 begin
	  showmessage('Sie haben keine Berechtigung Auswertungen aufzurufen');
	  exit;
 end;
 dbspeichern(false); //17.01.2001
 formular_sperren;
 //datamodul.sql_new(false, datamodul.q_a2,'select name, vorname, geb_dat from mitarbeiter where nummer=-1','');
 form_auswertung.showmodal;
// PageControl_mainChange(pagecontrol_main);
end;

procedure Tform_main.winrun(pfad, parameter: string);
var

 erzeugt: boolean;
 datei,command :pchar;
 startinfo: tstartupinfo;
 processinfo: tprocessinformation;
begin

	datei:= pchar(pfad);
	command:=pchar(parameter);
	//if (processinfo.hProcess=0) then //processinfo:=runexe(datei,nil);
	begin
	startinfo.cb:=68;
	startinfo.lpReserved:=nil;
	startinfo.lpDesktop:=nil;
	startinfo.lpTitle:=nil;
	startinfo.dwX:=0;
	startinfo.dwY:=0;
	startinfo.dwXSize:=0;
	startinfo.dwYsize:=0;
	startinfo.dwXCountChars:=0;
	startinfo.dwYCountChars:=0;
	startinfo.dwFillAttribute:=0;
	startinfo.dwFlags:=129;
	startinfo.wShowWindow:=9;
	startinfo.cbReserved2:=0;
	startinfo.lpReserved2:=nil;
	startinfo.hStdInput:=0;
	startinfo.hStdOutput:=0;
	startinfo.hStdError:=0;
	try
	erzeugt:=createprocess(datei,command,nil,nil,false,create_default_error_mode,nil,nil,startinfo,processinfo);
	if not erzeugt then processinfo.hProcess:=0;
	except
	 processinfo.hProcess:=0;
	end;

  end;
end;


function dateiname(pfad: string): string;
var
back: integer;
begin
  pfad:=stringwenden(pfad);
  back:=pos('\', pfad);
  pfad:=copy(pfad, 1, back-1);
  result:=stringwenden(pfad);
end;


function wurzelpfad(pfad: string): string;
var
back: integer;
begin
pfad:=copy(pfad, 1, length(pfad)-1);
pfad:=stringwenden(pfad);
back:=pos('\', pfad);
pfad:=copy(pfad, back+1, length(pfad));
result:=stringwenden(pfad);
if  pos('\', result)=0 then result:=result+'\';
end;

function stringwenden(wort: string): string;
var
 laenge, i: integer;
begin
laenge:=length(wort);
result:='';
for i:=laenge downto 1 do
begin
	result:=result+copy(wort,i,1);
end;
end;


function neuer_datensatz(tabelle: tzquery;feldnamen, felder:array of variant ):int64;
var
nummer:int64;
sql,tab_name,feldname:string;
i:integer;
feld:tfield;
begin
	result:=-1;
	sql:=tabelle.sql.text;
	tab_name:=tabellen_name(sql);
	nummer:=mysql_d.neue_nummer(tab_name);
	if nummer=0 then exit; //nummer kann nicht gezogen werden

	//Tabellen, die nicht dauerhaft sind evtl l�schen
	if tab_name='termine' then
	begin

	end;

	if tab_name='email' then
	begin

	end;

	begin
   //tabelle.post;
	 tabelle.insert;  //append 20120221
   if tabelle.State in [dsbrowse] then tabelle.Edit;
    //tabelle.InsertRecord([inttostr(nummer)]) ; automatisch post?
    //tabelle.Edit;

    datamodul.not_null_abfragen(tabelle);

	 if (nummer>0) and (lowercase(feldnamen[0])='nummer') then
		begin
		 feld:=tabelle.FieldByName('nummer');
		 if feld.datatype=ftLargeInt then felder[0]:=inttostr(nummer)
			else felder[0]:=integer(nummer);
		end;
    feld:=tabelle.findfield('ts');
    if feld<>nil then tabelle.FindField('ts').AsDateTime :=now();

		for i:=0 to high(feldnamen) do
		begin
		  feldname:=feldnamen[i];
		  feld:=tabelle.findfield(feldname);
        //feld.ReadOnly:=false;
		  if  (felder[i]<>null)and (feld<>nil) then
		  try
			 if feld.DataType=ftLargeInt	then
			 begin
				setbigint(tabelle,feldname,StrToInt64(felder[i]));
			 end
			 else  tabelle[feldname]:=felder[i] ;
          //else tabelle.FindField(feldname).AsString:=felder[i];
		  except
		  end;
		end;


		Form_main.tool_aktiv(tabelle);
		result:=nummer;

		tabelle.post; //war bis 6.10.16 deaktiviert    wiring-fehler
    tabelle.Edit; //auch neu
	end;
end;



procedure TForm_main.editClick(Sender: TObject);
begin
	ToolButton_editClick(Sender);
end;

procedure TForm_main.menue_saveClick(Sender: TObject);
begin
 speichern;
end;

procedure TForm_main.Menue_NewClick(Sender: TObject);
begin
datensatz_neu;
end;

procedure TForm_main.menue_deleteClick(Sender: TObject);
begin
loeschen;
end;

procedure TForm_main.ComboBox_historyChange(Sender: TObject);
begin
  gehe_zu_history(true);
end;

procedure TForm_main.gehe_zu_history(modus:boolean);
var
data: pnodepointer;
snr:string;
begin
try
   if comboBox_history.Itemindex<0 then exit;
   
	 dbspeichern(false);
	 data:=pnodepointer(ComboBox_history.Items.Objects[ComboBox_history.Itemindex]);
   snr:=inttostr(data^.nummer);
   if modus then
   begin
	 pagecontrol_main.ActivePage:=tabsheet_stammdaten;
   	 gehezu_stammdaten(data^.nummer);
   end
   else
    datamodul.q_mitarbeiter.Locate('nummer', snr,[]) ;
	 table_focusieren;
except
//fehler wenn fokus auf proband und dann historybox und dann buchstabe eingabe
end;
end;

procedure tform_main.history_add(name_string:string;nummer:int64);
var
data: pnodepointer;
i:integer;
begin
new(data);
data^.nummer:=nummer;
combobox_history.Items.InsertObject(0,name_string, tobject(data));
i:=1;
while i<=combobox_history.Items.Count-1 do
	begin
	  data:=pnodepointer(ComboBox_history.Items.Objects[i]);
	  if data^.nummer=nummer then history_delete(i) else inc(i);
	end;
while combobox_history.Items.Count>35 do history_delete(combobox_history.items.Count-1);
combobox_history.itemindex:=0;

end;

procedure tform_main.history_delete(index:integer);
var
data: pnodepointer;
begin
	data:=pnodepointer(ComboBox_history.Items.Objects[index]);
  dispose(data);
	combobox_history.items.Delete(index);
end;

function tform_main.gehezu_name_Stammdaten(name, vorname: string;geb_dat:tdate):boolean;
var
nummer:int64;
query,sql_dat:string;
begin
	 result:=false;
	 sql_dat:=sql_datetostr(geb_dat);
   query:=format('select count(*) from mitarbeiter where name="%s" and vorname="%s" and geb_dat=%s',[name, vorname,sql_dat ]);
   nummer:=strtoint64(mysql_d.Feldinhalt(query,0));
   if nummer>1 then
      result:=ma_suchen(name+','+vorname)
   else
   begin
     query:=format('select nummer from mitarbeiter where name="%s" and vorname="%s" and geb_dat=%s',[name, vorname,sql_dat ]);
     nummer:=strtoint64(mysql_d.Feldinhalt(query,0));
     if nummer>0 then result:=gehezu_stammdaten(nummer);
   end;
     if result then pagecontrol_main.activepage:=tabsheet_stammdaten;

end;


function  tform_main.gehezu_stammdaten(nummer: int64):boolean;
var
feld:string;
begin
with datamodul do
begin
  akt_abteilung:=0;
  akt_arbeitsplatz:=0;
	feld:=mysql_d.Feldinhalt('select i_firma from mitarbeiter where nummer= '+inttostr(nummer),0);
	if feld='' then exit;
	//q_firma.first;
	q_firma.locate('nummer',strtoint(feld),[]);
  pagecontrol_main.ActivePage:=tabsheet_stammdaten;
	mitarbeiter_filtern;
	//result:=q_mitarbeiter.locate('nummer',vararrayof([inttostr(nummer)]),[]);
  result:=q_mitarbeiter.locate('nummer',vararrayof([(nummer)]),[]);

	if not result then
	begin
	 sql_relation('mitarbeiter','i_firma',q_firma,q_mitarbeiter,'i_firma,name,vorname'); //alle mitarbeiter auch wenn archiviert!!!
	 //result:=q_mitarbeiter.locate('nummer',inttostr(nummer),[]);
   result:=q_mitarbeiter.locate('nummer',(nummer),[]);

	end;
  grid_einstellen(dbgridext_namen);
end;
end;

procedure tform_main.table_focusieren;
var
	contr:twincontrol;
begin

  contr:=pagecontrol_main.ActivePage;
  if contr=tabsheet_firma then dbgridext_firma.setfocus;
  if contr=tabsheet_stammdaten then dbgridext_namen.setfocus;
//	if contr=tabsheet_anamnese then akt_editor.SetFocus;

end;

procedure tform_main.olecontainer_word;
begin
	Arbeitsmedizin.Destroyobject;
	Arbeitsmedizin.CreateObject('Word.Document',false);
	Arbeitsmedizin.DoVerb(0);
	com.wordobjekterstellen(Arbeitsmedizin);
	com.neuer_text;
end;

procedure olecontainer_close(ole:tolecontainerext);
begin
try
	if ole.State <>osempty then
	begin
		ole.Close;   //schlie�t die serveranwendung objek bleibt geladen
	end;
  if word_close then com.word_schliessen;
except
end;
end;


procedure tform_main.olecontainer_destroy(ole:tolecontainerext);
begin
try
if ole.State <>osempty then
begin
    ole.Destroyobject;
end;
except
	showmessage('(Word)-Objekt konnnte nicht geschlossen werden.');
end;
ole.Modified:=false;
//ole.modified_reset();
if word_close then com.word_schliessen;
end;



function tform_main.stream_komprimieren(tabelle:tzquery;feld:string):boolean;
var

timestamp,query,nr:string;

begin
with datamodul do
begin

    //nr:=tabelle.findfield('nummer').asstring;
    //query:= format('select * from %s where nummer=%s',[tabelle,nr]);
    //datamodul.sql_new(false,datamodul.q_1,query,'');
    try
    timestamp:=tabelle.findfield('ts').asstring;
    timestamp:=timestamp;//+':59' ;
    olecontainer_laden( tabelle,'ole',arbeitsmedizin);
    tabelle.edit;
    olecontainer_speichern(true,tabelle,'ole',arbeitsmedizin);
    //tabelle.findfield('ts').asstring:=timestamp;
    tabelle.Post;
    except
      //showmessage('fehler');
    end;

end;
end;



function tform_main.olecontainer_laden(tabelle:tzquery;feld:string;ole:tolecontainerext):boolean;
var
stream:tstream;
stream_in, stream_out:tmemorystream;
t:ttime;
a:array[1..4] of char;
s:string;
i:integer;
//kompressor: tlzw1;
begin
try
  result:=false;
  laed_gerade:=true;
	if ole.State <>osempty then ole.Close;
	if ole.State <>osempty then ole.Destroyobject;
  application.ProcessMessages;
	if kein_datensatz(tabelle) then
	begin
	  ole.Modified:=false;
	  exit;
	end;

   t:=time();
   i:=1;
   while lzr1_akt do
   begin
     pause (100);
     //if i/2= i div 2 then showmessage('Bitte aktuelle Aktion abwarten: '+lzr1_akt_s);
     if i=100 then exit;
     //if time()> t+0.000055 then exit;     //20131102 zeit verl�ngert
     //if time()> t+0.00005 then showmessage('Bitte aktuelle Aktion abwarten: '+lzr1_akt_s);
     application.ProcessMessages;
     inc(i);
   end;



		//if Tabelle[feld]<>'' then
    if  tabelle.FieldByName(feld).asstring<>'' then
		try
			screen.cursor:=crhourglass;
      tabelle.DisableControls;
      ole.Visible:=false;
			lzr1_akt:=true;
      lzr1_akt_s:='olecontainer_laden';
      olecontainer_destroy(ole);
      //dbgridext1.Enabled:=false;
      //dbtreeview.Enabled:=false;

			stream_in:=tmemorystream.Create;
			stream_out:=tmemorystream.Create;
			stream:=tabelle.CreateBlobStream(Tabelle.FieldByName(feld),bmRead);
			lzr1.InputStream:=stream_in;
			lzr1.OutputStream:=stream_out;

      //while time()< t+0.000002 do; //2/10 sec
      application.ProcessMessages;
       stream.Position:=0;
       stream.ReadBuffer(a,10);

       s:= a[1]+a[2]+a[3]+a[4];
       if (s='!HZL') or (s='WRZL') then
       begin
          stream.position:=0;
          stream_in.LoadFromStream(stream);
          stream_in.Position:=0;
          lzr1.Decompress;
       end
       else
       stream_out.LoadFromStream(stream);

			stream_out.Position:=0;
			try
				ole.loadfromStream(stream_out);
			except
				stream.position:=0;
				ole.loadfromStream(stream);
			end;
      try
        pause(10);
        ole.Modified:=false;
        //ole.UpdateObject; //das dauert ist hier eigentlich nicht notwendig da word nicht ge�ffnet########################################
        ole.UpdateVerbs;
      except
        showmessage ('Word antwortet nicht');
      end;
      pause(10);
			stream_out.position:=0;
      result:=true;
	  finally
      application.ProcessMessages;
		  stream.Free;
		  stream_in.free;
		  stream_out.free;
		  lzr1_akt:=false;
      ole.Visible:=true;
      tabelle.EnableControls;
		  screen.cursor:=crdefault;

      application.ProcessMessages;
	  end;
finally
  laed_gerade:=false;
end;
end;


function Tform_main.oleausdatei(OLeContainer: TOLeContainerext;dat:string): boolean;   //muss in pdf umgewandelt werden
var                                                                                    // wenn pdf dann mit pdftk behandeln
i,z:integer;
t:ttime;
dat_pdf,dat_copy,endung:string;
begin
if not FileExists(dat) then
        begin
          result:=false;
        	exit;
        end ;
  //dat in pdf umwandeln
  endung:= dateiendung(dat);
  if pos (endung,'pdf doc docx') >0  then
  begin
     dat_pdf:=dat;
  end
  else
  begin
    //alle ausser pdf  doc und docx
    z:=0;
    endung:=dateiendung(dat);
    dat_pdf:=pfad_temp+'a_'+stringdatum(now)+'_';
    dat_pdf:=sonderzeichenraus(dat_pdf);
    while  ((fileexists(dat_pdf+inttostr(z)+'.pdf')) or ((fileexists(dat_pdf+inttostr(z)+'.'+endung)))) do inc(z);
    dat_pdf:=dat_pdf+inttostr(z);
    dat_copy:=dat_pdf+'.'+endung;
    dat_pdf:=dat_pdf+'.pdf';

    copyfile_delphi( dat,dat_copy);

    if not  warte_bis_datei_lesbar(dat_copy) then exit;

    if not dat_to_pdf(dat_copy,dat_pdf) then exit;

    if not  warte_bis_datei_lesbar(dat_pdf) then exit;

  end;
  //dat ist erstellt
  i:=0;
  result:=false;
  while result=false do
  begin
    try
      if i=50 then
      begin
        Messagedlg('konnte OLE-Verbindung nicht aufbauen.',mtinformation, [mbOK],0);
        exit;
      end;
        if not  warte_bis_datei_lesbar(dat_pdf) then exit;    //2015-05-03
        if endung='pdf' then dat_pdf:=pdf_behandeln(dat_pdf);
        if olecontainer.State<>osempty then olecontainer.Close;
        if olecontainer.State<>osempty then olecontainer.DestroyObject;
        pause(100);
        olecontainer.CreateObjectFromFile(dat_pdf,false);
        OLeContainer.visible:=true;
        olecontainer.Modified:=false;

        //if dat_pdf<>dat then deletefile(dat_pdf);
        result:=true;
        application.ProcessMessages;
        if olecontainer.state<>osloaded then
        begin
          showmessage('CreateObjectFromFile konnte nicht ausgef�hrt werden- '+dat_pdf+' nicht lesbar?');
        end;
    except
      //showmessage(dat_pdf +' kann nicht gelesen werden.');
      pause(100);
      inc(i);
    end;
  end;

end;

function Tform_main.oleausobjekt(OLeContainer: TOLeContainerext): boolean;
begin

try
    if olecontainer.State<>osempty then olecontainer.DestroyObject;
		result:=OLeContainer.InsertObjectDialog;

		OLeContainer.visible:=true;
      //DataModul.q_dokumente.findfield('titel').asstring:= olecontainer.SourceDoc;
      //if OLeContainer.OleClassName<>'' then DataModul.q_dokumente.findfield('titel').asstring:= OLeContainer.OleClassName;
     	application.ProcessMessages;
		//activecontrol:=OLeContainer;

 except
		Messagedlg('konnte OLE-Verbindung nicht aufbauen.',mtinformation, [mbOK],0);
     olecontainer.Modified:=false;
     result:=false;
 end;
end;




procedure tform_main.olecontainer_speichern(direkt:boolean;tabelle:tzquery;feld:string;ole:tolecontainerext);
var
stream:tstream;
stream_in, stream_out:tmemorystream;

t:ttime;
dat_backup,ts:string;
a,a1:array[1..10] of char;
i:integer;
c:twincontrol;
g_e,t_e:boolean;
begin

   if ole.state= osempty then exit;
	if not (ole.state in [osopen,osloaded,osrunning]) then
    begin
    	showmessage('Word nicht manuell schlie�en! Datei konnte nicht gespeichert werden.');
    	exit;
    end;
	if ole.OleClassName=null then exit;
	//if  (tabelle.eof) and (tabelle.bof) then exit;
   if kein_datensatz(tabelle) then
	if (not  (ole.Modified) ) then exit;
	//if not b.b_archiv_ok(datamodul.q_akt_untersuchung) then exit;
	if not b.b_alter_ok(tabelle) then exit;
try
	ole.Modified:=false;
  ole.UpdateObject;  //dauert nicht lange wenn anwendung ge�ffnent
  c:=form_main.ActiveControl;


		t:=time();
    i:=1;
		while lzr_akt do
		begin
       pause (1000);
       if i/2= i div 2 then showmessage('Bitte olecontainer_speichern abwarten');
       if i=3 then exit;
		  //if time()> t+0.000055 then exit;
      //  if time()> t+0.00005 then   showmessage('Bitte olecontainer_speichern abwarten');
        application.ProcessMessages;
        inc(i);
		end;
	//ole.updateobject;
	direkt:=direkt or (not speichern_fragen);
	//if  direkt or (MessageDlg('Speichern des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes )  then
	try
      tabelle.disableControls;
			screen.cursor:=crhourglass;
			lzr_akt:=true;
      lzr_akt_s:='olecontainer_speichern';
      g_e:=dbgridext_texte.Enabled;
      t_e:=dbtreeview.Enabled;
      dbgridext_texte.Enabled:=false;
      dbtreeview.Enabled:=false;
			stream_in:=tmemorystream.Create;
			stream_out:=tmemorystream.Create;
      //stream_in:=tolestream.Create(;
			//stream_out:=tolestream.Create;


     {if  (lowercase(copy(ole.OleClassName,1,13))='word.document') or (lowercase(copy(ole.OleClassName,1,8))='acroexch') then
     begin  }
        lzr.InputStream:=stream_in;
        lzr.OutputStream:=stream_out;
        //ole.UpdateObject;
        //ole.DoVerb(ovshow);;// SaveToFile('c:\temp\test1.pdf');
        //ole.SaveToFile('c:\temp\test112.pdf') ;
        //application.ProcessMessages;

       //stream_in.SetSize(1000000000);
       //stest:=tfilestream.Create('c:\temp\datei.str',fmOpenReadWrite );
       pause(100);

       try
         ole.savetoStream(stream_in);  //in

       except
        ole.Iconic:=true;
        try
        ole.savetoStream(stream_in);
        except
          showmessage('Die Pdf-Datei l�sst sich nicht  speichern.');
          //exit;
        end;
       end;
       stream_in.position:=0;

        //t:=time;
        //while time()< t+0.0000002 do; //2/100 sec

        lzr.Compress;


      stream_out.Position:=0;
			if stream_out.Size>0 then
			try

        if stream_out.size<15000000 then
        begin

             tabelle.edit;
             stream:=tabelle.CreateBlobStream(Tabelle.FieldByName(feld),bmWrite);
             stream.Seek(0, soFromBeginning);
             stream.CopyFrom(stream_out,stream_out.size);
             //stream.Position:=0;
             t:=time();
             while time()< t+0.0000002 do; //2/100 sec

			  end
        else
        begin
        if lowercase(copy(ole.OleClassName,1,13))='word.document' then
            begin
             	ts:=sql_datetostr(now);
              ts:=ts+'-'+timetostr(now);
              ts:=stringreplace(ts,'.','-',[rfreplaceall]);
              ts:=stringreplace(ts,':','-',[rfreplaceall]);
              ts:=stringreplace(ts,' ','-',[rfreplaceall]);
              ts:=stringreplace(ts,'"','',[rfreplaceall]);
             com.wordobjekterstellen(ole);
             if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.', [ pfad_temp]));
             com.doku_speichern(pfad_temp+'grosse_datei'+ts+'.doc');
             com.word_schliessen;
             showmessage('Die Word-Datei darf (komprimiert) nicht gr�sser als 15MB sein.'+
             chr(13)+'Kopie der Word-Datei unter '+ pfad_temp+'grosse_datei'+ts+'.doc');
            end
        else showmessage(format('Die Dateigr��e betr�gt %d MB. Sie darf  nicht gr�sser als 15MB sein.',[round(stream_out.size/1000000)]));
        end;
			except
				showmessage('Fehler beim Schreiben vom stream');

			end
      else showmessage('Datei wurde nicht gespeichert.');

      //if tabelle.State in [dsedit, dsinsert] then tabelle.Post;

	finally
		  stream.Free;
		  stream_in.free;
		  stream_out.free;
      dbgridext_texte.Enabled:=t_e;
      dbgridext_texte.Repaint;
      dbtreeview.Enabled:=t_e;
      tabelle.enablecontrols; //vorlagentitel wird wieder zur�ckgesetzt
		  lzr_akt:=false;
		  screen.cursor:=crdefault;
      try
        if (c<>nil) and c.Enabled then c.setfocus;   //hier krachts ggf
      except
      end;
	end;
finally
    if Tabelle.findfield('ole')=nil then showmessage('Fehler: Datei wurde nicht gespeichert');
end;

end;



procedure tform_main.datei_speichern(tabelle:tzquery;feld, datname:string);
var
stream:tstream;
stream_in:tfilestream;
stream_out:tmemorystream;
t:ttime;
begin


	if  kein_datensatz(tabelle) then exit;
	IF NOT fileexists(datname) then exit;
		t:=time();
		while lzr_akt do
		begin
		  if time()> t+0.00001 then exit;
        showmessage('datei_speichern');
        application.ProcessMessages;
		end;
	try
			lzr_akt:=true;
      lzr_akt_s:='datei_speichern';
			stream_in:=tfilestream.Create(datname,fmOpenRead );
			stream_out:=tmemorystream.Create;
			lzr.InputStream:=stream_in;
			lzr.OutputStream:=stream_out;
			tabelle.edit;
			stream:=tabelle.CreateBlobStream(Tabelle.FieldByName(feld),bmReadWrite);
			stream_in.position:=0;

			lzr.Compress;
			stream_out.Position:=0;
			stream.CopyFrom(stream_out,stream_out.size);
			stream.Position:=0;

	finally
		  stream.Free;
		  stream_in.free;
		  stream_out.free;
		  lzr_akt:=false;
	end;
end;



procedure tform_main.datei_laden(tabelle:tzquery;feld,datname:string);
var
stream:tstream;
stream_in:tmemorystream;
 stream_out:tfilestream;
	t:ttime;
begin

	if kein_datensatz(tabelle) then exit;
	//IF fileexists(datname) then deletefile(datname);
	//IF fileexists(datname) then exit;
		t:=time();
		while lzr1_akt do
		begin
		  if time()> t+0.00001 then exit;
        showmessage('datei_laden');
		end;
try
	try
		lzr1_akt:=true;
    lzr1_akt_s:='datei_laden';
		stream_in:=tmemorystream.Create;
		stream_out:=tfilestream.Create(datname,fmCreate);
		if Tabelle[feld]<>'' then
		begin
        lzr1.InputStream:=stream_in;
			lzr1.OutputStream:=stream_out;
			stream:=tabelle.CreateBlobStream(Tabelle.FieldByName(feld),bmRead);
			stream.position:=0;
			stream_in.LoadFromStream(stream);
			stream_in.Position:=0;

			lzr1.Decompress;
			stream_out.Position:=0;
		end;
	finally
		stream.Free;
		stream_in.free;
		stream_out.free;
		lzr1_akt:=false;
	end;
except
end;  
end;





procedure tform_main.strings_speichern(tabelle:tzquery;feld:string; liste:tstrings);     //nicht mehr komprimiert  , muss auf blob feld schreiben
var
stream:TStream;
begin
   if kein_datensatz(tabelle) then exit;
	try
      //tabelle.edit;
			stream:=tabelle.CreateBlobStream( Tabelle.FieldByName(feld),bmWrite);
  		liste.SaveToStream(stream);
      stream.Position:=0;

 	finally
		  stream.Free;
      //tabelle.post;
 	end;
end;



procedure tform_main.strings_laden(tabelle:tzquery;feld:string; const liste:tstrings);   // abchecken ob koprimiert
var
stream:tstream;
stream_in:tmemorystream;
stream_out:tmemorystream;
t:ttime;
l:longint;
a:array[1..4] of char;
s,f:string;
begin
 liste.Clear;
	//if (tabelle.eof) and (tabelle.bof) then exit;
  if kein_datensatz(tabelle) then  	exit;
  if Tabelle.FindField(feld) =nil then exit;
  if Tabelle.FindField(feld).asstring='' then exit;
  f:= Tabelle.FindField(feld).asstring;
  //liste.Clear;
  try
    stream:=tabelle.CreateBlobStream(Tabelle.FieldByName(feld),bmRead);
    stream.position:=0;
    if stream.Size>=4 then
      begin
        stream.ReadBuffer(a,4);
        s:= a[1]+a[2]+a[3]+a[4];
      end
    else
      s:='';

    if (s='!HZL')  or (s='WRZL') then
    begin
        t:=time();
        while lzr1_akt do
        begin
          if time()> t+0.0001 then exit;
            showmessage('string_laden');
            application.ProcessMessages;
        end;
          lzr1_akt:=true;
          lzr1_akt_s:='strings_laden';
          stream_in:=tmemorystream.Create;
          stream_out:=tmemorystream.Create;
          lzr1.InputStream:=stream_in;
          lzr1.OutputStream:=stream_out;
          stream_in.LoadFromStream(stream);
          stream_in.Position:=0;
        try
          if stream_in.size>0 then 	l:=lzr1.Decompress;
          stream_out.Position:=0;
          liste.LoadFromStream(stream_out);
        except
          stream_in.Position:=0;
          liste.LoadFromStream(stream_in);
        end;
          //stream.Free;
          stream_in.free;
          stream_out.free;
          lzr1_akt:=false;
     end
     else
     begin
          stream.Position:=0;
          //if s='WRZL' then stream.Position:=5;
          liste.LoadFromStream(stream);
     end;
   finally
     stream.Free;
     lzr1_akt:=false;
   end;
end;


procedure richeditladen(dataset:tdataset;feld:string;richedit:trichedit);
var
feld_blob:string;
begin // feld immer ohne blob
  feld_blob:=feld+'_blob';
  richedit.Text:='';
  if dataset.FindField(feld_blob)<>nil then
  form_main.strings_laden(tzquery(dataset),feld_blob,richedit.Lines);
  //if richedit.Text='' then richedit.Text:=dataset.findfield(feld).asstring;
  if richedit.Text='' then form_main.strings_laden(tzquery(dataset),feld,richedit.Lines);
  richedit.Modified:=false;
end;

procedure richeditspeichern(dataset:tdataset;feld:string;richedit:trichedit);    //feld ohne Blob
var
feld_blob,s:string;
begin
  feld_blob:=feld+'_blob';
  if richedit.Modified then
  begin
    if not (dataset.State in [dsedit, dsinsert]) then dataset.Edit;
     s:=richedit.text;
     dataset.findfield(feld).asstring:= s;
     form_main.strings_speichern(tzquery(dataset),feld_blob,richedit.Lines);
    richedit.Modified:=false;
  end;
end;

{function tform_main.strings_laden_txt(tabelle:tzquery;feld:string):string;   // abchecken ob koprimiert
var
stream:tstream;
stream_in:tmemorystream;
stream_out:tmemorystream;
t:ttime;
l:longint;
a:array[1..4] of char;
s:string;


function streamread(s:tstream):string;
var i:longint;
b: char;

begin
for i:=s.Position+1 to s.size-1 do
          begin
            s.ReadBuffer(b,1);
            result:=result+b;
            s.Position:=i;
          end;
end;

//######################################
begin

	//if (tabelle.eof) and (tabelle.bof) then exit;
  if kein_datensatz(tabelle) then  	exit;
  if Tabelle.FindField(feld).asstring='' then exit;
  if Tabelle.FindField(feld) =nil then exit;

  try
    stream:=tabelle.CreateBlobStream(Tabelle.FieldByName(feld),bmRead);
    stream.position:=0;
    stream.ReadBuffer(a,4);
    s:= a[1]+a[2]+a[3]+a[4];
    if (s='!HZL')  or (s='WRZL') then
    begin
        t:=time();
        while lzr1_akt do
        begin
          if time()> t+0.0001 then exit;
            showmessage('string_laden');
            application.ProcessMessages;
        end;
          lzr1_akt:=true;
          lzr1_akt_s:='strings_laden';
          stream_in:=tmemorystream.Create;
          stream_out:=tmemorystream.Create;
          lzr1.InputStream:=stream_in;
          lzr1.OutputStream:=stream_out;
          stream_in.LoadFromStream(stream);
          stream_in.Position:=0;
        try
          if stream_in.size>0 then 	l:=lzr1.Decompress;
          stream_out.Position:=0;
          result:=streamread(stream_out);
        except
          stream_in.Position:=0;
           result:=streamread(stream_in);
        end;
          //stream.Free;
          stream_in.free;
          stream_out.free;
          lzr1_akt:=false;
     end
     else
     begin
          stream.Position:=0;
          if s='WRZL' then stream.Position:=5;


          result:=streamread(stream);
     end;
   finally
     stream.Free;
     lzr1_akt:=false;
   end;
end;}

function olecontainer_anzeigen(ole:tolecontainerext):boolean;
var
s:string;
begin
  if ole.State in [osempty] then
  begin
   showmessage('Das Dokument ist leer');
  	exit;
  end;
  s:=copy(ole.oleclassname,0,13);
  if s='' then
  begin
  	showmessage('Die Anwendung um das Dokument anzuzeigen scheint nicht korrekt installiert zu sein');
  	exit;
  end;

	try
  {ole.UpdateVerbs;
  e_status('olecontainer_anzeigen vor doverb');
	ole.DoVerb(ovshow);
  e_status('olecontainer_anzeigen nach doverb'); }
  oleanzeigen(ole);

	result:=com.wordobjekterstellen(ole); //nur wenn word
  ole.Modified:=false;
  //ole.modified_reset(5000);
	except
		showmessage('Das OLE-Objekt konnte nicht erstellet werden '+chr(13)+ole.Name+chr(13)+ole.OleClassName+chr(13)+ole.SourceDoc);

	end;
end;


procedure TForm_main.Arbeitsmedizin_FirmaLButtonDown(Sender: TObject);
begin
	olecontainer_anzeigen(arbeitsmedizin_Firma);
end;





procedure TForm_main.Textvorlagen1Click(Sender: TObject);
var ber:integer;
begin
//if nicht_satellit_tab then exit;
  ber:=b.b_string('Dokumenten-vorlagen');
  if ber =-1 then exit;
  speichern;
	form_vorlagen:=tform_vorlagen.create(self);
    form_vorlagen.TabControl1.Visible:=false;
    form_vorlagen.Edit_datum.Visible:=false;
    form_vorlagen.SpeedButton_dat.Visible:=false;
    form_vorlagen.Label6.Visible:=false;
    form_vorlagen.CheckBox_speichern.Visible:=false;
	form_vorlagen.tag:=1;
	//form_vorlagen.caption:='Vorlagen, aktuelle Firma: '+datamodul.q_firma['firma'] ;
	  if ber<2 then
	  begin
		 form_vorlagen.ToolButton_neu.Visible:=false;
		 form_vorlagen.ToolButton_delete.Visible:=false;
	  end;
	  if ber<1 then
	  begin
		 form_vorlagen.ToolButton_edit.Visible:=false;
		 form_vorlagen.ToolButton_sichern.Visible:=false;
	  end;

    form_vorlagen.Notebook_beschreibung.PageIndex:=0;
	  form_vorlagen.Panel_bottom.Height:=0;
	  form_vorlagen.filtern;
	  form_vorlagen.showmodal;
	  form_vorlagen.release;
	  form_vorlagen:=nil;
end;

procedure tform_main.bescheinigung_drucken(sender:tobject);
 var
   allowchange:boolean;
   datum:tdate;
   s_nummer,query,s_arbmedvv:string;
   bm:tbookmark;
begin
    be_druck_immer:=0;

     if not b.b_alter_ok(datamodul.q_untersuchung) then exit;
     PageControl_mainChanging (Sender,allowchange );
     if not allowchange then exit;
     speichern;
     //pagecontrol_main.ActivePage:=tabsheet_stammdaten;
     //pagecontrol_main.ActivePage:=tabsheet_stammdaten;
     datum:=datamodul.q_untersuchung.findfield('datum').AsDateTime ;
     application.ProcessMessages;
     form_kartei_druck:=tform_kartei_druck.create(self);
     form_kartei_druck.einzeln:=true;
     form_kartei_druck.modus:=0;

     form_kartei_druck.MaskEdit_dat.Text:=datetostr(datum);
     //form_kartei_druck.MaskEdit_dat.Text:=datetostr(now()-30);
     form_kartei_druck.MaskEdit_dat_bis.Text:='31.12.2100';
     if g_erstelldatum_besch='' then g_erstelldatum_besch:=datetostr(date());
     form_kartei_druck.MaskEdit_erstelldatum.Text:=g_erstelldatum_besch;   // hier noch glob Variable


       s_nummer:= datamodul.q_untersuchung.findfield('i_typ').asstring;
        query:=format('select i_arbmedvv from typ where nummer=%s',[s_nummer]);
        s_arbmedvv:=mysql_d.Feldinhalt(query,0);
       if s_arbmedvv='0' then
       begin
          form_kartei_druck.CheckBox_vo_pro.Checked:=false;
          form_kartei_druck.CheckBox_vo_fi.Checked:=false;
          form_kartei_druck.CheckBox_ei_pro.Checked:=true;
          form_kartei_druck.CheckBox_ei_fi.Checked:=true;

       end
       else
       begin
          form_kartei_druck.CheckBox_vo_pro.Checked:=true;
          form_kartei_druck.CheckBox_vo_fi.Checked:=true;
          form_kartei_druck.CheckBox_ei_pro.Checked:=false;
          form_kartei_druck.CheckBox_ei_fi.Checked:=false;

       end;
           //form_kartei_druck.RadioGroup_typ.ItemIndex:=1
           //else form_kartei_druck.RadioGroup_typ.ItemIndex:=0 ;

     try
         bm:=datamodul.q_untersuchung.GetBookmark;
         form_kartei_druck.showmodal;
     finally
        g_erstelldatum_besch:=form_kartei_druck.MaskEdit_erstelldatum.Text;
        form_kartei_druck.Release;
        datamodul.q_untersuchung.Refresh;
        datamodul.q_untersuchung.GotoBookmark(bm);
        datamodul.q_untersuchung.FreeBookmark(bm);
     end;

     


end;

procedure tform_main.bescheinigung_drucken_proband(sender:tobject);
begin
    be_druck_immer:=0;
	//bescheinigung_erstellen(datamodul.q_untersuchung['datum'],0,0,'','proband');
end;

procedure tform_main.bescheinigung_drucken_Firma_Proband(sender:tobject);
begin
    be_druck_immer:=0;
	//bescheinigung_erstellen(datamodul.q_untersuchung['datum'],0,0,'firma_proband');
end;

procedure tform_main.bescheinigung_emailen(sender:tobject);
var
   allowchange:boolean;
   datum:tdate;
   s_nummer,query,s_arbmedvv:string;
begin
    be_druck_immer:=0;

     if not b.b_alter_ok(datamodul.q_untersuchung) then exit;
     PageControl_mainChanging (Sender,allowchange );
     if not allowchange then exit;
     speichern;
     //pagecontrol_main.ActivePage:=tabsheet_stammdaten;
     //pagecontrol_main.ActivePage:=tabsheet_stammdaten;
     datum:=datamodul.q_untersuchung.findfield('datum').AsDateTime ;
     application.ProcessMessages;
     form_kartei_druck:=tform_kartei_druck.create(self);
     form_kartei_druck.Caption:='Bescheinigungen per email versenden';
     form_kartei_druck.Label2.Caption:='Die folgenden Bescheinigungen werden per email verschickt.';
     form_kartei_druck.CheckBox_nicht_sdruck.Checked:=false;
     form_kartei_druck.CheckBox_nicht_sdruck.Visible:=false;
     form_kartei_druck.CheckBox_n_d.Caption:='Nur die Bescheinigungen verschicken, die bisher noch nicht gedruckt sind.';
     form_kartei_druck.CheckBox_close.Caption:='Fenster nach dem Verschicken der mails schlie�en';
     form_kartei_druck.BitBtn_druck.Caption:='verschicken' ;
     form_kartei_druck.einzeln:=true;
     form_kartei_druck.modus:=1; //email
     form_kartei_druck.MaskEdit_dat.Text:=datetostr(datum);
     form_kartei_druck.MaskEdit_erstelldatum.Text:=datetostr(now());
     form_kartei_druck.MaskEdit_dat_bis.Text:='31.12.2100';

       s_nummer:= datamodul.q_untersuchung.findfield('i_typ').asstring;
        query:=format('select i_arbmedvv from typ where nummer=%s',[s_nummer]);
        s_arbmedvv:=mysql_d.Feldinhalt(query,0);
       if s_arbmedvv='0' then
       begin
          form_kartei_druck.CheckBox_vo_pro.Checked:=false;
          form_kartei_druck.CheckBox_vo_fi.Checked:=false;
          form_kartei_druck.CheckBox_ei_pro.Checked:=true;
          form_kartei_druck.CheckBox_ei_fi.Checked:=true;

       end
       else
       begin
          form_kartei_druck.CheckBox_vo_pro.Checked:=true;
          form_kartei_druck.CheckBox_vo_fi.Checked:=true;
          form_kartei_druck.CheckBox_ei_pro.Checked:=false;
          form_kartei_druck.CheckBox_ei_fi.Checked:=false;

       end;
           //form_kartei_druck.RadioGroup_typ.ItemIndex:=1
           //else form_kartei_druck.RadioGroup_typ.ItemIndex:=0 ;


     form_kartei_druck.showmodal;

     //hier verschicken
     form_kartei_druck.Release;


end;




procedure tform_main.Kurz_Anzeige_mailen(sender:tobject);
begin
	speichern;
	dok_erstellen(Arbeitsmedizin,8,'ambulanz',datamodul.q_ambulanz,[''],['']);
  email_erstellen(Arbeitsmedizin, false,'','abteilung','Kurzanzeige');
  olecontainer_close(Arbeitsmedizin);
	//com.doku_schliessen;
	//if word_close then com.word_schliessen;
end;




procedure tform_main.Anhang_mailen(sender:tobject);
var
tit,dat:string;
begin
//showmessage('jetzt mailen');
 speichern;
 if tmenuitem(sender).name='e_ma' then
	 begin
	 olecontainer_laden(datamodul.q_dokumente,'ole',arbeitsmedizin_Proband);
	 olecontainer_anzeigen(arbeitsmedizin_Proband);
	 tit:=datamodul.q_dokumente['titel'];
	 end
 else
	 begin
	 olecontainer_laden(datamodul.q_texte,'ole',arbeitsmedizin_Firma);
	 olecontainer_anzeigen(arbeitsmedizin_Firma);
	 tit:=datamodul.q_texte['titel'];
	 end;
 dat:=pfad_temp+'TextAnhang.rtf';
 if fileexists( dat) then deletefile(dat);

 //application.BringToFront;
 arbene_vor;
//showmessage('jetzt speichern'); //#####
 com.doku_speichern(dat);
//showmessage('jetzt schliessen');
if tmenuitem(sender).name='e_ma' then
   olecontainer_destroy(arbeitsmedizin_proband)
else
    olecontainer_destroy(arbeitsmedizin_firma);

 //com.doku_schliessen;
	//if word_close then com.word_schliessen;
//showmessage('geschlossen');
 try
	form_smtp:=tform_smtp.create(self);
	 form_smtp.Memo.Text:='Siehe Anhang (RTF-Datei)';
	 form_smtp.ComboBoxAttachedFiles.items.add( dat);
	 form_smtp.ComboBoxAttachedFiles.itemIndex := 0;
	 form_smtp.EditSubject.Text:=''+tit;
	 form_smtp.ShowModal;
	 if fileexists( dat) then deletefile(dat);
	finally
	  form_smtp.Release;
	end;
end;

procedure tform_main.adressat_ausdruck(sender:tobject);
begin
Form_arztbrief_drucken:=tForm_arztbrief_drucken.create(self);
Form_arztbrief_drucken.ShowModal;
Form_arztbrief_drucken.Release;

end;

procedure tform_main.Kurz_Anzeige_drucken(sender:tobject);
begin
	speichern;
	dok_erstellen(Arbeitsmedizin,8,'ambulanz',datamodul.q_ambulanz,[''],['']);
	com.doku_drucken;
  olecontainer_destroy(Arbeitsmedizin);
	//com.doku_schliessen;
	//if word_close then com.word_schliessen;
end;

procedure tform_main.personalien_druck(sender:tobject);
begin
  speichern;
  dok_erstellen(Arbeitsmedizin,9,'mitarbeiter',datamodul.q_mitarbeiter,[''],['']);
  com.doku_drucken;
  olecontainer_destroy(Arbeitsmedizin);
end;




function tform_main.bescheinigung_erstellen(dat_erstellt,udat,dat_bis:tdate;modus:integer;typ,adressat:string;n_d,druck_immer,drucken,nicht_sofort_druck,speichern_im_karteibaum:boolean;datname_email:string):boolean;
var ausgabe,druckmodus,query,subj,dat_gedruckt,titel_kartei:string;
	  untersuchungen, untersuchungen_proband: array[0..2] of string;
	  untersucher,nachunt,manummer, sdatum,datname,pdf_datname:string;
	  bdatum,dat,s,nr_unt:string;
     i_typ, i_arbmedvv,vorlage,z:integer;
     weitergabe,word_schliessen_besch:boolean;
//hier die relevanten daten manummer �bergeben
//modus 0 drucken, modus 1 email
//bescheinigung drucken


function bescheinigung_trotzdem_druck:boolean;
begin
  if be_druck_immer=0 then    //nur das erste mal
  begin
	if messagedlg('Sollen Bescheinigungen gedruckt werden, obwohl keine Zustimmung '+chr(13) +'zur Weitergabe vorliegt?',mtWarning,[mbYes, mbNo], 0)=mryes
    then
     	be_druck_immer :=1
    else
    	be_druck_immer:=2
  end;
  case be_druck_immer of
  1: result:=true;
  2: result:=false;
  end;
end;

//procedure gew_besch_erstellen;

	  //udat:tdate;
begin
result:=false;
com.datum_dok_erstellt:=datetostr(dat_erstellt);


if n_d then
  begin
     //firma: w_dat
     //proband: dat_gedruckt
     if adressat='proband' then
      dat_gedruckt:=' and dat_gedruckt="1900-01-01" '
     else
       dat_gedruckt:=' and w_dat="1900-01-01" ' ;
  end
else dat_gedruckt:='';

with datamodul do
try
		  untersuchungen[2]:='';
		  untersuchungen[1]:='';
		  untersuchungen[0]:='';
      untersuchungen_proband[2]:='';
      untersuchungen_proband[1]:='';
		  untersuchungen_proband[0]:='';
        if typ='vorsorge' then i_arbmedvv:=1 else  i_arbmedvv:=0;


		  manummer:=getbigint_str(q_mitarbeiter,'nummer');
		  dat:=datetostr(udat);
		  sdatum:=dat_delphi_to_sql(dat);
      if dat_bis>0 then bdatum:=dat_delphi_to_sql(datetostr(dat_bis))else bdatum:='2999-12-31';

		  query:='select untersuchung.nummer, untersuchung.i_paw, untersuchung.i_typ,untersuchung.datum,untersuchung.memo,untersuchung.memo_proband,untersuchung.i_status,untersuchung.i_weitergabe,untersuchung.i_untersucher, typ.untersuchung,typ.i_arbmedvv, ';
		  query:=query+'untersuchungsart.name,beurteilung.text, untersucher.untersucher as Vo_Untersucher, untersuchungsart2.name as art_name  from untersuchung left join typ on (untersuchung.i_typ = typ.nummer)    ';
        query:=query+ 'left join untersuchungsart on (untersuchung.i_uart=untersuchungsart.nummer) left join beurteilung on (untersuchung.i_beurteilung= beurteilung.nummer) left outer join untersucher on (untersucher.nummer=untersuchung.i_untersucher) ';
       query:=query+' left outer join untersuchungsart2 on (untersuchungsart2.nummer=untersuchung.i_paw)';
		  query:=query +format('  where (i_mitarbeiter =%s and untersuchung.datum>= "%s"  and untersuchung.storno=0 and typ.i_arbmedvv=%d %s) order by untersuchung.datum',[manummer,sdatum,i_arbmedvv,dat_gedruckt]);
		  sql_new(false,q_1,query,'');
		  nachunt:='';

        if  kein_datensatz(q_1) then exit;

		  q_1.first;
        untersuchungen_proband[2]:= untersucher_stempel( q_1.findfield('i_untersucher').asinteger); //q_1.findfield('vo_untersucher').asstring;
        untersuchungen[2]:=untersuchungen_proband[2];
        weitergabe:=true;
		  while not q_1.eof do    //alle untersuchungen
          begin


            if ((q_1.findfield('i_status').asinteger=4) and (q_1.findfield('datum').AsDateTime<=dat_bis ))   then      // muss abgeschlossen sein , Zustimmung muss erteilt sein je nach def. ,
            begin
                //vorsorge: q_typ(i_arbmedvv)=1

                //erst mal weglassen bis genau definiert if ((adressat<>'firma') and (q_1.FindField('i_paw').asinteger=1) or ( q_1.FindField('i_paw').asinteger=0) or ( form_main.zust_arbmed_besch_vorhanden) or bescheinigung_trotzdem_druck then
                if druck_immer or  zust_arbmed_besch_vorhanden(q_1.findfield('i_weitergabe').asinteger,q_1.findfield('i_typ').asinteger,q_1.findfield('datum').AsDateTime 	)then
                begin
                  nr_unt:=q_1.FindField('nummer').asstring;
                  dat:=sql_datetostr(date());
                  //Druckdatum eintragen getrennt nach AG und proband      adressat:
                  //firma: w_dat
                  //proband: dat_gedruckt
                  if adressat='proband' then
                    query:=format('update untersuchung set dat_gedruckt=%s where nummer=%s',[dat,nr_unt])
                  else
                    query:=format('update untersuchung set w_dat=%s where nummer=%s',[dat,nr_unt]);

                  mysql_d.sql_exe(query);
                   { q_1.Edit;
                    q_1.FindField('dat_gedruckt').AsDateTime:=now();
                    q_1.Post;}

                    //zuerst Datum
                    untersuchungen[0]:=untersuchungen[0]+ htmltag(f_ve_datum, datetostr(q_1.findfield('datum').AsDateTime)){+#10#13}; //untersuchung
                    untersuchungen_proband[0]:=untersuchungen_proband[0]+ htmltag(f_ve_datum, datetostr(q_1.findfield('datum').AsDateTime)){+#10#13}; //untersuchung

                    // untersuchungsname
                    untersuchungen[0]:=untersuchungen[0]+htmltag( f_ve_text,q_1.findfield('untersuchung').asstring){+#10#13}; //untersuchung
                    untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag( f_ve_text,q_1.findfield('untersuchung').asstring){+#10#13}; //untersuchung
                    //erst / folgeuntesuchng...
                    if ((i_arbmedvv=0) and (q_1['name']<>null)) then
                    begin
                        untersuchungen[0]:=untersuchungen[0]+htmltag(f_ve_text, q_1.findfield('name').asstring){+#10#13}; //untersuchungsart
                        untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag(f_ve_text, q_1.findfield('name').asstring){+#10#13}; //untersuchungsart
                    end;
                    //art
                    if (i_arbmedvv=1) then
                    begin
                       untersuchungen[0]:=untersuchungen[0]+htmltag(f_ve_art,q_1.findfield('art_name').asstring){+#10#13}; //untersuchung
                       untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag(f_ve_art,q_1.findfield('art_name').asstring){+#10#13}; //untersuchung
                    end;
                     //beurteilung
                    if ((i_arbmedvv=0) and (q_1['text']<>null)) then
                     begin
                          untersuchungen[0]:=untersuchungen[0]+htmltag(f_ve_beurteilung,'Beurteilung:  '+q_1.findfield('text').asstring){+#10#13}; //beurteilung
                          untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag(f_ve_beurteilung,'Beurteilung:  '+q_1.findfield('text').asstring){+#10#13}; //beurteilung
                     end
                     else //vorsorge
                     begin
                         untersuchungen[0]:=untersuchungen[0]+htmltag(f_ve_beurteilung,q_1.findfield('text').asstring){+#10#13}; //beurteilung
                          untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag(f_ve_beurteilung,q_1.findfield('text').asstring){+#10#13}; //beurteilung
                     end;


                    //Anmerkung _ag
                    s:=q_1.findfield('memo').asstring;
                    s:=stringreplace(s,chr(10), '',[rfReplaceAll]);
                    if copy(s,length(s),1)=#13 then s:=copy(s,1,length(s)-1);
                    if ((trim(s)<>'') and (i_arbmedvv=0))  then  //anmerkung firma nur bei eignung
                    begin
                      untersuchungen[0]:=untersuchungen[0]+htmltag(f_ve_text,'Anmerkung Arbeitgeber:  '+s){+#10#13}; //+chr(13);
                      untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag(f_ve_text,'Anmerkung Arbeitgeber:  '+s){+#10#13}; //+chr(13);
                    end
                    else
                    begin
                         untersuchungen[0]:=untersuchungen[0]{+#10#13};
                    end;

                    // anmerkung Proband
                    s:=q_1['memo_proband'];
                    s:=stringreplace(s,chr(10), '',[rfReplaceAll]);
                    if copy(s,length(s),1)=#13 then s:=copy(s,1,length(s)-1);
                    if ((trim(s)<>'')  and (adressat<>'firma'))  then
                      untersuchungen_proband[0]:=untersuchungen_proband[0]+htmltag(f_ve_text,'Anmerkung Proband:  '+s){+#10#13}
                    else
                      untersuchungen_proband[0]:=untersuchungen_proband[0]{+#10#13};
                   //##
                   untersuchungen_proband[0]:=untersuchungen_proband[0]+html_br(f_ve_px);{+#10#13};
                   untersuchungen[0]:=untersuchungen[0]+html_br(f_ve_px){+#10#13};
             	end;
          	end
          else //nachuntersuchungen  status <>4
          begin
              if q_1.findfield('i_status').asinteger=1 then   //geplant
              begin
                untersuchungen[1]:=untersuchungen[1]+htmltag(f_ve_datum,formatdatetime('mmmm " "yyyy',q_1['datum']))+ #10#13;;
                untersuchungen[1]:=untersuchungen[1]+htmltag(f_ve_text,q_1.findfield('untersuchung').asstring){+#10#13};
                if (i_arbmedvv=1) then  untersuchungen[1]:=untersuchungen[1]+htmltag(f_ve_art,q_1.findfield('art_name').asstring){+#10#13};
                untersuchungen[1]:=untersuchungen[1]+html_br(f_ve_px){+#10#13};

              end;
          end;

		q_1.Next;
	end;

	  untersuchungen_proband[1]:=untersuchungen[1];

    if untersuchungen[0]<>'' then //wenn keinen abgeschlossene Untersuchung dann soll nicht gedruckt werden
    begin
       result:=true;
       if modus=0 then ausgabe:='drucken' else ausgabe :='email';   //email w�re besser
       subj:='Arb.med. Bescheinigung f�r '+q_mitarbeiter['name']+', '+q_mitarbeiter['vorname'];
       druckmodus :='drucken_alles';
       if arbmed_besch_offen then word_schliessen_besch:=false else word_schliessen_besch:=true; ;
       if not drucken then druckmodus:='nur_erstellen';

       if adressat='firma_proband' then druckmodus :='drucken_alles'; // 2 bescheinigungen k�nnen nicht offen gehalten werden

       case i_arbmedvv of
         0: titel_kartei:='Eignung';
         1: titel_kartei:='Vorsorge';
       end;

       if (adressat='firma') and (untersuchungen[0]+untersuchungen[1]<>'') then //wenn nicht leer
       begin
          if i_arbmedvv=1 then  vorlage:=13 else vorlage:=2;
          titel_kartei:=titel_kartei+'_Arbeitgeber';
       end;
       if (adressat='proband') and (untersuchungen_proband[0]+untersuchungen_proband[1]<>'') then //wenn nicht leer
       begin
        if i_arbmedvv=1 then  vorlage:=14 else vorlage:=3;
        titel_kartei:=titel_kartei+'_Proband';
        untersuchungen[0]:=untersuchungen_proband[0];
       end;

       //datname:=pfad_bescheinigungen+datamodul.q_mitarbeiter['name']+'-'+datamodul.q_mitarbeiter['vorname']+'-'+ datamodul.q_mitarbeiter.findfield('geb_dat').asstring+'-'+stringdatum+'-';
       datname:=pfad_bescheinigungen+datamodul.q_mitarbeiter['name']+'_'+datamodul.q_mitarbeiter['vorname']+'_'+stringdatum(datamodul.q_mitarbeiter.findfield('geb_dat').asdatetime)+'_'+titel_kartei+'_'+stringdatum(now())+'_';
       datname:=sonderzeichenraus(datname);
       z:=0;
      while  fileexists(datname+inttostr(z)+'.pdf') do inc(z);
      pdf_datname:=datname+inttostr(z)+'.pdf';
      datname:=datname+inttostr(z)+'.doc';

      //arbeitsmedizin_proband.Close;
      //dok_erstellen( Arbeitsmedizin,vorlage,true,ausgabe,druckmodus,word_schliessen,adressat,subj ,'mitarbeiter',datamodul.q_mitarbeiter,['#untersuchungen#','#nachuntersuchungen#','#untersucher#'],untersuchungen,datname );
      dok_erstellen( Arbeitsmedizin,vorlage,'mitarbeiter',datamodul.q_mitarbeiter,['untersuchungen','nachuntersuchungen','untersucher'],
      [untersuchungen[0],untersuchungen[1],untersuchungen[2] ]);
      //pdf_datname:='c:\temp\aaa.pdf';


      if nicht_sofort_druck then
        begin
          arbene_vor;
          if Messagedlg('Soll die Bescheinigung so �bernommen werden?'+#13#10+'Wechslen Sie nach Word und �berpr�fen Sie die Bescheinigung'#13#10+'Danach hier best�tigen',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
        end;
      if arbeitsmedizin.State  <> osopen then
      begin
        arbene_vor;
        showmessage('Das Word-Dokument ist nicht mehr vorhanden - wiederholen Sie bitte den Vorgang');
        exit;
      end;


      //########################### Ausgabe





      if   speichern_im_karteibaum or speicher_bescheinigungen then
      begin
        com.doku_speichern(datname);
        //word_to_pdf(datname,pdf_datname);
      end;

      if datname_email<>'' then
      begin
        com.doku_speichern(datname_email+'.doc');
        //word_to_pdf(datname_email+'.doc',datname_email+'.pdf');
      end;
      //end;
      if drucken then com.doku_drucken;

      olecontainer_close(Arbeitsmedizin);
      arbeitsmedizin.DestroyObject;
      
      arbene_vor;
      pause(100);
      if   speichern_im_karteibaum or speicher_bescheinigungen then
      begin
        word_to_pdf(datname,pdf_datname);
      end;
      if datname_email<>'' then
      begin
        pause(100);
        word_to_pdf(datname_email+'.doc',datname_email+'.pdf');
      end;

      if speichern_im_karteibaum then
      begin
        leeres_dokument_einhaengen(titel_kartei,date,0);
        datamodul.q_dokumente.edit;
        oleausdatei(arbeitsmedizin_Proband,pdf_datname);
        arbeitsmedizin_Proband.Modified:=true;
        datamodul.q_dokumente.post;

      end;



end;
   // aufr�umen
   //result:=true;
   if fileexists(datname) then deletefile(datname);
   if ( (not speicher_bescheinigungen) and fileexists(pdf_datname)) then  deletefile(pdf_datname);

   arbene_vor;
except
  if m_erweitertefehlermeldungen.Checked then
    raise
  else
	  showmessage ('Fehler beim Erstellen der Bescheinigung ggf. "erweiterte Fehmeldungen" ausw�hlen');
end;
end;


function tform_main.leeres_dokument_einhaengen(titel:string;datum:tdate;i_kapitel:integer):int64;
var
i_master:int64;
begin
      i_master:=getbigint(datamodul.q_mitarbeiter,'nummer');
      result:=neuer_datensatz(datamodul.q_dokumente,['nummer','i_mitarbeiter','titel','datum','b_ambulanz','i_kapitel','w_user','w_dat']
		  ,[null,inttostr(i_master),titel,datum,false,i_kapitel,0,'1900-01-01']);
		  direktspeichern:=true;
		  datamodul.q_dokumente.post;

       //datamodul.q_dokumente.refresh;
      form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_dokumente,result,8,-1);
end;

function  tform_main.leeren_text_erzeugen(titel:string):int64;
var
i_master:int64;
begin
  i_master:=getbigint(datamodul.q_Firma,'nummer');
   result:=neuer_datensatz( datamodul.q_texte,['nummer','i_master','mastertab','titel','datum','i_kapitel','i_user','time_origin']
    ,[null,inttostr(i_master),0,titel,now(),0,0,'1900-01-01']);
    direktspeichern:=true;
    datamodul.q_texte.post;
end;




function tform_main.dok_erstellen(container:tolecontainerext;vorlage:integer; tab_name: string;tabelle:tzquery; namen, ersatz:array of string):boolean;
//modus 1 email, 0 drucken, 3 nichts ; subject f�r email
//anhang: rtf-dat wird erzeugt
//tab_name aktueller Tabellenname
//tabelle  entsprechende mysqlquery

var query,dat,datname:string;
 i_firma,i,position,z:integer;
 s_nr,anzahl,anzahlf:string;
 fspez:boolean;
begin        //bescheinigung erstellen
//speichern;
fspez:=true;
result:=true;
dat:='';
with datamodul do
begin

	i_firma:=q_firma['nummer'];
   //firmenspezifisch  Fallbestimmung  s. auch vorlage_w�hlen
	query:=format('select count(*) from vorlagen where (i_filter=%d) and (i_firma= %d) and storno=0', [vorlage,i_firma]);
   anzahlf:=mysql_d.Feldinhalt(query,0);
   if anzahlf='0' then
   begin
      fspez:=false;
   end;
   query:=format('select count(*) from vorlagen where (i_filter=%d) and storno=0', [vorlage]);
   anzahl:=mysql_d.Feldinhalt(query,0);
   if  anzahl='0' then
   begin
      showmessage('Es gibt keine entsprechende Vorlage, bitte unter Einstellungen>Stammdaten>Dokumentenvorlagen anlegen');
      exit;
   end;
   if anzahl='1' then
   begin
   	if fspez then query:=format('select * from vorlagen where (i_filter=%d) and (i_firma= %d)', [vorlage,i_firma])
      else query:=format('select * from vorlagen where (i_filter=%d) ', [vorlage]);
   end
   else
   begin
		 s_nr:=vorlage_waehlen(vorlage,fspez);
       if s_nr='' then exit;
       query:=format('select * from vorlagen where nummer=%s', [s_nr]);
   end;


   sql_new(true,q_vorlagen,query,'');
	olecontainer_laden(q_vorlagen,'ole',container);
  pause(10);

  {try
    container.UpdateVerbs;
    container.DoVerb(ovshow);
  except
    pause(2000);
    container.UpdateVerbs;
    container.DoVerb(ovshow);
  end; }
  oleanzeigen(container);
  pause(10);
	if com.wordobjekterstellen(container) then
	begin
	  com.bookmarks_lesen;
    com.word_unsichtbar;
	  com.bookmarks_einfuegen_tabelle(tab_name,getbigint(tabelle,'nummer'),0);
	  try
	  for i:= 0 to high(namen) do
	  begin
		//ersatz[i]:=copy(ersatz[i],1,255);
      if namen[i]<>'' then
      begin
        if pos('</',ersatz[i])=0 then com.bookmark_einfuegen(namen[i], ersatz[i],true)
        else  com.bookmark_einfuegen_alsdatei(namen[i], ersatz[i],true) ;
      end;
	  end;
	  finally
	  end;
    com.word_sichtbar;
	end;
  

end;
end;



function tform_main.email_erstellen(container:tolecontainerext;anhang:boolean;ausgabe,adressat, subject:string):boolean;
var
modus_2,dat:string;
position:integer;
begin
with datamodul do
  try
  modus_2:='';
	position:=pos(',',ausgabe) ;
	if position>0 then
	begin
		modus_2:=trim(copy(ausgabe, position+1,length(ausgabe)));
		ausgabe:=trim(copy(ausgabe,1,position-1));
	end;

       arbene_vor;
		  form_smtp:=tform_smtp.create(self);
		  if ausgabe='arbeitssicherheit' then
			begin
				email_von_ansprechpartner(form_smtp.memo_to.Lines,datamodul.q_firma['nummer'],2);

				if email_abteilung_send then email_von_ma_abt(form_smtp.memo_to.Lines,getbigint(q_mitarbeiter,'nummer'));  // Unfallmeldung darf laut mail kick 12.4.10 nicht an abteilung geschickt werden
			end;
		  if adressat='abteilung' then email_von_ma_abt(form_smtp.memo_to.Lines,getbigint(q_mitarbeiter,'nummer'));
		  //if adressat='firma' then if datamodul.q_firma['email']<>null then form_smtp.memo_to.Lines.Add(datamodul.q_firma['email']);
		  if adressat='firma' then email_von_ansprechpartner(form_smtp.memo_to.lines,q_firma['nummer'],1);
      //if adressat='personal' then email_von_ansprechpartner(form_smtp.memo_to.lines,q_firma['nummer'],1);
      if adressat='proband'   then  form_smtp.memo_to.Lines.Add(q_mitarbeiter.findfield('email_p').asstring);
		  form_smtp.EditSubject.Text:=Subject;

		  if anhang then
		  begin
				dat:=pfad_temp+'TextAnhang.rtf';
				if fileexists( dat) then deletefile(dat);
				olecontainer_anzeigen(container); //arbeitsmedizin_Proband
				//application.BringToFront;
        arbene_vor;
				com.doku_speichern(dat);
        olecontainer_close(container);
				//com.doku_schliessen;
				//if word_schliessen then com.word_schliessen;
				form_smtp.Memo.Text:='Siehe Anhang (RTF-Datei)';
				form_smtp.ComboBoxAttachedFiles.items.add( dat);
				form_smtp.ComboBoxAttachedFiles.itemIndex := 0;

		  end
		  else
			 com.doku_memo(form_smtp.memo);   //form_smtp.Memo.Text:=com.doku_txt;

		  if modus_2='senddirekt' then
		  begin
				result:=form_smtp.send(true);
				if not result then
				showmessage('Die mail konnte nicht verschickt werden, verschicken Sie sie aus der Mailbox.');

		  end
		  else
			 if form_smtp.ShowModal<>mrok then
			 begin
			 result:=false;
			 //if MessageDlg('E-mail konnte nicht verschickt werden drucken?', mtConfirmation	,[mbYes, mbNo], 0)=mryes then
			 //		  com.doku_drucken;
			 end;
		finally
		  form_smtp.release;
		  if fileexists( dat) then deletefile(dat);
		end;
end;


procedure TForm_main.ToolButton_ma_suchClick(Sender: TObject);

begin
	//dbspeichern(true); //17.01.2001
  ToolButtonsuchen.Enabled:=false;
  application.ProcessMessages;
  try
    speichern;
    formular_sperren;
    ma_suchen('');
  finally
  ToolButtonsuchen.Enabled:=true;
  end;
end;

function tform_main.ma_suchen(n:string):boolean;
begin
  result:=false;
	if firma_gefiltert then form_namsuch.filtern;
  if not einzelplatz then
  begin
		datamodul.q_mitarbeiter_such.Refresh;
		ma_such_neu:=false;
	end;

	if ma_such_neu then
	begin
		datamodul.q_mitarbeiter_such.Refresh;
		ma_such_neu:=false;
	end;



	form_namsuch.Edit1.Text:=n;
	form_namsuch.CheckBox_email_f.Checked:=false;
	form_namsuch.CheckBox_email_p.Checked:=false;

	if form_namsuch.showmodal= mrok then
	begin
		//if pagecontrol_main.ActivePage=tabsheet_firma
		 //	then  pagecontrol_main.ActivePage:=tabsheet_stammdaten;

		application.ProcessMessages;
		gehezu_stammdaten(getbigint(datamodul.q_mitarbeiter_such,'nummer'));
		//pagecontrol_main.ActivePage:=TabSheet_stammdaten;

		//PageControl_mainChange(pagecontrol_main);
		namenanzeigen;
     result:=true;
	end;
end;


function Tform_main.schluessel(wort1,wort2: string): string;
var
	q_string,h_string ,querstring,querstring1, querstring2,buchstaben,zahlen : string;
	i, dezimal,h_zahl,q_zahl: integer;
	quersumme: int64;

begin
	wort1:=trim(lowercase(wort1));
	wort2:=trim(lowercase(wort2));
	buchstaben:='tzuKLswlyxvbUIOYXCVpadfoqPASDFBnmQWERTierGHJZcghjkNM';
	zahlen:='31596275418462143697913603672392';
	quersumme:=0;
	result:='';
	for i:=1 to length(wort1) do
		begin
			quersumme:=quersumme +ord(wort1[i]);
		end;
		quersumme:=quersumme+124;
		q_string:=inttostr(quersumme);
		q_string:=copy(q_string,1,3);
		q_zahl:=strtoint(q_string);
		for i:=1 to 6 do
		begin
			h_string:=copy(zahlen,i*3-2,3);
			h_zahl:=strtoint(h_string)+q_zahl;
			h_string:=inttostr(h_zahl);
			querstring1:=querstring1+copy(h_string,length(h_string)-2,3);
		 end;;

	  quersumme:=0;
	  for i:=1 to length(wort2) do
		begin
			quersumme:=quersumme +ord(wort2[i]);
		end;
	  quersumme:=quersumme+124;
	  q_string:=inttostr(quersumme);
	  q_string:=copy(q_string,1,3);
		q_zahl:=strtoint(q_string);

	  for i:=1 to 6 do
	  begin
		h_string:=copy(zahlen,i*3-2,3);
		h_zahl:=strtoint(h_string)+q_zahl;
		h_string:=inttostr(h_zahl);
		querstring2:=querstring2+copy(h_string,length(h_string)-2,3);
	  end;


	  querstring:=querstring1+querstring2;
          h_string:='';
	  for i:=1 to 18 do
	  begin
	  dezimal:=strtoint(copy(querstring,2*i-1,2))div 2;
	  h_string:=lowercase(h_string+copy(buchstaben,dezimal,1));
	  end;

          result:='';
          for i:=1 to 9 do
          begin
          result:=result+copy(h_string,i,1);
          result:=result+copy(h_string,i+9,1);
          end;

end;


procedure TForm_main.m_Berechtigungen_bearbClick(Sender: TObject);
begin
if nicht_satellit_tab then showmessage('Ver�nderungen werden beim n�chsten Datenabgleich �berschrieben.');//exit;
speichern;
//if user_id <>1 then
	if  b.b_string('Berechtigungen')=-1  then exit;

with datamodul do
try
	//q_schluessel.Locate('nummer',inttostr(user_id),[]);
  q_schluessel.Locate('nummer',(user_id),[]);

	form_berechtigung:=TForm_berechtigung.create(self);
	form_berechtigung.tag:=1;
	form_berechtigung.Edit_p_alt.Visible:=false;
	form_berechtigung.Label_p_alt.Visible:=false;

	form_berechtigung.ShowModal;
finally
	form_berechtigung.dbradiogroup_aktion.DataSource:=nil;
	form_berechtigung.Release;
  form_berechtigung:=nil;
	berechtigung_firma;
  berechtigungen_einstellen;
    //baum_sichtbarkeiten(true);
  pagecontrol_main.TabIndex:=0;
  pagecontrol_firma.TabIndex:=0;
end;
end;

procedure TForm_main.hilfe_inhaltClick(Sender: TObject);
var
pfad:string;
begin
	pfad:=wurzelpfad(application.ExeName);
    pfad:=pfad+'\arbene.chm';
    shellexecute_se(0,'open',pchar(pfad),'','',sw_shownormal);

end;

procedure TForm_main.DBgridEXT_untersuchungCellClick(Column: TColumn);
begin
	bereich_speichern;
end;

procedure TForm_main.PageControl_untersuchungExit(Sender: TObject);
begin
	bereich_speichern;
end;

procedure TForm_main.m_maverschiebenClick(Sender: TObject);
vAR query:string;
	  manr:int64;
begin

if  b.b_string('proband-Verschieben')=-1 then exit;
if pagecontrol_main.ActivePage<> TabSheet_stammdaten then
begin
  showmessage('Der Kartenreiter "Probanden" muss ausgew�hlt sein.');
  exit;
end;
speichern;
with datamodul do
begin
  manr:=getbigint(datamodul.q_mitarbeiter,'nummer');
	form_verschieb:=TForm_verschieb.create(self);

	query:=format('select * from firma where nummer in (%s)',[firmen_berechtigung]);
	sql(true,q_1,query,'firma');
	if form_verschieb.ShowModal=mrok then
	begin
		q_mitarbeiter.edit;
		q_mitarbeiter['i_firma']:=q_1['nummer'];
		direktspeichern:=true;
		q_mitarbeiter.Post;
		q_mitarbeiter.Refresh ;
	end;
	form_verschieb.Release;
	gehezu_stammdaten(manr);
end;
end;








procedure TForm_main.m_BilderListeClick(Sender: TObject);
begin
if nicht_satellit_tab then exit;
if  b.b_string('anwendung')=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.suchfeld:='beschreibung';
	form_tabellen.Notebook.PageIndex:=0;
	datamodul.sql(false,datamodul.q_1,'select * from bitmaps','');
	//form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='bmp';
	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Beschreibung';
	form_tabellen.DBGrid_tabellen.Columns[0].width:=17;
	form_tabellen.DBEdit_bild_beschreibung.DataField:='beschreibung';
	form_tabellen.DBimage.DataField:='bmp';
	grid_einstellen(form_tabellen.dbgrid_tabellen);
  form_tabellen.caption:='Bilder Liste';
	form_tabellen.showmodal;
	form_tabellen.release;
	//datamodul.q_bitmaps.Refresh;
end;



procedure tform_main.einstellungen_lesen;    //user bezogen!!
var
t:string;
liste:tstringlist;
stream:tstream;
i,z:integer;
begin
with datamodul do
try
	//q_schluessel.Locate('nummer',inttostr(user_id),[]);
	q_schluessel.Locate('nummer',(user_id),[]);

	liste:=tstringlist.Create;
	stream:=q_schluessel.CreateBlobStream(q_schluessel.FieldByName('einstellungen'),bmRead);
	liste.LoadFromStream(stream);
	if liste.Count=0 then
	begin
       //reglesen;   //alte reg funktionier nicht mehr
        speichern_fragen:=false;
        bestaet_neu:=true;
        archiv_show:=false;
        backup_durchf:=true;
        m_befunde_c:=true;
        m_befunde_c:=true;
        m_vorsorge_c:=true;
        m_impfung_C:=true;
        m_labor_C:=true;
        m_ambulanz_c:=true;
        m_diagnosen_c:=true;
        m_besonderheiten_c:=true;
        m_dokumente_C:=true;
        m_artikel_c:=true;
        m_rechnungen_c:=true;
        m_kontakte_c:=true;
        m_gdt_c:=true;
        m_storno_show.Checked:=false;
        storno_show:=false;//   2012/01/12
        showmessage('bitte anschlie�end "Optionen" aufrufen und die Einstellungen vornehmen');
        //Optionen1Click(self); tut nicht
	   exit;
	end;
   try
      email_name:= liste[2];
      email_adresse:=liste[3];
      email_pop:= liste[4];
      email_userein:= liste[5];
      email_passwortein:=liste[6];
      email_smtp:= liste[7];
      email_useraus:= liste[8];

       form_main.Width:=strtoint(liste[21]);
      form_main.Height:=strtoint(liste[22]);
      form_main.Top:=strtoint(liste[23]);
      form_main.left:=strtoint(liste[24]);
      form_main.panel_tree.Width:=min(strtoint(liste[25]),form_main.width-500);
      form_main.groupbox_mitarbeiter.Width:=min(strtoint(liste[26]),form_main.width-500);
      form_main.groupbox_firma.Width:=min(strtoint(liste[27]),form_main.width-500);
      form_main.groupbox_ansprech.Width:=min(strtoint(liste[28]),form_main.width-500);
       form_main.panel_tree.Width:=max(form_main.panel_tree.Width,100);
      form_main.groupbox_mitarbeiter.Width:=max(form_main.groupbox_mitarbeiter.Width,100);
      form_main.groupbox_firma.Width:=max(form_main.groupbox_firma.Width,100);
      form_main.groupbox_ansprech.Width:=max(form_main.groupbox_ansprech.Width,100);

      status_change_at_post:=liste[9]='true';
      email_send:=liste[10]='true';
       email_abteilung_send:=liste[44]='true';
      archiv_show:=liste[11]='true';
      m_ArchivierteProbandenanzeigen.Checked:=archiv_show;
      storno_show:=false;//   2012/01/12
       bestaet_neu:=liste[12]='true';
      arbmed_besch_offen:=liste[31]='true';
      word_close:=liste[32]='true';
      rechnung_offen:=liste[33]='true';
       speichern_fragen:=liste[40]='true';
       jahr_sperre:=liste[41]='true';
       backup_durchf:=liste[42]='true';
       einzelplatz:=liste[45]='true';

      abrechnen_voso:=liste[34]='true';
      abrechnen_befund:=liste[35]='true';
      abrechnen_impfung:=liste[36]='true';
      abrechnen_labor:=liste[37]='true';


      m_befunde_c:=liste[13]='true';
      m_vorsorge_c:=liste[14]='true';
      m_impfung_C:=liste[15]='true';
      m_labor_C:=liste[16]='true';
      m_ambulanz_c:=liste[17]='true';
      m_diagnosen_c:=liste[18]='true';
      m_besonderheiten_c:=liste[19]='true';
      m_dokumente_C:=liste[20]='true';
      m_artikel_c:=liste[38]='true';
      m_rechnungen_c:=liste[39]='true';
      m_kontakte_c:=liste[43]='true';
      if length(liste[46])>3 then liste[46]:='100';
       //if length(liste[47])>3 then liste[47]:='100';
      pagecontrol_firma.TabWidth:=strtoint(liste[46]);
      if pagecontrol_firma.Tabwidth<=125 then pagecontrol_firma.Tabwidth:=125;


       druck_mit_zustimmung:=liste[48]='true';
       panel_wiedervorlage.Width:=strtoint(liste[49]);
       jahresschluessel:=liste[50];
       abgleich_beginn:=liste[51]='true';
       abgleich_ende:=liste[52]='true';
       ole_text:=strtoint(liste[53]);
       ole_tab:=strtoint(liste[54]);
       m_gdt_c:=liste[55]='true';
       druck_mit_zustimmung_eignung:=liste[56]='true';
       druck_mit_zustimmung_besch:=liste[57]='true';
       druck_mit_zustimmung_besch_eignung:=liste[58]='true';
       g_labornummer_nummer:=liste[59];
       g_labornummer_vorspann:=liste[60];
       speicher_bescheinigungen:=liste[61]='true';
       speicher_rechnungen:=liste[62]='true';
       bescheinigung_in_kartei:=liste[63]='true';
       rechnungen_in_kartei:=liste[64]='true';
       email_via_mapi:=liste[65]='true';
       f_ve_datum:=liste[66];
       f_ve_text:=liste[67];
       f_ve_art:=liste[68];
       f_ve_beurteilung:=liste[69];
       termine_startzeit:=liste[70];
       untersuchung_planen_at_post:=liste[71]='true';
       drucke_bescheinigungen:=liste[72]='true';
       email_bescheinigungen:=liste[73]='true';
       f_ve_px:=liste[74];
       ldt_was:=liste[75];
       ldt_vor:=liste[76];
       ldt_nach:=liste[77];
       splitter_gefaehrdungen:=strtoint_safe(liste[78]);
       splitter_ap:= strtoint_safe(liste[79]);
       wdv_besch:=liste[80]='true';
       wdv_impfu:=liste[81]='true';
       wdv_labor:=liste[82]='true';
       wdv_ambul:=liste[83]='true';
       wdv_dokfi:=liste[84]='true';
       wdv_dokpr:=liste[85]='true';
       wdv_beson:=liste[86]='true';
       wdv_proje:=liste[87]='true';
       panel_zeitabrechnung_li.Width:=strtoint_safe(liste[88]);
       abgleich_sat_anpassen:=liste[89]='true';
       abgleich_neugewinnt:= liste[90]='true';
       zeiterfassung_showauswahl:=liste[91]='true';
       l_csv_arbene.CommaText:=liste[92];
       l_csv.CommaText:=liste[93];
       csv_pfad:=liste[94];
       csv_m:=liste[95];
       csv_w:=liste[96];
       csv_fl:=liste[97];
       csv_fh:=liste[98];
       csv_fg:=liste[99];
       csv_fw:=liste[100];
       csv_time:= strtoint_safe(liste[101]);
       csv_datum:= strtoint_safe(liste[102]);
       csv_ueberschreiben:=liste[103]='true';
       csv_archiv:=liste[104]='true';
       csv_archiv_rest:=liste[105]='true';
       csv_ueberein:=strtoint_safe(liste[106]);
       pdf_via_dat:=liste[107]='true';
       wdv_refi:=liste[108]='true';
       runadmin:=liste[109]='true';
       untergeordnete_firma_bez:=liste[110];
       g_termin_filtern_tab:=strtoint(liste[111]);

       if panel_zeitabrechnung_li.Width<423 then  panel_zeitabrechnung_li.Width:=423;

       if f_ve_datum='platzhalter' then f_ve_datum:='';
       if f_ve_text='platzhalter' then f_ve_text:='';
       if f_ve_art='platzhalter' then f_ve_art:='';
       if f_ve_beurteilung='platzhalter' then f_ve_beurteilung:='';
       if termine_startzeit='platzhalter' then termine_startzeit:='08:00';
       if f_ve_px='platzhalter' then f_ve_px:='5';
       if ldt_vor='platzhalter' then ldt_vor:='14';
       if ldt_nach='platzhalter' then ldt_nach:='14';
       if ldt_was='platzhalter' then ldt_was:='0';
       if splitter_ap<300 then splitter_ap:=300;
       if splitter_gefaehrdungen<300 then splitter_gefaehrdungen:=300;
       if splitter_zeitabrechnung.Left>splitter_agenda.Left then  panel_zeitabrechnung_li.Width:=423;
    except
    end;

	//50-100 platz f�r neues
    if liste.Count>=200  then      //verl�ngerung der liste
    try
      z:=strtoint(liste[200]);
      for i:= z-1 downto 0 do
      begin
        history_add(liste[201+i*2],strtoint64(liste[202+i*2]));
      end;
     except
     end;

 finally
    form_main.storno_sql(storno_show);
    m_storno_show.Checked:=storno_show;
    stream.Free;
    liste.free;


 end;
end;

procedure tform_main.einstellungen_lesen_global;
var
t:string;
liste:tstringlist;
stream:tstream;
i,z:integer;
begin
with datamodul do
try

	liste:=tstringlist.Create;
	stream:=q_main.CreateBlobStream(q_main.FieldByName('t1'),bmRead);
	liste.LoadFromStream(stream);
	if liste.Count=0 then
	begin
	   exit;
	end;

 try
     f_ve_datum:=liste[0];
     f_ve_text:=liste[1];
     f_ve_art:=liste[2];
     f_ve_beurteilung:=liste[3];
     f_ve_px:=liste[4];
     for i:=5 to 5+17 do //liste.Count-1 do //22 //format rechnung
      l_rechnung_format.Add(liste[i]);
     for i:=30 to 36 do //liste.Count-1 do  //�berschrift
      l_rechnung_spalten.Add(liste[i]);
     for i:=40 to 57 do //liste.Count-1 do
      l_labor_werte.Add(liste[i]);
     for i:=70 to 76 do //liste.Count-1 do
      l_labor_hinweise.Add(liste[i]);
  except
  end;



 finally
    stream.Free;
    liste.free;
 end;
end;

procedure tform_main.einstellungen_schreiben;
function booltostr(b:boolean):string;
begin
	if b then result:='true' else result:='false';
end;

var
t:string;
liste:tstringlist;
stream:tstream;
//stream_m:tmemorystream;
i:integer;
data: pnodepointer;
ti:ttime;
begin
with datamodul do
try
	//q_schluessel.Locate('nummer',inttostr(user_id),[]);
	q_schluessel.Locate('nummer',(user_id),[]);

	liste:=tstringlist.Create;
  //stream_m:=tmemorystream.Create;
	liste.add('');//( pfad_temp);
	liste.add('');//(pfad_ldt);

	liste.add(email_name);
	liste.add(email_adresse);
	liste.add(email_pop);
	liste.add(email_userein);
	liste.add(email_passwortein);
	liste.add(email_smtp);
	liste.add(email_useraus);

	liste.add(booltostr(status_change_at_post));
	liste.add(booltostr(email_send));
	liste.add(booltostr(archiv_show));
	liste.add(booltostr(bestaet_neu));

	liste.add(booltostr(m_befunde_c));
	liste.add(booltostr(m_vorsorge_c));
	liste.add(booltostr(m_impfung_C));
	liste.add(booltostr(m_labor_C));
	liste.add(booltostr(m_ambulanz_c));
	liste.add(booltostr(m_diagnosen_c));
	liste.add(booltostr(m_besonderheiten_c));
	liste.add(booltostr(m_dokumente_C));//20


	liste.add(inttostr(form_main.Width));//
	liste.add(inttostr(form_main.Height));
	liste.add(inttostr(form_main.Top));
	liste.add(inttostr(form_main.left));
   i:=round(form_main.panel_tree.Width*100/scale_i1);
	liste.add(inttostr(i));
   i:= round(form_main.groupbox_mitarbeiter.Width*100/scale_i1);
	liste.add(inttostr(i));
   i:=round(form_main.groupbox_firma.Width*100/scale_i1);
	liste.add(inttostr(i));
   i:=round(form_main.groupbox_ansprech.Width*100/scale_i1);
	liste.add(inttostr(i));

	liste.add('');//(pfad_abgleich_lokal); -> in der registry
	liste.add('');//(pfad_abgleich_haupt); //30
	liste.Add(booltostr(arbmed_besch_offen));//31
	liste.add(booltostr(word_close));//32
	liste.add(booltostr(rechnung_offen));//33
	liste.add(booltostr(abrechnen_voso)); //34
	liste.add(booltostr(abrechnen_befund));//35
	liste.add(booltostr(abrechnen_impfung)); //36
	liste.add(booltostr(abrechnen_labor));//37

	liste.add(booltostr(m_artikel_c)); //38
  liste.add(booltostr(m_rechnungen_c));//39
  liste.add(booltostr(speichern_fragen));//40
  liste.add(booltostr(jahr_sperre));//41
  liste.add(booltostr(backup_durchf));//42
  liste.add(booltostr(m_kontakte_c));//43
  liste.add(booltostr(email_abteilung_send));//44
  liste.add(booltostr(einzelplatz));//45
  liste.add(inttostr(pagecontrol_firma.TabWidth));//46
  liste.add('platzhalter');//47   //kann anders verwendet werden
  liste.add(booltostr(druck_mit_zustimmung));//48
   liste.add(inttostr(panel_wiedervorlage.Width));//49    // k
   liste.add(jahresschluessel);
   liste.add(booltostr(abgleich_beginn));
   liste.add(booltostr(abgleich_ende));
   liste.add(inttostr(ole_text));
   liste.add(inttostr(ole_tab));
    liste.add(booltostr(m_gdt_c));//55

    liste.add(booltostr(druck_mit_zustimmung_eignung));//[56]
    liste.add(booltostr( druck_mit_zustimmung_besch));//[57]
    liste.add(booltostr(druck_mit_zustimmung_besch_eignung));//[58]
    liste.add(g_labornummer_nummer);//59
    liste.add(g_labornummer_vorspann);//60
    liste.add(booltostr(speicher_bescheinigungen));//[61]
    liste.add(booltostr(speicher_rechnungen));//[62]
    liste.Add(booltostr(bescheinigung_in_kartei));//63
    liste.add(booltostr(rechnungen_in_kartei));//64
    liste.add(booltostr(email_via_mapi));//65
    liste.add( f_ve_datum);
    liste.add(f_ve_text);
    liste.add(f_ve_art);
    liste.add(f_ve_beurteilung);//69
    liste.add(termine_startzeit);//70
    liste.add(booltostr(untersuchung_planen_at_post)); //71
    liste.add(booltostr(drucke_bescheinigungen));//[72]
    liste.add(booltostr(email_bescheinigungen));//[73]
    liste.Add(f_ve_px);//74
    liste.add(ldt_was);
    liste.add(ldt_vor);
    liste.add(ldt_nach);//77
    liste.add(inttostr(splitter_gefaehrdungen));//78
    liste.add(inttostr(splitter_ap));//79
    liste.add(booltostr(wdv_besch));
    liste.add(booltostr(wdv_impfu));
    liste.add(booltostr(wdv_labor));
    liste.add(booltostr(wdv_ambul));
    liste.add(booltostr(wdv_dokfi));
    liste.add(booltostr(wdv_dokpr));
    liste.add(booltostr(wdv_beson));
    liste.add(booltostr(wdv_proje));//87
    liste.add(inttostr(panel_zeitabrechnung_li.Width));//88
    liste.Add(booltostr(abgleich_sat_anpassen));//89      abgleich_sat_anpassen:=liste[88]='true';
    liste.Add(booltostr(abgleich_neugewinnt));//90
    liste.Add(booltostr(zeiterfassung_showauswahl));//91
    liste.Add(l_csv_arbene.CommaText);//92];
    liste.Add(l_csv.CommaText);//[93];

    liste.Add(csv_pfad);//:=liste[94];
    liste.Add(   csv_m);//:=liste[95];
    liste.Add(   csv_w);//:=liste[96];
    liste.Add(   csv_fl);//:=liste[97];
    liste.Add(   csv_fh);//:=liste[98];
    liste.Add(   csv_fg);//:=liste[99];
    liste.Add(   csv_fw);//:=liste[100];
    liste.Add(inttostr(   csv_time));//:= strtoint_safe(liste[101]);
    liste.Add(inttostr(   csv_datum));//:= strtoint_safe(liste[102]);
    liste.Add(booltostr(   csv_ueberschreiben));//:=liste[103]=true;
    liste.Add(booltostr(   csv_archiv));//:=liste[104]=true;
    liste.Add(booltostr(   csv_archiv_rest));//:=liste[105]=true;
    liste.add(inttostr(csv_ueberein));//106
    liste.Add(booltostr(  pdf_via_dat));//107
    liste.Add(booltostr(wdv_refi));   //108
    liste.add(booltostr(runadmin));//109
    liste.add(untergeordnete_firma_bez); //110
    liste.add(inttostr(g_termin_filtern_tab)); //111
	for i:=112 to 199 do liste.add('platzhalter');

	liste.add(inttostr(combobox_history.items.count)); //Stelle 200
	for i:=0 to combobox_history.items.count-1 do
	begin
	  data:=pnodepointer(ComboBox_history.Items.Objects[i]);
	  liste.Add(combobox_history.items[i]);
	  liste.Add(inttostr(data^.nummer));
	end;


  //q_schluessel.Locate('nummer',inttostr(user_id),[]);
  q_schluessel.Locate('nummer',(user_id),[]);
	q_schluessel.edit;
  q_schluessel.fieldbyname('storno').asinteger:=0;
  try
	stream:=q_schluessel.CreateBlobStream(q_schluessel.FieldByName('einstellungen'),bmReadWrite);
  liste.SaveToStream(stream);
  stream.Position:=0;
  finally
  stream.Free;
  end;
 ti:=time();
 while time()< ti+0.000002 do; //2/10 sec

 q_schluessel.post;


finally

  liste.free;
end;
end;


procedure tform_main.einstellungen_schreiben_global;

function booltostr(b:boolean):string;
begin
	if b then result:='true' else result:='false';
end;

var
t:string;
liste:tstringlist;
stream:tstream;
//stream_m:tmemorystream;
i,j:integer;
data: pnodepointer;
ti:ttime;
begin
with datamodul do
try


	liste:=tstringlist.Create;


    liste.add( f_ve_datum); //0
    liste.add(f_ve_text);
    liste.add(f_ve_art);
    liste.add(f_ve_beurteilung);// 3
    liste.add(f_ve_px);//4
    for j:=0 to l_rechnung_format.count-1 do
      liste.add(l_rechnung_format[j]);
    while liste.Count <30 do liste.Add('');
    for j:=0 to l_rechnung_spalten.count-1 do
      liste.add(l_rechnung_spalten[j]);
    while liste.Count <40 do liste.Add('');
    for j:=0 to l_labor_werte.count-1 do
      liste.add(l_labor_werte[j]);
    while liste.Count <70 do liste.Add('');
    for j:=0 to l_labor_hinweise.count-1 do
      liste.add(l_labor_hinweise[j]);
	q_main.edit;

  try
	stream:=q_main.CreateBlobStream(q_main.FieldByName('t1'),bmReadWrite);
  liste.SaveToStream(stream);
  stream.Position:=0;
  finally
  stream.Free;
  end;

   ti:=time();
   while time()< ti+0.000002 do; //2/10 sec

 q_main.post;

finally
   liste.free;
end;
end;

procedure tform_main.inischreiben(folder:integer);
var
	f:textfile;
	dat,pw,s:string;
  //i:integer;
begin
  try
  try
  s:=datetimetostr(now());
  s:=copy(s,1,6);
  if db_passwort<>'nixreinkommen' then
      pw:=string_verschluesseln(s+db_passwort+s);
   dat:=GetSpecialFolderPath(folder, true)+'\arbene';
   if not directoryexists(dat) then createdir(dat);
  dat:=dat+'\'+application.title+'.ini';
  if folder=0 then dat:=w_pfad+'\'+application.title+'.ini'; //im programmverzeichnis
	AssignFile(F,dat );   {globales }
	rewrite(F);
  writeln(f,'arbene_ini_datei');
  writeln(f,format('<host wert="%s"/>',[db_host]));
  writeln(f,format('<datenbank wert="%s"/>',[db_datenbank]));
  writeln(f,format('<passwort wert="%s"/>',[pw]));
  writeln(f,format('<protokoll wert="%d"/>',[idb_protocol]));
  writeln(f,format('<last_backup wert="%s"/>',[last_backup]));
  writeln(f,format('<wahl_der_db wert="%s"/>',[dbwahl]));
  writeln(f,format('<liste_auswahl1 wert="%s"/>',[h1]));
  writeln(f,format('<liste_auswahl2 wert="%s"/>',[h2]));
  writeln(f,format('<liste_auswahl3 wert="%s"/>',[h3]));
  writeln(f,format('<skalierung1 wert="%d"/>',[scale_i1]));
  writeln(f,format('<skalierung2 wert="%d"/>',[scale_i2]));
  writeln(f,format('<skalierung3 wert="%d"/>',[scale_i3]));
  writeln(f,format('<fontsize_anmerkung wert="%d"/>',[fontsize_anmerkung]));

  {writeln(f,format('<pfad_temp wert="%s"/>',[pfad_temp]));        weiter in der registry
  writeln(f,format('<pfad_ldt wert="%s"/>',[pfad_ldt]));
  writeln(f,format('<pfad_infotexte wert="%s"/>',[pfad_infotexte]));
  writeln(f,format('<pfad_mysql_daten wert="%s"/>',[pfad_mysql_daten]));

  writeln(f,format('<pfad_mysql_prg wert="%s"/>',[pfad_mysql_prg]));
  writeln(f,format('<pfad_backupverzeichnis wert="%s"/>',[pfad_backupverzeichnis]));
  writeln(f,format('<pfad_vorlagen wert="%s"/>',[pfad_vorlagen]));
  writeln(f,format('<pfad_bescheinigungen wert="%s"/>',[pfad_bescheinigungen]));
  writeln(f,format('<pfad_rechnungen wert="%s"/>',[pfad_rechnungen]));
  writeln(f,format('<pfad_abgleich_lokal wert="%s"/>',[pfad_abgleich_lokal]));

  writeln(f,format('<pfad_abgleich_haupt wert="%s"/>',[pfad_abgleich_haupt]));
  writeln(f,format('<pfad_pdf_converter wert="%s"/>',[pfad_pdf_converter]));
  writeln(f,format('<abgl_iphaupt wert="%s"/>',[abgl_iphaupt]));
  writeln(f,format('<abgl_dbhaupt wert="%s"/>',[abgl_dbhaupt])); }


  {	writeln(f,db_host);
  writeln(f,db_datenbank);
  writeln(f,last_backup);
  writeln(f,dbwahl);
  writeln(f,h1);
  writeln(f,h2);
  writeln(f,h3);
  writeln(f,scale_i1);
  writeln(f,scale_i2);
  writeln(f,scale_i3);
  writeln(f,idb_protocol);  }
  finally
    CloseFile(F);
  end;

  except
  end;

end;


procedure tform_main.inilesen;
var
	f:textfile;
	dat,s,s1,gruppe,variable, wert,pw,pw_1:string;
begin
   dat:=GetSpecialFolderPath(28,false)+'\arbene';  //user
   dat:=dat+'\'+application.title+'.ini';
   if not fileexists(dat) then
   begin
      dat:=GetSpecialFolderPath(35,false)+'\arbene';   //common
      dat:=dat+'\'+application.title+'.ini';
      if not fileexists(dat) then dat:=w_pfad+'\'+application.title+'.ini';
   end;


	try
  if not fileexists(dat) then showmessage('Die INI-Datei "'+dat+'" ist nicht vorhanden');
	AssignFile(F,dat );   {globales }
	Reset(F);
   Readln(F, S); //bei neuer Version steht hier arbene_ini_datei
      if true then //s='arbene_ini_datei' then
      begin
        while not eof(f) do
        begin
          Readln(F, S);
          xms(s, gruppe, variable, wert);
          if gruppe='host' then db_host:=wert;
          if gruppe='datenbank' then db_datenbank:=wert;
          if gruppe='passwort' then  pw:=wert;
          if gruppe='protokoll' then idb_protocol:=strtoint(wert);
          if gruppe='last_backup' then last_backup:=wert;
          if gruppe='wahl_der_db' then dbwahl:=wert;
          if gruppe='liste_auswahl1' then h1:=wert;
          if gruppe='liste_auswahl2' then h2:=wert;
          if gruppe='liste_auswahl3' then h3:=wert;
          if gruppe='skalierung1' then scale_i1:=strtoint(wert);
          if gruppe='skalierung2' then scale_i2:=strtoint(wert);
          if gruppe='skalierung3' then scale_i3:=strtoint(wert);
          if gruppe='fontsize_anmerkung' then fontsize_anmerkung:=strtoint(wert);

          {if gruppe='pfad_temp' then pfad_temp:=wert;   weiter in der registry
          if gruppe='pfad_ldt' then pfad_ldt:=wert;
          if gruppe='pfad_infotexte' then pfad_infotexte:=wert;
          if gruppe='pfad_mysql_daten' then pfad_mysql_daten:=wert;
          if gruppe='pfad_mysql_prg' then pfad_mysql_prg:=wert;
          if gruppe='pfad_backupverzeichnis' then pfad_backupverzeichnis:=wert;
          if gruppe='pfad_vorlagen' then pfad_vorlagen:=wert;
          if gruppe='pfad_bescheinigungen' then pfad_bescheinigungen:=wert;
          if gruppe='pfad_rechnungen' then pfad_rechnungen:=wert;
          if gruppe='pfad_abgleich_lokal' then pfad_abgleich_lokal:=wert;
          if gruppe='pfad_abgleich_haupt' then pfad_abgleich_haupt:=wert;
          if gruppe='pfad_pdf_converter' then pfad_pdf_converter:=wert;
          if gruppe='abgl_iphaupt' then abgl_iphaupt:=wert;
          if gruppe='abgl_dbhaupt' then abgl_dbhaupt:=wert;}

        end;

      end
      else  //wird nicht mehr aufgefufen
      begin  //alte inidatei reihenfolge
      //Readln(F, S); //host
          db_host:=s;
          readln(f,s1); //datenbank
          readln(f,last_backup);
          readln(f,dbwahl);
          readln(f,h1);
          readln(f,h2);
          readln(f,h3);
          readln(f,s);
          if s<>'' then scale_i1:=strtoint(s);
          readln(f,s);
          if s<>'' then scale_i2:=strtoint(s);
          readln(f,s);
          if s<>'' then scale_i3:=strtoint(s);
          readln(f,s);
          if s<>'' then idb_protocol:=strtoint(s) else idb_protocol:=0;
          if s1='' then db_datenbank:='Arbene' else db_datenbank:=s1;
      end;
	finally
	 CloseFile(F);
    if scale_i1<=0 then scale_i1:=100;
    if scale_i2<=0 then scale_i2:=100;
    if scale_i3<=0 then scale_i3:=100;
    if fontsize_anmerkung<9 then fontsize_anmerkung:=12;
    if db_host='' then db_host:='localhost';
    if db_datenbank='' then db_datenbank:='arbene';
    if dbwahl ='' then dbwahl:='0';
    if pw<>'' then
     begin
      if pw='nixreinkommen' then db_passwort:=pw
      else
      begin
        pw_1:=string_entschluesseln(pw);
        pw_1:=copy(pw_1,7,length(pw_1));
        pw_1:=copy(pw_1,1,length(pw_1)-6);
        db_passwort:=pw_1;
      end;
      end;
	end;
  case  idb_protocol of
     0:db_protocol:='mysql';
     1:db_protocol:='MariaDB-5';//'mysql-5';
   end;
   db_login:='Arbene';
   db_port:='3306';
   //if db_passwort='' then db_passwort:='nix'+'reinkommen';

	if pfad_temp='' then pfad_temp:='c:\temp\';
	if pfad_ldt='' then pfad_temp:='c:\ldt\';
  if pfad_infotexte='' then pfad_infotexte:=wurzelpfad(application.ExeName)+'\infotexte\';
end;


procedure reg_lesen;    //neue iniwerte
var
	reg: Tregistry;
  i:integer;
begin
try
	reg := Tregistry.create;

  reg.RootKey:=hkey_current_user;

	if prg_typ=2 then
		reg.openkey('software\stock-informatik\zeus\pfade',true)
  else reg.openkey('software\Klawitter\Arbene\pfade',true);

  pfad_temp:= reg.ReadString('pfad_temp');
  pfad_ldt:=reg.ReadString('pfad_ldt');
  pfad_infotexte:=reg.ReadString('pfad_infotexte');
  pfad_mysql_daten:=reg.ReadString('pfad_mysql_daten');
  pfad_mysql_prg:=reg.ReadString('pfad_mysql_prg');
  pfad_backupverzeichnis:=reg.ReadString('pfad_backupverzeichnis');
  pfad_vorlagen:=reg.ReadString('pfad_vorlagen');
  pfad_bescheinigungen:=reg.ReadString('pfad_bescheinigungen');
  pfad_rechnungen:=reg.ReadString('pfad_rechnungen');
  pfad_abgleich_lokal:=reg.ReadString('pfad_abgleich_lokal');
  pfad_abgleich_haupt:=reg.ReadString('pfad_abgleich_haupt');
  abgl_iphaupt:= reg.ReadString('abgl_iphaupt');
  abgl_dbhaupt:= reg.ReadString('abgl_dbhaupt');
  pfad_pdf_converter:=reg.ReadString('pfad_pdf_converter');
  pfad_pdftk:=reg.readstring('pfad_pdftk');
  if pfad_temp='' then pfad_temp:='c:\';
  if pfad_mysql_daten='' then pfad_mysql_daten:='c:\mariadb55\data\';
  if pfad_mysql_prg='' then pfad_mysql_prg:='c:\mariadb55\bin\';
  if pfad_backupverzeichnis='' then pfad_backupverzeichnis:=pfad_temp;
  if pfad_vorlagen='' then pfad_vorlagen:=pfad_temp;
  if pfad_bescheinigungen='' then pfad_bescheinigungen:=pfad_temp;
  if pfad_rechnungen='' then pfad_rechnungen:=pfad_temp;
	if pfad_ldt='' then pfad_ldt:='c:\';
  if pfad_infotexte='' then pfad_infotexte:=wurzelpfad(application.ExeName)+'\infotexte\';

  if pfad_abgleich_lokal='' then pfad_abgleich_lokal:='c:\';
  if pfad_abgleich_haupt='' then pfad_abgleich_haupt:='c:\';
  if pfad_pdf_converter='' then pfad_pdf_converter:='C:\Program Files (x86)\7-PDF\7-PDF Maker\';
  if pfad_pdftk='' then pfad_pdftk:='C:\Program Files (x86)\PDFtk Server\bin\';

  reg.CloseKey;

  reg.RootKey:=hkey_current_user;;
  adobe_vorhanden:=reg.KeyExists('software\Adobe\Acrobat Reader');
  if adobe_vorhanden then
  begin
    reg.openkey('software\Adobe\Acrobat Reader',true);
    if reg.KeyExists('10.0') then
    if reg.openkey('10.0\Privileged',true) then
    begin
       if reg.ValueExists('bProtectedMode') then i:=reg.ReadInteger('bProtectedMode') else i:=-1 ;
       if (i<>0) and (Messagedlg('Der gesch�tzte Modus des Acrobat Reader X muss deaktiviert werden ',mtConfirmation, mbOKCancel	,0)=mrOK ) then  reg.WriteInteger('bProtectedMode',0);
    end;

    if reg.KeyExists('11.0') then
    if reg.openkey('11.0\Privileged',true) then
    begin
       if reg.ValueExists('bProtectedMode') then i:=reg.ReadInteger('bProtectedMode') else i:=-1 ;
       if (i<>0) and (Messagedlg('Der gesch�tzte Modus des Acrobat Reader XI muss deaktiviert werden ',mtConfirmation, mbOkCancel	,0)=mrOK ) then  reg.WriteInteger('bProtectedMode',0);
    end;
    if reg.KeyExists('DC') then
    if reg.openkey('DC\Privileged',true) then
    begin
       if reg.ValueExists('bProtectedMode') then i:=reg.ReadInteger('bProtectedMode') else i:=-1 ;
       if (i<>0) and (Messagedlg('Der gesch�tzte Modus des Acrobat Reader DC muss deaktiviert werden ',mtConfirmation, mbOkCancel	,0)=mrOK ) then  reg.WriteInteger('bProtectedMode',0);
    end;
  end;
finally
  reg.CloseKey;
	reg.free;
end;
end;




procedure reg_schreiben;
var
	reg: Tregistry;
begin
try
	reg := Tregistry.create;
	reg.RootKey:=hkey_current_user;
	if prg_typ=2 then
	reg.openkey('software\stock-informatik\zeus\pfade',true)
	else reg.openkey('software\Klawitter\Arbene\pfade',true);
	reg.writestring('pfad_temp',pfad_temp);
	reg.writestring('pfad_ldt',pfad_ldt);
  reg.writestring('pfad_infotexte',pfad_infotexte);
  reg.writestring('pfad_mysql_daten',pfad_mysql_daten);
  reg.writestring('pfad_mysql_prg',pfad_mysql_prg);
  reg.writestring('pfad_backupverzeichnis',pfad_backupverzeichnis);
  reg.writestring('pfad_vorlagen',pfad_vorlagen);
  reg.Writestring('pfad_bescheinigungen',pfad_bescheinigungen);
  reg.Writestring('pfad_rechnungen',pfad_rechnungen);
  reg.writestring('pfad_abgleich_lokal',pfad_abgleich_lokal);
	reg.writestring('pfad_abgleich_haupt',pfad_abgleich_haupt);
  reg.writestring('pfad_pdf_converter',pfad_pdf_converter);
  reg.writestring('pfad_pdftk',pfad_pdftk);
  reg.writestring('abgl_iphaupt',abgl_iphaupt);
  reg.writestring('abgl_dbhaupt',abgl_dbhaupt);
	reg.CloseKey;
	reg.free;
except
   showmessage('Sie haben keine Schreib-Berechtigung f�r die Registry "hkey_current_user/software/klawitter/arbene" (regedt32.exe ausf�hren und entsprechende Berechtigung einstellen.)');
end;
end;


procedure TForm_main.m_SystemClick(Sender: TObject);
begin
if  b.b_string('system')=-1 then exit;
    speichern;
	  pagecontrol_main.activepage:=tabsheet_firma;
	  optionen_show(0);
	  //inischreiben;
	  sichtbarkeiten_einstellen;
	  berechtigungen_einstellen;//neu
	  //sichtbarkeiten_einstellen;
end;


function db_connect() : boolean;
begin
try
	datamodul.connection_main.database:=db_datenbank;
	datamodul.connection_main.HostName:=db_host;
	datamodul.connection_main.Port:=strtoint(db_port);
	datamodul.connection_main.User:=db_login;
	datamodul.connection_main.Password:=db_passwort;

	datamodul.connection_main.Connect;
	//datamodul.tr_main.Host:=db_host;
	result:=true;
	connected:=true;
except
	showmessage('Falsche IP-Adresse, mysql nicht gestartet oder Datenbank '+db_datenbank+' existiert nicht '+#10+'Standardeinstellung'+#10+'  Host: localhost'+#10+'  Datenbank: arbene');
	result:=false;
	connected:=false;
end;
end;


function tform_main.ServiceGetStatus(sMachine, sService: PChar): DWORD;
  {******************************************}
  {*** Parameters: ***}
  {*** sService: specifies the name of the service to open
  {*** sMachine: specifies the name of the target computer
  {*** ***}
  {*** Return Values: ***}
  {*** -1 = Error opening service ***}
  {*** 1 = SERVICE_STOPPED ***}
  {*** 2 = SERVICE_START_PENDING ***}
  {*** 3 = SERVICE_STOP_PENDING ***}
  {*** 4 = SERVICE_RUNNING ***}
  {*** 5 = SERVICE_CONTINUE_PENDING ***}
  {*** 6 = SERVICE_PAUSE_PENDING ***}
  {*** 7 = SERVICE_PAUSED ***}
  {******************************************}
var
  SCManHandle, SvcHandle: SC_Handle;
  SS: TServiceStatus;
  dwStat: DWORD;
begin
  dwStat := 0;
  // Open service manager handle.
  SCManHandle := OpenSCManager(sMachine, nil, SC_MANAGER_CONNECT);
  if (SCManHandle > 0) then
  begin
    SvcHandle := OpenService(SCManHandle, sService, SERVICE_QUERY_STATUS);
    // if Service installed
    if (SvcHandle > 0) then
    begin
      // SS structure holds the service status (TServiceStatus);
      if (QueryServiceStatus(SvcHandle, SS)) then
        dwStat := ss.dwCurrentState;
      CloseServiceHandle(SvcHandle);
    end;
    CloseServiceHandle(SCManHandle);
  end;
  Result := dwStat;
end;

function tform_main.besteht_verbindung:boolean;
var s,query:string;
	  ch:boolean;
begin
//with datamodul do
	ch:=false;
	result:=true; //normalfall
	if lowercase(db_host)<>'localhost' then exit;
try
  datamodul.connection_main.connected:=false;
  datamodul.connection_main.protocol:=db_protocol;
  datamodul.connection_main.User:=db_login;
  datamodul.connection_main.Password:=db_passwort;
  datamodul.connection_main.Database:='mysql';
  datamodul.connection_main.HostName:='localhost';
  datamodul.connection_main.connect;
  datamodul.connection_grant.connected:=false;
  datamodul.connection_grant.protocol:=db_protocol;
  datamodul.connection_grant.User:=db_login;
  datamodul.connection_grant.Password:=db_passwort;
  datamodul.connection_grant.Database:='mysql';
  datamodul.connection_grant.HostName:='localhost';
  datamodul.connection_grant.connect;
  //	mysql_d:=tmysql_d.create( 'localhost',db_protocol,db_port,'mysql','root','');
  mysql_d.connect( 'localhost',db_protocol,db_port,'mysql',db_login,db_passwort);
  result:= mysql_d.sql_exe_grant('select * from user ');
  //mysql_d.disconnect;
  //mysql_d.free;
  datamodul.connection_main.Disconnect;
  datamodul.connection_grant.Disconnect;
except
   result:=false;
//connection_main.Disconnect;
//end;
end;
end;

function tform_main.ist_db_da:boolean;
var s,query:string;
	  ch:boolean;
begin
//with datamodul do
result:=true;
if lowercase(db_host)<>'localhost' then exit;
//irgendawas funkt nicht wenn anders als localhost
try
	datamodul.connection_main.Database:=db_datenbank;
	datamodul.connection_main.HostName:=db_host;
	datamodul.connection_main.User:=db_login;
	datamodul.connection_main.Password:=db_passwort;
	datamodul.connection_main.Protocol:=db_protocol;
	//datamodul.connection_main.HostName:=db_host;
  //showmessage(db_host);

	mysql_d.connect( db_host,db_protocol,db_port,db_datenbank,db_login,db_passwort);

  result:= mysql_d.sql_exe('select * from firma ');
  //mysql_d.disconnect_d;
  //mysql_d.free;
  datamodul.connection_main.Disconnect;
except
   result:=false;
end;
end;


function tform_main.grant:boolean;
var s,query,m:string;
	  ch:boolean;
	  t:ttime;
begin
//with datamodul do
	ch:=false;
	result:=false; //normalfall
	if lowercase(db_host)<>'localhost' then exit; //darf wegen root nur lokal ausgef. werden



try
  try
    datamodul.connection_main.Database:='mysql';
    datamodul.connection_main.HostName:='localhost';


    mysql_d.connect( 'localhost',db_protocol,db_port,'mysql','Arbene_Admin','75vsk&a2');
    m:='Verbindung zum User Abene_Admin korrekt'+#13;
  except
    showmessage ('Erstellen Sie jetzt in MariaDB einen Benutzer mit allen Rechten : user: "Arbene_Admin", passwort: "75vsk&a2" - Dann erst weiter (Arbene mit "localhost" starten!)');
  end;

  try
    datamodul.connection_main.Database:='mysql';
    datamodul.connection_main.HostName:='localhost';


    mysql_d.connect( 'localhost',db_protocol,db_port,'mysql','Arbene_Admin','75vsk&a2');
    m:='Verbindung zum User Abene_Admin korrekt'+#13;
  except
    showmessage (' user: "Arbene_Admin", passwort: "75vsk&a2"  wurden nicht erstellt');
    exit;
  end;

	query:='select host from user where user="Arbene" and host="localhost"';
	s:=mysql_d.Feldinhalt( query,0);
	if s='' then
	begin
   {query:='GRANT Select,  Insert,  Update,  Delete,  Create,  Drop,  Reload,  Shutdown,   File,   References,  Index,  Alter ,   Super,  Execute,  Event,  Trigger   ON *.* TO Arbene@"localhost" IDENTIFIED BY "nixreinkommen"';
	  //query:='insert user values ("localhost","Arbene","66a67e0a59a83f8f",0,0,0,0,0,0,0,0,0,0,0,0,0,0) ';
	  //mysql_d.sql_exe_grant(query);
    mysql_d.sql_exe(query);
	  //query:='insert user values ("%","Arbene","66a67e0a59a83f8f",0,0,0,0,0,0,0,0,0,0,0,0,0,0) ';
	  //mysql_d.sql_exe(query);
	  ch:=true; }
    m:=m+'User Arbene auf localhost ist nicht vorhanden -->bitte anlegen'+#13;
	end
  else
  m:=m+'User Arbene auf localhost ist vorhanden  -->Datenbankpasswort vermutlich nicht korrekt'+#13;

	query:='select host from user where user="Arbene" and host="%"';
	s:=mysql_d.Feldinhalt( query,0);
	if s='' then
	begin
    {
	  //query:='insert user values ("localhost","Arbene","66a67e0a59a83f8f",0,0,0,0,0,0,0,0,0,0,0,0,0,0) ';
	  //mysql_d.sql_exe(query);
	  //query:='insert user values ("%","Arbene","66a67e0a59a83f8f",0,0,0,0,0,0,0,0,0,0,0,0,0,0) ';
    query:='GRANT Select,  Insert,  Update,  Delete,  Create,  Drop,  Reload,  Shutdown,   File,   References,  Index,  Alter ,   Super,  Execute,  Event,  Trigger   ON *.* TO Arbene@"%" IDENTIFIED BY "nixreinkommen"';

	  //mysql_d.sql_exe_grant(query);
    mysql_d.sql_exe(query);
	  ch:=true; }
    m:=m+'User Arbene auf % ist nicht vorhanden -->bitte anlegen wenn dies der Server ist'+#13;
	end
  else m:=m+'User Arbene auf % ist vorhanden'+#13;;







finally
	//mysql_d.disconnect_d;
	//mysql_d.free;
  if trim(m)<>'' then   showmessage(m);
	datamodul.connection_main.Disconnect;
end;
ch:=false;
if ch then
begin
	form_backup:=tform_backup.Create(self);
	form_backup.Datenbankinitialisieren;
	form_backup.release;
	t:=time;
	{while time<t+0.0004 do
	begin
		  if besteht_verbindung then exit;
	end;}
result:=true;


end;

end;



function tform_main.optionen_show(db:integer) : boolean;
var i:integer;


function format_css(s:string):string;
var i:integer;
begin
i:=pos('{',s);
s:=copy(s,i+1,length(s));
i:=pos('}',s);
result:=copy(s,0,i-1);
end;

//########################
begin
form_optionen:=tform_optionen.create(self);
with form_optionen do
begin
	result:=false;
	pagecontrol.ActivePage:=tabsheet_allgemein;
  if not mehrplatz then  //mehrplatz
	begin
			panel_abgleich.Visible:=false;
			tabsheet_db.Visible:=false;
	end;

	if db=1 then  //start ohne db
	begin
		tabsheet_db.visible:=true;
		tabsheet_internet.tabvisible:=false;
		tabsheet_pfade.tabvisible:=false;
		tabsheet_abrechnung.TabVisible:=false;
     tabsheet_allgemein.tabvisible:=false;
     tabsheet_vors_eign.TabVisible:=false;
     tabsheet_abrechnung_user.tabvisible:=false;
		pagecontrol.ActivePage:=tabsheet_db;
		bitbtn_default.Visible:=true;
		label18.Visible:=false;
		form_optionen.caption:='Optionen Datenbank';

	end
	else
	begin
	  if  b.b_string('system')=-1 then   //system
	  begin
		  tabsheet_db.TabVisible:=false;
		  tabsheet_internet.TabVisible:=false;
		  tabsheet_abrechnung.TabVisible:=false;
      TabSheet_fehler.TabVisible:=false;
	  end;

	  if  b.b_string('anwendung')=-1 then
	  begin
			 tabsheet_allgemein.tabvisible:=false;
       tabsheet_vors_eign.TabVisible:=false;
			 tabsheet_abrechnung_user.tabvisible:=false;
			 tabsheet_pfade.TabVisible:=false;
       Datenabgleich.TabVisible:=false;
	  end;
	end;


	if connected then
	begin
	  datamodul.sql(false,datamodul.q_1,'select * from haupt_tabelle','');
	  try
		  //edit_temp_mysql.Text:=datamodul.q_1['pfad_temp'];
	  edit_vorspann.Text:=datamodul.q_1['renu_kopf'];
	  spinedit_renu.Value:=datamodul.q_1['renu'];
	  spinedit_mws.Value:=datamodul.q_1['mws'];
    spinedit_mws_reduziert.Value:=datamodul.q_1['i2'];
	  edit_punktwert.text:=datamodul.q_1['punktwert'];
    Edit_zeitansatz.text:=datamodul.q_1.findfield('zeitansatz').asstring;
	  except
		  //groupbox_nurambulanz.visible:=false;
	  end;
	end;

	Edit_host.text:=db_host;
	Edit_mysqladmin.text:=db_mysqladmin;
	edit_temp.text:=pfad_temp;
	edit_ldt.text:=pfad_ldt;
	edit_infotexte.Text:=pfad_infotexte;  //f�r die gezippten ldt-dateien
	edit_mysql_daten.text:=pfad_mysql_daten;
	edit_mysql_prg.text:=pfad_mysql_prg;
	edit_backupverzeichnis.text:=pfad_backupverzeichnis;
  edit_vorlagen.text:=pfad_vorlagen;
  edit_bescheinigungen.Text:=pfad_bescheinigungen;
  edit_rechnungen.text:=pfad_rechnungen;
	edit_abgl_lokal.text:=pfad_abgleich_lokal;
	edit_abgl_mysql.text:=	pfad_abgleich_haupt;
  edit_abgl_iphaupt.text:=abgl_iphaupt;
  edit_abgl_dbhaupt.text:=abgl_dbhaupt;
  edit_hilfsprogramme.Text:=pfad_pdf_converter;
  edit_pdftk.text:=pfad_pdftk;

	Edit_email_name.text:= email_name;
	Edit_email_adresse.text:= email_adresse;
	Edit_email_pop.text:= email_pop;
	Edit_email_userein.text:= email_userein;
	Edit_email_passwortein.text:= email_passwortein;
	Edit_email_smtp.text:= email_smtp;
	Edit_email_useraus.text:= email_useraus;

    spinedit_s1.Value:=scale_i1; //allgemeiner Faktor
    spinedit_s2.Value:=scale_i2; // Breite Formular
    spinedit_s3.Value:=scale_i3; // H�he Formular
    spinedit_s4.Value:=pagecontrol_firma.tabwidth;
    spinedit_s5.Value:=fontsize_anmerkung;
	checkbox_status.Checked:=status_change_at_post;
  checkbox_untersuchung_planen_at_post.checked:=untersuchung_planen_at_post;
  checkbox_email.checked:=email_send;
  checkbox_email_abteilung.checked:=email_abteilung_send;
	checkbox_archiv_show.checked:=archiv_show;
	checkbox_bestaet_neu.checked:=bestaet_neu;
	//checkbox_storno_show.checked:=storno_show;
	CheckBox_arbmed_besch.Checked:=arbmed_besch_offen;
	checkbox_rechnung.checked:=rechnung_offen;
	checkBox_word_close.checked:=word_close;
	checkbox_jahr.checked:=jahr_sperre;
	checkbox_speichern_fragen.checked:=speichern_fragen;
	checkbox_voso_ab.Checked:=abrechnen_voso;
	checkbox_bef_ab.checked:=abrechnen_befund;
	checkbox_impf_ab.Checked:=abrechnen_impfung;
	checkbox_lab_ab.checked:=abrechnen_labor;
	checkbox_backup.Checked:=backup_durchf;
  checkbox_einzelplatz.Checked:=einzelplatz;
  checkbox_dbwahl.checked:= dbwahl='1';
  checkbox_speicher_bescheinigungen.Checked:=speicher_bescheinigungen;
  checkbox_speicher_rechnungen.Checked:=speicher_rechnungen;
  checkbox_bescheinigung_in_kartei.checked:=bescheinigung_in_kartei;
  checkbox_rechnungen_in_kartei.checked:=rechnungen_in_kartei;
  checkbox_druck_mit_zustimmung.checked:=druck_mit_zustimmung;
  CheckBox_druck_mit_zustimmung_eignung.Checked:=druck_mit_zustimmung_eignung;
  CheckBox_druck_mit_zustimmung_besch.Checked:= druck_mit_zustimmung_besch;
  CheckBox_druck_mit_zustimmung_besch_eignung.Checked:=druck_mit_zustimmung_besch_eignung;
  CheckBox_wdv_besch.checked:=wdv_besch;
  CheckBox_wdv_impfu.checked:=wdv_impfu;
  CheckBox_wdv_labor.checked:=wdv_labor;
  CheckBox_wdv_ambul.checked:=wdv_ambul;
  CheckBox_wdv_dokfi.checked:=wdv_dokfi;
  CheckBox_wdv_dokpr.checked:=wdv_dokpr;
  CheckBox_wdv_beson.checked:=wdv_beson;
  CheckBox_wdv_proje.checked:=wdv_proje;
  CheckBox_wdv_refi.checked:=wdv_refi;

  Combobox_ve_datum.Text:=f_ve_datum;
  Combobox_ve_text.Text:=f_ve_text;
  Combobox_ve_art.Text:=f_ve_art;
  Combobox_ve_beurteilung.Text:=f_ve_beurteilung;
  ComboBox_f_ve_px.Text:=f_ve_px;

  CheckBox_ableich_beginn.checked:=abgleich_beginn;
  CheckBox_ableich_ende.checked:=abgleich_ende;
  CheckBox_anpass_berechtigung.Checked:=abgleich_sat_anpassen;
  checkbox_abgleich_neugewinnt.checked:=abgleich_neugewinnt;
  combobox_ole_text.itemindex:=ole_text;
  combobox_ole_tab.itemindex:=ole_tab;
  checkbox_mapi.checked:=email_via_mapi;
  MaskEdit_termine_startzeit.Text:=termine_startzeit;
  checkbox_zeiterfassung.checked:=zeiterfassung_showauswahl;
  Edit_untergeordnete_firma.text:=untergeordnete_firma_bez;

	if prg_typ=3 then
	begin
	  checkbox_voso_ab.Caption:='Behandlung abrechnen';
	  checkbox_impf_ab.Visible:=false;
	  checkbox_lab_ab.visible:=false;
	end;

  try
  for i:=0 to l_csv.Count-1 do form_optionen.StringAlignGrid_csv.Cells[1,i]:=l_csv[i];
  except
  end;

  try
  for i:=0 to l_rechnung_format.Count-1 do form_optionen.StringAlignGrid_rechnung_format.Cells[1,i+1]:=format_css(l_rechnung_format[i]);
  except
  end;

  try
  for i:=0 to l_rechnung_spalten.Count-1 do form_optionen.StringAlignGrid_rechnung_spalten.Cells[1,i+1]:=l_rechnung_spalten[i];
  except
  end;
  try
  for i:=0 to l_labor_werte.Count-1 do form_optionen.StringAlignGrid_labor_werte.Cells[1,i+1]:=format_css( l_labor_werte[i]);
  except
  end;
  try
  for i:=0 to l_labor_hinweise.Count-1 do form_optionen.StringAlignGrid_labor_hinweise.Cells[1,i+1]:=format_css(l_labor_hinweise[i]);
  except
  end;

  edit_csv_pfad.Text:=csv_pfad;
  edit_csv_m.Text:=csv_m;
  edit_csv_w.Text:=csv_w;
  edit_csv_fl.Text:=csv_fl;
  edit_csv_fh.Text:=csv_fh;
  edit_csv_fg.Text:=csv_fg;
  edit_csv_fw.Text:=csv_fw;
  spinedit_csv.Value:=csv_time;
  radiogroup_csv_datum.ItemIndex:=csv_datum;
  radiogroup_csv_ueberein.ItemIndex:=csv_ueberein;

 checkbox_csv_ueberschreiben.Checked:=csv_ueberschreiben;
 checkbox_csv_archiv.Checked:=csv_archiv;
 checkbox_csv_archiv_rest.checked:=csv_archiv_rest;
  checkbox_pdf_via_dat.checked:=pdf_via_dat;
  checkbox_runadmin.checked:=runadmin ;

 //#########################################################################################################################
	if form_optionen.ShowModal=mrok then
	begin
	 if connected then
	 try
		datamodul.q_1.edit;
		//datamodul.q_1['pfad_temp']:=edit_temp_mysql.Text;
		datamodul.q_1['renu_kopf']:=edit_vorspann.Text;
		datamodul.q_1['renu']:=spinedit_renu.Value;
		datamodul.q_1['mws']:=spinedit_mws.Value;
    mws:= spinedit_mws.Value;
    datamodul.q_1['i2']:=spinedit_mws_reduziert.Value;
    mws_reduziert:= spinedit_mws_reduziert.Value;
		datamodul.q_1['punktwert']:=decimalseparator_change(edit_punktwert.text);
    datamodul.q_1.findfield('zeitansatz').asstring:=Edit_zeitansatz.text;

		if datamodul.q_1.state in [dsedit, dsinsert] then datamodul.q_1.Post;
    zeitansatz_lesen;
	 except
		//showmessage('Fehler beim �bertragen der Einstellungen 1');    wenn nummernkreis nue eingelesen wird
	 end;
	 try //wenn db niht gestartet
		db_host:=form_optionen.Edit_host.text;
		db_mysqladmin:=form_optionen.Edit_mysqladmin.text;
		pfad_temp:=makepfad(form_optionen.edit_temp.text);
		pfad_ldt:=makepfad(form_optionen.edit_ldt.text);
		pfad_infotexte:=makepfad(form_optionen.edit_infotexte.text);  //gezippte ldt-dateien
		pfad_mysql_daten:=makepfad(form_optionen.edit_mysql_daten.text);
     pfad_mysql_prg:=makepfad(form_optionen.edit_mysql_prg.text);
		pfad_backupverzeichnis:=makepfad(form_optionen.edit_backupverzeichnis.text);
    pfad_vorlagen:=makepfad(form_optionen.edit_vorlagen.text);
    pfad_bescheinigungen:=makepfad(form_optionen.edit_bescheinigungen.text);
    pfad_rechnungen:=makepfad(form_optionen.edit_rechnungen.text);
		pfad_abgleich_lokal:=makepfad(form_optionen.edit_abgl_lokal.text);
		pfad_abgleich_haupt:=makepfad(form_optionen.edit_abgl_mysql.text);
    abgl_iphaupt:=form_optionen.edit_abgl_iphaupt.text;
    abgl_dbhaupt:=form_optionen.edit_abgl_dbhaupt.text;
    pfad_pdf_converter:=edit_hilfsprogramme.Text;
    pfad_pdftk:=edit_pdftk.text;

		email_name:=Edit_email_name.text;
		email_adresse:=Edit_email_adresse.text;
	  email_pop:=Edit_email_pop.text;
	  email_userein:=Edit_email_userein.text;
	  email_passwortein:=Edit_email_passwortein.text;
	  email_smtp:=Edit_email_smtp.text;
	  email_useraus:=Edit_email_useraus.text;
      g_punktwert:=strtofloat(edit_punktwert.text);
      if spinedit_s1.Value<=0 then spinedit_s1.Value:=100;
      if spinedit_s2.Value<=0 then spinedit_s2.Value:=100;
      scale_i1:=spinedit_s1.Value;
      scale_i2:=spinedit_s2.Value;
      scale_i3:=spinedit_s3.Value;
      pagecontrol_firma.TabWidth:=spinedit_s4.Value;
      fontsize_anmerkung:= spinedit_s5.Value;
    status_change_at_post:=checkbox_status.Checked;
    untersuchung_planen_at_post:=checkbox_untersuchung_planen_at_post.checked;
	  email_send:=checkbox_email.Checked;
      email_abteilung_send:=checkbox_email_abteilung.checked;
	  archiv_show:=checkbox_archiv_show.checked;
    bestaet_neu:=checkbox_bestaet_neu.checked;
	  arbmed_besch_offen:=CheckBox_arbmed_besch.Checked;
	  rechnung_offen:=checkbox_rechnung.checked;
	  word_close:=checkBox_word_close.checked;
    jahr_sperre:=checkbox_jahr.checked;
    speichern_fragen:=checkbox_speichern_fragen.checked;

	  abrechnen_voso:=checkbox_voso_ab.Checked;
	  abrechnen_befund:=checkbox_bef_ab.checked;
	  abrechnen_impfung:=checkbox_impf_ab.Checked;
	  abrechnen_labor:=checkbox_lab_ab.checked;
     backup_durchf:=checkbox_backup.checked;
     einzelplatz:=checkbox_einzelplatz.checked;
     abgleich_beginn:=CheckBox_ableich_beginn.checked;
     abgleich_ende:=CheckBox_ableich_ende.checked;
     abgleich_sat_anpassen:=CheckBox_anpass_berechtigung.Checked;
     abgleich_neugewinnt:=checkbox_abgleich_neugewinnt.checked;
     ole_text:=combobox_ole_text.itemindex;
     ole_tab:= combobox_ole_tab.itemindex;
      if checkbox_dbwahl.checked then dbwahl:='1' else dbwahl:='0';
      speicher_bescheinigungen:=checkbox_speicher_bescheinigungen.Checked;
      speicher_rechnungen:=checkbox_speicher_rechnungen.Checked;
      bescheinigung_in_kartei:=checkbox_bescheinigung_in_kartei.checked;
      rechnungen_in_kartei:=checkbox_rechnungen_in_kartei.checked;
      druck_mit_zustimmung:=CheckBox_druck_mit_zustimmung.checked;
      druck_mit_zustimmung_eignung:=CheckBox_druck_mit_zustimmung_eignung.checked;
      druck_mit_zustimmung_besch:=CheckBox_druck_mit_zustimmung_besch.checked;
      druck_mit_zustimmung_besch_eignung:=CheckBox_druck_mit_zustimmung_besch_eignung.checked;
      email_via_mapi:=checkbox_mapi.checked;
      wdv_besch:=CheckBox_wdv_besch.checked;
      wdv_impfu:=CheckBox_wdv_impfu.checked;
      wdv_labor:=CheckBox_wdv_labor.checked;
      wdv_ambul:=CheckBox_wdv_ambul.checked;
      wdv_dokfi:=CheckBox_wdv_dokfi.checked;
      wdv_dokpr:=CheckBox_wdv_dokpr.checked;
      wdv_beson:=CheckBox_wdv_beson.checked;
      wdv_proje:=CheckBox_wdv_proje.checked;
      wdv_refi:=CheckBox_wdv_refi.checked;

      termine_startzeit:=MaskEdit_termine_startzeit.EditText;
      f_ve_datum:=Combobox_ve_datum.Text;
      f_ve_text:=Combobox_ve_text.text;
      f_ve_art:=Combobox_ve_art.Text;
      f_ve_beurteilung:=Combobox_ve_beurteilung.Text;
      f_ve_px:=ComboBox_f_ve_px.Text;
      zeiterfassung_showauswahl:=checkbox_zeiterfassung.Checked;
      l_csv.Clear;
      l_csv_arbene.Clear;
      for i:=0 to form_optionen.StringAlignGrid_csv.RowCount-1 do l_csv.Add(sonderzeichenraus_all(form_optionen.StringAlignGrid_csv.Cells[1,i]));
      for i:=0 to form_optionen.StringAlignGrid_csv.RowCount-1 do l_csv_arbene.Add(sonderzeichenraus_all(form_optionen.StringAlignGrid_csv.Cells[0,i]));
      l_rechnung_format.Clear;
      for i:=1 to form_optionen.StringAlignGrid_rechnung_format.RowCount-1 do l_rechnung_format.Add(form_optionen.StringAlignGrid_rechnung_format.Cells[2,i]+' {'+form_optionen.StringAlignGrid_rechnung_format.Cells[1,i]+'}');
      l_rechnung_spalten.Clear;
      for i:=1 to form_optionen.StringAlignGrid_rechnung_spalten.RowCount-1 do l_rechnung_spalten.Add(form_optionen.StringAlignGrid_rechnung_spalten.Cells[1,i]);
      l_labor_werte.Clear;
      for i:=1 to form_optionen.StringAlignGrid_labor_werte.RowCount-1 do l_labor_werte.Add(form_optionen.StringAlignGrid_labor_werte.Cells[2,i]+' {'+form_optionen.StringAlignGrid_labor_werte.Cells[1,i]+'}');
      l_labor_hinweise.Clear;
      for i:=1 to form_optionen.StringAlignGrid_labor_hinweise.RowCount-1 do l_labor_hinweise.Add(form_optionen.StringAlignGrid_labor_hinweise.Cells[2,i]+' {'+form_optionen.StringAlignGrid_labor_hinweise.Cells[1,i]+'}');
      csv_pfad:=edit_csv_pfad.Text;
      csv_m:=edit_csv_m.Text;
      csv_w:=edit_csv_w.Text;
      csv_fl:=edit_csv_fl.Text;
      csv_fh:=edit_csv_fh.Text;
      csv_fg:=edit_csv_fg.Text;
      csv_fw:=edit_csv_fw.Text;
      csv_time:=spinedit_csv.Value;
      csv_datum:=radiogroup_csv_datum.ItemIndex;
      csv_ueberein:=radiogroup_csv_ueberein.ItemIndex;
      csv_ueberschreiben:=checkbox_csv_ueberschreiben.Checked;
      csv_archiv:=checkbox_csv_archiv.Checked;
      csv_archiv_rest:=checkbox_csv_archiv_rest.checked;
      pdf_via_dat:=checkbox_pdf_via_dat.checked;
      runadmin:=checkbox_runadmin.checked;
      untergeordnete_firma_bez:=Edit_untergeordnete_firma.text;
	  storno_sql(storno_show);
	 except
    //showmessage('Fehler beim �bertragen der Einstellungen 2');
	 end;
	 result:=true;
	end;
	form_optionen.release;
    //form_main.scaleby(scale_i1,scale_i2);
end;
end;


function str_l_space(str:string; laenge:integer):string;
begin
 if length(str)<laenge then
	 result:=StringOfChar(' ',laenge-length(str))+str
 else result:=copy(str,1,laenge);
end;

function str_r_space(str:string; laenge:integer):string;
begin
 if length(str)<laenge then
	 result:=str+StringOfChar(' ',laenge-length(str))
 else result:=copy(str,1,laenge);
end;


function tform_main.vorlage_waehlen(vorlage:integer;fspez:boolean):string;
var
nummer,i_master,i_mitarbeiter:int64;
titel,kapitel,query:string;
i_kapitel,i:integer;
begin
try
      i:=0;
      i:=i+1;
	  form_vorlagen:=tform_vorlagen.create(self);
    form_vorlagen.Notebook_beschreibung.PageIndex:=1;
	  form_vorlagen.tag:=1;
	  form_vorlagen.ToolBar_main.visible:=false;
	  form_vorlagen.combobox_typ.visible:=false;
	  form_vorlagen.groupbox_typ.visible:=false;
	  form_vorlagen.Arbeitsmedizin_vorlagen.Ctl3D:=true;
	  form_vorlagen.Arbeitsmedizin_vorlagen.Enabled:=false;
	  form_vorlagen.i_filter_p:=vorlage;
	  form_vorlagen.TreeView.DragMode:=dmmanual;
    form_vorlagen.pfad:=vorlagenpfad;
    form_vorlagen.filterindex:=vorlagenfilterindex;
     if fspez then form_vorlagen.RadioGroup_allgemein.ItemIndex:=1;
	  i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter,'nummer');
	  case prg_typ of
		 1,2:
		 begin
			  if datamodul.q_Firma['firma']<>null then form_vorlagen.caption:='Vorlagen, aktuelle Firma: '+datamodul.q_firma['firma'] ;
		 end;
		 4:
		 begin
			  if datamodul.q_Firma['firma']<>null then form_vorlagen.caption:='Vorlagen, aktuelle Abteilung: '+datamodul.q_firma['firma'] ;

		 end;
	  end;

	  if form_vorlagen.ShowModal=mrok then
	  begin

        case form_vorlagen.tabcontrol1.tabindex of
          	0:
            begin
            	//titel:=datamodul.q_vorlagen.findfield('name').asstring;
				      //i_kapitel:=datamodul.q_vorlagen.findfield('i_kapitel').asinteger;

              titel:=form_vorlagen.Edit_titel_wahl.Text;
              i_kapitel:= form_main.combowert(form_vorlagen.combobox_thema_wahl);
        		end;
          	1:
          	begin
                //if not FileExists(form_vorlagen.filelistbox.FileName) then
                if not FileExists(form_vorlagen.Edit_datname.Text) then
                begin
                	showmessage('Datei existiert nicht');

        			exit;
        			end ;
            	titel:=  form_vorlagen.dokutitel.text;
					    i_kapitel:=-1;
        		end;
            2:
          	begin
            	titel:='Objekt';
			      	i_kapitel:=-1;
        		end;
          end;

          result:=datamodul.q_vorlagen.findfield('nummer').asstring;

		//application.BringToFront;
     //application.ProcessMessages;

	 end;
 finally
    vorlagenpfad:=form_vorlagen.pfad;
    vorlagenfilterindex:=form_vorlagen.filterindex;
	 form_vorlagen.release;
	 form_vorlagen:=nil;

end;
end;



function tform_main.text_neu(modus:integer; datum:tdate;ansicht:integer;pdf_loeschen:boolean):int64;// 0 firma, 1 ma
var
nummer,i_master,i_mitarbeiter:int64;
titel,kapitel,query:string;
i_kapitel,i,s:integer;
begin
try
    application.processmessages;
    i:=0;
    i:=i+1;
	  form_vorlagen:=tform_vorlagen.create(self);
    form_vorlagen.Notebook_beschreibung.PageIndex:=1;
 	  form_vorlagen.tag:=ansicht;
	  form_vorlagen.ToolBar_main.visible:=false;
	  form_vorlagen.combobox_typ.visible:=false;
	  form_vorlagen.groupbox_typ.visible:=false;
	  form_vorlagen.Arbeitsmedizin_vorlagen.Ctl3D:=true;
	  form_vorlagen.Arbeitsmedizin_vorlagen.Enabled:=false;
    //form_vorlagen.CheckBox_speichern.Visible:=true;
	  form_vorlagen.i_filter_p:=modus;
	  form_vorlagen.TreeView.DragMode:=dmmanual;
    //form_vorlagen.Timer1.enabled:=true;
    form_vorlagen.Edit_datum.Text:=datetostr(datum);
    form_vorlagen.pfad:=vorlagenpfad;
    form_vorlagen.filterindex:=vorlagenfilterindex;
	  i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter,'nummer');
     if ansicht=2 then //pdf
     begin
     	form_vorlagen.TabControl1.visible:=false;
      form_vorlagen.Notebook1.pageindex:=1;
     end;
	  case prg_typ of
		 1,2:
		 begin
			  if datamodul.q_Firma['firma']<>null then form_vorlagen.caption:='Vorlagen, aktuelle Firma: '+datamodul.q_firma['firma'] ;
		 end;
		 4:
		 begin
			  if datamodul.q_Firma['firma']<>null then form_vorlagen.caption:='Vorlagen, aktuelle Abteilung: '+datamodul.q_firma['firma'] ;

		 end;
	  end;
    //#######################
	  if form_vorlagen.ShowModal=mrok then
	  begin
        datum:=strtodate(form_vorlagen.Edit_datum.Text);
        com.datum_dok_erstellt:=datetostr(datum);

        case form_vorlagen.notebook1.pageindex of
        0:
            begin
            	//titel:=datamodul.q_vorlagen.findfield('name').asstring;
				      //i_kapitel:=datamodul.q_vorlagen.findfield('i_kapitel').asinteger;
              titel:=form_vorlagen.Edit_titel_wahl.Text;
              i_kapitel:= form_main.combowert(form_vorlagen.combobox_thema_wahl);
              i_stempel:= form_main.combowert(form_vorlagen.combobox_unterschrift_wahl); //nachher bei den word-variablen

        		end;
        1:
          	begin
                //if not FileExists(form_vorlagen.filelistbox.FileName) then
              if  Fileinuse(form_vorlagen.edit_datname.text) then
              begin
                	showmessage('Datei ist durch ein anderes Programm ge�ffnet');
        			    exit;
        			end ;
              if not FileExists(form_vorlagen.edit_datname.text) then
              begin
                	showmessage('Datei existiert nicht');
        			exit;
        			end ;
               s:= getfilesize(form_vorlagen.edit_datname.text);//filelistbox.FileName);
               if s >30000000 then
                begin
                	showmessage('Die Datei darf maximal 30MB gross sein');
        				exit;
                end ;
            	titel:=form_vorlagen.dokutitel.text;
					    i_kapitel:=-1;
        		end;
        2:
          	begin
            	titel:='Objekt';
					    i_kapitel:=-1;
        		end;
        end;

		case  modus of
		0: begin  //firma
        //abfragen ob die datei >10000000
       application.ProcessMessages;
		  i_master:=getbigint(datamodul.q_Firma,'nummer');
		  nummer:=neuer_datensatz( datamodul.q_texte,['nummer','i_master','mastertab','titel','datum','i_kapitel','i_user','time_origin']
		                          ,[null,inttostr(i_master),0,titel,datum,i_kapitel,0,'1900-01-01']);
		  direktspeichern:=true;
      form_main.arbeitsmedizin_Firma.modified:=true;
		  datamodul.q_texte.post;
		  entsperren(tabsheet_firma_texte);
      application.ProcessMessages;
      datamodul.q_texte.edit;
          case form_vorlagen.tabcontrol1.tabindex of
          	0: olecontainer_laden(datamodul.q_vorlagen,'ole',arbeitsmedizin_Firma);
          	//1: oleausdatei(arbeitsmedizin_Firma,form_vorlagen.filelistbox.FileName);
            1: oleausdatei(arbeitsmedizin_Firma,form_vorlagen.edit_datname.text);
          	2: oleausobjekt(arbeitsmedizin_Firma);
          end;
		  //olecontainer_laden(datamodul.q_vorlagen,'ole',arbeitsmedizin_Firma);
      arbeitsmedizin_firma.Modified:=true;
      if datamodul.q_texte.State in [dsedit, dsinsert] then datamodul.q_texte.post;
      arbeitsmedizin_firma.Modified:=true;
      arbene_vor;
      //if  (lowercase(copy(ole.OleClassName,1,13))='word.document') or (lowercase(copy(ole.OleClassName,1,8))='acroexch')
      if lowercase(copy(arbeitsmedizin_firma.OleClassName,1,13))='word.document' then    //nur word anzeigen
		  if olecontainer_anzeigen(arbeitsmedizin_Firma) {and (form_vorlagen.tabcontrol1.tabindex=0)} then
		  begin
			 com.bookmarks_lesen;
       com.word_unsichtbar;
			 com.bookmarks_einfuegen_tabelle('firma',i_master,0);
       com.word_sichtbar;
		  end;
		  end;
		1:begin   //proband

		  i_master:=getbigint(datamodul.q_mitarbeiter,'nummer');
      application.ProcessMessages;
		  nummer:=neuer_datensatz(datamodul.q_dokumente,['nummer','i_mitarbeiter','titel','datum','b_ambulanz','i_kapitel','w_user','w_dat']
		  ,[null,inttostr(i_master),titel,datum,datamodul.q_vorlagen['b_ambulanz'],i_kapitel,0,'1900-01-01']);
		  direktspeichern:=true;
		  //datamodul.q_dokumente.post;
       //datamodul.q_dokumente.refresh;

        //datamodul.q_dokumente.edit;
        case form_vorlagen.notebook1.pageindex of
          0: olecontainer_laden(datamodul.q_vorlagen,'ole',arbeitsmedizin_Proband);
          //1: oleausdatei(arbeitsmedizin_Proband,form_vorlagen.filelistbox.FileName);
          1: oleausdatei(arbeitsmedizin_proband,form_vorlagen.edit_datname.Text);
          2: oleausobjekt(arbeitsmedizin_Proband);
        end;
		  arbeitsmedizin_Proband.Modified:=true;
      application.ProcessMessages;
      arbeitsmedizin_proband.Modified:=true;
      //if datamodul.q_dokumente.State in [dsedit, dsinsert] then datamodul.q_dokumente.post;          //wird eh nachher gespeichert
      datamodul.q_dokumente.post;

      form_main.dbTreeView.selected:=form_main.dbTreeView.einhaengen( datamodul.q_dokumente,nummer,8,-1);

      arbeitsmedizin_Proband.Modified:=true;
		  entsperren(panel_texte);
      arbene_vor;
      try
        if (lowercase(copy(arbeitsmedizin_proband.OleClassName,1,13))='word.document') then
        if (ansicht=1) and  olecontainer_anzeigen(arbeitsmedizin_Proband)   then       //
        begin
           //com.kapitel_boukmarks(i_mitarbeiter);    Kapitel deaktiviert  openoffice
         com.word_unsichtbar;
         com.bookmarks_lesen_ersetzen;
         com.bookmarks_einfuegen_tabelle('mitarbeiter',i_master,0);
         com.bookmarks_lesen_serienbrief;
         com.word_sichtbar;
         //com.bookmarks_einfuegen_tabelle('mitarbeiter',i_master,0);
           //com.zeilen_loeschen('//') ;
          // com.kapitel_boukmarks(i_mitarbeiter);
        end;
      except
      //leerer container
       //loeschen;  da ist alles markiert - unklar
      end;
		  end;

		 2:
		  begin

		  end;
		end;
		//application.BringToFront;
     //application.ProcessMessages;

	 end;
 finally
   vorlagenpfad:=form_vorlagen.pfad;
   vorlagenfilterindex:=form_vorlagen.filterindex;
   if (form_vorlagen.notebook1.pageindex =1) and pdf_loeschen and fileexists(form_vorlagen.edit_datname.Text) then
      deletefile (form_vorlagen.edit_datname.Text);

	 form_vorlagen.release;
	 form_vorlagen:=nil;
     result:=nummer;
end;
end;


procedure TForm_main.Arbeitsmedizin_ProbandLButtonDown(Sender: TObject);
begin
   if  not (arbeitsmedizin_proband.state in [osrunning,osOpen	]) then 	olecontainer_anzeigen(arbeitsmedizin_Proband);
end;


procedure TForm_main.savebuttonClick(Sender: TObject);
var
stream:tstream;
filestream: tfilestream;
endung,datname: string;
begin
if rtfedit.FUpdating then Exit;
if savedialog.execute  then
	begin
    datname:= form_main.SaveDialog.FileName;
	  endung:= dateiendung(datname);
	  if endung='' then datname:=datname+'.rtf';
	  if FileExists(datname) then
		 if MessageDlg(Format('Solle %s �berschrieben werden?', [datname]),
			mtConfirmation, mbYesNoCancel, 0) <> idYes then Exit;
     try
		filestream:=tfilestream.Create(datname,fmcreate);
		stream:=datamodul.q_anamnese.CreateBlobStream(datamodul.q_anamnese.FieldByName('memo'),bmread);
		filestream.copyfrom(stream,stream.Size);
     finally
     stream.Free;
     filestream.free;
     end;
	end;
//rtfedit.FileSaveAs;
end;

procedure TForm_main.m_mazusammenfuehrenClick(Sender: TObject);
begin
pagecontrol_main.tabindex:=1;
//application.ProcessMessages;

if b.b_string('proband-Verschieben')=-1 then exit;
//nur auf dem HS
//if nicht_satellit('Probanden m�ssen auf dem Hauptsystem zusammengef�hrt werden') then exit;


   speichern;
   form_ma_unit:=tform_ma_unit.Create(self);
   form_ma_unit.Showmodal;
   form_ma_unit.Release;
 	gehezu_stammdaten(getbigint(datamodul.q_mitarbeiter,'nummer'));

end;

function stringinvert(str:string): string;
var l,i:integer;
begin
	result:='';
  l:=Length(str);
	for i:= l downto 1 do
	begin
		result:=result+str[i];
  end;
end;

function status_farbe(status:integer):integer;
begin
with datamodul do
begin
	sql(true,q_3,'select * from status','');
  //if q_3.Locate('nummer',inttostr(status),[]) then result:=q_3['farbe'] else result:=0;
  if q_3.Locate('nummer',(status),[]) then result:=q_3['farbe'] else result:=0;
end;
end;


function tabellen_name(sql:string):string;
var position:integer;
begin
	position:=pos('from',sql);
	sql:=trim(copy(sql,position+4,length(sql)));
	position:=pos(' ',sql);
	if position> 0 then result:=trim(copy(sql,1,position))
	else result:=trim(sql);
	result:=lowercase(result);
end;



procedure TForm_main.DBLookupComboBox_unters_statusClick(Sender: TObject);
begin
status_neu:=true;
end;


procedure TForm_main.einladung_drucken(nummer:int64;modus:integer) ;
var
i_mitarbeiter:int64;
i_art:integer;
query,s_dat:string;
felder :array [0..5] of string;
sent:boolean;
begin
with datamodul do
begin
	sent:=true;
	query:='select termine.i_mitarbeiter,termine.i_untersuchung,termine.i_art,termine.datum_s, mitarbeiter.vorname, mitarbeiter.name  from termine left join mitarbeiter on (mitarbeiter.nummer=termine.i_mitarbeiter) where termine.nummer='+inttostr(nummer);

	mysql_d.Feldarray(query,[0,1,2,3,4,5],felder);

	i_mitarbeiter:=strtoint64(felder[0]);
  i_art:=strtoint(felder[2]);
  if i_mitarbeiter*i_art=0 then
  begin
  showmessage('Es kann keine Einladung erstellt werden, nicht alle Felder in der Terminplanung ausgef�llt');
  exit;
  end;
	s_dat:=  dat_sql_to_delphi1(felder[3]);//datetostr(sql_strtodate(felder[3]));

	vorlage_laden(4, q_firma['nummer']);//3
	com.bookmarks_einfuegen_tabelle('termine',nummer,0);
	//application.BringToFront;
  //application.ProcessMessages;
  arbene_vor;
	if modus=1 then //email
	  try
		  form_smtp:=tform_smtp.create(self);
		  form_smtp.Memo.Text:=com.doku_txt;
       olecontainer_close(Arbeitsmedizin);
       //application.BringToFront;
       arbene_vor;
		  email_von_ma(form_smtp.memo_to.lines,i_mitarbeiter);
      email_von_ma_abt(form_smtp.memo_to.Lines,i_mitarbeiter);
		  form_smtp.EditSubject.Text:='Einladung f�r ' + trim(felder[4])+' '+trim(felder[5]) + ' am '+s_dat;
		  if form_smtp.ShowModal<>mrok then
			  //if MessageDlg('E-mail konnte nicht verschickt werden drucken?', mtConfirmation	,[mbYes, mbNo], 0)=mryes then
			  //		com.doku_drucken
			  sent:=false; //nicht verschickt oder gedruckt

	  finally
		  form_smtp.release;
	  end
	else  //modus=0
	  begin
		com.doku_drucken;
     	olecontainer_destroy(Arbeitsmedizin);

      arbene_vor;
      //application.BringToFront;
	  end;

	if sent then
	  begin
	   case i_art of
		 1:  sql_new(true,q_1,'select * from untersuchung where nummer = '+felder[1],''); //tabelle:=q_untersuchung_read;
		 2:  sql_new(true,q_1,'select * from impfung where nummer = '+felder[1],''); //tabelle:=q_impfung_read;
		 3:  sql_new(true,q_1,'select * from labor where nummer = '+felder[1],''); //tabelle:=q_labor_read;
		 end;
		q_1.edit;
		q_1['datum_einladung']:=date();
		q_1.post;
	  end;

	end;
end;



procedure TForm_main.Nationen1Click(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('proband-Listen');
if ber=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen_berechtigung(ber);
  form_tabellen.HelpContext:=10;
  form_tabellen.suchfeld:='Nationalit�t';
	form_tabellen.Caption:='Nationalit�t';
	form_tabellen.Notebook.PageIndex:=19;
	form_tabellen.Label_string.Caption:='Nationalit�t';
	form_tabellen.ToolButton_neu.enabled:=true;
	datamodul.sql(true,datamodul.q_1,'select * from nationen','nation');

	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Nation';

	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	form_tabellen.DBEdit_string.DataField:='nation';
	grid_einstellen(form_tabellen.dbgrid_tabellen);

	form_tabellen.showmodal;
  datamodul.q_1.first;
  while not datamodul.q_1.eof do
	begin
	  	datamodul.q_1.edit;
		datamodul.q_1['soundex']:=soundex.soundex(datamodul.q_1['nation']);
		datamodul.q_1.post;
		datamodul.q_1.next;
	end;
  dbcombo_laden;
	form_tabellen.release;


end;

procedure TForm_main.natnew1Click(Sender: TObject);
begin
with datamodul do
begin
	sql(true,q_1,'select * from mitarbeiter','');
	while not q_1.eof  do
	begin
		q_1.edit;
		if q_1['staat']<>null then
		q_1['i_nation']:=nationalitaet(q_1['staat'])
		else q_1['i_nation']:=0 ;
		q_1.post;
		q_1.next;
	end;
end;
end;



{function tform_main.nationalitaet(land:string):integer;
var
	q:tzquery;
	query:string;
	s_land:string;
begin
try
query:='select * from nationen ';

q:=tzquery.create(self);
q.connection:=datamodul.connection_main;
sql_normieren(query);
q.Sql.Text:=query;
q.Open;
	s_land:=soundex.soundex(land);
	s_land:=copy(s_land,1,3);
	if q.Locate('soundex',s_land,[loCaseInsensitive,loPartialKey])then
	result:=q['nummer']
	else result:=-1;
finally
q.free;
end;
end; }

function tform_main.nationalitaet(land:string):integer;
var
	query:string;
	s_land:string;
  res:string;
begin
   result:=-1;
   land:=trim(land);
   s_land:=soundex.soundex(land);
   land:='%'+land+'%';
   query:=format('select nummer from nationen where nation like "%s"',[land]);
   res:= mysql_d.Feldinhalt(query,0);
   if res<>'' then result:=strtoint(res)
   else
   begin
      query:=format('select nummer from nationen where soundex like "%s"',[s_land]);
     res:= mysql_d.Feldinhalt(query,0);
     if res<>'' then result:=strtoint(res);
   end;


end;




function tform_main.i_taetigkeit(i_branche:integer;taetigkeit:string):int64;
var
query,s_result:string;
begin
	query:=format('select nummer from taetigkeiten where  taetigkeit like "%s" ',[taetigkeit+'%']);  //i_branche=%d and Branche nicht mehr bedeutsam
	s_result:=mysql_d.feldinhalt(query,0);
	if s_result='' then
	begin
			neuer_datensatz(datamodul.q_taetigkeiten,['nummer','i_branche','taetigkeit'],[null,i_branche,taetigkeit]);
			datamodul.q_taetigkeiten.post;
			s_result:=mysql_d.feldinhalt(query,0);
	end;
	if s_result<>'' then result:=strtoint64(s_result) else result:=-1;
end;

function tform_main.i_abteilung(i_firma:integer;kuerzel:string;modus:integer):int64;
var
query,s_result:string;
begin
	query:=format('select nummer from abteilung where i_firma=%d and kuerzel like "%s" ',[i_firma, kuerzel+'%']);
	s_result:=mysql_d.feldinhalt(query,0);
	if( (modus=0) and (s_result='') )then    //problem wenn Abteilungen eingelesen werden
	begin
			neuer_datensatz(datamodul.q_abteilung,['nummer','i_firma','kuerzel'],[null,i_firma,kuerzel]);
			datamodul.q_abteilung.post;
			s_result:=mysql_d.feldinhalt(query,0);
	end;
	if s_result<>'' then result:=strtoint64(s_result) else result:=-1;
end;

procedure tform_main.termin_typ_update(tabelle: tzquery);
var query,info,nachname,vorname,typ,hk:string;
	  i_typ:integer;
	  i_termin:int64;
begin
   hk:=chr(39);
	i_termin:=getbigint(tabelle,'i_termin');
	if i_termin=0 then exit;
  i_typ:=tabelle['i_typ'];
  typ:=mysql_d.Feldinhalt(format('select untersuchung from typ where nummer=%d',[i_typ]),0);

  nachname:= datamodul.q_mitarbeiter['name'];
	vorname:= trim(copy(datamodul.q_mitarbeiter['vorname'],1,1))+', ';
	info:=hk+ vorname+nachname+' / '+typ+hk;
  query:=format('update termine set i_typ= %d ,info= %s where nummer= %d',[i_typ,info,i_termin]);
 	mysql_d.sql_exe( query);

	if form_termine<>nil then form_termine.all_refresh;
  dbspeichern(true);

end;

procedure TForm_main.m_ImpfabfolgeClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
speichern;
form_tabellen:=tform_tabellen.Create(self);
form_tabellen.HelpContext:=10840;
form_tabellen_berechtigung(ber);

form_tabellen.Notebook.PageIndex:=3;
datamodul.sql(true,datamodul.q_1,'select * from impfabfolge','reihenfolge');

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='reihenfolge';
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='impfabfolge';
form_tabellen.DBGrid_tabellen.Columns[0].width:=30;
form_tabellen.DBEdit_frist2.DataField:='reihenfolge';
form_tabellen.DBEdit_frist1.DataField:='impfabfolge';
form_tabellen.Label3.caption:='Impfabfolge';
form_tabellen.Label4.caption:='Reihenfolge';
form_tabellen.showmodal;
form_tabellen.release;
dbcombo_laden;

end;

procedure TForm_main.BitBtn_labwahlClick(Sender: TObject);
begin
   //labor
	untersuchung_einzelwahl(3);
   kartei_aktualisieren;
end;

procedure tform_main.untersuchung_einzelwahl(bereich:integer);
begin
try
form_untwahl:=tform_untwahl.Create(self);
with datamodul do
begin
	 sql_new(true,q_typ_tree,format('select * from typ where bereich = %d',[bereich]), 'reihenfolge');
	form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.Tree.bereich_wert:=bereich;
	form_untwahl.tree.mysqlquery:=datamodul.q_typ_tree;
	form_untwahl.tree.mysql_feld:='untersuchung';
	form_untwahl.tree.liste_lesen;
	form_untwahl.tree.dragmode:=dmmanual;
	form_untwahl.multiselect:=false;
	form_untwahl.ToolBar.Visible:=false;
	form_untwahl.Panel_botttom.visible:=true;
	form_untwahl.notebook.visible:=false;
	form_untwahl.Notebook.PageIndex:=3;
	combolesen(form_untwahl.ComboBox_untersucher,'untersucher','untersucher','untersucher','');
	case bereich of
		1: if q_untersuchung['datum']<>null then form_untwahl.MaskEdit_datum.Text:=datetostr(q_untersuchung['datum']);
		2: if q_impfung['datum']<>null then form_untwahl.MaskEdit_datum.Text:=datetostr(q_impfung['datum']);
		3: if q_labor['datum']<>null then form_untwahl.MaskEdit_datum.Text:=datetostr(q_labor['datum']);
	end;
	case bereich of
		1: form_untwahl.caption:='Bescheinigung';
		2: form_untwahl.caption:='Impfung';
		3: form_untwahl.caption:='Labor';
	end;
	if form_untwahl.showmodal<>mrok then
		begin
		  exit;
		end;
	case bereich of
	1:begin
		 q_untersuchung.edit;
		 q_untersuchung['i_typ']:=q_typ_tree['nummer'];
		end;
	2:begin
		 q_impfung.edit;
		 q_impfung['i_typ']:=q_typ_tree['nummer'];
		end;
	3:begin
		 if q_typ_tree['b_group']=1 then exit;
		 q_labor.edit;
		 q_labor['i_typ']:=q_typ_tree['nummer'];
		 q_labor['einheit']:=q_typ_tree['einheit'];
		 if q_mitarbeiter['geschlecht']=1 then q_labor['norm']:=q_typ_tree['norm_w'] else q_labor['norm']:= q_typ_tree['norm_m'] ;
		 //tree_labor.tabelle_feld_abgleich;
		end;
	end;
  //dbtreeview.knoten_neuordnen(dbtreeview.selected);

end;
finally
	form_untwahl.release;
  	speichern;
	application.processmessages;
end;
end;


function  TForm_main.untersuchung_hinzufuegen(i_mitarbeiter:int64;bereich,i_status:integer; datum:tdate;multiselect:boolean):int64;
var
      i_untersucher, i,i_typ:integer;
       knoten: ttreenode;
       daten:pnodepointer;
       flag:boolean;
       i_paw,i_arbmedvv:integer;
       query:string;
       felder :array [0..2] of string;
begin
result:=0;
flag:=false;
with datamodul do
try
	form_untwahl:=tform_untwahl.Create(self);
  form_untwahl.ini(bereich,datum);
  form_untwahl.multiselect:=multiselect;
  form_untwahl.einzelauswahl:=true;
	combogehezu(form_untwahl.ComboBox_untersucher,akt_untersucher);
	//entsperren(form_untwahl.panel_main);

	if form_untwahl.showmodal<>mrok then exit;

	if form_untwahl.Tree.Selected=nil then exit;
	datum:=strtodate(form_untwahl.MaskEdit_datum.text);
	//i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter,'nummer');
	daten:=pnodepointer(form_untwahl.ComboBox_untersucher.items.Objects[form_untwahl.ComboBox_untersucher.Itemindex]);
 i_untersucher:=daten.nummer;

	for i:=0 to  form_untwahl.Tree.Items.Count-1 do
	begin
		if form_untwahl.Tree.Items[i].stateindex>0 then //ausgew�hlt
		begin
       knoten :=form_untwahl.Tree.items[i];
       daten:=knoten.Data;
       i_typ:=daten^.nummer;
       query:=format('select i_paw,i_arbmedvv,ueberschrift from typ where nummer=%d',[i_typ]);
       mysql_d.Feldarray(query,[0,1,2],felder);

       i_paw:=strtoint(felder[0]);
       i_arbmedvv:=strtoint(felder[1]);
       if felder[2]='0' then
       begin
         case bereich of
          1: result:=unt_neu(datum,i_typ,i_paw,i_status,i_mitarbeiter,i_untersucher,true);
          2: result:=impf_neu(datum,i_typ,i_status,i_mitarbeiter,i_untersucher,true);
          3: result:=lab_neu(datum,i_typ,i_status,i_mitarbeiter,i_untersucher,true,false);
         end;
         flag:=true;
       end;
      end;
	end;

	if not flag then //datum anlegen f�r den ttreeview
	begin
	  knoten :=form_untwahl.Tree.Selected;
	  daten:=knoten.Data;
	  i_typ:=daten^.nummer;
     query:=format('select i_paw,i_arbmedvv,ueberschrift from typ where nummer=%d',[i_typ]);
     mysql_d.Feldarray(query,[0,1,2],felder);
    i_paw:=strtoint(felder[0]);
    i_arbmedvv:=strtoint(felder[1]);

    if felder[2]='0' then    //keine �berschrift
	  case bereich of
		 1: begin
				result:=unt_neu(datum,i_typ,i_paw,i_status,i_mitarbeiter,i_untersucher,false); //true
				//q_untersuchung.refresh;
			 end;
		 2: begin
				result:=impf_neu(datum,i_typ,i_status,i_mitarbeiter,i_untersucher,false);     //true
           //q_impfung.refresh;
         end;
      3: begin
           result:=lab_neu(datum,i_typ,i_status,i_mitarbeiter,i_untersucher,true,false);
				//q_labor.refresh;
         end;
    end;
  end;

finally
	form_untwahl.release;
	application.processmessages;
end;
end;



procedure TForm_main.BitBtn_untwahlClick(Sender: TObject);
begin
	untersuchung_einzelwahl(1);

   kartei_aktualisieren;
   formular_entsperren(true,false);
end;

procedure TForm_main.BitBtn_labaendernClick(Sender: TObject);
begin
   //impfung
	untersuchung_einzelwahl(2);
  kartei_aktualisieren;
  formular_entsperren(true,false);

end;


procedure TForm_main.Tree_laborChange(Sender: TObject; Node: TTreeNode);
var data:pnodepointer;
begin
  if node=nil then exit; //bei leerer liste
	data:=node.data;
	if data^.nummer>0 then
	begin
	  //datamodul.ds_labor.enabled:=true;
	end
	else
	begin
    datamodul.sql_post(datamodul.q_labor);
	  //datamodul.ds_labor.enabled:=false;
	end;
  if pagecontrol_labor.ActivePage=tabsheet_lab_graph then labor_zeichnen;
end;


procedure tform_main.impfung_drucken_alle(sender:tobject);
var i_typ:integer;
query,s_typ, s_dat,status,impfstoff:string;
begin
speichern;
with datamodul do
begin

 vorlage_laden(6, q_firma['nummer']);
 com.bookmarks_einfuegen_tabelle('mitarbeiter',getbigint(q_mitarbeiter,'nummer'),0);
 q_impfung.first;
 while not q_impfung.Eof do
 begin
   i_typ:=q_impfung['i_typ'];
 	query:=format('select untersuchung from typ where nummer=%d',[i_typ]);
 	s_typ:=mysql_d.Feldinhalt(query,0);
 	s_dat:=datetostr(q_impfung['datum']);
   query:=format('select status from status where nummer=%s',[inttostr(q_impfung['i_status'])]);
   status:=mysql_d.Feldinhalt(query,0);
   query:=format('select impfstoff from impfstoff where nummer=%s',[inttostr(q_impfung['i_stoff'])]);
   impfstoff:=mysql_d.Feldinhalt(query,0);
   com.texteinfuegen(format('%s  %s  %s %s',[datetostr(q_impfung['datum']), s_typ,impfstoff, status]),true);
   q_impfung.next;
 end;
 	com.doku_drucken;
end;
end;



procedure tform_main.untersuchung_drucken_alle(sender:tobject);
var i_typ,i:integer;
beurteilung,query,s_typ, s_dat:string;
begin
speichern;
with datamodul do
begin
 i_typ:=q_untersuchung['i_typ'];
 query:=format('select untersuchung from typ where nummer=%d',[i_typ]);
 s_typ:=mysql_d.Feldinhalt(query,0);
 s_dat:=datetostr(q_untersuchung['datum']);
 vorlage_laden(6, q_firma['nummer']);
	com.bookmarks_einfuegen_tabelle('mitarbeiter',getbigint(q_mitarbeiter,'nummer'),0);


 q_untersuchung.first;
 while not q_untersuchung.Eof do
 begin
	 try

	 if q_untersuchung['i_beurteilung']>0 then beurteilung:=dbradiogroup_beurteilung_eignung.Items[q_untersuchung['i_beurteilung']-1] else beurteilung:='';
	 com.texteinfuegen(format('  %s  %s  ',[datetostr(q_untersuchung['datum']), string(q_untersuchung['untersuchung'])]),true);
	 com.texteinfuegen(format('       %s  ',[beurteilung]),true);
	 for i :=0 to dbmemo_beurteilung.lines.Count-1 do
		com.texteinfuegen(format('            %s  ',[copy(dbmemo_beurteilung.lines[i],1,length(dbmemo_beurteilung.lines[i])-2)]),true);
	 except
	 end;
	 q_untersuchung.next;
 end;
 	com.doku_drucken;

end;
end;


procedure tform_main.untersuchung_drucken_speziell(sender:tobject);
var
	nodedata:pnodepointer;
	knoten,parent: ttreenode;

procedure memo_drucken(memo: trichedit);
var j:integer;
begin
if  trim(memo.Text)='' then exit;
//com.schrift('', 8,false);
for j:=0 to memo.Lines.Count-1 do
	 begin
		com.texteinfuegen(chr(9)+memo.lines[j],true);
	 end;
end;

procedure audiogramm_drucken;
var
wort, sfl, sfr, sdbr, sdbl, spr, spl:string;
i_weber:integer;
falt:tcolor;
//hd: HWND;
weber :array[0..2] of string;
begin


falt:=audiogramm_re.Color;
audiogramm_re.Color:=clwindow;
pagecontrol_st.ActivePage:=tabsheet_st_re;
//application.BringToFront;
//application.ProcessMessages;
arbene_vor;
if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.', [ pfad_temp]));
audiogramm_re.speichern(pfad_temp+'2.bmp');
//printcontrol.GetImage(audiogramm_re).savetofile(pfad_temp+'2.bmp');
audiogramm_re.Color:=falt;

falt:=audiogramm_li.Color;
audiogramm_li.Color:=clwindow;
pagecontrol_st.ActivePage:=tabsheet_st_li;
application.ProcessMessages;
//printcontrol.GetImage(audiogramm_li).savetofile(pfad_temp+'1.bmp');
audiogramm_li.speichern(pfad_temp+'1.bmp');
audiogramm_li.Color:=falt;

weber[0]:='links';
weber[1]:='medial';
weber[2]:='rechts';
//application.bringtofront;
//	application.ProcessMessages;
arbene_vor;
	  wort:=datamodul.q_akt_untersuchung['str'];
	  i_weber:=strtoint(copy(wort,1,2));
	  sfr:=trim(copy(wort,3,4));
	  sfl:=trim(copy(wort,7,4));
	  sdbr:=trim(copy(wort,11,3));
	  sdbl:=trim(copy(wort,14,3));
	  spr:=trim(copy(wort,17,3));
	  spl:=trim(copy(wort,20,3));
	  com.texteinfuegen('',true);
	  //com.schrift('',12,true);
	  com.texteinfuegen('Weber',true);
	  //com.schrift('',10,false);
     if i_weber >=0 then  com.texteinfuegen(chr(9)+weber[i_weber],true);
	  //com.schrift('',12,true);
	  com.texteinfuegen('SISI',true);
	  //com.schrift('',10,false);
	  com.texteinfuegen(format('%sFrequenz li: %s re: %s', [chr(9),sfr,sfl]),true);
	  com.texteinfuegen(format('%sDezibel  li: %s re: %s', [chr(9),sdbr,sdbl]),true);
	  com.texteinfuegen(format('%sProzent  li: %s re: %s', [chr(9),spr,spl]),true);
	  com.texteinfuegen('',true);

	  //com.schrift('',12,true);

     com.schrift('',11,true);
	  com.texteinfuegen('Rechtes Ohr',true);
     com.texteinfuegen('',true);
	  com.graphikeinfuegen(pfad_temp+'2.bmp');

     com.texteinfuegen('Linkes Ohr',true);
     com.texteinfuegen('',true);
	  com.schrift('',10,false);
	  com.graphikeinfuegen(pfad_temp+'1.bmp');

	  com.texteinfuegen('',true);
	  memo_drucken(richedit_befund);
end;

procedure sehtest_drucken;
begin

	 //printcontrol.GetImage(grid_sehtest).savetofile(pfad_temp+'3.bmp');
     bild_speichern(grid_sehtest,pfad_temp+'3.bmp');

   com.graphikeinfuegen(pfad_temp+'3.bmp');
   
	 //com.texteinfuegen(format('%s %s %s', [chr(9),'Abweichung horizontal:', edit_s_horizontal.text]),true);
	 //com.texteinfuegen(format('%s %s %s', [chr(9),'Abweichung vertikal:', edit_s_vertikal.text]),true);
	 //com.texteinfuegen(format('%s %s', [chr(9),'Phorie']),true);
	 {if checkbox_farbsinn.checked then
			com.texteinfuegen(format('%s %s', [chr(9)+chr(9),'Farbsinn OK']))
			else	com.texteinfuegen(format('%s %s', [chr(9)+chr(9),'Farbsinn gest�rt']));
	 if checkbox_zentral_gesichtsfeld.checked then
			com.texteinfuegen(format('%s %s', [chr(9)+chr(9),'zentrales Gesichtsfeld OK']))
			else	com.texteinfuegen(format('%s %s', [chr(9)+chr(9),'zentrales Gesichtsfeld gest�rt']));
	 if checkbox_stereo.checked then
			com.texteinfuegen(format('%s %s', [chr(9)+chr(9),'Stereo-Sehen OK']))
			else	com.texteinfuegen(format('%s %s', [chr(9)+chr(9),'Stereo-Sehen gest�rt']));}
	 memo_drucken(richedit_befund);
end;

function norm_auffaellig(i:integer):string;
begin
	result:='nicht beurteilt';
	case i of
	  0:result:='Normalbefund';
	  1:result:='Auff�llig';
	end;
end;

procedure wert_drucken;
begin
	com.texteinfuegen(format('%s %s %s', [chr(9),edit_bef.text, label_bezeichner_dim.caption]),true);
	memo_drucken(richedit_befund);
end;

procedure liste_drucken;
begin
	com.texteinfuegen(format('%s %s', [chr(9),combobox.text]),true);
	memo_drucken(richedit_befund);
end;

procedure anmerkung_drucken;
begin
	memo_drucken(richedit_befund);
end;

procedure rr_drucken;
begin
	com.texteinfuegen(format('%s RR: %d / %d mmHg Puls: %d', [chr(9),spinedit_sy.value, spinedit_di.value,spinedit_pu.value]),true);
	memo_drucken(richedit_befund);
end;

procedure bmi_drucken;
begin
	com.texteinfuegen(format('%s Gr�sse: %s cm  Gewicht: %s kg Bauchumfang: %s cm  WHtR: %s  BMI: %s kg/m�', [chr(9),edit_groesse.text, edit_gewicht.text,edit_bauchumfang.text, edit_whtr.text,edit_bmi.text]),true);
	memo_drucken(richedit_befund);
end;

procedure lufu_drucken;
begin
	com.texteinfuegen(format('%s FEV1: %s l/s  FVC: %s l  FEV1/FVC: %s %%', [chr(9),edit_fev1.text, edit_fvc.text,edit_fev_fvc.text]),true);
	memo_drucken(richedit_befund);
end;

procedure wai_drucken;
begin
	com.texteinfuegen(format('%s Work Ability Index %s', [chr(9),edit_wai.text]),true);
	//memo_drucken(richedit_befund);
end;

procedure befund_drucken;
var
falt:tcolor;

begin


	//com.schrift('', 12,true);
	com.texteinfuegen(format('%s  :  %s', [dbtreeview.Selected.Text,norm_auffaellig(radiogroup_auffaellig.itemindex) ]),true);
	//com.schrift('', 10,false);
	 case datamodul.q_bereich['modus'] of
		 0:anmerkung_drucken ;
		 1:wert_drucken;
		 2:liste_drucken;
		 3:audiogramm_drucken;
		 4:sehtest_drucken;
		 5:rr_drucken;
		 6:bmi_drucken;
		 7:lufu_drucken;
		 8:wai_drucken;
	 end;
end;


begin //main
speichern;
//pagecontrol_untersuchung.ActivePage:=TabSheet_unt_befunde;
with datamodul do
begin

	vorlage_laden(6,q_firma['nummer']);
	com.bookmarks_lesen;
	com.bookmarks_einfuegen_tabelle('befunde',getbigint(q_befunde,'nummer'),0);
	com.schrift('Courier New', 10,false);

   arbene_vor;
	//ForceForegroundWindow(application.Handle);


	knoten:=dbtreeview.Selected;

	parent:=knoten.Parent;

	while (parent<>nil)  do
	begin
		nodedata:=parent.Data;
		if nodedata^.ebene=1 then break;
		parent:=parent.Parent;
	end;
	//nodedata:=parent.Data;
	//bdatum:=nodedata^.datum;

	knoten:=parent.GetNext;

	while knoten<>nil do
	begin
		nodedata:=knoten.Data;
		if (nodedata^.rubrik<>1)  then break;

		if knoten.StateIndex<>-1 then
		begin
		  dbtreeview.Selected:=knoten;
		  if datensatz(datamodul.q_akt_untersuchung) then befund_drucken;
		end;
		knoten:=knoten.GetNext;
	end;

	com.doku_drucken;
end;
end;

procedure tform_main.anamnese_drucken(sender:tobject);
var
titel:string;
begin
	//memo_drucken(akt_editor);
	titel:='Anamnese von '+datamodul.q_mitarbeiter['name']+ ' '+datamodul.q_mitarbeiter['vorname'];
	akt_editor.lines.Insert(0,titel);
	akt_editor.lines.insert(1,'');
	akt_editor.Print(titel);
	akt_editor.lines.Delete(0);
	akt_editor.lines.Delete(0);

end;




procedure tform_main.labor_drucken_alle(sender:tobject);
var
//us:thtml_uebersicht;
f_name:string;
i_mitarbeiter:int64;
i:integer;
begin
	speichern;

	i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter,'nummer');
	vorlage_laden(5, datamodul.q_firma['nummer']);
	com.bookmarks_einfuegen_tabelle('mitarbeiter',i_mitarbeiter,0);
  oleanzeigen(arbeitsmedizin);
  {  arbeitsmedizin.UpdateVerbs;
 	Arbeitsmedizin.DoVerb(ovshow);}
end;



procedure tform_main.labor_drucken_speziell(sender:tobject);
begin
speichern;
with datamodul do
begin
	vorlage_laden(6, q_firma['nummer']);

	com.bookmarks_einfuegen_tabelle('mitarbeiter',getbigint(q_mitarbeiter,'nummer'),0);


	pagecontrol_labor.ActivePage:=tabsheet_lab_graph;
	application.ProcessMessages;
   arbene_vor;

	com.texteinfuegen('',true);
	//printcontrol.GetImage(xygraph).savetofile(pfad_temp+'4.bmp');
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.', [ pfad_temp]));
   xygraph.speichern(pfad_temp+'4.bmp');
   //bild_speichern(xygraph,pfad_temp+'4.bmp');
	com.graphikeinfuegen(pfad_temp+'4.bmp');
	com.doku_drucken;
end;
end;



procedure tform_main.unfallanzeige_drucken(sender:tobject);
begin
with datamodul do
begin
	speichern;
	dok_erstellen(Arbeitsmedizin,7,'ambulanz',datamodul.q_ambulanz,[''],['']);
	com.doku_drucken;
  olecontainer_destroy(Arbeitsmedizin);
	//com.doku_schliessen;
	//if word_close then com.word_schliessen;
end;
end;


procedure tform_main.unfallanzeige_drucken_seite(sender:tobject);
begin
with datamodul do
begin
	speichern;
	dok_erstellen(Arbeitsmedizin,7,'ambulanz',datamodul.q_ambulanz,[''],['']);
	com.doku_drucken_seite;
  olecontainer_destroy(Arbeitsmedizin);
	//com.doku_schliessen;
	//if word_close then com.word_schliessen;
end;
end;

procedure tform_main.ambulanz_drucken_vital(sender:tobject);
var
s1,s2,s3,s4,s5,s6,s7, dat:string;
i1,i2,i3,i4:integer;
begin
speichern;
with datamodul do
begin
	vorlage_laden(6, q_firma['nummer']);
	com.bookmarks_einfuegen_tabelle('ambulanz',getbigint(q_ambulanz,'nummer'),0);
  q_ambulanz.First;
  while not q_ambulanz.eof do
  begin
	  try
		 s2:='';
		 {if q_ambulanz['rhythmus']=1 then s2:='regelm��ig';
		 if q_ambulanz['rhythmus']=0 then s2:='unregelm��ig'; }
         s2:=datamodul.q_ambulanz.findfield('rhythmus').asstring;
		 dat:=datetostr(q_ambulanz['datum']) +' '+timetostr(q_ambulanz['urzeit']);
		 i1:=datamodul.q_ambulanz['rrs'];
		 i2:=datamodul.q_ambulanz['rrd'];
		 i3:=datamodul.q_ambulanz['freq'];
		 i4:=datamodul.q_ambulanz['bz'];
         s3:= datamodul.q_ambulanz.findfield('spo2').asstring;
         s4:= datamodul.q_ambulanz.findfield('gcs').asstring;
         s5:= datamodul.q_ambulanz.findfield('temp').asstring;
         s6:= datamodul.q_ambulanz.findfield('tropt').asstring;
         s7:= datamodul.q_ambulanz.findfield('ekg').asstring;

		 s1:=format(' %s | RR: %d / %d, Frequenz: %d %s, BZ: %d, SPO2: %s, GCS: %s, Temp.: %s�C, Trop-T: %s, EKG: %s ', [dat,i1,i2,i3,s2,i4,s3,s4,s5,s6,s7]);
		 com.texteinfuegen(s1,true);
	  except
	  end;
    q_ambulanz.Next;
	end;
  oleanzeigen(arbeitsmedizin);
  {arbeitsmedizin.UpdateVerbs;
	 Arbeitsmedizin.DoVerb(ovshow);}
	//  com.doku_drucken;
	//application.BringToFront;

end;
end;



procedure tform_main.vorlage_laden(i_filter, i_firma:integer); //bookmarks werden gelesen
var
query,s_nr,anzahl, anzahlf:string;
vorlage:integer;
fspez:boolean;
begin
with datamodul do
begin
  vorlage:=i_filter;
  
    //##
    //firmenspezifisch  Fallbestimmung  s. auch doc_erst
	query:=format('select count(*) from vorlagen where (i_filter=%d) and (i_firma= %d)', [vorlage,i_firma]);
   anzahlf:=mysql_d.Feldinhalt(query,0);
   if anzahlf='0' then
   begin
      fspez:=false;
   end;
   query:=format('select count(*) from vorlagen where (i_filter=%d)', [vorlage]);
   anzahl:=mysql_d.Feldinhalt(query,0);
   if  anzahl='0' then
   begin
      showmessage('Es gibt keine entsprechende Vorlage, bitte unter Einstellungen>Stammdaten>Dokumentenvorlagen anlegen');
      exit;
   end;
   if anzahl='1' then
   begin
   	if fspez then query:=format('select * from vorlagen where (i_filter=%d) and (i_firma= %d)', [vorlage,i_firma])
      else query:=format('select * from vorlagen where (i_filter=%d) ', [vorlage]);
   end
   else
   begin
		 s_nr:=vorlage_waehlen(vorlage,fspez);
       if s_nr='' then exit;
       query:=format('select * from vorlagen where nummer=%s', [s_nr]);
   end;


    //##
    sql_new(true,q_vorlagen,query,'');

	olecontainer_laden(q_vorlagen,'ole',Arbeitsmedizin);
  {try
    pause(10);
    arbeitsmedizin.UpdateVerbs;
	  Arbeitsmedizin.DoVerb(ovshow);
  except
    pause(200);
    arbeitsmedizin.UpdateVerbs;
    Arbeitsmedizin.DoVerb(ovshow);
  end; }
  oleanzeigen(arbeitsmedizin);

  if not com.wordobjekterstellen(Arbeitsmedizin) then exit;

  //application.BringToFront;
  //application.ProcessMessages;
  arbene_vor;
	com.bookmarks_lesen;
end;
end;


procedure TForm_main.PageControl_stammdatenChange(Sender: TObject);
var kurz:string;
begin
if pagecontrol_stammdaten.ActivePage=tabsheet_ma_firma then with datamodul do
begin
	//if q_mitarbeiter['abteilung']<>null then kurz:=q_mitarbeiter['abteilung'] else kurz:='';
	form_main.taetigkeitanzeigen;
  form_main.abteilunganzeigen;

end;
end;

procedure tform_main.arbeitsunfall_show;
var
	nummer:int64;
	query:string;
begin
with datamodul do
begin

if (not q_arbeitsunfall.active) then exit;

if (kein_datensatz(q_ambulanz)) and (not (q_ambulanz.state in [dsinsert]))  then
begin
	tabsheet_u_arbeit_I.tabvisible:=false;
	tabsheet_u_arbeit_II.tabvisible:=false;
	tabsheet_u_hergang.tabvisible:=false;
	exit;
end;




//if datamodul.q_ambulanz['b_arbeitsunfall']=null then exit;
nummer:= getbigint(q_ambulanz,'nummer');
query:= format('select * from arbeitsunfall where nummer=%s',[inttostr(nummer)]);
sql(false, q_arbeitsunfall,query,'');

//if dbcheckbox_aunfall.State =   cbunChecked	then
case  dbcheckbox_aunfall.State of
	cbunChecked:
			begin
			//if q_arbeitsunfall.Locate('nummer',inttostr(nummer),[]) then
      if q_arbeitsunfall.Locate('nummer',(nummer),[]) then
			dateiloeschen(q_arbeitsunfall, true,false); //q_arbeitsunfall.delete;
			tabsheet_u_arbeit_I.tabvisible:=false;
			tabsheet_u_arbeit_II.tabvisible:=false;
			tabsheet_u_hergang.tabvisible:=false;
			end;
	cbChecked :
		 begin
			//if not q_arbeitsunfall.Locate('nummer',inttostr(nummer),[]) then
      if not q_arbeitsunfall.Locate('nummer',(nummer),[]) then
			begin
              neuer_datensatz(q_arbeitsunfall,['nummer','u_datum','u_zeit','arb_ein_dat','arb_auf_dat']
              ,[inttostr(nummer),date,time,date,date]);
				{q_arbeitsunfall.append;
				q_arbeitsunfall.edit;
				setbigint(q_arbeitsunfall,'nummer',nummer);
				q_arbeitsunfall['u_datum']:=date;
				q_arbeitsunfall['u_zeit']:=time;
				q_arbeitsunfall['arb_ein_dat']:=date;
				q_arbeitsunfall['arb_auf_dat']:=date;
				//q_arbeitsunfall['d_arbentgelt']:=date+41;}
				if q_firma['i_wo_entgelt']<>null
					then
					q_arbeitsunfall['i_wo_entgelt']:=q_firma['i_wo_entgelt']
					else q_arbeitsunfall['i_wo_entgelt']:=6;
				q_arbeitsunfall['s_wer_schildert']:='des Versicherten';
				q_arbeitsunfall.Post;
			end
			else
				if q_arbeitsunfall['storno']= 1 then
					begin
						q_arbeitsunfall.edit;
						q_arbeitsunfall['storno']:=0;
					end;
			tabsheet_u_arbeit_I.TabVisible:=true;
			tabsheet_u_arbeit_II.TabVisible:=true;
			tabsheet_u_hergang.tabvisible:=true;
			arbeitsunfall_time_show;
		end;
end;
end;
end;

procedure TForm_main.DBCheckBox_aunfallClick(Sender: TObject);
begin
	if dbcheckbox_aunfall.ReadOnly then exit;
  if dbtreeview.a_rubrik<>5 then exit;
	if bool then
	begin
		bool:=false;
		if MessageDlg('Formular Arbeitsunfall l�schen', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then dbcheckbox_aunfall.State:= cbChecked ;
	end;

	arbeitsunfall_show;
  if dbcombo_num_weiter.getnummer=1 then showmessage('Sie haben die weitere Behandlung noch nicht ausgef�llt');

end;

procedure TForm_main.DBComboBox_u_einClick(Sender: TObject);
begin
	if dbcombobox_u_ein.Itemindex=1 then
	begin
		dbedit_u_ein_dat.Visible:=true;
		SpeedButton_a_ein.visible:=true;
		dbedit_time_arb_ende.Visible:=true;
		label_arb_ende.Visible:=true;
		datamodul.q_arbeitsunfall['arb_ein_dat']:=date();
	end
	else
	begin
		dbedit_u_ein_dat.Visible:=FALSE;
		SpeedButton_a_ein.visible:=false;
		dbedit_time_arb_ende.Visible:=false;
		label_arb_ende.Visible:=false;
		datamodul.q_arbeitsunfall.edit;
		datamodul.q_arbeitsunfall['arb_ein_dat']:=strtodate('01.01.0001');
	end;

end;

procedure TForm_main.DBComboBox_u_ausClick(Sender: TObject);
begin
 if dbcombobox_u_aus.Itemindex=1 then
	begin
		dbedit_u_auf_dat.Visible:=true;
		SpeedButton_a_auf.visible:=true;
		datamodul.q_arbeitsunfall['arb_auf_dat']:=date();
	end
	else
	begin
		dbedit_u_auf_dat.Visible:=FALSE;
		SpeedButton_a_auf.visible:=false;
		datamodul.q_arbeitsunfall.edit;
		datamodul.q_arbeitsunfall['arb_auf_dat']:=strtodate('01.01.0001');
	end;
end;

procedure tform_main.arbeitsunfall_time_show;   //zwei unterschiedliche Funktionen leider beide notwendig
begin

	if datamodul.q_arbeitsunfall['arb_eingestellt']=dbcombobox_u_ein.Items[1] then
	begin
		dbedit_u_ein_dat.Visible:=true;
		 SpeedButton_a_ein.visible:=true;
		 dbedit_time_arb_ende.Visible:=true;
		label_arb_ende.Visible:=true;
	end
	else
	begin
		dbedit_u_ein_dat.Visible:=FALSE;
		SpeedButton_a_ein.visible:=false;
		dbedit_time_arb_ende.Visible:=false;
		label_arb_ende.Visible:=false;
     datamodul.q_arbeitsunfall.edit;
     datamodul.q_arbeitsunfall['arb_ein_dat']:=strtodate('01.01.0001');
	end;
	if datamodul.q_arbeitsunfall['arb_aufgenommen']= dbcombobox_u_aus.Items[1] then
	begin
		dbedit_u_auf_dat.Visible:=true;
     SpeedButton_a_auf.visible:=true;
	end
	else
	begin
		dbedit_u_auf_dat.Visible:=FALSE;
     SpeedButton_a_auf.visible:=false;
     datamodul.q_arbeitsunfall.edit;
     datamodul.q_arbeitsunfall['arb_auf_dat']:=strtodate('01.01.0001');
	end;
end;


procedure TForm_main.pagecontrol_ambulanzChange(Sender: TObject);
begin
if pagecontrol_ambulanz.ActivePage=tabsheet_u_arbeit_I then arbeitsunfall_time_show;
end;

procedure TForm_main.DBCheckBox_aunfallEnter(Sender: TObject);
begin

	if dbcheckbox_aunfall.State =   cbChecked	then  bool:=true else bool:=false;
end;

procedure TForm_main.SpeedButton_u_erstClick(Sender: TObject);
begin
	 aerzteliste('a_erst');
end;

procedure tform_main.aerzteliste(feld:string);
var
text:string;
ind:int64;
k:pkontaktpointer;
begin
with datamodul do
begin
	// sql_new(true,q_1,'select * from aerzteliste','s_name');
	//form_liste:=tform_liste.create(self);
	//if form_liste.showmodal<>mrok then exit;
	ind:=0;
	k:=aerzteanzeigen(false,ind);
	q_ambulanz.edit;
	text:=k.titel+' '+k.name+ ' '+k.strasse;
  dispose(k);
	if text<>'' then   q_ambulanz[feld]:=text;
	end;
end;

procedure TForm_main.SpeedButton_u_waClick(Sender: TObject);
begin
		 aerzteliste('a_dz');
end;

procedure TForm_main.SpeedButton_u_daClick(Sender: TObject);
begin
	 aerzteliste('a_d');
end;

procedure TForm_main.SpeedButton_u_khClick(Sender: TObject);
begin
		 aerzteliste('a_kh');
end;

procedure TForm_main.BitBtn_u_lokalClick(Sender: TObject);
begin
ambulanz_memo_add(1,true);
end;

procedure tform_main.ambulanz_memo_add(bereich:integer;fz:boolean);
var zelle:string;
begin
try
   

   with datamodul do
	 begin
      sql_new(false,q_1,format('select * from ambulanz_insert where bereich=%d  and storno=0 ',[bereich]),'reihenfolge');

		 form_treewahl:=tform_treewahl.create(self);
      form_treewahl.HelpContext:=0;
		 form_treewahl.treeview.mysqlquery:=q_1;
		 form_treewahl.treeview.mysql_feld:='s_text';
		 form_treewahl.treeview.bereich_wert:=bereich;
		 form_treewahl.TreeView.bereich_feldname:='bereich';
		 form_treewahl.treeview.liste_lesen;
     form_treewahl.treeview.autoexpand :=false;
     form_treewahl.Panel_fz.Visible:=fz;
     form_treewahl.HelpContext:=34;
		 if form_treewahl.showmodal<>mrok then exit;

		 zelle:=grid_ambulanz.Cells[bereich-1,grid_ambulanz.row];

     if zelle='' then grid_ambulanz.Cells[bereich-1,grid_ambulanz.row]:=q_1['s_text'] else
     grid_ambulanz.Cells[bereich-1,grid_ambulanz.row]:=zelle+chr(13)+chr(10)+q_1['s_text'];

     grid_ambulanz.tag:=-1;//modified;
     datamodul.gridschreiben(form_main.grid_ambulanz,datamodul.q_ambulanz);
     grid_ambulanz.SetFocus;
   end;
finally
	form_treewahl.release;
end;
end;

function tform_main.schutzzmassnahmen(bereich:integer):string ;
begin
try
	 with datamodul do
	 begin
		  sql_new(true,q_1,format('select * from schutzmassnahmen where bereich=%d',[bereich]),'reihenfolge');
		 form_treewahl:=tform_treewahl.create(self);
		 if system_modus=0 then form_treewahl.TreeView.PopupMenu:=nil;
		 form_treewahl.treeview.mysqlquery:=q_1;
		 form_treewahl.treeview.mysql_feld:='s_text';
		 form_treewahl.treeview.bereich_wert:=bereich;
		 form_treewahl.TreeView.bereich_feldname:='bereich';
		 form_treewahl.treeview.liste_lesen;
		 if form_treewahl.showmodal=mrok then result:=q_1['s_text'] else result:='';
	 end;

finally
form_treewahl.release;
end;
end;


function tform_main.firma_ort(bereich:integer):string;
begin

end;



procedure TForm_main.BitBtn_u_artClick(Sender: TObject);
begin
ambulanz_memo_add(2,true);
end;

procedure TForm_main.BitBtn_u_versorgClick(Sender: TObject);
begin
ambulanz_memo_add(3,true);
end;

procedure TForm_main.ToolButton_schichtplanClick(Sender: TObject);
begin
if (datamodul.q_mitarbeiter['i_schichtmodell']=null) or (datamodul.q_mitarbeiter['schicht_s_dat']=null) then
begin
	showmessage('Bitte Schichtmodell eintragen');
  exit;
end;
datamodul.sql(false,datamodul.q_1,'select * from schicht','');

 if not datamodul.q_1.Locate('nummer',datamodul.q_mitarbeiter['i_schichtmodell'],[]) then
 begin
 	showmessage('Kein Schichtmodell zugeordnet');
  exit;
 end;
try
form_schicht:=tform_schicht.create(self);
form_schicht.ini(datamodul.q_mitarbeiter['name'],datamodul.q_mitarbeiter['vorname'],date,datamodul.q_mitarbeiter['schicht_s_dat'], datamodul.q_1['schicht']);
form_schicht.zeichne;

form_schicht.showmodal;
finally
form_schicht.Release;
end;


end;

procedure TForm_main.m_SchichtmodelleClick(Sender: TObject);
var
ber:integer;
begin
if nicht_satellit_tab then exit;
	ber:=b.b_string('Firmen-Listen');
	if ber=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.HelpContext:=15;
	form_tabellen_berechtigung(ber);

form_tabellen.suchfeld:='schicht_name';
//i_firma:=datamodul.q_firma['nummer'];
form_tabellen.Notebook.PageIndex:=5;
datamodul.sql(true,datamodul.q_1,'select * from schicht','schicht_name');// where i_firma=%d',[i_firma]),'');

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='schicht_name';
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='';
form_tabellen.DBGrid_tabellen.Columns[0].width:=200;
form_tabellen.DBEdit_s_name.DataField:='schicht_name';
form_tabellen.DBEdit_s_schicht.DataField:='schicht';
form_tabellen.DBEdit_s_kuerzel.DataField:='schicht_kuerzel';
	grid_einstellen(form_tabellen.dbgrid_tabellen);


form_tabellen.showmodal;
form_tabellen.release;
dbcombo_laden;
end;

procedure TForm_main.m_dbabgleichClick(Sender: TObject);
var
ende:boolean;
query:string;
begin
//if  b.b_string('Satelliten-Abgleichen')=-1 then exit;  sollte immer erlaubt sein, dann kann man sich nicht aussperren.
speichern;
pagecontrol_main.ActivePage:=tabsheet_firma;
datamodul.q_firma.first;
query:='select * from schluessel where storno=0 order by name';
datamodul.sql_new(false,datamodul.q_16,query,'');
form_abgleich:=tform_abgleich.create(self);

//form_abgleich.PageControl.ActivePage:=form_abgleich.TabSheet_export;
try
  ende:=false;
   form_abgleich.ShowModal;


   if form_main.tag=2 then   //erstes aktualisieren
   begin
         aktualisiere_nummern_kreis_aktuell; //vorsichtshalber falls doch ein bestehender NKR wiederverwendet wird.
         showmessage ('Import abgeschlossen, die Anwendung muss jetzt beendet werden.');
         ende:=true;
   end
   else
   begin
     try
      refresh_all;
      pagecontrol_main.ActivePage:=tabsheet_firma;
      berechtigung_firma;
      datamodul.q_firma.refresh;
      berechtigungen_einstellen;
      sichtbarkeiten_einstellen;
      dbcombo_laden;
      vert_syst_menue;
      if nummern_kreis_fehler then
      begin
        showmessage('Nummenrkreis wird angepasst');
        aktualisiere_nummern_kreis_aktuell; //vorsichtshalber
      end;
      //l�schen von allem �berfl�ssigen
      datamodul.sql_new(false,datamodul.q_2,'select * from haupt_tabelle','');
	   if datamodul.q_2['haupt']=0 then   //nur auf Satelitten!!!
        if form_abgleich.checkbox_loeschen.Checked then ueberfluessiges_loeschen;

    except
      //bei Abbruch abgleich 
    end;
    end;
finally
	form_abgleich.release;
  if ende then form_main.close;

end;
end;

procedure TForm_main.m_SchichtPlanClick(Sender: TObject);
begin
  ToolButton_schichtplanClick(Sender);
end;

procedure TForm_main.Berufsgenossenschaften1Click(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;

	ber:=b.b_string('Firmen-Listen');
	if ber=-1 then exit;
    speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.HelpContext:=9;
	form_tabellen_berechtigung(ber);

	form_tabellen.suchfeld:='bg';
	form_tabellen.Caption:='Berufsgenossenschaften';
	form_tabellen.Notebook.PageIndex:=7;
	form_tabellen.BitBtn_bwahl.visible:=false;
	form_tabellen.ToolButton_neu.enabled:=true;
	form_tabellen.Label_kundennummer.Visible:=false;
	form_tabellen.DBEdit_kundennummer.visible:=false;

	//form_tabellen.ToolButton_del.enabled:=false;
	datamodul.sql_new(true,datamodul.q_1,'select * from berufsgenossenschaft','bg_name');

	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='BG_name';

	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	form_tabellen.DBEdit_name.DataField:='bg_name';
	form_tabellen.DBEdit_strasse.DataField:='bg_strasse';
	form_tabellen.DBEdit_plz.DataField:='bg_plz';
	form_tabellen.DBEdit_ort.DataField:='bg_ort';
	form_tabellen.DBEdit_telefon.DataField:='bg_telefon';
	form_tabellen.DBEdit_fax.DataField:='bg_fax';
	form_tabellen.DBEdit_email.DataField:='bg_email';
	form_tabellen.DBEdit_www.DataField:='bg_www';
	form_tabellen.DBmemo_memo.DataField:='bg_memo';
	grid_einstellen(form_tabellen.dbgrid_tabellen);
    //datamodul.q_1.BeforeScroll:=form_tabellen.dbchange;
	form_tabellen.showmodal;
	form_tabellen.release;
    //datamodul.q_1.BeforeScroll:=nil;
	//datamodul.q_bg.refresh;
	dbcombo_laden;

end;

procedure TForm_main.Panel_ambulanzResize(Sender: TObject);
var
i:integer;
begin
	for i:=0 to grid_ambulanz.ColCount-1 do
	grid_ambulanz.ColWidths[i]:=grid_ambulanz.Width div grid_ambulanz.colCount-(grid_ambulanz.ColCount-1)*2; //wenn mehr zeilen dann 7
	for i:=0 to grid_ambulanz.RowCount-1 do
	grid_ambulanz.RowHeights[i]:=grid_ambulanz.Height div grid_ambulanz.RowCount-3;
	panel_wbl.Width:=groupbox_behandlung.Width div 2 -30;
	dbedit_u_aerst.Width:=panel_wbr.Width-15;
	dbedit_u_akh.Width:=panel_wbr.Width-15;
	dbcombo_num_weiter.Width:=panel_wbr.Width-25;
	dbedit_u_adz.Width:=panel_wbr.Width-15;
	dbedit_icd.width:=groupbox_icd_diagnose.Width-225;
end;

procedure TForm_main.grid_ambulanzAfterEdit(Sender: TObject; col,
  row: Integer);
begin
grid_ambulanz.tag:=-1;//modified;
end;

procedure TForm_main.grid_ambulanzExit(Sender: TObject);
begin
//datamodul.q_ambulanz.Edit;
//form_main.grid_ambulanz.HideEdit(false);
datamodul.gridschreiben(form_main.grid_ambulanz,datamodul.q_ambulanz);
end;

procedure TForm_main.email_emailClick(Sender: TObject);
begin
  form_smtp:=tform_smtp.create(self);
  //if datamodul.q_mitarbeiter['email_f']<>null then form_smtp.editto.text:=datamodul.q_mitarbeiter['email_f'];
	form_smtp.ShowModal;
	form_smtp.Release;
end;

procedure TForm_main.Panel_befund_sehtestResize(Sender: TObject);
var
i:integer;
begin
for i:=1 to grid_sehtest.ColCount-1 do
	grid_sehtest.ColWidths[i]:=((grid_sehtest.Width-grid_sehtest.ColWidths[0]) div (grid_sehtest.ColCount-1)) -2; //wenn mehr zeilen dann 7

for i:=0 to grid_sehtest.RowCount-1 do
	grid_sehtest.RowHeights[i]:=(grid_sehtest.Height div 4)-2;
end;

procedure TForm_main.Edit_s_horizontalChange(Sender: TObject);
begin
richedit_befund.tag:=10;
end;



procedure TForm_main.DBCheckBox_abMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

datamodul.q_ambulanz.edit;
if dbcheckbox_ab.Checked then
	datamodul.q_ambulanz['farbe']:=clwindow
else if dbcheckbox_intern.Checked then
			datamodul.q_ambulanz['farbe']:=clred
		 else datamodul.q_ambulanz['farbe']:=clwindow;	;

end;

procedure tform_main.prg_typ_einstellen;
var
s:string;
begin

case prg_typ of
1:
	begin
		  
       //tabsheet_anamnese.tabvisible:=false;

		  //m_verteiltesysteme.Visible:=false;
       //m_artikel1.visible:=false;
       //m_artikel.visible:=false;
		  //m_Schichtmodelle.visible:=false;
       //m_schichtplan.Visible:=false;
		  //toolbutton_schichtplan.Visible:=false;
       //DBEdit_schicht_s_datum.Visible:=false;
	   //Label_schicht_s_datum.Visible:=false;

       //m_bediener.visible:=false;

  end;
2:begin
      //tabsheet_anamnese.tabvisible:=false;

		  //m_verteiltesysteme.Visible:=false;
		  //m_artikel1.visible:=false;
		  //m_artikel.visible:=false;
		  //m_Berechtigungen_bearb.visible:=false;
		  //m_Schichtmodelle.visible:=false;
		  //m_schichtplan.Visible:=false;
		  //toolbutton_schichtplan.Visible:=false;
		  DBEdit_schicht_s_datum.Visible:=false;
          Label_schicht_s_datum.Visible:=false;

       //m_bediener.visible:=false;

  end;

3:
	begin
    //toolbuttonabteilung.Visible:=false;
	  //toolbutton_schichtplan.Visible:=false;
	  //toolbutton_firma.visible:=false;
	  //toolbuttonmaterial.Visible:=true;
	  //label_fname.caption:='';
    //label_ma.Caption:='Kunde:';
    tabsheet_firma.Caption:=' Kunden-Gruppe ';
     tabsheet_stammdaten.Caption:=' Kunden ';
     panel_firma_daten.visible:=false;
		TabSheet_FirmenAdresse.Caption:='';
     TabSheet_Ansprechpartner.tabvisible:=false;
     TabSheet_firma_texte.Tabvisible:=false;
     TabSheet_sollist.Tabvisible:=false;
     Tabsheet_Rechnungen.Tabvisible:=false;
     Tabsheet_firma_einstellungen_rechnung.Tabvisible:=false;
     tabsheet_ma_firma.TabVisible:=false;
     tabsheet_be_doku.pageIndex:=0;

		pagecontrol_befunde.ActivePage:=tabsheet_be_doku;
     label_gdb.Visible:=false;
     dbedit_gdb.Visible:=false;
     label_rv.Visible:=false;
     DBEdit_rvnummer.visible:=false;


     //radiobutton_arzt.Visible:=false;
     //radiobutton_sani.Visible:=false;
     groupbox_firma.Caption:='';
     edit_b_goae.Visible:=false;
     label_goae.Visible:=false;
     toolbuttonsuchen.caption:='Kd. suchen';
     toolbuttonsuchen.hint:='Kunde suchen';
     //GroupBox_re_faktor.Height:=0;
     DBgridEXT_re_positionen.Columns[3].width:=0;
     GroupBox_anmerkung_proband.Visible:=false;
		GroupBox_anmerkung_proband.height:=0;
     GroupBox_anmerkung_arbeitgeber.Caption:='Anmerkung';
     //Label_art.visible:=false;
     dbcombo_num_vor_art.visible:=false;
     dbradiogroup_beurteilung_eignung.visible:=false;
     untersuchungsdatum.visible:=false;
     dbedit_u_date.visible:=false;
     label_arzt.visible:=false;
		label_befunde_kapitel.visible:=false;
		dbcombo_num_befunde.Visible:=false;
		label_kapitel.visible:=false;
		dbcombo_num_dokumente.Visible:=false;

     dbcombo_num_vor_untersucher.visible:=false;
     //bitbtn_beurt_ueber.visible:=false;
     speedbutton_dat.visible:=false;
     bitbtn_b_zurueck.visible:=false;
		bitbtn_unters_planen.visible:=false;
		bitbtn_untwahl.Caption:='Behandlung w�hlen';

		m_firmenlisten.Visible:=false;
		m_goaeliste.Visible:=false;
		m_impfungen.Visible:=false;
		m_impfabfolge.Visible:=false;
		m_lab.Visible:=false;
		m_untersuchung.Caption:='Behandlung';
		//m_schichtplan.Visible:=false;
		m_Firmaauswhlen.Visible:=false;
		m_berechtigungen.Visible:=false;
		m_maArchivieren.Visible:=false;
		m_madearchivieren.visible:=false;
		m_maloeschen.Visible:=false;
		m_maverschieben.Caption:='Kunden verschieben';
		m_mazusammenfuehren.Caption:='Kunden zusammenf�hren';
		m_probandenbearb.Caption:='Kunden bearbeiten';
		m_LaborLDT.Visible:=false;
		m_gefaehrdungen.visible:=false;
		m_ambulanz_main.visible:=false;
		//m_druck_alleBesch.visible:=false;
	end;
4:
	begin

		  m_artikel1.visible:=false;
		  //m_artikel.visible:=false;
		  //m_Berechtigungen_bearb.visible:=false;
		  //m_Schichtmodelle.visible:=false;
		  //m_schichtplan.Visible:=false;
		  //m_untersuchung.Visible:=false;
		  //m_impfung.Visible:=false;
		  //m_impfabfolge.Visible:=false;
		  m_ambulanz_main.Visible:=false;
		  m_firmaauswhlen.visible:=false;
		  m_terminuebersicht.Visible:=false;
		  m_firmenlisten.Visible:=false;
		  m_taetigkeiten.visible:=false;
		  m_fristen.visible:=false;
		  m_impfungen.visible:=false;
		  m_impfabfolge.visible:=false;
		  m_ambulanz_main.visible:=false;
		  m_probandenbearb.caption:='Patienten bearbeiten';
		  m_maverschieben.caption:='Patienten verschieben';
		  m_mazusammenfuehren.caption:='Patienten zusammenf�hren';
		  m_maArchivieren.visible:=false;
		  m_maArchivieren.visible:=false;
		  m_maloeschen.visible:=false;
		  m_VerteilteSysteme.visible:=false;
		  m_madearchivieren.visible:=false;
		  m_NamenSuchen.visible:=false;
		  m_goaeliste.Visible:=false;
		  m_untersuchung.Caption:='Behandlung';
		  //m_schichtplan.Visible:=false;
			//m_firmenlisten.Visible:=false;

		//m_impfungen.Visible:=false;
		//m_impfabfolge.Visible:=false;
		//m_lab.Visible:=false;

		//m_Firmaauswhlen.Visible:=false;
		//m_berechtigungen.Visible:=false;
		//m_maArchivieren.Visible:=false;
		//m_madearchivieren.visible:=false;
		//m_maloeschen.Visible:=false;

		//m_LaborLDT.Visible:=false;
		//m_abteilungen.visible:=false;
		//m_gefaehrdungen.visible:=false;
		//m_ambulanz_main.visible:=false;
		//m_druck_alleBesch.visible:=false;


		  //toolbutton_schichtplan.Visible:=false;
		  DBEdit_schicht_s_datum.Visible:=false;
		  Label_schicht_s_datum.Visible:=false;
		  //#######
		  //ToolButton_material.Visible:=false;
		  toolbuttonauswertung.visible:=false;
		 // toolbuttonabteilung.Visible:=false;
		 //toolbutton_schichtplan.Visible:=false;
		 toolbuttontermine.Visible:=false;
		 //toolbutton_firma.visible:=false;
		 //toolbutton_material.Visible:=false;
		 //label_fname.caption:='';
		 //label_ma.Caption:='Patient:';
		 tabsheet_firma.Caption:=' Abteilungs-Gruppe ';
		tabsheet_stammdaten.Caption:=' Patienten ';
		panel_firma_daten.visible:=false;
		TabSheet_FirmenAdresse.Caption:=str_r_space(' ',1000);
		TabSheet_Ansprechpartner.tabvisible:=false;
     TabSheet_firma_texte.Tabvisible:=false;
     TabSheet_sollist.Tabvisible:=false;
     Tabsheet_Rechnungen.Tabvisible:=false;
     Tabsheet_firma_einstellungen_rechnung.Tabvisible:=false;
		tabsheet_ma_firma.TabVisible:=false;
		tabsheet_ma_person.caption:= str_r_space(' ',1000);
     //tabsheet_be_doku.pageIndex:=0;

		pagecontrol_befunde.ActivePage:=tabsheet_be_doku;
     SpeedButton_ha.visible:=false;
     dbedit_ha.Visible:=false;
     label_ha.Visible:=false;

     //radiobutton_arzt.Visible:=false;
     //radiobutton_sani.Visible:=false;
     groupbox_firma.Caption:='';
     edit_b_goae.Visible:=false;
     label_goae.Visible:=false;
     toolbuttonsuchen.caption:='Patient suchen';
     toolbuttonsuchen.hint:='Patient suchen';
     //GroupBox_re_faktor.Height:=0;
     DBgridEXT_re_positionen.Columns[3].width:=0;
     GroupBox_anmerkung_proband.Visible:=false;
     GroupBox_anmerkung_proband.height:=0;
     GroupBox_anmerkung_arbeitgeber.Caption:='Anmerkung';
     //Label_art.visible:=false;
     dbcombo_num_vor_art.visible:=false;
     dbradiogroup_beurteilung_eignung.visible:=false;
     untersuchungsdatum.visible:=false;
     dbedit_u_date.visible:=false;
     label_arzt.visible:=false;
     dbcombo_num_vor_untersucher.visible:=false;
     //bitbtn_beurt_ueber.visible:=false;
		speedbutton_dat.visible:=false;
		bitbtn_b_zurueck.visible:=false;
		bitbtn_unters_planen.visible:=false;
		bitbtn_untwahl.Caption:='Behandlung w�hlen';



  end;
end;
end;


procedure tform_main.sichtbarkeiten_einstellen;
begin
	groupbox_nurambulanz.Visible:=email_send;
	//dbcheckbox_artikel.Visible:=m_artikel.Visible;
	m_gefaehrdungen.visible:=show_gefaehrdungen;
	m_impfungen.Visible:=show_impfung;
	m_impfabfolge.Visible:=show_impfung;
	m_lab.Visible:=show_labor;
	m_ambulanz_main.Visible:=show_ambulanz;
	storno_show:=false;
	m_Berechtigungen_bearb.visible:=mehrplatz;
	m_verteiltesysteme.Visible:=verteiltesysteme;
end;


procedure TForm_main.berechtigungen_einstellen;
var
i:integer;
procedure menu_berechtigung(m:tmenuitem;ber:string);
begin
if b.b_string_silent(ber)=-1 then
	 begin
		m.Visible:=false;
		m.Checked:=false;
	 end;

if b.b_string_silent('proband-anamnese-arzt')=-1 then dbtreeview.kopfrubrik1_rubrik:=0;
end;

procedure parent_sichtbar(mi:tmenuitem );
begin
	if mi<>nil then
	begin
		mi.tag:=0;
		parent_sichtbar(mi.Parent);
	end;
end;

procedure kindunsichtbar(mi:tmenuitem );
var i:integer;
begin
 mi.visible:=false;
 mi.Checked:=false;
 for i:=0 to mi.Count-1 do
	 begin
		kindunsichtbar(mi[i]);
	 end;
end;

procedure kind1(mi:tmenuitem );
var i:integer;
begin
 mi.tag:=1;
 if not mi.visible then kindunsichtbar(mi);
 for i:=0 to mi.Count-1 do
	 begin
		kind1(mi[i]);
	 end;
end;


procedure parent0(mi:tmenuitem );
var i:integer;
begin
 if (mi.visible) and (mi.Count=0) then parent_sichtbar(mi);
 for i:=0 to mi.Count-1 do
	 begin
		parent0(mi[i]);
	 end;
end;


procedure sichtbar(mi:tmenuitem );
var i:integer;
begin
 if mi.tag=1 then
 begin
	mi.Visible:=false;
	mi.Checked:=false;
 end;
 for i:=0 to mi.Count-1 do
	 begin
		sichtbar(mi[i]);
	 end;
end;


begin //main       #####20131014
	 {if b.b_string_silent('proband-befunde')>-1 then
	 begin
		m_befunde.visible:=true;
	 end
	 else m_befunde.visible:=false;

	 if b.b_string_silent('Proband-Bescheinigung')>-1 then
	 begin
		m_vorsorge.visible:=true;
		//m_druck_allebesch.Visible:=true;
	 end
	 else m_vorsorge.visible:=false;
	 if (b.b_string_silent('proband-impfung')>-1) and show_impfung then
	  begin
		m_impfung.visible:=true;
	 end
	 else m_impfung.visible:=false;

	 if (b.b_string_silent('proband-labor')>-1) and show_labor then
	 m_labor.visible:=true
	 else m_labor.visible:=false;

	 if ((b.b_string_silent('proband-ambulanz')>-1) or (b.b_string_silent('proband-arbeitsunfall')>-1)) and show_ambulanz then
		m_ambulanz.visible:=true
	 else m_ambulanz.visible:=false;

	 if b.b_string_silent('proband-diagnosen')>-1 then
	 begin
		m_diagnosen.visible:=true;
	 end
	 else m_diagnosen.Visible:=false;
	 if b.b_string_silent('proband-besonderheiten')>-1 then
	 begin
	 m_besonderheiten.visible:=true;
	 end
	 else m_besonderheiten.Visible:=false;
	 if (b.b_string_silent('proband-dokumente-arzt')>-1) or ((b.b_string_silent('proband-dokumente-sani')>-1)) then
	 begin
	 m_dokumente.visible:=true;
	 end
	 else m_dokumente.visible:=false;

	 if b.b_string_silent('proband-rechnung')>-1 then  m_rechnungen.visible:=true
	 else m_rechnungen.Visible:=false;

	 if b.b_string_silent('proband-artikel')>-1 then  m_artikel.visible:=true
	 else m_artikel.Visible:=false;}

	 if b.b_string_silent('proband-anamnese-arzt')=-1 then
		begin
			//radiobutton_sani.Checked:=true;
			//radiobutton_arzt.Enabled:=false;
			//radiobutton_sani.Enabled:=false;
		end;
	 if (b.b_string_silent('proband-ambulanz')=-1) and (b.b_string_silent('proband-arbeitsunfall')>-1) then
			groupbox_nurambulanz.Visible:=false;
	 if (b.b_string_silent('proband-ambulanz')>-1) and (b.b_string_silent('proband-arbeitsunfall')=-1) then
			groupbox_nurarbeitssicherheit.Visible:=false;

   if b.b_string_silent('firma-dokumente')=-1 then tabsheet_firma_texte.TabVisible:=false;

	 if b.b_string_silent('firma-kriterien')=-1 then TabSheet_firma_filter.TabVisible:=false;
    if b.b_string_silent('firma-ansprechpartner')=-1 then TabSheet_Ansprechpartner.TabVisible:=false;

	 if b.b_string_silent('firma-dokumente')=-1 then tabsheet_firma_texte.TabVisible:=false;

	 if b.b_string_silent('firma-zeitabrechnung')=-1 then
     begin
         tabsheet_sollist.TabVisible:=false;
         Tabsheet_firma_einstellungen_zeitabrechnung.Tabvisible:=false;
         TabSheet_sollist.Tabvisible:=false;
     end;

     if b.b_string_silent('firma-rechnung')=-1 then
     begin
           tabsheet_rechnungen.TabVisible:=false;
           Tabsheet_firma_einstellungen_Rechnung.Tabvisible:=false;
     end;

   //if (not tabsheet_sollist.TabVisible) and (not tabsheet_rechnungen.TabVisible) then tabsheet_firma_einstellungen;
   tabsheet_firma_einstellungen.TabVisible:=(tabsheet_sollist.TabVisible or tabsheet_rechnungen.TabVisible);

     if b.b_string_silent('anwendung')=-1 then  //gibs noch nicth
     begin
           Tabsheet_firma_einstellungen.Tabvisible:=false;
     end;

   tabsheet_main.TabVisible:=true;
   if ((b.b_string_silent('proband-befunde')=-1) and
      (b.b_string_silent('Proband-Bescheinigung')=-1) and
       (b.b_string_silent('proband-bescheinigung-ag')=-1) and
       (b.b_string_silent('proband-impfung')=-1) and
      (b.b_string_silent('proband-labor')=-1) and
      (b.b_string_silent('proband-ambulanz')=-1) and
       (b.b_string_silent('proband-arbeitsunfall')=-1) and
       (b.b_string_silent('proband-diagnosen')=-1) and
       (b.b_string_silent('proband-besonderheiten')=-1) and
       (b.b_string_silent('proband-dokumente-arzt')=-1) and
       (b.b_string_silent('proband-dokumente-sani')=-1) and
       (b.b_string_silent('proband-rechnung')=-1) and
       (b.b_string_silent('proband-artikel')=-1) ) then tabsheet_main.TabVisible:=false;

	 menu_berechtigung( m_firmenlisten,'Firmen-Listen');
	 //menu_berechtigung( m_mitarbeiterlisten,'proband-Listen');
	 menu_berechtigung( m_untersuchung,'untersuchungen-listen');
	 //menu_berechtigung( m_BilderListe ,'anwendung');
	 menu_berechtigung( m_kbesonderheiten,'besonderheiten');
	 menu_berechtigung( m_dokumentenvorlagen ,'Dokumenten-vorlagen');
	 menu_berechtigung( m_maverschieben ,'proband-Verschieben');
	 menu_berechtigung( m_mazusammenfuehren ,'proband-Verschieben');
	 menu_berechtigung( m_madearchivieren,'anwendung');
	 menu_berechtigung( m_maArchivieren ,'anwendung');
	 menu_berechtigung( m_maloeschen ,'anwendung');
	 menu_berechtigung( m_importieren,'importieren');
	 menu_berechtigung( m_dbabgleich,'Satelliten-Abgleichen');
	 menu_berechtigung( m_berechtigungen_bearb,'Berechtigungen');
	 menu_berechtigung( m_bediener,'Berechtigungen');
	 menu_berechtigung( m_satellitini,'system');
	 menu_berechtigung( m_satellitschluessel,'system');
	 menu_berechtigung( m_systemtyp,'system');
	 //menu_berechtigung( m_terminart,'anwendung');
	// menu_berechtigung( m_artikel,'proband-artikel');

	 if system_modus=0 then m_dbabgleich.Visible:=true;// Satelliten-System hat keine eigene Berechtigungs-Verwaltung

	 for i:=0 to mainmenu.Items.Count-1 do
	 begin
		kind1(mainmenu.items[i]);
		//mainmenu.items[i].tag:=1 ;
	 end;

	 for i:=0 to mainmenu.Items.Count-1 do
	 begin
		parent0(mainmenu.items[i]);
		//mainmenu.items[i].tag:=1 ;
	 end;

	 for i:=0 to mainmenu.Items.Count-1 do
	 begin
		sichtbar(mainmenu.items[i]);
		//mainmenu.items[i].tag:=1 ;
	 end;

end;


procedure TForm_main.SpeedButton12Click(Sender: TObject);
begin
form_icd:=tform_icd.Create(self);
if form_icd.showmodal=mrok then
begin
	datamodul.q_ambulanz.edit;
	//datamodul.q_ambulanz['i_icd']:=datamodul.q_icd_main['nummer'];
	datamodul.q_ambulanz['icd']:=datamodul.q_icd_main['icd_schluessel'];
	datamodul.q_ambulanz['icd_text']:=datamodul.q_icd_main['icd_text'];

end;
//datamodul.sql(true,datamodul.q_icd_main,'select * from icd_main','');
timer_icd.Enabled:=true;
form_icd.Release ;
end;

procedure TForm_main.Timer_icdTimer(Sender: TObject);
begin
timer_icd.enabled:=false;
datamodul.sql(true,datamodul.q_icd_main,'select * from icd_main','');
end;

procedure TForm_main.Edit_bef_check(Sender: TObject);
begin
if tedit(sender).text='' then
   begin
        tedit(sender).text:='0';// exit;
        tedit(sender).selectall;
    end;
	radiogroup_auffaellig.tag:=1;
	try
	  strtofloat(tedit(sender).text);
	except
	  tedit(sender).text:='0';
	end;
end;

procedure TForm_main.Edit_befChange(Sender: TObject);
begin
with datamodul do
begin
  edit_bef_check(sender);
  try
    if q_bereich['s_rel_auff']='>' then
      if strtofloat(tedit(sender).text)>q_bereich['r_auff'] then radiogroup_auffaellig.ItemIndex:=1 else radiogroup_auffaellig.ItemIndex:=0
    else
      if strtofloat(tedit(sender).text)<q_bereich['r_auff'] then radiogroup_auffaellig.ItemIndex:=1 else radiogroup_auffaellig.ItemIndex:=0;
  except
  end;
end;
end;

procedure TForm_main.Edit_befKeyDown(Sender: TObject; var Key: Word;
	Shift: TShiftState);

begin
case key of
		48..57 : ;
		190 : key:=ord(decimalseparator);//.
  	188 :key:=ord(decimalseparator);//,
else
	key:=0;
end;

end;

procedure TForm_main.SpinEdit_sChange(Sender: TObject);
begin
spinedit_sy.tag:=1;
end;


procedure TForm_main.tab_reihenfolge(tabelle:tdataset;richtung:integer);
var
reihenfolge1,reihenfolge2:integer;
nummer1:integer;
begin
with datamodul do
begin
	if tabelle.State=dsedit then
			begin
			tabelle.Post;
			//exit;
		  end;
	if (richtung=0) and (tabelle.Bof) then exit;
	if (richtung=1) and (tabelle.eof) then exit;
	nummer1:=tabelle['nummer'];
	reihenfolge1:=tabelle['reihenfolge'];
	if (richtung=0) then tabelle.Prior else tabelle.Next;
	//position2:=tabelle.GetBookmark;
	reihenfolge2:=tabelle['reihenfolge'];
	tabelle.edit;
	tabelle['reihenfolge']:=reihenfolge1;
	tabelle.post;
	//tabelle.first;
	tabelle.locate('nummer',nummer1,[]);
	tabelle.edit;
	tabelle['reihenfolge']:=reihenfolge2;
	tabelle.post;
	tabelle.refresh;
	tabelle.locate('nummer',nummer1,[]);
end;
end;

procedure TForm_main.SerienbriefFelder1Click(Sender: TObject);
begin
if nicht_satellit_tab then exit;
if  b.b_string('Dokumenten-Vorlagen')<2 then exit;
speichern;
form_serienfelder:=tform_serienfelder.create(self);
form_serienfelder.HelpContext:=10850;

form_serienfelder.showmodal;
form_serienfelder.release;
end;

procedure TForm_main.Gewerbeaufsichtsmter1Click(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
	ber:=b.b_string('Firmen-Listen');
	if ber=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.HelpContext:=11;
	form_tabellen_berechtigung(ber);
	//form_tabellen.suchfeld:='bg';
	form_tabellen.Caption:='Gewerbeaufsichts�mter';
	form_tabellen.Notebook.PageIndex:=7;
	form_tabellen.BitBtn_bwahl.visible:=false;
	form_tabellen.ToolButton_neu.enabled:=true;
		form_tabellen.Label_kundennummer.Visible:=false;
	form_tabellen.DBEdit_kundennummer.visible:=false;
	//form_tabellen.ToolButton_del.enabled:=false;
	datamodul.sql(true,datamodul.q_1,'select * from gewerbeaufsicht','ge_name');
	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Ge_name';
	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	form_tabellen.DBEdit_name.DataField:='ge_name';
	form_tabellen.DBEdit_strasse.DataField:='ge_strasse';
	form_tabellen.DBEdit_plz.DataField:='ge_plz';
	form_tabellen.DBEdit_ort.DataField:='ge_ort';
	form_tabellen.DBEdit_telefon.DataField:='ge_telefon';
	form_tabellen.DBEdit_fax.DataField:='ge_fax';
	form_tabellen.DBEdit_email.DataField:='ge_email';
	form_tabellen.DBEdit_www.DataField:='ge_www';
	form_tabellen.DBmemo_memo.DataField:='ge_memo';
	grid_einstellen(form_tabellen.dbgrid_tabellen);
    //datamodul.q_1.BeforeScroll:=form_tabellen.dbchange;
	form_tabellen.showmodal;
	form_tabellen.release;
    //datamodul.q_1.BeforeScroll:=nil;
	dbcombo_laden;
end;


function email_von_ma(zeilen:tstrings;nummer:int64):boolean;
var
	s_vorges,query,adressen:string;
	felder:array[0..2] of string;
begin
query:='select email_f,abteilung, i_firma  from mitarbeiter where nummer='+inttostr(nummer);
mysql_d.Feldarray(query,[0,1,2],felder);
adressen:=felder[0];

string_to_liste(zeilen,adressen,',;');
if (adressen='') and (felder[2]<>'') then  //wenn ma hat keien Adresse dann Abteilungsadresse
	 begin
		 query:=format('select i_mitarbeiter from abteilung where kuerzel="%s" and i_firma=%s',[felder[1], felder[2]]);
		 s_vorges:=mysql_d.feldinhalt(query,0);
      if s_vorges<>'' then
      begin
         query:='select email_f from mitarbeiter where nummer='+s_vorges;
         adressen:=mysql_d.feldinhalt(query,0);
         string_to_liste(zeilen,adressen,',;');
      end;

	 end;

end;


function email_von_ma_abt(zeilen:tstrings;nummer:int64):boolean;
var
	s_vorges,query,adressen:string;
	felder:array[0..2] of string;
begin
query:='select email_f,i_abteilung, i_firma  from mitarbeiter where nummer='+inttostr(nummer);
mysql_d.Feldarray(query,[0,1,2],felder);
if felder[2]<>'' then
	 begin
		 query:=format('select i_mitarbeiter from abteilung where nummer="%s" and i_firma=%s',[felder[1], felder[2]]);
		 s_vorges:=mysql_d.feldinhalt(query,0);
		 if s_vorges<>'' then
		 begin
			 query:='select email_f from mitarbeiter where nummer='+s_vorges;
			 adressen:=mysql_d.feldinhalt(query,0);

       string_to_liste(zeilen,adressen,',;');
		 end;

	 end;
end;


procedure email_von_ansprechpartner(zeilen:tstrings;i_firma:integer;funktion:integer);
var
	query,em:string;
begin
with datamodul do
begin
		 query:=format('select email from firma_ansprech where i_firma=%d and i_email_funktion = "%d" ',[i_firma, funktion]);
		 sql(true,datamodul.q_4,query,'');
		 q_4.First;
		 while not q_4.Eof do
		 begin
			em:='';
			if q_4['email']<>null then em:=trim(q_4['email']);
			if em<>'' then string_to_liste(zeilen,em,',;');// zeilen.Add(em);
			q_4.Next;
		 end;
end;
end;

procedure string_to_liste(zeilen:tstrings;str, tz: string);
var
p,n,i:integer;
sstr,d: string;
begin
  n:=length(tz);
  str:=trim(str);
  while str<>'' do
  begin
    sstr:=str;
    for i:=1 to n do
    begin
      d:=copy(tz,n,1);
      p:=pos(d,str);
      if p>0 then
      begin
        sstr:=trim(copy(str,1,p-1));
        str:=copy(str,p+1,length(str));
        zeilen.Add(sstr);
      end;
    end;

    if sstr=str then
    begin
      sstr:=trim(sstr);
      zeilen.Add(sstr);
      str:='';
    end;
end;

end;

procedure TForm_main.ToolButton_abteilungClick(Sender: TObject);


begin
     form_timer.art('stopp');
     form_timer.showmodal;

end;


function tform_main.abt_manager(i_firma:integer;i_ma:int64;modus:string;zquery:tzquery; i_nummer:int64):twoint64;
var nr,nr_ap:int64;
begin
//speichern; wenn offen dann wird danach gespeichert sonst egal
if modus<>'wahl' then speichern; //vorsicht ist die mutter....

try
  {if i_ma=0 then                                            //wird nicht benutzt
     nr:=getbigint(datamodul.q_mitarbeiter,'i_abteilung')
  else nr:=i_ma;}
  nr:=getbigint(datamodul.q_mitarbeiter,'i_abteilung');
  nr_ap:=getbigint(datamodul.q_mitarbeiter,'i_arbeitsplatz');

  datamodul.q_2.Close;
  datamodul.q_ap3.Close;
  datamodul.q_ap4.Close;
  datamodul.q_ap5.Close;
  datamodul.q_ap6.Close;
  datamodul.q_ap11.close;
  datamodul.q_ap12.close;
  datamodul.q_ap13.close;
  //query:=format('select * from gefaehrdung_3 where nummer=%s',[s_g3]);
  //sql_new(false,q_12,query,'');
  form_ap:=tform_ap.create(self);
  form_ap.modus:=modus;
  //form_ap.MainMenu.

  if modus='ma'then
  begin
  	if  nr=0 then
      begin
           showmessage('Dem Probanden ist keine Abteilung zugeordnet. Bitte w�hlen Sie eine aus');
           pagecontrol_main.activepage:=tabsheet_stammdaten;
           pagecontrol_stammdaten.activepage:=tabsheet_ma_firma;
           BitBtn_abteilungClick(self);
           exit;
      end;
 	 form_ap.ini(i_firma,1,nr);
  	if nr>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(1,nr);
  	if nr_ap>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(2,nr_ap);
  	form_ap.toolbutton_abort.enabled:=false;
  	//form_ap.toolbutton_new.enabled:=false;
  	//form_ap.m_datei.visible:=false;

 	 form_ap.m_tools.visible:=false;
  	form_ap.m_report.visible:=false;
  end;

  if modus='all' then
  begin
  form_ap.ini(i_firma,0,0);
  form_ap.toolbutton_abort.enabled:=false;
  end;

  if modus='wahl' then
  begin
  form_ap.ini(i_firma,0,nr);
  form_ap.toolbutton_abort.enabled:=true;
  form_ap.toolbutton_ok.Caption:='Ausw�hlen';
  if nr>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(1,nr);
  if nr_ap>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(2,nr_ap);
  end;

  if form_ap.dbtreeview_k1.selected<>nil then form_ap.dbtreeview_k1.selected.Expand(true);

  if (zquery<>nil) and (i_nummer>0) then
  begin
   form_ap.dbtreeview_k1.expand_e1;
      //  function gehe_zu_query(zquery:tzquery;nummer:int64): boolean;

   form_ap.dbtreeview_k1.gehe_zu_datensatz_zquery(zquery, i_nummer);
  end;
  if form_ap.showmodal=mrok then
  		begin
  	 		    result[0]:=getbigint(datamodul.q_ap3,'nummer') ;
            if form_ap.dbtreeview_k1.Items.Count>0 then
            begin
        		if form_ap.dbtreeview_k1.Selected.StateIndex >=2 then result[1]:=getbigint(datamodul.q_ap4,'nummer') else result[1]:= 0;
            end;
        end
        else
        begin
        	result[0]:=0;
            result[1]:=0;
        end;
finally
  //datamodul.q_abteilung.refresh;
  form_ap.release;
  form_ap:=nil;
  datamodul.q_2.Close;
  datamodul.q_ap3.Close;
  datamodul.q_ap4.Close;
  datamodul.q_ap5.Close;
  datamodul.q_ap6.Close;
end;
end;


function tform_main.abt_manager1(i_firma:integer;i_ma:int64;modus:string;zquery:tzquery; i_nummer:int64):twoint64;
var nr,nr_ap:int64;
begin
//speichern; wenn offen dann wird danach gespeichert sonst egal
if modus<>'wahl' then speichern; //vorsicht ist die mutter....

try
  {if i_ma=0 then                                            //wird nicht benutzt
     nr:=getbigint(datamodul.q_mitarbeiter,'i_abteilung')
  else nr:=i_ma;}
  nr:=getbigint(datamodul.q_mitarbeiter,'i_abteilung');
  nr_ap:=getbigint(datamodul.q_mitarbeiter,'i_arbeitsplatz');

  datamodul.q_2.Close;
  datamodul.q_ap3.Close;
  datamodul.q_ap4.Close;
  datamodul.q_ap5.Close;
  datamodul.q_ap6.Close;
  datamodul.q_ap11.close;
  datamodul.q_ap12.close;
  datamodul.q_ap13.close;
  //query:=format('select * from gefaehrdung_3 where nummer=%s',[s_g3]);
  //sql_new(false,q_12,query,'');
  form_ap:=tform_ap.create(self);
  form_ap.modus:=modus;
  form_ap.dbtreeview_k1.a_all:=true;
  form_ap.ToolBar.Visible:=false;
  if modus='ma'then
  begin
  	if  nr=0 then
      begin
           showmessage('Dem Probanden ist keine Abteilung zugeordnet. Bitte w�hlen Sie eine aus');
           pagecontrol_main.activepage:=tabsheet_stammdaten;
           pagecontrol_stammdaten.activepage:=tabsheet_ma_firma;
           BitBtn_abteilungClick(self);
           exit;
      end;
 	 form_ap.ini(i_firma,1,nr);
  	if nr>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(1,nr);
  	if nr_ap>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(2,nr_ap);
  	form_ap.toolbutton_abort.enabled:=false;
  	//form_ap.toolbutton_new.enabled:=false;
  	//form_ap.m_datei.visible:=false;

 	 form_ap.m_tools.visible:=false;
  	form_ap.m_report.visible:=false;
  end;

  if modus='all' then
  begin
  form_ap.ini(i_firma,0,0);
  form_ap.toolbutton_abort.enabled:=false;
  end;

  if modus='wahl' then
  begin
  form_ap.ini(i_firma,0,nr);
  form_ap.toolbutton_abort.enabled:=true;
  form_ap.toolbutton_ok.Caption:='Ausw�hlen';
  if nr>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(1,nr);
  if nr_ap>0 then form_ap.dbtreeview_k1.gehe_zu_datensatz(2,nr_ap);
  end;

  if form_ap.dbtreeview_k1.selected<>nil then form_ap.dbtreeview_k1.selected.Expand(true);

  if (zquery<>nil) and (i_nummer>0) then
  begin
   form_ap.dbtreeview_k1.expand_e1;
      //  function gehe_zu_query(zquery:tzquery;nummer:int64): boolean;

   form_ap.dbtreeview_k1.gehe_zu_datensatz_zquery(zquery, i_nummer);
  end;

    form_ap.BorderStyle := bsNone;
    form_ap.Parent := TabSheet_abteilung;
    form_ap.Align := alClient;
    form_ap.Show;


finally

end;
end;

procedure TForm_main.m_taetigkeitenClick(Sender: TObject);
var i_branche,ber:integer;
begin
//if nicht_satellit_tab then exit;
	ber:=b.b_string('proband-Listen');
	if ber=-1 then exit;
  speichern;
	i_branche:=datamodul.q_firma.findfield('i_branche').asinteger;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.HelpContext:=15;
	form_tabellen.Caption:='T�tigkeiten';


	form_tabellen_berechtigung(ber);
  ///dieser code an zwei stellen !!!!!!!!!!!!!!!!!!!!!!!!!
	form_tabellen.suchfeld:='taetigkeit';
	form_tabellen.Notebook.PageIndex:=9;
	datamodul.sql(true,datamodul.q_1,'select * from taetigkeiten  ','taetigkeit');
  form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Taetigkeit';
	//form_tabellen.DBGrid_tabellen.Columns[0].width:=200;
	form_tabellen.DBEdit_taetigkeit.DataField:='taetigkeit';
	form_tabellen.DBEdit_einsatzzeit.DataField:='einsatzzeit';
  form_tabellen.DBCombo_taet_branche.DataField:='i_branche';
  form_tabellen.DBCombo_taet_branche.lookup_laden;
	grid_einstellen(form_tabellen.dbgrid_tabellen);

	form_tabellen.showmodal;
	form_tabellen.release;
	datamodul.q_taetigkeiten.refresh;
end;

procedure TForm_main.DBLookupComboBox_taeitigkeitEnter(Sender: TObject);
begin
datamodul.q_taetigkeiten.filtered:=false;
datamodul.q_taetigkeiten.filter:='i_firma=' +inttostr(datamodul.q_firma['nummer']);
datamodul.q_taetigkeiten.filtered:=true;

end;

procedure TForm_main.DBLookupComboBox_taeitigkeitExit(Sender: TObject);
begin
datamodul.q_taetigkeiten.filtered:=false;
end;

procedure TForm_main.VerteilteSysteme1Click(Sender: TObject);

begin
try
	 datamodul.sql(true,datamodul.q_1,'select * from satelliten','');
	 datamodul.sql(false,datamodul.q_2,'select * from haupt_tabelle','');
	 datamodul.sql(true,datamodul.q_3,'select * from nummern_kreis','');

	 form_vert_syst:=tform_vert_syst.create(self);
	 form_vert_syst.ShowModal;

finally
	 form_vert_syst.Release;
end;
end;

procedure TForm_main.m_SystemTypClick(Sender: TObject);
var
ende:boolean;
begin
if  b.b_string('Berechtigungen')=-1 then exit;
  speichern;

	 datamodul.sql(true,datamodul.q_1,'select * from satelliten','');
	 datamodul.sql(false,datamodul.q_2,'select * from haupt_tabelle','');
	 datamodul.sql(true,datamodul.q_3,'select * from nummern_kreis','');
   try
   ende:=false;
	 form_vert_syst:=tform_vert_syst.create(self);
	 form_vert_syst.Notebook.PageIndex:=2;
	 if form_vert_syst.ShowModal=mrok then
    begin

       //form_vert_syst.Release;
       showmessage('Die Anwendung muss jetzt beendet werden');
       ende:=true;

    end;
  finally
    form_vert_syst.Release;
    if ende then
    begin
      form_main.tag:=2;
       form_main.close;
    end;
  end;

end;

procedure TForm_main.m_SatellitschluesselClick(
	Sender: TObject);
begin
if b.b_string('system')=-1 then exit;
  speichern;
try
	 datamodul. sql_new(true,datamodul.q_1,'select * from satelliten','');
	 datamodul. sql_new(false,datamodul.q_2,'select * from haupt_tabelle','');
	 datamodul. sql_new(true,datamodul.q_3,'select * from nummern_kreis','');
	 if kein_datensatz(datamodul.q_1) then  showmessage('Achtung, mit dem Erzeugen eines Schl�ssels wird das System unwiderruflich zum Haupt-System');
	 form_vert_syst:=tform_vert_syst.create(self);
	 form_vert_syst.Notebook.PageIndex:=0;
	 form_vert_syst.ShowModal;

finally
	 form_vert_syst.Release;
	 vert_syst_menue;
end;
end;

procedure TForm_main.m_SatellitiniClick(Sender: TObject);
begin
if b.b_string('system')=-1 then exit;
  speichern;
try
	 datamodul.sql(true,datamodul.q_1,'select * from satelliten','');
	 datamodul.sql(false,datamodul.q_2,'select * from haupt_tabelle','');
	 datamodul.sql(true,datamodul.q_3,'select * from nummern_kreis','');

	 if datamodul.q_2['haupt']=0 then showmessage('Achtung, das System ist bereits als Satelliten-System initialisiert.');

	 form_vert_syst:=tform_vert_syst.create(self);
	 form_vert_syst.Notebook.PageIndex:=1;
	 if form_vert_syst.ShowModal=mrok then
	 begin
		{showmessage('Satelliten-System initialisiert, die Anwendung muss jetzt beendet werden');
		form_main.tag:=2;
		form_main.close;}
	 end;


finally
	 form_vert_syst.Release;
	 showmessage('Das Satelliten-System wurde initialisiert, die Anwendung muss jetzt beendet werden');
	 form_main.tag:=2;
	 form_main.close;
	 //vert_syst_menue;
end;
end;

procedure tform_main.vert_syst_menue;
var
nkr:integer;
begin
	 datamodul.sql(true,datamodul.q_1,'select * from satelliten','');
	 datamodul.sql(false,datamodul.q_2,'select * from haupt_tabelle','');
   nkr:=datamodul.q_2.findfield('nummern_kreis').AsInteger;
	 if datamodul.q_2['haupt']=0 then
	 begin   //satellit
		m_systemtyp.Visible:=false;
		m_satellitschluessel.Visible:=false;
		//m_Satellitini.Visible:=true;       ziemlich �berfl�ssig
		system_modus:=0;
		StatusBar.Panels[0].Text:='Satelliten-System - '+inttostr(nkr);
		//tabsheet_rechnungen.TabVisible:=false;       //rechnungen auf dem sat sollten kein Problem sein, da rechnung und re_positionen abgeglichen wird  20120125
		//Tabsheet_Rechnung_einstellungen.Tabvisible:=false;   //rechnungserstellung muss geblockt werden
		//m_rechnungen.Visible:=false;
		//m_rechnungen_c:=false;
	 end
	 else
	 begin    //haupt-system
		system_modus:=1;
		if datensatz(datamodul.q_1) then //auf jeden fall haupt-system da satelliten bestehen
		 begin
			m_systemtyp.Visible:=false;
			m_satellitini.visible:=false;
			m_satellitschluessel.Visible:=true;
		 end
		else
		begin
			m_satellitini.Visible:=false; ;
			m_satellitschluessel.Visible:=false;
			m_dbabgleich.Visible:=false;
			m_systemtyp.Visible:=true;
		end;
		StatusBar.Panels[0].Text:='Haupt-System - '+inttostr(nkr);;
	 end;
	 if (prg_typ=2) and (max_mitarbeiter<>0) then StatusBar.Panels[0].Text:='Demo-Version';

end;


function tform_main.trotz_satellit_weiter: boolean;
begin
result:=false;
if '0'<>mysql_d.Feldinhalt('select count(nummer) from satelliten',0) then
		if MessageDlg('Fortfahren kann unvorhergesehene Auswirkung auf den Datenabgleich haben trotzdem weiter?',
				mtConfirmation	,[mbYes, mbNo], 0)=mryes then result:= true;
end;

procedure TForm_main.UntersuchungsBefunde1Click(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
try
  speichern;
	form_untwahl:=tform_untwahl.Create(self);
	form_untwahl.HelpContext:=19;
	toolbar_berechtigung(form_untwahl.toolbar, ber);

	datamodul.sql_new(true,datamodul.q_bereich,'select * from bereich ', 'reihenfolge');

  if prg_typ=3 then
	begin
	  form_untwahl.RadioButton1.Visible:=false;
	  form_untwahl.DBEdit_goae_bef.Visible:=false;
	  form_untwahl.RadioButton2.Checked:=true;
	  form_untwahl.dbcheckbox_ext.visible:=false;
    form_untwahl.label23.visible:=false;
    form_untwahl.label20.Visible:=false;
    form_untwahl.button_goae.Visible:=false;
	end;

	form_untwahl.notebook.visible:=true;
	form_untwahl.notebook.pageindex:=4;
	form_untwahl.multiselect:=false;
	form_untwahl.ToolBar.Visible:=true;
	form_untwahl.Panel_botttom.Visible:=false;
	form_untwahl.caption:='Untersuchungs-Befunde';
	//  form_untwahl.tree.bereich_feldname:='bereich';
  form_untwahl.tree.mysqlquery:=datamodul.q_bereich;
  form_untwahl.tree.mysql_feld:='name';
	form_untwahl.tree.liste_lesen;
	form_untwahl.showmodal;

   if datamodul.q_bereich.state in [dsedit, dsinsert] then  datamodul.q_bereich.post;

   datamodul.q_bereich.refresh;
	datamodul.q_typ.refresh;
    //datamodul.q_berlist.filtered:=false;
    //datamodul.q_berlist.SQL.text:='select * from berlist';
    datamodul.sql_new(true,datamodul.q_berlist,'select * from berlist ', 'reihenfolge');

    //datamodul.q_berlist.filtered:=true;
    //treeview_ext_bereich.liste_lesen;
    PageControl_mainChange(self);
finally
	form_untwahl.release;
	application.processmessages;
end;
end;

procedure TForm_main.Normalbefund1Click(Sender: TObject);
begin
radiogroup_auffaellig.ItemIndex:=0;
end;

procedure TForm_main.Auffllig1Click(Sender: TObject);
begin
radiogroup_auffaellig.ItemIndex:=1;
end;

procedure TForm_main.SpeedButton_kalender1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
	 datamodul.q_arbeitsunfall.edit;
	 datamodul.q_arbeitsunfall['d_arbentgelt'] :=int(form_kalender.auswahlkalender.date);
	 end;
end;




procedure TForm_main.SpeedButton_a_einClick(Sender: TObject);
begin
 if form_kalender.showmodal =mrok then
	 begin
	 datamodul.q_arbeitsunfall.edit;
   datamodul.q_arbeitsunfall['arb_ein_dat'] :=int(form_kalender.auswahlkalender.date);
   end;
end;

procedure TForm_main.SpeedButton_a_aufClick(Sender: TObject);
begin
 if form_kalender.showmodal =mrok then
	 begin
	 datamodul.q_arbeitsunfall.edit;
   datamodul.q_arbeitsunfall['arb_auf_dat'] :=int(form_kalender.auswahlkalender.date);
   end;
end;

procedure TForm_main.SpeedButton13Click(Sender: TObject);
begin
 if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_arbeitsunfall.edit;
		datamodul.q_arbeitsunfall['u_datum'] :=int(form_kalender.auswahlkalender.date);
		datamodul.q_arbeitsunfall['d_arbentgelt']:=datamodul.q_arbeitsunfall['u_datum']+41;
   end;

end;

procedure TForm_main.SpeedButton14Click(Sender: TObject);
begin
 if form_kalender.showmodal =mrok then
	 begin
	 datamodul.q_arbeitsunfall.edit;
	 datamodul.q_ambulanz.edit;
	 //direktspeichern:=true;
	 datamodul.q_ambulanz['datum'] :=int(form_kalender.auswahlkalender.date);
   baum_neu:=true;
	 //datamodul.q_ambulanz.post;
	 //dbtreeview.knoten_neuordnen(dbtreeview.selected);
	 end;
end;

procedure TForm_main.BitBtn_b_zurueckClick(Sender: TObject);
begin
	datamodul.q_untersuchung['i_beurteilung']:=-1;
end;

procedure TForm_main.BitBtn_abteilungClick(Sender: TObject);
var
i_firma:integer;
abtres:twoint64;
abt_nr,ap_nr:int64;
query:string;
begin
  i_firma:=datamodul.q_firma['nummer'];
  abtres:=abt_manager(i_firma,0,'wahl',nil,0);
  abt_nr:=abtres[0];//abteilung
  ap_nr:=abtres[1];//Arbeitsplatz
  if abt_nr=0 then exit;
  query:=format('select kuerzel from abteilung where nummer =%s',[inttostr(abt_nr)]);
  datamodul.sql_new(false,datamodul.q_1,query,'');
  datamodul.q_mitarbeiter.edit;
  //datamodul.q_mitarbeiter['abteilung']:=datamodul.q_1['kuerzel'];
  setbigint(datamodul.q_mitarbeiter,'i_abteilung',abt_nr);
  setbigint(datamodul.q_mitarbeiter,'i_arbeitsplatz',ap_nr);
  abteilunganzeigen;
end;



procedure TForm_main.grid_ambulanzEnter(Sender: TObject);
begin
grid_ambulanz.tag:=-1;//modified;
end;


procedure tform_main.berechtigung_firma;
var
query,s_firma,s_firma_alt:string;
begin
with datamodul do
try
	firmen_berechtigung:='-1';
  //wenn berechtigung_firma leer und berechtigung_firma_old datenenth�lt sollen diese getauscht werden
  //-s. berechtigungen_lesen
	query:=format('select * from berechtigung_firma where benutzer=%d and storno=0',[user_id]);
	sql_new(true,q_b1,query,'i_firma');
	q_b1.first;
  s_firma_alt:='';
	while not q_b1.eof do
	begin
        s_firma:= q_b1.findfield('i_firma').asstring;
        if s_firma_alt<>s_firma then
        begin
        	firmen_berechtigung:=firmen_berechtigung+','+s_firma;
         s_firma_alt:=s_firma;
        end
        else
        begin
         q_b1.Edit;
        	q_b1.findfield('storno').asinteger:=1;
         q_b1.post;
        end;
      q_b1.next;
	end;
	//firmen_berechtigung:=copy(firmen_berechtigung, 2,length(firmen_berechtigung)); //erstes komma weg

   firmen_filter:=firmen_berechtigung;
	query:=format('select firma.* from firma where nummer in (%s)',[firmen_berechtigung]);
	sql_new(true,q_firma, query,'firma');
except
  showmessage('Fehler in Berechtigung_Firma' );  
end;
end;


function tform_main.aerzteanzeigen(edit:boolean;var index:int64):pkontaktpointer;
var
ber:integer;
begin
	new(result);
	if edit then
    begin
    ber:=b.b_string('aerzte-liste');
    if ber=-1 then exit;
	end
  else ber:=0;

	form_tabellen:=tform_tabellen.Create(self);
  //	form_tabellen_berechtigung(ber);
	form_tabellen.HelpContext:=47;  //2010
	form_tabellen.Panel_suchen.Visible:=true;
	form_tabellen.suchfeld:='name, fachgebiet,ort,memo,plz';
  form_tabellen.Panel_suchen.Visible:=true;

	form_tabellen.Caption:='�rzteliste';
	form_tabellen.Notebook.PageIndex:=10;
	form_tabellen.toolbar.Visible:=edit;

	if ber<2 then
	begin
	  form_tabellen.ToolButton_neu.Visible:=false;
	  form_tabellen.ToolButton_del.Visible:=false;
	end;
	if ber<1 then
	begin
	  form_tabellen.ToolButton_edit.Visible:=false;
	  form_tabellen.ToolButton_save.Visible:=false;
	end;

	//if not edit then sperren(form_tabellen.panel_notebook_10);
	if not edit then sperren(form_tabellen.notebook);
	form_tabellen.Panel_b.Visible:=true;
	form_tabellen.BitBtn_bwahl.visible:=false;
	form_tabellen.ToolButton_neu.enabled:=true;
	//form_tabellen.ToolButton_del.enabled:=true;
	datamodul.sql_new(true,datamodul.q_1,'select * from aerzteliste','name');

	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='name';

	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
  form_tabellen.DBmemo_p_name.DataField:='p_name';
	form_tabellen.DBEdit_aname.DataField:='name';
	form_tabellen.DBEdit_avorname.DataField:='vorname';
	form_tabellen.DBEdit_atitel.DataField:='titel';
	form_tabellen.DBEdit_azhd.DataField:='zhd';
	form_tabellen.DBEdit_astrasse.DataField:='strasse';
	form_tabellen.DBEdit_aplz.DataField:='plz';
	form_tabellen.DBEdit_aort.DataField:='ort';
	form_tabellen.DBEdit_afach.DataField:='fachgebiet';
	form_tabellen.DBEdit_atelefon.DataField:='telefon';
	form_tabellen.DBEdit_afax.DataField:='fax';
	form_tabellen.DBEdit_aemail.DataField:='email';
	form_tabellen.DBEdit_awww.DataField:='www';
	form_tabellen.DBmemo_amemo.DataField:='memo';
	grid_einstellen(form_tabellen.dbgrid_tabellen);
	//datamodul.q_1.Locate('nummer',inttostr(index),[]);
  datamodul.q_1.Locate('nummer',(index),[]);
	if form_tabellen.showmodal =mrok then
	 begin
    result.p_name:= form_tabellen.DBmemo_p_name.text;
    result.titel:=trim(form_tabellen.DBEdit_atitel.text);
	  result.name:=form_tabellen.DBEdit_aname.text;
	  result.vorname:=form_tabellen.DBEdit_avorname.text;
	  result.zhd:=form_tabellen.DBEdit_azhd.text;
	  result.strasse:=form_tabellen.DBEdit_astrasse.text;
	  result.plz:=form_tabellen.DBEdit_aplz.text;
	  result.ort:=form_tabellen.DBEdit_aort.text;
	  result.fach:=form_tabellen.DBEdit_afach.text;
	  result.email:=form_tabellen.DBEdit_aemail.text;
	  result.telefon:=form_tabellen.DBEdit_atelefon.text;
    result.fax:=form_tabellen.DBEdit_afax.text;
	  result.www:=form_tabellen.DBEdit_awww.text;
	  result.memo:=form_tabellen.DBmemo_amemo.text;

    result.nummer:=getbigint(datamodul.q_1,'nummer');
    index:=getbigint(datamodul.q_1,'nummer');
	 end;

	form_tabellen.release;
end;




procedure TForm_main.arztelistClick(Sender: TObject);
var
ind:int64;
k:pkontaktpointer;
begin
	//if nicht_satellit_tab then exit;
	speichern;
	ind:=0;
	k:=aerzteanzeigen(TRUE,ind);
  dispose(k);
end;


procedure TForm_main.DBgridEXT_namenDblClick(Sender: TObject);
begin
namenanzeigen;
end;


procedure tform_main.menue_leeren;
var
	newmenu: tmenuitem;
begin
while m_drucken.Count > 3 do
	begin
		newmenu:=m_drucken.Items[m_drucken.Count-1];
		m_drucken.Remove(newmenu); //me�eintr�ge l�schen
		newmenu.Free;
	end;
	//drucken.Visible:=false;

while email.Count > 2 do
	begin
		newmenu:=email.Items[email.Count-1]; //
		email.Remove(newmenu); //me�eintr�ge l�schen
		newmenu.Free;
	end;
  m_abteilung.visible:=false;
  m_UnfallverteilunginderFirma.visible:=false;
  //m_loeschen_rueck.Enabled:=false;
  m_RechnungDruckenundSpeichern.Visible:=false;
  m_UntersuchungsSchritte.visible:=false;
  m_druck_bescheinigung_kartei.Visible:=false;
  m_folgebescheinigung_kartei.visible:=false;
  m_word_pdf_anzeige_kartei.visible:=false;
  m_aendern_kartei.Visible:=false;
  //m_Export_Probandenbersicht.Visible:=false;
  if pagecontrol_main.ActivePage<>tabsheet_main then
	begin
  		m_uebersicht_kartei.enabled:=false;
  		m_uebersicht_kartei.visible:=false;
      m_GDT_schreiben.enabled:=false;
      m_GDT_schreiben.visible:=false;
    end;
  application.processmessages;
end;


procedure TForm_main.dbTreeViewChange(Sender: TObject; Node: TTreeNode);
var
newmenu: tmenuitem;
t:ttime;
	i_nummer:int64;
  rubrik:integer;
  daten:pnodepointer;
  query,s_nummer,t_nummer,s_dat:string;
  felder: array [0..1]of string;
begin
if not dbtreeview.change_allowed then  exit;
try
      dbtreeview.change_allowed:=false;

      //	statusbar.Panels[3].text:='change';
      application.ProcessMessages;
       t:=time();
       while dbtreeview.aktiv do
       begin
         if time()> t+0.0001 then exit;
       end;

      dbtreeview.aktiv:=true;

      if dbtreeview.a_rubrik>-1 then
      begin
      //

      image_paw.Visible:=false;
      {case dbtreeview.a_rubrik of
          1: begin
               if dbtreeview.tag=1 then form_main.formular_entsperren(true,false);
             end ;
          2: begin
             if dbtreeview.tag=2 then form_main.formular_entsperren(true,false);

           end ;
          4: begin
                if dbtreeview.tag=4 then form_main.formular_entsperren(true,false);
             end;
           7: begin
                if dbtreeview.tag=7 then form_main.formular_entsperren(true,false);
             end;
          else
          begin
            dbtreeview.tag:=0;
          end;

      end; }

        menue_leeren;

        application.processmessages;
        notebook_main.PageIndex:=dbtreeview.a_rubrik;
        //dbtreeview.Tag:=dbtreeview.a_rubrik;

        case dbtreeview.a_rubrik of   //befund, vorsorge.....
        1: begin

            m_aendern_kartei.Visible:=true;
            //menue_delete.Enabled:=true;
            if dbtreeview.a_ebene<2 then
            begin
              notebook_main.PageIndex:=9; //befund-datum
              exit;
            end;
            bereich_change(false); //hier darstellung des entspr. notebooks
            richedit_befund.SelAttributes.Size:=fontsize_anmerkung;
            richedit_befund.Font.Size:=fontsize_anmerkung;
            richedit_befund.SelAttributes.Style:=[];//   CurrText.Style
            richedit_befund.Font.Style:=[];


            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='aktuelle Befunde';
            newmenu.Name:='m2';
            newmenu.OnClick:=untersuchung_drucken_speziell;
            m_drucken.Add(newmenu);



            if datamodul.q_befunde['storno']=1 then
            begin
              toolbuttonedit.Enabled:=false;
              menue_edit.Enabled:=false;
            end
            else
            begin
              toolbuttonedit.Enabled:=true;
              menue_edit.Enabled:=true;
            end;



            end;


        2: begin    //untersuchung
            if dbtreeview.a_ebene<1 then
            begin
              notebook_main.PageIndex:=0;
              exit;
            end;
            //alles sichtbar
            m_aendern_kartei.Visible:=true;
            dbradiogroup_beurteilung_eignung.Visible:=true;
            dbmemo_beurteilung.Visible:=true;
            dbmemo_beurteilung_ma.Visible:=true;

            //Arbeitgeber
            if ((b.b_string_silent('proband-bescheinigung-ag')>=0) and (b.b_string_silent('proband-bescheinigung')<0))  then
            begin
              dbmemo_beurteilung_ma.Visible:=false; //mitarbeiter niemals sichtbar
            end;

            tool_aktiv(datamodul.q_untersuchung);
            s_nummer:= datamodul.q_untersuchung.findfield('i_typ').asstring;
            query:=format('select untersuchung,i_arbmedvv from typ where nummer=%s',[s_nummer]);
            mysql_d.Feldarray(query,[0,1],felder);
            memo_unt_typ.text:=felder[0];


            //Eignung und vorsorge
            if  felder[1]='0' then  //eignung
            begin
                notebook_vorsorge.ActivePage:='eignung';
                groupbox_anmerkung_arbeitgeber.Visible:=true;
                label_bescheinigung.Caption:='Eignungsuntersuchung';
                dbradiogroup_beurteilung_eignung.Enabled:=true;
                dbradiogroup_beurteilung_eignung.DataSource:=datamodul.ds_untersuchung;
                dbradiogroup_beurteilung_arbmedvv.Enabled:=false;
                dbradiogroup_beurteilung_arbmedvv.DataSource:=nil;
                if druck_mit_zustimmung_besch_eignung then panel_erlaubnis_ag.Visible:=true else panel_erlaubnis_ag.Visible:=false;

                //wenn zustimmung zur Weitergabe an AG
                if zust_arbmed_besch_vorhanden(datamodul.q_untersuchung.findfield('i_weitergabe').asinteger,datamodul.q_untersuchung.findfield('i_typ').asinteger,datamodul.q_untersuchung.findfield('datum').AsDateTime  ) then
                begin
                    image_paw.Visible:=false;
                end
                else
                begin
                    image_paw.Visible:=true;
                    if (b.b_string_silent('proband-bescheinigung-ag')>=0)  then
                     begin
                       dbradiogroup_beurteilung_eignung.Visible:=false;
                       dbmemo_beurteilung.Visible:=false;
                     end;
                  end;
               end
            else  //vorsorge
               begin
                 notebook_vorsorge.ActivePage:='vorsorge';
                  groupbox_anmerkung_arbeitgeber.Visible:=false;
                  label_bescheinigung.Caption:='Vorsorge nach ArbmedVV';
                  dbradiogroup_beurteilung_eignung.Enabled:=false;
                  dbradiogroup_beurteilung_eignung.DataSource:=nil;
                  dbradiogroup_beurteilung_arbmedvv.Enabled:=true;
                  dbradiogroup_beurteilung_arbmedvv.DataSource:=datamodul.ds_untersuchung;
                  if druck_mit_zustimmung_besch then panel_erlaubnis_ag.Visible:=true else panel_erlaubnis_ag.Visible:=false;

                  if zust_arbmed_besch_vorhanden(datamodul.q_untersuchung.findfield('i_weitergabe').asinteger,datamodul.q_untersuchung.findfield('i_typ').asinteger,datamodul.q_untersuchung.findfield('datum').AsDateTime  ) then
                  begin
                    image_paw.Visible:=false ;
                    //dbmemo_beurteilung_ma.Visible:=true;
                  end
                  else
                  begin
                    image_paw.Visible:=true;
                    //dbmemo_beurteilung_ma.Visible:=true;
                  end;

               end;

            if datamodul.q_untersuchung['datum_einladung']<>null_datum then
            begin
              label_vedat.visible:=true;
              dbedit_vedat.visible:=true;
            end
            else
            begin
              label_vedat.visible:=false;
              dbedit_vedat.visible:=false;
            end;

            m_UntersuchungsSchritte.visible:=true;
            m_druck_bescheinigung_kartei.Visible:=true;
            //if speicher_bescheinigungen or bescheinigung_in_kartei then
            m_druck_bescheinigung_kartei.Caption:='Erstellen der arbeitsmedizinischen Bescheinigung';
            //else
            //m_druck_bescheinigung_kartei.Caption:='Drucken der arbeitsmedizinischen Bescheinigung';

            m_folgebescheinigung_kartei.visible:=true;

            m_drucken.visible:=true;
            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Erstellen der arbeitsmedizinische Bescheinigungen';
            newmenu.Name:='m3';
             newmenu.ShortCut:=ShortCut(Word('B'), [ssCtrl, ssshift]);
            newmenu.OnClick:=bescheinigung_drucken;
            m_drucken.Add(newmenu);


            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='arbeitsmedizinische Bescheinigung';
            newmenu.Name:='e1';
            newmenu.OnClick:=bescheinigung_emailen;
            email.Add(newmenu);

            //if datamodul.q_untersuchung['storno']=1 then m_loeschen_rueck.Enabled:=true;
            //if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        3: begin      //impfung
            if dbtreeview.a_ebene<1 then
            begin
              notebook_main.PageIndex:=0;
              exit;
            end;
            m_aendern_kartei.Visible:=true;
            tool_aktiv(datamodul.q_impfung);
                              s_nummer:= datamodul.q_impfung.findfield('i_typ').asstring;
                              query:=format('select untersuchung from typ where nummer=%s',[s_nummer]);
                              edit_impf_typ.text:=mysql_d.Feldinhalt(query,0);

            if datamodul.q_impfung['datum_einladung']<>null_datum then
            begin
              label_iedat.visible:=true;
              dbedit_iedat.visible:=true;
            end
            else
            begin
              label_iedat.visible:=false;
              dbedit_iedat.visible:=false;

            end;

            //drucken.visible:=true;
            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Impf�bersicht';
            newmenu.Name:='m1';
            newmenu.OnClick:=impfung_drucken_alle;
            m_drucken.Add(newmenu);
            //if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        4: begin  //labor
            if dbtreeview.a_ebene<2 then
            begin
            daten:=node.Data;

              s_dat:=dat_delphi_to_sql(  datetostr(daten^.datum));
             query:=format('select textbausteine.*, SUBSTR(tbs,1,500) as tbs_s from textbausteine left join tabelle_hinweise on (tabelle_hinweise.i_tbs =textbausteine.nummer) left join labor on(labor.nummer=tabelle_hinweise.i_tab_nummer) where labor.datum="%s" ',[s_dat]);
              query:=query + ' and tabelle_hinweise.storno=0 and textbausteine.i_kapitel>=0 ' ;
              datamodul.sql_new(false,datamodul.q_hinweis,query,'');
            if datensatz(datamodul.q_hinweis) then dbgrid_labor_hinweise_all.Visible:=true else  dbgrid_labor_hinweise_all.Visible:=false;
              notebook_main.PageIndex:=16;
              menue_delete.Enabled:=false;
              ToolButtonloeschen.Enabled:=false;
              exit;
            end;
            m_aendern_kartei.Visible:=true;
            tool_aktiv(datamodul.q_labor);
            t_nummer:= datamodul.q_labor.findfield('nummer').asstring;
            s_nummer:= datamodul.q_labor.findfield('i_typ').asstring;
            query:=format('select untersuchung from typ where nummer=%s',[s_nummer]);
            edit_lab_typ.text:=mysql_d.Feldinhalt(query,0);

            if datamodul.q_labor['datum_einladung']<>null_datum then
            begin
              label_ledat.visible:=true;
              dbedit_ledat.visible:=true;
            end
            else
            begin
              label_ledat.visible:=false;
              dbedit_ledat.visible:=false;

            end;

            if dbtreeview.a_ebene=1 then
            begin
              //auswahl aller hinweise

              query:=format('select * from textbausteine left join tabelle_hinweise on (tabelle_hinweise.i_tbs =textbausteine.nummer) left join labor on(labor.nummer=tabelle_hinweise.i_tab_nummer) where labor.nummer=%s and tabelle_hinweise.storno=0  ',[t_nummer]);
              query:=query + ' and textbausteine.i_kapitel>=0 ' ;
            end
            else
            begin
              //auswahl aktueller hinweis
              query:=format('select textbausteine.*,SUBSTR(tbs,1,500) as tbs_s from textbausteine left join tabelle_hinweise on (tabelle_hinweise.i_tbs =textbausteine.nummer) left join labor on(labor.nummer=tabelle_hinweise.i_tab_nummer) where labor.nummer=%s  ',[t_nummer]);
              query:=query + ' and tabelle_hinweise.storno=0  and textbausteine.i_kapitel>=0 ' ;
            end;
            datamodul.sql_new(false,datamodul.q_hinweis,query,'');
            if datensatz(datamodul.q_hinweis) then dbgrid_labor_hinweise.Visible:=true else  dbgrid_labor_hinweise.Visible:=false;

            //drucken.visible:=true;
            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Labor-Mitteilung an den Probanden';
            newmenu.Name:='m1';
            newmenu.OnClick:=labor_drucken_alle;
            m_drucken.Add(newmenu);
            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Verlauf aktueller Laborwert';
            newmenu.Name:='m2';
            newmenu.OnClick:=labor_drucken_speziell;
            m_drucken.Add(newmenu);
            PageControl_laborChange(sender);
            //if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        5: begin
            if dbtreeview.a_ebene<1 then
            begin
              notebook_main.PageIndex:=0;
              exit;
            end;
            m_aendern_kartei.Visible:=true;
            arbeitsunfall_show;
            tool_aktiv(datamodul.q_ambulanz);

            //drucken.visible:=true;
            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Unfallanzeige erste Seite';
            newmenu.Name:='m2';
            newmenu.OnClick:=unfallanzeige_drucken_seite;
            m_drucken.Add(newmenu);

            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Unfallanzeige alle Seiten';
            newmenu.Name:='m1';
            newmenu.OnClick:=Unfallanzeige_drucken;
            m_drucken.Add(newmenu);

            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='Unfall-Kurzanzeige ';
            newmenu.Name:='m3';
            newmenu.OnClick:=kurz_anzeige_drucken;
            m_drucken.Add(newmenu);

            newmenu:=tmenuitem.Create(self);
            newmenu.Caption:='�bersicht Vitalparameter ';
            newmenu.Name:='m4';
            newmenu.OnClick:=ambulanz_drucken_vital;
            m_drucken.Add(newmenu);

            // nur bei Arbeitsunfall
            if datamodul.q_ambulanz.findfield('b_arbeitsunfall').asinteger=1 then
            begin
              newmenu:=tmenuitem.Create(self);
              newmenu.Caption:='Unfall-Kurzanzeige an Abteilung';
              newmenu.Name:='e2';
              newmenu.OnClick:=Kurz_Anzeige_mailen;
              email.Add(newmenu);
            end;
            if ((b.b_string_silent('proband-ambulanz')<=0) and (b.b_string_silent('proband-arbeitsunfall')>0))  then
            begin
              groupbox_vitaldaten.Visible:=false ;

             end
             else
             begin
                groupbox_vitaldaten.Visible:=true;

             end;

            //if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        6: begin
            if dbtreeview.a_ebene<1 then
            begin
              notebook_main.PageIndex:=0;
              exit;
            end;
            m_aendern_kartei.Visible:=true;
            tool_aktiv(datamodul.q_diagnosen);
            //if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        7: begin
            if dbtreeview.a_ebene<1 then
            begin
              notebook_main.PageIndex:=0;
              exit;
            end;
            m_aendern_kartei.Visible:=true;
            tool_aktiv(datamodul.q_besonderheiten);
            //if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        8: begin   //dokument
            if dbtreeview.a_ebene<1 then
            begin
              notebook_main.PageIndex:=0;
              exit;
            end;

            tool_aktiv(datamodul.q_dokumente);
            try //wenn container leer

            if (arbeitsmedizin_Proband.State<>osempty) and (lowercase(copy(arbeitsmedizin_Proband.OleClassName,1,13))='word.document') then
            begin
              m_aendern_kartei.Visible:=true;
              newmenu:=tmenuitem.Create(self);
              newmenu.Caption:='Dokument als Anhang verschicken';
              newmenu.Name:='e_ma';
              newmenu.OnClick:=Anhang_mailen;
              email.Add(newmenu);

              if prg_typ=4 then
              begin
                newmenu:=tmenuitem.Create(self);
                newmenu.Caption:='Dokument f�r Adressaten ausdrucken';
                newmenu.Name:='m_adressat_ausdruck';
                newmenu.OnClick:=adressat_ausdruck;
                m_drucken.Add(newmenu);
              end;

              m_word_pdf_anzeige_kartei.Visible:=true;

            end;

            if (arbeitsmedizin_Proband.State<>osempty) and ((lowercase(copy(arbeitsmedizin_Proband.OleClassName,1,8))='acroexch') or(lowercase(copy(arbeitsmedizin_Proband.OleClassName,1,11))='foxitreader')) then
            begin
               m_word_pdf_anzeige_kartei.Visible:=true;
            end;

            except
                showmessage('Die Anwendung zur Anzeige des Dokuments ist vermutlich nicht korrekt installiert oder das Dokument ist leer.');
            end;

           // if dbtreeview.a_nummer<>dbtreeview.oldnummer then formular_sperren;
          end;
        9: begin
            //gel�schte einzelbefunde
            m_aendern_kartei.Visible:=true;
            menue_delete.Enabled:=menue_save.Enabled ;
            ToolButtonloeschen.Enabled:=menue_delete.Enabled;
            notebook_main.PageIndex:=1;

            bereich_change(true); //hier darstellung des entspr. notebooks
          end;
        10:begin
            //Rechnungen
            //rechnung f�r das aktuelle datum und den MA muss gefiltert werden firma=-1
            //
              //grid_einstellen(dbgridext_repo);
             m_aendern_kartei.Visible:=true;

            newmenu:=tmenuitem.Create(self);
            if speicher_rechnungen or rechnungen_in_kartei then
              newmenu.Caption:='Rechnung drucken und speichern'
              else
              newmenu.Caption:='Rechnung drucken';
            newmenu.Name:='rechnung2';
            newmenu.OnClick:=BitBtn_re_maClick;
            m_drucken.Add(newmenu);
            m_RechnungDruckenundSpeichern.Visible:=true;
               grid_einstellen(DBgridEXT_re_positionen);
          end;
        11:begin
            //verkauf
            m_aendern_kartei.Visible:=true;
              if datamodul.q_artikel['i_material']=0 then
              begin
                   radiobutton2.Checked:=true;
                   bitbtn_art_wahl.Visible:=false;
              end
              else
              begin
               radiobutton1.Checked:=true;
               bitbtn_art_wahl.Visible:=true;
              end;
          end;
        15:begin
             //anamnese_laden(0);
             toolbuttonedit.Enabled:=false;

          end;


        end;
      end;
      if (dbtreeview.a_rubrik<>dbtreeview.oldrubrik) and (dbtreeview.a_rubrik<=12) then
        begin
          formular_sperren;

        end;
         dbtreeview.oldnummer:=dbtreeview.a_nummer;
         dbtreeview.oldrubrik:=dbtreeview.a_rubrik;

finally
      dbtreeview.aktiv:=false;
      dbtreeview.change_allowed:=true;
end;

if baum_neu then
		begin
      formular_sperren;
		  baum_neu:=false;
		 {i_stammdaten:=getbigint(datamodul.q_mitarbeiter,'nummer');
		 i_nummer:=getbigint(datamodul.q_untersuchung,'nummer');
		 gehezuuntersuchung(1,i_stammdaten, i_nummer);}
      daten:=dbtreeview.Selected.Data;
      i_nummer:=daten^.nummer;
      rubrik:=daten^.rubrik;
      dbtreeview.liste_lesen;
      dbtreeview.change_allowed:=false;
      dbtreeview.Expand_e1;
      dbtreeview.change_allowed:=true;
      dbtreeview.gehe_zu_datensatz(rubrik,i_nummer);
      dbtreeview.Expand_e1;

		end
   else
     dbtreeview.name_neu(dbtreeview.selected);

case dbtreeview.a_rubrik of
	  1: begin
         if dbtreeview.tag=1 then form_main.formular_entsperren(true,false);
	     end ;
    2: begin
       if dbtreeview.tag=2 then form_main.formular_entsperren(true,false);

     end ;
	  4: begin
          if dbtreeview.tag=4 then form_main.formular_entsperren(true,false);
       end;
     7: begin
          if dbtreeview.tag=7 then form_main.formular_entsperren(true,false);
       end;
    else
    begin
      dbtreeview.tag:=0;
    end;

end;

end;


procedure TForm_main.dbTreeViewChanging(Sender: TObject; Node: TTreeNode;
	var AllowChange: Boolean);
var
daten:pnodepointer;
bereich,ebene,rubrik:integer;
nummer:int64;
begin
if (not dbtreeview.change_allowed) or (node=nil) then exit;
if speichert_gerade then
begin
  Allowchange:=false;
  exit;
end;
  dbtreeview.loesch_flag:=false;
  menue_leeren;  //k�nnte eigentlich weg
	//statusbar.Panels[3].text:='changing';
	application.ProcessMessages;
  //  dbspeichern(false);
  //  form_main.formular_sperren;   //immer sperren      aber am ende
  daten:=node.Data;
  bereich:=daten^.bereich;
  ebene:=daten^.ebene;
  rubrik:=daten^.rubrik;
  nummer:=daten^.nummer;

  //###speichern### solange ebene gleich wird nicht gespeichert - message an entsprerren (es wird auch nicht entsperrt????)
  // befunde labor evtl neues sperrobjekt alle labors, befunde des probanden
	  case dbtreeview.a_rubrik of
	  1: begin
         bereich_speichern;
         datamodul.sql_post(datamodul.q_befunde);
         //if dbtreeview.tag<>1 then form_main.formular_sperren;
         if (bereich>-1) and (ebene>-1) and (rubrik>-1) and (nummer>-1) then
         form_main.formular_sperren;   //immer sperren
	     end ;
	  4: begin
          datamodul.sql_post(datamodul.q_labor);
          //if dbtreeview.tag<>1 then form_main.formular_sperren;
          form_main.formular_sperren;   //immer sperren
       end;
    8: begin
       try
           //olecontainer_close(arbeitsmedizin_Proband);
           //arbeitsmedizin_Proband.close;
           //dbspeichern(true);
           datamodul.sql_post(datamodul.q_dokumente);

           application.ProcessMessages;
           olecontainer_close(arbeitsmedizin_Proband);
           //olecontainer_destroy(arbeitsmedizin_Proband);
           form_main.formular_sperren;   //immer sperren
       except
           //showmessage('olecontainer ist leer, Datei wurde nicht gespeichert');
       end;
      end;
      else
      begin
        dbspeichern(false);
        form_main.formular_sperren;   //immer sperren
        //if dbtreeview.tag<>1 then form_main.formular_sperren;
      end;

	  end;
  //form_main.formular_sperren;   //immer sperren
   allowchange:=not dbtreeview.loesch_flag;
end;


procedure TForm_main.CheckBox_impfungClick(Sender: TObject);
begin
 pagecontrol_mainchange(pagecontrol_main);
end;


procedure TForm_main.dbTreeViewDblClick(Sender: TObject);
var
	i64:int64;
  i:integer;
begin
	 if dbtreeview.a_ebene=0 then exit;
	 case dbtreeview.a_rubrik of
	 1:
	 begin
		if radiogroup_auffaellig.Enabled then
     begin
			if radiogroup_auffaellig.ItemIndex=0  then radiogroup_auffaellig.ItemIndex:=1 else radiogroup_auffaellig.ItemIndex:=0 ;
            bereich_speichern;
     end;   
	 end;
	 2:
	 begin
		dbspeichern(false);
		i64:= getbigint(datamodul.q_untersuchung,'i_termin');
		check_form_termine;
		if (i64=0)  then
		begin
		  if (datamodul.q_untersuchung['i_status']<>4) then
			if form_termine.terminiere_uil( getbigint(datamodul.q_untersuchung,'nummer'),1)
				then form_termine.showmodal;
		end
		else
		begin

			if form_termine.gehezutermin(i64) then
			begin
			 form_termine.neu_lesen:=false;
			 form_termine.ShowModal;
			end;
		end;
	 end;
	 3:begin
		dbspeichern(false);
		i64:= getbigint(datamodul.q_impfung,'i_termin');
		check_form_termine;
		if (i64=0)  then
		begin
		  if (datamodul.q_impfung['i_status']<>4) then
			if form_termine.terminiere_uil( getbigint(datamodul.q_impfung,'nummer'),2)
				then form_termine.showmodal;
		end
		else
		 if form_termine.gehezutermin(getbigint(datamodul.q_impfung,'i_termin'))
		 then form_termine.ShowModal;
	 end;
	 4:begin
      dbspeichern(false);
      i64:= getbigint(datamodul.q_labor,'i_termin');
      check_form_termine;
      if (i64=0)  then
        begin
          if (datamodul.q_labor['i_status']<>4) then
          if form_termine.terminiere_uil( getbigint(datamodul.q_labor,'nummer'),3)
            then form_termine.showmodal;
        end
      else
        if form_termine.gehezutermin(getbigint(datamodul.q_labor,'i_termin'))
        then form_termine.ShowModal;
		end;
    10:begin
      i:=datamodul.q_firma.findfield('nummer').asinteger;
      i64:=getbigint(datamodul.q_rechnung,'nummer');
      gehezu_rechnung_firma(i, i64);
    end;

   else
    begin
      formular_entsperren(true,false);
     end;
     
	 end; //case
end;


procedure TForm_main.Befunde1Click(Sender: TObject);
begin
 if b.b_string('Anwendung')=-1 then exit;
tmenuitem(sender).checked :=  not tmenuitem(sender).checked;

if sender=m_storno_show then
begin
	storno_show:=m_storno_show.Checked;
	dbspeichern(false);
	storno_sql(storno_show);
  if storno_show then
  showmessage('Anzeigen der gel�schten Datens�tze nur f�r Pr�fzwecke, Keine Dateneingaben vornehmen!');
end;

pm_f_rechnung_wiederherstellen.Enabled:= storno_show;

berechtigung_firma;
berechtigungen_einstellen;
application.ProcessMessages;
PageControl_mainChange(pagecontrol_main);
end;


procedure TForm_main.Panel_tResize(Sender: TObject);
begin
panel_tl.Width:=panel_t.width div 2;
combobox_history.Width:= panel_t.width div 2-40;//-10;
end;


procedure TForm_main.DBLookupComboBox1CloseUp(Sender: TObject);
begin
	if tdblookupcombobox(sender).datasource.DataSet['i_untersucher']<>null then
		akt_untersucher:=tdblookupcombobox(sender).datasource.DataSet['i_untersucher'] ;// datamodul.q_ambulanz['i_untersucher'] ;
end;

procedure TForm_main.baumrunter1Click(Sender: TObject);
begin
  if pagecontrol_main.ActivePage<>tabsheet_main then exit;
	with dbtreeview do
	begin
	if selected<>nil then
     begin
       if Selected.GetNext=nil then exit;
       selected:= Selected.GetNext;
       dbtreeview.change(selected);
     end;
	end;
end;

procedure TForm_main.baumhoch1Click(Sender: TObject);
begin
   if pagecontrol_main.ActivePage<>tabsheet_main then exit;
	with dbtreeview do
	begin
	if selected<>nil then
     begin
          if Selected.GetPrev=nil then exit;
          selected:= Selected.GetPrev ;
          dbtreeview.change(selected);
     end;
	end;
end;

procedure TForm_main.BitBtn3Click(Sender: TObject);
var n:string ;
begin
if b.b_objekt('proband-ambulanz')<1 then
begin
	 showmessage('keine Berechtigung');
	 exit;
end;
if  email_send then
	begin
	  speichern;
	  n:='Arbeitsunfall ';
	  if datamodul.q_mitarbeiter['name']<>null then n:=n+datamodul.q_mitarbeiter['name'];
	  if datamodul.q_mitarbeiter['vorname']<>null then n:=n+' '+datamodul.q_mitarbeiter['vorname'];
	  if datamodul.q_mitarbeiter['geb_dat']<>null then n:=n+' ' +datetostr(datamodul.q_mitarbeiter['geb_dat']);

	  form_main.dok_erstellen(Arbeitsmedizin,8,'ambulanz',datamodul.q_ambulanz,[''],['']);
    if email_erstellen( Arbeitsmedizin,false,'arbeitssicherheit','senddirekt','Ambulanz')   //( Arbeitsmedizin,false,'email','arbeitssicherheit,senddirekt','Ambulanz')
	  then
		 begin
		  datamodul.q_arbeitsunfall.edit;
		  datamodul.q_arbeitsunfall['b_int_meld']:=1;
		  direktspeichern:=true;
		  datamodul.q_arbeitsunfall.Post;
		 end;
	  //com.doku_schliessen;
    olecontainer_destroy(Arbeitsmedizin);
	  //if word_close then com.word_schliessen;
end;
end;

procedure TForm_main.m_maloeschenClick(
	Sender: TObject);
var query:string;
f_nummer:integer;
begin
if  b.b_string('Importieren')=-1 then exit;
  speichern;
if Messagedlg('Soll archivierte Probanden ohne Karteieintr�ge gel�scht werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;

with datamodul do
begin
		pagecontrol_main.activepage:=tabsheet_firma;
		f_nummer:=q_firma['nummer'];
		query:=format('select * from mitarbeiter where i_firma=%d and archiv="X"',[f_nummer]);
		sql(true,q_mitarbeiter,query,'');

		q_mitarbeiter.first;
		while not q_mitarbeiter.eof do
		begin
			dateiloeschen(q_mitarbeiter,true,true);
			q_mitarbeiter.next;
		end;
end;
end;

procedure TForm_main.Grid_sehtestKeyDown(Sender: TObject; var Key: Word;
	Shift: TShiftState);
begin
	 Edit_s_horizontalChange(Sender);

end;

procedure TForm_main.Splitter3Moved(Sender: TObject);
begin
	  grid_suchen(tabsheet_firmenadresse);
end;

procedure TForm_main.FormShow(Sender: TObject);
begin
if form_main.tag<>1 then PageControl_mainChange(pagecontrol_main);


end;

procedure TForm_main.Branchen1Click(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;

	ber:=b.b_string('Firmen-Listen');
	if ber=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.HelpContext:=0;
	form_tabellen_berechtigung(ber);

	form_tabellen.suchfeld:='Branche';
	form_tabellen.Caption:='Branche';
	form_tabellen.Notebook.PageIndex:=19;
	form_tabellen.Label_string.Caption:='Branchen';
	form_tabellen.ToolButton_neu.enabled:=true;
	datamodul.sql_new(true,datamodul.q_1,'select * from branche','branche');

	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Branche';

	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	form_tabellen.DBEdit_string.DataField:='branche';
	grid_einstellen(form_tabellen.dbgrid_tabellen);

	form_tabellen.showmodal;
	form_tabellen.release;
	dbcombo_laden;
end;

procedure TForm_main.FirmenGruppen1Click(Sender: TObject);
var ber:integer;
begin
	if nicht_satellit_tab then exit;

	ber:=b.b_string('Firmen-Listen');
	if ber=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen.HelpContext:=0;
	form_tabellen_berechtigung(ber);

	form_tabellen.suchfeld:='f_gruppe';
	form_tabellen.Caption:='Firmen-Gruppen';
	form_tabellen.Notebook.PageIndex:=19;
	form_tabellen.Label_string.Caption:='Firmen-Gruppe';
	form_tabellen.ToolButton_neu.enabled:=true;
	datamodul.sql(true,datamodul.q_1,'select * from f_gruppe','f_gruppe');

	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='F_Gruppe';

	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	form_tabellen.DBEdit_string.DataField:='f_gruppe';
	grid_einstellen(form_tabellen.dbgrid_tabellen);

	form_tabellen.showmodal;
	form_tabellen.release;
	dbcombo_laden;
end;

procedure TForm_main.ToolButton_firmaClick(Sender: TObject);
var
query:string;
begin
try
	form_f_wahl:=tform_f_wahl.create(self);
	if form_f_wahl.ShowModal<>mrok then exit;

	pagecontrol_main.ActivePage:=tabsheet_firma;
	pagecontrol_firma.ActivePage:=tabsheet_firmenadresse;

	firmen_filter:=form_f_wahl.firmen_nummern;
   if form_f_wahl.CheckBox_rfiltern.Checked then
   	query:=format('select * from firma where (parent in (%s)) or (nummer in (%s)) ' ,[firmen_filter,firmen_filter])
   else
		query:=format('select * from firma where nummer in (%s)',[firmen_filter]);

	datamodul.sql(true,datamodul.q_firma,query,'firma');

   if  form_f_wahl.CheckBox_rzeigen.Checked  then
   begin
   	if dbgridext_firma.Columns.Count=1 then
      begin
   		dbgridext_firma.Columns.Add;
         dbgridext_firma.Columns[1].FieldName:='re_empfaenger';
         dbgridext_firma.Columns[1].Width:=300;
         dbgridext_firma.Columns[1].title.caption:='�bergeordnete Firma';
         dbgridext_firma.Options:=dbgridext_firma.Options + [dgtitles];
        grid_einstellen(DBgridEXt_firma);

      end;
   end
   else
   begin
     if dbgridext_firma.Columns.Count>1 then
      begin
   		dbgridext_firma.Columns.Delete(1);
         dbgridext_firma.Options:=dbgridext_firma.Options - [dgtitles];
         grid_einstellen(DBgridEXt_firma);

      end;
   end;

   //arbene_geladen:=false;
   //arbene_geladen:=true;
	firma_gefiltert:=true;
	form_namsuch.filtern;
  pagecontrol_mainChange(self);
finally
	form_f_wahl.Release;
end;
end;

procedure TForm_main.SpeedButton15Click(Sender: TObject);
begin
 if form_kalender.showmodal =mrok then
   if sperrdatum_ok(form_kalender.auswahlkalender.date,1) then
	 begin
		datamodul.q_befunde.edit;
		datamodul.q_befunde['datum'] :=int(form_kalender.auswahlkalender.date);
     //datamodul.q_befunde.post;
     baum_neu:=true;
	 end;
end;

procedure TForm_main.SpeedButton16Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
   if sperrdatum_ok(form_kalender.auswahlkalender.date,1) then
	 begin
		datamodul.q_dokumente.edit;
		datamodul.q_dokumente['datum'] :=int(form_kalender.auswahlkalender.date);
     baum_neu:=true;
		//datamodul.q_dokumente.post;
		//dbtreeview.knoten_neuordnen(dbtreeview.selected);
	 end;
end;

procedure TForm_main.SpeedButton_besonderheitenClick(Sender: TObject);
var
	tab:tzquery;
begin
if sender =speedbutton_icd then tab:=datamodul.q_diagnosen;
if sender =speedbutton_besonderheiten then tab:=datamodul.q_besonderheiten;
if form_kalender.showmodal =mrok then
  if sperrdatum_ok(form_kalender.auswahlkalender.date,1) then
	 begin
		tab.edit;
		tab['datum'] :=int(form_kalender.auswahlkalender.date);
     baum_neu:=true;
		//direktspeichern:=true;
		//tab.post;
		//dbtreeview.knoten_neuordnen(dbtreeview.selected);
	 end;
end;

procedure TForm_main.m_maArchivierenClick(Sender: TObject);
var
flag,query:string;
begin
if  b.b_string('Importieren')=-1 then exit;
  speichern;
with datamodul do
begin
	if kein_datensatz(q_firma) then
	begin
	  showmessage('Es muss eine Firma ausgew�hlt sein.');
	  exit;
	end;

	if sender =m_maarchivieren then   flag:='X' else flag:='-';

	if MessageDlg('Archiv bei allen Probanden der Firma: '+q_firma['firma']+' �ndern?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
    query:=format('update mitarbeiter set archiv="%s" where i_firma=%s',[flag,inttostr(q_firma['nummer'])]);
    mysql_d.sql_exe(query);
   {
	query:=format('select nummer,archiv from mitarbeiter where i_firma=%s',[inttostr(q_firma['nummer'])]);
	sql_new(true,q_1,query,'');
	datamodul.q_1.first;
	while not datamodul.q_1.eof do
	begin
	  datamodul.q_1.edit;
	  datamodul.q_1['archiv']:=flag;
	  datamodul.q_1.post;
	  datamodul.q_1.next;
	end;}

	datamodul.q_mitarbeiter.refresh;
	datamodul.q_mitarbeiter_such.refresh;
end;
end;

procedure TForm_main.PasswortchangeClick(Sender: TObject);
begin
speichern;
with datamodul do
try
	//datamodul.q_schluessel.Locate('nummer',inttostr(user_id),[]);
  speichern;
	form_berechtigung:=TForm_berechtigung.create(self);
	form_berechtigung.tag:=1;
	form_berechtigung.Panel_left.visible:=false;
	form_berechtigung.TabSheet_b_firma.TabVisible:=false;
	form_berechtigung.TabSheet_b_aktionen.TabVisible:=false;
	form_berechtigung.ToolBar.visible:=true;
   form_berechtigung.Toolbutton_new.visible:=false;
   form_berechtigung.Toolbutton_del.visible:=false;
	form_berechtigung.Caption:='Passwort ver�ndern';
	form_berechtigung.Notebook.PageIndex:=1;
   datamodul.q_schluessel.Locate('nummer',user_id,[]);
  form_berechtigung.edit_p_user.text:= datamodul.q_schluessel.findfield('name').asstring;
  if copy (datamodul.q_schluessel['passwort'],1,3)='Arb' then form_berechtigung.edit_p_alt.visible:=true else form_berechtigung.edit_p_alt.visible:=false;
  
	form_berechtigung.ShowModal;

  user_name:=datamodul.q_schluessel.findfield('name').asstring;
  StatusBar.Panels[1].Text:='User: '+user_name;
  Form_berechtigung.formularentsperren;
finally
	form_berechtigung.Release;
end;
end;

procedure TForm_main.emailListe1Click(Sender: TObject);
begin
try
form_email:=tform_email.Create(self);
form_email.showmodal;
finally
form_email.Release;
end;
end;

procedure TForm_main.form_tabellen_berechtigung(ber:integer);
begin
if ber<2 then
begin
	form_tabellen.ToolButton_neu.Visible:=false;
	form_tabellen.ToolButton_del.Visible:=false;
end;
if ber<1 then
begin
	form_tabellen.ToolButton_edit.Visible:=false;
	form_tabellen.ToolButton_save.Visible:=false;
end;
end;

procedure TForm_main.form_untersuchung_berechtigung(ber:integer);
begin
if ber<2 then
begin
	form_untersuchungstyp.ToolButton_new.Visible:=false;
	form_untersuchungstyp.ToolButton_delete.Visible:=false;
end;
if ber<1 then
begin
	form_untersuchungstyp.ToolButton_edit.Visible:=false;
	form_untersuchungstyp.ToolButton_save.Visible:=false;
	form_untersuchungstyp.ToolButton_cancel.Visible:=false;
end;
end;


procedure TForm_main.toolbar_berechtigung(bar:ttoolbar;ber:integer);
begin
if ber<2 then
begin
	bar.Buttons[0].Visible:=false; //new
	bar.Buttons[2].Visible:=false; //delete
end;
if ber<1 then
begin
	bar.Buttons[1].Visible:=false;  //edit
	bar.Buttons[3].Visible:=false;  //save
	bar.Buttons[4].Visible:=false;  //cancel
end;
end;
                                     
procedure TForm_main.Edit_groesseChange(Sender: TObject);
var
bmi,gr,ge,bu,whtr:real;
begin

 //Edit_befChange(sender);
 edit_bef_check(sender);
 try
   gr:=strtofloat(edit_groesse.text);
   ge:=strtofloat(edit_gewicht.text);
   bu:=strtofloat(edit_bauchumfang.text);

   if gr>0 then whtr:=bu/gr else whtr:=0;
   edit_whtr.Text:=floattostrf(whtr,ffFixed ,5,2);

   if gr>3 then gr:=gr/100;//in meter
   if gr>0 then bmi:=ge/(gr*gr) else bmi:=0;
   edit_bmi.Text:=floattostrf(bmi,fffixed,5,2);
   radiogroup_auffaellig.tag:=1;
 except
 end;
end;

procedure TForm_main.Edit_fev1Change(Sender: TObject);
var
fev,fvc,q:real;
begin
 radiogroup_auffaellig.tag:=1;
 //Edit_befChange(sender);
 edit_bef_check(sender);
 TRY
   fev:=strtofloat(edit_fev1.text);
   fvc:=strtofloat(edit_fvc.text);
   if fvc=0 then exit;
   q:=(fev / fvc)*100;
   edit_fev_fvc.text:=floattostr(q);
  except
  end;
end;



procedure TForm_main.SpeedButton_haClick(Sender: TObject);
var
k:pkontaktpointer;
ha:string;
ind:int64;
begin
	ind:=getbigint(datamodul.q_mitarbeiter,'i_hausarzt');
	k:=aerzteanzeigen(true,ind);
  ha:=k.titel+' '+k.name+', '+ k.strasse+', '+k.plz+' '+k.ort;
  dispose(k);
	datamodul.q_mitarbeiter.edit;
	if ha<>'' then
	begin
	 setbigint( datamodul.q_mitarbeiter,'i_hausarzt',ind);
	 datamodul.q_mitarbeiter['s_hausarzt']:=ha;
	end;
end;

procedure TForm_main.ha_einfuegen;
var
k:pkontaktpointer;
ha,t:string;
ind:int64;
i:integer;
begin
	ind:=getbigint(datamodul.q_mitarbeiter,'i_hausarzt');
	k:=aerzteanzeigen(true,ind);
  ha:=k.titel+' '+k.name+', '+ k.strasse+', '+k.plz+' '+k.ort;


	Form_checkbox:=TForm_checkbox.Create(self);
  if k.p_name<>'' then form_checkbox.CheckListBox.AddItem(k.p_name ,nil);
  if k.fach <>'' then form_checkbox.CheckListBox.AddItem(k.fach,nil);
  if k.zhd <>'' then form_checkbox.CheckListBox.AddItem(k.zhd ,nil);
  if k.titel  <>'' then form_checkbox.CheckListBox.AddItem(k.titel ,nil);
  if k.vorname <>'' then form_checkbox.CheckListBox.AddItem(k.vorname,nil);
  if k.name <>'' then form_checkbox.CheckListBox.AddItem(k.name,nil);
  if k.strasse <>'' then form_checkbox.CheckListBox.AddItem(k.strasse,nil);
  if k.plz <>'' then form_checkbox.CheckListBox.AddItem(k.plz,nil);
  if k.ort <>'' then form_checkbox.CheckListBox.AddItem(k.ort,nil);
  if k.email <>'' then form_checkbox.CheckListBox.AddItem(k.email ,nil);
  if k.telefon <>'' then form_checkbox.CheckListBox.AddItem(k.telefon ,nil);
  if k.fax <>'' then form_checkbox.CheckListBox.AddItem(k.fax,nil);
  if k.www <>'' then form_checkbox.CheckListBox.AddItem(k.www ,nil);
  if k.memo <>'' then form_checkbox.CheckListBox.AddItem(k.memo,nil);
  if form_checkbox.CheckListBox.Items.Count>0 then form_checkbox.CheckListBox.Checked[0]:=true;
  if form_checkbox.ShowModal=mrok then
  begin
     for i:=0 to form_checkbox.CheckListBox.count-1 do
     if  form_checkbox.CheckListBox.Checked[i] then  t:=t+ form_checkbox.CheckListBox.items.Strings[i]+#10;
     t:=copy(t,1,length(t)-1);
     com.texteinfuegen(t,false);
     if form_main.arbeitsmedizin_Proband.State in [osopen, osrunning] then oleanzeigen(form_main.arbeitsmedizin_Proband);//form_main.arbeitsmedizin_Proband.DoVerb(ovshow);// form_main.olecontainer_anzeigen(form_main.arbeitsmedizin_Proband);
     if form_main.arbeitsmedizin_firma.State in [osopen, osrunning] then oleanzeigen(form_main.arbeitsmedizin_firma); //form_main.arbeitsmedizin_firma.DoVerb(ovshow); //form_main.olecontainer_anzeigen(form_main.arbeitsmedizin_firma);
  end;
  form_checkbox.Release;
  dispose(k);
end;




procedure TForm_main.pagecontrol_mainDrawTab(Control: TCustomTabControl;
	TabIndex: Integer; const Rect: TRect; Active: Boolean);
	var
	l,o,bi:integer;
	wort:string;
	bild:tbitmap;
begin
bild:=tbitmap.Create;
//if not tabsheet_anamnese.TabVisible then  //tabindex bezieht sich nur auf sichtbare
begin
	//if tabindex=2 then tabindex:=3;
end;


case prg_typ of //if (prg_typ ='a') or (prg_typ ='s') then
1,2:begin
    case tabindex of
      0:	begin
         wort:='Firmen';
         bi:=0;
         end;
      1:begin
         wort:='Abteilungen';
         bi:=7;
        end;
      2:begin
         wort:='Probanden';
         bi:=1;
        end;
      3:begin
         wort:='Kartei';
         bi:=2;
        end;
    end;
  end;
3:  //fusspflege
  begin
    case tabindex of
      0:	begin
         wort:='Kunden-Gruppe';
         bi:=10;
         end;
      1:begin
         wort:='Kunde';
         bi:=11;
        end;

      2:begin
         wort:='Kartei';
         bi:=15;
        end;
    end;
  end;
4:begin
    case tabindex of
      0:	begin
         wort:='Abteilung';
         bi:=10;
         end;
      1:begin
         wort:='Patient';
         bi:=11;
        end;

      2:begin
         wort:='Kartei';
         bi:=15;
        end;
    end;
  end;

end;


imagelist_tabs.GetBitmap(bi,bild);
l:=rect.Left;
o:=rect.Top;
if active then control.canvas.Brush.Color:=clwhite else control.canvas.Brush.Color:=clBtnFace ;
//control.canvas.pen.Color:=clred;
control.canvas.FillRect(rect);
control.Canvas.Draw(l+2,o+2,bild);     //+4, +3
control.canvas.pen.Color:=clblack;
control.Canvas.TextOut(l+40,o+6,wort);
bild.Free;
end;


procedure TForm_main.PageControl_firmaDrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
	var
	l,o,bi,ti:integer;
	wort:string;
	bild:tbitmap;
begin
bild:=tbitmap.Create;
//if not tabsheet_anamnese.TabVisible then  //tabindex bezieht sich nur auf sichtbare
begin
	//if tabindex=2 then tabindex:=3;
end;
ti:=tabindex;

case prg_typ of //if (prg_typ ='a') or (prg_typ ='s') then
1,2:begin
    case tabindex of
      0:	begin
         wort:='Adresse';
         bi:=0;
         end;
      1:begin
         wort:='Kriterien';
         bi:=1;
        end;
      2:begin
         wort:='Kontakte';
         bi:=2;
        end;
      3:begin
         wort:='Dokumente';
         bi:=3;
        end;
      4:begin
         wort:='Zeiterfassung';
         bi:=4;
        end;
     5:begin
         wort:='Rechnungen';
         bi:=5;
        end;
     6:begin
         wort:='Einstellungen';
         bi:=6;
        end;
    end;
  end;
3:  //fusspflege
  begin

  end;
4:begin

  end;

end;

      if ti=0 then	begin
         wort:='Adresse';
         bi:=0;
         end;
      if ti=1 then begin
         wort:='Kriterien';
         bi:=1;


        end;
      if not tabsheet_firma_filter.TabVisible then inc(ti);
      if ti=2 then begin
         wort:='Kontakte';
         bi:=2;

        end;
      if not tabsheet_ansprechpartner.TabVisible then inc(ti);
      if ti=3 then begin
         wort:='Dokumente';
         bi:=3;

        end;
      if not tabsheet_firma_texte.TabVisible then inc(ti);
      if ti=4 then begin
         wort:='Zeiterfassung';
         bi:=4;

        end;
     if not tabsheet_sollist.TabVisible then inc(ti);
     if ti=5 then begin
         wort:='Rechnungen';
         bi:=5;

        end;
     if not tabsheet_rechnungen.TabVisible then inc(ti);
     if ti=6 then begin
         wort:='Einstellungen';
         bi:=6;
         if not tabsheet_firma_einstellungen.TabVisible then inc(ti);
        end;
//wort:=pagecontrol_firma.Pages[tabindex].Caption;

imagelist_tabs_firma.GetBitmap(bi,bild);
l:=rect.Left;
o:=rect.Top;
if active then control.canvas.Brush.Color:=clwhite else control.canvas.Brush.Color:=clBtnFace ;
//control.canvas.pen.Color:=clred;
control.canvas.FillRect(rect);
control.Canvas.Draw(l+2,o+2,bild);     //+4, +3
control.canvas.pen.Color:=clblack;
control.Canvas.TextOut(l+36,o+5,wort);
bild.Free;
end;


procedure TForm_main.Arbeitsmedizin_BefundLButtonDown(Sender: TObject);
var
w_name:string;
begin
w_name:=vorlagenverzeichnis+'\befund.doc';
scrollbox_ole_befund.tag:=1;
if arbeitsmedizin_befund.State in [osEmpty] then
 begin
	if fileexists(w_name) then
		Arbeitsmedizin_befund.CreateObjectFromFile(w_name,false)
	else
	  arbeitsmedizin_befund.CreateObject('Word.Document',false);
 end;
try
  oleanzeigen(arbeitsmedizin_befund);
	//arbeitsmedizin_befund.DoVerb(ovshow);
	//result:=com.wordobjekterstellen(olecontainerext_be);
	except
	showmessage('Bitte zur Anwendung �ber die Task-Leiste wechseln');
	end;
end;

procedure TForm_main.Info1Click(Sender: TObject);
var query,s1:string;
begin
	query:='select ln from haupt_tabelle where haupt=1';
	 s1:=mysql_d.Feldinhalt( query,0);
	form_start:=tform_start.create(self);
	try
	  form_start.notebook.PageIndex:=1;
	  form_start.memo.Lines.Add(Application.title+' '+version);
	  {form_start.memo.Lines.Add('Lizenznehmer: '+s1);
	  if max_mitarbeiter=0 then   form_start.memo.Lines.Add('Lizenz f�r unlimitierte Probanden-Zahl')
	  else                        form_start.memo.Lines.Add('Lizenz f�r '+inttostr(max_mitarbeiter)+' Probanden');
    if max_mitarbeiter=50 then form_start.memo.Lines.Add('Demoversion');
	  if mehrplatz then s1:='JA' else S1:='NEIN';
	  form_start.memo.Lines.Add('Mehrplatzsystem freigeschaltet: '+s1);
	  if verteiltesysteme then s1:='JA' else S1:='NEIN';
	  form_start.memo.Lines.Add('Satelliten freigeschaltet: '+s1);}

	  {if show_Impfung then s1:='JA' else S1:='NEIN';
	  form_start.memo.Lines.Add('Impfungen freigeschaltet: '+s1);
	  if show_labor then s1:='JA' else S1:='NEIN';
	  form_start.memo.Lines.Add('Labor freigeschaltet: '+s1);
	  if show_gefaehrdungen then s1:='JA' else S1:='NEIN';
    form_start.memo.Lines.Add('Arbeitsplatz/Gef�hrdungen freigeschaltet: '+s1);}

	  form_start.ShowModal;
	finally
	  form_start.Release;
	end;
end;

procedure falle(info:string);
begin

end;


procedure TForm_main.m_registrierenClick(Sender: TObject);
var
ende:boolean;
begin
	dbspeichern(false);
	formular_sperren;
	form_registrierung:=tform_registrierung.create(self);
	try
	if form_registrierung.ShowModal=mrok then ende:=true else ende :=false;
	finally
	form_registrierung.Release;
	Arbeitsmedizin.DestroyObject;

	end;
  baum_sichtbarkeiten(true);
  if ende then form_main.close;
end;

{
procedure tform_main.lizenztest 1.89;
var
s1,k1,k2,query:string;
z:integer;
felder: array [0..1] of string;
neu:boolean;
begin
 //max_mitarbeiter:=0;
 z:=0;
 max_mitarbeiter:=50;
 //m_lizenz.Visible:=false;// wieder l�schen
 query:='select hs1,hs2 from haupt_tabelle';
 mysql_d.Feldarray( query,[0,1],felder);
 k1:=string_entschluesseln( felder[0]);
 k2:=string_entschluesseln(felder[1]);
 if copy(felder[1],1,3)='Arb' then neu:=true else neu :=false;
 if k1='' then
	  begin
			randomize(); //schl�ssel erzeugen
			while z< 1000000 do z:=random(9999999);
			//s1:=ciphermanager.EncodeString(inttostr(z));
        s1:=string_verschluesseln(inttostr(z));
			query:=format('update haupt_tabelle set hs1="%s" where haupt=1',[s1]); //zufallszahl
			mysql_d.sql_exe(query);
			exit;
	  end;
	if k2='' then exit;
  if neu then
  begin
    if copy(k2,2,7)<>k1 then
      begin
         showmessage('Schl�ssel-Kombination fehlerhaft, bitte neu beantragen.');
         exit;
      end;
    z:=strtoint(copy(k2,1,1));
  end
  else
  begin   //alt
    if copy(k2,1,7)<>k1 then
    begin
       showmessage('Schl�ssel-Kombination fehlerhaft, bitte neu beantragen.');
       exit;
    end;
    z:=strtoint(copy(k2,8,1));
  end;
  m_lizenz.Visible:=false;

	case z of
	1:max_mitarbeiter:=300;
	2:max_mitarbeiter:=600;
	3:max_mitarbeiter:=1200;
	4:max_mitarbeiter:=0;
	end;

end;
}

function tform_main.lizenztest:boolean;

var
s,s1,k1,k2,query,hd,hv:string;
z,a,nutzerzahl:integer;
felder: array [0..1] of string;
neu:boolean;
	reg: Tregistry;
begin
result:=true;
//rechner-ID
try
	reg := Tregistry.create;
	reg.RootKey:=hkey_current_user;
	if prg_typ=2 then
	reg.openkey('software\stock-informatik\zeus\',true)
	else reg.openkey('software\Klawitter\Arbene\',true);
	try
		pc_id:=reg.ReadInteger('inst_dat');
	except
		pc_id:=round(now()*1000);
		reg.writeinteger('inst_dat',pc_id);
	end;
finally
	reg.CloseKey;
	reg.Free;
end;

 nutzerzahl:=pcid('schreib'); //schreiben der ID in die Tabelle pcid


 //max_mitarbeiter:=0;
 z:=0;  //f�r die neuen lizenzen ab horus , arbene 1.90 nur noch entweder oder
 a:=0;  //alles davor
 max_mitarbeiter:=0;
 show_ambulanz:=true;
 show_impfung:=true;
 show_labor:=true;
 show_gefaehrdungen:=true;
 verteiltesysteme:=true; //satelliten
 mehrplatz:=true;   //mehrplatz-einplatz

 case prg_typ of
 1:begin
		mehrplatz:=true;
		show_gefaehrdungen:=true;
	end;
 2:  begin
			show_gefaehrdungen:=true;
			show_ambulanz:=false;
		end;
 end;
 //m_lizenz.Visible:=false;// wieder l�schen
 query:='select hs1,hs2 from haupt_tabelle';

 mysql_d.Feldarray( query,[0,1],felder);
 k1:=string_entschluesseln( felder[0]);
 k2:=string_entschluesseln(felder[1]);
 if copy(felder[1],1,3)='Arb' then neu:=true else neu :=false;
 if k1='' then  //erster Programmstart
	  begin
			verteiltesysteme:=true;
			try
				hv:=trim(GetIdeDiskSerialNumber);
				hd:=copy(hv,1,3)+stringinvert(hv);
			except
				hd:='';
			end;

			if length(hd)<=7 then
			begin
			  randomize(); //schl�ssel erzeugen
			  while z< 1000000 do z:=random(9999999);
			  s1:=string_verschluesseln(inttostr(z));
			end
			else
			begin
				hd:=copy(hd,1,7);
				s1:=string_verschluesseln(hd);
			end;


			query:=format('update haupt_tabelle set hs1="%s" where haupt=1',[s1]); //zufallszahl
			mysql_d.sql_exe(query);
			baum_sichtbarkeiten(true);
			exit;
	  end;

	{if k2='' then // micht registriert
	begin
	 verteiltesysteme:=true;

	 if ((prg_typ=2) and (nutzerzahl>2)) then
	 begin
		showmessage('Sie d�rfen auf maximal zwei PCs arbeiten');
		result:=false;
	 end;

	 if prg_typ=4 then
	 begin
		 m_lizenz.Visible:=false;
		 max_mitarbeiter:=0;
	 end;
	 exit; //alles weitere nur durchlaufen wenn registriert
	end;}

	if not((copy(k2,3,7)=k1) or (copy(k2,4,7)=k1)) then  //irgend was altes
		begin
			 if copy(k2,2,7)=k1 then // alter schl�ssel 2.version
			 begin
			  a:=strtoint(copy(k2,1,1));
			 end;

			 if copy(k2,1,7)=k1 then //alter schl�ssel 1. version
			 begin
				a:=strtoint(copy(k2,8,1));
			 end;
			 a:=4; //immer vollizenz
			 {if a=0 then //weder noch
			 begin
				randomize(); //schl�ssel erzeugen
				while z< 1000000 do z:=random(9999999);
				query:=format('update haupt_tabelle set hs1="%s" where haupt=1',[s1]); //zufallszahl
				mysql_d.sql_exe(query);
				query:='update haupt_tabelle set hs2="" where haupt=1';
				mysql_d.sql_exe(query);
				showmessage('Schl�ssel-Kombination fehlerhaft, bitte neu beantragen. Ein neuer Programmschl�ssel wurde erzeugt.');
				result:=false;
				exit;
			 end;}
		 end;

	m_lizenz.Visible:=false;



	case a of
	  1:max_mitarbeiter:=300;
	  2:max_mitarbeiter:=600;
	  3:max_mitarbeiter:=1200;
	  4:max_mitarbeiter:=0;  //unendlich viele
	end;

	if a>0 then z:=15 else
	begin
	 if (copy(k2,3,7)=k1) then z:=strtoint(copy(k2,11,3))    // ab neuem Schl�sselprogramm mit mail
		else z:=strtoint('$'+(copy(k2,11,4)));
	end;


	if z and 128=128 then max_mitarbeiter:=300;
	if z and 64=64 then max_mitarbeiter:=600;
	if z and 32=32 then max_mitarbeiter:=1200;
	if z and 16=16 then max_mitarbeiter:=0;
	if z and 8=8 then   show_ambulanz:=true; //nicht mehr n�tig
	if z and 4=4  then  show_impfung:=true; //nicht mehr n�tig
	if z and 2=2  then  verteiltesysteme:=true; //satelliten
	if z and 1=1  then  mehrplatz:=true;   //mehrplatz-einplatz

	//alles erlaubt
	max_mitarbeiter:=0;
	show_ambulanz:=true;
  show_impfung:=true;
	verteiltesysteme:=true;
	mehrplatz:=true;
	{if ((prg_typ=2) and mehrplatz and (nutzerzahl>2)) then
	begin
		showmessage('Sie d�rfen auf maximal zwei PCs arbeiten');
		result:=false;
	end;
	if ((prg_typ=2) and (not mehrplatz) and (nutzerzahl>1)) then
	begin
		showmessage('Sie d�rfen auf maximal einem PC arbeiten');
		result:=false;
	end;}



end;


function tform_main.nicht_satellit_tab:boolean;
begin
	result:=false;
	if system_modus=0 then
	begin
		showmessage('Ver�nderungen m�ssen auf dem Haupt-System durchgef�hrt werden.');
		result:=true;
	end;
end;


function nicht_satellit(hinweis:string):boolean;
begin
	result:=false;
	if system_modus=0 then
	begin
		showmessage(hinweis);
		result:=true;
	end;
end;


function ist_hauptsystem:boolean;
begin
	result:=false;
	if system_modus=1 then
	begin
		result:=true;
	end;
end;

procedure TForm_main.BitBtn_taetigkeitClick(Sender: TObject);
var i_branche:integer;
    si_taetigkeit:string;
begin
//if not b.b_string('vorlagen') then exit;

i_branche:=datamodul.q_firma['i_branche'];
form_tabellen:=tform_tabellen.Create(self);
if b.b_string('proband-Listen')=-1 then
begin
	form_tabellen.ToolBar.visible:=false;
end;
   form_tabellen.toolbar.visible:=false;
   form_tabellen.BitBtn_laden.Visible:=false;
///dieser code an zwei stellen !!!!!!!!!!!!!!!!!!!!!!!!!
	form_tabellen.Panel_b.visible:=true;
  form_tabellen.toolbutton_quit.Enabled:=false ;
	form_tabellen.BitBtn_taetig_lesen.Visible:=false;
  si_taetigkeit:=getbigint_str(datamodul.q_mitarbeiter,'i_taetigkeit');
	datamodul.sql_new(true,datamodul.q_1,format('select * from taetigkeiten where i_branche in (0,%d) ',[i_branche]),'taetigkeit');

	if si_taetigkeit<>'0' then	datamodul.q_1.Locate('nummer',si_taetigkeit,[loCaseInsensitive]);

	form_tabellen.suchfeld:='taetigkeit';
  form_tabellen.Panel_suchen.Visible:=true;
	form_tabellen.Notebook.PageIndex:=9;
	form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='Taetigkeit';
	form_tabellen.DBGrid_tabellen.Columns[0].width:=200;
   form_tabellen.DBGrid_tabellen.Columns[1].width:=0;
	form_tabellen.DBEdit_taetigkeit.DataField:='taetigkeit';
	form_tabellen.DBEdit_einsatzzeit.DataField:='einsatzzeit';
  form_tabellen.DBCombo_taet_branche.DataField:='i_branche';
  form_tabellen.DBCombo_taet_branche.lookup_laden;
	grid_einstellen(form_tabellen.dbgrid_tabellen);


if form_tabellen.showmodal=mrok then
begin
	 datamodul.q_taetigkeiten.refresh;
	 datamodul.q_mitarbeiter.edit;
	 setbigint(datamodul.q_mitarbeiter,'i_taetigkeit',getbigint(datamodul.q_1,'nummer'));
    taetigkeitanzeigen;
end;


form_tabellen.release;

end;

procedure TForm_main.import_excelClick(Sender: TObject);
begin
	if  b.b_string('Importieren')=-1 then exit;

	dbspeichern(false);
	form_importieren:=tform_importieren.create(self);
  form_importieren.excel_ueberpruefen;
	form_importieren.showmodal;
	form_importieren.release;
  refresh_all;
	pagecontrol_main.ActivePage:=tabsheet_firma;
	berechtigung_firma;
	//datamodul.q_firma.refresh;
	sichtbarkeiten_einstellen;
	berechtigungen_einstellen;
end;

procedure TForm_main.m_LaborLDTClick(Sender: TObject);
var
allowchange,se:boolean;
mr:integer;
begin
  se:=m_erweitertefehlermeldungen.Checked;
  if se then showmessage('ldt-import wurde ausgew�hlt');

  PageControl_mainChanging(Sender,allowchange );
  if not allowchange then
  begin
    showmessage('aktuell ist kein Import m�glich');
    exit;
  end;
  if form_main.nicht_satellit_tab then
  begin
    if se then showmessage('satellit');
    exit;
  end;
  speichern;
  pagecontrol_main.ActivePage:=tabsheet_stammdaten;
  if se then showmessage('tabsheet stammdaten gewechselt');
  form_ldt:=tform_ldt.create(self);
  if se then showmessage('form_ldt wurde erzeugt');
  form_ldt.HelpContext:=40;

  try
    if se then showmessage('ldt_was=<'+ldt_was+'>');
    form_ldt.ComboBox_was.ItemIndex:=strtoint(ldt_was);
    if se then showmessage('ldt_vor=<'+ldt_vor+'>');
    form_ldt.SpinEdit_vor.Text:=ldt_vor;
    if se then showmessage('ldt_nach=<'+ldt_nach+'>');
    form_ldt.SpinEdit_nach.Text:=ldt_nach;
  except
    form_ldt.ComboBox_was.ItemIndex:=0;
    form_ldt.SpinEdit_nach.Text:='14';
    form_ldt.SpinEdit_vor.Text:='14';
  end;
  if se then showmessage('form_ldt wird jetzt angezeigt');
  application.ProcessMessages;
  mr:=form_ldt.showmodal;
  if se then showmessage('r�ckgabewert modalresult: '+inttostr(mr) );
  application.ProcessMessages;
  if se then showmessage('form_ldt wurde angezeigt und wieder geschlossen');
  ldt_was:=inttostr( form_ldt.ComboBox_was.ItemIndex);
  ldt_vor:=form_ldt.SpinEdit_vor.Text;
  ldt_nach:=form_ldt.SpinEdit_nach.Text;
  form_ldt.release;
  if se then showmessage('form_ldt wurde freigegeben');
  datamodul.q_labor.Refresh;
  if se then showmessage('ende routine ldt-import');
end;

function tform_main.umlaut_convert(text:string):string;
var j:integer;
a1,a2: array[1..7] of string;
begin
   a1[1]:=chr(132);
   a2[1]:='�';
   a1[2]:=chr(148);
   a2[2]:='�';
	 a1[3]:=chr(129);
   a2[3]:='�';
	 a1[4]:=chr(153);
   a2[4]:='�';
   a1[5]:=chr(154);
   a2[5]:='�';
   a1[6]:=chr(142);
	 a2[6]:='�';
	 a1[7]:='�';
	 a2[7]:='�';

	 for j:=1 to 7 do
	 begin
		text:=stringreplace(text,a1[j],a2[j],[rfReplaceAll]);
	 end;
	 result:=text;
end;

function tform_main.kleine_aenderungen:boolean;
var
query:string;

begin
    //query:='UPDATE export_import SET main=1 WHERE  tab_name="beurteilung"';
	  //result:=mysql_d.sql_exe(query);
		//if not result then showmessage(query+' wurde nicht ausgef�hrt');
end;

function tform_main.neue_felder_abfragen_once:boolean;
 var
     f:textfile;
     s,dat,tabelle,sql_nummer,sql_query,query,query2,tab_name,struktur:string;
     i,p,lastupdate:integer;
     b,first,angepasst:boolean;
     t:ttime;
 begin
   result:=true;
   first:=true;
    
      dat:=wurzelpfad(application.ExeName)+'\change_db_individuell.txt';
     if not fileexists(dat) then exit;
 try
     AssignFile(F,dat );   { Datei ausgew?hlt }
     Reset(F);
     readln(f,s);
     s:='Soll die inidividuelle Datenbankanpassung durchgef�hrt werden: '+s;
     angepasst:=false;
     if Messagedlg(s,mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;

     if not sicherung(true) then
     begin
      if messagedlg('Es wurde kein Backup erzeugt - trotzdem fortfahren',mtConfirmation, mbOkCancel	,0)<>mrOK then exit; 
     end;

     while not eof(f) do
     begin
         Readln(F, S);
         if copy(s,1,2)='//' then continue; //kommentar
          p:=pos('|',s);
         if p=0 then continue; //leer
          sql_nummer:=copy(s,1,p-1);
         s:=copy(s,p+1,length(s));
         sql_query:=s;
         b:=false;
         b:=mysql_d.sql_exe(sql_query);
         
				if not b then showmessage(sql_query+' wurde nicht ausgef�hrt');
        protokoll('sys','dbupdate_inidi',sql_query,b);
     end;
     angepasst:=true;
 finally
       CloseFile(F);
     if angepasst and fileexists(dat) then deletefile(dat);
 end;
 end;




function tform_main.neue_felder_abfragen:boolean;
var
	f,f1:textfile;
	s,dat,tabelle,sql_nummer,sql_query,query,query2,tab_name,struktur,s1,s3,s4:string;
	i,p,lastupdate:integer;
	b,first:boolean;
	t:ttime;
  liste:tstringlist;
  stream:tstream;
begin
  result:=true;
  first:=true;
	query:='select i1 from haupt_tabelle';

	lastupdate:=strtoint(mysql_d.Feldinhalt(query,0));


  dat:=wurzelpfad(application.ExeName)+'\change_db.txt';
	if not fileexists(dat) then exit;
try
	AssignFile(F,dat );   { Datei ausgew�hlt }
	Reset(F);
	while not eof(f) do
	begin
		Readln(F, S);
		if copy(s,1,2)='//' then continue; //kommentar

		p:=pos('|',s);
		if p=0 then continue; //leer

		sql_nummer:=copy(s,1,p-1);
		s:=copy(s,p+1,length(s));

		sql_query:=s;
      if (idb_protocol=1) then //mariadb
            begin
                sql_query:=stringreplace(sql_query,'timestamp(14)','TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',[rfreplaceall]);
            end;
		if lastupdate<strtoint(sql_nummer) then
		try

			if first then   //erster Durchlauf
			begin
			  b:=	mysql_d.sql_exe('select * from goae');
			  if not b then showmessage('Tabelle GOAE nicht vorhanden, bitte downloaden und in das Verzeichnis c:\mariadb55\data\arbene kopieren');
			  datamodul.connection_main.Disconnect;

			  //showmessage('Datenbank wird aktualisiert, bitte alle anderen Arbene-Anwendungen schlie�en.');

			  showmessage('Die Datenbank wird aktualisiert, bitte best�tigen Sie die folgenden Schritte.'+chr(13)+'Erstellen Sie gegebenenfalls ein Backup'
                    +chr(13)+'Die Aktualisierung kann eventuell l�nger dauern.');

        if not sicherung(true) then
       begin
        if messagedlg('Es wurde kein Backup erzeugt - trotzdem fortfahren',mtConfirmation, mbOkCancel	,0)<>mrOK then
        begin
          result:=false;
          exit;
        end;
       end;

            {if  (Messagedlg('Besteht ein aktuelles Backup der Datenbank?',mtConfirmation, [mbyes, mbNo] ,0)<>mryes ) then
              begin
              	showmessage('Die Anwendung wird beendet.');
              	result:=false;
              	exit;

              end; }
            // backup_durchf:=true;
            //  sicherung;

			  if  (Messagedlg('Soll Arbene jetzt die Datenbank aktualisieren?',mtConfirmation, [mbyes, mbNo] ,0)<>mryes ) then
              begin
              	showmessage('Die Anwendung wird beendet.');
              	result:=false;
              	exit;
              end;



			  if not db_connect() then
			  begin

				showmessage('MySql-Datenbank wurde vermutlich nicht gestartet');

           if not db_connect then
			  begin
				  showmessage('Datenbank kann nicht ge�ffnet werden.(Mysql ist nicht gestartet oder falsche IP-Adresse 2)'+#10+'Standardeinstellung'+#10+'  Host: localhost'+#10+'  Datenbank: arbene');
				  result:=false;
				  exit;
			  end;
            end;
			  first:=false;
			end;
         //showmessage ('Die Datenbank wird aktualisiert.'+chr(13)+sql_query);
         

        form_start.StatusBar.panels[0].text:='Aktualisierung l�uft, bitte warten. Schritt: ' +sql_nummer;
        application.ProcessMessages;

        if copy(sql_query,1,1)='*' then //besondere befehle
        begin
          if copy(sql_query,2,3)='all' then //gilt f�r alle tabs
          begin
             datamodul. sql_new(false,datamodul.q_1,'select tab_name from export_import ','');
             datamodul.q_1.first;
             sql_query:=copy(sql_query,5,length(sql_query));
             while not datamodul.q_1.eof do
             begin
              try
               tab_name:=datamodul.q_1['tab_name'];
               query2:=format(sql_query,[tab_name]);
               mysql_d.sql_exe_still(query2);
               datamodul.q_1.next;
              except
              end;
             end;
             datamodul.q_1.close;
             protokoll('sys','dbupdate',sql_query,true);
          end;

          if copy(sql_query,2,3)='rep' then   datamodul.reptable;

          if copy(sql_query,2,3)='cvc' then //alle varcar to char
          begin
             datamodul.sql_new(false,datamodul.q_1,'select tab_name from export_import ','');
             datamodul.q_1.first;
             while not datamodul.q_1.eof do
             begin
              try
               tab_name:=datamodul.q_1['tab_name'];
               varchar_ex(tab_name);
              	except
              	end;
              datamodul.q_1.next;
             end;
             datamodul.q_1.close;
             protokoll('sys','dbupdate',sql_query,true);
          end;

          if copy(sql_query,2,3)='nkr' then  //nummernkreis neu lesen
          begin

               form_main.aktualisiere_nummern_kreis_aktuell;

               protokoll('sys','dbupdate',sql_query,true);
          end;

          if copy(sql_query,2,3)='abt' then  //abteilungen neu lesen
          begin
               //abteilungskuerzel_eintragen;
          end;

          if copy(sql_query,2,3)='f01' then // rechungsnummern
          begin
            with datamodul do
            begin
               query:='select * from re_positionen';
               datamodul.sql_new( false,datamodul.q_3,query,'');
               query:='select * from rechnung';
               datamodul.sql_new(false,datamodul.q_4,query,'');
                 q_3.First;
                 while not q_3.eof do
                 begin
                  s:=q_3.findfield('renu').asstring;
                    if q_4.Locate('renu',s,[]) then
                    begin
                       q_3.Edit;
                       q_3.FindField('i_rechnung').asstring:=q_4.findfield('nummer').asstring;
                       q_3.Post;
                    end;
                    q_3.Next;
                 end;
            end;
            protokoll('sys','dbupdate','f01',true);
          end;

          if copy(sql_query,2,3)='f02' then //  firma_filter_vorlage
          with datamodul do
          begin
          	query:='delete from  firma_filter_vorlage where name="Wagner" and reihenfolge=12 ';
            mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Goossen" and reihenfolge=2 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Klawitter" and reihenfolge=8 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Kessler" and reihenfolge=7 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Busch" and reihenfolge=1 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Heimann" and reihenfolge=6 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="M�ller" and reihenfolge=9 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Murawsky" and reihenfolge=11 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="H�rle Kerstin" and reihenfolge=3 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="H�rle Sabine" and reihenfolge=4 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Heilig" and reihenfolge=5 ';
          	mysql_d.sql_exe(query);
            query:='delete from  firma_filter_vorlage where name="Knau�-Mack" and reihenfolge=10 ';
          	mysql_d.sql_exe(query);
            protokoll('sys','dbupdate','f02',true);
          end;

          if copy(sql_query,2,3)='f03' then //  dokumente, texte komprimieren
          with datamodul do
          begin
              dokumente_texte_komprimieren;
              //arbene_update:='f03';
              protokoll('sys','dbupdate','f03',true);
          end;

           if copy(sql_query,2,3)='f04' then //  once -zur?cksetzen
           begin
             query:=format('update haupt_tabelle set s2="%s"',[sql_nummer]);
             b:=mysql_d.sql_exe(query);
             lastupdate:=strtoint(sql_nummer);

             protokoll('sys','dbupdate',sql_query,b);
           end;

           if copy(sql_query,2,3)='f05' then //
           begin
             speichern_fragen:=true;

             dat:=wurzelpfad(application.ExeName)+'\css_format.txt';
             if fileexists(dat) then
             begin
               //query:=format('update haupt_tabelle set t1="%s"',[s]);
               //b:=mysql_d.sql_exe(query);
               lastupdate:=strtoint(sql_nummer);
               try
                liste:=tstringlist.Create;
                liste.LoadFromFile(dat);
                s:=liste.text;
                sql_query:=format('update haupt_tabelle set t1="%s"',[s]);
                b:=mysql_d.sql_exe(sql_query);
               finally
                liste.Free;
               end;


               //deletefile(dat);
               lastupdate:=strtoint(sql_nummer);

               protokoll('sys','dbupdate',sql_query,b);
             end
              else protokoll('sys','dbupdate',dat +'nicht vorhanden',false);
           end;
           //############

           if copy(sql_query,2,3)='f06' then // colate latin1_german2_ci
           BEGIN

               query:='SET foreign_key_checks = 0';
                mysql_d.sql_exe(query);
               s:= db_datenbank;
                query:=format('ALTER DATABASE %s  CHARACTER SET latin1 COLLATE latin1_german2_ci ',[s]);
                mysql_d.sql_exe(query);
               //query:='SELECT table_name FROM  INFORMATION_SCHEMA.TABLES';
               query:=format('SELECT table_name FROM information_schema.tables where table_schema="%s"',[db_datenbank]);
               datamodul.sql(false,datamodul.q_3,query,'');
               while not datamodul.q_3.Eof do
               begin
                 query:='SET foreign_key_checks = 0';
                mysql_d.sql_exe(query);
                 s:=datamodul.q_3.findfield('table_name').asstring;
                 query:=format( 'ALTER TABLE %s.%s CONVERT TO CHARACTER SET latin1 COLLATE latin1_german2_ci ',[db_datenbank,s]);
                 mysql_d.sql_exe(query);
                 datamodul.q_3.next;
                 // f�r alle tabellen, der db db-name
                  {ALTER DATABASE `db-name`
                  DEFAULT CHARACTER SET latin1
                  COLLATE   latin1_german1_ci;

                  ALTER TABLE table CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; }
                end;

           end;
          if copy(sql_query,2,3)='f07' then  // aufbau abr_konto_kriterium
          begin
            //f�r jedes konto alle Kriterien (wie bisher)
            query:='select * from abr_zeitkonto';
            datamodul.sql(false,datamodul.q_3,query,'');
            query:='select * from abr_anlass_krit';
            datamodul.sql(false,datamodul.q_4,query,'');
            datamodul.q_3.first;
            while not datamodul.q_3.Eof do
            begin
              datamodul.q_4.first;
              while not datamodul.q_4.Eof do
              begin

                s3:=datamodul.q_3.findfield('nummer').asstring;
                s4:=datamodul.q_4.findfield('nummer').asstring;
                query:=format ('insert abr_konto_kriterium  set  i_konto="%s",i_kriterium="%s" ',[s3,s4]);
                mysql_d.sql_exe(query);
                datamodul.q_4.Next;
              end;
              datamodul.q_3.Next;
            end;


          end;

          if copy(sql_query,2,3)='f08' then  // m�sste schon ge�ndert sein vorsichtshalber
          begin
            try
            query:='ALTER TABLE texte CHANGE COLUMN time_origin w_dat DATE default "1900-01-01"';
            mysql_d.sql_exe(query);
            except
              //wenn time_origin nicht mehr
            end;
          end;

      end
    else  //sql-queries
		begin
      //##############################
        if (idb_protocol=1) then
        begin
            query:=stringreplace(query,'timestamp(14)','TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',[rfreplaceall]);
        end;

				b:=mysql_d.sql_exe(sql_query);
				if not b then showmessage(sql_query+' wurde nicht ausgef�hrt');

        protokoll('sys','dbupdate',sql_nummer+'|'+sql_query,b);

		end;

    query:=format('update haupt_tabelle set i1="%s"',[sql_nummer]);
		mysql_d.sql_exe(query);
		lastupdate:=strtoint(sql_nummer);
    form_start.StatusBar.panels[0].text:='Aktualisierung abgeschlossen';

		finally
		end;

	end;
finally
	  CloseFile(F);
end;
end;


procedure tform_main.varchar_ex(tab_name:string);
var
   p1,p2,p3,p4,p5,l,i:integer;
   s,s1,s2,s3,s4,feld_name,feld_def1,feld_def2,se,lf,hk:string;
   query:string;
begin
se:=char(96);
lf:=char(10);
hk:=char(39);

s:=mysql_d.create_sql_von_db(tab_name);
l:=length(s);
//tabname
   i:=0;
 {	p1:=pos(se,s);
   s:=copy(s,p1+1,l);
   p1:=pos(se,s);
   tab_name:=copy(s,1,p1);
   s:=copy(s,p1+1,l); }

   query:=format('alter table %s ',[tab_name]);
// feldname und felddef
   while pos('varchar',s)>0 do
   begin
      p1:=pos('varchar',s);
      s1:=copy(s,1,p1);
      s:=copy(s,p1,l);

   	//feldname
      s1:=stringwenden(s1);
      p1:=pos(se,s1);
   	s1:=copy(s1,p1+1,l);
   	p1:=pos(se,s1);
   	feld_name:=copy(s1,1,p1-1);
      feld_name:=stringwenden(feld_name);

      // felddefinition
      p1:=pos(')',s);
      feld_def1:=copy(s,4,p1-3);   //char(xx)
      s:=copy(s,p1+1,l);

      p1:=pos(lf,s);
      feld_def2:=copy(s,1,p1-2);
      s:=copy(s,p1,l);

      if trim(lowercase(feld_def2))='default null' then feld_def2:='NOT NULL default '+hk+hk;
      if i>0 then query:=query+',';
      query:=query+ format(' change %s %s %s %s ',[feld_name, feld_name, feld_def1, feld_def2]);
      inc(i);
   end;
 if i>0 then mysql_d.sql_exe(query);
end;

procedure tform_main.lufu_normwert;
var
	a,h,fvc,fev1,pev:real;
	fvcg,fev1g,fev1fvcg,pevg:real;
	m:boolean;
	query,nr,s_untersuchung,sh:string;
	i,bnr:integer;
	i_untersuchung:int64;
begin
	with datamodul do
	begin
		if q_mitarbeiter['geschlecht']=2 then m:=true else m:=false;
		a:= (date()-q_mitarbeiter['geb_dat'])/364.75;
		i_untersuchung:=getbigint(q_befunde,'nummer');
		s_untersuchung:=inttostr(i_untersuchung);
		//gr�sse-gewicht suchen     modus=6
		query:='select nummer from bereich where modus=6';
		nr:=mysql_d.Feldinhalt(query,0);
		query:=format('select f_1 from akt_untersuchung where i_untersuchung=%s and i_bereich=%s and storno=0',[s_untersuchung,nr]);
		sh:=mysql_d.Feldinhalt(query,0);
		if sh='' then
		begin
		  l_fvc.caption:='';
		  l_fev1.caption:='K�rper-Gr�sse fehlt';
		  l_fev1fvc.caption:='';
		  l_pev.caption:='';
		  l_fvcg.caption:='';
		  l_fev1g.caption:='';
		  l_fev1fvcg.caption:='';
		  l_pevg.caption:='';
		  exit;
		end;
		sh:=decimalseparator_change(sh);

		h:=strtofloat(sh);
		if h=0 then exit;
		if m then
		begin //M�nner
		  if a<=25 then
		  begin
			 fvc:=exp(-10.321+2.1685*ln(h)+0.0655*a-0.001325*a*a);
			 fev1:=exp(-9.280+1.9095*ln(h)+0.0795*a-0.001698*a*a);
			 pev:=exp(-6.189+1.2965*ln(h)+0.1379*a-0.002731*a*a);
			 fvcg:= fvc* exp(-0.140-0.00166*a);
			 fev1g:=fev1*exp(-0.143-0.00158*a);
			 fev1fvcg:=fev1/fvc*exp(-0.132-0.00047*a);
			 pevg:=pev*exp(-0.313-0.00274*a);
			end
			else
			begin
			 fvc:=exp(-9.540+2.1685*ln(h)+0.0030*a-0.000075*a*a);
			 fev1:=exp(-8.240+1.9095*ln(h)-0.0037*a-0.000033*a*a);
			 pev:=exp(-4.548+1.2965*ln(h)+0.0066*a-0.000106*a*a);
			 fvcg:= fvc* exp(-0.140-0.00166*a);
			 fev1g:=fev1*exp(-0.143-0.00158*a);
			 fev1fvcg:=fev1/fvc*exp(-0.132-0.00047*a);
			 pevg:=pev*exp(-0.313-0.00274*a);
			end
		end
		else
		begin //Frauen
		  fvc:=exp(-9.457+2.0966*ln(h)+0.0091*a-0.000152*a*a);
		  fev1:=exp(-8.271+1.875*ln(h)+0.0035*a-0.00013*a*a);
		  pev:=exp(-5.790+1.4902*ln(h)+0.0042*a-0.000082*a*a);
		  fvcg:= fvc* exp(-0.194-0.00112*a);
		  fev1g:=fev1*exp(-0.174-0.00137*a);
		  fev1fvcg:=fev1/fvc*exp(-0.103-0.00059*a);
		  pevg:=pev*exp(-0.291-0.00349*a);
		end;

		l_fvc.caption:=floattostrf(fvc,fffixed,7,2)+' l';
		l_fev1.caption:=floattostrf(fev1,fffixed,7,2)+' l';
		l_fev1fvc.caption:=floattostrf(100*fev1/fvc,fffixed,7,2)+' %';
		l_pev.caption:=floattostrf(pev,fffixed,7,2)+' l/s';

		l_fvcg.caption:=floattostrf(fvcg,fffixed,7,2)+' l';
		l_fev1g.caption:=floattostrf(fev1g,fffixed,7,2)+' l';
		l_fev1fvcg.caption:=floattostrf(100*fev1g/fvcg,fffixed,7,2)+' %';
		l_pevg.caption:=floattostrf(pevg,fffixed,7,2)+' l/s';

	end;
end;


procedure TForm_main.PageControl_befundeChange(Sender: TObject);
begin
if pagecontrol_befunde.ActivePage=TabSheet_be_graph then
begin
	RadioGroup_parameterClick(Sender);

end;
end;

procedure TForm_main.RadioGroup_parameterClick(Sender: TObject);
var
	query,s_mitarbeiter,feldname:string;
	bereich:integer;
	r:real;
begin
with datamodul do
begin
	if pagecontrol_befunde.ActivePage=TabSheet_be_graph then
	begin
	 xygraph_befund.clear;
	 s_mitarbeiter:=getbigint_str(q_mitarbeiter,'nummer');
	 bereich:=q_bereich['nummer'];
	 case datamodul.q_bereich['modus'] of
			0:begin
				  feldname:='';
			  end;
			1:	begin  //Zahl
				  feldname:='wert';
				end;
			2:begin  //liste
				 feldname:='';
				end;
			 3: begin    //audiogramm
				 feldname:='';
				end;
			 4: begin //sehtest
				 feldname:='';
				end;
			  5:begin //blutdruck
				 case radiogroup_parameter.ItemIndex of
					0:feldname:='i_1';
					1:feldname:='i_2';
					2:feldname:='';
					3:feldname:='';
				 end;
				 end;
			  6: begin //gr�sse gewicht
				 case radiogroup_parameter.ItemIndex of
					0:feldname:='f_1';
					1:feldname:='f_2';
					2:feldname:='f_3';
					3:feldname:='';
				 end;
				  end;
			  7: begin //lufu
				  case radiogroup_parameter.ItemIndex of
					0:feldname:='f_1';
					1:feldname:='f_2';
					2:feldname:='f_3';
					3:feldname:='f_4';
				 end;
				end;
           8: begin //wai
              end;
	  end;
	  if feldname='' then exit;

//	  query:=format('select akt_untersuchung.%s as wert from mitarbeiter left join befunde on (befunde.i_mitarbeiter=mitarbeiter.nummer) left join akt_untersuchung  on (akt_untersuchung.i_untersuchung=befunde.nummer) where akt_untersuchung.i_bereich=%i and mitarbeiter.nummer=%s {order by befunde.datum}'

		  query:=format('select datum, %s as wert from mitarbeiter left join befunde on (befunde.i_mitarbeiter=mitarbeiter.nummer) left join akt_untersuchung  on (akt_untersuchung.i_untersuchung=befunde.nummer) where akt_untersuchung.i_bereich=%d and mitarbeiter.nummer=%s '
				 ,[feldname,bereich,s_mitarbeiter]);
		  query:=query+'and akt_untersuchung.storno=0';
		  query:=query+' order by befunde.datum ';
		sql_new(false,q_1,query,'');
	  q_1.first;
	  while not q_1.Eof do
	  begin
		r:=q_1['wert'];
		xygraph_befund.add(datetostr(datamodul.q_1['datum']),r);

		q_1.next;
	  end;
	  xygraph_befund.paint;
	  xygraph_befund.shape_zeichnen;

	end;
end;
end;


procedure tform_main.storno_sql(zeig_st:boolean);
begin
if zeig_st then
begin
	storno:='';
	and_storno:='';
end
else
begin
	storno:=' storno=0';
	and_storno:=' and storno=0';
end;


end;

procedure tform_main.parameter_abfrage;
var i:integer;
	  s:string;
begin
	for I := 1 to ParamCount do
	begin
	  s:=lowercase(ParamStr(I));
	  if s='nm' then mailslot_active:=false  ;
	  if s='h' then prg_typ:=3;
      if s='s' then prg_typ:=2;
      if s='ab' then prg_typ:=4;
      
	end;
end;


procedure TForm_main.BitBtn_f_rechnung_plClick(Sender: TObject);
var summe,cent,renu:int64;
faktor,i:integer;
bm:tbookmark;
grid:tdbgridext;
begin
if ((sender=BitBtn_repositionen_loesch_proband) or (sender=m_p_Positionlschen)  or (sender=BitBtn_repo_dauerhaftentfernen) or (sender=m_p_Positiondauerhaftentfernen)) then grid:=dbgridext_re_positionen else grid :=dbgridext_repo;

if grid.SelectedRows.Count=0 then
begin
  showmessage('Es muss mindestens eine Zeile markiert sein.');
  exit;
end;
if repositionen_verweismarkiert then
  showmessage('Ein Verweis auf eine Rechnung kann nicht gel�scht werden,'+#10+' l�schen Sie die dazugeh�rige Rechnung' )
else
   if Messagedlg('Sollen die markierten Rechnungs-Positionen gel�scht werden?',mtConfirmation, mbOkCancel	,0)=mrOK then
   begin
     with datamodul do
     begin
         for I := 0 to grid.SelectedRows.Count - 1 do
         begin
              q_repositionen.GotoBookmark(Pointer(grid.SelectedRows.Items[I]));
              q_repositionen.findfield('goae_nn').asstring;

                q_repositionen.Edit;
                q_repositionen.FindField('storno').AsInteger:=1;
                if ( (sender=BitBtn_f_rechnung_pdhl) or (sender=m_f_dauerhaftloesch) or (sender=BitBtn_repo_dauerhaftentfernen) or (sender=m_p_Positiondauerhaftentfernen) )then q_repositionen.FindField('dauerloesch').AsInteger:=1;
                q_repositionen.Post;
          end;
          if not q_repositionen.eof then q_repositionen.next else q_repositionen.prior;

        renu:=getbigint(q_repositionen,'i_rechnung');
        rechnung_aktualisieren(renu);
       for I := 0 to grid.SelectedRows.Count - 1 do
       begin
        q_repositionen.Prior;
        if  q_repositionen.bof then exit;
       end;
       rechnung_refresh;
       formular_entsperren(true,false);
     end;
end;
end;



procedure TForm_main.PageControl_rechnungenChanging(Sender: TObject;
	var AllowChange: Boolean);
begin
 dbspeichern(false);
end;



procedure TForm_main.BitBtn_redruckClick(Sender: TObject);
begin
  // rechnung_drucken
	redruck(10);
end;



procedure TForm_main.redruck(dat: integer);
//rechnung drucken
var
ersatz,sbetrag,renu,sstornozeile,snetto,sbrutto,sust,smws,smws_satz,smwb,sgoae,sfaktorbetrag,sfaktor,sdatum,re,re_datum,MWS_Summe_netto,MWS_Summe_brutto,MWSfrei_Summe,re_bestellnummer:string;
re_stundensatz, re_faktor_gewerblich, re_faktor_verwaltung, re_ma_gewerblich,re_ma_verwaltung ,re_intervall,query ,s_rechnung,datname,datname_pdf,renu_storno:string;
db,faktorbetrag,ust,z:integer;
dbnr,summe,i_rechnung:int64;
faktor:real;
netto,brutto:currency;
word_schliessen_re,verberg,redruck_stornorechnung:boolean;
znetto,zbrutto,su_mws_netto,su_mws_brutto,su_mwsfrei,mws_faktor,c_brutto:currency;
r:thtml_report;
f_name,s1,s2,s3:string;

begin
    speichern;
    mws_faktor:=mws/100;
    if (dat=10) and (datamodul.q_rechnung.FindField('i_mitarbeiter').asstring<>'0') then
    begin
      showmessage ('Drucken Sie die Probandenrechnung aus der Kartei.');
       exit;
    end;

    if datamodul.q_rechnung.FindField('i_parent').asinteger>0 then
    begin
      showmessage('Dies ist nur eine Teilrechnung. Drucken Sie die �bergeordnete Rechnung');
      exit;
    end;

    //if Messagedlg('Soll die Rechnung anonymisiert werden?',mtConfirmation, [mbyes,mbno]	,0)=mryes  then anonym :=true else anonym :=false;
    re_bestellnummer:=datamodul.q_firma.findfield('bestellnummer').asstring;   //muss noch nach sql_felder
    renu_storno:=datamodul.q_rechnung.FindField('renu_storno').asstring;

    try

      form_rechnungsgestaltung:=tform_rechnungsgestaltung.create(self);
       if renu_storno<>'' then
       begin
        form_rechnungsgestaltung.CheckBox_stornorechnung.visible:=true;
        form_rechnungsgestaltung.CheckBox_stornorechnung.Checked:=true;
       end;
      form_rechnungsgestaltung.CheckBox_anonymisieren.Checked:=redruck_anonym;
      form_rechnungsgestaltung.CheckBox_goae.Checked:=redruck_goae;
      form_rechnungsgestaltung.CheckBox_impfstoff.Checked:=redruck_impfstoff;
      form_rechnungsgestaltung.CheckBox_labor.Checked:=redruck_labor;

      if form_rechnungsgestaltung.ShowModal<>mrok then exit; ;

      redruck_anonym:=form_rechnungsgestaltung.CheckBox_anonymisieren.Checked;
      redruck_goae:=form_rechnungsgestaltung.CheckBox_goae.Checked;
      redruck_impfstoff:=form_rechnungsgestaltung.CheckBox_impfstoff.Checked;
      redruck_labor :=form_rechnungsgestaltung.CheckBox_labor.Checked;
      redruck_stornorechnung:=form_rechnungsgestaltung.CheckBox_stornorechnung.checked;
    finally
      form_rechnungsgestaltung.Free;
    end;

    smws_satz:=inttostr(mws);
with datamodul do
begin

try
      if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.8', [ pfad_temp]));
	    f_name:=pfad_temp+'re_text.html';
      r:=thtml_report.create(f_name);

      r.css_ini(l_rechnung_format);
      r.t_css_beginn(7,3);

      if prg_typ=3 then //fuss
        ersatz:=str_l_space('             Endpreis',82)+chr(13)
      else
        //ersatz:=str_l_space('Netto  Faktor   USt   Brutto',82)+chr(13);
        ersatz:=str_l_space('Einzelpreis  Faktor USt  Ges-Preis',84)+chr(13);  //�berschrift

      //r.t_css_zeile_header(['class="h1"','class="h2"','class="h3"','class="h4"','class="h5"','class="h6"','class="h7"'],['Datum','GO�','Position','Einzelpreis','Faktor','USt','Ges.Preis']);
      r.t_css_zeile_header(['class="h1"','class="h2"','class="h3"','class="h4"','class="h5"','class="h6"','class="h7"'],
                            [l_rechnung_spalten[0],l_rechnung_spalten[1],l_rechnung_spalten[2],l_rechnung_spalten[3],l_rechnung_spalten[4],l_rechnung_spalten[5],l_rechnung_spalten[6]]);
      summe:=0;
      i_rechnung:=getbigint(q_rechnung,'nummer');

      query:=format('select i_link_rechnung from re_positionen where i_link_rechnung>0 and i_rechnung=%d',[i_rechnung]);
      sql_new(true,q_9,query,'');
      s_rechnung:=inttostr(i_rechnung);
      q_9.First;
      while not q_9.Eof do
      begin
         s_rechnung:=s_rechnung +','+q_9.findfield('i_link_rechnung').asstring;
        q_9.Next;
      end;

      //if redruck_goae or redruck_impfstoff or  redruck_labor then
      //  query:=format('select * from re_positionen where i_rechnung in (%s) and (storno=0) order by haupt_re desc,i_firma, i_mitarbeiter ,ust , db , datum, re_text ',[s_rechnung])
      //else
        query:=format('select * from re_positionen where i_rechnung in (%s) and (storno=0)  order by haupt_re desc,i_firma, i_mitarbeiter , ust, db , datum, re_text ',[s_rechnung]);

      sql_new(true,q_9,query,'');
      q_9.First;

      if kein_datensatz(q_9) then
      begin
        showmessage('keine Rechnungspositionen');
        exit;
      end;

      znetto:=0;
      zbrutto:=0;
      su_mws_netto:=0;
      su_mws_brutto:=0;
      su_mwsfrei:=0;

      while not q_9.eof do
      begin
          s1:='';
          s2:='';
          s3:='';
          ust:=q_9.findfield('ust').AsInteger ;
          sust:= str_l_space(inttostr(ust)+'%',5);
          faktor:=q_9.findfield('faktor').asfloat;
          sfaktor:=str_l_space(floattostrf(faktor,fffixed,5,2),7);
          netto:= myfloattocurr( q_9.findfield('netto').asfloat);
          snetto:=str_l_space(floattostrf(netto,ffcurrency,8,2),11);    //8,2
          brutto:= myfloattocurr(q_9.findfield('brutto').asfloat);
          sbrutto:=str_l_space(floattostrf(brutto,ffcurrency,8,2),11);  //8,2

          sgoae:=q_9.FindField('goae').asstring;

        //if q_9.FindField('goae').asstring<>'' then sgoae:=(q_9['goae']) else sgoae:='';

         db:=q_9.findfield('db').asinteger;
         dbnr:=getbigint(q_9,'dbnr');
        sdatum:='';
        case db of
          0: begin
              sdatum:='';          //verweis
              brutto:=0;
              netto:=0;
             end;
          1: sdatum:='';//sdatum:=datetostr(q_repositionen['datum'])+' ';  pauschale
          2: sdatum:='';//sdatum:=datetostr(q_repositionen['datum'])+' ';  stunden
          3: sdatum:=datetostr(q_9['datum'])+' ';                          //vorsorge
          4: sdatum:=datetostr(q_9['datum'])+' ';                           //befunde  die zur vorsorge geh�ren
          5: sdatum:=datetostr(q_9['datum'])+' ';                           // impfung
          6: sdatum:=datetostr(q_9['datum'])+' ';                           //labor
          7: sdatum:=datetostr(q_9['datum'])+' ';                           //impfstoff
          8: sdatum:=datetostr(q_9['datum'])+' ';                           //artikel
          9: sdatum:=datetostr(q_9['datum'])+' ';  //f�r hinzugef�gte Rechnungspositionen
        end;



        case db of
        0..2: sfaktor:='       ';
        else
            //sfaktor:=' X '+str_l_space(floattostrf(faktor,fffixed,7,2),4);
        end;
        //end case

        verberg:=false;
        case db of
        3..5,9: if redruck_goae then verberg:=true;
        6:    if redruck_labor then verberg:=true;
        7:    if redruck_impfstoff then verberg:=true;
        end;
        if verberg then
        begin
          znetto:=znetto+netto*faktor;
          //znetto:=znetto+brutto-round(brutto*100*ust/100)/100;
          zbrutto:=zbrutto+brutto;
        end;

        if sgoae='' then   // alles was nicht goae ist
          begin
             if  (redruck_anonym and ((db=0)and (dbnr=0) )) then //nur probandenname
               begin
               ersatz:=ersatz+str_r_space('Proband anonymisiert',50);
               //r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],['Proband anonymisiert']);
               s1:='Proband anonymisiert';
               end
             else
               if not verberg then
               begin
                ersatz:=ersatz+str_r_space(sdatum+q_9['re_text'],50);
                //r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[q_9['re_text']]);
                s1:=trim(sdatum);
                s2:='';
                s3:=trim(q_9['re_text']);
                if s1='' then
                begin
                  s1:=trim(q_9['re_text']);
                  s2:='';
                  s3:='';
                end;
               end;
          end
        else
           begin
               if not verberg then
               begin
                ersatz:=ersatz+str_r_space(sdatum+'GO�: '+sgoae,25)+ str_r_space(q_9['re_text'],25);
                s1:=trim(sdatum);
                s2:=trim(sgoae);
                s3:=trim(q_9['re_text']);
                //r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[sdatum,sgoae,re_text]);
               end;
           end;

        if not verberg then
        if ( {(not verberg) and }(brutto<>0) and ( not( (db=0)and (dbnr=1)))) then
            begin
              ersatz:=ersatz+snetto+sfaktor+sust+sbrutto;     //(db=0)and (dbnr=1) zuvor db=9 bei Verweis
              r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[s1,s2,s3,trim(snetto), trim(sfaktor),trim(sust),trim(sbrutto)]);
            end
            else
            begin
              if s2+s3='' then
              r.t_css_zeile(['class="d1_bold"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[s1,s2,s3,'', '','',''])
              else
              if trim(s1+s2+s3)<>'' then
                r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[s1,s2,s3,'', '','','']);
            end;

        if not verberg then  ///alles in der zeile
        begin
          ersatz:=ersatz+chr(13);
        end;  
        //summe:=summe+ trunc(getbigint(q_9,'cent')*faktor);  //hier runden
         summe:=summe+ round(getbigint(q_9,'cent')*faktor);  //hier runden
        if ust>0 then
        begin
          su_mws_netto:=su_mws_netto+myfloattocurr(netto*faktor);
          su_mws_brutto:=su_mws_brutto+brutto;
        end
        else
        begin
           //su_mwsfrei:=su_mwsfrei+myfloattocurr(netto*faktor);
           su_mwsfrei:=su_mwsfrei+brutto;
        end;

        q_9.next;

        if  (zbrutto>0) and  (q_9.Eof or (q_9.findfield('ust').asinteger<>ust)) then   //Gruppenwechsel   bzw eof
        begin
          sfaktor:='       ';//7
          sdatum:='          ';
          snetto:=str_l_space(floattostrf(znetto,ffcurrency,8,2),11);    //8,2
          sust:=sust; //5
          sbrutto:=str_l_space(floattostrf(zbrutto,ffcurrency,8,2),11);  //8,2
          ersatz:=ersatz+str_r_space(sdatum+'Zwischensumme anonymisierte Inhalte',50)+snetto+sfaktor+sust+sbrutto+chr(13);
          r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],
                        ['','','Zwischensumme anonymisierte Inhalte',trim(snetto), '',trim(sust),trim(sbrutto)]);
          zbrutto:=0;
          znetto:=0;
        end;

      end;//while q9

      r.t_css_ende;
finally
  r.free;
end;
      //#######################
        re_datum:=datetostr(q_rechnung['datum']);
        renu:=q_repositionen['renu'];
        //sbrutto:=floattostrf( q_rechnung['brutto'],ffcurrency,12,2);
        //snetto:=floattostrf( q_rechnung['eurobetrag'],ffcurrency,12,2);
        //smwb:= floattostrf( q_rechnung['brutto']-q_rechnung['eurobetrag'],ffcurrency,15,2);
        //smwb:= floattostrf( (strtocurr(q_rechnung['brutto'])-strtocurr(q_rechnung['eurobetrag'])),ffcurrency,15,2);
        MWS_Summe_netto:=str_l_space(floattostrf(su_MWS_netto,ffcurrency,8,2),11);
        //MWS_Summe_brutto:=str_l_space(floattostrf(su_MWS_brutto,ffcurrency,8,2),11);
        MWSfrei_Summe:=str_l_space(floattostrf(su_mwsfrei,ffcurrency,8,2),11); ;

        //mws betr�ge von der Summe
        MWS_Summe_brutto:=floattostrf(su_MWS_netto* (mws_faktor+1),ffcurrency,12,2);
        smwb:=   floattostrf( su_MWS_netto*mws_faktor,ffcurrency,15,2);

        snetto:= floattostrf( su_MWS_netto+su_mwsfrei,ffcurrency,12,2);
        c_brutto:=  myfloattocurr(su_MWS_netto*(mws_faktor+1)+su_mwsfrei);
        sbrutto:= floattostrf( c_brutto,ffcurrency,12,2);

        re_stundensatz:=q_firma.findfield('stundensatz_berechnung').asstring;
        re_faktor_gewerblich:=q_firma.findfield('faktor_ge').asstring;
        re_faktor_verwaltung:=q_firma.findfield('faktor_an').asstring;
        re_ma_gewerblich:=q_firma.findfield('anz_ma_ge').asstring;
        re_ma_verwaltung:=q_firma.findfield('anz_ma_an').asstring;

        re_intervall:=datetostr(q_rechnung.findfield('von').asdatetime )+' - '+datetostr(q_rechnung.findfield('bis').asdatetime );

        if rechnung_offen then
        begin
           re:='nicht_drucken';
           word_schliessen_re:=false;
        end
        else
        begin
          re:='drucken_alles';
          word_schliessen_re:=true;   //??
        end;
        if dat=10 then
        begin
          datname:=pfad_rechnungen+datamodul.q_firma['firma']+'_'+ersetzesonderzeichen(stringdatum(now()))+'_Rechnung_';
          //datname:=ersetzesonderzeichen(datname);
          z:=0;
          while  fileexists(datname+inttostr(z)+'.pdf') do inc(z);
          datname_pdf:=datname+inttostr(z)+'.pdf';
          datname:=datname+inttostr(z)+'.doc';
          arbeitsmedizin.Destroyobject;

          if redruck_stornorechnung then
            begin
              sstornozeile:='Storno zu Rechnung '+renu;
              renu:=renu_storno;
             end;
          dok_erstellen(Arbeitsmedizin, dat,'firma',datamodul.q_firma,
            ['rechnung_datum','rechnung_text','rechnung_nummer','rechnung_storno','rechnung_brutto','rechnung_netto','rechnung_MWSbetrag','rechnung_MWSsatz','Rechnung_MWS_Summe_netto','Rechnung_MWS_Summe_brutto','Rechnung_MWSfrei_Summe','rechnung_intervall','re_stundensatz', 're_f_gewe', 're_f_verw', 're_ma_gewe','re_ma_verw','rechnung_bestellnummer'],
            [re_datum,ersatz,renu,sstornozeile,sbrutto,snetto,smwb,smws_satz,mws_summe_netto,mws_summe_brutto,MWSfrei_summe,re_intervall,re_stundensatz, re_faktor_gewerblich, re_faktor_verwaltung, re_ma_gewerblich,re_ma_verwaltung,re_bestellnummer] );

          com.bookmark_einfuegen_datei('rechnung_tabelle','',f_name,false);  //leerer text

          if rechnung_offen then
          begin
            //arbeitsmedizin.DoVerb(ovshow);
            if Messagedlg('Soll die Rechnung �bernommen werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then
            begin
              arbeitsmedizin.DestroyObject;
              exit;
            end;
          end;
          if (arbeitsmedizin.State in [osempty]) then exit;

          if not (arbeitsmedizin.State in [osopen]) then olecontainer_anzeigen(arbeitsmedizin);
          pause(100);
          com.doku_speichern(datname);
          com.doku_drucken;

          olecontainer_close(arbeitsmedizin);
          arbeitsmedizin.DestroyObject;
          arbene_vor;
          pause(100);

          word_to_pdf(datname,datname_pdf);


         if rechnungen_in_kartei then
          begin

            //leeren_text_erzeugen('Rechnung '+renu);
            leeren_text_erzeugen(datamodul.q_vorlagen.findfield('name').asstring+' - '+renu);

            datamodul.q_texte.edit;
            oleausdatei(arbeitsmedizin_firma,datname_pdf);
            arbeitsmedizin_firma.Modified:=true;
            datamodul.q_texte.FindField('modus').asinteger:=1;
            datamodul.q_texte.post;
            arbeitsmedizin_firma.Modified:=true;
         end;
          deletefile(datname);
          if not speicher_rechnungen then deletefile(datname_pdf);

        end;
        if dat=11 then
          begin
           //datname:=pfad_rechnungen+datamodul.q_mitarbeiter['name']+'-'+datamodul.q_mitarbeiter['vorname']+'-'+ datamodul.q_mitarbeiter.findfield('geb_dat').asstring+'-'+stringdatum+'-';
           datname:=pfad_rechnungen+datamodul.q_mitarbeiter['name']+'_'+datamodul.q_mitarbeiter['vorname']+'_'+
                    ersetzesonderzeichen(stringdatum(datamodul.q_mitarbeiter.findfield('geb_dat').asdatetime)+'_Rechnung'+'_'+stringdatum(now())+'_');
           //datname:=ersetzesonderzeichen(datname);
            z:=0;
            while  fileexists(datname+inttostr(z)+'.pdf') do inc(z);
            datname_pdf:=datname+inttostr(z)+'.pdf';
            datname:=datname+inttostr(z)+'.doc';

            if redruck_stornorechnung then
            begin
              sstornozeile:='Storno zu Rechnung '+renu;
              renu:=renu_storno;
            end;

            dok_erstellen(Arbeitsmedizin, dat,'mitarbeiter',datamodul.q_mitarbeiter,
              ['rechnung_datum','rechnung_text','rechnung_nummer','rechnung_storno','rechnung_brutto','rechnung_netto','rechnung_MWSbetrag','rechnung_MWSsatz','Rechnung_MWS_Summe_netto','Rechnung_MWS_Summe_brutto','Rechnung_MWSfrei_Summe','rechnung_intervall','rechnung_bestellnummer' ],
              [re_datum,ersatz,renu,sstornozeile,sbrutto,snetto,smwb,smws_satz,MWS_Summe_netto,mws_summe_brutto,MWSfrei_Summe,re_intervall,re_bestellnummer ]);
            com.bookmark_einfuegen_datei('rechnung_tabelle','',f_name,false);

            if rechnung_offen then
            begin
              //arbeitsmedizin.DoVerb(ovshow);
              if Messagedlg('Soll die Rechnung �bernommen werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then
              begin
                arbeitsmedizin.DestroyObject;
                exit;
              end;
            end;
            if (arbeitsmedizin.State in [osempty]) then exit;

            if not (arbeitsmedizin.State in [osopen]) then olecontainer_anzeigen(arbeitsmedizin);
            pause(100);
            com.doku_speichern(datname);
            com.doku_drucken;
            com.word_schliessen; // warum nicht if word_drucken_re then ...
            arbeitsmedizin.DestroyObject;

            word_to_pdf(datname,datname_pdf);


            if rechnungen_in_kartei then
            begin
              leeres_dokument_einhaengen(datamodul.q_vorlagen.findfield('name').asstring+' - '+renu,date,0);
              datamodul.q_dokumente.edit;
              oleausdatei(arbeitsmedizin_Proband,datname_pdf);
              datamodul.q_dokumente.FindField('modus').asinteger:=1;
              arbeitsmedizin_Proband.Modified:=true;
              datamodul.q_dokumente.post;
              speichern();
            end;
            deletefile(datname);
            if not speicher_rechnungen then deletefile(datname_pdf);
          end;

      //end;

end;
end;


procedure TForm_main.m_UntersuchungsSchritteClick(Sender: TObject);
var us,ui,query,itypen,dat_sql:string;
	  i_typ,i,r:integer;
    datum:tdatetime;
    s_mitarbeiter:string;
begin
with datamodul do
begin
	i_typ:=q_untersuchung['i_typ'];
  s_mitarbeiter:=q_mitarbeiter.findfield('nummer').AsString;
  datum:=q_untersuchung.findfield('datum').AsDateTime;
  dat_sql:=sql_datetostr(datum);
  query:=format('select i_typ, untersuchung from untersuchung left join typ on (untersuchung.i_typ=typ.nummer) where untersuchung.i_mitarbeiter=%s and untersuchung.datum=%s and untersuchung.storno=0 group by i_typ',[s_mitarbeiter,dat_sql]);
  sql_new(false,q_3,query,'');
  q_3.first;
  itypen:='0';
  while not q_3.Eof do
  begin
    itypen:=itypen + ','+q_3.findfield('i_typ').asstring;
    q_3.next;
  end;

  query:='select  bereich.name as Untersuchungsschritt, unt_befunde.eu,unt_befunde.nu,unt_befunde.prioritaet,bereich.reihenfolge from unt_befunde ';
  query:=query+ format(' left join bereich on(unt_befunde.i_befunde=bereich.nummer ) where unt_befunde.storno=0 and unt_befunde.i_typ in ( %s) group by bereich.name order by reihenfolge',[itypen]);
	sql_new(false,q_1,query,'');//go�-bausteine
	query:='select left(typ.untersuchung,200) as Laborparameter, unt_labor.eu,unt_labor.nu,unt_labor.prioritaet,typ.reihenfolge';
	query:=query+		 format(' from unt_labor left join typ on(unt_labor.i_typ=typ.nummer ) where unt_labor.storno=0 and unt_labor.ii_typ in (%s) group by typ.untersuchung order by reihenfolge',[itypen]);
	sql_new(false,q_2,query,'');//go�-bausteine

  form_u_info:=tform_u_info.create(self);
	form_u_info.Caption:=' Befunde / Labor ';

	form_u_info.showmodal;
	form_u_info.release;


end;
end;

procedure TForm_main.RichEdit_befundChange(Sender: TObject);
begin
richedit_befund.tag:=1;
end;


procedure tform_main.maskedit_format(me:tmaskedit);
begin

	me.EditMask:=decimalseparator_change(me.EditMask);
end;


function fmin(x,y:real):real;
begin
if x<y then result:=x else result :=y;
end;


procedure TForm_main.BitBtn_fallmonitorClick(Sender: TObject);
var
f_name, html_name:string;
us:thtml_uebersicht;

begin
if not m_uebersicht_kartei.enabled then exit;
try
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
	speichern;
	us:=thtml_uebersicht.create(f_name);
	us.report(dbtreeview.Items,0,'','',0);
finally
	us.free;
end;

	html_name:='file:\\'+f_name;
  shellexecute_se(0,'open',pchar(f_name),'','',sw_shownormal);     //application.Handle         shellexecute_se(0,'open',pchar(datname_pdf),'','',sw_shownormal);

end;


procedure TForm_main.Zeituebersicht_anzeigen(Sender: TObject);
begin
if sender=BitBtn_zeituebersicht_anzeigen then Zeituebersicht_ausgabe(0) else
begin
   if  Messagedlg('Das Dokument wird unter Dokumente gespeichert - weiter? ',mtConfirmation, mbOkCancel	,0)=mrOK  then Zeituebersicht_ausgabe(1) else Zeituebersicht_ausgabe(0);
end;
end;

procedure TForm_main.Zeituebersicht_ausgabe(modus:integer);
var
f_name, html_name,query:string;
r:thtml_report;
stunden, minuten,i_krit,zeitansatz:integer;
stunden_r, minuten_r,t:real;
m,h,ort,s1,s2,s3,s4,s5,s6,s7,vz,fr,fl:string;
p:boolean;
hb,mb:real;
begin

einsatzzeiten_filtern;
fr:='align="right"';
fl:='align="left"';

with datamodul do
try
    query:='select * from abr_anlass_krit';
    sql(false,q_5,query,'');

    if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.', [ pfad_temp]));

	f_name:=pfad_temp+'infotext.html';
	//speichern;
    stunden_r:=0;
    minuten_r:=0;
    ort:=q_firma.findfield('firma').asstring;
    q_firma_sollist.last;

	  r:=thtml_report.create(f_name);
    r.stag('font size= medium');
    r.zeile(format('  Dokumentation der geleisteten Zeiten  f�r %s ',[ort]));
    r.stag('font size= small');
    r.absatz;

    if CheckBox_zeit_ansatz.Checked then
    begin
       r.t_kopfzeile('border="1" width="90%" frame=box align="right"','',['Datum','Stunden (Netto)','Minuten (Netto)','Zeitansatz in %','Stunden (Brutto)','Minuten (Brutto)','Kriterium','Aufgabenfeld','T�tigkeit/Aufwand'],['5%','3%','3%','3%','3%','3%','10%','30%','40%']);
       while not q_firma_sollist.Bof do
          begin
            s1:=q_firma_sollist.findfield('Datum').asstring;
            s2:=q_firma_sollist.findfield('stunden').asstring;
            s3:=q_firma_sollist.findfield('minuten').asstring;
            s4:= q_firma_sollist.findfield('zeitansatz').asstring;
            try
            	zeitansatz:=strtoint(s4);
            except
               q_firma_sollist.edit;
               q_firma_sollist.findfield('zeitansatz').asstring:='0';
               q_firma_sollist.post;
               s4:='0';
            end;
            hb:= q_firma_sollist.findfield('stunden').asinteger*zeitansatz/100;
            mb:= q_firma_sollist.findfield('minuten').asinteger*zeitansatz/100;
            s5:=q_firma_sollist.findfield('anlass').asstring;
            s7:=q_firma_sollist.findfield('memo').asstring;
            i_krit:=q_firma_sollist.findfield('i_kriterium').asinteger;
            if q_5.Locate('nummer',i_krit,[])then
                 s6:=q_5.findfield('name').asstring
            else s6:='';


             stunden:=trunc(hb);
             minuten:=round((hb-stunden)*60);
             minuten:=minuten+round(mb);

             stunden:=stunden +minuten div 60;
             minuten:=minuten mod 60;
             if minuten<10 then m:='0' else m:='';
             m:=m+inttostr(minuten);
             h:=inttostr(stunden);
           //r.t_zeile('',[s1,s2,s3,s4,s5,s6,s7]);
            r.t_zeile_f('',[s1,s2,s3,s4,h,m,s6,s5,s7],[fl,fr,fr,fr,fr,fr,fl,fl,fl]);

            stunden_r:=stunden_r+q_firma_sollist.findfield('stunden').asinteger*zeitansatz/100;
            minuten_r:=minuten_r+ q_firma_sollist.findfield('minuten').asinteger*zeitansatz/100;
            q_firma_sollist.Prior;
          end;
          r.stag('/table');
    end
    else
    begin
       r.t_kopfzeile('border="1" width="90%" frame=box align="right"','',['Datum','Stunden','Minuten','Kriterium','Aufgabenfeld','T�tigkeit/Aufwand'],['4%','4%','4%','8%','40%','40%']);
       while not q_firma_sollist.Bof do
       begin
         s1:=q_firma_sollist.findfield('Datum').asstring;
         s2:=q_firma_sollist.findfield('stunden').asstring;
         s3:=q_firma_sollist.findfield('minuten').asstring;
         s4:= q_firma_sollist.findfield('zeitansatz').asstring;
         s5:=q_firma_sollist.findfield('anlass').asstring;
         s7:=q_firma_sollist.findfield('memo').asstring;
         i_krit:=q_firma_sollist.findfield('i_kriterium').asinteger;
         if q_5.Locate('nummer',i_krit,[])then
              s6:=q_5.findfield('name').asstring
         else s6:='';

        //r.t_zeile('',[s1,s2,s3,s4,s5,s6,s7]);
         r.t_zeile_f('',[s1,s2,s3,s6,s5,s7],[fl,fr,fr,fl,fl,fl]);
         zeitansatz:=strtoint(s4);
         stunden_r:=stunden_r+q_firma_sollist.findfield('stunden').asinteger*zeitansatz/100;
         minuten_r:=minuten_r+ q_firma_sollist.findfield('minuten').asinteger*zeitansatz/100;
         q_firma_sollist.Prior;
       end;
       r.stag('/table');
     end;

 
    stunden:=trunc(stunden_r);
 	 minuten:=round((stunden_r-stunden)*60);
    minuten:=minuten+round(minuten_r);
    
    stunden:=stunden +minuten div 60;
    minuten:=minuten mod 60;
    if minuten<10 then m:='0' else m:='';
    m:=m+inttostr(minuten);
    r.absatz;

    if CheckBox_zeit_ansatz.Checked then
    r.zeile(format(' Summe der geleisteten Brutto-Zeiten  f�r %s  : %d:%s  (Stunden:Minuten)',[ort,stunden,m]))
    else
        r.zeile(format(' Summe der geleisteten Zeiten  f�r %s  : %d:%s  (Stunden:Minuten)',[ort,stunden,m]));

    if CheckBox_zeit_ausgabe_rest.Checked then
    begin
      t:=strtofloat(edit_z_fehl.Text);
      if t>0 then p:=true else p:=false;
      t:=abs(t);
      stunden_r:=trunc(t);
      minuten_r:=(t-stunden_r)*60;
      minuten:=round(minuten_r);

      if (minuten<10) then m:='0' else m:='';
      m:=m+inttostr(minuten);

      r.absatz;
      if p then
      r.zeile(format(' noch zu erbringende Stunden f�r %s  : %g:%s  (Stunden:Minuten)',[ort,stunden_r,m]))
      else
      r.zeile(format(' bisherige Mehrleistung f�r %s  : %g:%s  (Stunden:Minuten)',[ort,stunden_r,m]));
    end;
finally
	r.free;
end;
	html_name:='file:\\'+f_name;
  if modus=0 then	shellexecute_se(0,'open',pchar(f_name),'','',sw_shownormal)  //application.Handle
  else //modus=1
  begin
    pagecontrol_firma.ActivePage :=TabSheet_firma_texte;
    //pageControl_firmaChanging(self,0);
    PageControl_firmaChange(self);
    datensatz_neu;
  end;
end;

function decimalseparator_change(sh:string):string;
begin
	result:=stringreplace(sh,',',decimalseparator,[rfreplaceall]);
	result:=stringreplace(result,'.',decimalseparator,[rfreplaceall]);
end;


procedure TForm_main.m_GOaeListeClick(Sender: TObject);
var ber:integer;
query:string;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
speichern;
form_tabellen:=tform_tabellen.Create(self);
//form_tabellen.HelpContext:=10840;
form_tabellen_berechtigung(ber);
form_tabellen.Caption:='GO�-Liste';
form_tabellen.Notebook.PageIndex:=11;
form_tabellen.helpcontext:=49;  //2010
//datamodul.sql(false,datamodul.q_1,'select * from goae order by nummer','');
query:='select * from goae where storno=0 order by lPAD(left(goae_nummer,4),4,"0") ';
datamodul.sql(false,datamodul.q_1,query,'');

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='goae_nummer';
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='goae_text';
form_tabellen.DBGrid_tabellen.Columns[0].width:=60;
form_tabellen.DBEdit_goae_nr.DataField:='goae_nummer';
form_tabellen.DBEdit_goae_text.DataField:='goae_text';
form_tabellen.DBEdit_goae_nn.DataField:='goae_nn';
form_tabellen.DBEdit_goae_punkte.DataField:='punkte';
form_tabellen.DBRadioGroup_goae_kategorie.DataField:='goae_typ';

form_tabellen.showmodal;

form_tabellen.release;


end;


procedure tform_main.anamnese_speichern(flag:integer);
var
	nr:int64;
	ana:boolean;
	query:string;
begin
	nr:=getbigint(datamodul.q_mitarbeiter,'nummer');
  if nr=0 then exit;
  if (not akt_editor.Modified) then exit;
  //if akt_editor.Lines.Text='' then exit;

  if kein_datensatz(datamodul.q_anamnese) then
     begin
      neuer_datensatz(datamodul.q_anamnese,['nummer'],[inttostr(nr)]);
      datamodul.q_anamnese.post;
      datamodul.q_anamnese.refresh;
     end;

   datamodul.q_anamnese.Edit;
   strings_speichern(datamodul.q_anamnese,'memo_blob',akt_editor.Lines);
   datamodul.q_anamnese.FindField('memo').asstring:=akt_editor.Text;

   if datamodul.q_anamnese.findfield('storno').asinteger<>0 then
   begin
       datamodul.q_anamnese.edit;
       datamodul.q_anamnese.findfield('storno').asinteger:=0
   end;

	if datamodul.q_anamnese.state in [dsedit, dsinsert] then datamodul.q_anamnese.post;
end;





procedure tform_main.ana_speichern;
begin
anamnese_speichern(0);
{if radiobutton_arzt.Checked then
begin
	anamnese_speichern(0);
end;
if radiobutton_sani.Checked then
begin
	anamnese_speichern(1);
end;}
end;



procedure tform_main.anamnese_laden(flag:integer);
////main
var
	nr:int64;
	ana,a,s:boolean;
	query:string;
begin
  try
    nr:=getbigint(datamodul.q_mitarbeiter,'nummer');
    if nr=0 then exit;
    strings_laden(datamodul.q_anamnese,'memo_blob',akt_editor.Lines); // dies sollte normalfall sein
    akt_editor.Modified:=false;
  except
  end;
end;

procedure TForm_main.akt_editorSelectionChange(Sender: TObject);
begin
rtfedit.SelectionChange(Self);
end;

procedure TForm_main.m_Personalien_druckenClick(Sender: TObject);
begin
	personalien_druck(nil) ;

end;

procedure TForm_main.DBCombo_num_vor_statusClick(Sender: TObject);
begin
status_neu:=true;
end;

procedure tform_main.check_form_termine;
begin
if form_termine=nil then form_termine:=tform_termine.Create(self);
end;

function tform_main.makepfad(pf:string):string;
begin
	result:=trim(pf);
	if copy(result,length(result),1)<>'\'then result:=result+'\';
end;



procedure TForm_main.BitBtn_audiogramm_exportClick(Sender: TObject);
var
ma:string;
query,rd,rx:string;
z:integer;
begin
with datamodul do
begin
  bereich_speichern;
	ma:=getbigint_str(q_mitarbeiter,'nummer');
	query:='select akt_untersuchung.*,bereich.*, befunde.datum from akt_untersuchung left join befunde on(akt_untersuchung.i_untersuchung=befunde.nummer)  left join bereich on(akt_untersuchung.i_bereich=bereich.nummer)';
	query:=query+format( 'left join berlist on(akt_untersuchung.i_berlist=berlist.nummer) where bereich.modus=3 and i_mitarbeiter=%s order by datum',[ma]);
	sql_new(false,q_1,query,'');

	//z:=com.audio_export(q_1);
	z:=com.audio_graph(q_1);

	com.eobjekt:=Unassigned;

end;
end;




procedure TForm_main.BitBtn_re_maClick(Sender: TObject);
begin
	redruck(11);
end;

procedure TForm_main.SpinEdit_diChange(Sender: TObject);
begin
//RadioGroup_auffaelligClick(sender);
try
if (spinedit_di.Value>90) or (spinedit_sy.value>140) then radiogroup_auffaellig.ItemIndex:=1 else radiogroup_auffaellig.ItemIndex:=0;
except
end;
end;

procedure TForm_main.Edit_bmiChange(Sender: TObject);
begin
try
if strtofloat(edit_bmi.Text)>30 then radiogroup_auffaellig.ItemIndex:=1 else
if strtofloat(edit_whtr.Text)>0.6 then radiogroup_auffaellig.ItemIndex:=1 else radiogroup_auffaellig.ItemIndex:=0;

except
end;

end;

procedure TForm_main.ComboBoxChange(Sender: TObject);
var
daten:pnodepointer;
berlistnummer: integer;
l,r:boolean;
begin
if combobox.itemindex<0 then exit;
RadioGroup_auffaelligClick(sender);
daten:=pnodepointer(combobox.Items.Objects[combobox.itemindex]);
 if daten<>nil then berlistnummer:=daten^.nummer
 else berlistnummer:=-1;
 datamodul.q_berlist.Locate('nummer',berlistnummer,[]);
 l:= not ( datamodul.q_berlist['c_auffaellig']='normal');
 //if datamodul.q_berlist['c_auffaellig']='normal' then radiogroup_auffaellig.ItemIndex:=0 else radiogroup_auffaellig.ItemIndex:=1;
 r:=false;
 if combobox_rechts.Visible and (combobox_rechts.itemindex>=0)  then
 begin
   RadioGroup_auffaelligClick(sender);
   daten:=pnodepointer(combobox_rechts.Items.Objects[combobox_rechts.itemindex]);
   if daten<>nil then berlistnummer:=daten^.nummer
   else berlistnummer:=-1;
   datamodul.q_berlist.Locate('nummer',berlistnummer,[]);
   r:= not (datamodul.q_berlist['c_auffaellig']='normal');
 end;
  if (l or r) then radiogroup_auffaellig.ItemIndex:=1 else radiogroup_auffaellig.ItemIndex:=0;

end;

procedure TForm_main.ToolButton_materialClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
speichern;
form_tabellen:=tform_tabellen.Create(self);
tabellen_material_einstellen;
form_tabellen.GroupBox_art.Visible:=false;
form_tabellen.showmodal;
form_tabellen.release;

end;

procedure TForm_main.tabellen_material_einstellen;
begin
form_tabellen.Notebook.PageIndex:=12;
form_tabellen.Caption:='Materialstamm';
form_tabellen.HelpContext:=11200;
datamodul.sql_new(false,datamodul.q_1,'select * from materialstamm order by art_name','');
with form_tabellen do
begin
	DBEdit_artnr.DataField:='art_nr';
	dbedit_ar_name.DataField:='art_name';
	dbedit_ean.DataField:='art_ean';
	dbmemo_art.DataField:='art_beschreibung';
	DBCombo_lieferant.DataField:='i_lieferant';
	dbcombobox_einheit.DataField:='einheit';
	dbedit_inhalt.DataField:='inhalt';
	dbedit_ekp.DataField:='einkaufspreis';
	dbedit_evkp.DataField:='empfverkaufspreis';
	dbedit_vkp.DataField:='verkaufspreis';
	dbedit_lager.DataField:='lagerbestand';
	dbedit_lagermind.DataField:='mindestbestand';
	dbedit_lagerbest.DataField:='bestellt';
	DBEdit_mindbest.DataField:='mindestbestellmenge';
	dbedit_verpackungseinheit.DataField:='verpackungseinheit';
	dbcombo_lieferant.lookup_laden;
	suchfeld:='art_name';

//	DBGrid_tabellen.Columns[0].fieldname:='art_nr';
//	DBGrid_tabellen.Columns[0].width:=30;
	DBGrid_tabellen.Columns[0].fieldname:='art_name';
	DBGrid_tabellen.Columns[0].width:=DBGrid_tabellen.Width-30;;

end;
end;

procedure TForm_main.Lieferanten1Click(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:= b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
  speichern;
	form_tabellen:=tform_tabellen.Create(self);
	//form_tabellen.HelpContext:=10730;
	form_tabellen_berechtigung(ber);

	form_tabellen.suchfeld:='name';
	form_tabellen.Caption:='Lieferanten';
	form_tabellen.Notebook.PageIndex:=7;
	form_tabellen.ToolButton_neu.enabled:=true;
	datamodul.sql(false,datamodul.q_1,'select * from lieferanten','name');

	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='name';
	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;

	form_tabellen.DBEdit_name.DataField:='name';
	form_tabellen.DBEdit_strasse.DataField:='strasse';
	form_tabellen.DBEdit_plz.DataField:='plz';
	form_tabellen.DBEdit_ort.DataField:='ort';
	form_tabellen.DBEdit_telefon.DataField:='telefon';
	form_tabellen.DBEdit_fax.DataField:='fax';
	form_tabellen.DBEdit_email.DataField:='email';
	form_tabellen.DBEdit_www.DataField:='www';
	form_tabellen.DBmemo_memo.DataField:='memo';
	form_tabellen.DBedit_kundennummer.DataField:='kundennummer';
	form_tabellen.DBedit_postfach.DataField:='postfach';

	grid_einstellen(form_tabellen.dbgrid_tabellen);
	form_tabellen.showmodal;
	form_tabellen.release;

end;


procedure TForm_main.BitBtn_art_wahlClick(Sender: TObject);
var
m_nummer:integer;
begin
if sender.ClassType =tbitbtn then
   if (Messagedlg('Wollen Sie einen anderen Artikel w�hlen ?',mtConfirmation, mbOkCancel	,0)<>mrOK) then exit;

	try

	  form_tabellen:=tform_tabellen.Create(self);
	  form_tabellen.ToolBar.visible:=false;
	  form_tabellen.Panel_b.visible:=true;
	  tabellen_material_einstellen;
	  if form_tabellen.showmodal=mrok then
	  begin
		m_nummer:=datamodul.q_1['nummer'];
		datamodul.q_artikel.edit;
		datamodul.q_artikel['i_material']:=m_nummer;
		datamodul.q_artikel['art_name']:=datamodul.q_1['art_name'];
		datamodul.q_artikel['einzeL_netto']:=datamodul.q_1['verkaufspreis'];
    datamodul.q_artikel.findfield('einheit').asstring:=datamodul.q_1.findfield('einheit').asstring;
		datamodul.q_artikel.findfield('menge').asfloat:=datamodul.q_1.findfield('inhalt').asfloat;//0;

	  end;
	 finally
		 form_tabellen.Release;
	 end;
end;

procedure TForm_main.DBEdit_art_mengeChange(Sender: TObject);
begin
if datamodul.q_artikel.active and (edit_brutto.enabled) and(length(dbedit_art_menge.text)>0)  then
try
datamodul.q_artikel.edit;
datamodul.q_artikel['netto_o_rabatt']:=datamodul.q_artikel['einzel_netto']*strtoint(dbedit_art_menge.text);
//DBEdit_art_rabattChange(Sender);
edit_rabatt_prozchange(sender);
except
end;
end;

procedure TForm_main.DBEdit_art_rabattChange(Sender: TObject);
begin
if ( datamodul.q_artikel.active and (edit_brutto.enabled) and (length(dbedit_art_rabatt.text)>0) and (notebook_main.pageindex=11)) then
try
datamodul.q_artikel.edit;
datamodul.q_artikel['netto_m_rabatt']:=datamodul.q_artikel['netto_o_rabatt']-strtofloat(dbedit_art_rabatt.text);
if (sender=dbedit_art_rabatt) and (datamodul.q_artikel.FindField('netto_o_rabatt').asfloat<>0) then
	edit_rabatt_proz.text:=  strtofloat(dbedit_art_rabatt.text) / datamodul.q_artikel['netto_o_rabatt'] *100;
except
end;

end;

procedure TForm_main.DBEdit_art_endpreisChange(Sender: TObject);
begin
if datamodul.q_artikel.active and (datamodul.q_artikel.findfield('netto_m_rabatt').asstring<>'')  then
try
 if dbcheckbox_art_ust.Checked then
  edit_brutto.text:=floattostrf(datamodul.q_artikel.findfield('netto_m_rabatt').asfloat+datamodul.q_artikel.findfield('netto_m_rabatt').AsFloat*mws/100,ffCurrency,7,2)
  else
    edit_brutto.text:=floattostrf(datamodul.q_artikel.findfield('netto_m_rabatt').asfloat,ffCurrency,7,2)
except
end;
end;

procedure TForm_main.Edit_rabatt_prozChange(Sender: TObject);
begin
if  datamodul.q_artikel.active and (edit_brutto.enabled) and (datamodul.q_artikel.findfield('netto_o_rabatt').asstring<>'')then
	try
	datamodul.q_artikel.edit;
	datamodul.q_artikel['rabatt_euro']:= datamodul.q_artikel['netto_o_rabatt']*strtofloat(edit_rabatt_proz.text) /100;
	DBEdit_art_rabattChange(Sender);
	except
	end;
end;

procedure TForm_main.ToolButton_sichernClick(Sender: TObject);
begin
	  dbspeichern(true);

end;

procedure TForm_main.ToolButton_abortClick(Sender: TObject);
begin
    dbspeichern(false);

end;

procedure TForm_main.ToolButton_loeschenClick(Sender: TObject);
begin
loeschen;
end;

procedure TForm_main.BitBtn_artplusClick(Sender: TObject);
var
query:string;
nr:integer;
begin
	nr:=datamodul.q_artikel['i_material'];
	datamodul.q_artikel.edit;
	DBEDIT_ART_MENGE.text:=inttostr(strtoint(DBEDIT_ART_MENGE.text)+1);
    datamodul.q_artikel['menge']:=datamodul.q_artikel['menge']+1;
  //DBEDIT_ART_MENGE.Refresh;
	query:=format('update materialstamm set lagerbestand = lagerbestand-1 where nummer=%d',[nr]);
	mysql_d.sql_exe(query);
  //direktspeichern:=true;
  //datamodul.q_artikel.post;
end;

procedure TForm_main.BitBtn_artminusClick(Sender: TObject);
var
query:string;
nr:integer;
begin
	nr:=datamodul.q_artikel['i_material'];
	datamodul.q_artikel.edit;
	//datamodul.q_artikel['menge']:=datamodul.q_artikel['menge']-1;
  DBEDIT_ART_MENGE.text:=inttostr(strtoint(DBEDIT_ART_MENGE.text)-1);
  //DBEDIT_ART_MENGE.Refresh;
  //DBEDIT_ART_MENGE.text:=inttostr(datamodul.q_artikel['menge']+1);
	query:=format('update materialstamm set lagerbestand = lagerbestand+1 where nummer=%d',[nr]);
	mysql_d.sql_exe(query);
  //direktspeichern:=true;
  //datamodul.q_artikel.post;
end;


procedure TForm_main.m_arbeitsplatzClick(Sender: TObject);
begin
if  b.b_string('anwendung')=-1 then exit;
    speichern;
	  pagecontrol_main.activepage:=tabsheet_firma;
	  optionen_show(2);
	  //inischreiben;
	  sichtbarkeiten_einstellen;
	  berechtigungen_einstellen;//neu
	  //sichtbarkeiten_einstellen;
end;

procedure TForm_main.PageControl_befundeChanging(Sender: TObject;
  var AllowChange: Boolean);
  var ac:boolean;
begin
  dbTreeViewChanging(sender,dbtreeview.selected,ac);
end;



procedure TForm_main.reihenfolge_schreiben(tabelle:tzquery;gruppenwechsel:string);
var i,gw1,gw2:integer;
begin
i:=1;
if gruppenwechsel='' then gw1:=0 else gw1:=tabelle.findfield(gruppenwechsel).asinteger;
while not tabelle.Eof do
  begin
    if gruppenwechsel='' then gw2:=0 else gw2:=tabelle.findfield(gruppenwechsel).asinteger;
    if gw1<>gw2 then i:=1;
    gw1:=gw2;
    tabelle.Edit;
    tabelle.FindField('reihenfolge').asinteger:=i;
    tabelle.Post;
    inc(i);
    tabelle.Next;
  end;
end;

procedure TForm_main.m_GefaehrdungenClick(Sender: TObject);
var
query:string;

begin
   speichern;
   if b.b_string('Firmen-Listen')>-1 then
   begin
     query:='select * from gefaehrdungen_1 where storno=0 order by reihenfolge,name';
     datamodul.sql_new(false,datamodul.q_7,query,'');
      query:='select * from gefaehrdungen_2 where storno=0 order by i_gef1,reihenfolge, name';
     datamodul.sql_new(false,datamodul.q_8,query,'');
      query:='select * from gefaehrdungen_3 where storno = 0 order by i_gef2,reihenfolge, memo_gef ';
     datamodul.sql_new(false,datamodul.q_9,query,'');
     //reihenfolgen eintragen
      datamodul.q_9.first;
      datamodul.q_8.first;
      datamodul.q_7.first;
      if datamodul.q_9.FindField('reihenfolge').asinteger=0 then  reihenfolge_schreiben(datamodul.q_9,'i_gef2');
      if datamodul.q_8.FindField('reihenfolge').asinteger=0 then  reihenfolge_schreiben(datamodul.q_8,'i_gef1');
      if datamodul.q_7.FindField('reihenfolge').asinteger=0 then  reihenfolge_schreiben(datamodul.q_7,'');


     datamodul.q_14.close;
      datamodul.q_15.close;
     datamodul.q_16.close;

     form_gefaehrdungen:=tform_gefaehrdungen.create(self);
     form_gefaehrdungen.toolbutton_ok.Caption:='Beenden';
     form_gefaehrdungen.dbtreeview.selected:=form_gefaehrdungen.dbtreeview.TopItem;
     //f_gef.ShowModal;
     form_gefaehrdungen.Width:=form_main.Width;
     form_gefaehrdungen.Height:=form_main.Height;
     form_gefaehrdungen.ShowModal;
     form_gefaehrdungen.release;
   end;  
end;



procedure tform_main.abteilungskuerzel_eintragen; //f�r aktuelle firma
var query:string;
f,I:integer;
i64:INT64;
s:string;
begin
with datamodul do
begin
     f:= q_firma['nummer'];
     //query:='select mitarbeiter.i_abteilung,mitarbeiter.i_firma,mitarbeiter.i_abteilung, abteilung.nummer as i_abt, abteilung.i_firma as i_firma_abt from mitarbeiter left join abteilung on (mitarbeiter.abteilung=abteilung.kuerzel)';
     query:=format('select nummer,i_firma,abteilung,i_abteilung from mitarbeiter where i_firma=%d',[f]);
     sql_new(false,q_7,query,'');
     query:=format('select nummer, kuerzel, i_firma from abteilung where i_firma=%d',[f]);
     sql_new(false,q_8,query,'');

     q_7.First;
     while not q_7.Eof do
     begin
            if q_7['abteilung']<>null then s:=q_7['abteilung'] else s:='';
            if s<>'' then
            begin
              i:=q_7['i_firma'];
              if q_8.Locate('kuerzel',s,[]) then
              begin
                   i64:=getbigint(q_8,'nummer');
                   try
                   q_7.edit;
                   setbigint(q_7,'i_abteilung',i64);
                   q_7.Post;
                   except
                   end;
              end;
            end;
       q_7.Next;
       end;
end;
end;

procedure TForm_main.SpeedButton1Click(Sender: TObject);
begin
dbtreeview.suche_datensatz(edit_such.text);
end;

procedure TForm_main.Edit_suchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//if key<>vk_return then exit;
//key:=0;
application.ProcessMessages;
dbtreeview.suche_datensatz(edit_such.text);
end;

procedure TForm_main.Edit_suchEnter(Sender: TObject);
begin
//dbtreeview.Selected:=dbtreeview.Items[0];
//dbtreeview.change(dbtreeview.selected);
end;

procedure TForm_main.ComboBox_historyKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  key:=0;
end;

function tform_main.bruttos(netto,mws:string):string;
var
r:real;
begin
try
     netto:=decimalseparator_change(netto);
     r:=strtofloat(netto)*strtofloat(mws)/100+strtofloat(netto);
     result:=floattostrf(r,fffixed,7,2);
except
      result:='';
end;
end;

procedure TForm_main.Edit_firma_suchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_return then SpeedButton_firma_suchClick(sender);
end;

procedure TForm_main.SpeedButton_firma_suchClick(Sender: TObject);
var
re,query,firma,such,ws,subs:string;
nr:int64;
p:integer;
begin
//treeview_ext_firma.suche_datensatz(edit_firma_such.text);

if kein_datensatz(datamodul.q_firma) then exit;
such:=edit_firma_such.text;
such:=stringreplace(such,' ','',[rfreplaceall]);
if such='' then exit;

p:=pos('+',such);
while p>0 do
begin
  subs:=copy(such,0,p-1);
  such:=copy(such,p+1,length(such));
  if ws<>'' then ws :=ws+ ' and ';
  ws:=ws+' firma like "%'+subs+'%"';
  p:=pos('+',such);
end;
  if such<>'' then
  begin

    if ws<>'' then ws :=ws+ ' and ';
    ws:=ws+' firma like "%'+such+'%"';
  end;

firma:=datamodul.q_firma['firma'];
query:='select nummer from firma where '+ws +
  format(' and firma>"%s" and nummer in (%s) and storno=0 order by firma',[firma,firmen_filter]); //filter statt firmen_berechtigung

re:=mysql_d.Feldinhalt(query,0);

if re='' then
begin
  datamodul.q_firma.first; // noch mal von vorne
  firma:=datamodul.q_firma['firma'];
  query:='select nummer from firma where '+ws +
  //hier >=
  format(' and firma>="%s" and nummer in (%s) and storno=0 order by firma',[firma,firmen_berechtigung]);

  re:=mysql_d.Feldinhalt(query,0);
end;

if re='' then exit;
nr:=strtoint(re);
//treeview_ext_firma.suche_nummer(nr);
datamodul.q_firma.locate('nummer',nr,[ ]);  

end;

procedure TForm_main.dbTreeViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if chr(key) in ['a'..'z', 'A'..'Z'] then edit_such.text:=chr( key);
   edit_such.SetFocus;
   edit_such.SelStart:=99;
end;

function tform_main.string_verschluesseln(s:string):string;
begin
		cipher.InitStr(tkey,TDCP_sha1);
     result:=cipher.EncryptString(s);
     cipher.Burn;
     result:='Arb'+result;
end;

function tform_main.string_entschluesseln_pr(s:string):string;
begin
  if copy(s,1,3)='Arb' then
  begin
     s:=copy(s,4,length(s)-3);
		cipher.InitStr(tkey,Tdcp_sha1);
     result:=cipher.DecryptString(s);
     cipher.Burn;
  end
  else
  begin
    //result:=CipherManager.DecodeString(s);
    showmessage('Das Passwort muss zur�ckgesetzt werden - melden Sie sich mit aaaaa (5 mal a) an' +chr(10)+'Vergeben Sie dann das Passwort neu.');
    result:='aaaaa';
  end;
end;

function tform_main.string_entschluesseln(s:string):string;
begin
  if copy(s,1,3)='Arb' then
  begin
     s:=copy(s,4,length(s)-3);
		cipher.InitStr(tkey,Tdcp_sha1);
     result:=cipher.DecryptString(s);
     cipher.Burn;
  end
  else
  begin
    //result:=CipherManager.DecodeString(s);
    //showmessage('Das Passwort muss neu zur�ckgesetzt werden - melden Sie sich mit aaaaa (5 mal a) an' +chr(10)+'Vergeben Sie dann das Passwort neu.');
    //result:='aaaaa';
  end;
end;
function tform_main.datei_verschluesseln_key(quelle, ziel:string):boolean;
var
    KeyStr: string;
    Source, Dest: TFileStream;
  begin
    KeyStr:= '';
    result:=true;
    if InputQuery('Bitte Passwort zum Verschl�sseln der Datei eingeben','(Das Passwort kann auch leer sein)',KeyStr) then // get the passphrase
    begin
      try
        Source:= TFileStream.Create(quelle,fmOpenRead);
        Dest:= TFileStream.Create(ziel,fmCreate);
			Cipher.InitStr(KeyStr,TDCP_sha1);              // initialize the cipher with a hash of the passphrase
        Cipher.EncryptStream(Source,Dest,Source.Size); // encrypt the contents of the file
        Cipher.Burn;
        Dest.Free;
        Source.Free;
        result:=true;
      except
        Result:=false;
      end;
    end
    else result:=false;

end;

function tform_main.datei_entschluesseln_key(quelle, ziel:string):boolean;

  var
    KeyStr: string;
    Source, Dest: TFileStream;
  begin
    KeyStr:= '';
    if InputQuery('Bitte Passwort zum Verschl�sseln der Datei eingeben','(Das Passwort kann auch leer sein)',KeyStr)  then   // get the passphrase
    begin
      try
        Source:= TFileStream.Create(quelle,fmOpenRead);
        Dest:= TFileStream.Create(ziel,fmCreate);
			Cipher.InitStr(KeyStr,TDCP_sha1);              // initialize the cipher with a hash of the passphrase
        Cipher.DecryptStream(Source,Dest,Source.Size); // decrypt the contents of the file
        Cipher.Burn;
        Dest.Free;
        Source.Free;
        result:=true;
      except
        result:=false;
      end;
    end
    else result :=false;
end;

function tform_main.dateistruktur( dat: string):string;
var
query:string;
i:integer;
begin
query:= format('select * from %s where nummer=0',[dat]);
datamodul.sql_new(false,datamodul.q_9, query,'');
result:='';
for i:=0 to datamodul.q_9.fields.Count-1 do
begin
     result:=result+ lowercase(datamodul.q_9.fields[i].fieldname);
end;

end;

procedure TForm_main.SpeedButton_datum_artikelClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_artikel.edit;
		datamodul.q_artikel['datum'] :=int(form_kalender.auswahlkalender.date);
     baum_neu:=true;
	 end;
end;

procedure TForm_main.SpeedButton_bes_erledigtClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_besonderheiten.edit;
		datamodul.q_besonderheiten['datum_bis'] :=int(form_kalender.auswahlkalender.date);
    if datamodul.q_besonderheiten['i_user']=0 then datamodul.q_besonderheiten['i_user'] :=akt_untersucher;
   end;
end;

procedure TForm_main.m_druck_alleBeschClick(Sender: TObject);
var
allowchange:boolean;
begin
   if b.b_string('Proband-Bescheinigung')=-1 then exit;
  if not b.b_alter_ok(datamodul.q_untersuchung) then exit;   //berechtigung
  PageControl_mainChanging (Sender,allowchange );
  if not allowchange then exit;
  speichern;
  pagecontrol_main.ActivePage:=tabsheet_stammdaten;
  //pagecontrol_main.ActivePage:=tabsheet_stammdaten;

  application.ProcessMessages;

  form_kartei_druck:=tform_kartei_druck.create(self);
  form_kartei_druck.einzeln:=false;
  form_kartei_druck.MaskEdit_dat.Text:=datetostr(now());
  form_kartei_druck.MaskEdit_dat_bis.Text:='31.12.2100';
  form_kartei_druck.showmodal;
  form_kartei_druck.Release;
end;


procedure TForm_main.m_druck_bescheinigung_karteiClick(Sender: TObject);
begin
  bescheinigung_drucken(sender);//
end;

procedure TForm_main.SpeedButton_verschicktClick(Sender: TObject);
var
	tab:tzquery;
  feld:string;
  fnummer:integer;
rnummer,query,strdat:string;
i_ma:int64;
bm:tbookmark;
begin
tab:=datamodul.q_rechnung;
if ((sender =speedbutton_verschickt) or (sender =speedbutton_f_verschickt)) then feld:='dat_verschickt';
if ((sender =speedbutton_bezahlt) or (sender =speedbutton_f_bezahlt)) then feld:='dat_bezahlt';
if form_kalender.showmodal =mrok then
	 begin
    strdat:=dat_delphi_to_sql(form_kalender.MaskEdit1.Text );
		tab.edit;
		tab[feld] :=int(form_kalender.auswahlkalender.date);
    if sender=speedbutton_f_bezahlt then tab.FindField('i_konto').Asinteger:= konto_gewaehlt;
     direktspeichern:=true;
		tab.post;
    // wenn �bergeordnete Rechnung auch die daten bei der untergeordneten angleichen
    // suche nach Verweisen, alle durchgehen und entsprechendes eintragen.

    //##

      with datamodul do
      begin
        bm:=q_repositionen.GetBookmark;
        q_repositionen.First;
       while not q_repositionen.Eof do
       begin
         rnummer:= q_repositionen.findfield('i_link_rechnung').asstring;
         if rnummer<>'0' then
         begin
            query:=format('update rechnung set %s="%s" where nummer=%s', [feld,strdat,rnummer] );
            mysql_d.sql_exe(query);
         end;
         q_repositionen.Next;
        end;
       q_repositionen.GotoBookmark(bm);
       q_repositionen.FreeBookmark(bm);
      end;
    //##
	 end;
end;

procedure TForm_main.BitBtn_f_rechnung_phClick(Sender: TObject);
begin
  repo_aendern(true,true);
  rechnung_refresh;
  formular_entsperren(true,false);
end;

procedure TForm_main.BitBtn_pos_prob_neuClick(Sender: TObject);
begin
  repo_aendern(true,false);
  rechnung_refresh;
  formular_entsperren(true,false);
end;

procedure TForm_main.BitBtn_f_rechnung_paeClick(Sender: TObject);
var grid: tdbgridext;
bm:tbookmark;
fipr:boolean;
begin

  if ((sender= BitBtn_repoaendern_prob) or (sender=m_p_Positionndern))  then
  begin
    grid:=dbgridext_re_positionen;
    fipr:=false;
  end
  else
  begin
    grid :=dbgridext_repo;
    fipr:=true;
  end;
  if grid.SelectedRows.Count=0 then showmessage('Es muss mindestens eine Zeile markiert sein (Punkt am Zeilenanfang)'+#13#10+'Markieren mehrerer Zeilen mit Strg+li Maustaste');

  if repositionen_verweismarkiert then
  		showmessage('Ein Verweis auf eine Rechnung kann nicht ge�ndert werden' )
  else
  begin
     if grid.SelectedRows.Count=1 then   repo_aendern(false,fipr);
     if grid.SelectedRows.Count>1 then   repo_aendern_mehrere(grid);
     {bm:=datamodul.q_rechnung.GetBookmark;
     datamodul.q_rechnung.refresh;
     datamodul.q_rechnung.GotoBookmark(bm);
     datamodul.q_rechnung.FreeBookmark(bm);}
     rechnung_refresh;
     formular_entsperren(true,false);
  end;
end;

procedure TForm_main.rechnung_refresh;
var
bm1,bm2:tbookmark;
begin
   try
     bm1:=datamodul.q_rechnung.GetBookmark;
     bm2:=datamodul.q_repositionen.GetBookmark;

     datamodul.q_rechnung.refresh;
     datamodul.q_repositionen.refresh;
     try

     datamodul.q_rechnung.GotoBookmark(bm1);
     finally
     datamodul.q_rechnung.FreeBookmark(bm1);
     end;
     try
     datamodul.q_repositionen.GotoBookmark(bm2);
     finally
     datamodul.q_repositionen.FreeBookmark(bm2);
     end;
    except
    end; 
end;

function tform_main.repositionen_verweismarkiert: boolean;
var
i:integer;
begin
result:=false;
 for I := 0 to DBGridext_repo.SelectedRows.Count - 1 do
      begin
           datamodul.q_repositionen.GotoBookmark(Pointer(DBGridext_repo.SelectedRows.Items[I]));
           if getbigint(datamodul.q_repositionen,'i_link_rechnung')>0 then  result:=true;
      end;
end;

procedure TForm_main.repo_aendern_mehrere(grid:tdbgridext);
var
i,ust:integer;
cent,i_renu:int64;
faktor,netto,brutto:real;
goae,s_cent:string;
dat:tdate;
begin
 with datamodul do
  try
       form_repo_edit_m:=tform_repo_edit_m.create(self);
       form_repo_edit_m.MaskEdit_dat.text:=datetostr(now);


       if form_repo_edit_m.showmodal=mrcancel then exit;
        //exit;

      if grid.SelectedRows.Count > 0 then          //DBGridext_repo
   	  with grid.DataSource.DataSet do
      for I := 0 to grid.SelectedRows.Count - 1 do
      begin
           GotoBookmark(Pointer(grid.SelectedRows.Items[I]));

           q_repositionen.findfield('goae_nn').asstring;
           q_repositionen.findfield('faktor').asstring;

           cent:=getbigint(q_repositionen,'cent');
           ust:=q_repositionen.findfield('ust').asinteger;
           goae:=q_repositionen.findfield('goae').asstring;
           faktor:=q_repositionen.findfield('faktor').asfloat;
           dat:= strtodate(q_repositionen.findfield('datum').asstring);
           //�bernehmen


          if form_repo_edit_m.CheckBox1.checked then dat:=strtodate(form_repo_edit_m.MaskEdit_dat.text);

          if form_repo_edit_m.CheckBox2.checked then
          try
            faktor:=strtofloat(form_repo_edit_m.Edit_faktor.Text);
          except
            faktor :=1;
          end;

          if form_repo_edit_m.CheckBox3.checked then   ust:=strtoint(form_repo_edit_m.combobox_ust.text); //ust
          //if  form_repo_edit_m.CheckBox_ust.Checked then ust:=mws else ust:=0;

          //netto:=round(cent / 1000);
          netto:=cent;
          //brutto:=round((netto + round(netto*ust/100))*faktor);
          brutto:=round(round(netto*faktor)* (100+ust)/100);
          netto:=netto/(cent_mult); // /100
          brutto:=brutto/(cent_mult);// /100

          s_cent:=inttostr(cent);


          q_repositionen.edit;
            q_repositionen['datum']:=dat;
            setbigint(q_repositionen,'cent',cent);
            q_repositionen['faktor']:=faktor;
            q_repositionen['netto']:=netto;
            q_repositionen['brutto']:=brutto;
            q_repositionen['ust']:=ust;
         q_repositionen.post;

      end;

    i_renu:=getbigint(q_repositionen,'i_rechnung');

    rechnung_aktualisieren(i_renu);
    //formular_entsperren(true,false);
  finally
  form_repo_edit_m.release;
  end;

end;

procedure TForm_main.repo_aendern(neu,fipr:boolean);
var
i_firma,summe,ust,i_fimra,haupt_re:integer;
cent,dbnr,i_renu,i_rechnung,ma_nr:int64;
s_cent,query:string;
faktor:real;
brutto,netto: real;
//i_ma:string;
renu,re_text,preis,goae, goae_nn,z:string;
dat:tdate;
bm:tbookmark;
begin
 with datamodul do
  try
       form_repo_neu:=tform_repo_neu.create(self);
       form_repo_neu.MaskEdit_dat.text:=datetostr(now);

      if  neu then
       begin
          //probandenrechung manr
          //manr:=getbigint_str(q_mitarbeiter,'nummer');
          //firmenrechnung
           if fipr then
           begin
            if  kein_datensatz(q_repositionen) then  ma_nr:=0 else ma_nr:=getbigint(q_repositionen,'i_mitarbeiter');
           end
           else
           begin
            ma_nr:=getbigint(q_mitarbeiter,'nummer');
           end;

           form_repo_neu.Edit_faktor.Text:='1';//datamodul.q_firma.findfield('fa_bef').asstring;
           form_repo_neu.Edit_preis.text:='0';
       end
       else
       begin
       	form_repo_neu.MaskEdit_dat.text:=q_repositionen.findfield('datum').asstring;
        form_repo_neu.Edit_rete.Text:=q_repositionen.findfield('re_text').asstring;
        form_repo_neu.Edit_goae.Text:=q_repositionen.findfield('goae').asstring;
        form_repo_neu.Edit_goae_nn.Text:=q_repositionen.findfield('goae_nn').asstring;
        form_repo_neu.Edit_faktor.Text:=q_repositionen.findfield('faktor').asstring;
       // form_repo_neu.Edit_preis.text:=floattostr(getbigint(q_repositionen,'cent')/ 100000);
        form_repo_neu.Edit_preis.text:=floattostr(getbigint(q_repositionen,'cent')/ cent_mult);
        form_repo_neu.ComboBox_ust.Text:=q_repositionen.findfield('ust').asstring;
        //form_repo_neu.CheckBox_ust.Checked:=q_repositionen.findfield('ust').asstring<>'0';
        //form_repo_neu.SpinEdit_ust.Value:=q_repositionen.findfield('ust').asinteger;
        ma_nr:=getbigint( q_repositionen,'i_mitarbeiter');
       end ;

       form_repo_neu.ma_nr :=ma_nr; //q_repositionen.findfield('i_mitarbeiter').asstring;
       form_repo_neu.Edit_proband.Text:=ma_name( form_repo_neu.ma_nr,3);

       if not fipr then form_repo_neu.BitBtn_proband.Visible :=false;

       if form_repo_neu.showmodal=mrcancel then exit;


       //exit;
       ma_nr:=form_repo_neu.ma_nr;
       //if  form_repo_neu.CheckBox_ust.Checked then ust:=mws else ust:=0;
       dat:=strtodate(form_repo_neu.MaskEdit_dat.text);
       re_text:=form_repo_neu.Edit_rete.Text;
       preis:=form_repo_neu.Edit_preis.Text;
       goae:=form_repo_neu.Edit_goae.Text;
       goae_nn:=form_repo_neu.Edit_goae_nn.Text;
       //ust:=form_repo_neu.SpinEdit_ust.Value;
       ust:=strtoint(form_repo_neu.ComboBox_ust.Text);
       try
       	faktor:=strtofloat(form_repo_neu.Edit_faktor.Text);
       except
       	faktor :=1;
       end;
       //cent:=trunc(strtofloat(preis)*100000);
       cent:=round(strtofloat(preis)*cent_mult); //*100
       //netto:=round(cent / 1000);
       netto:=cent;
       brutto:=round(round(netto*faktor)* (100+ust)/100);
       netto:=netto/(cent_mult); // /100
       brutto:=brutto/(cent_mult);// /100

       s_cent:=inttostr(cent);

        renu:=q_rechnung['renu'];
        i_rechnung:=getbigint(q_rechnung,'nummer');
        i_firma:=q_rechnung['i_firma'] ;

        if q_rechnung.FindField('i_parent').asinteger=0 then  haupt_re:=1 else haupt_re:=0;

       if neu then
       begin

       		neuer_datensatz(q_repositionen,['nummer','i_firma','i_mitarbeiter','datum','re_text','renu','cent','netto','faktor','db','brutto', 'ust','goae','goae_nn','i_rechnung','haupt_re']
       		,[null,i_firma,inttostr(ma_nr),dat,re_text,renu,s_cent,netto,faktor ,9,brutto, ust,goae ,goae_nn,i_rechnung,haupt_re]);

       end
       else
       begin
          q_repositionen.edit;
          q_repositionen['datum']:=dat;
       		q_repositionen['re_text']:=re_text;
       		q_repositionen['goae']:=goae;
       		q_repositionen['goae_nn']:=goae_nn;
            setbigint(q_repositionen,'cent',cent);
       		//q_repositionen['cent']:=cent;
       		q_repositionen['goae_nn']:=goae_nn;
       		q_repositionen['faktor']:=faktor;
       		q_repositionen['netto']:=netto;
          q_repositionen['brutto']:=brutto;
          q_repositionen['ust']:=ust;
          q_repositionen.FindField('i_mitarbeiter').asstring:=inttostr(ma_nr);
       end;
       q_repositionen.post;

       //abfragen ob manr als headerzeile in den repositionen ist, sonst anlegen mit rf, i_fima, i_mitarbeiter, renu, re_text, i_rechnung, db=0, farbe=7, haupt_rechnung
       query:=format('select count(*) from re_positionen where i_mitarbeiter=%s and db=0 and i_rechnung=%s and storno=0',[inttostr(ma_nr),inttostr(i_rechnung)]);
       z:=mysql_d.Feldinhalt(query,0);
       if fipr and (z='0') and (ma_nr>0)  then
       begin
          re_text:=ma_name(ma_nr,3);
          neuer_datensatz(q_repositionen,['nummer','i_firma','i_mitarbeiter','datum','re_text','renu','cent','netto','faktor','db','brutto', 'ust','goae','goae_nn','i_rechnung','haupt_re','farbe']
       		,[null,i_firma,inttostr(ma_nr),dat,re_text,renu,0,0,0 ,0,0,0,'' ,'',i_rechnung,haupt_re,7]);
          q_repositionen.post;
          neu:=true;
       end;

       if neu then
       begin
        //dbnr:=getbigint_str(q_repositionen,'nummer');
       	dbnr:=getbigint(q_repositionen,'nummer');
       	q_repositionen.Refresh;

        q_repositionen.Locate('nummer',dbnr,[]);
       end;
       i_renu:=getbigint(q_repositionen,'i_rechnung');
       rechnung_aktualisieren(i_renu);
       //rechnung_refresh;
       //formular_entsperren(true,false);
  finally
  form_repo_neu.release;
  end;
end;



procedure tform_main.rechnung_aktualisieren_alt(renu:int64);
var
netto,brutto,cent,b1,parent,nummer,nwert,bwert: int64;
ust,db:integer;
faktor:real;
euronetto,eurobrutto:currency;
//bm:tbookmark;
query,srepo,srenu,sbrutto, snetto,re_pos:string;
begin
with datamodul do
	begin
       //bm:=q_repositionen.GetBookmark;
       re_pos:=q_repositionen.findfield('nummer').asstring;
	    //q_repositionen.first;
       netto:=0;
       brutto:=0;
       srenu:=inttostr(renu);
       query:=format('select * from re_positionen where i_rechnung=%s',[srenu]);
       sql_new(true,q_7,query,'');
       q_7.first;
       while not datamodul.q_7.eof do
       begin
         db:=q_7.findfield('db').asinteger;
         cent:= getbigint(q_7,'cent');
         faktor:=q_7.findfield('faktor').asfloat  ;
         ust:=q_7.findfield('ust').asinteger;
         b1:=round(q_7.findfield('brutto').asfloat*100);


         if db=0 then //verweise
         begin
            netto:=netto +round(cent*faktor);
            brutto:=brutto+b1;
         end
         else
         begin
            nwert:=round(cent*faktor);
            netto:=netto+nwert;
            bwert:=round(nwert *(100+ust)/100);
            brutto:=brutto+bwert;
         end;
         q_7.next;
       end;


       query:=format('select * from rechnung where nummer=%s ',[srenu]);
       sql_new(true,q_7,query,'');

       q_7.edit;

       euronetto:=centtobetrag(netto);
       q_7['eurobetrag']:= euronetto; //

       //Eurobrutto:=round(brutto/100);
       Eurobrutto:=centtobetrag(brutto);
       q_7['brutto']:= eurobrutto; //

       q_7.post;
       

       parent:=getbigint(q_7,'i_parent');   //�bergeordnete Rechnu ng
       nummer:=getbigint(q_7,'nummer');      //rechnungsnummer
       //�bergeordnete re_position aktualisieren (nummer)
       query:=format('select nummer from re_positionen where i_link_rechnung=%d and storno=0',[nummer]);  //verweiswt auf obige rechnung
       srepo:=mysql_d.Feldinhalt(query,0);
       if (parent>0) and (srepo<>'') then
       begin
       	//neuer_datensatz(datamodul.q_repositionen,['nummer','i_firma','renu','db','dbnr', 're_text','faktor','cent','brutto','i_rechnung','i_link_rechnung'],
			//									  [null,i_firma,renu,9,0,rete,1,cent,s_betrag,renr_parent,renr_child]);
         //eurobrutto in string mit . umwandeln
         sbrutto:=floattosqlstring(eurobrutto);
         snetto:= floattosqlstring(euronetto);

         query:=format('update re_positionen set cent=%d, brutto=%s, netto=%s where nummer=%s ',[cent,sbrutto, snetto,srepo]);
         mysql_d.sql_exe(query);
         rechnung_aktualisieren(parent);
        end;
        q_repositionen.Locate('nummer',re_pos,[]) ;
       //uebergeordnete_rechnung_aktualisieren(parent);
end;
end;


procedure tform_main.rechnung_aktualisieren(renu:int64);
var
cent,b1,parent,nummer,bwert: int64;
pos_ust,db:integer;
pos_faktor:real;
euronetto,eurobrutto,s_mit_mws,s_ohne_mws,mws_faktor,pos_netto,pos_brutto,netto,brutto,nwert:currency;
//bm:tbookmark;
query,srepo,srenu,sbrutto, snetto,re_pos:string;
begin
mws_faktor:=mws/100;
with datamodul do
	begin
       //bm:=q_repositionen.GetBookmark;
       re_pos:=q_repositionen.findfield('nummer').asstring;
	    //q_repositionen.first;
       netto:=0;
       brutto:=0;
       s_mit_mws:=0;
       s_ohne_mws:=0;
       srenu:=inttostr(renu);
       query:=format('select * from re_positionen where i_rechnung=%s',[srenu]);
       sql_new(true,q_7,query,'');
       q_7.first;
       while not datamodul.q_7.eof do
       begin
         db:=q_7.findfield('db').asinteger;
         pos_netto:=myfloattocurr(q_7.findfield('netto').asfloat);
         pos_faktor:=q_7.findfield('faktor').asfloat  ;
         pos_ust:=q_7.findfield('ust').asinteger;
         pos_brutto:= myfloattocurr(q_7.findfield('brutto').asfloat);

         if db=0 then //verweise
         begin
            netto:=netto +(pos_netto*pos_faktor);
            brutto:=brutto+pos_brutto;
         end
         else
         begin
            nwert:=myfloattocurr(pos_netto*pos_faktor);
            if pos_ust>0 then
               s_mit_mws:=s_mit_mws+nwert
            else
               s_ohne_mws:=s_ohne_mws+nwert

         end;
         q_7.next;
       end;


       brutto:=brutto+myfloattocurr(s_mit_mws*(mws_faktor+1))+s_ohne_mws;
       netto:=s_mit_mws+s_ohne_mws;

       query:=format('select * from rechnung where nummer=%s ',[srenu]);
       sql_new(true,q_7,query,'');

       q_7.edit;

         q_7['eurobetrag']:= netto; //
         q_7['brutto']:= brutto; //

       q_7.post;


       parent:=getbigint(q_7,'i_parent');   //�bergeordnete Rechnu ng
       nummer:=getbigint(q_7,'nummer');      //rechnungsnummer
       //�bergeordnete re_position aktualisieren (nummer)
       query:=format('select nummer from re_positionen where i_link_rechnung=%d and storno=0',[nummer]);  //verweiswt auf obige rechnung
       srepo:=mysql_d.Feldinhalt(query,0);
       if (parent>0) and (srepo<>'') then
       begin
       	//neuer_datensatz(datamodul.q_repositionen,['nummer','i_firma','renu','db','dbnr', 're_text','faktor','cent','brutto','i_rechnung','i_link_rechnung'],
			//									  [null,i_firma,renu,9,0,rete,1,cent,s_betrag,renr_parent,renr_child]);
         //eurobrutto in string mit . umwandeln
         sbrutto:=floattosqlstring(brutto);
         snetto:= floattosqlstring(netto);
         cent:=round(netto*100);
         query:=format('update re_positionen set cent=%d, brutto=%s, netto=%s where nummer=%s ',[cent,sbrutto, snetto,srepo]);
         mysql_d.sql_exe(query);
         rechnung_aktualisieren(parent);
        end;
        q_repositionen.Locate('nummer',re_pos,[]) ;
       //uebergeordnete_rechnung_aktualisieren(parent);
end;
end;

procedure TForm_main.Lokalisation1Click(Sender: TObject);
begin
     if nicht_satellit_tab then exit;
     if b.b_string('Proband-Listen')>-1 then
     ambulanz_memo_add(1,false);
end;

procedure TForm_main.Verletzungsart1Click(Sender: TObject);
begin
     if nicht_satellit_tab then exit;
     if b.b_string('Proband-Listen')>-1 then
     ambulanz_memo_add(2,false);
end;

procedure TForm_main.Versorgung1Click(Sender: TObject);
begin
     if nicht_satellit_tab then exit;
     if b.b_string('Proband-Listen')>-1 then
   ambulanz_memo_add(3,false);
end;

procedure TForm_main.SpeedButton_d1Click(Sender: TObject);
begin
with datamodul do
try
  sql_new(false,q_1,'select * from wai_diagnosen', 'reihenfolge');
  form_treewahl:=tform_treewahl.create(self);
  form_treewahl.treeview.mysqlquery:=q_1;
  form_treewahl.treeview.mysql_feld:='diagnose';
  form_treewahl.treeview.liste_lesen;
  form_treewahl.treeview.autoexpand :=false;
  if form_treewahl.showmodal<>mrok then exit;
  if q_1['parent']= 0 then
  begin
       showmessage('Sie m�ssen eine Unterkategorie ausw�hlen');
       exit;
  end;
  if tspeedbutton(sender).name='SpeedButton_d1' then maskedit_d1.text:=q_1['ind_nr'];
  if tspeedbutton(sender).name='SpeedButton_d2' then maskedit_d2.text:=q_1['ind_nr'];
  if tspeedbutton(sender).name='SpeedButton_d3' then maskedit_d3.text:=q_1['ind_nr'];
  if tspeedbutton(sender).name='SpeedButton_d4' then maskedit_d4.text:=q_1['ind_nr'];
  if tspeedbutton(sender).name='SpeedButton_d5' then maskedit_d5.text:=q_1['ind_nr'];

finally
  form_treewahl.Release;
end;
end;

procedure TForm_main.BitBtn_waiClick(Sender: TObject);

procedure fehlt(text:string);
begin
  label_wai.caption:='Es fehlen Eingaben bei: '+text;
end;

function anzahl_psyche_wert:integer;
var i:integer;
begin
i:=4-combobox7.ItemIndex +4-combobox8.ItemIndex+4-combobox9.itemindex;
result:=0;
case i of
  0..3: result:=1;
  4..6:result:=2;
  7..9:result:=3;
  10..12:result:=4;
end;
end;

function anzahl_diagnosen_wert:integer;
var
leer: string;
i:integer;
begin
{Mind. 5 Krankheiten = 1 Punkt
4 Krankheiten =1  Punkt
3 Krankheiten = 3 Punkte
2 Krankheiten = 3   Punkte
1 Krankheit = 5 Punkte
0 Krankheiten = 7 Punkte}

leer:='';
Result:=0;
i:=0;
if trim(edit_d1.text)<>leer then inc(i);
if trim(edit_d2.text)<>leer then inc(i);
if trim(edit_d3.text)<>leer then inc(i);
if trim(edit_d4.text)<>leer then inc(i);
if trim(edit_d5.text)<>leer then inc(i);
case i of
  0: result:=7;
  1: result:=5;
  2: result:=3;
  3: result:=3;
  else
  result:=1;
end;

end;

var
wai:integer;
begin
// wai berechnen bzw fehlermeldung
edit_wai.SetFocus;

if combobox_arbeit.ItemIndex<0 then
   begin
   fehlt('Art der Arbeit');
   exit;
   end;

if combobox1.ItemIndex<0 then
   begin
   fehlt('derzeitige Arbeitsf�higkeit');
   exit;
   end;
wai:=10-combobox1.itemindex;
if combobox_arbeit.ItemIndex<0 then
   begin
   fehlt('Belastung der Arbeit');
   exit;
   end;
if combobox2.ItemIndex<0 then
   begin
   fehlt('Arbeitsf�higkeit in Relation zu den k�rperlichen Anforderungen');
   exit;
   end;
if combobox3.ItemIndex<0 then
   begin
   fehlt('Arbeitsf�higkeit in Relation zu den psychischen Anforderungen');
   exit;
   end;
case combobox_arbeit.ItemIndex of
  0:wai:=wai+round(1.5*(5-combobox2.itemindex) + 0.5*(5-combobox3.itemindex));
  1: wai:=wai+round(0.5*(5-combobox2.itemindex) + 1.5*(5-combobox3.itemindex));
  2:wai:=wai+(5-combobox2.itemindex + 5-combobox3.itemindex);
end;

wai:=wai+anzahl_diagnosen_wert;

if combobox4.ItemIndex<0 then
   begin
   fehlt('Behindert Sie eine Erkrankung oder Verletzung...');
   exit;
   end;
wai:=wai+6-combobox4.itemindex;

if combobox5.ItemIndex<0 then
   begin
   fehlt('Krankenstand');
   exit;
   end;
wai:=wai+5-combobox5.itemindex;

if combobox6.ItemIndex<0 then
   begin
   fehlt('Arbeitsf�higkeit in 2 Jahren');
   exit;
   end;
case combobox6.ItemIndex of
  0:wai:=wai+1;
  1:wai:=wai+4;
  2:wai:=wai+7;
end;

if combobox7.ItemIndex<0 then
   begin
   fehlt('Arbeit mit Freude erledigt');
   exit;
   end;

if combobox8.ItemIndex<0 then
   begin
   fehlt('Aktiv und rege');
   exit;
   end;
if combobox9.ItemIndex<0 then
   begin
   fehlt('Zuversichtlich');
   exit;
   end;
wai:=wai+ anzahl_psyche_wert;

edit_wai.text:=inttostr(wai);
//label_wai.caption:='';
if wai>30 then radiogroup_auffaellig.ItemIndex:=0 else radiogroup_auffaellig.ItemIndex:=1;
end;

procedure TForm_main.MaskEdit_d1Change(Sender: TObject);
var
nr:integer;
ind,dia,query:string;
begin
nr:=strtoint(copy(tmaskedit(sender).name,11,1));
case nr of
     1: ind:=maskedit_d1.Text;
     2: ind:=maskedit_d2.Text;
     3: ind:=maskedit_d3.Text;
     4: ind:=maskedit_d4.Text;
     5: ind:=maskedit_d5.Text;
end;
if length(trim(ind))<2 then exit;

query:='select diagnose from wai_diagnosen where ind_nr='+ind;
if strtoint(ind) in [1..51] then dia:=mysql_d.Feldinhalt(query,0)
   else dia:='';


case nr of
     1: edit_d1.Text:=dia;
     2: edit_d2.Text:=dia;
     3: edit_d3.Text:=dia;
     4: edit_d4.Text:=dia;
     5: edit_d5.Text:=dia;
end;


end;

procedure TForm_main.Edit_WAIChange(Sender: TObject);
var i:integer;
s1,s2:string;
begin
if edit_wai.text='' then
begin
     label_wai.Caption:='';
     exit;
end;
i:=strtoint(edit_wai.text);
case i of
0..27: begin
           label_wai.Caption:='Die Arbeitsf�higkeit ist kritisch';
           //label_wai.Color:=clred;
           s2:='Arbeitsf�higkeit wieder herstellen';
         end;
28..36: begin
           label_wai.Caption:='Die Arbeitsf�higkeit ist m��ig';
           s2:='Arbeitsf�higkeit verbessern';
         end;
37..43: begin
           label_wai.Caption:='Die Arbeitsf�higkeit ist gut';
           //label_wai.Color:=clred;
           s2:='Arbeitsf�higkeit unterst�tzen';
         end;
44..49: begin
           label_wai.Caption:='Die Arbeitsf�higkeit ist sehr gut';
           //label_wai.Color:=clgreen;
           s2:='Arbeitsf�higkeit beibehalten';
         end;
end;
end;

function goae_nn_normieren(goae:string):string;
begin
goae:=stringreplace(goae,' ','',[rfReplaceAll]);
goae:=stringreplace(goae,char(39),'',[rfReplaceAll]);
goae:=stringreplace(goae,',',char(39)+','+char(39),[rfReplaceAll]);
if goae<>'' then result:=char(39)+goae+char(39) else result:='';
end;

procedure TForm_main.DBEdit_StundenChange(Sender: TObject);
begin
//BitBtn_z_aktualisierenClick(self);
end;

procedure TForm_main.DBEdit15Change(Sender: TObject);
begin
if length(tdbedit(sender).text)>8 then tdbedit(sender).text:=copy((tdbedit(sender).text),12,6);
end;

procedure tform_main.taetigkeitanzeigen;
var
query,s:string;
i:int64;
begin
     edit_taetigkeit.text:='';
     i:=getbigint(datamodul.q_mitarbeiter, 'i_taetigkeit');
     query:='select taetigkeit from taetigkeiten where nummer='+inttostr(i);
     edit_taetigkeit.text:=mysql_d.Feldinhalt(query,0);
end;

procedure tform_main.abteilunganzeigen;
var
query,s:string;
i:int64;
begin
     edit_abteilung.text:='';
     edit_arbeitsplatz.text:='';
     i:=getbigint(datamodul.q_mitarbeiter, 'i_abteilung');
     query:='select kuerzel from abteilung where nummer='+inttostr(i);
     edit_abteilung.text:=mysql_d.Feldinhalt(query,0);

     i:=getbigint(datamodul.q_mitarbeiter, 'i_arbeitsplatz');
     query:='select name from a_platz where nummer='+inttostr(i);
     edit_arbeitsplatz.text:=mysql_d.Feldinhalt(query,0);

end;

procedure TForm_main.Optionen1Click(Sender: TObject);
begin
	speichern;
    pagecontrol_main.activepage:=tabsheet_firma;
    optionen_show(0);
    //inischreiben;
    sichtbarkeiten_einstellen;
    berechtigungen_einstellen;//neu
    //sichtbarkeiten_einstellen;
end;

procedure TForm_main.DBEdit_reverschChange(Sender: TObject);
begin
try
   if tdbedit(sender).text='' then exit;
   if strtodate(tdbedit(sender).text)=null_datum then tdbedit(sender).Visible:=false else tdbedit(sender).visible:=true;
except
end;
end;

function tform_main.sicherung(immer:boolean):boolean;
var
s:string;

begin
  if not immer then
	    if not backup_durchf then exit;

	s:=format('Ihre letzte Datensicherung stammt vom %s %sM�chten Sie jetzt eine Datensicherung durchf�hren? ',[last_backup, chr(13)]);
	if Messagedlg(s,mtConfirmation, [mbyes,mbno]	,56)<>mryes then exit;


	form_backup:=tform_backup.create(self);
  if immer then
  begin
     form_backup.CheckBox_ende.Visible:=false;
     form_backup.BitBtn_ok.Caption:='Weiter';
  end;
	form_backup.showmodal;
  result:=form_backup.f_sicher;
	form_backup.Release;
end;


procedure TForm_main.SpeedButton_diagn_bisClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_diagnosen.edit;
		datamodul.q_diagnosen.findfield('datum_bis').asdatetime :=int(form_kalender.auswahlkalender.date);
   end;
end;

 procedure TForm_main.baum_sichtbarkeiten(check:boolean);
 begin
    m_befunde_c:=check;
    m_vorsorge_c:=check;
	  m_impfung_C:=check;
    m_labor_C:=check;
	  m_ambulanz_c:=check;
	  m_diagnosen_c:=check;
	  m_besonderheiten_c:=check;
	  m_dokumente_C:=check;
    m_artikel_c:=check;
    m_rechnungen_c:=check;
 end;

procedure TForm_main.m_kapitelClick(Sender: TObject);
var
query:string;
begin
if nicht_satellit_tab then exit;
try
   query:='select * from kapitel where nummer=0';
	 datamodul.sql_new(false,datamodul.q_1,query,'');
	 form_kapitel:=tform_kapitel.create(self);
	 //form_kapitel.comboBox.itemindex:=1;
	 //form_kapitel.ComboBoxChange(sender);
	 form_kapitel.dbgrid.DataSource:=datamodul.ds_1;
   form_kapitel.Showmodal;
finally
    form_kapitel.release;
    alle_kapitel_lesen;
end;
end;

procedure tform_main.kapitel_lesen(liste: tstrings; bereich:integer);
var query,s:string;
begin
  liste.Clear;
	//query:=format('select * from kapitel where bereich=%d',[bereich]);
	query:='select * from kapitel' ;
  datamodul.sql_new(true,datamodul.q_7,query,'kapitel');
  datamodul.q_7.first;
  while not datamodul.q_7.eof do
  begin
       s:=datamodul.q_7.findfield('kapitel').asstring;
       liste.Add(s);
       datamodul.q_7.next;
  end;
end;

procedure tform_main.alle_kapitel_lesen;
begin
	dbcombo_num_befunde.lookup_laden;
	dbcombo_num_dokumente.lookup_laden;
  dbcombo_num_texte.lookup_laden;
	dbcombo_num_besonderheiten.lookup_laden;

end;


procedure TForm_main.BitBtn_kontaktimportClick(Sender: TObject);
var
k:pkontaktpointer;
ind:int64;
begin
with datamodul do begin
ind:=0;
k:=aerzteanzeigen(false,ind);
q_kontakte.edit;
q_kontakte.findfield('name').asstring:=k.name;
q_kontakte.findfield('vorname').asstring:=k.vorname;
q_kontakte.findfield('titel').asstring:=k.titel;
q_kontakte.findfield('zhd').asstring:=k.zhd;
q_kontakte.findfield('strasse').asstring:=k.strasse;
q_kontakte.findfield('plz').asstring:=k.plz;
q_kontakte.findfield('ort').asstring:=k.ort;
q_kontakte.findfield('fachgebiet').asstring:=k.fach;
q_kontakte.findfield('email').asstring:=k.email;
q_kontakte.findfield('telefon').asstring:=k.telefon;
q_kontakte.findfield('fax').asstring:=k.fax;
q_kontakte.findfield('www').asstring:=k.www;
q_kontakte.findfield('memo').asstring:=k.memo;
dispose(k);
end;
end;

procedure tform_main.tbs(modus:integer;te:tobject);
var
query,lz:string;
begin
//�0:men�
//1: kein men�
try

	query:='select * from textbausteine where nummer=0';
	datamodul.sql_new(true,datamodul.q_6,query,'');

   query:='select * from kapitel';
	datamodul.sql_new(true,datamodul.q_7,query,'');

	Form_textbausteine:=TForm_textbausteine.create(self);
  form_textbausteine.CheckBox_alpha.checked:= form_textbausteine_CheckBox_alpha_checked;
	Form_textbausteine.helpcontext:=12200;
  form_textbausteine.modus:=modus;
	case modus of
	  0:	begin
			  Form_textbausteine.Panel_bottom.Visible:=false;
		  end;
	  1:	begin
			  Form_textbausteine.ToolBar_main.Visible:=false;
        Form_textbausteine.GroupBox_thema.Visible:=false;
		  end;
    2: begin
          form_textbausteine.GroupBox_kapitel.Visible:=false;
          form_textbausteine.GroupBox_thema.visible:=false;
          Form_textbausteine.Panel_bottom.Visible:=false;
          form_textbausteine.i_kapitel:=-1;//labor
          form_textbausteine.filtern;
       end;
    3: begin
          form_textbausteine.GroupBox_kapitel.Visible:=false;
          form_textbausteine.GroupBox_thema.visible:=false;
          Form_textbausteine.Panel_bottom.Visible:=true;
          Form_textbausteine.ToolBar_main.Visible:=false;
          form_textbausteine.i_kapitel:=-1;//labor
          form_textbausteine.filtern;
       end;
      4: begin
          //form_textbausteine.GroupBox_kapitel.Visible:=false;
          form_textbausteine.GroupBox_thema.visible:=false;
          Form_textbausteine.Panel_bottom.Visible:=true;
          Form_textbausteine.ToolBar_main.Visible:=false;
          //form_textbausteine.i_kapitel:=-1;//labor
          form_textbausteine.filtern;
       end;
	end;

    //nur wenn Dokument ge�ffnet also modus m�sste 1 sein

	if (modus=1) and (pagecontrol_main.ActivePage=tabsheet_main) and (notebook_main.pageindex=8) then
    begin

	  form_textbausteine.ComboBox_thema.ItemIndex:=form_textbausteine.ComboBox_thema.Items.IndexOf(dbcombo_num_dokumente.text);
	  //form_textbausteine.ComboBox_kapitel.text :=dbcombobox_dokumente_kapitel.text;
	  form_textbausteine.ComboBox_themaChange(self);
	end;

  if (modus=1) and (pagecontrol_main.ActivePage=tabsheet_firma)  then
	begin
	  form_textbausteine.ComboBox_thema.ItemIndex:=  form_textbausteine.ComboBox_thema.Items.IndexOf(dbcombo_num_texte.text);
	  //form_textbausteine.ComboBox_kapitel.text :=dbcombobox_dokumente_kapitel.text;
	  form_textbausteine.ComboBox_themaChange(self);
	end;

	form_textbausteine.ShowModal;
finally
  if form_textbausteine.text<>'' then
  begin
    if modus=1 then
    begin
      com.texteinfuegen(form_textbausteine.text,false);
      if form_main.arbeitsmedizin_Proband.State in [osopen, osrunning]then olecontainer_anzeigen(form_main.arbeitsmedizin_Proband);
      if form_main.arbeitsmedizin_firma.State in [osopen, osrunning]then olecontainer_anzeigen(form_main.arbeitsmedizin_firma);
    end;
    if modus=3 then
    begin
      if  form_main.DBMemo_ergebnistext.Text<>'' then lz:=' ' else lz:='';
      if  form_main.DBMemo_ergebnistext.Text<>'' then lz:=' ' else lz:='';
      dbmemo_ergebnistext.SetFocus;
      DBMemo_ergebnistext.Text:= form_main.DBMemo_ergebnistext.Text+lz+form_textbausteine.text;
    end;
    if modus=4 then
    begin
      if  tdbmemo(te).text<>'' then lz:=' ' else lz:='';
      tdbmemo(te).text:= tdbmemo(te).text+lz+form_textbausteine.text;
    end;
  end;

  form_textbausteine_CheckBox_alpha_checked:=form_textbausteine.CheckBox_alpha.checked;
  Form_textbausteine.Release;
end;
end;

procedure TForm_main.systemExecuteMacro(Sender: TObject;
	Msg: TStrings);
	var m:string;
begin
if msg[0]='tbs' then  tbs(1,nil);
if msg[0]='doc' then  ha_einfuegen;

end;

procedure TForm_main.m_TextbausteineClick(Sender: TObject);
begin
	tbs(1,nil); //kein Men�
end;

procedure TForm_main.Textbausteine1Click(Sender: TObject);
begin
tbs(0,nil);//men�
end;

function n_tes_wort(n:integer;trenner,wort:string):string;
var
	position1,position2,position,i:integer;
begin
	wort:=trim(wort);
	for i:=1 to n do
	begin
		position1:=pos(trenner,wort);
		position2:=pos(' ',wort);
		if position1*position2=0 then position:=max(position1,position2)
		else position:=min( position1, position2);
		if position>0 then dec(position) else position:=length(wort);
		result:=copy(wort,1,position);
		if position<length(wort) then wort:=copy(wort,position+2,length(wort));
		wort:=trim(wort);
	end;
end;

procedure TForm_main.m_Export_ProbandenbersichtClick(
  Sender: TObject);
var
f_name, html_name,pfad,query:string;
us:thtml_uebersicht;
sma_nummer,oleklasse,datname,sj:string;
i,j,fo: integer;
begin
fo:=0;
if (b.b_string('Auswertung-Gesamt')<0) then exit;

if not (pagecontrol_main.ActivePage=tabsheet_stammdaten)  then
begin
  showmessage('Bitte w�hlen Sie den Kartenreiter "Probanden" aus.');
  exit;
end;

if akt_abteilung +akt_arbeitsplatz >0 then
  if Messagedlg('Achtung es werden nicht die Probanden von allen Abteilungen / Arbeitspl�tzen angezeigt - trotzdem weiter? ',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;

try
	form_pfadwahl:=tform_pfadwahl.create(self);
	form_pfadwahl.Edit_uv.Visible:=true;
  form_pfadwahl.button_anlegen.visible:=true;
  form_pfadwahl.DirectoryListBox.Directory:= pfad_temp;
	if form_pfadwahl.ShowModal=mrok then
	begin

	  pfad:=form_pfadwahl.DirectoryListBox.Directory;

    if Messagedlg(format('Sollen die Dateien der angezeigten Probanden in das Verzeichnis %s exportiert werden?',[pfad]),mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;

    //if Messagedlg('Sollen die Word-Dateien statt im RTF-Format exportiert werden? (Standard ist doc)',mtConfirmation, [mbyes,mbno],0)=mrok then fo:=6 else
    fo:=0;
    pagecontrol_main.ActivePage:=tabsheet_stammdaten;

	  datamodul.q_mitarbeiter.first;
		 while not datamodul.q_mitarbeiter.eof do
		 begin

			  //speichern der �bersicht;
        pagecontrol_main.ActivePage:=tabsheet_main;
         namenanzeigen;
	      tree_befunde_filtern(false);  //hier auch Anamnese laden
	      dbtreeview.Expand_e1;
        //PageControl_mainChange(self);
        f_name:=pfad+'\'+datamodul.q_mitarbeiter['name']+'-'+datamodul.q_mitarbeiter['vorname']+'-'+datetostr(datamodul.q_mitarbeiter['geb_dat'])+'-0001.html';
			  us:=thtml_uebersicht.create(f_name);
			  us.report(dbtreeview.Items,0,'','',0);
			  us.free;
        //pagecontrol_main.ActivePage:=tabsheet_stammdaten;
			  //datamodul.q_mitarbeiter.Next;

        
        // speichern der ole-dokumente
        //speziell word und pdf
        sma_nummer:=datamodul.q_mitarbeiter.findfield('nummer').asstring;
        if b.b_string_silent('proband-rechnung')<=0 then
          query:=format('select * from dokumente where i_mitarbeiter=%s and modus=0 order by datum ',[sma_nummer])
        else
          query:=format('select * from dokumente where i_mitarbeiter=%s order by datum ',[sma_nummer]);

        datamodul.sql(true,datamodul.q_1,query,'');
        datamodul.q_1.first;
        j:=1;
        while not datamodul.q_1.Eof do
        begin
         inc(j);
         try   
           olecontainer_close(arbeitsmedizin_Proband);
           olecontainer_destroy(arbeitsmedizin_Proband);
           olecontainer_laden(datamodul.q_1,'ole',arbeitsmedizin_Proband);
           //if  not (arbeitsmedizin_proband.state in [osrunning,osOpen	]) then 	olecontainer_anzeigen(arbeitsmedizin_Proband);
           oleklasse:=arbeitsmedizin_Proband.oleclassname;
           i:=pos('.',oleklasse);
           oleklasse:=copy(oleklasse,1,i-1);
           sj:='000'+inttostr(j);
           sj:=stringwenden(sj);
           sj:=copy(sj,1,4);
           sj:=stringwenden(sj);

           datname:= pfad+'\'+datamodul.q_mitarbeiter['name']+'-'+datamodul.q_mitarbeiter['vorname']+'-'+ datamodul.q_mitarbeiter.findfield('geb_dat').asstring+'-'+sj+'-'+datamodul.q_1.findfield('datum').asstring ;

           if lowercase(oleklasse)='word' then
           begin
              if  not (arbeitsmedizin_proband.state in [osrunning,osOpen	]) then 	olecontainer_anzeigen(arbeitsmedizin_Proband);

              //datname:= pfad+'\'+datamodul.q_mitarbeiter['name']+'-'+datamodul.q_mitarbeiter['vorname']+'-'+ datamodul.q_mitarbeiter.findfield('geb_dat').asstring+'-';


              if fo=6 then   //bisher immer fo:=0
              begin
                //datname:=datname+inttostr(j)+'-'+datamodul.q_1.findfield('datum').asstring +'.rtf';
                datname:=datname+'.rtf';
                arbeitsmedizin_Proband.OleObject.application.activedocument.saveas(Filename:=datname,FileFormat:=6);
              end
              else
              begin
                //datname:=datname+inttostr(j)+'-'+datamodul.q_1.findfield('datum').asstring +'.doc';
                datname:=datname+'.doc';
                arbeitsmedizin_Proband.OleObject.application.activedocument.saveas(Filename:=datname,FileFormat:=0);
              end;
           end
           else
           //if lowercase(oleklasse)='acroexch' then
           begin
              //datname:= pfad+'\'+datamodul.q_mitarbeiter['name']+'-'+datamodul.q_mitarbeiter['vorname']+'-'+ datamodul.q_mitarbeiter.findfield('geb_dat').asstring+'-';
              //datname:=datname+inttostr(j)+'-'+datamodul.q_1.findfield('datum').asstring +'.pdf';
              datname:=datname+'.pdf';
              SaveOleContainerPDF(arbeitsmedizin_Proband,datname);
           end;


           olecontainer_close(arbeitsmedizin_Proband);
           olecontainer_destroy(arbeitsmedizin_Proband);
           //showmessage(oleklasse);
         except
         end;

         Datamodul.q_1.Next;
        end;
      application.BringToFront;
      application.ProcessMessages;
		  datamodul.q_mitarbeiter.Next;
		 end;
	 end;
finally

	form_pfadwahl.Release;
  application.BringToFront;
end;


end;


function SaveOleContainerPDF(OleContainer: TOleContainerext; Location: string): int64;
var
  FileStream: TFileStream;
  OleStream: TStringStream;
  p1,p2,p3,i,o:integer;
  s:string;
  sa:array[1..3]of string;
begin
  Result := 0;
  sa[1]:='%%EOF'+chr($0D)+chr($0A);
  sa[2]:='%%EOF'+chr($0D);
  sa[3]:='%%EOF'+chr($0A);
  
  OleStream := TStringStream.Create('');
  OleContainer.SaveToStream(OleStream);
  try
    try
      if Pos('%PDF', OleStream.DataString) > 0 then
        begin
          try
          FileStream := TFileStream.Create(Location, fmCreate);

          s:='%%EOF';
          p1:=posex(s, OleStream.DataString,1);
          while p1>0 do
          begin
           p2:=p1;
           p1:=posex(s, OleStream.DataString,p1+1);
          end;
          //p2 letztes %%eof

          i:=0;
          repeat
            inc(i);
            p3:=posex(sa[i], OleStream.DataString,p2);
          until ((i>=3) or (p3>0));


          if p3=0 then o:=4
          else
          case i of
            1:o:=2;
            2:o:=1;
            3:o:=1;
          end;

          p2:=p2+4+o;

          OleStream.Position := Pos('%PDF', OleStream.DataString)-1;
          Result := FileStream.CopyFrom(OleStream, p2 - OleStream.Position);  //OleStream.Size
          finally
              FileStream.Free;
          end;
        end;
    except
      // handle exceptions
    end;
  finally
    OleStream.Free;
  end;
end;


function SaveOleContainer_word_PDF(OleContainer: TOleContainerext; Location: string): int64;
var
  FileStream: TFileStream;
  OleStream: TStringStream;
  p1,p2,p3,i,o:integer;
  s:string;
  sa:array[1..3]of string;
begin
  Result := 0;
  sa[1]:='%%EOF'+chr($0D)+chr($0A);
  sa[2]:='%%EOF'+chr($0D);
  sa[3]:='%%EOF'+chr($0A);
  
  OleStream := TStringStream.Create('');
  FileStream := TFileStream.Create(Location, fmCreate);
  OleContainer.SaveToStream(OleStream);
  OleStream.Position:=1;
  Result := FileStream.CopyFrom(OleStream, OleStream.Size-1);  //OleStream.Size
  OleStream.Free;
  filestream.Free;
  {
  try
    try
      if Pos('%PDF', OleStream.DataString) > 0 then
        begin
          try
          FileStream := TFileStream.Create(Location, fmCreate);

          s:='%%EOF';
          p1:=posex(s, OleStream.DataString,1);
          while p1>0 do
          begin
           p2:=p1;
           p1:=posex(s, OleStream.DataString,p1+1);
          end;
          //p2 letztes %%eof

          i:=0;
          repeat
            inc(i);
            p3:=posex(sa[i], OleStream.DataString,p2);
          until ((i>=3) or (p3>0));


          if p3=0 then o:=4
          else
          case i of
            1:o:=2;
            2:o:=1;
            3:o:=1;
          end;

          p2:=p2+4+o;

          OleStream.Position := Pos('%PDF', OleStream.DataString)-1;
          Result := FileStream.CopyFrom(OleStream, p2 - OleStream.Position);  //OleStream.Size
          finally
              FileStream.Free;
          end;
        end;
    except
      // handle exceptions
    end;
  finally
    OleStream.Free;
  end;}
end;



procedure TForm_main.DBgridEXT_namenColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
begin
  namenanzeigen;
end;


procedure TForm_main.BitBtn_leistungswahlClick(Sender: TObject);
begin
leistungsauswahl('aufgabe');
end;


procedure TForm_main.m_AbrechnungsanlaesseClick(Sender: TObject);

begin
 if b.b_string('Firmen-Listen')>-1 then
leistung_edit;


end;

procedure TForm_main.SpeedButton_z_bisClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
begin
     if  not (datamodul.q_firma_zeiten_soll.state in [dsedit, dsinsert]) then  datamodul.q_firma_zeiten_soll.Edit;
     datamodul.q_firma_zeiten_soll['bis'] :=(form_kalender.auswahlkalender.date);
     firma_zeitvorgaben_berechnen;

end;
end;

procedure TForm_main.SpeedButton_z1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	begin
   if sender=speedbutton_z1 then
   MaskEdit_z1.Text:=datetostr(form_kalender.auswahlkalender.date)
   else
   MaskEdit_z2.Text:=datetostr(form_kalender.auswahlkalender.date);
  end;

end;

procedure TForm_main.SpeedButton_zfilternClick(Sender: TObject);
begin
	einsatzzeiten_filtern;
end;

procedure tform_main.einsatzzeiten_filtern;
var
d1,d2: tdate;
s,query,i_firma,ds1,ds2,s_krit,s_krit_query,s_zeitkonto,s_zeitkonto_query:string;
h,m,h1,h2,m1,m2,i_krit,i_zeitkonto,i_krit_p, i_zeitkonto_p:integer;
datvon, datbis,p:tdatetime;
t1,t2,t3:real;
sd:real;
begin
t1:=0;
t2:=0;
try
d1:=strtodate(maskedit_z1.Text);
d2:=strtodate(maskedit_z2.Text);
except
	showmessage('kein g�ltiges Datum');
    exit;
    //ComboBox_zeiterfassungChange(Sender);
end;

s_krit:=ComboBox_zeit_kriterium.Text ;
query:=format('select nummer from abr_anlass_krit where name="%s"',[s_krit]);

s_krit_query:=mysql_d.Feldinhalt(query,0);
if s_krit_query<>'' then
begin
    i_krit:=strtoint(s_krit_query);
	s_krit_query:='and i_kriterium='+s_krit_query;
end
else
begin
   i_krit:=-1;
   s_krit_query:='';
end;


s_zeitkonto:=ComboBox_zeit_konto.Text ;
query:=format('select nummer from abr_zeitkonto where name="%s"',[s_zeitkonto]);

s_zeitkonto_query:=mysql_d.Feldinhalt(query,0);

if s_zeitkonto_query<>'' then
begin
    i_zeitkonto:=strtoint(s_zeitkonto_query);
	s_zeitkonto_query:='and i_zeitkonto='+s_zeitkonto_query;
end
else
begin
   i_zeitkonto:=-1;
   s_zeitkonto_query:='';
end;

i_firma:=datamodul.q_firma.findfield('nummer').asstring;
if i_firma='' then exit;
ds1:=sql_datetostr(d1);
ds2:=sql_datetostr(d2);
h:=0;
m:=0;
if checkbox_zeit.Checked then
query:=format('select *, left(anlass,250) as anlass_text from firma_sollist where i_firma=%s %s %s and datum>= %s and datum <=%s and storno=0 order by i_kriterium, anlass',[i_firma,s_krit_query,s_zeitkonto_query,ds1,ds2])
else
query:=format('select *, left(anlass,250) as anlass_text from firma_sollist where i_firma=%s %s %s and datum>= %s and datum <=%s and storno=0 order by datum desc, ts desc',[i_firma,s_krit_query,s_zeitkonto_query,ds1,ds2]);

datamodul.sql(false,datamodul.q_firma_sollist,query,'');

query:=format ('select sum(stunden*zeitansatz/100)  from firma_sollist where i_firma= %s %s %s and datum>= %s and datum<= %s and storno=0', [i_firma,s_krit_query,s_zeitkonto_query,ds1,ds2 ]);
datamodul.sql_new(false,datamodul.q_3,query, '');
s:=mysql_d.Feldinhalt(query,0);

if s<>'' then
begin
 t3:=strtofloat(s);
 h:=trunc(t3);
 m:=round((t3-h)*60);
end;


query:=format ('select sum(minuten*zeitansatz/100)  from firma_sollist where i_firma= %s %s %s and  datum>= %s and datum<= %s and storno=0', [i_firma,s_krit_query,s_zeitkonto_query,ds1,ds2 ]);
datamodul.sql_new(false,datamodul.q_3,query, '');
s:=mysql_d.Feldinhalt(query,0);
if s<>'' then
begin
  t3:=strtofloat(s);
  m:=m+round(t3) ;
end;

h:=h+m div 60;
m:=m mod 60;
t1:=h+m/60;

// sollzeiten

{t2:=0;

query:=format('select  * from firma_zeiten_soll  where (i_firma=%s ) ',[i_firma]);


if i_krit<>-1 then
begin
	query:=query+ format('and  i_abrechnungskriterium=%d ', [i_krit]);
end;
if i_zeitkonto<>-1 then
begin
	query:=query+ format(' and  i_zeitkonto=%d ', [i_zeitkonto]);
end;
datamodul.sql_new(true,datamodul.q_firma_zeiten_soll,query,'von');

DataModul.q_firma_zeiten_soll.first;

while not  DataModul.q_firma_zeiten_soll.eof do
begin
    i_krit_p:=DataModul.q_firma_zeiten_soll.findfield('i_abrechnungskriterium').asinteger;
    i_zeitkonto_p:=DataModul.q_firma_zeiten_soll.findfield('i_zeitkonto').asinteger;
    datvon:=DataModul.q_firma_zeiten_soll.findfield('von').asdatetime;
    datbis:=DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime;
    sd:=DataModul.q_firma_zeiten_soll.findfield('einsatzstunden').asfloat/(trunc(DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime) -trunc( DataModul.q_firma_zeiten_soll.findfield('von').asdatetime));



    if (d1>=datvon) and (d1<=datbis) and (d2>=datbis )   then
    begin
    	p:=datbis-d1;
    end;
    if (d1>=datvon) and (d2<=datbis ) then
    begin
    	p:=d2-d1;
    end;
    if (d1<=datvon ) and (d2<=datbis ) and  (d2>=datvon ) then
    begin
    	p:=d2-datvon;
    end;
    if (d1<=datvon) and (d2>=datbis ) then
    begin
    	p:=datbis-datvon;
    end;

    t2:=t2+p*sd;
	DataModul.q_firma_zeiten_soll.next;
end;  }

t2:=sollzeiten(i_firma, d1,d2,i_krit, i_zeitkonto);

t2:=t2-t1;



edit_z_gel.Text:=FloatToStrF(t1,fffixed,7,2) ;
edit_z_fehl.text:=FloatToStrF(t2,fffixed,7,2) ;



end;

function tform_main.sollzeiten(s_firmen: string; d1,d2: tdate;i_krit, i_zeitkonto:integer):real;
var
query: string;
datvon, datbis:tdate;
p,sd:real;
i_krit_p, i_zeitkonto_p:integer;
begin
result:=0;

query:=format('select  * from firma_zeiten_soll  where (i_firma in (%s) )  ',[s_firmen]);

if i_krit<>-1 then
begin
	query:=query+ format('and  i_abrechnungskriterium=%d ', [i_krit]);
end;
if i_zeitkonto<>-1 then
begin
	query:=query+ format(' and  i_zeitkonto=%d ', [i_zeitkonto]);
end;
datamodul.sql_new(true,datamodul.q_firma_zeiten_soll,query,'von');

dataModul.q_firma_zeiten_soll.DisableControls;
DataModul.q_firma_zeiten_soll.first;

while not  DataModul.q_firma_zeiten_soll.eof do
begin
    i_krit_p:=DataModul.q_firma_zeiten_soll.findfield('i_abrechnungskriterium').asinteger;
    i_zeitkonto_p:=DataModul.q_firma_zeiten_soll.findfield('i_zeitkonto').asinteger;
    datvon:=DataModul.q_firma_zeiten_soll.findfield('von').asdatetime;
    datbis:=DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime;
    if (trunc(DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime) -trunc( DataModul.q_firma_zeiten_soll.findfield('von').asdatetime)) >0 then
    sd:=DataModul.q_firma_zeiten_soll.findfield('einsatzstunden').asfloat/(trunc(DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime) -trunc( DataModul.q_firma_zeiten_soll.findfield('von').asdatetime));


    p:=0;
    if (d1>=datvon) and (d1<=datbis) and (d2>=datbis )   then
    begin
    	p:=datbis-d1;
    end;
    if (d1>=datvon) and (d2<=datbis ) then
    begin
    	p:=d2-d1;
    end;
    if (d1<=datvon ) and (d2<=datbis ) and  (d2>=datvon ) then
    begin
    	p:=d2-datvon;
    end;
    if (d1<=datvon) and (d2>=datbis ) then
    begin
    	p:=datbis-datvon;
    end;

    result:=result+p*sd;
	DataModul.q_firma_zeiten_soll.next;
end;
dataModul.q_firma_zeiten_soll.EnableControls;

end;



procedure TForm_main.ImportberBDT1Click(Sender: TObject);
begin
//
form_bdt:=tform_bdt.create(self);
form_bdt.showmodal;

form_bdt.release;
//datamodul.q_labor.Refresh;

end;

function preis_von_goae(goae:string):t5stringarray;
var
query:string;
begin
try
query:=format('select punkte,goae_text,goae_nn, goae_typ from goae where goae_nummer="%s" and storno=0',[goae]);
 mysql_d.feldarray(query,[0,1,2,3,4],result);
except
	showmessage('GO� '+goae+' nicht gefunden. Bitte in der GO�-Liste nachtragen');
    
end;

end;




procedure TForm_main.notebook_mainPageChanged(Sender: TObject);
begin
case notebook_main.pageindex of
1: tabsheet_main.HelpContext:=7; //befunde
2: tabsheet_main.HelpContext:=23;   //vorsorge
3: tabsheet_main.HelpContext:=26;  //impfung
4: tabsheet_main.HelpContext:=24; //labor
5: tabsheet_main.HelpContext:=33;  //ambulanz
6: tabsheet_main.HelpContext:=8;//diagnose
7: tabsheet_main.HelpContext:=20;//besonderheit
8: tabsheet_main.HelpContext:=28;  //dokumente
10: tabsheet_main.HelpContext:=40; //rechnung
11: tabsheet_main.HelpContext:=0;//artikel
15: tabsheet_main.HelpContext:=64;//editor


end;
end;

procedure TForm_main.ComboBox_zeiterfassungChange(Sender: TObject);
begin
	einsatzZeiteneinstellen;
end;

procedure TForm_main.einsatzZeiteneinstellen;
var dat:tdate;
	yy,m,d:word;
	yl,y,a,e:string;
begin
decodedate(date,yy,m,d);
y:=inttostr(yy);
yl:=inttostr(yy-1);
if combobox_zeiterfassung.ItemIndex=-1 then combobox_zeiterfassung.ItemIndex:=0;
case combobox_zeiterfassung.ItemIndex of
0:begin
	a:='01.01.'+y;
    e:='31.12.'+y;
  end;
1:begin
	a:='01.01.'+y ;
    e:='31.03.'+y ;
  end;
2:begin
    a:='01.04.'+y;
    e:='30.06.'+y;
  end;
3:begin
	a:='01.07.'+y;
    e:='30.09.'+y;
  end;
4:begin
	a:='01.10.'+y;
    e:='31.12.'+y;
  end;

5:begin
	a:='01.01.'+yl;
    e:='31.12.'+yl;
  end;
6:begin
	a:='01.01.'+yl ;
    e:='31.03.'+yl ;
  end;
7:begin
    a:='01.04.'+yl;
    e:='30.06.'+yl;
  end;
8:begin
	a:='01.07.'+yl;
    e:='30.09.'+yl;
  end;
9:begin
	a:='01.10.'+yl;
    e:='31.12.'+yl;
  end;
10:begin
	a:='01.01.2000';
    //e:=DateToStr(Now);
    e:='31.12.9999';
  end;
end;


MaskEdit_z1.Text:=a;
MaskEdit_z2.Text:=e;

einsatzzeiten_filtern;
//SpeedButton_zfilternClick(Sender);
end;

function betrag(r:real ):real;
begin
r:=r*100;
r:=round(r);
result:=r/cent_mult;
end;

function centtobetrag(i:integer ):real;
begin
result:=myfloattocurr(i/100);
end;

function myfloattocurr(r:real):real;
begin
  r:=r*100;
  r:=round(r);
  result:=r/100;
end;

procedure TForm_main.m_AbrechnungskriteriumClick(Sender: TObject);

var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:= b.b_string('Firmen-Listen');
if ber=-1 then exit;

speichern;
form_tabellen:=tform_tabellen.Create(self);
form_tabellen.HelpContext:=4;
form_tabellen_berechtigung(ber);

form_tabellen.suchfeld:='Abrechnungskriterien';
form_tabellen.Caption:='Abrechnungskriterien';

form_tabellen.Notebook.PageIndex:=19;
form_tabellen.label_string.Caption:='Abrechnungskriterium';
form_tabellen.SpeedButton_stringhoch.Visible:=true;
form_tabellen.SpeedButton_stringrunter.Visible:=true;

datamodul.sql(true,datamodul.q_1,'select * from abr_anlass_krit','reihenfolge');
form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='name';
form_tabellen.DBGrid_tabellen.Columns[0].width:=500;
form_tabellen.DBGrid_tabellen.Columns[1].width:=0;
form_tabellen.DBEdit_string.DataField:='name';
grid_einstellen(form_tabellen.dbgrid_tabellen);

form_tabellen.showmodal;
form_tabellen.release;

dbcombo_laden;
end;


procedure TForm_main.BitBtn_redurckmaClick(Sender: TObject);
begin
   redruck(11);
end;

procedure TForm_main.BitBtn_redruck_fiClick(Sender: TObject);
var
strdat:string;
begin
   redruck(10);
	 datamodul.q_rechnung.edit;
	 datamodul.q_rechnung.FindField('dat_verschickt').AsDateTime:=now();
     direktspeichern:=true;
	 datamodul.q_rechnung.post;
end;

procedure TForm_main.DBRadioGroup_beurteilung_eignungChange(Sender: TObject);
begin
//if DBRadioGroup_bewertung.ItemIndex>=0 then DBCombo_num_vor_status.Enabled:=true else DBCombo_num_vor_status.Enabled:=false;
end;

procedure TForm_main.DBCombo_num_vor_statusChange(Sender: TObject);
begin

  if (notebook_vorsorge.pageindex =0) and (DBRadioGroup_beurteilung_eignung.ItemIndex=-1) and (DBCombo_num_vor_status.ItemIndex=3) then messagedlg('Achtung, Eignung noch nicht beurteilt',mtWarning ,[mbok],0	);
  if (notebook_vorsorge.pageindex =1) and (DBRadioGroup_beurteilung_arbmedvv.ItemIndex=-1) and (DBCombo_num_vor_status.ItemIndex=3) then messagedlg('Achtung, Vorsorge noch nicht beurteilt',mtWarning ,[mbok],0	);

end;

procedure TForm_main.SpeedButton4Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
 		datamodul.q_rechnung['datum'] :=int(form_kalender.auswahlkalender.date);
     end;   
end;

procedure protokoll(art, funk, aktion: string; wf:boolean);
var
   query,db,un:string;
   w,dbver,lf,la,lu:integer;
begin

   if art='user' then db:='protokoll_user' else db:='protokoll_sys';


   query:='select i1 from haupt_tabelle';
   dbver:=strtoint(mysql_d.Feldinhalt(query,0));
   if dbver<694 then
   begin
    lf:=5;
    la:=255;
    lu:=7;
    end
   else
   begin
    lf:=255;
    la:=1000;
    lu:=255;
   end;

   //aktion:=sonderzeichenraus(aktion);  //sonst
   aktion:=stringreplace(aktion,'"','|',[rfreplaceall]);
   aktion:=stringreplace(aktion,char(39),'|',[rfreplaceall]);
   funk:=copy(funk,1,lf);
   aktion:=copy(aktion,1,la);
   un:=copy(user_name,1,lu);

   if wf then w:=1 else w:=0;
   query:=format('insert %s set funk="%s" , aktion="%s", resultat=%d,user_name="%s"',[db, funk,aktion,w,un]);
   try
   	mysql_d.sql_exe_still(query);
   except
   	//wenn prot-dat noch nicht angelegt ist
    showmessage('Nicht in Protokolldat. eingetragen: '+query);
   end;
end;

function sonderzeichenraus_all(f:string):string;
var sz:string;
   i,p:integer;
begin
   f:=trim(f);//Leerzeichen weg
   sz:='"~�^|�`.,;:!�$%&/()=?{}[] '+chr(39);
   for i:=1 to length(sz) do
   f:=stringreplace(f,copy(sz,i,1),'',[rfreplaceall]);
   result:=f;
end;

function sonderzeichenraus(f:string):string;
var sz:string;
   i,p:integer;
begin
   f:=trim(f);//Leerzeichen weg
   sz:='"~�^|�` '+chr(39);
   for i:=1 to 9 do
   f:=stringreplace(f,copy(sz,i,1),'',[rfreplaceall]);
   result:=f;
end;

function ersetzesonderzeichen(f:string):string;
var sz:string;
   i,p:integer;
begin
   f:=trim(f);//Leerzeichen weg
   sz:='"~�^|�`:. '+chr(39);
   for i:=1 to 10 do
   f:=stringreplace(f,copy(sz,i,1),'_',[rfreplaceall]);
   result:=f;
end;



procedure pause(Milliseconds: Integer);
var
 Tick: DWord;
 Event: THandle;
begin
 Event := CreateEvent(nil, False, False, nil);
 try
  Tick := GetTickCount + DWord(Milliseconds);
  while (Milliseconds > 0) and
   (MsgWaitForMultipleObjects(1, Event, False, Milliseconds, QS_ALLINPUT) <> WAIT_TIMEOUT) do
    begin
      Application.ProcessMessages;
      if Application.Terminated then Exit;
      Milliseconds := Tick - GetTickcount;
      sleep(0);
    end;
 finally
  CloseHandle(Event);
  Application.ProcessMessages;
 end;
end;

procedure tc(tab:string);
var query:string;
begin
    query:= format('Repair TABLE %s ',[tab]);
    mysql_d.sql_exe(query);
    query:= format('select * from %s limit 1 ',[tab]);
    mysql_d.sql_exe(query);
	{mysql_d.drop_table('tmp_tab1');
    query:= format('CREATE TABLE tmp_tab1 SELECT * FROM %s',[tab]);
    mysql_d.sql_exe(query);
    mysql_d.drop_table(tab);
   query:= format('rename table tmp_tab1 to %s',[tab]);
   mysql_d.sql_exe(query); }
end;

procedure TForm_main.DBEdit3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i:integer;
begin
try
i:=strtoint(text);
if i<0 then text:='0';
if i>100 then text:='100';
except
text:='0';
end;
end;





procedure TForm_main.FirmenFilter1Click(Sender: TObject);
var
ber:integer;
begin
	if nicht_satellit_tab then exit;

	ber:=b.b_string('Firmen-Listen');
	if ber=-1 then exit;


   form_besonderheiten:=tform_besonderheiten.create(self);
   form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
   form_besonderheiten.Caption:='Firmen-Kriterien';
   form_besonderheiten.Panel_bottom.Visible:=false;
	form_besonderheiten.tquery:=datamodul.q_firma_filter_vorlage;
	form_besonderheiten.Tree.mysqlquery:=datamodul.q_firma_filter_vorlage;
	form_besonderheiten.Tree.mysql_feld:='name';
	form_besonderheiten.tree.liste_lesen;
	form_besonderheiten.ShowModal;
   form_besonderheiten.free;
end;

procedure TForm_main.Edit_suchKeyPress(Sender: TObject; var Key: Char);
begin
dbtreeview.suche_datensatz(edit_such.text);
end;

procedure TForm_main.SpeedButton_doceigenschaften1Click(Sender: TObject);
begin
try
//arbeitsmedizin_proband.ChangeIconDialog;
arbeitsmedizin_proband.ObjectPropertiesDialog;
except
	showmessage('Das Dokument ist leer');
end;
end;

procedure TForm_main.Splitter4Moved(Sender: TObject);
begin
grid_suchen(tabsheet_firma_texte);
end;

procedure TForm_main.ZeiterfassungfrmehrereFirmen1Click(Sender: TObject);
var
i_firma, i_kriterium,i_zeitkonto,i_untersucher,i_konto: integer;
stunden,minuten,sek, msek: word;
datum,anlass,memotext,memotext2,s,s1,query,s_konto:string;
d1,d2:tdate;
anteilzeit,gesamtzeit,akt_einsatzzeit,abzurech_zeit:real;
sf:boolean;
begin
 if b.b_string('Firma-zeitabrechnung')=-1 then exit;
sf:=speichern_fragen;
speichern_fragen:=false;
form_zeiterfassung_all:=tform_zeiterfassung_all.create(self);
if form_zeiterfassung_all.showmodal= mrok then
begin

    //unver�nderliche Werte
    datum:=form_zeiterfassung_all.MaskEdit_date.text;
    anlass:=form_zeiterfassung_all.memo_aufgabenfeld.text;
    s:=form_zeiterfassung_all.ComboBox_kriterium.Items[form_zeiterfassung_all.ComboBox_kriterium.itemindex];
    query:=format('select nummer from abr_anlass_krit where name="%s"',[s]);
    i_kriterium:=strtoint(mysql_d.feldinhalt(query,0));

    s:=form_zeiterfassung_all.ComboBox_zeitkonto.Items[form_zeiterfassung_all.ComboBox_zeitkonto.itemindex];
    query:=format('select nummer from abr_zeitkonto where name="%s"',[s]);
    i_zeitkonto:=strtoint(mysql_d.feldinhalt(query,0));
    //i_zeitkonto:=form_zeiterfassung_all.ComboBox_zeitkonto.itemindex;;

    s:=form_zeiterfassung_all.ComboBox_erbringer.Items[form_zeiterfassung_all.ComboBox_erbringer.itemindex];
    query:=format('select nummer from untersucher where untersucher="%s"',[s]);
    i_untersucher:=strtoint(mysql_d.feldinhalt(query,0));
    //i_untersucher:=form_zeiterfassung_all.ComboBox_erbringer.itemindex;

    memotext:=form_zeiterfassung_all.Memo_taetigkeit.Text;
    memotext2:=form_zeiterfassung_all.Memo2.text;

    stunden:=form_zeiterfassung_all.spinedit1.Value;
    minuten:=form_zeiterfassung_all.spinedit2.Value;
    abzurech_zeit:=stunden+minuten/60;


    datamodul.q_5.first; // firmen

gesamtzeit:=0;
{while not datamodul.q_5.Eof do   //gesamtzeit
begin


    if i_zeitkonto=1 then gesamtzeit:=gesamtzeit+ datamodul.q_5.findfield('Zeit-Medizin').asfloat else gesamtzeit:=gesamtzeit+ datamodul.q_5.findfield('Zeit-Technik').asfloat ;
    datamodul.q_5.next;
end; }


d1:=strtodate(form_zeiterfassung_all.maskedit_z1.Text);
d2:=strtodate(form_zeiterfassung_all.maskedit_z2.Text);
s_konto:=form_zeiterfassung_all.ComboBox_abr_zeitkonto.Text ;
query:=format('select nummer from abr_zeitkonto where name="%s"',[s_konto]);

s_konto:=mysql_d.Feldinhalt(query,0);
if s_konto<>'' then i_konto:=strtoint(s_konto) else i_konto:=-1;
gesamtzeit:=sollzeiten(form_zeiterfassung_all.ff,d1,d2,-1,i_konto);

anteilzeit:=abzurech_zeit / gesamtzeit; //jeweils in stunden

datamodul.q_5.first;
while not datamodul.q_5.Eof do   //hier eintrag f�r jeden Datensatz
begin

    //ver�nderliche Werte
    i_firma:=datamodul.q_5.findfield('nummer').asinteger;

    akt_einsatzzeit:= datamodul.q_5.findfield('einsatzstunden2').asfloat;

    if i_kriterium<1 then i_kriterium:=1;
    if i_zeitkonto<1 then i_zeitkonto:=1;


    Stunden:=trunc(akt_einsatzzeit);
    minuten:=round((akt_einsatzzeit-stunden)*60);

   if stunden+minuten>0 then
	neuer_datensatz(datamodul.q_firma_sollist,['nummer','i_firma','datum','stunden','minuten','anlass','i_kriterium','i_zeitkonto','i_untersucher','zeitansatz','memo','memo2']
				 ,	[null,i_firma,datum,stunden,minuten,anlass,i_kriterium,i_zeitkonto,i_untersucher,'100',memotext,memotext2]);

   datamodul.q_5.Next;
end;
end;

datamodul.q_firma_sollist.refresh;
form_zeiterfassung_all.release;
speichern_fragen:=sf;
end;



procedure TForm_main.DBComboBox_zeitansatzKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
t:string;
z:integer;
begin
if ( key in [58..95]) or (key in [106..126])  then
begin
 DBComboBox_zeitansatz.Text:='100';
end;

if (key in [48..57]) or (key in [96..105]) then
begin
  t:=trim(DBComboBox_zeitansatz.Text);
  if length(t)>3
  then DBComboBox_zeitansatz.Text:=copy( DBComboBox_zeitansatz.Text,1,3)//'0';//chr(key);
end;
try
  z:=-2;
  if DBComboBox_zeitansatz.Text<>'' then z:=strtoint(  DBComboBox_zeitansatz.Text);
  if key=37 then dec(z);
  if key=39 then inc(z);
  if z =-1 then z:=0;
  if z>=0 then DBComboBox_zeitansatz.Text:=inttostr(z);
except
   DBComboBox_zeitansatz.Text:='0';
end;
   
end;

procedure TForm_main.Button_firma_zeitpauschale_berechnenClick(
  Sender: TObject);
var
stas:real;
begin

with datamodul do
try
stas:=(q_firma.findfield('anz_ma_ge').asfloat*q_firma.findfield('faktor_ge').asfloat+q_firma.findfield('anz_ma_an').asfloat*q_firma.findfield('faktor_an').asfloat)* q_firma.findfield('stundensatz_berechnung').asfloat;

q_firma.findfield('euro_pauschal').asfloat:=stas;

except
end
end;


procedure TForm_main.Button1Click(Sender: TObject);
var
i,j:integer;
begin
//i:=round(date()*date()*date()*date()*date()*date()*date());
//i:=0;
//j:=3 div i;
//form_main.scaleby(3,4);
//varchar_ex('sql_baum_vorlage');
//treeview_ext_firma.liste_lesen;


//netzlaufwerkverbinden( 'y:',\\192.168.0.2\temp','','');
netzlaufwerktrennen('y:',true,true,false);
if (NetzlaufwerkVerbinden('y:','\\192.168.0.2\temp', '', '', False) = 0) then
    ShowMessage('OK')
  else
    ShowMessage('Fehler!');
end;

procedure TForm_main.m_Ber_ges_FirmaClick(Sender: TObject);
var
i,i_firma, i_user:integer;
query: string;
daten:pnodepointer;
vorhanden:boolean;
begin
//  formular erzeugen
//  alle user in checklistbox
//  diejenigen die Bereichtigung f�r firma haben checken
//  formular anzeigen
//  Berechtigung f�r alle user �berpr�fen und aktualisieren
if nicht_satellit_tab then exit;

if  b.b_string('Berechtigungen')=-1  then exit;
speichern;
with datamodul do try
i_firma:=q_firma.findfield('nummer').asinteger;
form_checkbox:=tform_checkbox.create(self);
form_checkbox.Caption:='zugelassene User';
query:='select * from schluessel';
sql( true, q_1,query,'name');
q_1.first;


listeeinlesen(form_checkbox.CheckListBox.items,q_1,'name');
for i:=0 to  form_checkbox.CheckListBox.Items.Count-1 do
  begin
  	daten:=pnodepointer(form_checkbox.checklistbox.Items.Objects[i]);
     i_user:=daten^.nummer;

     query:=format('select nummer from berechtigung_firma where benutzer=%d and i_firma=%d and storno=0 ',[i_user, i_firma]);
     vorhanden:= mysql_d.sql_exe_rowsvorhanden(query);
     form_checkbox.CheckListBox.Checked[i]:=vorhanden;
  end;


if form_checkbox.ShowModal =mrok then
begin
  for i:=0 to  form_checkbox.CheckListBox.Items.Count-1 do
  begin
         query:='';
         daten:=pnodepointer(form_checkbox.checklistbox.Items.Objects[i]);
         i_user:=daten^.nummer;

         query:=format('select nummer from berechtigung_firma where benutzer=%d and i_firma=%d ',[i_user, i_firma]);
         vorhanden:= mysql_d.sql_exe_rowsvorhanden(query);
  		if form_checkbox.CheckListBox.Checked[i] then
       	begin
          if not vorhanden then query:=format('insert berechtigung_firma set benutzer=%d, i_firma=%d',[i_user, i_firma]);
          if  vorhanden then query:=format('update berechtigung_firma set storno =0 where benutzer=%d and i_firma=%d',[i_user, i_firma]);
       	end
        else
        begin
          if vorhanden then query:=format('update berechtigung_firma set storno =1 where benutzer=%d and i_firma=%d',[i_user, i_firma]);
          //if vorhanden then query:=format('delete from berechtigung_firma where benutzer=%d and i_firma=%d',[i_user, i_firma]);
       end;

       mysql_d.sql_exe(query);
  end;


end;



finally
listedeaktivieren(form_checkbox.CheckListBox.items);
form_checkbox.free;
end;
end;


function tform_main.zust_arbmed_besch_vorhanden(i_weitergabe, i_typ:integer; dat:tdatetime): boolean;
var
d1,d2:tdate;
query,s:string;
result_proband, result_besch,vorsorge:boolean;
begin
  query:=format('select i_arbmedvv from typ where nummer=%d',[i_typ]);
  s:=mysql_d.Feldinhalt(query,0);
  if s='0'then vorsorge:=false else vorsorge:=true;
  result_proband:=datamodul.q_mitarbeiter.findfield('zustimmung_bescheinigung').asinteger=1;
  d1:=datamodul.q_mitarbeiter.findfield('zustimmung_bescheinigung_ausgestellt').asdatetime;
  d2:=datamodul.q_mitarbeiter.findfield('zustimmung_bescheinigung_gueltig_bis').asdatetime;
  if ((dat>=d1 )and (dat<=d2)) then result_proband:=(result_proband and true) else result_proband :=false;
  if i_weitergabe=0 then result_besch:=false else result_besch:=true;
  result:=true;

  if (druck_mit_zustimmung and vorsorge ) then  result:=result_proband;
  if (druck_mit_zustimmung_eignung and (not vorsorge) ) then  result:=result_proband;
  if (druck_mit_zustimmung_besch and vorsorge ) then  result:=result_besch;
  if (druck_mit_zustimmung_besch_eignung and (not vorsorge) ) then  result:=result_besch;


end;


procedure TForm_main.DBCombo_num_unt_art2Change(Sender: TObject);
begin
 if zust_arbmed_besch_vorhanden(datamodul.q_untersuchung.findfield('i_weitergabe').asinteger,datamodul.q_untersuchung.findfield('i_typ').asinteger,datamodul.q_untersuchung.findfield('datum').AsDateTime ) then image_paw.Visible:=false else  image_paw.Visible:=true;
end;

procedure TForm_main.DBCheckBox_zustimmung_beschMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

  if DBCheckBox_zustimmung_besch.Checked then
   begin
   	if Messagedlg('Soll das Datum aktualisiert werden?',mtConfirmation, mbOkCancel	,0)=mrOK then
      begin
      try
      	datamodul.q_mitarbeiter.FindField('zustimmung_bescheinigung_ausgestellt').asdatetime:= (now());
         datamodul.q_mitarbeiter.FindField('zustimmung_bescheinigung_gueltig_bis').asdatetime:= (strtodate('31.12.2999'));
      except
      	showmessage('"�ndern / �ffnen" ist notwendig');
  		end;
      end;
   end;

end;

procedure TForm_main.DBCombo_num_unt_art2MeasureItem(Control: TWinControl;
  Index: Integer; var Height: Integer);
begin
   //if zust_arbmed_besch_vorhanden then image_paw.Visible:=false else  image_paw.Visible:=true;
end;

procedure TForm_main.DBCombo_num_unt_art2DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  //if zust_arbmed_besch_vorhanden then image_paw.Visible:=false else  image_paw.Visible:=true;
end;

procedure TForm_main.DBCombo_num_unt_art2Click(Sender: TObject);
begin
if (DBCombo_num_unt_art2.itemindex<>0) and (not form_main.zust_arbmed_besch_vorhanden(datamodul.q_untersuchung.findfield('i_weitergabe').asinteger,datamodul.q_untersuchung.findfield('i_typ').asinteger,datamodul.q_untersuchung.findfield('datum').AsDateTime ))    then form_main.image_paw.Visible:=true else  form_main.image_paw.Visible:=false;
form_main.image_paw.Repaint;
end;

procedure TForm_main.DBgridEXT_texteDblClick(Sender: TObject);
begin
    formular_entsperren(true,false);
end;


procedure TForm_main.DBEdit_z_stunden1Exit(Sender: TObject);
begin
	firma_zeitvorgaben_berechnen;
end;

procedure tform_main.firma_zeitvorgaben_berechnen;
var
d1,d2:tdate;
t:integer;
sd:real;
begin
    if not  datensatz(DataModul.q_firma_zeiten_soll) then exit;
	t:=trunc(DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime) -trunc( DataModul.q_firma_zeiten_soll.findfield('von').asdatetime);
    if t<0 then
    begin
    		showmessage('Zeiten wurden  vertauscht');
        d1:= DataModul.q_firma_zeiten_soll.findfield('von').asdatetime;
        if DataModul.q_firma_zeiten_soll.State in [dsedit, dsinsert] then DataModul.q_firma_zeiten_soll.edit;
        DataModul.q_firma_zeiten_soll.findfield('von').asdatetime:=DataModul.q_firma_zeiten_soll.findfield('bis').asdatetime;
        DataModul.q_firma_zeiten_soll.findfield('von').asdatetime:=d1;
    end;

    if t=0 then
    begin
     showmessage('Die Zeiten d�rfen nicht gleich sein');
     exit;
    end;

	sd:= DataModul.q_firma_zeiten_soll.findfield('einsatzstunden').asfloat;// / t;
   if not (DataModul.q_firma_zeiten_soll.State in [dsedit, dsinsert]) then DataModul.q_firma_zeiten_soll.edit;
   DataModul.q_firma_zeiten_soll.findfield('stunden_tag').asfloat:=sd;

end;

procedure TForm_main.DBEdit_z_vonExit(Sender: TObject);
begin
	firma_zeitvorgaben_berechnen;
end;

procedure TForm_main.DBCombo_num_firma_zeiten_soll_kriteriumClick(
  Sender: TObject);
begin
   if datamodul.q_firma_zeiten_soll.State in [dsedit, dsinsert] then
   datamodul.q_firma_zeiten_soll.FindField('abrechnungskriterium').asstring:=DBCombo_num_firma_zeiten_soll_kriterium.Text;
end;

procedure TForm_main.DBCombo_num_firma_zeiten_soll_kontoClick(
  Sender: TObject);
begin
if datamodul.q_firma_zeiten_soll.State in [dsedit, dsinsert] then
   datamodul.q_firma_zeiten_soll.FindField('zeitkonto').asstring:=DBCombo_num_firma_zeiten_soll_konto.Text;
end;

procedure TForm_main.m_AbrechnungskontoClick(Sender: TObject);

var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:= b.b_string('Firmen-Listen');
if ber=-1 then exit;

    speichern;
    form_tabellen:=tform_tabellen.Create(self);
    form_tabellen.HelpContext:=4;
    form_tabellen_berechtigung(ber);

    form_tabellen.suchfeld:='Abrechnungskonto';
    form_tabellen.Caption:='Abrechnungskonto';
    form_tabellen.SpeedButton_stringhoch.Visible:=true;
    form_tabellen.SpeedButton_stringrunter.Visible:=true;

    form_tabellen.Notebook.PageIndex:=20;
    datamodul.sql(true,datamodul.q_1,'select * from abr_zeitkonto','reihenfolge');
    form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='name';
    form_tabellen.DBGrid_tabellen.Columns[0].width:=500;
    form_tabellen.DBEdit_zeitkonto.DataField:='name';
    grid_einstellen(form_tabellen.dbgrid_tabellen);
    form_tabellen.showmodal;
    form_tabellen.release;
    dbcombo_laden;
end;




procedure TForm_main.SpeedButton10Click(Sender: TObject);
var
  typeName, defaultExtension: string;
begin
  // Get the information from the registry
  //GetOleClassInfo(arbeitsmedizin_proband.OleObjectInterface, defaultExtension, typeName);
  with SaveDialog do
  begin
    // Set up the TSaveDialog
    //DefaultExt := Copy(defaultExtension, 2, 3);
    //FileName := '*.'+DefaultExt;
    //Filter  := typeName+'|'+FileName;

    // Run the dialog
    if Execute then
    begin
      // If the user pressed OK, save the document
      arbeitsmedizin_proband.OleObject.SaveAs(FileName);
      //OleConvertOLESTREAMToIStorage(
      // Alternative method:
      // uoSaveIStorageToFile(OleContainer1.StorageInterface, FileName);
    end;
  end;    // with
end;

procedure TForm_main.m_FirmenzusammenfgenClick(Sender: TObject);
var
query,tab:string;
i_firma_bleib, i_firma_weg:integer;
begin
try
   if b.b_string('Anwendung')=-1 then exit;
  query:='select nummer,firma,kuerzel from firma where nummer in ('+firmen_berechtigung+')';
  datamodul.sql_new(true,datamodul.q_5,query,'firma');
  form_firmen_zusammen:=tform_firmen_zusammen.create(self);
  if form_firmen_zusammen.showmodal=mrok then
  begin
  	if Messagedlg('Sollen die Firmen zusammengef�hrt werden?',mtConfirmation, mbOkCancel	,0)<>mrOK  then exit;

    i_firma_bleib:=form_firmen_zusammen.i_firma1;
    i_firma_weg:=form_firmen_zusammen.i_firma2;
    if  i_firma_bleib= i_firma_weg then
    begin
    	showmessage('Sie haben die gleichen Firmen ausgew�hlt, Funktion wird abgebrochen.');
        exit;
    end;

    query:=format('select storno from firma where nummer=%d',[i_firma_bleib]);
    if mysql_d.Feldinhalt(query,0)='1' then
    begin
    	showmessage('Einer gel�schte Firma kann keine Probanden zugeordnet werden, Funktion wird abgebrochen.');
      exit;
    end;

    query:='select * from export_import where main=0';
    datamodul.sql(false,datamodul.q_4, query,'');
    datamodul.q_4.first;
    while not datamodul.q_4.eof do
    begin
       tab:=datamodul.q_4.findfield('tab_name').asstring;
       try
         query:=format('show columns from %s like "i_firma" ',[tab]);
         if mysql_d.sql_exe_rowsvorhanden(query) and (i_firma_weg>0) and (i_firma_bleib>0) then
         begin
         //fehler!!!!!!!!!!!!!!
          query:=format('update %s set i_firma=%d where i_firma=%d',[tab,i_firma_bleib, i_firma_weg]);
          mysql_d.sql_exe(query);
         end;
       except
       end;
      datamodul.q_4.next;
    end;

   query:=format('update texte set i_master=%d where i_master=%d',[i_firma_bleib, i_firma_weg]);
   mysql_d.sql_exe(query);

   query:=format('update firma set storno=1 where nummer=%d',[i_firma_weg]);
   mysql_d.sql_exe(query);
   datamodul.q_firma.Refresh;

  end;
finally
form_firmen_zusammen.release;
end;
end;

procedure TForm_main.grid_sehtestKeyPress(Sender: TObject; var Key: Char);
begin
radiogroup_auffaellig.tag:=1;
end;

procedure TForm_main.SpeedButton11Click(Sender: TObject);
var
  tab:tzquery;
  feld:string;
  d:extended;
begin
tab:=datamodul.q_mitarbeiter;
if (sender =speedbutton11)  then feld:='zustimmung_bescheinigung_ausgestellt';
if (sender =speedbutton17)  then feld:='zustimmung_bescheinigung_gueltig_bis';
if (sender =speedbutton21)  then feld:='ende';
if (sender =speedbutton22)  then feld:='beginn';
if form_kalender.showmodal =mrok then
	 begin
		tab.edit;
    d:=int(form_kalender.auswahlkalender.date);
    if (sender =speedbutton21) then if d=int(strtodate('01.01.1900')) then d:=int(strtodate('31.12.2999'));
    if (sender =speedbutton22) then if d=int(strtodate('01.01.1900')) then d:=int(strtodate('01.01.2000'));
    if (sender =speedbutton17) then if d=int(strtodate('01.01.1900')) then d:=int(strtodate('31.12.2999'));
    if (sender =speedbutton11) then if d=int(strtodate('01.01.1900')) then d:=int(strtodate('01.01.2000'));

		tab[feld] :=d;

     direktspeichern:=true;
		tab.post;

	 end;
  if (sender =speedbutton11)  then DBEdit_arbmed_besch_ert.SetFocus;
  if (sender =speedbutton17)  then DBEdit_arbmed_besch_bis.SetFocus;
  if (sender =speedbutton21)  then dbedit_ende.SetFocus;
  if (sender =speedbutton22)  then dbedit_beginn.setfocus;
end;

procedure TForm_main.GDTProgramme1Click(Sender: TObject);
var
 ber: integer;
begin
  ber:=b.b_string('Untersuchungen-Listen');
  if ber=-1 then exit;

   datamodul.sql(true,datamodul.q_1,'select * from gdt','name');

	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen_berechtigung(ber);
   form_tabellen.HelpContext:=78;
   form_tabellen.suchfeld:='GDT-Programm';
	form_tabellen.Caption:='GDT-Programm-Aufruf';
	form_tabellen.Notebook.PageIndex:=15;

	form_tabellen.ToolButton_neu.enabled:=true;
	//form_tabellen.ToolButton_del.enabled:=false;

   form_tabellen.DBGrid_tabellen.DataSource:=datamodul.ds_1;
	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Name';
	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;

   form_tabellen.DBEdit_gdtdatei.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtname.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtprogramm.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtparameter.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtrueckantwort.DataSource:=datamodul.ds_1;
   form_tabellen.DBCheckBox_gdtwarten.DataSource:=datamodul.ds_1;
   form_tabellen.DBRadioGroup_gdt_version.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtidemfaenger.DataSource:=datamodul.ds_1;
   form_tabellen.DBRadioGroup_gdtparameter.DataSource:=datamodul.ds_1;
	grid_einstellen(form_tabellen.dbgrid_tabellen);

	form_tabellen.showmodal;

   form_tabellen.Release;
end;

procedure TForm_main.m_GDT_schreibenClick(Sender: TObject);
begin
	gdt_erzeugen;
end;

function TForm_main.gdt_erzeugen: int64;
var
  ber,gdt_version: integer;
  pfad_org,prg,par,gdt_datei,vorlagen_pfad:string;

 function pdf_wahl(pfad:string):string;
 begin
   if copy(pfad,length(pfad),1)<>'\' then pfad:=pfad+'\';

   form_pdfwahl:=tform_pdfwahl.create(self);
   form_pdfwahl.ini( pfad,'*.pdf','Auswahl der PDF-Vorlage');
   if form_pdfwahl.showmodal=mrok then
   begin
     result:=form_pdfwahl.FileListBox.FileName;
   end;
   form_pdfwahl.release ;
 end;

//########################
begin
   result:=0;
   ber:=b.b_string_silent('Berechtigungen');

try
   datamodul.sql(true,datamodul.q_1,'select * from gdt','name');
	form_tabellen:=tform_tabellen.Create(self);
	form_tabellen_berechtigung(ber);
   form_tabellen.HelpContext:=78;
   form_tabellen.suchfeld:='GDT-Programm';
	form_tabellen.Caption:='GDT-Programm-Aufruf';
	form_tabellen.Notebook.PageIndex:=15;

	form_tabellen.ToolBar.Visible:=false;
   form_tabellen.panel_b.Visible:=true;

   form_tabellen.DBGrid_tabellen.DataSource:=datamodul.ds_1;
	form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Name';
	form_tabellen.DBGrid_tabellen.Columns[0].width:=0;

   form_tabellen.DBEdit_gdtdatei.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtname.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtprogramm.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtparameter.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtidemfaenger.DataSource:=datamodul.ds_1;
   form_tabellen.DBEdit_gdtrueckantwort.DataSource:=datamodul.ds_1;
   form_tabellen.DBCheckBox_gdtwarten.DataSource:=datamodul.ds_1;
   form_tabellen.DBRadioGroup_gdt_version.DataSource:=datamodul.ds_1;
   form_tabellen.DBRadioGroup_gdtparameter.DataSource:=datamodul.ds_1;

	grid_einstellen(form_tabellen.dbgrid_tabellen);

	if form_tabellen.showmodal=mrok then
   begin
      if kein_datensatz(datamodul.q_1) then
      begin
      	showmessage('Kein GDT-Datensatz vorhanden, bitte neu anlegen');
         exit;
      end;
       gdt_version:=datamodul.q_1.findfield('i1').asinteger;

       if  gdt_version=3 then
        gdt_datei:=pfad_temp+'stepover.fff'
       else
        gdt_datei:= datamodul.q_1.findfield('datei').asstring;

      GDT_schreiben(gdt_datei,datamodul.q_1.findfield('programm').asstring,datamodul.q_1.findfield('id_empfaenger').asstring,datamodul.q_1.findfield('parameter').asinteger,gdt_version);


     if  gdt_version=3 then
     begin
       //stepover
       vorlagen_pfad:= pdf_wahl(datamodul.q_1.findfield('id_empfaenger').asstring);
       par:= datamodul.q_1.findfield('s1').asstring;
       par:='"'+vorlagen_pfad+'" "'+gdt_datei+ '" '+par;
     end
     else
       //gdt
      par:= datamodul.q_1.findfield('s1').asstring;
      par:=trim(par);
      pause(100);
      prg:=datamodul.q_1.findfield('programm').asstring;
      if prg<>'' then
      begin
      	shellexecute_se(0,'open',pchar(prg),pchar(par),nil,sw_shownormal);
         pause(100);
      end;

      if form_tabellen.DBCheckBox_gdtwarten.Checked then
      begin
         pfad_org:=pfad_vorlagen;
         pfad_vorlagen:=datamodul.q_1.findfield('rueckantwort').asstring;
         application.ProcessMessages;
         dbtreeview.Enabled:=false;
      	 result:=text_neu(1,date(),2,true);
         dbtreeview.Enabled:=true;
         pfad_vorlagen:=pfad_org;
      end;
   end ;

  //if fileexists(gdt_datei) then deletefile(gdt_datei);
finally
   form_tabellen.Release;
end;
end;



function TForm_main.GDT_schreiben(datei,programm,id_empfaenger: string; parameter: integer;gdt_version:integer):boolean;
var
	se,s_mitarbeiter,nachname, vorname,strasse, plz, ort, geb_dat,gewicht, groesse,z ,p_nummer,query:string;
   geschlecht,dl,sl,i,j:integer;
   liste:tstringlist;
   felder: array [0..2] of string;

function slength(s:string):string;
    begin
    result:=inttostr(length(s)+5);
    while length(result)<3 do
    begin
      result:='0'+result;
    end;
    result:=result+s;
  end;

procedure schreibe_gdt;
var
 i:integer;
begin

   //erste Zeile
   case parameter of
   1:  z:='01380006301';
   2:  z:='01380006302';
   end;
   liste.Add(z);   //satzart
   liste.add('0148100'); //dateil�nge (es fehlen noch 5 zeichen)
   if id_empfaenger<>'' then
   begin
     z:='8315'+copy(id_empfaenger+'        ',1,8);
     liste.add(slength(z));//id_empf�nger
   end;
   //z:='8316Arbene__';
   z:='8316arbene  ';
   liste.add(slength(z));//id_sender

   case gdt_version of
     0: liste.Add('014921802.10'); //Version GDT hier 2.10
     1: liste.Add('014921801.00');
   end;


   z:='3000'+p_nummer;
   liste.add(slength(z));//patientennummer / personalnummer
   //z:='3100'+namenszusatz;
   //liste.add(slength(z)); //namenszusatz
   z:='3101'+nachname;
   liste.add(slength(z)); //name
   z:='3102'+vorname;
   liste.add(slength(z)); //vorname
   z:='3103'+geb_dat;
   liste.add(slength(z)); //gebdat

   z:='3110'+inttostr(geschlecht);
   liste.add(slength(z)); //geschlecht
   //z:='3104'+titel;
   //liste.add(slength(z)); //titel
   if plz+ort<>'' then
   begin
     z:='3106'+plz+' '+ort;
     liste.add(slength(z)); //plz+ort
   end;
   if strasse<>'' then
   begin
     z:='3107'+strasse;
     liste.add(slength(z)); //strasse + hausnummer
   end;
   z:='3622'+groesse;
   liste.add(slength(z)); //gr��e
   z:='3623'+gewicht;
   liste.add(slength(z)); //gewicht
   //liste.add(''); //cr  2016-09-16 entfernt
   //z:='8410'+'12345';//testident;
   //liste.add(slength(z)); //testident
// gesamtl�nge z�hlen   udn in zeile 2 schreiben
   dl:=0;
   for i:=0 to liste.count-1 do
   begin
   dl:=dl+length(liste[i])+2;
   end;
   dl:=dl+3;  //empirisch s. pr�fmodul http://www1.docexpert.de/web/qms/checkgdt.nsf/Dokumentz/A040B217AB5BD594C1257C6100736F2E?OpenDocument
   z:=inttostr(dl);
   while length(z)<5 do
   begin
      z:='0'+z;
   end;
   liste[1]:=liste[1]+z;

 end;

procedure schreibe_stepover;
begin
   liste.add('[Fields]');
   z:='Name='+nachname;
    liste.add(z); //name
    z:='Vorname='+vorname;
    liste.add(z); //vorname
    z:='Geburtsdatum='+datetostr(datamodul.q_mitarbeiter.findfield('geb_dat').asdatetime);
    liste.add(z); //gebdat
 end;

 //######################################################

 begin


 with datamodul do
 try
     se:=#13+#10;
    //id_empfaenger:=id_empfaenger+'_________';
    //id_empfaenger:=copy(id_empfaenger,1,8);


    s_mitarbeiter:=getbigint_str(q_mitarbeiter,'nummer');
      vorname:=copy(q_mitarbeiter.findfield('vorname').asstring,1,28);
    nachname:=copy(q_mitarbeiter.findfield('name').asstring,1,28);
    strasse:=q_mitarbeiter.findfield('strasse').asstring;
    plz:=q_mitarbeiter.findfield('plz').asstring;
    ort:=q_mitarbeiter.findfield('ort').asstring;
    i:=q_mitarbeiter.findfield('geschlecht').asinteger;
    case i of
      1: geschlecht:=2;
      2: geschlecht:=1;
    end;
   //if geschlecht=2 then  geschlecht:=1 else geschlecht:=2;    //1 m?nnlich , 2 weiblich
    geb_dat:=q_mitarbeiter.findfield('geb_dat').asstring;
    geb_dat:=stringreplace(geb_dat,'.','',[rfreplaceall]);
    //p_nummer:=copy(q_mitarbeiter.findfield('p_nummer').asstring,1,10);
    p_nummer:=q_mitarbeiter.findfield('p_nummer').asstring;
    if p_nummer='' then p_nummer:=q_mitarbeiter.findfield('nummer').asstring; //m?ssen verschieden sein
    //gewicht:='70';
    //groesse:='180';

    query:= 'select  akt_untersuchung.f_1, akt_untersuchung.f_2 , befunde.datum from akt_untersuchung';
    query:=query+' left join  befunde on (akt_untersuchung.i_untersuchung=befunde.nummer) left join bereich on (akt_untersuchung.i_bereich=bereich.nummer)';
     query:=query+format('    where befunde.i_mitarbeiter=%s and bereich.modus=6 and akt_untersuchung.storno=0 order by befunde.datum desc limit 1 ',[s_mitarbeiter]);

    mysql_d.Feldarray(query,[0,1,2],felder);
    groesse:=felder[0];
    gewicht:=felder[1];
    if groesse='' then groesse:='0';
    if gewicht='' then gewicht:='';
    liste:=tstringlist.Create;

    if gdt_version =3 then schreibe_stepover else schreibe_gdt;


// datei schreiben
   try
   liste.SaveToFile(datei);
   except
    showmessage('Die Datei '+datei+' kann nicht erstellt werden.');
   end;

finally
liste.Free;
result:=true;
end;
end;

procedure TForm_main.BitBtn_zeit_kmClick(Sender: TObject);
begin
   if {(datamodul.q_firma_sollist.findfield('km_gefahren').asfloat=0 ) or} (dbedit_abr_kmgefahren.text='0')
   then
   begin
		km_anzeigen;
   	dbedit_abr_kmgefahren.Text:=floattostr(datamodul.q_firma.findfield('km_entfernung').asfloat*2);
      BitBtn_zeit_km.Caption:='zur�cksetzen';
   end
   else
   begin

     DBCheckBox_abr_privat.Checked:=false;

     //dbedit_abr_kmgefahren.text:='0';
     datamodul.q_firma_sollist.findfield('km_gefahren').asfloat:=0;
     datamodul.q_firma_sollist.findfield('km_modus').asinteger:=0;
     BitBtn_zeit_km.Caption:='km eintragen';
     km_verbergen;
   end;

end;

procedure TForm_main.km_verbergen ;
begin
   label_abr_km.Visible:=false;
   label_abr_kmgefahren.Visible:=false;
   dbedit_abr_kmgefahren.Visible:=false;
   DBCheckBox_abr_privat.visible:=false;
   label_privatfahrzeug.Visible:=false;

end;
procedure TForm_main.km_anzeigen;
begin
   label_abr_km.Visible:=true;
   label_abr_kmgefahren.Visible:=true;
   dbedit_abr_kmgefahren.Visible:=true;
   DBCheckBox_abr_privat.visible:=true;
   label_privatfahrzeug.Visible:=true;
end;

//function untergeordnete_firmen(i_firma:integer):tintegerlist;
function untergeordnete_firmen(i_firma:integer):tlist;
var
query,sf,s:string;
nr:integer;
begin
	//result:=tintegerlist.create;
   result:=tlist.create;
   sf:=inttostr(i_firma);
   s:='-1';
   //suche alle untergeordnten firmen
   while s<>'' do
   begin
      query:=format('select nummer from firma where parent in (%s) and nummer not in (%s)',[sf,sf]);
      s:=mysql_d.Feldinhalt(query,0);
      if s<>'' then
      begin
         nr:=strtoint(s);
         //result.hinzufuegen(0,nr);
         result.Add(pointer(nr));
         sf:=sf+','+s;
      end;
   end;

end;

procedure TForm_main.DBgridEXt_firmaColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
begin
  firmaanzeigen;
end;

procedure TForm_main.BitBtn_rechnungsempfaengerClick(Sender: TObject);
var
fname:string;
fnummer:integer;
begin
  //if datamodul.q_firma.State in [dsedit, dsinsert]
  firmawaehlen(fname,fnummer);

  if   (Messagedlg('Sollen die �bergeordnete Firma ge�ndert werden?',mtConfirmation, mbOkCancel	,0)=mrOK ) then
  begin
     if datamodul.q_firma.FindField('nummer').asinteger=fnummer then
     begin
       datamodul.q_firma.FindField('re_empfaenger').asstring:='';
       datamodul.q_firma.FindField('parent').asinteger:=0;
     end
     else
     begin
        datamodul.q_firma.FindField('re_empfaenger').asstring:=fname;
        datamodul.q_firma.FindField('parent').asinteger:=fnummer;
   	end;
  end;
end;

procedure TForm_main.firmawaehlen(var fname: string; var fnummer:integer);
var
query:string;
begin
try
   fname:='';
   fnummer:=-1;
   with datamodul do
	 begin
    	query:='select nummer,firma from firma where nummer in ('+firmen_berechtigung+')';

	  	sql(true,q_7,query,'firma');
		 form_liste:=tform_liste.create(self);
		 form_liste.dbgridext.DataSource:=ds_7;
		 form_liste.s_name:='firma';
		 form_liste.DBgridEXT.Columns[0].fieldname:='firma';
		 form_liste.DBgridEXT.Options:=form_liste.DBgridEXT.Options + [dgMultiSelect]	 ;

     	 form_liste.toolbar.Visible:=false;
       form_liste.Panel_b.Visible:=true;
       form_liste.height:= form_main.Height-170;
		 if form_liste.showmodal=mrok then
       begin
		 	fname:=q_7.findfield('firma').asstring;
       	fnummer:=q_7.findfield('nummer').asinteger;
       end;
	 end;
finally
	form_liste.release;

end;
end;

procedure TForm_main.DBgridEXT_repoDblClick(Sender: TObject);
var
fnummer:integer;
rnummer:string;
i_ma:int64;
begin

//gehe zur Firma
//gehe zur entsprechenden Rechnung
with datamodul do
begin
   rnummer:= q_repositionen.findfield('i_link_rechnung').asstring;
   fnummer:= q_repositionen.findfield('i_firma').asinteger;
   i_ma:=getbigint(q_repositionen,'i_mitarbeiter');
   if (i_ma>0) and (rnummer='0') then
   begin
   	gehezu_stammdaten(i_ma );
      pagecontrol_main.ActivePage:=tabsheet_main;
      PageControl_mainChange(Sender);
   end
   else
   begin
      if rnummer='0' then exit;
      q_firma.locate('nummer',fnummer,[]);
      PageControl_firmaChange(pagecontrol_firma);
      //pagecontrol_firma.ActivePage:=tabsheet_rechnungen;
      q_rechnung.locate('nummer',rnummer,[]);
   end;

end;
end;


function tform_main.gehezu_rechnung(rnummer:string):boolean;
var
query,sfirma:string;
begin
      result:=false;
		if rnummer='0' then exit;
      query:=format('select i_firma from rechnung where renu="%s" and i_parent=0',[rnummer]);
      sfirma:=mysql_d.Feldinhalt(query,0);
      datamodul.q_firma.locate('nummer',sfirma,[]);
      //PageControl_firmaChange(pagecontrol_firma);
      application.ProcessMessages;
      pagecontrol_firma.ActivePage:=tabsheet_rechnungen;
      PageControl_firmaChange(pagecontrol_firma);
      application.ProcessMessages;
      result:=datamodul.q_rechnung.locate('renu',rnummer,[]);

end;

procedure TForm_main.DBgridEXT_rechnungDblClick(Sender: TObject);
var
rnummer,fnummer:string;
query:string;
i_ma:int64;
begin
//gehe zur �bergeordneten Rechnung
with datamodul do
begin
   rnummer:= q_rechnung.findfield('i_parent').asstring;
   i_ma:=getbigint(q_rechnung,'i_mitarbeiter');
   if (i_ma>0)  then
   begin
   	  gehezu_stammdaten(i_ma );
      pagecontrol_main.ActivePage:=tabsheet_main;
      PageControl_mainChange(Sender);
      dbtreeview.suche_datensatz(q_rechnung.findfield('renu').asstring);
      exit;
   end;


   if rnummer<>'0' then
   begin
      query:=format('select i_firma from rechnung where nummer=%s',[rnummer]);
      fnummer:= mysql_d.Feldinhalt(query,0);
      q_firma.locate('nummer',fnummer,[]);
      PageControl_firmaChange(pagecontrol_firma);

      q_rechnung.locate('nummer',rnummer,[]);
   end;

   //pagecontrol_firma.ActivePage:=tabsheet_rechnungen;


end;
end;

procedure TForm_main.DBgridEXt_firmaDblClick(Sender: TObject);
var
i_parent:integer;
begin
i_parent:=datamodul.q_firma.findfield('parent').asinteger;
if i_parent>0 then datamodul.q_firma.Locate('nummer',i_parent,[]);

end;

function firmenname(i_firma:integer):string;
var query:string;
begin
	query:=format('select firma from firma where nummer=%d',[i_firma]);
   result:=mysql_d.Feldinhalt(query,0);
end;


function tageimmonat(m,y:word): Integer;
begin
  Result := MonthDays[IsLeapYear(y), m];
end;

function Quartalbeginn(m:word): Integer;
begin
   result:=1;
   if m>=10 then
   begin
      result:=10;
      exit;
   end;
   if m>=7 then
   begin
      result:=7;
      exit;
   end;
   if m>=4 then
   begin
      result:=4;
      exit;
   end;
end;


function halbjahrbeginn(m:word): Integer;
begin
   result:=1;
   if m>=7 then
   begin
      result:=7;
      exit;
   end;

end;

procedure arbene_vor;
begin
	ForceForegroundWindow(application.Handle);
   application.processmessages;
end;

function ForceForegroundWindow(hwnd: THandle): Boolean;
const
  SPI_GETFOREGROUNDLOCKTIMEOUT = $2000;
  SPI_SETFOREGROUNDLOCKTIMEOUT = $2001;
var
  ForegroundThreadID: DWORD;
  ThisThreadID: DWORD;
  timeout: DWORD;
begin
  if IsIconic(hwnd) then ShowWindow(hwnd, SW_RESTORE);

  if GetForegroundWindow = hwnd then Result := True
  else
  begin
    // Windows 98/2000 doesn't want to foreground a window when some other
    // window has keyboard focus

    if ((Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion > 4)) or
      ((Win32Platform = VER_PLATFORM_WIN32_WINDOWS) and
      ((Win32MajorVersion > 4) or ((Win32MajorVersion = 4) and
      (Win32MinorVersion > 0)))) then
    begin
      // Code from Karl E. Peterson, [url]www.mvps.org/vb/sample.htm[/url]
      // Converted to Delphi by Ray Lischner
      // Published in The Delphi Magazine 55, page 16

      Result := False;
      ForegroundThreadID := GetWindowThreadProcessID(GetForegroundWindow, nil);
      ThisThreadID := GetWindowThreadPRocessId(hwnd, nil);
      if AttachThreadInput(ThisThreadID, ForegroundThreadID, True) then
      begin
        BringWindowToTop(hwnd); // IE 5.5 related hack
        SetForegroundWindow(hwnd);
        AttachThreadInput(ThisThreadID, ForegroundThreadID, False);
        Result := (GetForegroundWindow = hwnd);
      end;
      if not Result then
      begin
        // Code by Daniel P. Stasinski
        SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timeout, 0);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(0),
          SPIF_SENDCHANGE);
        BringWindowToTop(hwnd); // IE 5.5 related hack
        SetForegroundWindow(hWnd);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(timeout), SPIF_SENDCHANGE);
      end;
    end
    else
    begin
      BringWindowToTop(hwnd); // IE 5.5 related hack
      SetForegroundWindow(hwnd);
    end;

    Result := (GetForegroundWindow = hwnd);
  end;
end;





procedure bild_speichern(AControl :TWinControl;datei:string);
var
  DC: HDC;
  h: TBitmap;
begin
  if not Assigned(AControl) then Exit;
  try
     h:=TBitmap.Create;

     DC :=GetWindowDC(AControl.Handle);
     h.Width  :=AControl.Width;
     h.Height :=AControl.Height;
     with h do
     BitBlt(Canvas.Handle, 0, 0, Width, Height, DC, 0, 0, SrcCopy );

     ReleaseDC(AControl.Handle, DC);
     h.savetofile(datei);
  finally
    h.free;
  end;
end;
procedure TForm_main.Zeitvorgabenbernehmen1Click(Sender: TObject);
var
y,m,d:word;
query,von,bis,s,s1,s2,s3,s4,s5,s6,s7,s8:string;
begin
     if nicht_satellit_tab then exit;
     if  b.b_string('firma-zeitabrechnung')<2 then exit;

    if Messagedlg('Sollen die Zeitvorgaben f�r alle Firmen �bernommen werden?',mtConfirmation, mbOkCancel	,0)<>mrOK then exit;


    Form_uebername_jahr:= tForm_uebername_jahr.create(self);
    with Form_uebername_jahr do
    begin
       decodedate(now(),y,m,d);
       maskedit_von.editText:='01.01.'+inttostr(y-1);
       maskedit_bis.editText:='31.12.'+inttostr(y-1);

       if Form_uebername_jahr.ShowModal=mrok then
       begin
         von:=dat_delphi_to_sql(maskedit_von.Edittext);
         bis:=dat_delphi_to_sql(maskedit_bis.Edittext);
         query:=format('select * from firma_zeiten_soll where (von>="%s") and (bis <="%s")  ',[von,bis]);
         datamodul.sql_new( true, datamodul.q_1,query, 'i_firma');
         datamodul.q_1.first;
         while not datamodul.q_1.Eof do
         begin

                von:=datamodul.q_1.findfield('von').asstring;
                bis:=datamodul.q_1.findfield('bis').asstring;
                s1:=copy(von,7,4);
                s2:=copy(von,0,6);
                y:=strtoint(s1);
                y:=y+spinedit_y.Value;
                von:=s2+inttostr(y);

                s1:=copy(bis,7,4);
                s2:=copy(bis,0,6);
                y:=strtoint(s1);
                y:=y+spinedit_y.Value;
                bis:=s2+inttostr(y);

                s1:=datamodul.q_1.findfield('i_firma').asstring;
                s2:=datamodul.q_1.findfield('einsatzstunden').asstring;
                s3:=datamodul.q_1.findfield('stunden_tag').asstring;
                s4:=datamodul.q_1.findfield('i_zeitkonto').asstring;
                s5:=datamodul.q_1.findfield('i_abrechnungskriterium').asstring;
                s6:=datamodul.q_1.findfield('zeitkonto').asstring;
                s7:=datamodul.q_1.findfield('abrechnungskriterium').asstring;
                s8:=datamodul.q_1.findfield('memo').asstring;
                von:=dat_delphi_to_sql(von);
                bis:=dat_delphi_to_sql(bis);

                query:=format('select nummer from firma_zeiten_soll where (i_firma=%s) and (i_zeitkonto=%s) and (i_abrechnungskriterium=%s)  and (((von>="%3:s") and (von <="%4:s")) or((bis>="%3:s") and (bis <="%4:s"))) ',[s1,s4,s5,von,bis]);
                if not mysql_d.sql_exe_rowsvorhanden(query) then
                begin
                   neuer_datensatz(datamodul.q_firma_zeiten_soll,['nummer','i_firma','von','bis','einsatzstunden','stunden_tag','i_zeitkonto','i_abrechnungskriterium','zeitkonto','abrechnungskriterium','memo'],[null,s1,von,bis,s2,s3,s4,s5,s6 ,s7,s8]);
                   datamodul.q_firma_zeiten_soll.Post;
                end
                else
                begin
                  datamodul.q_firma_zeiten_soll.edit;
                  datamodul.q_firma_zeiten_soll.FindField('storno').asinteger:=0;
                  datamodul.q_firma_zeiten_soll.Post;
                end;
           	datamodul.q_1.Next;
         end;
       end;
    end;
    form_uebername_jahr.Free;
    pagecontrol_firma.ActivePage:=tabsheet_firmenadresse;
end;

procedure TForm_main.pm_f_rechnung_wiederherstellenClick(Sender: TObject);
var
snummer:string;
query:string;
begin
//rechnung firma
if MessageDlg('Soll der gel�schte Datensatz wieder hergestellt werden??', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
if datamodul.q_rechnung.findfield('storno').asinteger=0 then exit;

   sNummer :=datamodul.q_rechnung.findfield('nummer').asstring;
   query:=format('update rechnung set storno=0 where nummer=%s',[snummer]);
   mysql_d.sql_exe(query);
   query:=format('update re_positionen set storno=0 where i_rechnung=%s',[snummer]);
   mysql_d.sql_exe(query);

datamodul.q_rechnung.Refresh;
end;

procedure TForm_main.pm_zeit_verschiebenClick(Sender: TObject);
vAR query:string;
	  snr:string;
begin
//if  b.b_string('proband-Verschieben')=-1 then exit;
with datamodul do
begin
   if dbgridext_zeitabrechnung.SelectedRows.Count>1 then
   begin
    showmessage('Es darf nur eine Zeile ausgew�hlt sein.');
    exit;
   end;

   if not ( q_firma_sollist.State in [dsedit]) then
   begin
      showmessage('Datensatz nicht im �nderungsmodus');
   	exit;
   end;
	form_verschieb:=TForm_verschieb.create(self);
  form_verschieb.Caption:='der Zeiteintrag wird zur ausgew�hlten Firma verschoben';
	query:=format('select * from firma where nummer in (%s)',[firmen_berechtigung]);
	sql(true,q_1,query,'firma');
	if form_verschieb.ShowModal=mrok then
	begin
      snr:=q_1.findfield('nummer').asstring;
		q_firma_sollist.findfield('i_firma').asstring:=snr;
		direktspeichern:=true;
		q_firma_sollist.Post;
		q_firma_sollist.Refresh ;
	end;
	form_verschieb.Release;

   pagecontrol_firma.ActivePage:=TabSheet_FirmenAdresse;
   q_firma.Locate('nummer',snr,[]);
   application.ProcessMessages;
	//gehezu_stammdaten(manr);
end;

end;

function tform_main.nummern_kreis_fehler:boolean;
var
  intervall_firma,min_firma, max_firma,nk:integer;
	  intervall,min,max,aktuell_nkr,aktuell_nr:int64;
	  query,tabelle,s_min,s_max,s_aktuell:string;
begin
query:='select nummern_kreis from haupt_tabelle';
nk:=strtoint(mysql_d.Feldinhalt(query,0));

intervall:=1000000000000; //1 e 12
intervall_firma:=100000;    //1 e 5
min:=nk*intervall;
max:=min+intervall-1;
min_firma:=nk*intervall_firma;
max_firma:=min_firma+intervall_firma-1;
result:=false;
with datamodul do
begin
   sql(true,q_9,'select * from nummern_kreis','');
	 q_9.first;
	 while not q_9.eof do
	 begin
      tabelle:=q_9.findfield('tab_name').asstring;
      aktuell_nkr:=getbigint(q_9,'aktuell');
      if tabelle='firma' then
      begin
        s_min:=inttostr(min_firma);
        s_max:=inttostr(max_firma);
      end
      else
      begin
        s_min:=inttostr(min);
        s_max:=inttostr(max);
      end;
      query:=format('select max(nummer) from %s where nummer>=%s and nummer<=%s',[tabelle,s_min,s_max]);
      s_aktuell:=mysql_d.Feldinhalt(query,0);
      if s_aktuell='' then s_aktuell:=s_min;
      aktuell_nr:=strtoint64(s_aktuell);

      if aktuell_nkr<aktuell_nr then
      begin
        result:= true;
      end;
      
	  q_9.next;
	end;

end;

end;

procedure tform_main.aktualisiere_nummern_kreis_aktuell;
var
query:string;
i:integer;
begin
    //if ist_hauptsystem then
    begin
      query:='select nummern_kreis from haupt_tabelle';
      i:=strtoint(mysql_d.Feldinhalt(query,0));
      aktualisiere_nummern_kreis(i,'');
   end;
end;

procedure tform_main.aktualisiere_nummern_kreis(nk:integer;modus:string);
var intervall_firma,min_firma, max_firma:integer;
	  intervall,min,max,a,b:int64;
	  query,tabelle,s_min,s_max,s_aktuell:string;
begin
intervall:=1000000000000; //1 e 12
intervall_firma:=100000;    //1 e 5
min:=nk*intervall;
max:=min+intervall-1;
min_firma:=nk*intervall_firma;
max_firma:=min_firma+intervall_firma-1;

with datamodul do
begin
   datamodul.sql(true,datamodul.q_9,'select * from nummern_kreis','');
	q_9.first;
	while not q_9.eof do
	begin
	  tabelle:=q_9['tab_name'];
	  if tabelle='firma' then
		begin
			s_min:=inttostr(min_firma);
			s_max:=inttostr(max_firma);
		end
	  else
	  begin
			s_min:=inttostr(min);
			s_max:=inttostr(max);
		end;

	  query:=format('select max(nummer) from %s where nummer>=%s and nummer<=%s',[tabelle,s_min,s_max]);   //gr��ter wert    / falls auf dem Sat gel�scht wurde kann der wert 0 sein obwhol Daten extistieren!!!
	  s_aktuell:=mysql_d.Feldinhalt(query,0);
	  if s_aktuell='' then s_aktuell:=s_min;   //wenn kein datensatz vorhanden
	  q_9.edit;
    if modus='ini' then  //grenzen setzten
    begin
      setbigint(q_9,'von',StrToInt64Def(s_min,0));
      setbigint(q_9,'bis',StrToInt64Def(s_max,0));
      setbigint(q_9,'aktuell',StrToInt64Def(s_aktuell,0));
    end
    else
    begin    //nur aktuellen wert setzen
      setbigint(q_9,'von',StrToInt64Def(s_min,0));
      setbigint(q_9,'bis',StrToInt64Def(s_max,0));

      a:=getbigint(q_9,'aktuell'); //eingetragener wert
      b:= StrToInt64Def(s_aktuell,0); //max gefunden
      if b>a then setbigint(q_9,'aktuell',b); //nur wenn gr��er  (sollte eigentlihc nicht vorkommen)
    end;
	  q_9.Post;
	  q_9.next;
	end;

end;
end;


procedure tform_main.ueberfluessiges_loeschen;
var
query,snummer,s,s_count: string;
i:integer;
begin
//l�schen von
{.

f�r alle probanden where i_firma not in sfirmen
l�schen von   befunde, dokumente, impfung Labor, diagnosen...

l�schen wenn keine Berechtigung
  dokumenten
  texten
  befunden
      akt_untersuchung
  labor
  impfung
  untersuchung
  ambulanz
  besonderheit
  rechnung_proban
  rechnung_firma

}
//nur auf dem satelliten
if ist_hauptsystem then exit;

try
b.berechtigungen_lesen;
form_memo:=tform_memo.Create(self);
form_memo.Caption:='Anpassen der Datenbank';
form_memo.Memo.ScrollBars:=ssVertical;
//form_memo.Panel1.Visible:=false;
form_memo.Memo.Text:='';
form_memo.BitBtn_ok.Enabled:=false;
form_memo.BitBtn_abb.visible:=false;
form_memo.Show;// modal=mrok then

screen.Cursor:=crhourglass;
application.ProcessMessages;
with datamodul do
begin
  //
  //l�schen  wenn keine Berechtigung f�r die Tabelle besteht
  //diese Tabellen du�rfen nur abgeglichen werden wenn die Berechtigung besteht!!!

  if (b.b_string_silent('Firma-Dokumente') =-1) and (table_length('texte')>0) then
  begin
      query:= 'delete from texte';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Texte (Firma-Dokumente), die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
  application.ProcessMessages;
  if (b.b_string_silent('Proband-Dokumente-Arzt') =-1) and (b.b_string_silent('Proband-Dokumente-Sani') =-1) and (table_length('dokumente')>0) then
  begin
      query:= 'delete from dokumente ';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Dokumente (Proband-Dokumente-Arzt /Proband-Dokumente-Sani ) , die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
  application.ProcessMessages;
  if (b.b_string_silent('Proband-Bescheinigung') =-1) and (b.b_string_silent('Proband-Bescheinigung-AG') =-1) and (table_length('untersuchung')>0) then
  begin
      query:= 'delete from untersuchung';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Bescheinigungen (Proband-Bescheinigung / Proband-Bescheinigung-AG ), die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
  application.ProcessMessages;
 if (b.b_string_silent('Proband-Befunde') =-1) and (table_length('befunde')>0) then
  begin
      query:= 'delete from befunde';
      mysql_d.sql_exe(query);
      query:= 'delete from akt_untersuchung';
      mysql_d.sql_exe(query);
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Befunde (Proband-Befunde), die Eintr�ge wurden gel�scht.');
  end;
  application.ProcessMessages;
 if (b.b_string_silent('Proband-Impfung') =-1) and (table_length('impfung')>0) then
  begin
      query:= 'delete from impfung';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Impfungen (Proband-Impfung), die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
  application.ProcessMessages;
  if (b.b_string_silent('Proband-Labor') =-1) and (table_length('labor')>0) then
  begin
      query:= 'delete from labor';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Labor (Proband-Labor), die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
  application.ProcessMessages;
  if (b.b_string_silent('Proband-Besonderheiten') =-1) and (table_length('besonderheiten')>0) then
  begin
      query:= 'delete from besonderheiten';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Besonderheiten (Proband-Besonderheiten), die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
  application.ProcessMessages;
  if (b.b_string_silent('Proband-Rechnung') =-1) and (b.b_string_silent('Firma-Rechnung') =-1) and  (table_length('rechnung')>0) then
  begin
      query:= 'delete from rechnung ';//where i_mitarbeiter>0';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Rechnungen (Proband-Rechnung/Firma-Rechnung), die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;
 { if b.b_string_silent('Firma-Rechnung') =-1 then
  begin
      query:= 'delete from rechnung where i_mitarbeiter=-1';
      form_memo.Memo.Lines.Add('Sie haben keine Berechtigung f�r Rechnung-Firma, die Eintr�ge wurden gel�scht.');
      mysql_d.sql_exe(query);
  end;}


   //l�schen wenn keien Berechtigung f�r den Probanden besteht
   // kann nur bei ge�nderter berechtigung f�r Firmen auftreten
   query:=format('select count(*) from mitarbeiter where i_firma not in (%s)',[firmen_berechtigung]);
   s_count:=mysql_d.Feldinhalt(query,0);
   if s_count<>'0' then
   begin
       form_memo.Memo.Lines.Add(format( 'Sie haben keine Berechtigung f�r %s Probanden, die Tabellen werden angepasst ',[s_count]));
       application.ProcessMessages;
       query:=format('select nummer from mitarbeiter where i_firma not in (%s)',[firmen_berechtigung]);
       form_memo.Memo.Lines.Add('  ');
       i:=0;
       sql(true,q_1,query,'');
       while not q_1.eof do
       begin

          inc(i);
          if i/10=i div 10 then
          begin
            form_memo.Memo.Lines.Delete(form_memo.Memo.Lines.Count-1);
            form_memo.Memo.Lines.Add(format ('Proband  %s  von  %s  wird bearbeitet',[inttostr(i),s_count]));
          end;

          snummer:=q_1.findfield('nummer').asstring;
          query:= format('delete from dokumente where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Dokumente ');
          mysql_d.sql_exe(query);
          query:= format('delete from diagnosen where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Diagnosen');
          mysql_d.sql_exe(query);
          query:= format('delete from besonderheiten where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Besonderheiten');
          mysql_d.sql_exe(query);
          query:= format('delete from labor where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Labor');
          mysql_d.sql_exe(query);
          query:= format('delete from untersuchung where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Untersuchung');
          mysql_d.sql_exe(query);
          query:= format('delete from impfung where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Impfung');
          mysql_d.sql_exe(query);
          query:= format('select * from rechnung where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Rechnung_Proband');
          sql(true,q_2,query,'');
            while not q_2.eof do
            begin
               s:=q_2.findfield('nummer').asstring;
               query:= format('delete from re_positionen where i_rechnung=%s',[s]);
              mysql_d.sql_exe(query);
              q_2.Next;
            end;
          query:= format('delete from rechnung where i_mitarbeiter=%s',[snummer]);
          mysql_d.sql_exe(query);
          query:= format('delete from ambulanz where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Ambulanz');
          mysql_d.sql_exe(query);
          query:= format('select * from  befunde where i_mitarbeiter=%s',[snummer]);
          //form_memo.Memo.Lines.Add('  Anpassen: Befunde');
          sql(true,q_2,query,'');
            while not q_2.eof do
            begin
               s:=q_2.findfield('nummer').asstring;
               query:= format('delete from akt_untersuchung where i_untersuchung=%s',[s]);
              mysql_d.sql_exe(query);
              q_2.Next;
            end;
         application.ProcessMessages;
        q_1.Next;
       end;
    end;
   //l�schen wenn keine berechtigung f�r die firma besteht
   //nur bei ge�nderter Berechtigung f�r die Firma
   query:=format('select count(*) from firma where nummer not in (%s)',[firmen_berechtigung]);
   s_count:=mysql_d.Feldinhalt(query,0);
   if s_count<>'0' then
   begin
       form_memo.Memo.Lines.Add(format( 'Sie haben keine Berechtigung f�r %s Firmen, die Tabellen werden angepasst ',[s_count]));

       query:=format('select * from firma where nummer not in (%s) ',[firmen_berechtigung]);
       sql(true,q_1,query,'firma');
       application.ProcessMessages;
       form_memo.Memo.Lines.Add('  ');
       i:=0;
       while not q_1.eof do
       begin

          inc(i);
          if i/10=i div 10 then
          begin
            form_memo.Memo.Lines.Delete(form_memo.Memo.Lines.Count-1);
            form_memo.Memo.Lines.Add(format ('Firma  %s  von  %s  wird bearbeitet',[inttostr(i),s_count]));
          end;
          //form_memo.Memo.Lines.Add('  Anpassen: '+q_1.findfield('firma').asstring);
          snummer:=q_1.findfield('nummer').asstring;
          query:= format('delete from texte where i_master=%s',[snummer]);
          mysql_d.sql_exe(query);
          query:= format('delete from rechnung where i_firma=%s',[snummer]);
          mysql_d.sql_exe(query);
          query:= format('delete from re_positionen where i_firma=%s',[snummer]);
          mysql_d.sql_exe(query);
          query:= format('delete from firma_sollist where i_firma=%s',[snummer]);
          mysql_d.sql_exe(query);
         application.ProcessMessages;
        q_1.Next;
       end;

      query:=format('delete from mitarbeiter where i_firma not in (%s)',[firmen_berechtigung]);
      form_memo.Memo.Lines.Add(' Anpassen: Probanden');
      mysql_d.sql_exe(query);

      query:=format('delete from firma where nummer not in (%s)',[firmen_berechtigung]);
      form_memo.Memo.Lines.Add(' Anpassen: Firmen');
      mysql_d.sql_exe(query);
      form_memo.Memo.Lines.Add('Die Anpassung der Tabellen an Ihre Berechtigungen ist abgeschlossen');
   end;
end;
finally
screen.Cursor:=crdefault;
form_memo.Visible:=false;
//form_memo.Panel1.Visible:=true;
form_memo.BitBtn_ok.Enabled:=true;
//form_memo.BitBtn_abb.Enabled:=true;
if  form_memo.Memo.Lines.Count>0 then form_memo.ShowModal;
form_memo.Free;
end;

end;

procedure xms(s: string; var gruppe, variable, wert:string);
var i:integer;
begin
   i:=pos (' ',s);
   gruppe:=copy(s,2,i-2);
   s:=copy( s,i,length(s));
   i:=pos ('=',s);
   variable:=copy(s,1,i-1);
   variable:=trim(variable);
   s:=copy( s,i,length(s));
   i:=pos ('"',s);
   s:=copy(s,i+1,length(s));
   i:=pos ('"',s);
   wert:=copy(s,1,i-1);
end;

function GetFileSize(const szFile: String): Int64;
var
  fFile: THandle;
  wfd: TWIN32FINDDATA;
begin
  result := 0;
  if not FileExists(szFile) then exit;
  fFile := FindFirstfile(pchar(szFile),wfd);
  if fFile = INVALID_HANDLE_VALUE then exit;
  result := (wfd.nFileSizeHigh*(MAXDWORD))+wfd.nFileSizeLow;
  windows.FindClose(fFile);
end;

procedure TForm_main.SpeedButton_beschdruck_probandClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
begin
 datamodul.q_untersuchung.edit;
	 datamodul.q_untersuchung['dat_gedruckt'] :=int(form_kalender.auswahlkalender.date);
	 //datamodul.q_untersuchung.post;
	 //gehezuuntersuchung(1,i_stammdaten, i_nummer);
	 //dbtreeview.knoten_neuordnen(dbtreeview.selected);
	 baum_neu:=true;
   //if (sender =speedbutton_beschdruck_proband) then dbedit_druck.Text:=form_kalender.maskedit1.Text;
   //if (sender =speedbutton_beschdruck_firma) then dbedit_druck_firma.Text:=form_kalender.maskedit1.Text;
end;
end;

procedure TForm_main.DBEdit_druckChange(Sender: TObject);
begin
try
   if tdbedit(sender).text='' then exit;
   if strtodate(tdbedit(sender).text)=null_datum then tdbedit(sender).Visible:=false else tdbedit(sender).visible:=true;
except
end;
end;

procedure TForm_main.m_AnredeClick(Sender: TObject);
var ber:integer;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;
speichern;
form_tabellen:=tform_tabellen.Create(self);
//form_tabellen.HelpContext:=10840;
form_tabellen_berechtigung(ber);
form_tabellen.Caption:='Anreden';
form_tabellen.Notebook.PageIndex:=16;
//form_tabellen.helpcontext:=49;  //2010

datamodul.sql(false,datamodul.q_1,'select * from anrede order by name','');

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='name';
form_tabellen.DBGrid_tabellen.Columns[0].width:=60;
form_tabellen.DBEdit_an_name.DataField:='name';
form_tabellen.DBEdit_an_brief.DataField:='anrede_brief';
form_tabellen.DBEdit_an_dativ.DataField:='anrede_dativ';
form_tabellen.showmodal;

form_tabellen.release;
combolesen_s(tcombobox(dbcombobox_anrede),'anrede','name','name','');
end;



procedure TForm_main.BitBtn_zeit_mwahlClick(Sender: TObject);
var
snr:string;
begin
   leistungsauswahl('taetigkeit');
	//snr:=datamodul.q_firma_sollist.findfield('i_abrechnungs_anlass').AsString;
	//tateigkeit_auswahl('einfuegen',snr );
end;




procedure tform_main.tbs_auswahl(modus:string;tab_nr:integer;snr,suche:string );
var
ber,i:integer;
query,s_abrechnungs_anlass, s_tbs,tab_name,zeilen_name,s_rubrik:string;
daten:pnodepointer;
nr,i_tbs:int64;
form_untwahl_tbs:tform_untwahl;
knoten,sel: ttreenode;
list:tstringlist;
begin
//eing�gen h�kchen setzen und einf�gen in dok


with datamodul do try
  //speichern;
  query:='select * from tabelle_hinweise'; //f�r insert
  datamodul.sql_new(false,datamodul.q_6,query,'');

   case tab_nr of
    1:;
    2:;
    3:begin
        tab_name:='labor';
        s_rubrik:='Laborhinweis: ';

        query:=format('select untersuchung from typ where nummer=%s',[snr]);
        if modus='auswahl' then query:=format('select untersuchung from typ left join labor on (labor.i_typ=typ.nummer) where labor.nummer=%s',[snr]);
      end;
  end;
  //s_abrechnungs_anlass := q_firma_sollist.findfield('i_abrechnungs_anlass').asstring;
  zeilen_name:=s_rubrik+mysql_d.feldinhalt(query,0);


  if modus='einfuegen' then
  begin
      query:= format('select * from textbausteine left join tabelle_tbs on (tabelle_tbs.i_tab_nummer=%0:s.nummer) where tabelle_tbs.storno=0 and textbausteine.i_kapitel>=0 order by reihenfolge ',[tab_name]);

  end                                                                                                                                                //tabelle_tbs.i_tab_nummer=%d and
  else
  if modus ='einstellen' then   query:= format('select * from textbausteine   where  textbausteine.storno=0 and textbausteine.i_kapitel>=0 order by reihenfolge ',[])           //and all=0
  else
  if modus ='edit' then   query:= format('select * from textbausteine where tabelle_tbs.storno=0 and textbausteine.i_kapitel>=0 order by reihenfolge ',[tab_name])
  else
  if modus ='auswahl' then   query:= format('select * from textbausteine   where  textbausteine.storno=0 and textbausteine.i_kapitel>=0 order by reihenfolge ',[]) ;

  datamodul.sql_new(false,datamodul.q_5,query, '');

  //###############
  form_untwahl_tbs:=tform_untwahl.Create(self);
  form_untwahl_tbs.anzeigen:=false;
  form_untwahl_tbs.Caption:=zeilen_name;
  form_main.sperren(form_untwahl_tbs.notebook);
  form_untwahl_tbs.ToolButton_new.Enabled:=false;
	form_untwahl_tbs.notebook.visible:=true;
	form_untwahl_tbs.notebook.pageindex:=9;
	form_untwahl_tbs.multiselect:=true;
	form_untwahl_tbs.ToolBar.Visible:=true;
	form_untwahl_tbs.Panel_botttom.Visible:=false;

   form_untwahl_tbs.DBMemo_tbs.DataSource:=datamodul.ds_5;
   form_untwahl_tbs.DBEdit_tbs_name.DataSource:=datamodul.ds_5;
   
   //form_untwahl_l.DBCheckBox_ausblenden.DataSource:=datamodul.ds_4;
   form_untwahl_tbs.tree.mysqlquery:=datamodul.q_5;
	form_untwahl_tbs.tree.mysql_feld:='name';
	form_untwahl_tbs.tree.liste_lesen;
   form_untwahl_tbs.tree.AutoExpand:=false;
   form_untwahl_tbs.einzelauswahl:=true;
   form_untwahl_tbs.snr:=snr;
   if modus='einfuegen' then
    begin
    	//form_untwahl_tbs.Panel_botttom.visible:=true;
      form_untwahl_tbs.ToolBar.Visible:=false;
      form_untwahl_tbs.DBCheckBox_Aufwand_all.visible :=false;
      form_untwahl_tbs.DBCheckBox_ausblenden.Visible:=false;
    end;


  //h�kchen setzen   und grenze eintragen
  if modus ='einstellen' then
  begin
      //form_untwahl_tbs.Panel_botttom.visible:=true;
  		query:=format('select * from tabelle_tbs where i_tab_nummer=%s and storno=0',[snr]);
      datamodul.sql_new(false,datamodul.q_2,query, '');
      datamodul.q_2.first;
      while not datamodul.q_2.eof do
      begin
      	i_tbs:=getbigint( datamodul.q_2,'i_tbs');
         knoten:=form_untwahl_tbs.Tree.suche_nummer(i_tbs);
         if knoten<>nil then
          begin
            knoten.StateIndex:=3;
            daten:=knoten.data;
           
            list :=tstringlist.Create;
            list.Add(datamodul.q_2.findfield('i1').asstring);
            list.Add(datamodul.q_2.findfield('i2').asstring);
            list.Add(datamodul.q_2.findfield('i1w').asstring);
            list.Add(datamodul.q_2.findfield('i2w').asstring);
            list.Add(datamodul.q_2.findfield('s1').asstring);
            daten^.liste:=list;
            knoten.data:=daten;
          end;
      	datamodul.q_2.Next;
      end;
  end;

  if modus ='auswahl' then
  begin

      form_untwahl_tbs.Panel_botttom.visible:=true;
      form_untwahl_tbs.ToolBar.Visible:=false;
  		query:=format('select * from tabelle_hinweise where i_tabelle=3 and i_tab_nummer=%s and storno=0',[snr]);
      datamodul.sql_new(false,datamodul.q_2,query, '');
      datamodul.q_2.first;
      while not datamodul.q_2.eof do
      begin
      	i_tbs:=getbigint( datamodul.q_2,'i_tbs');
         knoten:=form_untwahl_tbs.Tree.suche_nummer(i_tbs);
         if knoten<>nil then
          begin
            knoten.StateIndex:=3;

          end;
      	datamodul.q_2.Next;
      end;

      form_untwahl_tbs.Tree.FullCollapse;
      form_untwahl_tbs.Edit_such.Text:=suche;
      form_untwahl_tbs.tree.suche_datensatz(suche);
      form_untwahl_tbs.GroupBox_tbs_auswahl.visible:=false;
  end;


 //form_untwahl_l.HelpContext:= 89;

 //bis hier  vorbereiten
 //form_untwahl_tbs.Tree.Selected:=form_untwahl_tbs.Tree.Items.GetFirstNode;
  form_untwahl_tbs.anzeigen:=true;
 form_untwahl_tbs.TreeChange(self, form_untwahl_tbs.Tree.Selected);


if (form_untwahl_tbs.showmodal=mrok)  then
begin

      if (modus='einfuegen') then
        begin
           //dbedit_anlass.Text:=datamodul.q_3['name'];

           sel:=form_untwahl_tbs.Tree.Selected;
           for i:=0 to form_untwahl_tbs.Tree.Items.Count-1 do
           begin

           		if (( form_untwahl_tbs.Tree.Items[i].StateIndex=3) or (form_untwahl_tbs.Tree.Items[i]=sel)) then
               	begin
                     daten:=form_untwahl_tbs.Tree.Items[i].data;
                     nr:=daten^.nummer;
                     q_4.Locate('nummer',nr,[]);
                     if (q_4.FindField('i_all').asinteger=0) or (q_5.FindField('i_ausblenden').asinteger=0) then
                     begin    //nicht in der ersten Ebene
                        if dbmemo_firma_sollist.Text<>'' then dbmemo_firma_sollist.Text:=dbmemo_firma_sollist.Text+#13#10;
                  		  dbmemo_firma_sollist.Text:=dbmemo_firma_sollist.Text+ form_untwahl_tbs.Tree.Items[i].Text;
                        dbmemo_firma_sollist.SetFocus;
                     end;
                  end;
           end;

        end;


        if (modus='einstellen') then
        begin
        // alle mit haken in die Tabelle einf�gen
        query:=format('update tabelle_tbs set storno=1 where i_tab_nummer=%s and storno=0',[snr]);
			  mysql_d.sql_exe(query);
         for i:=0 to form_untwahl_tbs.Tree.Items.Count-1 do
           begin
          		if  (form_untwahl_tbs.Tree.Items[i].StateIndex=3)  then //alle mit Haken
               	begin
                     daten:=form_untwahl_tbs.Tree.Items[i].data;
                     nr:=daten^.nummer;
                     s_tbs:=inttostr(nr);

                    tabelle_tbs_einfuegen('3',snr,s_tbs,daten^.liste);
                    // anlass_aufwand_einfuegen(snr,saufwand);
                  end;
           end;
        end;

        if (modus='auswahl') then
        begin
         //l�schen alles was zur aktuellen laboruntersuchung geh�rt
        	// alle mit haken in die Tabelle q_hinweis einf�gen
        query:=format('update tabelle_hinweise set storno=1 where i_tabelle=3 and  i_tab_nummer=%s ',[snr]);
			  mysql_d.sql_exe(query);
         for i:=0 to form_untwahl_tbs.Tree.Items.Count-1 do
           begin
          		if  (form_untwahl_tbs.Tree.Items[i].StateIndex=3)  then //alle mit Haken
               	begin
                     daten:=form_untwahl_tbs.Tree.Items[i].data;
                     nr:=daten^.nummer;
                     s_tbs:=inttostr(nr);
                    query:=format('select nummer from tabelle_hinweise where  i_tabelle=3 and  i_tab_nummer=%s and i_tbs=%s',[snr,s_tbs]);
                    if mysql_d.Feldinhalt(query,0)='' then
                      begin
                        neuer_datensatz(datamodul.q_6,['nummer','i_tabelle','i_tab_nummer','i_tbs'],[null,3,snr,s_tbs]);
                        datamodul.q_hinweis.Refresh;
                      end
                    else
                    begin
                       query:=format('update tabelle_hinweise set storno=0 where i_tabelle=3 and  i_tab_nummer=%s and i_tbs=%s',[snr,s_tbs]);
			                 mysql_d.sql_exe(query);
                       datamodul.q_hinweis.Refresh;
                    end  ;
                    
                  end;
           end;

        end;
end;
finally
  form_untwahl_tbs.Notebook.PageIndex:=0;
	form_untwahl_tbs.release;
	application.processmessages;
end;

end;



procedure tform_main.tateigkeit_auswahl(modus,snr:string );
var
ber,i:integer;
query,s_abrechnungs_anlass, saufwand:string;
daten:pnodepointer;
nr,i_aufwand:int64;
form_untwahl_l:tform_untwahl;
knoten,sel: ttreenode;
begin

with datamodul do try
  //speichern;
  s_abrechnungs_anlass := q_firma_sollist.findfield('i_abrechnungs_anlass').asstring;
  //if sender=BitBtn_zeit_mwahl then
  // query:=format('select aufwand.* from aufwand left join anlass_aufwand on (aufwand.nummer=anlass_aufwand.i_aufwand)  where ((anlass_aufwand.i_anlass=%s) or (aufwand.i_all=1))  ',[s_abrechnungs_anlass])     //and (aufwand.storno=0) and (anlass_aufwand.storno=0)
  //else
  if modus='einfuegen' then
  begin
   //if snr='0' then
      query:= format('select * from aufwand left join anlass_aufwand on (anlass_aufwand.i_aufwand=aufwand.nummer) where ((i_all=1) and (aufwand.storno=0)) or ((anlass_aufwand.i_anlass=%s) and (anlass_aufwand.storno=0) and (aufwand.storno=0)) order by reihenfolge ',[snr]);
   //else
  	  //	query:= format('select * from aufwand left join anlass_aufwand on (anlass_aufwand.i_aufwand=aufwand.nummer) where anlass_aufwand.i_anlass=%s and anlass_aufwand.storno=0 and aufwand.storno=0 order by reihenfolge ',[snr]);
  end
  else
  if modus ='einstellen' then   query:= format('select * from aufwand where  storno=0 order by reihenfolge ',[snr])            //and all=0
  else
  if modus ='edit' then   query:= format('select * from aufwand where  storno=0 order by reihenfolge ',[snr]);

  datamodul.sql_new(false,datamodul.q_4,query, '');

  form_untwahl_l:=tform_untwahl.Create(self);
  form_untwahl_l.Caption:='Abrechnungsanl�sse';
  form_main.sperren(form_untwahl_l.notebook);

	datamodul. sql_new(false,datamodul.q_4,query, '');
	form_untwahl_l.notebook.visible:=true;
	form_untwahl_l.notebook.pageindex:=7;
	form_untwahl_l.multiselect:=true;
	form_untwahl_l.ToolBar.Visible:=true;
	form_untwahl_l.Panel_botttom.Visible:=false;
	form_untwahl_l.caption:='T�tigkeiten';

   form_untwahl_l.DBmemo_aufwand.DataSource:=datamodul.ds_4;
   form_untwahl_l.DBCheckBox_Aufwand_all.DataSource:=datamodul.ds_4;
   form_untwahl_l.DBCheckBox_ausblenden.DataSource:=datamodul.ds_4;
   form_untwahl_l.tree.mysqlquery:=datamodul.q_4;
	form_untwahl_l.tree.mysql_feld:='name';
	form_untwahl_l.tree.liste_lesen;
   form_untwahl_l.tree.AutoExpand:=false;
   form_untwahl_l.einzelauswahl:=true;
   form_untwahl_l.snr:=snr;
   if modus='einfuegen' then
    begin
    	form_untwahl_l.Panel_botttom.visible:=true;
      form_untwahl_l.ToolBar.Visible:=false;
      form_untwahl_l.DBCheckBox_Aufwand_all.visible :=false;
      form_untwahl_l.DBCheckBox_ausblenden.Visible:=false;
    end;


  //h�kchen setzen
  if modus ='einstellen' then
  begin
      form_untwahl_l.DBCheckBox_Aufwand_all.visible :=false;
      form_untwahl_l.DBCheckBox_ausblenden.Visible:=false;
      form_untwahl_l.Panel_botttom.visible:=true;
      //form_untwahl_l.ToolBar.Visible:=false;

  		query:=format('select i_aufwand from anlass_aufwand where i_anlass=%s and storno=0',[snr]);
      datamodul.sql_new(false,datamodul.q_2,query, '');
      datamodul.q_2.first;
      while not datamodul.q_2.eof do
      begin
      	i_aufwand:=getbigint( datamodul.q_2,'i_aufwand');
         knoten:=form_untwahl_l.Tree.suche_nummer(i_aufwand);
         if knoten<>nil then knoten.StateIndex:=3;
      	datamodul.q_2.Next;
      end;
  end;

 form_untwahl_l.HelpContext:= 89;
	if (form_untwahl_l.showmodal=mrok)  then

      if (modus='einfuegen') then
        begin
           //dbedit_anlass.Text:=datamodul.q_3['name'];

           sel:=form_untwahl_l.Tree.Selected;
           for i:=0 to form_untwahl_l.Tree.Items.Count-1 do
           begin

           		if (( form_untwahl_l.Tree.Items[i].StateIndex=3) or (form_untwahl_l.Tree.Items[i]=sel)) then
               	begin
                     daten:=form_untwahl_l.Tree.Items[i].data;
                     nr:=daten^.nummer;
                     q_4.Locate('nummer',nr,[]);
                     if (q_4.FindField('i_all').asinteger=0) or (q_4.FindField('i_ausblenden').asinteger=0) then
                     begin    //nicht in der ersten Ebene
                        if dbmemo_firma_sollist.Text<>'' then dbmemo_firma_sollist.Text:=dbmemo_firma_sollist.Text+#13#10;
                  		dbmemo_firma_sollist.Text:=dbmemo_firma_sollist.Text+ form_untwahl_l.Tree.Items[i].Text;
                        dbmemo_firma_sollist.SetFocus;
                     end;
                  end;
           end;

        end
        else
        if (modus='einstellen') then
        begin
        	// alle mit haken in die Tabelle einf�gen
         query:=format('update anlass_aufwand set storno=1 where i_anlass=%s and storno=0',[snr]);
			mysql_d.sql_exe(query);
         for i:=0 to form_untwahl_l.Tree.Items.Count-1 do
           begin

           		if  (form_untwahl_l.Tree.Items[i].StateIndex=3)  then //alle mit Haken
               	begin
                     daten:=form_untwahl_l.Tree.Items[i].data;
                     nr:=daten^.nummer;
                     saufwand:=inttostr(nr);
                     anlass_aufwand_einfuegen(snr,saufwand);


                  end;
           end;

        end;


finally
	form_untwahl_l.release;
	application.processmessages;
end;

end;

procedure tform_main.tabelle_tbs_einfuegen(i_tabelle,i_tab_nummer,i_tbs:string;liste:tstringlist);
var
query,snummer,s1,sf1,sf2,sf1w,sf2w:string;
i1,i2,i1w,i2w:extended;
nummer:int64;
sar:array [1..3] of string;
begin
//gibts das paar
   i1:=strtofloat(liste[0]);
   i2:=strtofloat(liste[1]);
   i1w:=strtofloat(liste[2]);
   i2w:=strtofloat(liste[3]);


   if i2<i1 then i2:=9999;
   if i2w<i1w then i2w:=9999;
   sf1:=floattosqlstring(i1);
   sf2:=floattosqlstring(i2);
   sf1w:=floattosqlstring(i1w);
   sf2w:=floattosqlstring(i2w);
   s1:=liste[4];
   query:=format('select nummer from tabelle_tbs where i_tabelle=%s and i_tab_nummer=%s and i_tbs=%s ',[i_tabelle,i_tab_nummer,i_tbs]);
   if mysql_d.sql_exe_rowsvorhanden(query) then
    query:=	format('update tabelle_tbs set i1="%s", i2="%s",i1w="%s", i2w="%s", s1="%s", storno=0  where i_tabelle=%s and i_tab_nummer=%s and i_tbs="%s" ',[sf1,sf2,sf1w,sf2w,s1,i_tabelle,i_tab_nummer,i_tbs])
   else
    begin
      nummer:=mysql_d.neue_nummer('tabelle_tbs');
      snummer:=inttostr(nummer);
		if nummer=0 then exit; //nummer kann nicht gezogen werden
    	query:=	format('insert tabelle_tbs set nummer="%s", i_tabelle="%s", i_tab_nummer="%s" , i_tbs="%s", storno=0 ,i1="%s", i2="%s",i1w="%s", i2w="%s",s1="%s"',[snummer,i_tabelle,i_tab_nummer,i_tbs,sf1,sf2,sf1w,sf2w,s1]) ;
    end;
    mysql_d.sql_exe(query);
//gibts das paar gel�scht
end;


procedure tform_main.anlass_aufwand_einfuegen(snr,saufwand:string);
var
query,snummer:string;
nummer:int64;
begin
//gibts das paar
   query:=format('select nummer from anlass_aufwand where i_anlass=%s and i_aufwand=%s ',[snr,saufwand]);
   if mysql_d.sql_exe_rowsvorhanden(query) then
    query:=	format('update anlass_aufwand set storno=0 where i_anlass=%s and i_aufwand=%s  ',[snr,saufwand])
   else
    begin
      nummer:=mysql_d.neue_nummer('anlass_aufwand');
      snummer:=inttostr(nummer);
		if nummer=0 then exit; //nummer kann nicht gezogen werden
    	query:=	format('insert anlass_aufwand set nummer=%s, i_anlass=%s, i_aufwand=%s , storno=0 ',[snummer,snr,saufwand]) ;
    end;
    mysql_d.sql_exe(query);
//gibts das paar gel�scht
end;



procedure TForm_main.mAbrechnungTaetigkeitClick(Sender: TObject);
begin
 if b.b_string('Firmen-Listen')>-1 then
	tateigkeit_auswahl('edit','0' );
end;




procedure TForm_main.SpeedButton_firmasollist_zeitClick(Sender: TObject);
var
t1,t2,t3:ttime;
f:boolean;
s:string;
begin
f:=false;
if (DBEdit_time_sollistbeginn.Text<>'00:00') and ((DBEdit_stunden.Text='0') or ( DBEdit_stunden.Text='') ) and ((DBEdit_minuten.Text='0') or (DBEdit_minuten.Text='')) then
begin
 t2:=strtotime(DBEdit_time_sollistende.Text);
 t1:=strtotime(DBEdit_time_sollistbeginn.Text);
 if t2>t1 then
 t3:=t2-t1
 else
 t3:=1-t1+t2;

 s:=timetostr(t3);
 datamodul.q_firma_sollist.FindField('stunden').asstring:=copy(s,1,2);
 datamodul.q_firma_sollist.FindField('minuten').asstring:=copy(s,4,2);
 f:=true;
end;

if (DBEdit_time_sollistbeginn.Text<>'00:00') and (DBEdit_time_sollistende.Text='00:00')   then
begin
  t1:=strtotime(DBEdit_time_sollistbeginn.Text);
  //s:=inttostr_lf( datamodul.q_firma_sollist.FindField('stunden').asstring,'00');
  s:=inttostr_lf( dbedit_stunden.Text,'00');
  s:=s+':';
  //s:=s+inttostr_lf( datamodul.q_firma_sollist.FindField('minuten').asstring,'00');
  s:=s+inttostr_lf( dbedit_minuten.Text,'00');
  t2:=strtotime(s);
  t3:=t1+t2;
  s:=timetostr(t3);
  datamodul.q_firma_sollist.FindField('zeit_e').asstring:=s;
  f:=true;
end;

if (DBEdit_time_sollistbeginn.Text='00:00') and (DBEdit_time_sollistende.Text<>'00:00') then
begin
  t1:=strtotime(DBEdit_time_sollistende.Text);
  //s:=inttostr_lf( datamodul.q_firma_sollist.FindField('stunden').asstring,'00');
  s:=inttostr_lf( dbedit_stunden.Text,'00');
  s:=s+':';
  //s:=s+inttostr_lf( datamodul.q_firma_sollist.FindField('minuten').asstring,'00');
  s:=s+inttostr_lf( dbedit_minuten.Text,'00');
  t2:=strtotime(s);
  if t1>t2 then
  	t3:=t1-t2
  else
  	t3:=t1-(1-t2) ;

  s:=timetostr(t3);
  datamodul.q_firma_sollist.FindField('zeit_s').asstring:=s;
  f:=true;
end;

//if not f then showmessage('Der Endzeitpunkt oder die Dauer m�ssen "0" sein.');
end;

function inttostr_lf(s1,s2:string): string;
var
l:integer;
begin
   s1:=trim(s1);
   s2:=trim(s2);
   l:=length(s2);
	result:=s2+s1;
   result:=copy(result,length(result)-l+1,l);
end;


procedure tform_main.leistungsauswahl(parameter:string);
var ber,i:integer;
nr:int64;
s,w,query:string;
first:boolean;
sel:ttreenode;
daten:pnodepointer;
sl : TStringList;
ndx : Integer;

procedure alles_eintragen;
begin
          //datamodul.q_firma_sollist.edit;
           //datamodul.q_firma_sollist.FindField('anlass').asstring :=datamodul.q_3.findfield('name').asstring;
           if trim(datamodul.q_firma_sollist.FindField('anlass').asstring)<>'' then
           	datamodul.q_firma_sollist.FindField('anlass').asstring :=datamodul.q_firma_sollist.FindField('anlass').asstring+#13#10+datamodul.q_3.findfield('name').asstring
           else
            datamodul.q_firma_sollist.FindField('anlass').asstring :=datamodul.q_3.findfield('name').asstring;

            if trim(datamodul.q_firma_sollist.FindField('memo').asstring)<>'' then
           	datamodul.q_firma_sollist.FindField('memo').asstring :=datamodul.q_firma_sollist.FindField('memo').asstring+#13#10+datamodul.q_3.findfield('tbs').asstring
           else
            datamodul.q_firma_sollist.FindField('memo').asstring :=datamodul.q_3.findfield('tbs').asstring;

           if (datamodul.q_firma_sollist.FindField('stunden').asstring='0') and   (datamodul.q_firma_sollist.FindField('minuten').asstring='0') then
           begin
             if datamodul.q_3.findfield('zeitvorgabe').asstring<>'' then
             begin
               datamodul.q_firma_sollist.FindField('stunden').asstring :=copy(datamodul.q_3.findfield('zeitvorgabe').asstring,1,2);
               datamodul.q_firma_sollist.FindField('minuten').asstring :=copy(datamodul.q_3.findfield('zeitvorgabe').asstring,4,2);
             end;
           end;
           if datamodul.q_3.findfield('zeitansatz').asstring<>'' then datamodul.q_firma_sollist.FindField('zeitansatz').asstring :=datamodul.q_3.findfield('zeitansatz').asstring;
           if datamodul.q_3.findfield('i_kriterium').asinteger>=1 then datamodul.q_firma_sollist.FindField('i_kriterium').asinteger :=datamodul.q_3.findfield('i_kriterium').asinteger;
            if datamodul.q_3.findfield('i_zeitkonto').asinteger>=1 then datamodul.q_firma_sollist.FindField('i_zeitkonto').asinteger :=datamodul.q_3.findfield('i_zeitkonto').asinteger;

           datamodul.q_firma_sollist.FindField('i_abrechnungs_anlass').asstring :=datamodul.q_3.findfield('nummer').asstring;
           application.processmessages;
          //datamodul.q_firma_sollist.post;
end;

procedure memo_eintragen;
begin
//datamodul.q_firma_sollist.edit;
  if trim(datamodul.q_firma_sollist.FindField('anlass').asstring)<>'' then
   datamodul.q_firma_sollist.FindField('anlass').asstring :=datamodul.q_firma_sollist.FindField('anlass').asstring+#13#10+datamodul.q_3.findfield('name').asstring
  else
   datamodul.q_firma_sollist.FindField('anlass').asstring :=datamodul.q_3.findfield('name').asstring;

   if trim(datamodul.q_firma_sollist.FindField('memo').asstring)<>'' then
   datamodul.q_firma_sollist.FindField('memo').asstring :=datamodul.q_firma_sollist.FindField('memo').asstring+#13#10+datamodul.q_3.findfield('tbs').asstring
  else
   datamodul.q_firma_sollist.FindField('memo').asstring :=datamodul.q_3.findfield('tbs').asstring;

 //datamodul.q_firma_sollist.post;

end;
procedure parent_eintragen(n:string);
var query,p:string;
    felder: array [0..1] of string;
begin
if  n<>'0' then
begin
  query:= format('select name, parent from abrechnung_anlass where nummer=%s',[n]);
  mysql_d.Feldarray(query,[0,1],felder);
   if trim(datamodul.q_firma_sollist.FindField('anlass').asstring)<>'' then
   datamodul.q_firma_sollist.FindField('anlass').asstring :=datamodul.q_firma_sollist.FindField('anlass').asstring+#13#10+felder[0]
  else
   datamodul.q_firma_sollist.FindField('anlass').asstring :=felder[0];
  //n:=felder[1];
end;

end;

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxmain
begin
//if (parameter='edit') and nicht_satellit_tab then exit;

try

   s:=dbmemo_firma_sollist.Text;

       query:='select nummer, left(name,250) as name_f  from aufwand order by name';
       datamodul.sql_new(true, datamodul.q_7 ,query,'');
       query:='select * from abrechnung_anlass where storno=0';
		   datamodul.sql_new(false,datamodul.q_3,query, 'reihenfolge');
       form_suchliste:=tform_suchliste.create(self);
       form_suchliste.DBgrid.columns[0].fieldname:='name_f';
       form_suchliste.DBgrid.DataSource:=datamodul.ds_7;
       form_suchliste.s_name:='name_f';
       form_suchliste.tree.mysqlquery:=datamodul.q_3;
      form_suchliste.tree.mysql_feld:='name';
      form_suchliste.tree.liste_lesen;
      //form_suchliste.tree.AutoExpand:=false;
      if parameter='aufgabe' then  form_suchliste.CheckBox_aufgabenfeld.checked:=true;
      if parameter='taetigkeit' then  form_suchliste.CheckBox_taetigkeit.checked:=true;
      form_suchliste.startparameter:=parameter;

       if form_suchliste.ShowModal=mrok then
       begin

           if form_suchliste.CheckBox_aufgabenfeld.Checked then
           begin
              first:=true;
              sel:=form_suchliste.Tree.Selected;
              for i:=0 to form_suchliste.Tree.Items.Count-1 do
                 begin
                     if (( form_suchliste.Tree.Items[i].StateIndex=3) or (form_suchliste.Tree.Items[i]=sel)) then
                        begin
                           daten:=form_suchliste.Tree.Items[i].data;
                           nr:=daten^.nummer;


                           datamodul.q_3.Locate('nummer',nr,[]);

                          if form_suchliste.CheckBox_parent.Checked and first then parent_eintragen(datamodul.q_3.FindField('parent').asstring);

                           if first then
                              alles_eintragen
                           else
                              memo_eintragen;

                          first:=false;

                        end;
                 end;
              end;


              if form_suchliste.CheckBox_taetigkeit.Checked then
              begin

                 s:=datamodul.q_firma_sollist.FindField('memo').asstring;

                 if  form_suchliste.dbgrid.SelectedRows.Count>0 then
                  begin
                      for I := 0 to form_suchliste.dbgrid.SelectedRows.Count - 1 do
                        begin
                             datamodul.q_7.GotoBookmark(Pointer(form_suchliste.DBGrid.SelectedRows.Items[I]));
                             if length(s)>0 then s:=s+#13#10;
                             s:=s+datamodul.q_7.findfield('name_f').AsString;

                        end;
                  end
                  else
                  begin
                    if length(s)>0 then s:=s+#13#10;
                    s:=datamodul.q_7.findfield('name_f').AsString;
                  end;
                 datamodul.q_firma_sollist.FindField('memo').asstring:=s;
              end;
   		end;


finally
   form_suchliste.Release;
	application.processmessages;
end;
end;



procedure tform_main.leistung_edit;
var ber,i:integer;
nr:int64;
s,w,query:string;
first:boolean;
sel:ttreenode;
daten:pnodepointer;
sl : TStringList;
ndx : Integer;

//main
begin
//if (parameter='edit') and nicht_satellit_tab then exit;

try
  //speichern;

  //form_untwahl.Caption:='Abrechnungsanl�sse';


    //alle
	query:='select * from abrechnung_anlass where storno=0';


   datamodul.sql_new(false,datamodul.q_3,query, 'reihenfolge');

   form_untwahl:=tform_untwahl.Create(self);
  form_main.sperren(form_untwahl.notebook);
	form_untwahl.notebook.visible:=true;
	form_untwahl.notebook.pageindex:=5;
	form_untwahl.multiselect:=false;
	form_untwahl.ToolBar.Visible:=true;
	form_untwahl.Panel_botttom.Visible:=false;
	form_untwahl.caption:='Aufgabenfelder';
  //form_untwahl.DBEdit_leistung.DataSource:=datamodul.ds_3;
  form_untwahl.DBEdit_zeitansatz.DataSource:=datamodul.ds_3;
  form_untwahl.DBEdit_zeit.DataSource:=datamodul.ds_3;
  form_untwahl.DBmemo_zeit_aufgabenfeld.DataSource:=datamodul.ds_3;
  form_untwahl.DBmemo_zeit_leistung.DataSource:=datamodul.ds_3;
  form_untwahl.DBCombo_num_abrechnungskriterium.DataSource:=datamodul.ds_3;
  form_untwahl.DBCombo_num_abrechnung_zeitkonto.DataSource:=datamodul.ds_3;
  form_untwahl.DBCombo_num_abrechnungskriterium.lookup_laden;
  form_untwahl.DBCombo_num_abrechnung_zeitkonto.lookup_laden;

  	form_untwahl.tree.mysqlquery:=datamodul.q_3;
  	form_untwahl.tree.mysql_feld:='name';
	form_untwahl.tree.liste_lesen;
   form_untwahl.tree.AutoExpand:=false;
   form_untwahl.HelpContext:= 89;
   first:=true;


    //form_untwahl.showmodal;
	if (form_untwahl.showmodal=mrok)  then


finally
	form_untwahl.release;
	application.processmessages;
end;
end;




procedure TForm_main.DBComboBox_zeitansatzChange(Sender: TObject);
begin
if DBComboBox_zeitansatz.Text='' then DBComboBox_zeitansatz.Text:='0';
end;

function TForm_main.zeitkonto(akt_untersucher:integer):integer;
var
query:string;
begin
   try
     query:=format('select i_user from untersucher where nummer=%d' ,[akt_untersucher]);     //in i_user seht das Zeitkonto!!! nicht wundern
     result:=strtoint(mysql_d.Feldinhalt(query,0));
   except
      result:=1;
   end;
end;

procedure TForm_main.AnmeldeProtokoll1Click(Sender: TObject);
var
query:string;
begin
   if b.b_string('System')=-1 then exit;
	form_liste:=tform_liste.create(self);
   form_liste.Caption:='Anmeldeprotokoll';
   query:='select ts,funk, user_name from protokoll_user order by ts desc ';
    datamodul.sql_new(false,datamodul.q_7,query,'');

    form_liste.dbgridext.DataSource:=datamodul.ds_7;
    form_liste.s_name:='protokoll_user';
    form_liste.DBgridEXT.Columns[0].fieldname:='ts';
    form_liste.DBgridEXT.Columns[1].fieldname:='funk';
    form_liste.DBgridEXT.Columns[2].fieldname:='User_name';

    form_liste.DBgridEXT.Columns[0].Width:=100;
    form_liste.DBgridEXT.Columns[1].Width:=70;
    form_liste.DBgridEXT.Columns[2].Width:=120;
    form_liste.DBgridEXT.Columns[0].Title.caption:='Zeitpunkt';
    form_liste.DBgridEXT.Columns[1].Title.caption:='Aktion';
    form_liste.DBgridEXT.Columns[2].Title.caption:='User';
    //form_liste.DBgridEXT.Options:=form_liste.DBgridEXT.Options + [dgMultiSelect]	 ;
     form_liste.DBgridEXT.PopupMenu:=nil;
    form_liste.toolbar.Visible:=false;
    form_liste.Panel_b.Visible:=false;
    form_liste.height:= form_main.Height-170;
    form_liste.showmodal;
   form_liste.Release;
end;

procedure TForm_main.AnzeigeKartei1Click(Sender: TObject);
begin
form_anzeige_kartei:=tform_anzeige_kartei.create(self);
//m_befunde_c, m_vorsorge_c,  m_impfung_C, m_labor_C, m_ambulanz_c, m_diagnosen_c,m_besonderheiten_c, m_dokumente_C,m_artikel_c, m_rechnungen_c, m_kontakte_c:boolean

 form_anzeige_kartei.CheckListBox.Checked[0]:= m_befunde_c ;
  form_anzeige_kartei.CheckListBox.Checked[1]:=m_vorsorge_c;
  form_anzeige_kartei.CheckListBox.Checked[2]:=m_impfung_C;
 form_anzeige_kartei.CheckListBox.Checked[3]:=m_labor_C;
 form_anzeige_kartei.CheckListBox.Checked[4]:=m_diagnosen_c;
 form_anzeige_kartei.CheckListBox.Checked[5]:=m_besonderheiten_c;
 form_anzeige_kartei.CheckListBox.Checked[6]:=m_dokumente_C;
 form_anzeige_kartei.CheckListBox.Checked[7]:=m_rechnungen_c;
 form_anzeige_kartei.CheckListBox.Checked[8]:=m_ambulanz_c;
 form_anzeige_kartei.CheckListBox.Checked[9]:=m_artikel_c;
 form_anzeige_kartei.CheckListBox.Checked[10]:=m_kontakte_c;
 form_anzeige_kartei.CheckListBox.Checked[11]:=m_gdt_c;
 form_anzeige_kartei.CheckListBox.ItemEnabled[0]:=(b.b_string_silent('proband-befunde')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[1]:=(b.b_string_silent('Proband-Bescheinigung')>=0) or (b.b_string_silent('Proband-Bescheinigung-AG')>=0) ;
 form_anzeige_kartei.CheckListBox.ItemEnabled[2]:=(b.b_string_silent('proband-impfung')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[3]:=(b.b_string_silent('proband-labor')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[4]:=(b.b_string_silent('proband-diagnosen')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[5]:=(b.b_string_silent('proband-besonderheiten')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[6]:=(b.b_string_silent('proband-dokumente-arzt')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[7]:=(b.b_string_silent('proband-rechnung')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[8]:=(b.b_string_silent('proband-ambulanz')>=0) or (b.b_string_silent('Proband-Arbeitsunfall')>=0) ;
 form_anzeige_kartei.CheckListBox.ItemEnabled[9]:=(b.b_string_silent('proband-artikel')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[10]:=(b.b_string_silent('proband-dokumente-arzt')>=0);
 form_anzeige_kartei.CheckListBox.ItemEnabled[11]:=(b.b_string_silent('proband-dokumente-arzt')>=0);

if form_anzeige_kartei.showmodal=mrok then
begin

  m_befunde_c :=form_anzeige_kartei.CheckListBox.Checked[0] ;
  m_vorsorge_c:=form_anzeige_kartei.CheckListBox.Checked[1];
 m_impfung_C:=form_anzeige_kartei.CheckListBox.Checked[2];
 m_labor_C:=form_anzeige_kartei.CheckListBox.Checked[3];
 m_diagnosen_c:=form_anzeige_kartei.CheckListBox.Checked[4];
 m_besonderheiten_c:=form_anzeige_kartei.CheckListBox.Checked[5];
 m_dokumente_C:=form_anzeige_kartei.CheckListBox.Checked[6];
 m_rechnungen_c:=form_anzeige_kartei.CheckListBox.Checked[7];
 m_ambulanz_c:=form_anzeige_kartei.CheckListBox.Checked[8];
 m_artikel_c:=form_anzeige_kartei.CheckListBox.Checked[9];
 m_kontakte_c:=form_anzeige_kartei.CheckListBox.Checked[10];
 m_gdt_c:=form_anzeige_kartei.CheckListBox.Checked[11];

end;
form_anzeige_kartei.release;
tree_befunde_filtern(false);
dbtreeview.Expand_e1;
end;




procedure TForm_main.SpeedButton_ag_refreshClick(Sender: TObject);
begin
  aktualisiere_wiedervorlage;
end;




procedure TForm_main.aktualisiere_wiedervorlage;

var
i_firma, user,i,f_nummer_akt, i_user:integer;
aktuell_txt,firmen, dat_von, dat_bis, sql_query,knotentext,a_nummer_akt,s,userabfrage,query:string;
node1,node2, node3, pnode:ttreenode;
daten:pnodepointer;
aktuell_nr:int64;
begin
with datamodul do
begin
node1:= treeview_agenda.Selected;
if node1<>nil then
begin
	aktuell_txt:=node1.Text;
	daten:=node1.data;
   aktuell_nr:=daten.nummer;
end;
liste_leeren(treeview_agenda);


if (not checkbox_ag_all.Checked) then firmen := firmen_filter else firmen:=datamodul.q_firma.findfield('nummer').asstring;
case combobox_ag_von.ItemIndex of
0:
	dat_von:=dat_delphi_to_sql(datetostr(date()));

1: dat_von:=dat_delphi_to_sql(datetostr(date()-1));
2: dat_von:=dat_delphi_to_sql(datetostr(date()-7));
3: dat_von:=dat_delphi_to_sql(datetostr(date()-31));
4: dat_von:=dat_delphi_to_sql(datetostr(date()-365));
5: dat_von:='1910-01-01';
end;

if (not checkbox_ag_all.Checked ) then firmen := firmen_filter else firmen:=datamodul.q_firma.findfield('nummer').asstring;
case combobox_ag_bis.ItemIndex of
0:
	begin
      dat_bis:=dat_delphi_to_sql(datetostr(date()));
	end;
1:dat_bis:=dat_delphi_to_sql(datetostr(date()+1));
2:dat_bis:=dat_delphi_to_sql(datetostr(date()+7));
3:dat_bis:=dat_delphi_to_sql(datetostr(date()+31));
4:dat_bis:=dat_delphi_to_sql(datetostr(date()+365));
5:dat_bis:='9999-12-31';
end;

s:=combobox_ag_user.text;


query:=format('select nummer from untersucher where untersucher="%s"',[s]);
//i_user:=strtoint(mysql_d.feldinhalt(query,0));
s:=mysql_d.feldinhalt(query,0);
if s<>'' then i_user:=strtoint(s) else i_user:=0;

//geplant     vorsorge Impfung Labor

//bescheinigung
if ((b.b_string_silent('Proband-Bescheinigung')>-1) and wdv_besch {or (b.b_string_silent('Proband-Bescheinigung-AG')>-1)} ) then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  pnode:=treeview_agenda.Items.AddObject(nil,'Bescheinigung',daten);

  //geplant
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'geplant',daten);

  //if i_user>0 then userabfrage:=format('and (untersuchung.i_user=%d) ',[i_user]) else userabfrage:='';
  userabfrage:='';

          sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ,untersuchung.nummer as u_nr, untersuchung.datum as Vo_Datum ';
        sql_query:=sql_query+', typ.untersuchung as Vo_Typ from mitarbeiter  left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
        sql_query:=sql_query+format(' (( untersuchung.datum >= "%s" ) and ( untersuchung.datum <= "%s")) and (mitarbeiter.i_firma in (%s)) and  ( untersuchung.i_status=1 and untersuchung.storno=0) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, name   ',[dat_von,dat_bis,firmen, userabfrage]);
          //if form_einfache_abfragen.CheckBox_ausgeschieden.Checked then sql_query:=sql_query+' and mitarbeiter.ende="2999-12-31" ';
          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=1;
              daten^.liste:=nil;
              daten^.bereich:=getbigint(q_12, 'u_nr');
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.mysqlquery:=q_untersuchung;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('vo_typ').asstring,15,' ')+ str_laenge(q_12.findfield('vo_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
  //
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'offen',daten);

  if i_user>0 then userabfrage:=format('and (untersuchung.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  //userabfrage:='';

  sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ,untersuchung.nummer as u_nr, untersuchung.datum as Vo_Datum ,';
        sql_query:=sql_query+' typ.untersuchung as Vo_Typ from mitarbeiter  left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
        sql_query:=sql_query+format(' ( untersuchung.datum >= "%s" ) and ( untersuchung.datum <= "%s") and (mitarbeiter.i_firma in (%s)) and  ( untersuchung.i_status=3 and untersuchung.storno=0) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, name  ',[dat_von,dat_bis,firmen, userabfrage]);
          //if form_einfache_abfragen.CheckBox_ausgeschieden.Checked then sql_query:=sql_query+' and mitarbeiter.ende="2999-12-31" ';
          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.liste:=nil;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=1;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'u_nr');
              daten^.mysqlquery:=q_untersuchung;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('vo_typ').asstring,15,' ')+ str_laenge(q_12.findfield('vo_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

  //abgeschlossen
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'abgeschlossen',daten);

  if i_user>0 then userabfrage:=format('and (untersuchung.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  //userabfrage:='';

        sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ,untersuchung.nummer as u_nr, untersuchung.datum as Vo_Datum ';
        sql_query:=sql_query+', typ.untersuchung as Vo_Typ from mitarbeiter  left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
        sql_query:=sql_query+format(' (( untersuchung.datum >= "%s" ) and ( untersuchung.datum <= "%s")) and (mitarbeiter.i_firma in (%s)) and  ( untersuchung.i_status=4 and untersuchung.storno=0) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, name   ',[dat_von,dat_bis,firmen, userabfrage]);
          //if form_einfache_abfragen.CheckBox_ausgeschieden.Checked then sql_query:=sql_query+' and mitarbeiter.ende="2999-12-31" ';
          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.liste:=nil;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=1;
              daten^.bereich:=getbigint(q_12, 'u_nr');
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.mysqlquery:=q_untersuchung;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('vo_typ').asstring,15,' ')+ str_laenge(q_12.findfield('vo_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
end;
  //

/////////// impfung
if ((b.b_string_silent('Proband-Impfung')>-1) and wdv_impfu)  then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  pnode:=treeview_agenda.Items.AddObject(nil,'Impfung',daten);
  //geplant
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'geplant',daten);

  //if i_user>0 then userabfrage:=format('and (impfung.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  userabfrage:='';

           sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name ,mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ,impfung.nummer as i_nr,  ';
           sql_query:=sql_query+' impfung.datum as I_Datum , typ.untersuchung as I_Typ from mitarbeiter left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer)';
        sql_query:=sql_query+ format('   left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) where ( ( impfung.datum >= "%s" ) and ( impfung.datum <= "%s") ) ',[dat_von,dat_bis]) ;
        sql_query:=sql_query+format(' and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( impfung.i_status=1 and (impfung.storno=0 )  ) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-" ) order by f_name, name ',[firmen,userabfrage]);


          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=2;
              daten^.bereich:=getbigint(q_12, 'i_nr');
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.mysqlquery:=q_impfung;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('i_typ').asstring,15,' ')+ str_laenge(q_12.findfield('i_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
  //offen
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'offen',daten);

  if i_user>0 then userabfrage:=format('and (impfung.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  //userabfrage:='';

        sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name ,mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , impfung.nummer as i_nr,  ';
           sql_query:=sql_query+' impfung.datum as I_Datum , typ.untersuchung as I_Typ from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) ';
        sql_query:=sql_query+format('where ( ( impfung.datum >= "%s" ) and ( impfung.datum <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( impfung.i_status=3 and (impfung.storno=0 ) ) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-" ) order by f_name, name ',[dat_von,dat_bis,firmen, userabfrage]);


          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=2;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'i_nr');
              daten^.mysqlquery:=q_impfung;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('i_typ').asstring,15,' ')+ str_laenge(q_12.findfield('i_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
  //abgeschlossen
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'abgeschlossen',daten);

  if i_user>0 then userabfrage:=format('and (impfung.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  //userabfrage:='';

        sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name ,mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , impfung.nummer as i_nr,  ';
           sql_query:=sql_query+' impfung.datum as I_Datum , typ.untersuchung as I_Typ from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) ';
        sql_query:=sql_query+format('where ( ( impfung.datum >= "%s" ) and ( impfung.datum <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( impfung.i_status=4 and (impfung.storno=0 ) ) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-" ) order by f_name, name ',[dat_von,dat_bis,firmen, userabfrage]);


          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=2;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'i_nr');
              daten^.mysqlquery:=q_impfung;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('i_typ').asstring,15,' ')+ str_laenge(q_12.findfield('i_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
end;

///////labor
if ((b.b_string_silent('Proband-Labor')>-1) and wdv_labor)  then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  pnode:=treeview_agenda.Items.AddObject(nil,'Labor',daten);
  //geplant
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'geplant',daten);

  //if i_user>0 then userabfrage:=format('and (labor.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  userabfrage:='';
         sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name ,mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , labor.nummer as l_nr,  ';
           sql_query:=sql_query+' labor.datum as Lab_Datum , typ.untersuchung as Lab_Typ from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (labor.i_typ = typ.nummer)  ';
        sql_query:=sql_query+format('   where ( ( labor.datum >= "%s" ) and ( labor.datum <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( labor.i_status=1 and (labor.storno=0 )  ) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, name ',[dat_von,dat_bis,firmen, userabfrage]);
           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=3;
              daten^.bereich:=getbigint(q_12, 'l_nr');
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.mysqlquery:=q_labor;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('lab_typ').asstring,15,' ')+ str_laenge(q_12.findfield('lab_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
  //offen
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'offen',daten);

  if i_user>0 then userabfrage:=format('and (labor.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  //userabfrage:='';
           sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , labor.nummer as l_nr,  ';
           sql_query:=sql_query+' labor.datum as Lab_Datum , typ.untersuchung as Lab_Typ from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (labor.i_typ = typ.nummer)  ';
        sql_query:=sql_query+format('   where ( ( labor.datum >= "%s" ) and ( labor.datum <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( labor.i_status=3 and (labor.storno=0 )  ) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, name ',[dat_von,dat_bis,firmen, userabfrage]);
           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=3;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'l_nr');
              daten^.mysqlquery:=q_labor;
              daten^.liste:=nil;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('lab_typ').asstring,15,' ')+ str_laenge(q_12.findfield('lab_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

  //abgeschlossen
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  daten^.liste:=nil;
  node1:=treeview_agenda.Items.AddchildObject(pnode,'abgeschlossen',daten);

  if i_user>0 then userabfrage:=format('and (labor.i_untersucher=%d) ',[i_user]) else userabfrage:='';
  //userabfrage:='';
           sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , labor.nummer as l_nr,  ';
           sql_query:=sql_query+' labor.datum as Lab_Datum , typ.untersuchung as Lab_Typ from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (labor.i_typ = typ.nummer)  ';
        sql_query:=sql_query+format('   where ( ( labor.datum >= "%s" ) and ( labor.datum <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( labor.i_status=4 and (labor.storno=0 )  ) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, name ',[dat_von,dat_bis,firmen, userabfrage]);
           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 daten^.liste:=nil;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=3;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'l_nr');
              daten^.mysqlquery:=q_labor;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('lab_typ').asstring,15,' ')+ str_laenge(q_12.findfield('lab_datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
end;
 // ambulanz
 // arbeitssicherheit nur ARbeitsunf�lle, Ambulanz arbeitsunf�lle und ambulanzf�lle

if (((b.b_string_silent('Proband-ambulanz')>-1) or (b.b_string_silent('Proband-Arbeitsunfall')>-1) ) and wdv_ambul) then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  pnode:=treeview_agenda.Items.AddObject(nil,'Ambulanz',daten);

  //Arbeitsunf�lle
  if ((b.b_string_silent('Proband-ambulanz')>-1) or (b.b_string_silent('Proband-Arbeitsunfall')>-1) ) then
  begin
    new(daten); daten^.liste:=nil;
    daten^.ebene:=0;
    daten^.rubrik:=0;
    daten^.nummer:=0;
    node1:=treeview_agenda.Items.AddchildObject(pnode,'Arbeitsunf�lle',daten);
    userabfrage:='';
            sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ,ambulanz.nummer as a_nr, ambulanz.datum as a_Datum ';
            sql_query:=sql_query+'  from ambulanz left join mitarbeiter on (mitarbeiter.nummer= ambulanz.i_mitarbeiter)  left  join firma on (mitarbeiter.i_firma = firma.nummer)  where ';
            sql_query:=sql_query+format(' (( ambulanz.datum >= "%s" ) and ( ambulanz.datum <= "%s")) and (mitarbeiter.i_firma in (%s)) and  (ambulanz.b_arbeitsunfall=1 and ambulanz.storno=0) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, a_datum, name   ',[dat_von,dat_bis,firmen, userabfrage]);
            datamodul.sql_new(false, datamodul.q_12,sql_query,'');
            datamodul.q_12.first;
            f_nummer_akt:=-1;
            while not datamodul.q_12.Eof do
            begin
               if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
                begin
                   f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                   new(daten); daten^.liste:=nil;
                   daten^.ebene:=1;
                   daten^.rubrik:=0;
                   daten^.nummer:=getbigint( q_12,'nr');
                   daten^.mysqlquery:=q_ambulanz;
                   knotentext:=q_12.findfield('f_name').asstring;
                   node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
                end;
               new(daten); daten^.liste:=nil;
                daten^.ebene:=2;
                daten^.rubrik:=16;
                daten^.bereich:=getbigint(q_12, 'a_nr');
                daten^.nummer:=getbigint( q_12,'nr');
                daten^.mysqlquery:=q_untersuchung;
                knotentext:=str_laenge(q_12.findfield('a_datum').asstring,12,':  ')+str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,' ');
                node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
               datamodul.q_12.next;
            end;
  end;
  //Ambulanzf�lle
  if (b.b_string_silent('Proband-ambulanz')>-1)  then
  begin
    new(daten); daten^.liste:=nil;
    daten^.ebene:=0;
    daten^.rubrik:=0;
    daten^.nummer:=0;
    node1:=treeview_agenda.Items.AddchildObject(pnode,'Ambulanz-F�lle',daten);
    userabfrage:='';
            sql_query:='select firma.nummer as f_nummer, firma.firma as F_Name , mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ,ambulanz.nummer as a_nr, ambulanz.datum as a_Datum ';
            sql_query:=sql_query+'  from ambulanz left join mitarbeiter on (mitarbeiter.nummer= ambulanz.i_mitarbeiter)  left  join firma on (mitarbeiter.i_firma = firma.nummer)  where ';
            sql_query:=sql_query+format(' (( ambulanz.datum >= "%s" ) and ( ambulanz.datum <= "%s")) and (mitarbeiter.i_firma in (%s)) and  (ambulanz.b_arbeitsunfall=0 and ambulanz.storno=0) %s and (mitarbeiter.storno=0) and (mitarbeiter.archiv="-") order by f_name, a_datum, name   ',[dat_von,dat_bis,firmen, userabfrage]);
            datamodul.sql_new(false, datamodul.q_12,sql_query,'');
            datamodul.q_12.first;
            f_nummer_akt:=-1;
            while not datamodul.q_12.Eof do
            begin
               if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
                begin
                   f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                   new(daten); daten^.liste:=nil;
                   daten^.ebene:=1;
                   daten^.rubrik:=0;
                   daten^.nummer:=getbigint( q_12,'nr');
                   daten^.mysqlquery:=q_ambulanz;
                   knotentext:=q_12.findfield('f_name').asstring;
                   node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
                end;
               new(daten); daten^.liste:=nil;
                daten^.ebene:=2;
                daten^.rubrik:=16;
                daten^.bereich:=getbigint(q_12, 'a_nr');
                daten^.nummer:=getbigint( q_12,'nr');
                daten^.mysqlquery:=q_untersuchung;
                knotentext:= str_laenge(q_12.findfield('a_datum').asstring,12,':  ')+str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,' ');
                node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
               datamodul.q_12.next;
            end;
  end;
  end;



//Besonderheiten
if (( b.b_string_silent('Proband-Besonderheiten')>-1) and wdv_beson)  then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  pnode:=treeview_agenda.Items. AddObject(pnode,'Besonderheiten',daten);

  if i_user>0 then userabfrage:=format('and (besonderheiten.i_user=%d) ',[i_user]) else userabfrage:='';

           sql_query:='select firma.nummer as f_nummer,  firma.firma as F_Name ,  mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , ';
           sql_query:=sql_query+' besonderheiten.nummer as b_nr, besonderheiten.datum as Datum , besonderheitenliste.besonderheit as besonderheit from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join besonderheiten on (besonderheiten.i_mitarbeiter = mitarbeiter.nummer) left outer join besonderheitenliste on (besonderheiten.i_besonderheit = besonderheitenliste.nummer)  ';
        sql_query:=sql_query+format('   where ( ( besonderheiten.datum_bis >= "%s" ) and ( besonderheiten.datum_bis <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( besonderheiten.storno=0  ) %s and (mitarbeiter.storno=0) ',[dat_von,dat_bis,firmen, userabfrage]);
            sql_query:=sql_query+' and (mitarbeiter.archiv="-") order by f_name, name ';
           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
              if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=4;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'b_nr');
              daten^.mysqlquery:=q_besonderheiten;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('besonderheit').asstring,15,' ')+ str_laenge(q_12.findfield('datum').asstring,12,' ');
              node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
             datamodul.q_12.next;
          end;
end;

//dokumente Proband
if (((b.b_string_silent('Proband-Dokumente-Arzt')>-1) or(b.b_string_silent('Proband-Dokumente-Sani')>-1)) and wdv_dokpr )  then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  pnode:=treeview_agenda.Items. AddObject(pnode,'Dokumente Proband',daten);

  if i_user>0 then userabfrage:=format('and (dokumente.w_user=%d) ',[i_user]) else userabfrage:='';

           sql_query:='select firma.nummer as f_nummer,  firma.firma as F_Name ,  mitarbeiter.nummer as nr, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , ';
           sql_query:=sql_query+' dokumente.nummer as d_nr, dokumente.titel as titel, dokumente.datum as Datum , dokumente.w_dat as w_dat from mitarbeiter ';
        sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join dokumente on (dokumente.i_mitarbeiter = mitarbeiter.nummer)  ';
        sql_query:=sql_query+format('   where ( ( dokumente.w_dat >= "%s" ) and ( dokumente.w_dat <= "%s") ) and (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and  ( dokumente.storno=0  ) %s and (mitarbeiter.storno=0) ',[dat_von,dat_bis,firmen, userabfrage]);
           sql_query:=sql_query+' and (mitarbeiter.archiv="-") order by f_name, name ';
           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
              if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_dokumente;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=7;
              daten^.nummer:=getbigint( q_12,'nr');
              daten^.bereich:=getbigint(q_12, 'd_nr');
              daten^.mysqlquery:=q_dokumente;
              knotentext:=str_laenge(q_12.findfield('name').asstring,10,', ')+ str_laenge(q_12.findfield('vorname').asstring,5,'.  ')+ str_laenge(q_12.findfield('titel').asstring,15,' ')+ str_laenge(q_12.findfield('datum').asstring,12,' ');
              node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
             datamodul.q_12.next;
          end;
end;


//Firmendokumente
if ((b.b_string_silent('Firma-Dokumente')>-1) and wdv_dokfi )  then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  pnode:=treeview_agenda.Items.AddObject(nil,'Dokumente Firma',daten);
  if i_user>0 then userabfrage:=format('and (texte.w_user=%d) ',[i_user]) else userabfrage:='';

        sql_query:='select firma.nummer as f_nummer,  firma.firma as F_Name , texte.nummer as t_nr, texte.w_dat as Datum , texte.titel as titel from texte ';
        sql_query:=sql_query+' left outer join firma on (texte.i_master = firma.nummer)   ';
        sql_query:=sql_query+format('   where ( ( texte.w_dat >= "%s" ) and ( texte.w_dat <= "%s") ) and (firma.storno=0 ) and (firma.nummer in (%s)) and  ( texte.storno=0  ) %s order by f_name, titel  ',[dat_von,dat_bis,firmen, userabfrage]);

           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
              if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=5;
              daten^.nummer:=getbigint( q_12,'f_nummer');
              daten^.bereich:=getbigint(q_12, 't_nr');
              daten^.mysqlquery:=q_besonderheiten;
              knotentext:=str_laenge(q_12.findfield('titel').asstring,20,', ')+ str_laenge(q_12.findfield('datum').asstring,12,' ');
              node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
             datamodul.q_12.next;
          end;
end;


//rechnungen
if ((b.b_string_silent('Firma-Dokumente')>-1) and wdv_refi )  then
begin
  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  pnode:=treeview_agenda.Items.AddObject(nil,'Rechnungen ',daten);
  if i_user>0 then userabfrage:=format('and (rechnung.w_user=%d) ',[i_user]) else userabfrage:='';

        sql_query:='select firma.nummer as f_nummer,  firma.firma as F_Name , rechnung.nummer as r_nr, rechnung.w_dat as Datum , rechnung.renu as renu from rechnung ';
        sql_query:=sql_query+' left outer join firma on (rechnung.i_firma = firma.nummer)   ';
        sql_query:=sql_query+format('   where ( ( rechnung.w_dat >= "%s" ) and ( rechnung.w_dat <= "%s") ) and (firma.storno=0 ) and (firma.nummer in (%s)) and  ( rechnung.storno=0  ) %s order by f_name, renu  ',[dat_von,dat_bis,firmen, userabfrage]);

           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
              if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=18;
              daten^.nummer:=getbigint( q_12,'f_nummer');
              daten^.bereich:=getbigint(q_12, 'r_nr');
              daten^.mysqlquery:=q_besonderheiten;
              knotentext:=str_laenge(q_12.findfield('renu').asstring,20,', ')+ str_laenge(q_12.findfield('datum').asstring,12,' ');
              node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
             datamodul.q_12.next;
          end;
end;


//Projekte  offen
if ((b.b_string_silent('Firma-Abteilung')>-1) and  wdv_proje)  then
begin

  new(daten); daten^.liste:=nil;
  daten^.ebene:=0;
  daten^.rubrik:=0;
  daten^.nummer:=0;
  pnode:=treeview_agenda.Items.AddObject(pnode,'Projekte',daten);
          sql_query:='select firma.nummer as f_nummer,  firma.firma as F_Name ,a_projekte.nummer as p_nr, a_projekte.projektname as projektname, a_projekte.dat_erledigen as datum, a_projekte.i_ebene as i_ebene, ';
           sql_query:=sql_query+' abteilung.nummer as a_nummer, abteilung.kuerzel as kuerzel, abteilung.langtext as langtext   from  a_projekte left outer join a_platz on (a_projekte.i_nummer = a_platz.nummer)  ';
        sql_query:=sql_query+'left outer join abteilung on (a_platz.i_abteilung = abteilung.nummer)  left outer join firma on (abteilung.i_firma = firma.nummer)  where (firma.storno=0 )  ';
        sql_query:=sql_query+format('   and (firma.nummer in (%s)) and ( ( a_projekte.dat_erledigen >= "%s" ) and ( a_projekte.dat_erledigen <= "%s") ) and ( a_projekte.storno=0  ) order by f_name, langtext, projektname  ',[firmen,dat_von,dat_bis]);

           datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
              if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'nr');
                 daten^.mysqlquery:=q_besonderheiten;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if q_12.FindField('a_nummer').asstring<>a_nummer_akt then
              begin
                 a_nummer_akt:=q_12.FindField('a_nummer').asstring;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=getbigint( q_12,'i_ebene');
                 daten^.rubrik:=0;
                 daten^.nummer:=getbigint( q_12,'p_nr');
                 daten^.mysqlquery:=q_abteilung;
                 knotentext:=q_12.findfield('kuerzel').asstring+' | '+q_12.findfield('langtext').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;


              new(daten); daten^.liste:=nil;
              daten^.rubrik:=6;
              daten^.ebene:=getbigint( q_12,'i_ebene');
              daten^.bereich:=getbigint( q_12,'f_nummer');
              daten^.nummer:=getbigint( q_12,'p_nr');
              daten^.mysqlquery:=q_besonderheiten;
              knotentext:=str_laenge(q_12.findfield('projektname').asstring,25,', ')+ str_laenge(q_12.findfield('datum').asstring,12,' ');
              node3:=treeview_agenda.Items.AddchildObject(node2 ,knotentext,daten);
             datamodul.q_12.next;
          end;

        for i:=0 to treeview_agenda.Items.Count-1 do
        begin
           node1:= treeview_agenda.Items[i] ;
            daten:=node1.data;

           if (aktuell_txt=node1.Text) and (aktuell_nr=daten.nummer) then
           begin
              treeview_agenda.Selected:=node1;
              break;
           end;
        end;

  end;
end;
end;



procedure TForm_main.SpeedButton_wv_freitextClick(Sender: TObject);
begin
 screen.Cursor:=crhourglass;
 aktualisiere_freitextsuche;
 screen.Cursor:=crdefault;
end;

//##
procedure TForm_main.aktualisiere_freitextsuche;

var
i_firma, user,i,f_nummer_akt, i_user:integer;
m_nummer_akt:int64;
aktuell_txt,firmen, dat_von, dat_bis, sql_query,knotentext,a_nummer_akt,s,userabfrage,query,suchmodus:string;
node1,node2, node3,node4, pnode:ttreenode;
daten:pnodepointer;
aktuell_nr:int64;
begin
with datamodul do
begin
node1:= treeview_agenda.Selected;
if node1<>nil then
begin
	aktuell_txt:=node1.Text;
	daten:=node1.data;
   aktuell_nr:=daten.nummer;
end;
liste_leeren(treeview_agenda);


if checkbox_wv_freitext.Checked then firmen := firmen_filter else firmen:=datamodul.q_firma.findfield('nummer').asstring;
{case combobox_ag_von.ItemIndex of
0:
dat_von:=dat_delphi_to_sql(datetostr(date()));

1: dat_von:=dat_delphi_to_sql(datetostr(date()-1));
2: dat_von:=dat_delphi_to_sql(datetostr(date()-7));
3: dat_von:=dat_delphi_to_sql(datetostr(date()-31));
4: dat_von:=dat_delphi_to_sql(datetostr(date()-365));
5: dat_von:='1910-01-01';
end;

case combobox_ag_bis.ItemIndex of
0:
	begin
      dat_bis:=dat_delphi_to_sql(datetostr(date()));
	end;
1:dat_bis:=dat_delphi_to_sql(datetostr(date()+1));
2:dat_bis:=dat_delphi_to_sql(datetostr(date()+7));
3:dat_bis:=dat_delphi_to_sql(datetostr(date()+31));
4:dat_bis:=dat_delphi_to_sql(datetostr(date()+365));
5:dat_bis:='9999-12-31';
end; }

{
firma
firma_anpsrech
dokumente
firma_sollist
akt_untersuchung
bescheinigung
impfung
labor
diagnosen
besonderheiten
texte
kontakte
anamnese

es fehlen noch
abteilung, arbeitsplatz, memo Proband


}

s:=edit_wv_suchen.Text;

if CheckBox_suchen_wortanfang.checked then s:=s+'*';

suchmodus:='in boolean mode';

{case radiogroup_suchmodus.ItemIndex of
0:  suchmodus:='in boolean mode';//'IN natural language mode';
1:  suchmodus:='WITH QUERY EXPANSION';
end;}
//firma



         sql_query:=format(' SELECT *  FROM firma WHERE MATCH(firma,strasse, plz,ort, memo) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by firma',[s,suchmodus,firmen]);
         datamodul.sql_new(false, datamodul.q_12,sql_query,'');
         datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
             new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Firma',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin

                 //f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=q_12.findfield('nummer').asinteger;
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('firma').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);


             datamodul.q_12.next;
          end;
//firma_ansprech


         sql_query:='  SELECT  firma_ansprech.*, firma.firma as f_name  FROM firma_ansprech left outer join firma on (firma_ansprech.i_firma = firma.nummer) WHERE MATCH(funktion,name,firma_ansprech.memo) ';
         sql_query:=sql_query+  format(' AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name',[s,suchmodus,firmen]);
         datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
           new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Firma Ansprechpartner',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('i_firma').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('i_firma').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'i_firma');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=9;
              daten^.bereich:=getbigint(q_12, 'nummer');
              daten^.nummer:=getbigint( q_12,'i_firma');
              daten^.mysqlquery:=q_firma_ansprech;
              knotentext:=q_12.findfield('name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;


//texte

         sql_query:=format('  SELECT texte.*, firma.firma as f_name  FROM texte left outer join firma on (texte.i_master = firma.nummer) WHERE MATCH(texte.titel,texte.memo) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Firmen-Dokumente',daten);

          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('i_master').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('i_master').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'i_master');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=5;
              daten^.bereich:=getbigint(q_12, 'nummer');
              daten^.nummer:=getbigint( q_12,'i_master');
              daten^.mysqlquery:=q_texte;
              knotentext:=q_12.findfield('titel').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

// firma_sollist

         sql_query:=format('  SELECT firma_sollist.*, firma.firma as f_name  FROM firma_sollist left outer join firma on (firma_sollist.i_firma = firma.nummer) WHERE MATCH(anlass,firma_sollist.memo,memo2) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name',[s,suchmodus,firmen]);
         datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Zeiterfassung',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('i_firma').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('i_firma').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'i_firma');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node2:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=10;
              daten^.bereich:=getbigint(q_12, 'nummer');
              daten^.nummer:=getbigint( q_12,'i_firma');
              daten^.mysqlquery:=q_firma_sollist;
              knotentext:=q_12.findfield('datum').asstring +'  '+ copy(q_12.findfield('anlass').asstring,1,20) ;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

//akt_unterschung




  //if i_user>0 then userabfrage:=format('and (untersuchung.i_user=%d) ',[i_user]) else userabfrage:='';

           sql_query:='  SELECT akt_untersuchung.nummer as akt_u_nummer,bereich.name as b_name,akt_untersuchung.i_bereich as b_nummer, akt_untersuchung.i_untersuchung as unt_nummer, befunde.i_mitarbeiter as ma_nummer, befunde.datum as b_datum,  ' ;
           sql_query:=sql_query+' mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname, firma.firma as f_name , firma.nummer as f_nummer FROM akt_untersuchung  left outer join bereich on (akt_untersuchung.i_bereich = bereich.nummer)' ;
           sql_query:=sql_query+'   left outer join befunde on (akt_untersuchung.i_untersuchung = befunde.nummer)' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (befunde.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(akt_untersuchung.memo) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s, suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Befunde',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'ma_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'ma_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'ma_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=getbigint(q_12, 'unt_nummer');//2;
              daten^.rubrik:=12;
              daten^.bereich:=getbigint(q_12, 'b_nummer');
              daten^.nummer:=getbigint( q_12,'ma_nummer');
              daten^.mysqlquery:=q_akt_untersuchung;
              knotentext:=str_laenge(q_12.findfield('b_datum').asstring,10,',  ')+ q_12.findfield('b_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

//bescheinigung    , unmtersuchung


           sql_query:='  SELECT untersuchung.nummer as besch_nummer, untersuchung.datum as besch_datum, untersuchung.i_mitarbeiter as m_nummer ,typ.untersuchung as besch_name,mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname,  ' ;
           sql_query:=sql_query+' firma.firma as f_name , firma.nummer as f_nummer FROM untersuchung   left outer join typ on (untersuchung.i_typ = typ.nummer)' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (untersuchung.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(untersuchung.memo,untersuchung.memo_proband) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Bescheinigung',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=13;
              daten^.bereich:=getbigint(q_12, 'besch_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_untersuchung;
              knotentext:=str_laenge(q_12.findfield('besch_datum').asstring,10,',  ')+ q_12.findfield('besch_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

//impfung
        sql_query:='  SELECT impfung.nummer as impf_nummer, impfung.datum as impf_datum ,typ.untersuchung as impf_name,impfung.i_mitarbeiter as m_nummer, mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname, firma.firma as f_name ,  ' ;
           sql_query:=sql_query+' firma.nummer as f_nummer FROM impfung   left outer join typ on (impfung.i_typ = typ.nummer)' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (impfung.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(impfung.memo) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);



          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Impfung',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=2;
              daten^.bereich:=getbigint(q_12, 'impf_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_impfung;
              knotentext:=str_laenge(q_12.findfield('impf_datum').asstring,10,',  ')+ q_12.findfield('impf_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;


//labor
           sql_query:='  SELECT labor.nummer as lab_nummer, labor.datum as lab_datum , labor.i_mitarbeiter as m_nummer, typ.untersuchung as lab_name,mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname, firma.firma as f_name , firma.nummer as f_nummer FROM labor  ' ;
           sql_query:=sql_query+'   left outer join typ on (labor.i_typ = typ.nummer)' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (labor.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(labor.memo,testbezogenerhinweis,norm,ergebnistext) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);



          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Labor',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=3;
              daten^.bereich:=getbigint(q_12, 'lab_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_labor;
              knotentext:=str_laenge(q_12.findfield('lab_datum').asstring,10,',  ')+ q_12.findfield('lab_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

//diagnosen
        sql_query:='  SELECT diagnosen.nummer as d_nummer, diagnosen.datum as d_datum ,diagnosen.i_mitarbeiter as m_nummer, icd_main.icd_schluessel as d_schluessel,icd_main.icd_text as d_name,mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname,   ' ;
           sql_query:=sql_query+' firma.firma as f_name , firma.nummer as f_nummer FROM diagnosen  left outer join icd_main on (diagnosen.i_icd = icd_main.nummer)' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (diagnosen.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(diagnosen.icd,diagnosen.icd_text,diagnosen.memo) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Diagnosen',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=14;
              daten^.bereich:=getbigint(q_12, 'd_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_diagnosen;
              knotentext:= q_12.findfield('d_schluessel').asstring+' , ' + q_12.findfield('d_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

//besonderheiten
    sql_query:='  SELECT besonderheiten.nummer as b_nummer, besonderheiten.datum as b_datum ,besonderheitenliste.besonderheit as b_name, besonderheiten.i_mitarbeiter as m_nummer, mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname,   ' ;
           sql_query:=sql_query+' firma.firma as f_name , firma.nummer as f_nummer FROM besonderheiten  left outer join besonderheitenliste on (besonderheiten.i_besonderheit = besonderheitenliste.nummer)' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (besonderheiten.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(besonderheiten.memo) AGAINST("%s" %s )and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Besonderheiten',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=4;
              daten^.bereich:=getbigint(q_12, 'b_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_besonderheiten;
              knotentext:= q_12.findfield('b_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

//texte
          sql_query:='  SELECT dokumente.nummer as dok_nummer, dokumente.datum as dok_datum, dokumente.titel as dok_name ,dokumente.i_mitarbeiter as m_nummer, mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname,   ' ;
           sql_query:=sql_query+' firma.firma as f_name , firma.nummer as f_nummer FROM dokumente  ' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (dokumente.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(dokumente.titel,dokumente.memo) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Dokumente_Proband',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=1;
              daten^.bereich:=getbigint(q_12, 'dok_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_dokumente;
              knotentext:= q_12.findfield('dok_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;



//kontakte
     sql_query:='  SELECT kontakte.nummer as k_nummer, kontakte.name as k_name ,kontakte.i_mitarbeiter as m_nummer,mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname,   ' ;
           sql_query:=sql_query+' firma.firma as f_name , firma.nummer as f_nummer FROM kontakte  ' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (kontakte.i_mitarbeiter = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format('  WHERE MATCH(kontakte.memo,kontakte.name,kontakte.vorname,kontakte.strasse,kontakte.plz,kontakte.ort,kontakte.fachgebiet) AGAINST("%s" %s ) and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Kontakte',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'m_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=15;
              daten^.bereich:=getbigint(q_12, 'k_nummer');
              daten^.nummer:=getbigint( q_12,'m_nummer');
              daten^.mysqlquery:=q_kontakte;
              knotentext:= q_12.findfield('k_name').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;
      // anamnese
      sql_query:='  SELECT anamnese.nummer as a_nummer,  mitarbeiter.name as m_name,mitarbeiter.vorname as m_vname,   ' ;
           sql_query:=sql_query+' firma.firma as f_name , firma.nummer as f_nummer FROM anamnese  ' ;
           sql_query:=sql_query+'   left outer join mitarbeiter on (anamnese.nummer = mitarbeiter.nummer)' ;
           sql_query:=sql_query+'   left outer join firma on (mitarbeiter.i_firma=firma.nummer)';
          sql_query:=sql_query+format(' WHERE MATCH(anamnese.memo) AGAINST("%s" %s )and (firma.nummer in (%s)) order by f_name,m_name ',[s,suchmodus,firmen]);

          datamodul.sql_new(false, datamodul.q_12,sql_query,'');
          datamodul.q_12.first;
          if not datamodul.q_12.Eof then
          begin
            new(daten); daten^.liste:=nil;
            daten^.ebene:=0;
            daten^.rubrik:=0;
            daten^.nummer:=0;
            pnode:=treeview_agenda.Items.AddObject(nil,'Anamnese',daten);
          end;
          datamodul.q_12.first;
          f_nummer_akt:=-1;
          m_nummer_akt:=-1;
          while not datamodul.q_12.Eof do
          begin
             if q_12.FindField('f_nummer').asinteger<>f_nummer_akt then
              begin
                 f_nummer_akt:=q_12.FindField('f_nummer').asinteger;
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=8;
                 daten^.nummer:=getbigint( q_12,'f_nummer');
                 daten^.mysqlquery:=q_firma;
                 knotentext:=q_12.findfield('f_name').asstring;
                 node1:=treeview_agenda.Items.AddchildObject(pnode ,knotentext,daten);
              end;
              if getbigint(q_12,'m_nummer')<>m_nummer_akt then
              begin
                 m_nummer_akt:=getbigint(q_12,'m_nummer');
                 new(daten); daten^.liste:=nil;
                 daten^.ebene:=1;
                 daten^.rubrik:=11;
                 daten^.nummer:=getbigint( q_12,'a_nummer');
                 daten^.mysqlquery:=q_mitarbeiter;
                 knotentext:=q_12.findfield('m_name').asstring +' '+q_12.findfield('m_vname').asstring ;
                 node2:=treeview_agenda.Items.AddchildObject(node1 ,knotentext,daten);
              end;
             new(daten); daten^.liste:=nil;
              daten^.ebene:=2;
              daten^.rubrik:=17;
              daten^.bereich:=getbigint(q_12, 'a_nummer');
              daten^.nummer:=getbigint( q_12,'a_nummer');
              daten^.mysqlquery:=q_besonderheiten;
              knotentext:= 'Anamnese';// vom: '+q_12.findfield('a_datum').asstring;
              node3:=treeview_agenda.Items.AddchildObject(node2,knotentext,daten);
             datamodul.q_12.next;
          end;

     // ende anamnese

  end;
end;
//##

procedure liste_leeren(liste:ttreeview);
var
daten: pnodepointer;
begin
	while liste.Items.Count>0 do
	begin
	  daten:=liste.items[liste.items.count-1].data;
	  dispose(daten);
	  liste.Items.delete(liste.items[liste.Items.count-1]);
	end;
end;


function str_laenge (str: string; laenge: integer; hinten: string): string;
begin
   str:=trim (str);
   str:=str+'                                                ';
   str:=LeftStr(str, laenge);
   result:=str+hinten;
end;



{if tdbedit(sender).text='01.01.1900' then
	begin
   	tdbedit(sender).visible:=false;
      label261.Visible:=false;
      DBCombo_num_wdv_dokumente.Visible:=false;
   end
   else
   begin
   	tdbedit(sender).visible:=true;
      label261.Visible:=true;
      DBCombo_num_wdv_dokumente.Visible:=true;
   end;}


Procedure tform_main.kartei_aktualisieren;
begin
  dbtreeview.aktualisieren;
end;

Procedure tform_main.zeigedaten;
var
	i_nummer:int64;
  rubrik,ebene,bereich:integer;
  daten:pnodepointer;

begin
 		daten:=dbtreeview.Selected.Data;
      i_nummer:=daten^.nummer;
      rubrik:=daten^.rubrik;
      ebene:=daten^.ebene;
      bereich:=daten^.bereich;
      showmessage('ebene:'+inttostr(ebene)+' rubrik:'+inttostr(rubrik)+' bereich:'+inttostr(bereich))

end;

procedure TForm_main.SpeedButton_firna_text_wdvClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_texte.edit;
		datamodul.q_texte['w_dat'] :=int(form_kalender.auswahlkalender.date);
     if datamodul.q_texte['w_user']=0 then datamodul.q_texte['w_user']:=akt_untersucher;
   end;
end;

procedure TForm_main.DBEdit_firma_text_wdvChange(Sender: TObject);
begin
if tdbedit(sender).text='01.01.1900' then
	begin
   	tdbedit(sender).visible:=false;
      label258.Visible:=false;
      DBCombo_num_texte_user.Visible:=false;
   end
   else
   begin
   	tdbedit(sender).visible:=true;
      label258.Visible:=true;
      DBCombo_num_texte_user.Visible:=true;
   end;
end;



procedure TForm_main.TreeView_agendaDblClick(Sender: TObject);
var

	i_nummer,bereich:int64;
  rubrik, ebene:integer;
  daten:pnodepointer;
  node: ttreenode;
  query,s_nummer:string;
begin
try
  dbspeichern(true);
  node:=treeview_agenda.Selected;
  daten:=node.Data;
  i_nummer:=daten^.nummer;
  bereich:=daten^.bereich;
  ebene:=daten^.ebene;
  rubrik:=daten^.rubrik;

  case rubrik of
  1:gehezuuntersuchung (1, i_nummer,bereich) ;  //untersuchun
  2:gehezuuntersuchung (2, i_nummer,bereich) ;   //impfung
  3:gehezuuntersuchung (3, i_nummer,bereich) ;  //labor
  4:gehezubesonderheit(i_nummer, bereich);
  5:gehezutext(i_nummer, bereich);
  6:gehezuprojekt(bereich,i_nummer);
  7:gehezudokument_proband(i_nummer, bereich);
  8:gehezu_firma(i_nummer);//firma
  9:gehezu_firma_ansprechpartner(i_nummer, bereich);//firma_ansprech
  10:gehezu_firma_sollist(i_nummer, bereich); //firma_sollist
  11:gehezu_stammdaten(i_nummer);//mitarbeiter
  12:gehezu_aktuntersuchung(i_nummer,ebene, bereich); //akt_untersuchung
  13:gehezuuntersuchung (1, i_nummer,bereich) ;  //untersuchun// bescheinigung untersuchung
  14:gehezu_diagnosen(i_nummer, bereich);//diagnosen
  15:gehezu_kontakte(i_nummer, bereich);//kontakte
  16:gehezu_ambulanz(i_nummer,bereich);//ambulanz
  17:gehezu_anamnese(i_nummer,bereich);
  18:gehezu_rechnung_firma(i_nummer, bereich);
  end;
except

end;

end;

procedure TForm_main.Wiedervorlage1Click(Sender: TObject);
begin
	if  panel_wiedervorlage.Width=0 then panel_wiedervorlage.Width	:=250 else  panel_wiedervorlage.width:=0;
end;

procedure TForm_main.Splitter_agendaMoved(Sender: TObject);
var
b,l:integer;
begin
   //panel_wiedervorlage
    b:= pagecontrol_wdv.Width;
    l:=combobox_ag_user.Left;  //rand li
   combobox_ag_user.Width:=b-2*l;
   combobox_ag_von.Width:=b div 2 -3*l div 2;
   combobox_ag_bis.left:=b div 2 +l div 2;
   combobox_ag_bis.Width:=b div 2 -3*l div 2;
   edit_wv_suchen.Width:= b-2*l;
   //checkbox_ag_all.Left:=l ;
   speedbutton_ag_refresh.Left:=b-l-speedbutton_ag_refresh.Width;
   speedbutton_wv_freitext.Left:=b-l- speedbutton_wv_freitext.Width;
   grid_einstellen(DBgridEXT_re_positionen);
   grid_einstellen(DBgridEXT_repo);
end;

procedure TForm_main.SpeedButton18Click(Sender: TObject);
begin
treeview_agenda.FullCollapse;
end;

procedure TForm_main.SpeedButton19Click(Sender: TObject);
begin
 treeview_agenda.Fullexpand;
end;

procedure TForm_main.DBComboBox_anredeClick(Sender: TObject);
var
s:string;
begin
try
  //s:=trim(datamodul.q_mitarbeiter.findfield('anrede').asstring);
 // DBCombo_num_geschlecht.text:= DBCombo_num_geschlecht.Items[DBComboBox_anrede.ItemIndex];
 if datamodul.q_mitarbeiter.State in [dsinsert, dsedit] then
 begin
  datamodul.q_mitarbeiter.findfield('anrede').asstring:=DBComboBox_anrede.items[DBComboBox_anrede.ItemIndex];
  datamodul.q_mitarbeiter.findfield('geschlecht').asinteger:=DBComboBox_anrede.ItemIndex+1;
 end
 else showmessage('Feld�nderung wurde nicht gespeichert');
except
  raise;
end;
end;

procedure TForm_main.DBCombo_num_geschlechtClick(Sender: TObject);
begin
{try
   DBComboBox_anrede.Text:=DBComboBox_anrede.Items[DBCombo_num_geschlecht.itemindex];
except
end;}
try
if datamodul.q_mitarbeiter.State in [dsinsert, dsedit] then
 begin
  DBComboBox_anrede.Text:=DBComboBox_anrede.Items[DBCombo_num_geschlecht.itemindex];
  datamodul.q_mitarbeiter.findfield('anrede').asstring:=DBComboBox_anrede.Items[DBCombo_num_geschlecht.itemindex];
  //datamodul.q_mitarbeiter.findfield('geschlecht').asinteger:=DBComboBox_anrede.ItemIndex+1;
 end
 else showmessage('Feld�nderung wurde nicht gespeichert');
except
  raise;
end;
end;

function sql_dat:string;
begin
  result:=sql_datetostr(now);
  result:=result+'_'+timetostr(time);
  result:=stringreplace(result,'.','_',[rfreplaceall]);
  result:=stringreplace(result,':','_',[rfreplaceall]);
  result:=stringreplace(result,' ','_',[rfreplaceall]);
  result:=stringreplace(result,'-','_',[rfreplaceall]);
  result:=stringreplace(result,'"','',[rfreplaceall]);
end;

procedure TForm_main.SpeedButton_wdv_dokumenteClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_dokumente.edit;
		datamodul.q_dokumente['w_dat'] :=int(form_kalender.auswahlkalender.date);
    if datamodul.q_dokumente['w_user']=0 then datamodul.q_dokumente['w_user']:=akt_untersucher;
   end;
end;

procedure TForm_main.DBEdit_wdv_dokumenteChange(Sender: TObject);
begin
if tdbedit(sender).text='01.01.1900' then
	begin
   	tdbedit(sender).visible:=false;
      label261.Visible:=false;
      DBCombo_num_wdv_dokumente.Visible:=false;
   end
   else
   begin
   	tdbedit(sender).visible:=true;
      label261.Visible:=true;
      DBCombo_num_wdv_dokumente.Visible:=true;
   end;
end;

procedure TForm_main.FormResize(Sender: TObject);
var
h:integer;
begin
grid_einstellen(DBgridEXT_re_positionen);
grid_einstellen(DBgridEXT_repo);

h:=form_main.Height-515;//zeiterfassung
//GroupBox_fsi_5.height:=h div 3;
GroupBox_fsi_4.Height:=h div 3;
GroupBox_fsi_3.Height:=h div 3;

h:=form_main.Height-619;// bescheinigung
GroupBox_anmerkung_arbeitgeber.Height:=h div 2;
//GroupBox_anmerkung_proband.Height:=h div 2;
//if user_name='a' then statusbar.panels[5].Text:=inttostr(form_main.Width)+' / '+inttostr(form_main.Height);


end;

procedure TForm_main.m_WeiterversorgungClick(Sender: TObject);
begin
if nicht_satellit_tab then exit;
if b.b_string('Proband-Listen')>-1 then
begin
  form_tabellen:=tform_tabellen.Create(self);
  //form_tabellen.HelpContext:=10840;
  form_tabellen.Caption:='Ambulanz,  Weitere Behandlung';
  form_tabellen.Notebook.PageIndex:=17;
  //form_tabellen.helpcontext:=49;  //2010

  datamodul.sql(false,datamodul.q_1,'select * from amb_weiter order by reihenfolge','');

  form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='behandlung';
  form_tabellen.DBGrid_tabellen.Columns[0].width:=form_tabellen.DBgrid_tabellen.width-20;
  form_tabellen.DBEdit_amb_weiter.DataField:='behandlung';
  form_tabellen.HelpContext:=34;
  form_tabellen.showmodal;

  form_tabellen.release;
  DBCombo_num_weiter.lookup_laden;
end;
end;

procedure TForm_main.m_Folgebescheinigung_karteiClick(Sender: TObject);
begin
dbspeichern(false);
formular_entsperren(true,false);
unt_planen(datamodul.q_untersuchung);

end;

procedure TForm_main.m_AbrechnungTtigkeitalphabetischClick(
  Sender: TObject);
  var
  query:string;
begin
 if b.b_string('Firmen-Listen')>-1 then
 begin
  	form_liste:=tform_liste.create(self);
   form_liste.width:=700;
   form_liste.Caption:='Abrechnung - T�tigkeiten alphabetisch';
   query:='select nummer, name, left(name,250)as name_s from aufwand where storno=0 order by name ';
    datamodul.sql_new(false,datamodul.q_7,query,'');

    form_liste.dbgridext.DataSource:=datamodul.ds_7;
    form_liste.s_name:='protokoll_user';
    form_liste.DBgridEXT.Columns[0].fieldname:='name_s';
    form_liste.DBgridEXT.Options:=form_liste.DBgridEXT.Options + [dgMultiSelect,dgindicator]	 ;


    form_liste.DBgridEXT.Columns[0].Width:=500;

    form_liste.DBgridEXT.Columns[0].Title.caption:='Aufwand';

    //form_liste.DBgridEXT.Options:=form_liste.DBgridEXT.Options + [dgMultiSelect]	 ;
     form_liste.DBgridEXT.PopupMenu:=form_liste.PopupMenu_aufwand;
     form_liste.OnDblClick:=nil;
     form_liste.s_name:='name_s';
    form_liste.toolbar.Visible:=false;
    form_liste.Panel_b.Visible:=false;
    form_liste.HelpContext:= 89;
    form_liste.height:= form_main.Height-170;
    form_liste.showmodal;
   form_liste.Release;
end;
end;

procedure TForm_main.m_BeurteilungClick(Sender: TObject);
var
ber:integer;
begin
 if nicht_satellit_tab then exit;
 ber:=b.b_string('Untersuchungen-Listen');
if ber=-1 then exit;

speichern;
form_tabellen:=tform_tabellen.Create(self);
//form_tabellen.HelpContext:=10840;
form_tabellen_berechtigung(ber);
form_tabellen.Caption:='Beurteilung';
form_tabellen.Notebook.PageIndex:=17;
form_tabellen.helpcontext:=82;

datamodul.sql(false,datamodul.q_1,'select * from beurteilung order by nummer','');   //hier ist storno nicht m�glich

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='nummer';
form_tabellen.DBGrid_tabellen.Columns[0].width:=40;
form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='text';
form_tabellen.DBGrid_tabellen.Columns[1].width:=form_tabellen.DBgrid_tabellen.width-60;
form_tabellen.DBEdit_amb_weiter.DataField:='text';
form_tabellen.ToolButton_neu.Visible:=false;
form_tabellen.ToolButton_del.Visible:=false;
form_tabellen.Label70.Caption:='Beurteilung';
form_tabellen.SpeedButton3.Visible:=false;
form_tabellen.SpeedButton4.Visible:=false;

//form_tabellen.HelpContext:=34;

form_tabellen.showmodal;

form_tabellen.release;
showmessage('Die Ver�nderungen werden erst nach einem Neustart wirksam');

end;


procedure TForm_main.Panel_zeitabrechnung_reResize(Sender: TObject);
var s:integer;
begin
//#
s:=groupbox_fsi_3.Height+ groupbox_fsi_4.Height+groupbox_fsi_5.Height;
groupbox_fsi_3.Height:=s div 3;
groupbox_fsi_5.Height:=s div 3;


end;

procedure TForm_main.Jahresabschluss1Click(Sender: TObject);
var
ber:integer;
query,s:string;
begin
if nicht_satellit_tab then exit;
ber:=b.b_string('System');
if ber=-1 then exit;

speichern;
form_tabellen:=tform_tabellen.Create(self);
//form_tabellen.HelpContext:=10840;
form_tabellen_berechtigung(ber);
form_tabellen.Caption:='Buchungsperioden';
form_tabellen.Notebook.PageIndex:=18;
form_tabellen.helpcontext:=90; //2010
form_tabellen.ToolButton_edit.Visible :=false;
//form_tabellen.ToolButton_del.visible:=false;


datamodul.sql(false,datamodul.q_1,'select * from abschluss where storno=0 order by sperrdatum','');

form_tabellen.DBGrid_tabellen.Columns[0].fieldname:='sperrDatum';
//form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='goae_text';
form_tabellen.DBGrid_tabellen.Columns[0].width:=120;

form_tabellen.DBEdit_abschluss_sperrdatum.DataField:='sperrdatum';
form_tabellen.DBEdit_abschluss_erstelldatum.DataField:='datum';
form_tabellen.DBmemo_abschluss_kommentar.DataField:='kommentar';


form_tabellen.showmodal;

query:='select max(sperrdatum) from abschluss where storno=0';
    s:= mysql_d.feldinhalt(query,0);
    if s='' then max_sperrdatum:=0 else max_sperrdatum:=strtodate(s);

form_tabellen.release;

end;


function sperrdatum_OK(d:real;modus:integer):boolean;
var
sdat:string;
begin
  result:=d>max_sperrdatum;
  if not result then
  begin
    sdat:= '(< '+datetostr(max_sperrdatum) +' )';
    if modus=1 then showmessage(format('Das Datum liegt in der gesperrten Periode %s',[sdat]));
    if modus=2 then showmessage(format('Das Datum liegt in der gesperrten Periode %s, �nderungen sind nicht m�glich',[sdat]));
  end;
end;

procedure TForm_main.m_BuchungsperiodeClick(Sender: TObject);
var
query, s:string;

begin
if nicht_satellit_tab then exit;
if b.b_string('System')=-1 then exit;

tmenuitem(sender).checked :=  not tmenuitem(sender).checked;

if tmenuitem(sender).checked then
begin
 max_sperrdatum:=0;
 protokoll('user','periode_0','',true);
end;

if not tmenuitem(sender).checked then
begin
query:='select max(sperrdatum) from abschluss where storno=0';
    s:= mysql_d.feldinhalt(query,0);
    if s='' then max_sperrdatum:=0 else max_sperrdatum:=strtodate(s);
    protokoll('user','periode_vorgabe','',true);
end;

end;

procedure TForm_main.Spenden1Click(Sender: TObject);
begin
  spenden;
end;


procedure tform_main.spenden;
var
i,j:integer;
s,s1,s2,p,query,js:string;
begin
try

   form_spenden:=tform_spenden.create(self);
   if ((form_spenden.ShowModal=mrok) and ist_hauptsystem) then
   begin

		jahresschluessel:=form_spenden.edit.text;

	  cipher.InitStr(tkey,Tdcp_sha1);
     s:=cipher.DecryptString(jahresschluessel);
     cipher.Burn;
     s1:=copy(s,2,4);
     try
      j:=strtoint(s1);
     except
      j:=0;
     end;
     s2:= copy(datetostr(date),7,4);
     if j>=strtoint(s2) then
     begin
     	//ToolButtonspenden.Visible:=false;     gespendet
        query:='select count(*) from schluessel where name ="_&_jahr" and storno=1';
        js:=mysql_d.Feldinhalt(query,0);
        if js='0' then
        begin
          query:='insert schluessel set name="_&_jahr", storno="1"';
          mysql_d.sql_exe(query);
         end;

        query:=format('update schluessel set passwort="%s" where name="_&_jahr" and storno=1',[jahresschluessel]);
        mysql_d.sql_exe(query);

       showmessage('Das Freischalten war erfolgreich!');

     end
     else
     showmessage('Der Schl�ssel ist nicht korrekt');

   end;
finally
   form_spenden.release;
end;

end;

procedure TForm_main.dropdown_richedit_FettClick(Sender: TObject);

begin
//if rtfedit.FUpdating then Exit;
	//if fsbold in richedit_befund.SelAttributes.Style then richedit_befund.SelAttributes.Style:= richedit_befund.SelAttributes.Style - [fsBold]
  //else richedit_befund.SelAttributes.Style:=richedit_befund.SelAttributes.Style + [fsBold];
   //rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsBold];

 if fsbold in rich_akt.SelAttributes.Style then rich_akt.SelAttributes.Style:= rich_akt.SelAttributes.Style - [fsBold]
  else rich_akt.SelAttributes.Style:=rich_akt.SelAttributes.Style + [fsBold];
end;

procedure TForm_main.dropdown_richedit_KursivClick(Sender: TObject);
begin
	if fsItalic in  rich_akt.SelAttributes.Style then  rich_akt.SelAttributes.Style:=  rich_akt.SelAttributes.Style - [fsItalic]
  else  rich_akt.SelAttributes.Style:= rich_akt.SelAttributes.Style + [fsItalic];

end;

procedure TForm_main.dropdown_richedit_UnterstrichenClick(Sender: TObject);
begin
if fsunderline in  rich_akt.SelAttributes.Style then  rich_akt.SelAttributes.Style:=  rich_akt.SelAttributes.Style - [fsunderline]
  else  rich_akt.SelAttributes.Style:= rich_akt.SelAttributes.Style + [fsunderline];
end;

procedure TForm_main.dropdown_richedit_zurcksetzenClick(Sender: TObject);
begin
     rich_akt.SelAttributes.Style:=[];
     rich_akt.SelAttributes.Height:=fontsize_anmerkung * Screen.PixelsPerInch div 72;
end;


procedure TForm_main.Font1Click(Sender: TObject);
begin
  rich_akt.SelAttributes.Height:= rich_akt.SelAttributes.Height+2;
end;

procedure TForm_main.Font2Click(Sender: TObject);
begin
  if rich_akt.SelAttributes.Height>5 then
    rich_akt.SelAttributes.Height:= rich_akt.SelAttributes.Height-2;
end;


procedure TForm_main.BulletClick(Sender: TObject);
begin
 if rich_akt.Paragraph.Numbering=nsnone then  rich_akt.Paragraph.Numbering:= nsBullet	else  rich_akt.Paragraph.Numbering:= nsnone;
end;

function FileSize(fn: TFileName): Int64;
var
  sr: TSearchRec;
begin
  if FindFirst(fn, faArchive, sr) = 0 then
  begin
    with sr.FindData do
      Result := Int64(nFileSizeHigh) shl 32 + nFileSizeLow;
    FindClose(sr);
  end
  else
    Result := 0;
end;

procedure TForm_main.soundexneu;
var
	wort, s_name, s_vorname:string;
  query:string;
begin
  with datamodul do
	begin
		//q_mitarbeiter_such.IndexName:='';
    query:='select *  from mitarbeiter' ;
    sql_new(false,q_9,query,'');
     q_1.First;
     while not q_9.Eof do
     begin
        if q_9['name']<>null then wort:=q_9['name'] else wort:='';
			s_name:=soundex.soundex(wort);
      if q_9['vorname']<>null then wort:=q_9['vorname'] else wort:='';
       s_vorname:=soundex.soundex(wort);


        q_9.edit;
        q_9['s_name']:=s_name;

        q_9['s_vorname']:=s_vorname;
        direktspeichern:=true;
        q_9.Post;
        q_9.Next;
        application.ProcessMessages;
     end;
    q_mitarbeiter.refresh;
   end;

end;


function ConnectNetworkDrive(const ADrive: string; const ADirectory, AUsername, APassword: string;
                             const ARestoreAtLogon: Boolean ): Boolean;
var
  NetResource: TNetResource;
  dwFlags: DWORD;
  lPwd, lUser: PChar;
  ErrCode:dword;
begin
  NetResource.dwType:=RESOURCETYPE_DISK;
  NetResource.lpLocalName:=PChar(ADrive);
  NetResource.lpRemoteName:=PChar(ADirectory);
  NetResource.lpProvider:=nil;

  if ARestoreAtLogon then dwFlags:=CONNECT_UPDATE_PROFILE
                     else dwFlags:=0;
  if AUsername<>'' then lUser:=PChar(AUsername)
                   else lUser:=nil;
  if APassword<>'' then lPwd:=PChar(APassword)
                   else lPwd:=nil;

  //Result:=WNetAddConnection2(NetResource, lPwd, lUser, dwFlags)=NO_ERROR;

  errcode := WNetAddConnection2(NetResource, lPwd, lUser, dwFlags);
result := ErrCode = NO_ERROR;

//so aufrufen
{if not ConnectNetworkDrive('T:', '\\server-ad\ad','Nutzer','Passwort',false, err) then
begin
  showmessage('Verbindungsfehler zum Netzwerk. Bitte wiederholen. Fehlercode: ' + IntToStr(err) + ' Fehlermeldung: ' + SysErrorMessage(err));
  exit;
end; }
end;



procedure TForm_main.SpeedButton_labornummerClick(Sender: TObject);
begin
//labornummer einf�gen
form_labornummer:=tform_labornummer.create(self);
form_labornummer.edit_nummer.text:=g_labornummer_nummer;
form_labornummer.Edit_vorspann.text:=g_labornummer_vorspann;
form_labornummer.UpDown1Click(self,btnext);
if form_labornummer.ShowModal =mrok then
begin
  if  dbedit_labornummer.Text<>'' then dbedit_labornummer.Text:=dbedit_labornummer.Text+', ';
  dbedit_labornummer.Text:=dbedit_labornummer.Text+form_labornummer.Edit_vorspann.text+form_labornummer.edit_nummer.text;

  g_labornummer_nummer:=form_labornummer.edit_nummer.text;
  g_labornummer_vorspann:=form_labornummer.Edit_vorspann.text;
end;
form_labornummer.Release;
end;

procedure TForm_main.m_LaborErgebnistexteClick(Sender: TObject);
begin
if b.b_string('Untersuchungen-Listen')=-1 then exit;
tbs(2,nil);//men�
end;

procedure TForm_main.SpeedButton_lab_ergebnisClick(Sender: TObject);
begin
tbs(3,nil);//men�
end;

procedure TForm_main.DBComboBox_zeitansatzExit(Sender: TObject);
begin
if DBComboBox_zeitansatz.Text='' then DBComboBox_zeitansatz.Text:='0';
end;

procedure TForm_main.DBgridEXT_repoLButtonDown(Sender: TObject);
begin
//if DBGridext_repo.SelectedRows.Count >=1 then formular_entsperren(true,false);
end;

function stringdatum(dat:tdate):string;
begin
  result:=dat_delphi_to_sql( datetostr( dat));
  result:=stringreplace(result,'.','-',[rfreplaceall]);
  result:=stringreplace(result,':','-',[rfreplaceall]);
  result:=stringreplace(result,' ','-',[rfreplaceall]);
  //result:=stringreplace(result,'-','_',[rfreplaceall]);
  result:=stringreplace(result,'"','',[rfreplaceall]);
end;

function tform_main.pdf_maker_pruefen:boolean;
var
tf:textfile;
s,pfad_ini,wtp,html_name:string;
p:integer;
begin

  Pfad_ini := GetEnvVarValue('AppData');     //CommonApplicationData CommonApplicationData CSIDL_COMMON_APPDATA
  pfad_ini:=pfad_ini+'\7-pdfmaker\7p.ini';

  wtp := pfad_pdf_converter+'7p.exe';
  if not FileExists(wtp) then
    begin
      ShowMessage('Word-to-PDF-Konverter  nicht gefunden'#13+wtp+#13+'Bitte obigen Pfad �berpr�fen bzw.'+#13+'7-PDF Maker folgenen Website herunterladen und installieren'+#13+'und unter Arbene->Optionen-> Pfade den Dateipfad f�r 7-PDF ausw�hlen');
      //html_name:='http://www.7-pdf.de/produkte/7-pdf-maker';
      html_name:='http://www.arbene.de/index.php/download/file/13-7-pdf-maker';
      shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal); //application.Handle
      exit;
    end;

  if not FileExists(pfad_ini) then
  begin
    showmessage('Bitte �ffenen Sie den 7-PDF-Maker (Alle Apps....) und t�tigen Sie dort die gew�nschten Einstellungen. ');
    exit;
  end;
  try
    //hier �ber textfile....

    AssignFile(TF,pfad_ini );   { Datei ausgew�hlt }
    Reset(TF);
    while not eof(Tf) do
    begin
      Readln(TF, S);
      if p=0 then continue; //leer
    end;
    if p=0 then
    begin
         showmessage('�ffenen Sie den 7-PDF-Maker und w�hlen Sie "PDF/a-1 erzeugen" aus.');
         result:=false;
    end
    else result:=true;

  finally
    closefile(tf);
  end;

//
end;

function word_to_pdf(w_dat, pdf_dat:string):boolean;
var
befehl,wtp,html_name,pfad_ini,s:string;
p:integer;
g,fehler:boolean;
fs:tfilestream;
t:ttime;
begin
if not fileexists(w_dat) then
begin
  showmessage(w_dat +' existiert nicht');
  exit;
end;
 Pfad_ini := GetEnvVarValue('AppData');
 pfad_ini:=pfad_ini+'\7-pdfmaker\7p.ini';


wtp := pfad_pdf_converter+'7p.exe';

if not FileExists(wtp) then
  begin
    ShowMessage('Word-to-PDF-Konverter  nicht gefunden'#13+wtp+#13+'Bitte von der folgenen Website 7-PDF Maker herunterladen und installieren'+#13+'und unter Arbene->Optionen-> Pfade den Dateipfad f�r 7-PDF ausw�hlen');
  	//html_name:='http://www.7-pdf.de/produkte/7-pdf-maker';
    html_name:='http://www.arbene.de/index.php/download/file/13-7-pdf-maker';
	  shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal);    //application.Handle
    exit;
  end;

  if not FileExists(pfad_ini) then
  begin
    showmessage('Bitte �ffenen Sie den 7-PDF-Maker und t�tigen Sie dort die gew�nschten Einstellungen. ');
    exit;
  end;

//
 t:=time();

  befehl:=format(' "%s" ',[w_dat]);

  //if runasadmin(application.Handle,wtp,befehl) then
  if run_app(wtp,befehl) then
    warte_bis_datei_lesbar(pdf_dat)
  else
  showmessage (wtp+befehl+' wurde micht ausgef�hrt');
  //executefile(wtp,befehl,'',true);
  //shellexecute_se( 0,'open',pchar(wtp),PChar(befehl),nil,0 );

  //winrun(wtp,befehl);
  try
    if not fileexists(pdf_dat) then executefile(wtp,befehl,'',true);
    warte_bis_datei_lesbar(pdf_dat);
  except
  end;
  if fileexists(pdf_dat) then result:=true else result:=false;
end;


function tform_main.dat_to_pdf(dat, pdf_dat:string):boolean;
var
befehl,wtp,html_name,endung,endungen:string;
fs: tfilestream;
g:boolean;
t:ttime;
begin
endungen:='.odm .sxg .sgl .odt .ott .sxw .stw .doc .dot .docx .docm .dotx .dotm .rtf .wpd .sdw .vor .txt .html .oth .xml .ods .ots .sxc .stc .xls .xlt .xlsx .xlsb .xlsm .xltx .xltm .sdc .vor .slk .wb2';
endungen:=endungen+' .dif .odp .otp .swi .sti .ppt .pot .pptx .pptm .potm .sdd .vor .odg .otg .sxd .std .sda .eps .tif .tiff .dxf .jpg .png .gif .bmp .wmf .emf .met .pct .sgf .sgv .svm .pbm .pcx';
endungen:=endungen+' .pgm .ppm .ras .tga .xbm .xpm .pcd .psd .odf .sxm .smf .mml';
result:=false;
if not fileexists(dat) then exit;
wtp := pfad_pdf_converter+'7p.exe';
if not FileExists(wtp) then
  begin
    ShowMessage('Word-to-PDF-Konverter  nicht gefunden'#13+wtp+#13+'Bitte von der folgenen Website 7-PDF Maker herunterladen und installieren'+#13+'und unter Optionen Pfade den Dateipfad ausw�hlen');
  	//html_name:='http://www.7-pdf.de/produkte/7-pdf-maker';
    html_name:='http://www.arbene.de/index.php/download/file/13-7-pdf-maker';
	  shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal);    //application.Handle
    exit;
  end;
//
endung:=dateiendung(dat);
if  pos(endung,endungen)=0 then
begin
  showmessage('Die Dateiendung '+endung+' wird nicht unterst�tzt');
  exit;
end;

 t:=time();

  befehl:=format(' "%s" ',[dat]);

  //executefile(wtp,befehl,'',true);   //#####2017-07-21
  //shellexecute_se( 0,'open',pchar(wtp),PChar(befehl),nil,0 );
  //if runasadmin(application.Handle,wtp,befehl) then
  if run_app(wtp,befehl) then
      warte_bis_datei_lesbar(pdf_dat)
   else
  showmessage(wtp+befehl+' wurde micht ausgef�hrt');

  result:=true;
end;


function tform_main.pdf_to_txt(pdf_dat, txt_dat:string):boolean;
var
befehl,wtp,html_name,pfad_ini,s:string;
p:integer;
g:boolean;
fs:tfilestream;
t:ttime;

begin
if not fileexists(pdf_dat) then
begin
  showmessage(pdf_dat +' existiert nicht');
  exit;
end;
wtp := 'C:\tools\xpdf\bin64\pdftotext.exe' ;

if not FileExists(wtp) then
  begin
    ShowMessage('Word-to-PDF-Konverter  nicht gefunden'#13+wtp+#13+'Bitte von der folgenen Website 7-PDF Maker herunterladen und installieren'+#13+'und unter Arbene->Optionen-> Pfade den Dateipfad f�r 7-PDF ausw�hlen');
  	//html_name:='http://www.7-pdf.de/produkte/7-pdf-maker';
    html_name:='http://www.arbene.de/index.php/download/file/13-7-pdf-maker';
	  shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal);
    exit;
  end;
//
 t:=time();
   befehl:=format(' %s %s ',[pdf_dat, txt_dat]);
  shellexecute_se( 0,'open',pchar(wtp),PChar(befehl),nil,0 ); //application.Handle
  //winrun(wtp,befehl);
  warte_bis_datei_lesbar(txt_dat);

end;


function warte_bis_datei_lesbar_handle(datei:string):boolean;
var
g:boolean;
t:ttime;
begin
  screen.Cursor:=crhourglass;
  result:=false;
  g:=true;
  t:=time();
  while  g do
  begin
    if fileexists(datei) then g:=false;
    pause(100);
      if  time()>t+0.0003 then    //60 Sec
        begin
          showmessage( datei+' ist nicht vorhanden');
          g:=false;
        end;
  end;

  g:=true;
  t:=time();
  while  g do
  begin
    application.ProcessMessages;
    g:=fileinuse(datei);
    if g then
    begin
      pause(100);
      if  time()>t+0.0003 then    //60 Sec
        begin
          showmessage( datei+' ist in Verwendung oder wird gerade erzeugt.');
          g:=false;
        end;
    end;
  end;
  result:=warte_bis_datei_lesbar(datei);
  application.ProcessMessages;
  screen.Cursor:=crdefault;
end;


function warte_bis_datei_lesbar(datei:string):boolean;
var
fs: tfilestream;
g:boolean;
t:ttime;
i:integer;
begin
  result:=false;

  g:=true;
  t:=time();
  while  g do
  begin
    if fileexists(datei) then g:=false;
    pause(100);
      if  time()>t+0.0003 then    //25 Sec
        begin
          showmessage( datei+' ist nicht vorhanden');
          g:=false;
          exit;
        end;
  end;

  g:=true;
  t:=time();
  i:=0;
  while g do
  begin
    try
      application.ProcessMessages;
      fs:=tfilestream.Create(datei,fmOpenRead); //wenn hier fehler
      g:=false;
      fs.free;
      application.ProcessMessages;
    except
      g:=true;
      pause(100);
      if  time()-t>0.00005 then   //3 sek
        begin
          showmessage( datei+' konnte nicht zum Lesen ge�ffnet werden (openread).');
          inc(i);
          t:=time();
          if i>=1 then exit; //10 sec
        end;
    end;
  end;
  result:=true;
end;


function pdf_anzeigen_vers_datei(ole:TOleContainerext;modus:string):boolean;
var
s,s_n,befehl,exe:string;
i,j:integer;
begin
   if ole.state=osempty  then
  begin
    showmessage('Es ist kein Dokument vorhanden.');
    result:=false;
    exit;
  end;
   if (lowercase(copy(ole.OleClassName,1,8))<>'acroexch') and (lowercase(copy(ole.OleClassName,1,11))<>'foxitreader') then
   begin
    showmessage('Dies ist keine PDF-Datei ' +ole.OleClassName);
    result:=false;
    exit;
   end;

  i:=1;
  repeat
    s:=pfad_temp+'a_pdf'+sonderzeichenraus_all(datetimetostr(now()));
    s:= s+inttostr(i);
    inc(i);
  until (not fileexists(s+'.pdf'));
  s_n:=s+'_n.pdf';
  s:=s+'.pdf';
  SaveOleContainerPDF(ole,s); //erstellen
  j:=0;
  repeat
    pause(100);
  until fileexists(s) or (j>10);
  if j>10 then exit;
  
  if modus = 'neu' then
  begin

    {exe:=pfad_pdftk+'pdftk.exe';
    if fileexists(exe) then
    begin
        befehl:=format(' A="%s"  cat A  output  "%s"  drop_xfa drop_xmp ',[s,s_n]);
        executefile(exe,befehl,'',true);
        //shellexecute_se(0,'open',pchar(exe),pchar(befehl),nil,SW_MINIMIZE	);
        s:=s_n;
        repeat
          pause(500);
        until fileexists(s_n);
    end;}
    s:= pdf_behandeln(s);
    ole.CreateObjectFromFile(s,false);
    result:=true;
  end;
  if modus='anzeigen' then
  begin
    shellexecute_se(0,'open',pchar(s),'','',sw_shownormal);
    result:=true;
  end;
end;

procedure olecontainer_to_pdf(container:tolecontainerext);
var
datname, datname_pdf:string;
z:integer;
begin

  if container.state=osempty  then
  begin
    showmessage('Es ist kein Dokument vorhanden.');
    exit;
  end;
  //if  not (arbeitsmedizin_proband.state in [osrunning,osOpen	]) then
  datname:=pfad_temp+'a_'+sonderzeichenraus_all(stringdatum(now()))+'_';
  z:=0;
  while  fileexists(datname+inttostr(z)+'.pdf') do inc(z);
  datname_pdf:=datname+inttostr(z)+'.pdf';
  datname:=datname+inttostr(z)+'.doc';

  try
    if (lowercase(copy(container.OleClassName,1,13))='word.document')
    then
    begin
       // SaveOleContainer_word_PDF(container,datname);

      olecontainer_anzeigen(container);
      container.OleObject.application.activedocument.saveas(Filename:=datname,FileFormat:=0);
      warte_bis_datei_lesbar(datname);
      olecontainer_close(container);
      word_to_pdf(datname,datname_pdf);
      deletefile(datname);
    end
    ELSE
    if (lowercase(copy(container.OleClassName,1,8))='acroexch') or (lowercase(copy(container.OleClassName,1,11))='foxitreader') then
    begin
        SaveOleContainerPDF(container,datname_pdf);
    end
    ELSE
      SaveOleContainerPDF(container,datname_pdf);

  except
    SaveOleContainerPDF(container,datname_pdf);
  end;

  repeat
    pause(100);
  until fileexists(datname_pdf);
  if fileexists(datname_pdf) then shellexecute_se(0,'open',pchar(datname_pdf),'','',sw_shownormal);  //application.Handle

  container.Modified:=false;
end;

procedure olecontainer_save_to_pdf(container:tolecontainerext;datname:string);
var
datname_pdf:string;
z:integer;
begin

  if container.state=osempty  then
  begin
    showmessage('Es ist kein Dokument vorhanden.');
    exit;
  end;
 

  datname_pdf:=datname+'.pdf';
  datname:=datname+'.doc';
  if (lowercase(copy(container.OleClassName,1,13))='word.document')
  then
  begin
    olecontainer_anzeigen(container);
    container.OleObject.application.activedocument.saveas(Filename:=datname,FileFormat:=0);
    warte_bis_datei_lesbar(datname);
    olecontainer_close(container);
    word_to_pdf(datname,datname_pdf);
    deletefile(datname);
  end;

  if (lowercase(copy(container.OleClassName,1,8))='acroexch') or (lowercase(copy(container.OleClassName,1,11))='foxitreader') then
       begin
          SaveOleContainerPDF(container,datname_pdf);
       end;
  //if fileexists(datname_pdf) then shellexecute_se(0,'open',pchar(datname_pdf),'','',sw_shownormal);

end;

procedure TForm_main.m_word_pdf_anzeige_karteiClick(Sender: TObject);
begin
  if not b.b_archiv_ok(datamodul.q_dokumente) then exit;
  if  (b.b_string('proband-dokumente-arzt')<1) then exit;
   // if (lowercase(copy(arbeitsmedizin_Proband.OleClassName,1,8))='acroexch') then olecontainer_to_pdf(arbeitsmedizin_Proband); //pdf_anzeigen_vers_datei(arbeitsmedizin_Proband);
   olecontainer_to_pdf(arbeitsmedizin_Proband);
end;





procedure ExecuteFile(const AFilename: String;
                 AParameter, ACurrentDir: String; AWait: Boolean;
                 AOnWaitProc: TExecuteWaitEvent=nil);
var
  si: TStartupInfo;
  pi: TProcessInformation;
  bTerminate: Boolean;
begin
  bTerminate := False;

  if Length(ACurrentDir) = 0 then
    ACurrentDir := ExtractFilePath(AFilename);

  if AnsiLastChar(ACurrentDir) = '' then
    Delete(ACurrentDir, Length(ACurrentDir), 1);

  FillChar(si, SizeOf(si), 0);
  with si do begin
    cb := SizeOf(si);
    dwFlags := STARTF_USESHOWWINDOW;
    wShowWindow := SW_NORMAL;
  end;

  FillChar(pi, SizeOf(pi), 0);
  AParameter := Format('"%s" %s', [AFilename, TrimRight(AParameter)]);
  if CreateProcess(Nil, PChar(AParameter), Nil, Nil, False,
                   CREATE_DEFAULT_ERROR_MODE or CREATE_NEW_CONSOLE or
                   NORMAL_PRIORITY_CLASS , Nil, PChar(ACurrentDir), si, pi) then
  try
    if AWait then
      while WaitForSingleObject(pi.hProcess, 200) <> Wait_Object_0 do
      begin
        if Assigned(AOnWaitProc) then
        begin
          AOnWaitProc(pi, bTerminate);
          if bTerminate then
            TerminateProcess(pi.hProcess, Cardinal(-1));
        end;

        Application.ProcessMessages;
      end;
  finally
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
  end;
end;


procedure DeleteFiles(Path: string; Name: string);
var srec: TSearchRec;
begin

  if FindFirst(Path+Name, faAnyFile, srec)=0 then
  begin
  DeleteFile(Path+srec.Name);
  while FindNext(srec)=0 do
  DeleteFile(Path+srec.Name);
  end;
  FindCLose(srec);
end;

procedure tform_main.dokumente_texte_komprimieren;
var
nr,query,all,hs:string;
i:integer;
begin
with datamodul do
begin
  query:='select haupt from haupt_tabelle';
 hs:=mysql_d.Feldinhalt(query,0);

 if hs='0'then exit; //nur Hauptsystem erlaubt

 query:='select count(*) from dokumente';
 all:=mysql_d.Feldinhalt(query,0);
 i:=0;
  query:='select nummer from dokumente';
  sql_new(false,q_3,query,'');
  q_3.first;
  while not q_3.eof do
  begin
    nr:=q_3.findfield('nummer').AsString;
    query:=format('select * from dokumente where nummer=%s',[nr]);
    sql_new(false,q_4,query,'');
    stream_komprimieren(q_4,'ole');
    inc(i);
    form_start.StatusBar.panels[0].text:='Dokument '+inttostr(i) +' von '+all;
     application.ProcessMessages;
     q_3.Next;
  end;
  query:='select count(*) from texte';
 all:=mysql_d.Feldinhalt(query,0);
 i:=0;
  query:='select nummer from texte';
  datamodul.sql_new(false,q_3,query,'');
  q_3.first;
  while not q_3.eof do
  begin
     nr:=q_3.findfield('nummer').AsString;
    query:=format('select * from texte where nummer=%s',[nr]);
    sql_new(false,q_4,query,'');
     stream_komprimieren(q_4,'ole');
     inc(i);
     form_start.StatusBar.panels[0].text:='Texte '+inttostr(i)+' von '+all;;
     application.ProcessMessages;
     q_3.Next;
  end;
end;
form_start.StatusBar.panels[0].text:='Update abgeschlossen';
end;


procedure TForm_main.alsPDFanzeigen1Click(Sender: TObject);
begin
  if not b.b_archiv_ok(datamodul.q_texte) then exit;
  if  (b.b_string('firma-dokumente')<1) then exit;
  //if (lowercase(copy(arbeitsmedizin_firma.OleClassName,1,8))='acroexch') then olecontainer_to_pdf(arbeitsmedizin_firma); //pdf_anzeigen_vers_datei(arbeitsmedizin_firma);
  olecontainer_to_pdf(arbeitsmedizin_firma);
end;

function GetEnvVarValue(const VarName: string): string;
var
  BufSize: Integer; // buffer size required for value
begin
  // Get required buffer size (inc. terminal #0)
  BufSize := GetEnvironmentVariable(PChar(VarName), nil, 0);
  if BufSize > 0 then
  begin
    // Read env var value into result string
    SetLength(Result, BufSize - 1);
    GetEnvironmentVariable(PChar(VarName),
    PChar(Result), BufSize);
  end
  else
    // No such environment variable
    Result := '';
end;

function GetSpecialFolderPath(Folder: Integer; CanCreate: Boolean): string;

// Gets path of special system folders
//
// Call this routine as follows:
// GetSpecialFolderPath (CSIDL_PERSONAL, false)
//        returns folder as result
//
var
   FilePath: array [0..255] of char;

begin
 SHGetSpecialFolderPath(0, @FilePath[0], FOLDER, CanCreate);
 Result := FilePath;
end;

procedure TForm_main.m_SummederZeitenanzeigenClick(Sender: TObject);
var
i,st,min:integer;
t1:real;
m,h,f:string;

begin
  //
  if dbgridext_zeitabrechnung.SelectedRows.Count=0 then
  begin
    showmessage('Es muss mindestens eine Zeile markiert sein');
    exit;
  end;
      st:=0;
      min:=0;
      for I := 0 to dbgridext_zeitabrechnung.SelectedRows.Count - 1 do
      begin
           datamodul.q_firma_sollist.GotoBookmark(Pointer(dbgridext_zeitabrechnung.SelectedRows.Items[I]));

           st:=st+ datamodul.q_firma_sollist.findfield('stunden').asinteger;
           min:=min+ datamodul.q_firma_sollist.findfield('minuten').asinteger;
      end;
    st:=st+min div 60;
    min:=min mod 60;
    t1:=st+min/60;
    m:=inttostr(min);
    if length(m)=1 then m:='0'+m;
    h:=inttostr(st);
    if length(h)=1 then h:='0'+h;
    f:=FloatToStrF(t1,fffixed,7,2) ;
    showmessage(format('Summe der Zeiten: %s:%s  (hh:mm)  bzw. %s (Stunden)',[h, m,  f]));
end;

procedure TForm_main.PageControl_firma_einstellungenChange(
  Sender: TObject);
begin
//PageControl_firmaChange(sender);
//datamodul.q_firma_zeiten_soll.Refresh;
toolbuttonneu.Enabled:=true;
toolbuttonedit.Enabled:=true;

if (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_rechnung ) then
	begin
        toolbuttonneu.Enabled:=false;
	end;

if (pagecontrol_firma_einstellungen.ActivePage=tabsheet_firma_einstellungen_zeitabrechnung ) and (not ist_hauptsystem) then
	begin
        toolbuttonneu.Enabled:=false;
        toolbuttonedit.Enabled:=false;
	end;

end;


function TForm_main.word_to_pdf1(w_dat, pdf_dat:string):boolean;
var
  SevenPDFComObj : TSevenPDFObjConverter;
  InFile, OutFile : WideString;
  returncode:integer;
  code:string;

begin

  InFile := w_dat;
  OutFile := pdf_dat;

  SevenPDFComObj := TSevenPDFObjConverter.Create(nil);

  try //finally
    try //exception

      //******* INITIALIZATION *********

        SevenPDFComObj.Init;
        //SevenPDFComObj.UnlockKey('INSERT LICENSEKEY!');

      //********************************

      //****** Customize some PDF Settings *******
      //Notice: PDF encryption works only in registered version
      //******************************************

        SevenPDFComObj.setExportNotes(0);
        SevenPDFComObj.setExportNotesPages(0);
        SevenPDFComObj.setExportBookmarks(0);

        //Set PDF Security Options
        {
        SevenPDFComObj.setEncryptFile(1);
        SevenPDFComObj.setPermissionPassword('test123');

        SevenPDFComObj.setRestrictPermissions(1);
        SevenPDFComObj.setChanges(0);
        SevenPDFComObj.setPrinting(1);
        SevenPDFComObj.setEnableCopyingOfContent(0);
        SevenPDFComObj.setEnableTextAccessForAccessibilityTools(0);
        }

      //Make the Conversion


      Returncode := (SevenPDFComObj.convertToPdf(PWideChar(InFile), PWideChar(OutFile), 0));
      case returncode of
       0: code:='Die Umwandlung ist fehlgeschlagen.';
      99: code:='initmethode nicht aufgerufen';
      100: code:='outputpath is empty or destinationpath not exists';
      101: code:='outputfileextensin not pdf';
      102: code:='filetyp of inputfile is empty';
      103:code:='filteyp of inputfile not supported';
      104: code:='inputfilepath is empty';
      105: code:= 'allgemeiner Fehler bei der Umwandlung';
      else
        code:='';
      end;
      if code<>'' then showmessage(code);


    except
    on E:Exception do   // Exception abfangen, falls der User nicht die Rechte hat einen Task zu killen!
      begin
        ShowMessage('An excepton occurs: ' + E.Message);
      end;
    end;

  finally
    if Assigned(SevenPDFComObj) then begin
      SevenPDFComObj.Free;
    end;
  end;

end;

function FileInUse(FileName: string): Boolean;    //true wenn in benutzung oder nicht vorhanden
var
  hFileRes: HFILE;
begin
  Result := true;
  if not FileExists(FileName) then
    Exit;

  hFileRes := CreateFile(PChar(FileName),
    GENERIC_READ,// {or GENERIC_WRITE},
    1, //0
    nil,
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    0);
  Result := (hFileRes = INVALID_HANDLE_VALUE);
  if not Result then
  CloseHandle(hFileRes);
end;


procedure TForm_main.m_erweiterteFehlermeldungenClick(Sender: TObject);
begin
if m_erweitertefehlermeldungen.Checked then
begin
   m_erweitertefehlermeldungen.Checked:=false;
   modus_debug:=false;
   Application.OnException := nil;
end
else
begin
   m_erweitertefehlermeldungen.Checked:=true;
   modus_debug:=true;
   Application.OnException := AppException;
end;
end;



procedure ole_to_text(tabelle, nummer:string);
var
  datname,query:string;
  z:integer;
  datei:tstringlist;
begin
   //stillgelegt
    query:=format('select * from %s where nummer=%s',[tabelle,nummer]);
    datamodul.sql_new(false,datamodul.q_11,query,'');
    datname:=pfad_temp+'a_'+stringdatum(now())+'_';
    datname:=sonderzeichenraus(datname);


    z:=0;
    while  fileexists(datname+inttostr(z)+'.pdf') do inc(z);
    datname:= datname+inttostr(z);
    try
      datei:=tstringlist.Create;
      if datamodul.q_11.FindField('ole')<>nil then
      form_main.olecontainer_laden(datamodul.q_11,'ole',form_main.arbeitsmedizin);
      if form_main.arbeitsmedizin.State in [osloaded] then
      begin
        olecontainer_save_to_pdf(form_main.arbeitsmedizin,datname);
        form_main.pdf_to_txt(datname+'.pdf',datname+'.txt');
        datamodul.q_11.Edit;
        datei.LoadFromFile(datname+'.txt');
        datamodul.q_11.FindField('t_ole').asstring:=datei.Text;
        datamodul.q_11.Post;
        deletefile(datname+'.txt');
        deletefile(datname+'.pdf');
      end;
    finally
      datei.Free;
    end;
end;



procedure TForm_main.PageControl_wdvChange(Sender: TObject);
begin
liste_leeren(treeview_agenda);
if pagecontrol_wdv.ActivePage=TabSheet_wdv_freitextsuche then treeview_agenda.HelpContext:=91 else   treeview_agenda.helpcontext:=66;
end;

procedure TForm_main.zeitansatz_lesen;
var
s,wert,query:string;
p:integer;
begin
	if connected then
	begin
	  query:='select zeitansatz from haupt_tabelle';
    s:=mysql_d.Feldinhalt(query,0);
    if s='' then exit;
    s:=s+',';
    DBComboBox_zeitansatz.Items.Clear;
    p:=pos(',',s);
    while p>0 do
    begin
      wert:=trim(copy(s,0,p-1));
      if wert<>'' then  DBComboBox_zeitansatz.Items.Add(wert);
      s:=copy(s,p+1,length(s));
      p:=pos(',',s);
    end;

  end;
end;

function tform_main.untersucher_stempel(nr:integer):string;
var
query:string;

begin
query:=format('select * from untersucher where nummer=%d',[nr]);
datamodul.sql_new(false,datamodul.q_12,query,'');
if datamodul.q_12.findfield('untersucher').asstring<>'' then
	result:=htmltag(datamodul.q_12.findfield('f_1').asstring ,datamodul.q_12.findfield('untersucher').asstring);
  if datamodul.q_12.findfield('untersucher_2').asstring<>'' then
  result:=result+htmltag(datamodul.q_12.findfield('f_2').asstring ,datamodul.q_12.findfield('untersucher_2').asstring);
  if datamodul.q_12.findfield('untersucher_3').asstring<>'' then
  result:=result+htmltag(datamodul.q_12.findfield('f_3').asstring ,datamodul.q_12.findfield('untersucher_3').asstring);
  if datamodul.q_12.findfield('untersucher_4').asstring<>'' then
  result:=result+htmltag(datamodul.q_12.findfield('f_4').asstring ,datamodul.q_12.findfield('untersucher_4').asstring);
  if datamodul.q_12.findfield('untersucher_email').asstring<>'' then
  result:=result+htmltag(datamodul.q_12.findfield('f_email').asstring ,datamodul.q_12.findfield('untersucher_email').asstring);
  if datamodul.q_12.findfield('untersucher_telefon').asstring<>'' then
  result:=result+htmltag(datamodul.q_12.findfield('f_telefon').asstring ,datamodul.q_12.findfield('untersucher_telefon').asstring);
end;



function htmltag(f, text:string):string;
begin
  result:=format('<%s>%s</%s>',[f,text,f]){+#10#13};
end;

function html_br(px:string):string;
var
h:string;
begin
try
  //h:=inttostr(hoehe)+'%';
  //result:=format('<H6 style="line-height: %s"> <br /> </H6>',[h]);
  result:=format('<H6 style="font-size:%spx;"> <br /> </H6>',[px]);
 // result:=format('<H6 style="line-height: %spx"> <br /> </H6>',[px]);
except
  result:='';
end;
end;

procedure TForm_main.Timer_untplanTimer(Sender: TObject);
begin
timer_untplan.Enabled:=false;
form_main.unt_planen(datamodul.q_untersuchung);

end;

procedure TForm_main.daten1Click(Sender: TObject);
begin
zeigedaten;
end;

procedure Explode(Input: String; Delim: Char;var v:tstringlist);
var
  P: Integer;
  UnEsc: String;
  Sub: String;
begin
  //v := nil;
  if Delim = '%' then Sub := '$ESCAPEDCHAR$' else Sub := '%ESCAPEDCHAR%';
  v.Clear;
  UnEsc := AnsiReplaceText(Input, '\' + Delim, Sub);
  while Pos(Delim, UnEsc) > 0 do
  begin
    P := Pos(Delim, UnEsc);
    v.Add(Copy(UnEsc, 1, P-1));
    Delete(UnEsc, 1, P);
  end;
  if UnEsc <> '' then v.Add(UnEsc);
  for P := 0 to v.Count - 1 do
    v.Strings[P] := AnsiReplaceText(v.Strings[P], Sub, '\' + Delim);
end;




function IsRunning(ExeFileName: string): Boolean;    //uses .....Tlhelp32....  //hierf�r diese Unit einbinden
var ContinueLoop: BOOLean;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
begin
  result := False;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := Sizeof(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle,FProcessEntry32);
  while integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = UpperCase(ExeFileName))
        or (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName))) then
    Result := True;
    ContinueLoop := Process32Next(FSnapshotHandle,FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

function ProgIDExists(const ProgID:WideString):Boolean;
var
   tmp : TGUID;
begin
   Result := Succeeded(CLSIDFromProgID(PWideChar(ProgID), tmp));
end;

procedure TForm_main.SpeedButton20Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
begin
 datamodul.q_untersuchung.edit;
	 datamodul.q_untersuchung['w_dat'] :=int(form_kalender.auswahlkalender.date);

end;
end;


procedure tab_exportieren(tabelle:tzquery; m:TIntSet );
var
zeile:string;
i,zaehler:integer;
f:extended;
begin
with datamodul, form_main do
begin
	com.excel_connect('Auswertung');

  excel.visible:=false;
	tabelle.First;
	for i:=0 to tabelle.fieldcount-1 do
	begin
     if (i in m) then
		 com.excel_PutStrAt(1,i+1,tabelle.Fields[i].fieldname);
	end;

	zaehler:=2;
	while not tabelle.eof do
	begin
		zeile:='';

		for i:=0 to tabelle.fieldcount-1 do
		begin
      if (i in m) then //tabelle.Fields[i].FieldName in []
		  case tabelle.Fields[i].datatype of
		  ftInteger,ftSmallint,ftWord  :com.excel_PutIntAt(zaehler,i+1,tabelle.Fields[i].asinteger);
		  ftfloat: begin
                f:=tabelle.Fields[i].asfloat;
                com.excel_PutExtAt(zaehler,i+1,f);
              end;
		  else
        com.excel_PutStrAt(zaehler,i+1,tabelle.Fields[i].asstring);
		  end;
		end;
		inc(zaehler);
		tabelle.Next;
	end;
  com.exel_optimale_hoehe_breite;
  excel.visible:=true;
	com.excel_disconnect;
end;
end;

function zugeordneter_arzt:integer;
var
i:integer;
query,r:string;
begin
  result:=0;
  query:=format('select i_untersucher from schluessel where nummer=%d',[user_id] );
  r:= mysql_d.feldinhalt(query,0);
  if r<>'' then result:=strtoint(r);

  i:=datamodul.q_firma.findfield('i_arzt').AsInteger;
  if i>0 then result :=i;
  //i:=datamodul.q_mitarbeiter.findfield('i_arzt').AsInteger;
  //if i>0 then result :=i;
end;

procedure TForm_main.m_f_allegelschtenPositionenwiederaktivierenClick(
  Sender: TObject);
  var
  query:string;
  srenu:string;
begin
  if Messagedlg('Sollen die gel�schten Positionen wieder aktiviert werden?',mtConfirmation, mbOkCancel	,0)=mrOK then
  begin
    srenu:=datamodul.q_rechnung.findfield('nummer').AsString;
    query:=format('update re_positionen set storno=0, dauerloesch=0 where i_rechnung=%s',[srenu]);
    mysql_d.sql_exe(query);
    datamodul.q_repositionen.Refresh;
  end;

end;

procedure TForm_main.m_propertiesClick(Sender: TObject);
begin

arbeitsmedizin_proband.ObjectPropertiesDialog

end;

procedure TForm_main.m_iconicClick(Sender: TObject);
begin

arbeitsmedizin_proband.Iconic := not arbeitsmedizin_proband.Iconic ;

end;

procedure TForm_main.history_entfernen;
var
knoten:ttreenode;
begin


    groupbox_tree_hist.Height:=0;
    dbtreeview.Enabled:=true;
    //tabelle.SQL.Text:=query;
    if hist_daten.groupbox <>nil then hist_daten.groupbox.Caption:='';

    if hist_daten.grid<>nil then
    begin

      hist_daten.grid.Columns.Delete(hist_daten.grid.Columns.Count-1);
      hist_daten.grid.Columns.Delete(hist_daten.grid.Columns.Count-1);
      grid_einstellen(hist_daten.grid);
    end;
    if hist_daten.zquery<>nil then
    begin
      datamodul.sql_new(false,hist_daten.zquery,hist_daten.query,'');
      hist_daten.zquery.Refresh;
    end;
    hist_daten.zquery:=nil;


    hist_sperren:=false;
    modus_history:=false;

    if hist_daten.grid=nil then   //hier dbspeichern
    begin
      knoten:=dbtreeview.Selected;
      dbtreeview.selected:=dbtreeview.TopItem;
      dbtreeview.selected:=dbtreeview.FindNextToSelect;
      dbtreeview.Selected:=knoten;
   end;
   


end;



procedure TForm_main.history_erstellen;
var
query,num,tab_name,s:string;
grid:tdbgridext;
zquery:tzquery;
hist_set:thist_set;
begin
//suche tzquery und grid bzw treeviw
dbspeichern(true);

groupbox_tree_hist.Height:=dbtreeview.Height div 3;
dbtreeview.Enabled:=false;


try
hist_set:=aktuelle_tabelle;

grid:=hist_set.grid ;
zquery:=hist_set.zquery;
tab_name:=hist_set.tab_name;
// erzeuge history
if zquery =nil then exit;

query:=format('show tables like "log_%s"',[tab_name]);
s:=mysql_d.Feldinhalt(query,0);
if s='' then
begin
  showmessage(format('Die Tabelle "log_%s" existiert nicht. �berpr�fen Sie die Historien-Funktion �ber die Datenbanktools',[tab_name]));
  exit;
end;

num:=zquery.findfield('nummer').asstring;
if num='' then exit; // kein DS

mysql_d.history_von(tab_name,'history',num);


// vertausche query

// speichern alt
    hist_daten.query:=zquery.SQL.Text;
    hist_daten.zquery :=zquery;
    hist_daten.grid:=grid;
    hist_daten.tab_name:=tab_name;
    hist_daten.groupbox:=hist_set.groupbox;
//neue query   join mit schl�ssel name
    query:='select * from history order by nr ';
    datamodul.sql_new(false,zquery,query,'');
if grid<>nil then
begin

    hist_set.groupbox.Caption:='Historie von '+hist_set.anzeigetext ;

    grid.Columns.Add;
    grid.Columns.Items[grid.Columns.Count-1].FieldName:='dt';
    grid.Columns.Items[grid.Columns.Count-1].Title.caption:='�nderungs-Datum';
    grid.Columns.Items[grid.Columns.Count-1].Width:=200;
    grid.Columns.Add;
    grid.Columns.Items[grid.Columns.Count-1].FieldName:='c_user';
    grid.Columns.Items[grid.Columns.Count-1].Title.caption:='�nderung durch';
    grid.Columns.Items[grid.Columns.Count-1].Width:=120;
    grid_einstellen(grid);
    //tabelle.SQL.Text:=query;
    zquery.Refresh;
end
else
 if hist_set.datasource<>nil then
 begin
  dbgridext_tree_hist.DataSource:=hist_set.datasource;
  groupbox_tree_hist.Caption:='Historie von '+hist_set.anzeigetext ;
 end;

finally
  dispose(hist_set);
  modus_history:=true;
end;
end;

function tform_main.aktuelle_tabelle:thist_set;
var
sqll,ds_nr:string;
begin
new(result);
result.zquery:=nil;
result.grid:=nil;
result.datasource:=nil;
result.groupbox:=nil;
with datamodul do
begin
if pagecontrol_main.ActivePage=tabsheet_firma then
begin
	 if pagecontrol_firma.activepage=tabsheet_firmenadresse then
		begin
     {result.zquery:=q_firma;
     result.grid:=dbgridext_firma;
     result.tab_name:='firma';
     result.groupbox:=groupbox_firma;}
		end;
   if pagecontrol_firma.activepage=tabsheet_firma_filter then
		begin

		end;

   if pagecontrol_firma.activepage=tabsheet_ansprechpartner then
		begin

		end;

	if (pagecontrol_firma.activepage=tabsheet_sollist)then
		begin
     result.zquery:=q_firma_sollist;
     result.grid:=dbgridext_zeitabrechnung;
     result.tab_name:='firma_sollist';
     result.groupbox:=groupbox_sollist;
     result.anzeigetext:='Zeitabrechnung';
		end;


    if pagecontrol_firma.activepage=tabsheet_rechnungen then
		begin

		end;
		if pagecontrol_firma.activepage=tabsheet_firma_texte then
		begin
     result.zquery:=q_texte;
     result.grid:=dbgridext_texte;
     result.tab_name:='texte';
     result.groupbox:=groupbox_firma_text;
     result.anzeigetext:='Dokumente';
		end;

end;

if (pagecontrol_main.ActivePage=tabsheet_stammdaten)  then
		begin
      result.zquery:=q_mitarbeiter;
     result.grid:=dbgridext_namen;
     result.tab_name:='mitarbeiter';
     result.groupbox:=groupbox_mitarbeiter;
     result.anzeigetext:='Stammdaten Probanden';
		end;


if pagecontrol_main.ActivePage=tabsheet_main then
	begin
		if prg_typ=3 then
		  pagecontrol_befunde.ActivePage:=tabsheet_be_doku
		else
		  pagecontrol_befunde.ActivePage:=tabsheet_be_anmerkung;

    dbtreeview.tag:=0;
		case dbtreeview.a_rubrik of
			1:
				begin
									//result:=p_entsperren(q_mitarbeiter,q_befunde,user_id,[panel_befunde_datum,pagecontrol_befunde,panel_bef_auff,notebook_befund,groupbox_bef_abr],false);
          result.zquery:=q_akt_untersuchung;
          result.grid:=nil;
          result.tab_name:='akt_untersuchung';
          result.datasource:=ds_akt_untersuchung;
          result.anzeigetext:='Befunde';

				end;
			2: begin
					//result:=p_entsperren(q_mitarbeiter,q_untersuchung,user_id,[panel_untersuchung],false);
          result.zquery:=q_untersuchung;
          result.grid:=nil;
          result.tab_name:='untersuchung';
          result.datasource:=ds_untersuchung;
          result.anzeigetext:='Vorsorge / Eignung';
				end;
			3:	begin
					//result:=p_entsperren(q_mitarbeiter,q_impfung,user_id,[panel_impfung],false);
          result.zquery:=q_impfung;
          result.grid:=nil;
          result.tab_name:='impfung';
          result.datasource:=ds_impfung;
          result.anzeigetext:='Impfung';
				end;
			4: begin
					//result:=p_entsperren(q_mitarbeiter,q_labor,user_id,[panel_labor],false);
          result.zquery:=q_labor;
          result.grid:=nil;
          result.tab_name:='labor';
          result.datasource:=ds_labor;
          result.anzeigetext:='Labor';
				end;
			5: begin
				//	 result:=p_entsperren(q_mitarbeiter,q_ambulanz,user_id,[panel_ambulanz],false)
          result.zquery:=q_ambulanz;
          result.grid:=nil;
          result.tab_name:='ambulanz';
          result.datasource:=ds_ambulanz;
          result.anzeigetext:='Ambulanz';
				end;
			6: begin
					//result:=p_entsperren(q_mitarbeiter,q_diagnosen,user_id,[groupbox_diagnosen],false);
          result.zquery:=q_diagnosen;
          result.grid:=nil;
          result.tab_name:='diagnosen';
          result.datasource:=ds_diagnosen;
          result.anzeigetext:='Diagnosen';
				 end;
			7: begin
					//result:=p_entsperren(q_mitarbeiter,q_besonderheiten,user_id,[groupbox_besonderheiten],false);
          result.zquery:=q_besonderheiten;
          result.grid:=nil;
          result.tab_name:='besonderheiten';
          result.datasource:=ds_besonderheiten;
          result.anzeigetext:='Besonderheiten';
				end;
			8: begin
					//result:=p_entsperren(q_mitarbeiter,q_dokumente,user_id,[panel_texte],false);
          result.zquery:=q_dokumente;
          result.grid:=nil;
          result.tab_name:='dokumente';
          result.datasource:=ds_dokumente;
          result.anzeigetext:='Dokumente';
				end;
			10:begin
        //result:=p_entsperren(q_mitarbeiter,q_rechnung,user_id,[panel_rechnung_ma],false);

				end;
			11:begin
					//result:=p_entsperren(q_mitarbeiter,q_rechnung,user_id,[panel_verkauf],false);
				end;
        12:begin
					//result:=p_entsperren(q_mitarbeiter,q_kontakte,user_id,[panel_kontakte],false);
				end;
        15: begin
              //result:=p_entsperren(q_mitarbeiter,q_mitarbeiter,user_id,[panel_anamnese,toolbar_anamnese],refresh);
               result.zquery:=q_anamnese;
               result.grid:=nil;
               result.tab_name:='anamnese';
               result.datasource:=ds_anamnese;
               result.anzeigetext:='Anamnese';
            end;

		end;
end;
end;
end;


procedure TForm_main.ToolButton_historyClick(Sender: TObject);
begin
if hist_sperren then exit;
hist_sperren:=true;
toolbuttonneu.Enabled:= not modus_history;
toolbuttonloeschen.Enabled:=not modus_history;
toolbuttonsichern.Enabled:=not modus_history;
toolbutton_abort.enabled:=not modus_history;

if hist_daten.zquery=nil then  history_erstellen else history_entfernen;
hist_sperren:=false;

end;

procedure TForm_main.Timer_datumswechselTimer(Sender: TObject);
begin
if date()<>altes_datum then
begin
   meditdate:=datetostr(date());
    altes_datum:=date();
end;
end;

procedure TForm_main.m_allemarkierenClick(Sender: TObject);
begin
datamodul.q_repositionen.First;
while not datamodul.q_repositionen.Eof do
begin
  dbgridext_re_positionen.SelectedRows.CurrentRowSelected:=true;
  datamodul.q_repositionen.next;
end;
end;

procedure TForm_main.m_rechnung_f_allemarkierenClick(Sender: TObject);
begin
datamodul.q_repositionen.First;
while not datamodul.q_repositionen.Eof do
begin
  dbgridext_repo.SelectedRows.CurrentRowSelected:=true;
  datamodul.q_repositionen.next;
end;
end;



function NetzlaufwerkVerbinden(Laufwerk: string; Pfad: string;
  Username: string; Password: string; RestoreAtLogon: Boolean): DWORD;
var
  NetResource: TNetResource;
  dwFlags: DWORD;
begin
  with NetResource do
  begin
    dwType := RESOURCETYPE_DISK;
    lpLocalName := PChar(Laufwerk);
    lpRemoteName := PChar(Pfad);
    lpProvider := nil;
  end;

  if (RestoreAtLogon) then
    dwFlags := CONNECT_UPDATE_PROFILE
  else
    dwFlags := 0;

    if username='' then
    Result := WNetAddConnection2(NetResource, nil ,nil, dwFlags)
    else
    Result := WNetAddConnection2(NetResource, PChar(Password),PChar(Username), dwFlags);



end;


function NetzlaufwerkTrennen(_locDrive: string; _showError: Boolean; _force: Boolean; _save: Boolean): DWORD;
var
  dwFlags: DWORD;
  errCode: DWORD;

begin
  { Set dwFlags, if necessary }
  if _save then
    dwFlags := CONNECT_UPDATE_PROFILE
  else
    dwFlags := 0;
 
  { Cancel the connection }
  errCode := WNetCancelConnection2(PChar(_locDrive), dwFlags, _force);

  { Show Errormessage, if flag is set }
  if (errCode <> NO_ERROR) and (_showError) then
  begin

    Application.MessageBox(PChar('An error occured while disconnecting:' + #13#10 +
      SysErrorMessage(GetLastError)),
      'Error while disconnecting',
      MB_OK);

  end;
  Result := errCode; { NO_ERROR }
end;


procedure oleinfo(container:tolecontainerext);
var rguid:tguid;
clientsite: ioleclientsite;
s,s1,s2,s3:string;
i:integer;
begin
 //container.ObjectPropertiesDialog;

 try
   container.OleObjectInterface.GetUserClassID(rguid);
   SetString(s, PAnsiChar(@rguid.d4[0]), 8);
   for i:=0 to 1 do
   begin
    s1:=s1+inttohex(rguid.d4[i],2);
   end;
    for i:=2 to 7 do
   begin
    s2:=s2+inttohex(rguid.d4[i],2);
   end;
 except

 end;
 
 s3:=container.Name+#13#10;

 try
  s3:= s3+container.OleClassName ;
 except
   s3:= s3+'Fehler OleClassName';
 end;

  try
    if ProgIDExists(container.OleClassName) then
      s3:=s3+' -ProgIDExists -OK'
    else
      s3:=s3+' -ProgIDExists -FEHLER!!' ;
  except
     s3:=s3+' Fehler ProgIDExists ';
  end;

 s3:=s3+#13#10;

 try
 s3:=s3+ (inttohex(rguid.d1,8)+'-'+inttohex(rguid.d2,4)+'-'+inttohex(rguid.d3,4)+'-'+s1+'-'+s2  )+#13#10;
 except
  s3:=s3+ 'Fehler keine Reg-ID'+#13#10;
 end;

 clipboard.AsText:=s3;

 showmessage( s3 );


end;

procedure TForm_main.InfozumDokument1Click(Sender: TObject);
begin
if not (arbeitsmedizin_firma.State in [osempty] )then oleinfo(arbeitsmedizin_firma) ;
if not (arbeitsmedizin_proband.State in [osempty] )then oleinfo(arbeitsmedizin_proband) ;

end;

procedure TForm_main.OleSpeichern1Click(Sender: TObject);
var
datname: string;

begin
if form_main.SaveDialog.Execute then
begin

datname:= form_main.SaveDialog.FileName;
  if arbeitsmedizin_firma.State in [osloaded , osopen] then
  arbeitsmedizin_firma.SaveAsDocument(datname)
  else
  arbeitsmedizin_firma.SaveAsDocument(datname);
  end;

end;

procedure TForm_main.Oleladen1Click(Sender: TObject);
var
datname: string;

begin
if form_main.SaveDialog.Execute then
begin

datname:= form_main.SaveDialog.FileName;
  if arbeitsmedizin_firma.State in [osloaded , osopen] then
  arbeitsmedizin_firma.LoadFromFile(datname)
  else
  arbeitsmedizin_firma.LoadFromFile(datname);
  end;
end;

procedure TForm_main.m_projektmithbClick(Sender: TObject);
begin
  if assigned(form_ap) then form_ap.report(0);
end;

procedure TForm_main.m_UnfallverteilunginderFirmaClick(Sender: TObject);
begin
    if assigned(form_ap) then form_ap.Unfall_in_firma();

end;

procedure TForm_main.m_UnfallverteilunginderAbteilungClick(
  Sender: TObject);
begin
  if assigned(form_ap) then form_ap.Unfall_in_Abteilung();
end;

procedure TForm_main.m_ProjektemitPrioAClick(Sender: TObject);
begin
   if assigned(form_ap) then form_ap.report(3);
end;

procedure TForm_main.m_ProjektemitPrioBClick(Sender: TObject);
begin
    if assigned(form_ap) then form_ap.report(2);
end;

procedure TForm_main.m_ProjektemitPrioCClick(Sender: TObject);
begin
  if assigned(form_ap) then form_ap.report(1);
end;

procedure TForm_main.m_ueberpruefenderBescheinigungenClick(
  Sender: TObject);
begin
  form_ap.vorsorge_pruefen();
end;

procedure TForm_main.AuswertungberdieAbteilung1Click(Sender: TObject);
begin
  auswertung_abt(true);
end;

procedure TForm_main.auswertung_abt(modus:boolean);
var
abt,ap,s_firma,query:string;
begin

 if  not ((b.b_string_silent('Auswertung-Gesamt')>-1) or (b.b_string_silent('Auswertung-Arbeitssicherheit')>-1) or (b.b_string_silent('Proband-Bescheinigung')>-1) or (b.b_string_silent('Proband-Impfung')>-1) ) then
 begin
	  showmessage('Sie haben keine Berechtigung');
	  exit;
 end;
 dbspeichern(false); //17.01.2001
 formular_sperren;

 if modus then
 begin
   akt_abteilung:=form_ap.dbtreeview_k1.get_index(1);
    akt_arbeitsplatz:=form_ap.dbtreeview_k1.get_index(2);

     query:='select kuerzel from abteilung where nummer='+inttostr(akt_abteilung);
      abt:=mysql_d.Feldinhalt(query,0);

      query:='select name from a_platz where nummer='+inttostr(akt_arbeitsplatz);
      ap:=mysql_d.Feldinhalt(query,0);
  end;

  s_firma:=datamodul.q_firma.findfield('nummer').AsString;
  
 //datamodul.sql_new(false, datamodul.q_a2,'select name, vorname, geb_dat from mitarbeiter where nummer=-1','');
 form_auswertung.Abfrage_zurueck(0);
 form_auswertung.abfrage_neu('Firma',s_firma);
 if abt<>'' then form_auswertung.abfrage_neu('Proband-Abteilung',abt);
 if ap<>'' then form_auswertung.abfrage_neu('Proband-Arbeitsplatz',ap);

 form_auswertung.PageControl_auswertung.ActivePage:= form_auswertung.tabsheet_filtern;
 form_auswertung.showmodal;

end;

procedure TForm_main.m_AuswertungmitausgewhlterFirmaClick(Sender: TObject);
begin
  auswertung_abt(false);
end;

procedure TForm_main.m_AuswertungmitausgewhlterFirma1Click(
  Sender: TObject);
begin
   auswertung_abt(false);
end;

procedure TForm_main.m_DokumentohneSpeichernerstellenClick(
  Sender: TObject);
begin
  speichern;
  dok_erstellen(Arbeitsmedizin,1,'mitarbeiter',datamodul.q_mitarbeiter,[''],['']);

  //olecontainer_destroy(Arbeitsmedizin);
end;

procedure TForm_main.m_DokumentohneSpeichernerstellen_firmaClick(
  Sender: TObject);
begin
  speichern;
  dok_erstellen(Arbeitsmedizin,0,'firma',datamodul.q_firma,[''],['']);
end;


function Run_app( wtp: string; befehl: string): Boolean;
begin
if runadmin then
  result:=RunAsAdmin(application.Handle,wtp,befehl)
else
  result:=shellexecute_se( 0,'open',pchar(wtp),PChar(befehl),nil,sw_shownormal );
end;


function RunAsAdmin(hWnd: HWND; filename: string; Parameters: string): Boolean;
{
    See Step 3: Redesign for UAC Compatibility (UAC)
    http://msdn.microsoft.com/en-us/library/bb756922.aspx
    
}
var
    sei: TShellExecuteInfo;
begin
    ZeroMemory(@sei, SizeOf(sei));
    sei.cbSize := SizeOf(TShellExecuteInfo);
    sei.Wnd := hwnd;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI;
    sei.lpVerb := PChar('runas');
    sei.lpFile := PChar(Filename); // PAnsiChar;
    if parameters <> '' then
        sei.lpParameters := PChar(parameters); // PAnsiChar;
    sei.nShow := SW_SHOWNORMAL; //Integer;

    Result := ShellExecuteEx(@sei);
end;

procedure e_status(text:string);
begin
    if form_main.m_erweitertefehlermeldungen.Checked then showmessage(text);
end;


{function FileExists(const FileName: string): Boolean;
// This is a corrected version of FileExists(). The one supplied by
// Borland fails if the file doesn't have a valid DOS time (ie, time
// is 1979 etc). This has been a problem with our back-dated files.
var
    Handle: THandle;
    FindData: TWin32FindData;
begin
    Handle := FindFirstFile(PChar(FileName), FindData);
    if Handle <> INVALID_HANDLE_VALUE then begin
        Windows.FindClose(Handle);
        if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
begin
            Result:=True;
            Exit;
        end;
    end;
    Result:=False;
end; }


function FileExists(const filename : string):boolean;
 var
    FFileHandle: THandle;
    A: array[0..512] of Char;
 begin

   StrPCopy(A, filename);
   FFileHandle := CreateFile(A, GENERIC_READ, FILE_SHARE_READ,
        nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
   if FFileHandle <> INVALID_HANDLE_VALUE then
      begin // valid file handle so close the file and return
      closehandle(FFileHandle);
      result := True;
      end
   else result := False;
 end;

procedure oleanzeigen(ole:tolecontainerext);
begin
  try
    pause(10);
    e_status('oleanzeigen vor udateverbs');

    ole.UpdateVerbs;
    e_status('oleanzeigen vor ovshow');


    if pdf_via_dat and ((lowercase(copy(ole.OleClassName,1,8))='acroexch') or (lowercase(copy(ole.OleClassName,1,11))='foxitreader')) then //nur bei pdf
      olecontainer_to_pdf(ole)
      //pdf_anzeigen_vers_datei(ole)
    else
	    ole.DoVerb(ovshow);  // hier als eigener task, wenn nicht nach bestimmter zeit OK dann killen und pdf neu einlesen

  except
    pause(200);
    e_status('oleanzeigen vor udateverbs (except)');
    ole.Run;
    ole.UpdateVerbs;
    e_status('oleanzeigen vor ovshow (except)');
    ole.DoVerb(ovshow);
  end;
end;

procedure tForm_main.check_tables(modus:integer);
var i:integer;
query,tab_name,pfad,r,html_name:string;
felder :array [0..10] of string;
Fehlertext:tstringlist;
begin
try
    if modus<0 then modus :=0;
    //datamodul.sql_new(false,datamodul.q_1,'select tab_name from export_import ','');
    fehlertext:=tstringlist.Create;
    datamodul.sql_new(false,datamodul.q_1,'show tables ','');
    datamodul.q_1.first;
    while not datamodul.q_1.eof do
    begin

    	tab_name:=datamodul.q_1['tables_in_'+db_datenbank];
      form_start.info('�berp�fe Tabelle '+tab_name);
      case modus of
        0:query:='check table ' + tab_name +' quick';
        1:query:='check table ' + tab_name +' medium';
        2:query:='check table ' + tab_name +' extended';
      end;
      application.processmessages;
      mysql_d.Feldarray(query,[0,1,2,3,4,5],felder );

      if felder[3]<>'OK' then
      begin
        query:='repair table ' + tab_name +' extended';
        mysql_d.Feldarray(query,[0,1,2,3,4,5],felder );
        if felder[3]<>'OK' then
          Fehlertext.Add(felder[0]+' / '+felder[1]+' / '+felder[2]+' / '+felder[3]+' / '+felder[4]+' / '+felder[5]+' / ');
        //showmessage(felder[0]+' / '+felder[1]+' / '+felder[2]+' / '+felder[3]+' / '+felder[4]+' / '+felder[5]+' / '+#13+'>> F�hren Sie die Datenbank-Reparatur mit HeidiSQL durch <<');
      end;
      datamodul.q_1.next;
    end;

finally
      if fehlertext.Text<>'' then
      begin
        fehlertext.insert(0, 'Bei der �berpr�fung der Tabellen sind Fehler aufgetreten.');
        fehlertext.insert(1, '');
        fehlertext.insert(2, 'F�hren Sie die Datenbank-Reparatur mit HeidiSQL durch.');
        fehlertext.insert(3, 'www.arbene.de-->Fragen und Antworten->Datenbank / Datensicherheit -->Tabellen reparieren');
        fehlertext.insert(4, 'https://www.arbene.de/index.php/test/datenbank-datensicherheit/64-tabellen-reparieren');
        fehlertext.insert(5, '');
        {fehlertext.insert(6, 'Alternative: Datenbanktools->Tabellen reparieren');
        fehlertext.insert(7, '');}
        fehlertext.insert(6, 'Folgende Tabellen sind betroffen:');

        form_memo:=tform_memo.Create(self);
        form_memo.Caption:='Probleme bei der �berpr�fung der Tabellen';
        form_memo.Memo.ScrollBars:=ssVertical;
        form_memo.Memo.Text:=fehlertext.text;
        if form_memo.ShowModal=mrok then
        begin
          html_name:='https://www.arbene.de/index.php/test/datenbank-datensicherheit/64-tabellen-reparieren';
          shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal); //application.Handle
        end;
        form_memo.Free;
      end;
      fehlertext.Free;
end;
end;

function ma_name(i_ma:int64;c:integer):string;
var query:string;
felder: array[0..2] of string;
begin
if i_ma=0 then exit;

  query:=format('select name, vorname,geb_dat from mitarbeiter where nummer=%s ',[inttostr(i_ma)]);
  mysql_d.Feldarray(query,[0,1,2],felder);
  result:=felder[0];
  if c>=2 then result:=result+', '+felder[1];
  if c>=3 then result:=result+', '+felder[2];

end;



procedure TForm_main.PageControl_firma_einstellungenChanging(
  Sender: TObject; var AllowChange: Boolean);
begin
  speichern;
end;


function StreamToString(aStream: TStream): string;
var
  SS: TStringStream;
begin
  if aStream <> nil then
  begin
    SS := TStringStream.Create('');
    try
      SS.CopyFrom(aStream, 0);  // No need to position at 0 nor provide size
      Result := SS.DataString;
    finally
      SS.Free;
    end;
  end else
  begin
    Result := '';
  end;
end;

procedure TForm_main.Splitter_zeitabrechnungMoved(Sender: TObject);
begin
 
  grid_suchen(TabSheet_sollist);
end;

procedure TForm_main.Panel_zeitabrechnung_liResize(Sender: TObject);
begin
if panel_zeitabrechnung_li.Width<423 then panel_zeitabrechnung_li.Width:=423;
end;


function ShellExecuteErrMessage(int_Code: INTEGER): string;
//Gibt die passenede Fehlermeldung zum ShellExecute Fehlercode zur�ck
begin

 case int_code of
   0: result := 'Zuwenig Speicher, ausf�hrbare Datei war zerst�rt, Relokationswerte waren ung�ltig';
   2: result := 'Datei wurde nicht gefunden.';
   3: result := 'Verzeichnis wurde nicht gefunden.';
   5: result := 'Fehler beim Zugriff auf eine Datei.';
   6: result := 'Bibliothek forderte separate Datensegmente f�r jede Task an.';
   8: result := 'Zuwenig Speicher, um die Anwendung zu starten.';
  10: result := 'Falsche Windows-Version.';
  11: result := 'Ung�ltige ausf�hrbare Datei. Entweder keine Windows-Anwendung oder Fehler in der EXE-Datei.';
  12: result := 'Anwendung f�r ein anderes Betriebssystem.';
  13: result := 'Anwendung f�r MS-DOS 4.0.';
  14: result := 'Typ der ausf�hrbaren Datei unbekannt.';
  15: result := 'Versuch, eine Real-Mode-Anwendung (f�r eine fr�here Windows-Version) zu laden.';
  16: result := 'Versuch, eine zweite Instanz einer ausf�hrbaren Datei mit mehreren Datensegmenten die nicht als nur lesbar gekennzeichnet waren, zu laden.';
  19: result := 'Versuch, eine komprimierte ausf�hrbare Datei zu laden.' + #13 + 'Die Datei mu� dekomprimiert werden, bevor sie geladen werden kann.';
  20: result := 'Ung�ltige dynamische Linkbibliothek (DLL).' + #13 + 'Eine der DLLs, die ben�tigt wurde, um die Anwendung auszuf�hren, war besch�digt.';
  26: result := 'A sharing violation occurred.';
  27: result := 'The filename association is incomplete or invalid.' ;
  28: result := 'The DDE transaction could not be completed ' +#13 +'because the request timed out.';
  29: result := 'The DDE transaction failed.';
  30: result := 'The DDE transaction could not be completed because '+#13+'other DDE transactions were being processed.';
  31: result := 'Es ist keine Anwendung mit der Dateiendung von FileName verkn�pft';

 else
  result := 'Ein Unbekannter Fehler ist aufgetreten. (' + IntToStr(int_code) + ')';
 end;
end;

procedure ShellExecuteErrShow(int_code:integer);
begin
   if int_code<33 then showmessage(ShellExecuteErrMessage(int_Code));

end;

function shellexecute_se(hWnd:HWND; Operation: PAnsichar;FileName: PansiChar; Parameters: PAnsichar; Directory: PAnsichar; ShowCmd:integer):boolean ;
var
f:integer;
message:string;
begin
  result:=true;
  if hwnd=0 then hwnd:=application.Handle; //darf null sein
  f:=shellexecute(hwnd, Operation, FileName,Parameters, Directory,  ShowCmd);
  //f:=31;
  if f<33 then
  begin
    message:= ShellExecuteErrMessage(f)+#13;
    message:=message+#13+'Operation : '+Operation;
    message:=message+#13+'FileName  : '+Filename;
    message:=message+#13+'Parameters: '+Parameters;
    message:=message+#13+'Directory : '+Directory;
    message:=message+#13+'ShowCmd   : '+inttostr(showcmd);
    showmessage(message);
    result:=false;
  end;
end;

procedure TForm_main.RichEdit_Enter(Sender: TObject);
begin
rich_akt:=sender as trichedit;
end;

function TForm_main.weiter(s:string):boolean;
begin
  form_weiter:=tform_weiter.create(self);
  form_weiter.memo.Text:=s;
  if form_weiter.showmodal=mrok then result:=true else result:=false;
  form_weiter.release;
end;


procedure TForm_main.m_CSVImportClick(Sender: TObject);
var  pfad,pfad_alt:string;
    sr: TSearchRec;
begin
try
  findfirst(csv_pfad+'*.csv',faAnyFile,sr);
  if (sr.Name<>'.') and (sr.Name<>'..') then csvimport(csv_pfad+sr.Name);

except
end;
end;


procedure TForm_main.CSVImport(datei:string);
begin

  if fileexists(datei) then
    if not weiter('Ein CSV-Import steht an '+#13#10+'Die aktuell bearbeiteten Daten werden gespeichert'+#13#10+'Bei Abbruch erfolgt ein erneuter Aufruf in 5 Minuten') then exit;

  try


  dbspeichern(false);
  form_csv_import:=tform_csv_import.create(self);
  if fileexists(datei) then
  begin
    form_csv_import.csv_importieren(datei);
    form_csv_import.spaltentitel_angleichen(l_csv_arbene,l_csv ) ;
  end
  else
   form_csv_import.m_ImportierenClick(self) ;

  if form_csv_import.showmodal<>mrok then exit;

    form_importieren:=tform_importieren.create(self);

    form_importieren.csv_einlesen;
    form_importieren.release;
    refresh_all;
    pagecontrol_main.ActivePage:=tabsheet_firma;
    berechtigung_firma;
    sichtbarkeiten_einstellen;
    berechtigungen_einstellen;
    deletefile(datei);
    if fileexists(datei) then showmessage(datei +' konnte nicht gel�scht werden.');
  finally
    form_csv_import.release;
  end;
end;



procedure TForm_main.Timer_csvTimer(Sender: TObject);
var
  sr: TSearchRec;
begin
try
  findfirst(csv_pfad+'*.csv',faAnyFile,sr);
  if (sr.Name<>'.') and (sr.Name<>'..') and (sr.Name<>'') then csvimport(csv_pfad+sr.Name);

except
end;
end;

procedure TForm_main.m_archiv_all_ausgeschiedenClick(Sender: TObject);
var
query,dat:string;
begin
  dat:=sql_datetostr(now());
  query:=format('update mitarbeiter set archiv="X" where ende<=%s  where i_firma in (%s)',[dat,firmen_filter]);
  mysql_d.sql_exe(query);
end;

function Hash32(Data: pointer; Len: integer): cardinal;
function SubHash(P: PCardinalArray; L: integer): cardinal;
{$ifdef HASINLINE}inline;{$endif}
var s1,s2: cardinal;
    i: integer;
const Mask: array[0..3] of cardinal = (0,$ff,$ffff,$ffffff);
begin
  if P<>nil then begin
    s1 := 0;
    s2 := 0;
    for i := 1 to L shr 4 do begin // 16 bytes (4 DWORD) by loop - aligned read
      inc(s1,P^[0]);
      inc(s2,s1);
      inc(s1,P^[1]);
      inc(s2,s1);
      inc(s1,P^[2]);
      inc(s2,s1);
      inc(s1,P^[3]);
      inc(s2,s1);
      inc(PtrUInt(P),16);
    end;
    for i := 1 to (L shr 2)and 3 do begin // 4 bytes (DWORD) by loop
      inc(s1,P^[0]);
      inc(s2,s1);
      inc(PtrUInt(P),4);
    end;
    inc(s1,P^[0] and Mask[L and 3]);      // remaining 0..3 bytes
    inc(s2,s1);
    result := s1 xor (s2 shl 16);
  end else
    result := 0;
end;
begin // use a sub function for better code generation under Delphi
  result := SubHash(Data,Len);
end;

function CompareStreams(const A, B : TStream): Boolean;
const
   BUFSIZE = 8196;
var
   bufA, bufB : string;
   r : Integer;
begin
   if A.Size <> B.Size then
      Result := False
   else
   begin
      A.Position := 0;
      B.Position := 0;

      SetLength(bufA, BUFSIZE);
      SetLength(bufB, BUFSIZE);

      while A.Position < A.Size do
      begin
         r := A.Size - A.Position;
         if r > BUFSIZE then
            r := BUFSIZE;
         A.ReadBuffer(bufA[1], r);
         B.ReadBuffer(bufB[1], r);

         if not CompareMem(PChar(bufA), PChar(bufB), r) then
         begin
            Result := False;
            Exit;
         end;
      end;
      Result := True;
   end;
end;



procedure TForm_main.m_PDFneueinlesenClick(Sender: TObject);
begin
//
  if pdf_anzeigen_vers_datei(arbeitsmedizin_firma,'neu') then
  begin
    pause(100);
    dbspeichern(true);
    //formular_entsperren(true,false);//speichern
  end;
end;

procedure TForm_main.m_PDFneueinlesen1Click(Sender: TObject);
begin
  if pdf_anzeigen_vers_datei(arbeitsmedizin_proband,'neu') then
  begin
    pause(100);
    dbspeichern(true);
    //formular_entsperren(true,false);//speichern
    end;
end;

procedure TForm_main.m_InfomationzumDokumentClick(Sender: TObject);
begin
if not (arbeitsmedizin_firma.State in [osempty] )then oleinfo(arbeitsmedizin_firma) ;
if not (arbeitsmedizin_proband.State in [osempty] )then oleinfo(arbeitsmedizin_proband) ;
end;


function pdf_behandeln(pdf:string):string;
var
exe,befehl,pdf_n:string;
i,j:integer;
begin
screen.Cursor:=crhourglass;
result:=pdf;
warte_bis_datei_lesbar(pdf);
i:=0;
j:=0;
exe:=pfad_pdftk+'pdftk.exe';
if fileexists(exe) then
begin
    repeat
      pdf_n:=pfad_temp+'a_pdf'+sonderzeichenraus_all(datetimetostr(now()));
      pdf_n:= pdf_n+inttostr(i)+'.pdf';
      inc(i);
    until (not fileexists(pdf_n));
    exe:='"'+exe+'"';
    befehl:=format(' A="%s"  cat A  output  "%s"  drop_xfa drop_xmp ',[pdf,pdf_n]);
   // if  runasadmin(application.Handle,exe,befehl)
    //if shellexecute_se(0,'open',pchar(exe),pchar(befehl),nil,SW_MINIMIZE	)
    if run_app(exe,befehl)
    then
      begin
        if not (warte_bis_datei_lesbar(pdf_n)) then showmessage('Die Datei '+pdf +' ist vermutlich passwortgesch�tzt ->Acrobat->STRG+D');
      end
    else
      showmessage( exe+befehl+' wurde nicht ausgef�hrt');
    //executefile(exe,befehl,'',true);
    //shellexecute_se(0,'open',pchar(exe),pchar(befehl),nil,SW_MINIMIZE	);
    if (fileexists(pdf_n)) then result:= pdf_n else result:=pdf;

end;
screen.Cursor:=crdefault;
end;

//storno ///////////////////////////////////

procedure TForm_main.m_rechnungspositonen_wieder_freigebenClick(
  Sender: TObject);
var
query,parameter,parameter_sql: string;
renu,renu_child,rechnungs_nummer,storno_dat:string;
i_rechnung,i_renu:integer;
begin

if datamodul.q_rechnung.FindField('renu_storno').asstring<>'' then
begin
  showmessage('Diese Rechnung wurde schon storniert');
  exit;
end;

if datamodul.q_rechnung.FindField('i_parent').asinteger>0 then
begin
  showmessage('Stornieren Sie die �bergeordnete Rechnung');
  exit;
end;
//    if getbigint(datamodul.q_repositionen,'i_link_rechnung')>0 then  result:=true;
if  Messagedlg('Sollen die Rechnung storniert werden?'+#13#10+'Alle Rechnungspositionen werden zur erneuten Abrechnung frei gegeben.',mtConfirmation, mbOkCancel	,0)<>mrOK then exit;

//schreibe stono in Rechnung , storno_rechnungsnummer
rechnungs_nummer:= datamodul.q_main.findfield('renu_kopf').asstring+datamodul.q_main.findfield('renu').asstring;   //rechnungsnummer
Showmessage ('Es wird eine Stornorechnung mit der Nummer '+rechnungs_nummer+ ' erzeugt.') ;

i_renu:=datamodul.q_main['renu'];
datamodul.q_main.edit;
datamodul.q_main['renu']:=i_renu+1;
datamodul.q_main.post;

storno_dat:=dat_delphi_to_sql(datetostr(date()));

//if getbigint(datamodul.q_repositionen,'i_link_rechnung')>0 then  result:=true
datamodul.q_repositionen.First;
while not datamodul.q_repositionen.Eof do
begin
   renu_child:=datamodul.q_repositionen.findfield('i_link_rechnung').AsString ;
   if renu_child<>'0' then  //f�r alle Kinder
   begin
      // kind_rechnung bekommt aauch storno_rechnungsnummer

      //query:=format('update re_positionen set dauerloesch=2, farbe=7 where i_rechnung=%s and storno=0 and i_link_rechnung=0',[renu_child]);
      query:=format('update re_positionen set dauerloesch=2,re_storno=1,farbe=7 where i_rechnung=%s and storno=0 and i_link_rechnung=0',[renu_child]);
      mysql_d.sql_exe(query);

   end;
   datamodul.q_repositionen.Next;
end;

//kinder
renu:=datamodul.q_rechnung.findfield('nummer').asstring;
parameter:= 'Storno Nummer: '+rechnungs_nummer+#13#10'Storno am: '+datetostr(date())+#13#10'Storno durch: '+ user_name+  #13#10;
parameter_sql:= 'Storno Nummer: '+rechnungs_nummer+chr(13)+'Storno am: '+datetostr(date())+chr(13)+'Storno durch: '+ user_name+  chr(13);

query:=format('update rechnung set renu_storno="%s", datum_re_storno="%s", farbe=1,m_parameter=concat("%s",m_parameter) where i_parent=%s',[rechnungs_nummer,storno_dat,parameter_sql,renu]);
mysql_d.sql_exe(query);

{query:=format('update rechnung set renu_storno="%s", datum_re_storno="%s" where nummer=%s',[rechnungs_nummer,storno_dat,renu]);
mysql_d.sql_exe(query); }

datamodul.q_rechnung.edit;
datamodul.q_rechnung.FindField('farbe').AsInteger:=1;//rot
datamodul.q_rechnung.FindField('renu_storno').asstring:=rechnungs_nummer;
datamodul.q_rechnung.FindField('datum_re_storno').asstring:=datetostr(date());
datamodul.q_rechnung.FindField('m_parameter').asstring:= parameter+ datamodul.q_rechnung.FindField('m_parameter').asstring ;
datamodul.q_rechnung.post;
//dbgridext_rechnung.keine_farbe;
//query:=format('update re_positionen set dauerloesch=2, farbe=7 where i_rechnung=%s and storno=0 and i_link_rechnung=0',[renu]);

query:=format('update re_positionen set dauerloesch=2, re_storno=1,farbe=7 where i_rechnung=%s and storno=0 and i_link_rechnung=0',[renu]);
mysql_d.sql_exe(query);


datamodul.q_repositionen.Refresh;
//datamodul.q_rechnung.FindField()
end;


{
 SELECT 66 & 64
 0,1,2,4,8,16,32,64,128,256,1024,2048,4096
}
function stringbin(dez,bas:integer):string;
var
i,e,z:integer;
h:extended;
begin
result:='';
  for i:=bas-1 downto 0 do
  begin
    h:=i;
    z:= trunc( power(2,h));
    e:=  dez mod z;
    dez:=dez-e*z;
    result:=result+inttostr(e);
  end;
end;

function stringbin_to_integer(s:string):integer;
VAR
i:integer;
h:extended;
begin
result:=0;
  for i:=0 to length(s)-1 do
  begin
    if copy(s,i,1)='1' then
    begin
      h:=i;
      result:=result+trunc(power(2,h));
    end;
  end;

end;

procedure TForm_main.menue_aktivieren;
var i:integer;
begin
toolbar.Enabled:=true;
for i := 0 to MainMenu.Items.Count-1 do MainMenu.Items[i].Enabled:= true;
for i := 0 to popupmenu_texte.Items.Count-1 do popupmenu_texte.Items[i].Enabled:= true;

//popupmenu_dokumente.Items.Enabled:=true;
//popupmenu_kartei.Items.Enabled:=true;
//popupmenu_bereich.Items.Enabled:=true;
end;

procedure TForm_main.menue_deaktivieren;
var i:integer;
begin
toolbar.Enabled:=false;
for i := 0 to MainMenu.Items.Count-1 do MainMenu.Items[i].Enabled:= false;
for i := 0 to popupmenu_texte.Items.Count-1 do popupmenu_texte.Items[i].Enabled:= false;

end;

function copyfile_delphi(quelle, ziel:string):boolean;
var
  mem : TMemoryStream;
begin
result:=false;
if fileexists(quelle) then
begin
  mem := TMemoryStream.Create;
  try
    mem.LoadFromFile(quelle);
    mem.SaveToFile(ziel);
    result:=true;
  finally
    mem.Free;
  end;
end;
end;

procedure TForm_main.SpeedButton_wdv_rechnung_fiClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_rechnung.edit;
		datamodul.q_rechnung['w_dat'] :=int(form_kalender.auswahlkalender.date);
    if datamodul.q_rechnung['w_user']=0 then datamodul.q_rechnung['w_user']:=akt_untersucher;
   end;
end;

procedure TForm_main.SpeedButton23Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		datamodul.q_rechnung.edit;
		datamodul.q_rechnung['w_dat'] :=int(form_kalender.auswahlkalender.date);
      if datamodul.q_rechnung['w_user']=0 then datamodul.q_rechnung['w_user']:=akt_untersucher;
   end;
end;

procedure TForm_main.m_rechnungs_allClick(Sender: TObject);
begin
// Auswahl des Monats
form_rechnung_automatisch:=tform_rechnung_automatisch.create(self);
form_rechnung_automatisch.showmodal;
form_rechnung_automatisch.Free;
end;



procedure TForm_main.BitBtn_labor_hinweise_wahlClick(Sender: TObject);
var
snr:string;
begin
  snr:=datamodul.q_labor.findfield('nummer').asstring;
  form_main.tbs_auswahl('auswahl',3,snr,datamodul.q_hinweis.findfield('name').asstring );
  if datensatz(datamodul.q_hinweis) then dbgrid_labor_hinweise.Visible:=true else  dbgrid_labor_hinweise.Visible:=false;
end;

procedure TForm_main.BitBtn_laborhinweis_automatischClick(Sender: TObject);
var
snr:string;
begin

  snr:=datamodul.q_labor.findfield('nummer').asstring;
  hinweise_einfuegen(3,snr);
  if datensatz(datamodul.q_hinweis) then dbgrid_labor_hinweise.Visible:=true else  dbgrid_labor_hinweise.Visible:=false;
end;

procedure TForm_main.BitBtn_laborhinweis_automatisch_allClick(
  Sender: TObject);
var
snr,sdatum,query,sma:string;
daten:pnodepointer;

begin
  daten:=dbtreeview.Selected.Data;
  sdatum:=dat_delphi_to_sql(  datetostr(daten^.datum));
  sma:=datamodul.q_mitarbeiter.findfield('nummer').asstring;
  query:=format('select labor.nummer from labor where i_mitarbeiter=%s and datum="%s" and storno=0 and i_status=4 ',[sma,sdatum]);
  datamodul.sql_new(false,datamodul.q_8,query,'');
  while not datamodul.q_8.Eof do
    begin
      snr:=datamodul.q_8.findfield('nummer').AsString;
      hinweise_einfuegen(3,snr);
      datamodul.q_8.Next;
    end;
  if datensatz(datamodul.q_hinweis) then dbgrid_labor_hinweise_all.Visible:=true else  dbgrid_labor_hinweise_all.Visible:=false;
end;

procedure tform_main.hinweise_einfuegen(i_tab:integer;s_tab_nummer:string);
var
query, labwert,s_typ, s_tbs:string;
begin

query:='select * from tabelle_hinweise'; //f�r insert
datamodul.sql_new(false,datamodul.q_6,query,'');

//s_tab_nummer ->labor->i_typ   labor->wert
// tabelle_tbs : laborwert liegt innerhalb von i1..i2 oder s1 entspricht grenzwertindikator
labwert:=datamodul.q_labor.findfield('wert').asstring;
s_typ:= datamodul.q_labor.findfield('i_typ').asstring;


query:=format('select labor.i_typ, tabelle_tbs.i_tbs  from labor left join tabelle_tbs on (tabelle_tbs.i_tab_nummer = labor.i_typ) where labor.nummer=%s  ',[s_tab_nummer]);
query:=query+'  and tabelle_tbs.i_tabelle=3 and ((labor.wert>=tabelle_tbs.i1 and labor.wert<=tabelle_tbs.i2 ) or (labor.grenzwertindikator=tabelle_tbs.s1)) and tabelle_tbs.storno=0 ';
datamodul.sql_new(false,datamodul.q_7,query,'');
while not datamodul.q_7.Eof do
begin
  //checken ob besteht
    s_typ:=datamodul.q_7.findfield('i_typ').asstring;
    s_tbs:=datamodul.q_7.findfield('i_tbs').asstring;
      query:=format('select nummer from tabelle_hinweise where  i_tabelle=3 and  i_tab_nummer=%s and i_tbs=%s',[s_tab_nummer,s_tbs]);
      if mysql_d.Feldinhalt(query,0)='' then
        begin
          neuer_datensatz(datamodul.q_6,['nummer','i_tabelle','i_tab_nummer','i_tbs'],[null,3,s_tab_nummer,s_tbs]);
          datamodul.q_hinweis.Refresh;
        end
      else
      begin
         query:=format('update tabelle_hinweise set storno=0 where i_tabelle=3 and  i_tab_nummer=%s and i_tbs=%s',[s_tab_nummer,s_tbs]);
         mysql_d.sql_exe(query);
         datamodul.q_hinweis.Refresh;
      end  ;

  datamodul.q_7.Next;
end;


end;

procedure TForm_main.BitBtn_labor_hinweise_loeschenClick(Sender: TObject);
var query, snr:string;
begin
  snr:=datamodul.q_labor.findfield('nummer').AsString;
  query:=format('update tabelle_hinweise set storno=1 where i_tabelle=3 and  i_tab_nummer=%s ',[snr]);
  mysql_d.sql_exe(query);
  datamodul.q_hinweis.Refresh;

end;



procedure TForm_main.BitBtn_labor_hinweise_loeschen_allClick(
  Sender: TObject);
var
snr,sdatum,query,sma:string;
daten:pnodepointer;

begin
  daten:=dbtreeview.Selected.Data;
  sdatum:=dat_delphi_to_sql(  datetostr(daten^.datum));
  sma:=datamodul.q_mitarbeiter.findfield('nummer').asstring;
  query:=format('select labor.nummer from labor where i_mitarbeiter=%s and datum="%s" and storno=0 and i_status=4 ',[sma,sdatum]);
  datamodul.sql_new(false,datamodul.q_8,query,'');
  while not datamodul.q_8.Eof do
    begin
      snr:=datamodul.q_8.findfield('nummer').AsString;
      query:=format('update tabelle_hinweise set storno=1 where i_tabelle=3 and  i_tab_nummer=%s ',[snr]);
      mysql_d.sql_exe(query);
      datamodul.q_8.Next;
    end;
  datamodul.q_hinweis.Refresh;
  if datensatz(datamodul.q_hinweis) then dbgrid_labor_hinweise_all.Visible:=true else  dbgrid_labor_hinweise_all.Visible:=false;
end;
procedure TForm_main.BitBtn_rechnung_regeln_addClick(Sender: TObject);
var
s_nummer:string;
begin
with datamodul do
begin
  s_nummer:=q_firma.findfield('nummer').asstring;
  neuer_datensatz(q_rechnung_regeln,['nummer','name','i_firma','monat','periode','ust'],[null,'rechnungs_regel',s_nummer,1,1,1]);
  if datensatz(q_rechnung_regeln) then form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=true else  form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=false;
end;
end;

procedure TForm_main.BitBtn_rechnung_regel_delClick(Sender: TObject);
var
query,snr:string;
begin
with datamodul do
begin
  if not (q_rechnung_regeln.state in [dsedit, dsinsert]) then
  begin
    showmessage('nicht im �nderungsmodus');
    exit;
  end;
  q_rechnung_regeln.Post;
  snr:=q_rechnung_regeln.FindField('nummer').asstring;
  query:=format('update rechnung_regeln set storno=1 where nummer=%s',[snr]);
  mysql_d.sql_exe(query);
  q_rechnung_regeln.Refresh;
  q_rechnung_regeln.Edit;
  if datensatz(q_rechnung_regeln) then form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=true else  form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=false;
end;
end;

procedure TForm_main.BitBtn_zeit_timerClick(Sender: TObject);
begin
      form_timer.art('auswahl');
      form_timer.showmodal;

      DBEdit_time_sollistbeginn.text:=timetostr(form_timer.t_beginn);
     DBEdit_Stunden.Text:=inttostr(form_timer.stunden);
     DBEdit_minuten.Text:=inttostr(form_timer.minuten);
end;

procedure TForm_main.zeitkontoChange;
var
query,snr,nummern:string;
begin
if pagecontrol_firma.ActivePage<>tabsheet_sollist then exit;
nummern:='';
 snr:=datamodul.q_firma_sollist.findfield('i_zeitkonto').AsString;
 query:=format ('select i_kriterium from abr_konto_kriterium where i_konto=%s',[snr]);
 datamodul.sql(false,datamodul.q_1,query,'');
 datamodul.q_1.First;

 while not datamodul.q_1.eof do
  begin
    nummern:=nummern+','+datamodul.q_1.findfield('i_kriterium').AsString;
    datamodul.q_1.Next;
  end;
  nummern:=copy(nummern,2,length(nummern)-1);
 if snr<>'' then
      begin
        query:=format(' nummer in (%s) ',[nummern]);
        DBCombo_num_abrechnungskriterium.zwhere:=query;
        DBCombo_num_abrechnungskriterium.lookup_laden;
      end;


end;

procedure TForm_main.DBCombo_num_abrechnungskriteriumEnter(
  Sender: TObject);
var
query,snr,nummern:string;
begin
if pagecontrol_firma.ActivePage<>tabsheet_sollist then exit;
nummern:='';
 snr:=datamodul.q_firma_sollist.findfield('i_zeitkonto').AsString;
 query:=format ('select i_kriterium from abr_konto_kriterium where i_konto=%s and storno=0 ',[snr]);
 datamodul.sql(false,datamodul.q_1,query,'');
 datamodul.q_1.First;
 while not datamodul.q_1.eof do
  begin
    nummern:=nummern+','+datamodul.q_1.findfield('i_kriterium').AsString;
    datamodul.q_1.Next;
  end;
  nummern:=copy(nummern,2,length(nummern)-1);
 if (snr<>'') and (nummern<>'') then
  begin
    query:=format(' nummer in (%s) ',[nummern]);
    DBCombo_num_abrechnungskriterium.zwhere:=query;
    DBCombo_num_abrechnungskriterium.lookup_laden;
  end;

end;



procedure TForm_main.DBCombo_num_abrechnungskriteriumExit(Sender: TObject);
begin
    DBCombo_num_abrechnungskriterium.zwhere:='';
    DBCombo_num_abrechnungskriterium.lookup_laden;
end;

procedure TForm_main.m_ArchivierteProbandenanzeigenClick(Sender: TObject);
begin
 if b.b_string('Anwendung')=-1 then exit;
tmenuitem(sender).checked :=  not tmenuitem(sender).checked;

archiv_show:= tmenuitem(sender).checked;

berechtigung_firma;
berechtigungen_einstellen;
application.ProcessMessages;
PageControl_mainChange(pagecontrol_main);


end;

procedure TForm_main.DBEdit_bes_erledigt_datChange(Sender: TObject);
begin
   if tdbedit(sender).text='01.01.1900' then
	begin
   	tdbedit(sender).visible:=false;
    label34.Visible:=false;
    DBCombo_num_besonderheit_user.Visible:=false;
   end
   else
   begin
   	tdbedit(sender).visible:=true;
      label34.Visible:=true;
      DBCombo_num_besonderheit_user.Visible:=true;
   end;
   //DBEdit_bes_erledigt_dat.OnChange:=
end;

procedure TForm_main.db_edit_change(Sender: TObject);   //eigenes event 
begin
DBEdit_bes_erledigt_datChange(sender);
end;

procedure TForm_main.m_textbausteineinfgen_in_memoClick(Sender: TObject);
begin
if datamodul.q_besonderheiten.State in [dsedit, dsinsert] then form_main.tbs(4,DBMemo_besonderheiten);
end;

procedure TForm_main.DBComboBox_konto_fClick(Sender: TObject);

begin
  try
  konto_gewaehlt:=strtoint(DBComboBox_konto_f.Text);
  except
  konto_gewaehlt:=0;
  end;
end;

end.



