unit a_report;

interface
uses SysUtils, Windows, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls,comctrls,a_main,zdataset,a_data,Variants,StrUtils;



type
	Thtml_report = class(tobject)
	private
	  { Private-Deklarationen }
	  f:textfile;
    tagliste, widthliste:tstringlist;
    css:string;
    maxrows,max_verbinden:integer;
    function  gettag(z:string):string;
    function  getwidth(z:string):string;
    function gettagweitefromclass(d:string):string;
	public
		constructor create(f_name:string);
		destructor free;
		procedure stag(t:string);
		procedure tag(t,text:string);
		procedure absatz;
		procedure zeile(text: string);
		procedure linie;
		procedure t_kopfzeile(def,ueberschrift:string;text,breite:array of string);
		procedure t_kopf(def,ueberschrift:string;breite:array of string);
		procedure t_zeile(def:string;text:array of string);
    procedure t_zeile_f(def:string;text,d:array of string);
		procedure t_def(def,ueberschrift: string);
		procedure titel(text: string);
		procedure text(text: string);
		procedure zeile_ein(t,text:string;ruck:integer);
    procedure memo(text:tstrings; ruck:integer);
    procedure css_ini(css: tstringlist);
    procedure t_css_beginn(spalten,max_v:integer);
    procedure t_css_ende;
    procedure t_css_zeile(def,text:array of string);
    procedure t_css_zeile_header(def,text:array of string);
    function html_text(text:string):string;


end;

type
	thtml_uebersicht=class(tobject)
	private
		r:thtml_report;
	public
	constructor create(f_name:string);
	destructor free;
	procedure report(items:ttreenodes;wahlrubrik:integer;filterfeld,filterwert:string;variante:integer);
end;


type
	thtml_gef_report=class(tobject)
	private
		r:thtml_report;
	public
	constructor create(f_name:string);
	destructor free;
	procedure report(items:ttreenodes;n:integer);
end;


implementation


constructor Thtml_report.create(f_name:string);
begin
  if fileexists(f_name) then
	if not deletefile(pchar(f_name)) then
	begin
		showmessage('Datei '+f_name+' kann nicht gelöscht werden');
		exit;
	end;
	AssignFile(F, f_name);   { Datei ausgewählt }
	Rewrite(F);
  stag('!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN"');
  stag('html');
  stag('head');
  tag('title', 'infotext');
  stag('meta name="generator" content="arbene"');
  stag('/head');
  tagliste:=tstringlist.create;
  widthliste:=tstringlist.create;
end;

destructor thtml_report.free;
begin
  //showmessage('lab5');
	CloseFile(F);
  tagliste.clear;
  tagliste.free;
  widthliste.clear;
  widthliste.free;
  //showmessage('lab6');
end;

procedure thtml_report.stag(t:string);
begin
	writeln(f,'<'+t+'>');
end;

procedure thtml_report.tag(t,text:string);
var zeile:string;
begin
  //if t='' then t:='a';
	zeile:=format('<%0:s> %1:s </%0:s>',[t,text]);
	writeln(f,zeile);
end;

procedure thtml_report.absatz;
begin
	writeln(f,'<p>');
end;

procedure thtml_report.zeile(text:string);
var z:string;
begin
	writeln(f,text+'<br>');
end;

procedure thtml_report.zeile_ein(t,text:string;ruck:integer);
var a,b:string;
begin
   text:=trim(text);
   if text<>'' then
   begin
     if t<>'' then text:=format(' <%0:s> %1:s </%0:s> ',[t,text]);  ///komisch
     text:=format('<p style="margin-left:%1:dpt"> %0:s </p>',[text,ruck]);
     writeln(f,text);
   end;
end;

procedure thtml_report.linie;
begin
	writeln(f,'<hr>');
end;

procedure thtml_report.t_kopfzeile(def,ueberschrift:string;text,breite:array of string);
var i:integer;
	  z,t,br:string;
begin
  //writeln(f, format('<table %s> ',[def]));   //border="1"
   writeln(f,'<table border>');
	 t_def(def,ueberschrift);
   writeln(f,'<colgroup>');
	for i:=0 to high(breite) do
	begin
	  br:=breite[i];
	  z:=format('<col width=%s> ',[br]);
	  writeln(f,z)
	end;
	writeln(f,'</colgroup>') ;
   writeln(f,'<TR>');
	for i:=0 to high(text) do
	begin
	  br:=breite[i];
	  t:=text[i];
	  z:=format('<th> %s </th>',[t]);
	  writeln(f,z)
	end;
	writeln(f,'</TR>') ;
end;


procedure thtml_report.t_kopf(def,ueberschrift:string;breite:array of string);
var i:integer;
	  z,br:string;
begin
	t_def(def,ueberschrift);
	writeln(f,'<colgroup>');
	for i:=0 to high(breite) do
	begin
	  br:=breite[i];
	  z:=format('<col width="%s"> ',[br]);
	  writeln(f,z)
	end;
	writeln(f,'</colgroup>') ;
end;

procedure thtml_report.t_zeile(def:string;text:array of string);
var i:integer;
	  z,t:string;
begin
	writeln(f,'<TR '+def+' >');
	for i:=0 to high(text) do
	begin
	  t:=text[i];
	  z:=format('<td> %s </td>',[t]);
	  writeln(f,z)
	end;
	writeln(f,'</TR>') ;
end;

procedure thtml_report.css_ini(css: tstringlist);//css zeilen
var i:integer;
	  z,t,css_tag,css_weite:string;
begin
	writeln(f,'<style type="text/css">');
  tagliste.Clear;
  widthliste.clear;
	for i:=0 to css.Count-1 do
	begin
	  t:=css[i];
	  z:=format('    %s',[t]);
	  writeln(f,z);
    css_tag:=gettag(z);
    css_weite:=getwidth(z);
    tagliste.add(css_tag);
    widthliste.Add(css_weite)
	end;
	writeln(f,'</style>') ;
end;

function  thtml_report.gettag(z:string):string;
var
p:integer;
rechts,links,s:string;
begin
z:=lowercase(z);
   //suche {
  p:=pos('{',z);
  if p>0 then links:=copy(z,0,p-1) else links:='';
  //rechtes wort
  s:=ReverseString(links);
  s:=trimleft(s);
  p:=pos('.',s);
  if p>0 then  result:=copy(s,0,p-1) else result:=trim(s); 
  
  result:=reversestring(result);

   //suche
end;

function  thtml_report.getwidth(z:string):string;
var
p:integer;
s:string;
begin
z:=lowercase(z);
   //suche {
  p:=pos('width:',z);
  if p>0 then  s:=copy(z,p+6,length(z)) else s:='';
  s:=trimleft(s);
  p:=pos('%',s);
  if p>0 then
    result:=copy(s,0,p-1)
 else
  result:=trim('99')   ;
end;

procedure thtml_report.t_css_beginn(spalten,max_v:integer);
var t:string;
begin
  maxrows:=spalten;
  max_verbinden:=max_v;
  writeln(f,'<table class="tg">') ;
end;

procedure thtml_report.t_css_ende;
var t:string;
begin
 writeln(f,'</table>');
end;

function thtml_report.gettagweitefromclass(d:string):string;
var
p1,p2,i:integer;
s,z:string;
begin
  i:=0;
   p1:=pos('"',d);

   s:=copy(d,p1+1,length(d)-p1-1);
   s:=trim(s);
   i:=tagliste.IndexOf(s);
   if i< widthliste.Count then result:=widthliste.Strings[i] else result:='';
   result:=trim(result);
end;

procedure thtml_report.t_css_zeile(def,text:array of string);
var i,j,c,max,last:integer;
	  css_weite,z,t,d,colspan:string;
    sichtbar:boolean;
begin

  max:=high(text);

	writeln(f,'<TR >');
	for i:=0 to max do
	begin
	  t:=text[i];
    d:=def[i];
    css_weite:=gettagweitefromclass(d);
    if css_weite='0' then sichtbar:=false else sichtbar:=true;
    if i<max_verbinden then
    begin
      j:=i;
      repeat
        inc(j);
      until (j>=max_verbinden) or (text[j]<>'');
      c:=j-i;
    end
    else c:=1;

    if c>1 then colspan:=format('colspan="%d"',[c]) else colspan:='' ;

    if (i>=max_verbinden) or (text[i]<>'') or (i=0) then 
    begin
      z:=format('<td %s %s> %s </td>',[d,colspan,t]);
      if sichtbar then  writeln(f,z);
    end;

	end;
	writeln(f,'</TR>') ;
end;

procedure thtml_report.t_css_zeile_header(def,text:array of string);
var i,max:integer;
	  z,t,d,css_weite:string;
    sichtbar:boolean;
begin

  max:=high(text);
	writeln(f,'<TR >');
	for i:=0 to max do
	begin
	  t:=text[i];
    d:=def[i];
    css_weite:=gettagweitefromclass(d);
    if css_weite='0' then sichtbar:=false else sichtbar:=true;
	  z:=format('<th %s > %s </th>',[d,t]);
    if sichtbar then  writeln(f,z)
	end;
	writeln(f,'</TR>') ;
end;

procedure thtml_report.t_zeile_f(def:string;text,d:array of string);
var i:integer;
	  z,t,fo:string;
begin
	writeln(f,'<TR '+def+' >');
	for i:=0 to high(text) do
	begin
	  t:=text[i];
     fo:=d[i];
	  z:=format('<td %s> %s </td>',[fo,t]);
	  writeln(f,z)
	end;
	writeln(f,'</TR>') ;
end;

procedure thtml_report.t_def(def,ueberschrift: string);
begin
	 if ueberschrift<>'' then writeln(f,format('<caption align=left>%s</caption>',[ueberschrift]));


end;

procedure thtml_report.titel(text: string);
begin
	 writeln(f,'<HTML>') ;
	 writeln(f,'<HEAD>') ;
	 tag('TITLE',text);
	 writeln(f,'</Head>') ;
end;

procedure thtml_report.text(text: string);
begin
	 writeln(f,text) ;
end;


procedure thtml_report.memo(text:tstrings; ruck:integer);
var j:integer;
    t:string;
begin

	 stag('font size=-1');
   t:=format('p style="margin-left:%dpt" style="margin-top:0pt"',[ruck]);
   stag(t);
	 for j:=0 to text.Count-1 do
	 begin
     writeln(f,text[j]+'<br>') ;
	 end;
   stag('/p');
	 stag('/font');
end;



function thtml_report.html_text(text:string):string;
begin
result:=stringreplace(text ,chr(13)+chr(10),'<br>',[rfReplaceAll]);
end;


/////////##########################################################################//////////
constructor thtml_uebersicht.create(f_name:string);
begin
	r:=thtml_report.create(f_name);
end;

destructor thtml_uebersicht.free;
begin
	r.free;
end;




//################################################################################
procedure thtml_uebersicht.report(items:ttreenodes;wahlrubrik:integer;filterfeld,filterwert:string;variante:integer);
//modus 0 normal
//modus 1 geplant
var i,i1,i2,i3,i4,rubrik,ebene,modus:integer;
dbnummer,bereich,i_mitarbeiter:int64;
daten:pnodepointer;
msql_q:tzquery;
datum:tdate;
query,s,s1,s2,s3,s4,s5,s6,s7,s8,filter:string;
r1,r2,r3,r4,r5:real;
lab_auffaellig:boolean;

procedure befmemo_drucken;
var j:integer;
begin
with datamodul do
try
	 form_main.strings_laden(q_2,'memo',form_main.richedit.Lines);
	  //vordefiniertern text laden
	 if form_main.richedit.Text='' then form_main.strings_laden(q_2,'memo_bereich',form_main.richedit.Lines);
   r.memo( form_main.richedit.lines ,15);
except

end;
end;

function html_text(text:string):string;
begin
result:=stringreplace(text ,chr(13)+chr(10),'<br>',[rfReplaceAll]);
end;

procedure dbmemo_drucken(feld:string);
var j:integer;
	  text:string;
begin
with datamodul do
try
	 text:=q_2[feld];
	  //vordefiniertern text laden
	 text:=trim(html_text(text));
   if text<>'' then
   begin
     r.stag('font size=-1');
     r.zeile_ein('',text,15);
     r.stag('/font');
   end;
finally

end;
end;


procedure audiogramm_drucken;
var
wort, sfl, sfr, sdbr, sdbl, spr, spl,weber:string;
i_weber,i,j:integer;

db:array[1..4,1..12] of string;
begin

	wort:=datamodul.q_2['str'];
	i1:=strtoint(copy(wort,1,2));
	case i1 of
	  0:weber:='rechts';
	  1:weber:='medial';
	  2:weber:='links';
	end;
	sfr:=trim(copy(wort,3,4));
	sfl:=trim(copy(wort,7,4));
	sdbr:=trim(copy(wort,11,3));
	sdbl:=trim(copy(wort,14,3));
	spr:=trim(copy(wort,17,3));
	spl:=trim(copy(wort,20,3));

	for i:=1 to 4 do
	begin
	  s:=copy(wort,23+(36*(i-1)),36);
	  for j:=1 to 12 do
	  begin
		 db[i,j]:=trim(copy(s,j*3-2,3));
		 if db[i,j]='-20' then db[i,j]:=' ';
	  end;
	end;

	s:=format( '%s',[s3]);
	r.zeile_ein('',s,5);
	s:='Weber: '+weber;
  r.zeile_ein('',s,10);

  if sdbr+sdbl+spr+spl<>'' then
  begin
    r.t_kopfzeile('border width="250" frame=box align=center','Sisi',[' ','rechts','links'],['100','50','50','50','50']);
	  r.t_zeile('',['Frequenz',sfr,sfl]);
	  r.t_zeile('',['db',sdbr,sdbl]);
	  r.t_zeile('',['%',spr,spl]);
	  r.stag('/table');
  end;

	r.t_kopfzeile('border width="540" frame=box align=center ','Audiogramm',[ '','0,125','0,25','0,5','0,75','1,0','1,5','2,0','3,0','4,0','6,0','8,0','12,0']
								,['180','30','30','30','30','30','30','30','30','30','30','30','30','30']);
		r.t_zeile('',['Rechts Luft',db[2,1],db[2,2],db[2,3],db[2,4],db[2,5],db[2,6],db[2,7],db[2,8],db[2,9],db[2,10],db[2,11],db[2,12]]);
		r.t_zeile('',['Rechts Knochen',db[4,1],db[4,2],db[4,3],db[4,4],db[4,5],db[4,6],db[4,7],db[4,8],db[4,9],db[4,10],db[4,11],db[4,12]]);
		r.t_zeile('',['Links Luft',db[1,1],db[1,2],db[1,3],db[1,4],db[1,5],db[1,6],db[1,7],db[1,8],db[1,9],db[1,10],db[1,11],db[1,12]]);
		r.t_zeile('',['Links Knochen',db[3,1],db[3,2],db[3,3],db[3,4],db[3,5],db[3,6],db[3,7],db[3,8],db[3,9],db[3,10],db[3,11],db[3,12]]);

	r.stag('/table');
	befmemo_drucken;
end;

procedure sehtest_drucken;
var i,j:integer;
st: array[1..3,1..6] of string;
horizontal, vertikal,zg,fs,sts:string;
begin
	  s:=datamodul.q_2['str'];
	  for i:=1 to 3do
			for j:=1 to 6 do
			begin
				  st[i,j]:=copy(s,(i-1)*18+(j-1)*3+1,3);
			end;
	  horizontal:=copy(s,55,5);
	  vertikal:=copy(s,60,5);
	  //checkbox_farbsinn.Checked:=boolean(strtoint(copy(wort,65,1)));
	  zg:= copy(s,66,1);
	  fs:=copy(s,65,1);
	  sts:=copy(s,67,1) ;

	  s:=format( '%s',[s3]);
	  r.zeile_ein('',s,5);

	  r.t_kopfzeile('border width="550" frame=box align=center ','Sehtest',[ '','bi','re','li','Brille bi','Brille re','Brille li'],['100','75','75','75','75','75','75']);
		 r.t_zeile('',['Ferne',st[1,1],st[1,2],st[1,3],st[1,4],st[1,5],st[1,6]]);
		 r.t_zeile('',['Nähe',st[2,1],st[2,2],st[2,3],st[2,4],st[2,5],st[2,6]]);
		 r.t_zeile('',['Bildschirm',st[3,1],st[3,2],st[3,3],st[3,4],st[3,5],st[3,6]]);
	  r.stag('/table');

	  //abwärtskompatibiltät
	  if trim(horizontal)<>'' then
	  begin
		 s:='Horizontale Abweichung: '+horizontal;
		r.zeile_ein('',s,10);
	  end;
	  if trim(vertikal)<>'' then
	  begin
	  s:='Vertikale Abweichung: '+vertikal;
	  r.zeile_ein('',s,10);
	  end;
	  if trim(zg)<>'' then
	  begin
		if zg='1'then s:='Zentrales Gesichtsfeld OK' else s:='Zentrales Gesichtsfeld auffällig';
		r.zeile_ein('',s,10);
	  end;
	  if trim(fs)<>'' then
	  begin
		if fs='1'then s:='Farbsinn OK' else s:='Farbsinn auffällig';
		r.zeile_ein('',s,10);
	  end;
	  if trim(sts)<>'' then
	  begin
		if sts='1'then s:='Stereosehen OK' else s:='Stereosehen auffällig';
		r.zeile_ein('',s,10);
	  end;

	befmemo_drucken;
end;


procedure wert_drucken;
begin
with datamodul do
begin
	//if varisnull(q_2['bezeichner']) then 	s1:='' else    s1:=(q_2['bezeichner']);
    s1:=q_2.findfield('bezeichner').asstring;
	//if varisnull(q_2['bezeichner_dim']) then 	s2:='' else s2:=(q_2['bezeichner_dim']);
    s2:=q_2.findfield('bezeichner_dim').asstring;
	r1:=q_2.findfield('wert').asfloat ;
	i1:=q_2['auffaellig'];
	s:=format( '%s: %s  %s %s',[s3,s1,floattostr(r1),s2]);
	r.zeile_ein('',s,10);
	befmemo_drucken;
end;
end;

procedure liste_drucken;
var i:integer;
 sl,sr:string;
begin
with datamodul do
begin
	if varisnull(q_2['bezeichner']) then 	s1:='' else    s1:=(q_2['bezeichner']);

	if varisnull(q_2['i_berlist']) then 	s2:='' else
		begin
			 i:=q_2['i_berlist'];
			 query:=format('select listenfeld from berlist where nummer=%d',[i]);
			 sl:= mysql_d.Feldinhalt(query,0);

			 i:=q_2['i_berlist_rechts'];
				query:=format('select listenfeld from berlist where nummer=%d',[i]);
			 sr:= mysql_d.Feldinhalt(query,0);
		end;


	i1:=q_2['auffaellig'];

	if q_2['reli']=0 then
	  begin
		s:=format( '%s: %s  %s',[s3,s1,sl]);
	  end
	else
	begin
		s:=format( '%s LINKS: %s  %s    RECHTS: %s',[s3,s1,sl,sr]);
	end;

	r.zeile_ein('',s,10);
	befmemo_drucken;


end;
end;

procedure anmerkung_drucken;
begin

	s:=format( '%s',[s3]);
	r.zeile_ein('',s,5);
	befmemo_drucken;
end;

procedure rr_drucken;
begin
with datamodul do
begin
	if varisnull(q_2['i_1']) then 	i1:=0 else    i1:=(q_2['i_1']);
	if varisnull(q_2['i_2']) then 	i2:=0 else i2:=(q_2['i_2']);
	if varisnull(q_2['i_3']) then 	i3:=0 else i3:=(q_2['i_3']);

	s:=format('%s: RR: %d / %d mmHg Puls: %d', [s3,i1,i2,i3]);
	r.zeile_ein('',s,10);
	befmemo_drucken;
end;
end;

procedure bmi_drucken;
begin
with datamodul do
begin
	if varisnull(q_2['f_1']) then 	r1:=0 else r1:=(q_2['f_1']);
	if varisnull(q_2['f_2']) then 	r2:=0 else r2:=(q_2['f_2']);
	if varisnull(q_2['f_3']) then 	r3:=0 else r3:=(q_2['f_3']);
    if varisnull(q_2['f_4']) then 	r4:=0 else r4:=(q_2['f_4']);
    if varisnull(q_2['f_5']) then 	r5:=0 else r5:=(q_2['f_5']);

	s:=format('%s: Größe: %8.1f cm, Gewicht: %8.1f kg, Bauchumfang: %8.1f,  WHtR: %8.1f, BMI: %8.1f kg/m²', [s3,r1,r2,r4,r5,r3]);

	//s:=format('%s: Größe: %8.1f cm Gewicht: %8.1f kg   BMI: %8.1f kg/m²', [s3,r1,r2,r3]);
	r.zeile_ein('',s,5);
	befmemo_drucken;
end;
end;

procedure lufu_drucken;
begin
with datamodul do
begin
	if varisnull(q_2['f_1']) then 	r1:=0 else r1:=(q_2['f_1']);
	if varisnull(q_2['f_2']) then 	r2:=0 else r2:=(q_2['f_2']);
	if varisnull(q_2['f_3']) then 	r3:=0 else r3:=(q_2['f_3']);
	if varisnull(q_2['f_4']) then 	r4:=0 else r4:=(q_2['f_4']);
	s:=format('%s: FEV1: %8.2f l   FVC: %8.2f l   FEV1/FVC: %8.2f Prozent   PEV: %8.2f l/s  ', [s3,r1,r2,r3,r4]);
	r.zeile_ein('',s,10);
	befmemo_drucken;
end;
end;

procedure wai_drucken;
begin
with datamodul do
begin

	r1:=q_2['wert'];
	i1:=q_2['auffaellig'];
	s:=format( 'Work Ability Index: %s  ',[floattostr(r1)]);
	r.zeile_ein('',s,10);
end;
end;

procedure anamnese_drucken;
var i:integer;
begin
for i:=0 to form_main.akt_editor.Lines.Count-1 do
begin
 r.zeile(form_main.akt_editor.Lines[i]);
end;

end;

/////main###########################################

begin
with datamodul do
begin
i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');


query:=format('select * from diagnosen where i_mitarbeiter=%s',[inttostr(i_mitarbeiter)]);
sql_new(true,q_4,query,'');
query:=format('select * from besonderheiten where i_mitarbeiter=%s',[inttostr(i_mitarbeiter)]);
sql_new(true,q_5,query,'');

if wahlrubrik=0 then
begin
  r.Titel('Übersicht');
  r.stag('BODY');

  s:=format('Befunde für %s %s geb. am %s',[q_mitarbeiter['name'],q_mitarbeiter['vorname'],q_mitarbeiter['geb_dat']]);
  r.tag('h2',s);
  r.linie;
end;

	for i:=0 to items.Count-1 do
	begin
	  daten:=items[i].data;
	  dbnummer:=daten.nummer;
	  bereich:=daten.bereich;
	  rubrik:=daten.rubrik;
	  ebene:=daten.ebene;
	  msql_q:=daten.mysqlquery;
	  datum:=daten.datum;

	  if (ebene<=1) and ((wahlrubrik=0) or (rubrik=wahlrubrik)) then
	  case rubrik of   //befund, vorsorge.....
	  1: begin
		  s:= format('%s Befunde',[datetostr(datum)]);
		  r.tag('h3',s);
			//query:='select akt_untersuchung.*,bereich.name,bereich.bezeichner, bereich.bezeichner_dim ,bereich.modus,bereich.memo as memo_bereich, berlist.listenfeld from akt_untersuchung left join befunde on(akt_untersuchung.i_untersuchung=befunde.nummer) ';
			//query:=query+format(' left join bereich on(akt_untersuchung.i_bereich=bereich.nummer) left join berlist on(akt_untersuchung.i_berlist=berlist.nummer) where akt_untersuchung.i_untersuchung=%s and akt_untersuchung.storno=0 order by bereich.reihenfolge',[inttostr(dbnummer)] );

			query:='select akt_untersuchung.*,bereich.name,bereich.bezeichner, bereich.bezeichner_dim ,bereich.modus,bereich.memo as memo_bereich ,bereich.reli from akt_untersuchung left join befunde on(akt_untersuchung.i_untersuchung=befunde.nummer) ';
			query:=query+format(' left join bereich on(akt_untersuchung.i_bereich=bereich.nummer) where akt_untersuchung.i_untersuchung=%s and akt_untersuchung.storno=0 order by bereich.reihenfolge',[inttostr(dbnummer)] );

			sql_new(false,q_2,query,'');
			q_2.first;
			while not q_2.eof do
			begin
			  if  varisnull(q_2['name']) then s3:='' else s3:=q_2['name'];
			  if  not varisnull(q_2['auffaellig']) then
			  begin
			  if  q_2['auffaellig']=1 then s3:=format('<font color="red"> %s </font>',[s3]) else s3:=format('<font color="blue"> %s </font>',[s3]);
			  end;
			  if q_2['modus'] <>null then modus:=q_2['modus'] else modus:=0;
			  case modus of
			  0:begin
					if q_2['i_bereich']>0 then
					anmerkung_drucken;
				 end;
			  1:	begin  //Zahl
					  wert_drucken;
					end;
			  2:begin  //liste
					 liste_drucken;
				  end;
				3: begin    //audiogramm
				  audiogramm_drucken;
				  end;
				4: begin //sehtest
				  sehtest_drucken;
				  end;
				 5:begin
					 rr_drucken
					end;
				 6: begin //grösse gewicht
					  bmi_drucken;
					 end;
				 7: begin //lufu
						Lufu_drucken;
					 end;
            8: wai_drucken;

				end;
				q_2.next;
			  end;

			  end;
	  2: begin
        if filterfeld<>'' then
        filter:=format(' and untersuchung.%s=%s ',[filterfeld,filterwert])
        else filterfeld:='';
			  query:='select untersuchung.*, typ.untersuchung, beurteilung.text,status.status,untersuchungsart.name  from untersuchung left join typ on(untersuchung.i_typ=typ.nummer) left join beurteilung on (untersuchung.i_beurteilung=beurteilung.nummer)'+
					format(' left join status on (untersuchung.i_status=status.nummer) left join untersuchungsart on (untersuchung.i_uart=untersuchungsart.nummer) where untersuchung.nummer=%s and untersuchung.storno=0 %s',[inttostr(dbnummer),filter]);
			  sql_new(false,q_2,query,'');
			  if varisnull(q_2['untersuchung']) then 	s1:='' else s1:=(q_2['untersuchung']);
			  if varisnull(q_2['text']) then 	s2:='' else s2:=(q_2['text']);
			  if varisnull(q_2['name']) then 	s3:='' else s3:=(q_2['name']);
			  if varisnull(q_2['status']) then 	s4:='' else s4:=(q_2['status']);


			  s:= format('%s Vorsorgeuntersuchung:  %s  (%s)',[datetostr(datum),s1, s4]);
			  r.tag('h3',s);
			  r.zeile_ein('',s3,10);
			  r.zeile_ein('',s2,10);
			  dbmemo_drucken('memo');
          dbmemo_drucken('memo_proband');
		  end;
	  3: begin
    if filterfeld<>'' then
        filter:=format(' and impfung.%s=%s ',[filterfeld,filterwert])
        else filterfeld:='';
			  query:='select impfung.*, typ.untersuchung,impfstoff.impfstoff,impfabfolge.impfabfolge,status.status  from impfung left join typ on(impfung.i_typ=typ.nummer) left join impfstoff on (impfung.i_stoff=impfstoff.nummer)'+
					format(' left join status on (impfung.i_status=status.nummer) left join impfabfolge on (impfung.i_impfabfolge=impfabfolge.nummer) where impfung.nummer=%s and impfung.storno=0 %s',[inttostr(dbnummer),filter]);
			  sql_new(false,q_2,query,'');
        if datensatz(q_2) then
        begin
          if varisnull(q_2['untersuchung']) then 	s1:='' else s1:=(q_2['untersuchung']);
          if varisnull(q_2['impfstoff']) then 	s2:='' else s2:=(q_2['impfstoff']);
          if varisnull(q_2['impfabfolge']) then 	s3:='' else s3:=(q_2['impfabfolge']);
          if varisnull(q_2['status']) then 	s4:='' else s4:=(q_2['status']);
          case variante of
          0: begin
            s:= format('%s:  %s  %s',[ datetostr(datum),s4,s1]);
            r.tag('h2',s);
            if s2<>'' then r.zeile_ein('',format('%s  mit  %s ',[s3,s2]),10);
            dbmemo_drucken('memo');
            end;
          1: begin
               s:= format('%s:  %s  %s',[ datetostr(datum),s3,s1]);
              r.tag('h2',s);
             end;
          end;   
        end;
			end;

	  4: begin
        if filterfeld<>'' then
        filter:=format(' and labor.%s=%s ',[filterfeld,filterwert])
        else filterfeld:='';
			  s:=dat_delphi_to_sql(datetostr(datum));
			  query:='select labor.*, typ.untersuchung,status.status  from labor left join typ on(labor.i_typ=typ.nummer) '+
					format(' left join status on (labor.i_status=status.nummer) where labor.i_mitarbeiter=%s and labor.datum="%s" and labor.storno=0 %s',[inttostr(i_mitarbeiter),s,filter ]);
			  sql_new(false,q_2,query,'');
			  q_2.first;
			  s:= format('%s Labor ',[datetostr(datum)]);
			  r.tag('h3',s);
			  r.t_kopfzeile('border width="90%" frame=box align=center','',['Labor-Parameter','Wert','Einheit','Ergebnistext','','Norm'],['29%','8%','8%','16%','3%','40%']);
        lab_auffaellig:=false;
			  while not q_2.eof do
			  begin
              try
				 if varisnull(q_2['untersuchung']) then 	s1:='' else s1:=q_2.findfield('untersuchung').asstring;
				 if varisnull(q_2['wert']) then 	r1:=0 else r1:=q_2.findfield('wert').asfloat;
				 if varisnull(q_2['einheit']) then 	s2:='' else s2:=q_2.findfield('einheit').asstring;
				 if varisnull(q_2['ergebnistext']) then 	s3:='' else s3:=html_text(q_2.findfield('ergebnistext').asstring);
				 if varisnull(q_2['grenzwertindikator']) then 	s4:='' else s4:=q_2.findfield('grenzwertindikator').asstring;
         if s4<>'' then lab_auffaellig:=true;
				 if varisnull(q_2['norm']) then 	s5:='' else s5:=html_text(q_2.findfield('norm').asstring);
				 //if varisnull(q_2['testbezogenerhinweis']) then 	s6:='' else s6:=html_text(q_2['testbezogenerhinweis']);
				 //if varisnull(q_2['status']) then 	s7:='' else s7:=(q_2['status']);
            //r.t_zeile('',[s1,floattostr(r1),s2,s3,s4,s5,s6,s7]);

				 r.t_zeile('',[s1,floattostr(r1),s2,s3,s4,s5]);
               finally
				 q_2.next;
               end;
			  end;
			  r.stag('/table');
        r.zeile('');
        if lab_auffaellig then
          r.zeile('Mindestens ein Wert liegt nicht im Normbereich.')
        else
        r.zeile('Alle Werte liegen im Normbereich.');

		  end;
	  5: begin
				query:='select ambulanz.*  from ambulanz left join arbeitsunfall on(ambulanz.nummer=arbeitsunfall.nummer)'+
					format(' where ambulanz.nummer=%s ',[inttostr(dbnummer)]);
			  sql_new(false,q_2,query,'');
			  if varisnull(q_2['rrs']) then 	i1:=0 else i1:=(q_2['rrs']);
			  if varisnull(q_2['rrd']) then 	i2:=0 else i2:=(q_2['rrd']);
			  if varisnull(q_2['freq']) then  i3:=0 else i3:=(q_2['freq']);
			  if varisnull(q_2['bz']) then 	i4:=0 else i4:=(q_2['bz']);

			  if varisnull(q_2['icd']) then 	s1:='' else s1:=(q_2['icd']);
			  if varisnull(q_2['icd_text']) then  s2:='' else s2:=(q_2['icd_text']);

			  if varisnull(q_2['s00']) then 	s3:='' else s3:=html_text(q_2['s00']);
			  if varisnull(q_2['s10']) then 	s4:='' else s4:=html_text(q_2['s10']);
			  if varisnull(q_2['s20']) then 	s5:='' else s5:=html_text(q_2['s20']);
			  if varisnull(q_2['s01']) then 	s6:='' else s6:=html_text(q_2['s01']);
			  if varisnull(q_2['s11']) then 	s7:='' else s7:=html_text(q_2['s11']);
			  if varisnull(q_2['s21']) then 	s8:='' else s8:=html_text(q_2['s21']);

			  s:= format('%s Ambulanz',[datetostr(datum)]);
			  r.tag('h3',s);


			  r.t_kopfzeile('border width="90%" frame=box align=center','Vitaldaten',['RR systolisch','RR Diastolisch','Frequenz','Blutzucker'],['25%','25%','25%','25%']);
			  r.t_zeile('',[inttostr(i1),inttostr(i2),inttostr(i3),inttostr(i4)]);
			  r.stag('/table');

			  if trim(s3+s4+s5+s6+s7+s8)<>'' then
			  begin
				 r.t_kopfzeile('border width="90%" frame=box align=center','Verletzung',['Lokalisation','Verletzung / Erkrankung','Versorgung'],['34%','33%','33%']);
				 r.t_zeile('',[s3,s4,s5]);
				 r.t_zeile('',[s6,s7,s8]);
				 r.stag('/table');
			  end;
		  end;
	  6: begin
			  query:='select *  from diagnosen '+
					format(' where diagnosen.nummer=%s ',[inttostr(dbnummer)]);
			  sql_new(false,q_2,query,'');
			  if varisnull(q_2['icd']) then 	s1:='' else s1:=(q_2['icd']);
			  if varisnull(q_2['icd_text']) then  s2:='' else s2:=(q_2['icd_text']);
			  s:= format('%s Diagnosen:  %s  %s ',[datetostr(datum),s1,s2]);
			  r.tag('h3',s);
			  dbmemo_drucken('memo');

		  end;
	  7: begin
			  query:='select besonderheiten.*, besonderheitenliste.besonderheit  from besonderheiten left join besonderheitenliste on (besonderheiten.i_besonderheit=besonderheitenliste.nummer) '+
					format(' where besonderheiten.nummer=%s ',[inttostr(dbnummer)]);
			  sql_new(false,q_2,query,'');
			  if varisnull(q_2['besonderheit']) then 	s1:='' else s1:=(q_2['besonderheit']);

			  s:= format('%s Besonderheit:  %s ',[datetostr(datum),s1]);
			  r.tag('h3',s);
			  dbmemo_drucken('memo');
		  end;
	  8: begin
			  query:='select dokumente.titel from dokumente '+
					format(' where dokumente.nummer=%s ',[inttostr(dbnummer)]);
			  sql_new(false,q_2,query,'');
			  if varisnull(q_2['titel']) then 	s1:='' else s1:=(q_2['titel']);

			  s:= format('%s Dokument:  %s ',[datetostr(datum),s1]);
			  r.tag('h3',s);
		  end;

     15: begin
          r.tag('h3','Allgemeine Anamnese');
          anamnese_drucken;
         end;
	  end;
 end;
r.stag('/BODY');
r.stag('/HTML');
end;
end;










//############################################################################################

constructor thtml_gef_report.create(f_name:string);
begin
	r:=thtml_report.create(f_name);
end;

destructor thtml_gef_report.free;
begin
	r.free;
end;


procedure thtml_gef_report.report(items:ttreenodes;n:integer);
var
  e1,e2,e3,e4,em,ic,i,i1,i2,i3,i4,rubrik,ebene,modus:integer;
  dbnummer,bereich,i_mitarbeiter:int64;
  daten:pnodepointer;
  msql_q:tzquery;
  datum:tdate;
  query,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,ap1,ap2,ap3,abt1,abt2:string;
  r1,r2,r3,r4:real;
  sl1,sl2,sl3,sl4,sl5:tstringlist;
  a: array[0..3] of string;
  b:boolean;

procedure ueberschrift;
begin
end;

procedure abteilung;
begin
s1:=datamodul.q_ap3['kuerzel'];
s2:=datamodul.q_ap3['langtext'];
if datamodul.q_ap3['memo']<>null then sl1.Text :=datamodul.q_ap3['memo'];

if (abt1<>s1) or (abt2<>sl1.text) then
begin
  r.zeile_ein('b',format(' %s , %s ',[s1,s2]),e1); //Abteilung
  r.memo(sl1,e1+em);
  abt1:=s1;
  abt2:=sl1.text;
  //bei neuer Abteilung neuen Arbeitsplatz
  ap1:='';
end;
end;

procedure arbeitsplatz;
begin
if datamodul.q_ap4['name']<>null then s3:=datamodul.q_ap4['name'];
if datamodul.q_ap4['memo_speziell']<>null then sl3.Text :=datamodul.q_ap4['memo_speziell'];

if (ap1<>s3)  or (ap3<>sl3.text) then
begin
  r.zeile_ein('b',format(' %s',[s3]),e2); //Arbeisplatz
  r.memo(sl3,e2+em);
  ap1:=s3;
  ap3:=sl3.text;
end;
end;

procedure projekt;
var
einzug:integer;
begin
if (datamodul.q_ap10['b_abgeschlossen']>0) then exit;
     if datamodul.q_ap10['datum']<>null then s6:=datetostr(datamodul.q_ap10['datum']) else s6:='';
     if datamodul.q_ap10['s_pr_nummer']<>null then s7:=datamodul.q_ap10['s_pr_nummer'] else s7:='';
     if datamodul.q_ap10['verantwortlicher']<>null then s8:=datamodul.q_ap10['verantwortlicher'] else s8:='';
     if datamodul.q_ap10['dat_erledigen']<>null then s9:=datetostr(datamodul.q_ap10['dat_erledigen']) else s9:='';
     if datamodul.q_ap10['b_abgeschlossen']<>null then
         begin
              if datamodul.q_ap10['b_abgeschlossen']=1 then s10:='Projekt abgeschlossen' else s10:='Projekt offen'
         end
         else s10:='Projekt offen';
      if datamodul.q_ap10['projektname']<>null then s3:=datamodul.q_ap10['projektname'];
      if datamodul.q_ap10['memo']<>null  then sl4.Text :=datamodul.q_ap10['memo'];
      i1:=datamodul.q_ap10['i_handlungsbedarf'];
      case ebene of
        1:einzug:=e2;
        2:einzug:=e3;
        3:einzug:=e4;
      end;
      r.zeile_ein('b',format('%s   ==> Handlungsbedarf Priorität: %s',[s3,a[i1]]),einzug);
      r.zeile_ein('',format('Datum: %s    Projektnummer: %s   Verantwortlicher: %s    zu erledigen bis: %s  %s',[s6,s7,s8,s9,s10]),einzug+em);
      r.memo(sl4,einzug+em+em);

end;


procedure gefaehrdungsfaktor;
begin

        if datamodul.q_ap5['name']<>null then s3:=datamodul.q_ap5['name'];
        if datamodul.q_ap5['memo_speziell']<>null  then sl4.Text :=datamodul.q_ap5['memo_speziell'];

        r.zeile_ein('b',format('Gefährdungsgruppe: %s ',[s3]),e3);
        r.memo(sl4,e3+em);
end;

procedure gefaehrdung;
begin
               sl1.Text:=datamodul.q_ap6['memo_gef'];
               sl2.text:=datamodul.q_ap6['memo_massnahme'];
               r.zeile_ein('b','Gefährdung:',e4);
               r.memo(sl1,e4+em);
               r.memo(sl2,e4+em);
end;

procedure ast_vorne;

begin
case ebene of
1:
  begin
       abteilung;
  end;
2:
  begin
       abteilung;
       arbeitsplatz;
  end;
3:
  begin
       abteilung;
       arbeitsplatz;
       gefaehrdungsfaktor;
  end;
end;
end;

procedure ast_hinten;
var e_start:integer;
begin
    e_start:=ebene;
    inc(i);
    daten:=items[i].data;
	  dbnummer:=daten.nummer;
	  ebene:=daten.ebene;
	  msql_q:=daten.mysqlquery;
	  datum:=daten.datum;

while (ebene >e_start) and (i<ic) do
begin
    if msql_q=datamodul.q_ap3 then
    begin
       //datamodul.q_ap3.Locate('nummer',inttostr(dbnummer),[]);
       datamodul.q_ap3.Locate('nummer',(dbnummer),[]);
       abteilung;
    end;

    if msql_q=datamodul.q_ap4 then
    begin
       //datamodul.q_ap4.Locate('nummer',inttostr(dbnummer),[]);
       datamodul.q_ap4.Locate('nummer',(dbnummer),[]);
       arbeitsplatz;
    end;

    if msql_q=datamodul.q_ap5 then
    begin
       // gefährdungsfaktor
       //datamodul.q_ap5.Locate('nummer',inttostr(dbnummer),[]);
       datamodul.q_ap5.Locate('nummer',(dbnummer),[]);
       gefaehrdungsfaktor;
    end;
    if msql_q=datamodul.q_ap6 then
    begin
       // gefährdung
        //datamodul.q_ap6.Locate('nummer',inttostr(dbnummer),[]);
        datamodul.q_ap6.Locate('nummer',(dbnummer),[]);
        gefaehrdung;
   end;
   if msql_q=datamodul.q_ap10 then
    begin
       // gefährdungsfaktor
        //datamodul.q_ap10.Locate('nummer',inttostr(dbnummer),[]);
        datamodul.q_ap10.Locate('nummer',(dbnummer),[]);
        projekt
    end;

   inc(i);

   if i>=ic then exit;

   daten:=items[i].data;
   dbnummer:=daten.nummer;
	 ebene:=daten.ebene;
	 msql_q:=daten.mysqlquery;
	 datum:=daten.datum;
end;
end;

///main
begin
try
  e1:=2;
  e2:=10;
  e3:=18;
  e4:=26;
  em:=4;
  a[0]:='kein Handlungsbedarf ';
  a[1]:='C';
  a[2]:='B';
  a[3]:='A';
  sl1:=tstringlist.Create;
  sl2:=tstringlist.Create;
  sl3:=tstringlist.Create;
  sl4:=tstringlist.Create;
  sl5:=tstringlist.Create;

  case n of
    0:s2:='Alle Gefährdungen mit Handlungsbedarf';
    1:s2:='Gefährdungen mit Priorität C';
    2:s2:='Gefährdungen mit Priorität B';
    3:s2:='Gefährdungen mit Priorität A';
    4:s2:='Alle Abteilungen, Arbeitsplätze, Gefährdungen, Projekte ';
  end;

  r.Titel('Übersicht');
  r.stag('BODY');

  s1:=format('%s für %s' ,[s2,datamodul.q_firma['firma']]);
  r.tag('h2',s1);
  r.linie;
  i:=0;
  ic:=items.Count;
	while i< ic do
	begin
	  daten:=items[i].data;
	  dbnummer:=daten.nummer;
	  ebene:=daten.ebene;
	  msql_q:=daten.mysqlquery;
	  datum:=daten.datum;

    if msql_q=datamodul.q_ap3 then
    begin
       //datamodul.q_ap3.Locate('nummer',inttostr(dbnummer),[]);
       datamodul.q_ap10.Locate('nummer',(dbnummer),[]);
       //inc(i);
       if n=4 then abteilung;
    end;

    if msql_q=datamodul.q_ap4 then
    begin
      //datamodul.q_ap4.Locate('nummer',inttostr(dbnummer),[]);
      datamodul.q_ap4.Locate('nummer',(dbnummer),[]);
       //inc(i);
       if n=4 then arbeitsplatz;
    end;

    if msql_q=datamodul.q_ap5 then
    begin
       // gefährdungsfaktor
        //datamodul.q_ap5.Locate('nummer',inttostr(dbnummer),[]);
        datamodul.q_ap5.Locate('nummer',(dbnummer),[]);
        //inc(i);
        if n=4 then gefaehrdungsfaktor;


    end;

    if msql_q=datamodul.q_ap6 then
    begin
       // gefährdungsfaktor
        //datamodul.q_ap6.Locate('nummer',inttostr(dbnummer),[]);
        datamodul.q_ap6.Locate('nummer',(dbnummer),[]);
        //inc(i);
        if n=4 then gefaehrdung;
   end;
   if msql_q=datamodul.q_ap10 then
    begin
       // gefährdungsfaktor
        //datamodul.q_ap10.Locate('nummer',inttostr(dbnummer),[]);
        datamodul.q_ap10.Locate('nummer',(dbnummer),[]);
        //inc(i);
        if n=4 then projekt
        else
        begin
             i1:=datamodul.q_ap10['i_handlungsbedarf'];
             case n of
                  0:b:=i1>0;
                  4:b:=true;
                  else b:=i1=n;
             end;
             if (b and (datamodul.q_ap10['b_abgeschlossen']=0)) then
             begin
             ast_vorne;//schon positioniert
             projekt;
             ast_hinten; // noch positioneiere
             dec(i);
             end;
        end;
    end;

   inc(i);
  end;

  r.stag('/BODY');
  r.stag('/HTML');
finally
       sl1.Free;
       sl2.Free;
       sl3.Free;
       sl4.Free;
       sl5.Free;
end;
end;



end.
