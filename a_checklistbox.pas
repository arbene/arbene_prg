unit a_checklistbox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls, Grids, Aligrid;

type
  TForm_checklistbox = class(TForm)
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    StringGrid: TStringAlignGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_checklistbox: TForm_checklistbox;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_checklistbox.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));

end;

end.
