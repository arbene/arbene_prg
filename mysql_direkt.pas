unit mysql_direkt;

interface
uses SysUtils, Windows, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Menus, ComCtrls, ClipBrd,
	ToolWin ,RichEdit, ShellAPI, zcompatibility,ZDbcDbLibStatement,ZDbcDBLib, ZDbcIntfs,db,ZDataset,dbtreeview,dbtreeview_kaskade;

type
	Tmysql_d = class(tobject)
	private
	  { Private-Deklarationen }
  		Connection_d: IZConnection;
  		ResultSet: IZResultSet;
  		LastRowNr: Integer;//This is to detect row nr change
  		InsertState: Boolean;
      besitzer: TComponent;
      procedure protokoll(art, funk, aktion: string; wf:boolean);
    public
		 ds_neu_nr: int64;
     ds_neu_tab:string;
     baum:tdbtreeview;
     baum_k:tdbtreeview_k;
     no_transaction:boolean;
     loeschabfrage:boolean;
     keine_abfrage:boolean;
     ds_aenderung,ds_geloescht:boolean;
     hash1,hash2:int64;
		constructor create(AOwner: TComponent;abaum:tdbtreeview);//(host,protocol,port,db,user,pass:string);

		function neue_nummer(tabelle:string):int64;

		function ds_sperren(tabelle_name:string; nummer:int64;user_id:integer):boolean; //schreiben in die Sperrtabelle
		function ds_entsperren(tabelle_name:string; nummer:int64;user_id:integer;tabelle:tzquery):boolean; //freigebne in der Sperrtabelle
    procedure aktualisieren(tabelle_name:string;tabelle:tzquery);
		function sql_exe(sql:string):boolean;
    function sql_exe_still(sql:string):boolean;
		function sql_exe_grant(sql:string):boolean;
		function sql_exe_rowsvorhanden(sql:string):boolean;
		function Feldinhalt(sql:string;nummer:integer):string;
		function sql_pfad(pfad:string):string;
		procedure Feldarray(sql:string;nummer:array of integer; var result: array of string);
		function mysql_getbyindex(tabelle,indexfeld,index,feldname:string):string;
		function get_user(nummer:integer):string;
     function connect(host,protocol,port,db,user,pass:string):boolean;
     function disconnect:boolean;
     procedure zeile(sql:string;liste:tstringlist);
     function zeilen_zahl(sql:string):integer;
     function db_von_db(quelle,ziel:string;leer:boolean):boolean;
     function struktur_von_db (quelle, ziel:string): boolean;
     function struktur_von_db_history (quelle, ziel:string): boolean;
     function create_sql_von_db (quelle:string): string;
     function drop_table(tab_name:string):boolean;
     function anzahl_datensaetze (tabelle:tzquery):string;
     function history_von(quelle,ziel,nummer:string):boolean;
     function history_von2(quelle,ziel,nummer:string):boolean;
     function replace_query(quelle,ziel,wo:string):string;
     procedure start_transaction(tabelle_name:string; nummer:int64);
    function stop_transaction(tabelle_name:string; nummer:int64;tabelle:tzquery) :boolean;
	end;
implementation
uses
a_main,a_data,a_memo;

constructor Tmysql_d.create(AOwner: TComponent;abaum:tdbtreeview);//(host,protocol,port,db,user,pass:string);
begin
inherited create;
besitzer:=aowner;
baum:=abaum;
ds_geloescht:=false;
end;



       

function Tmysql_d.get_user(nummer:integer):string;
var query:string;
begin
	query:='select name from schluessel where nummer='+inttostr(nummer);
	result:=trim(feldinhalt(query,0));

end;




function tmysql_d.sql_pfad(pfad:string):string;
var
position:integer;
begin
position:=pos('\',pfad);
while position>0 do
begin
	delete(pfad,position,1);
	insert('/',pfad,position);
  position:=pos('\',pfad);
end;
result:=pfad;
end;


function tmysql_d.mysql_getbyindex(tabelle,indexfeld,index,feldname:string):string;
var query:string;
begin
query:=format('select %s from %s where %s="%s"',[feldname, tabelle, indexfeld, index]);
result:=feldinhalt(query,0);
end;

function tmysql_d.connect(host,protocol,port,db,user,pass:string):boolean;
var
  Url: string;
begin
  if db_port <> '' then
  begin
    Url := Format('zdbc:%s://%s:%s/%s?UID=%s;PWD=%s', [protocol, host,
      port, db, user, pass]);
  end
  else
  begin
    Url := Format('zdbc:%s://%s/%s?UID=%s;PWD=%s', [protocol, host,
      db, user, pass]);
  end;
  Connection_d :=DriverManager.GetConnectionWithParams(Url, nil);
  Connection_d.SetAutoCommit(True);
  Connection_d.SetTransactionIsolation(tiReadCommitted);
  Connection_d.Open;
end;


function tmysql_d.disconnect:boolean;
begin
if Assigned(self.Connection_d) then
    if not self.Connection_d.IsClosed then
      self.Connection_d.Close;
end;

procedure Tmysql_d.zeile(sql:string;liste:tstringlist);
var
Statement: IZStatement;
//zResultSet: IZResultSet;
i:integer;
value:string;
begin
try
  sql_normieren(sql);
  Statement := Connection_d.CreateStatement;  //##############
  //This is not neccesseary if you do not want to modify the data
 // Statement.SetResultSetConcurrency(rcUpdatable);
  ResultSet := Statement.ExecuteQuery(sql);
  //Was any resultset returned?
  if Assigned(ResultSet) then
  begin
      //nur erste Zeile
      if resultset.Next=false then
      begin
           liste.Add('');
           exit;
      end;

      for I := 1 to ResultSet.GetMetadata.GetColumnCount do
      begin

	  //read out the proper value for the column
        try
        if ResultSet.IsNull(I) then  Value := ''
        else
          case ResultSet.GetMetadata.GetColumnType(I) of
            stBoolean: if ResultSet.GetBoolean(I) then Value := 'True' else Value := 'False';
            stByte: Value := IntToStr(ResultSet.GetByte(I));
            stShort: Value := IntToStr(ResultSet.GetShort(I));
            stInteger: Value := IntToStr(ResultSet.GetInt(I));
            stLong: Value := IntToStr(ResultSet.GetLong(I));
            stFloat: Value := FloatToStr(ResultSet.GetFloat(I));
            stDouble: Value := FloatToStr(ResultSet.GetDouble(I));
            stBigDecimal: Value := FloatToStr(ResultSet.GetBigDecimal(I));
            stBytes, stBinaryStream: Value := ResultSet.GetBlob(I).GetString;
            stDate: Value := DateToStr(ResultSet.GetDate(I));
            stTime: Value := TimeToStr(ResultSet.GetTime(I));
            stTimeStamp: Value := DateTimeToStr(ResultSet.GetTimeStamp(I));
            stAsciiStream, stUnicodeStream: Value := ResultSet.GetBlob(I).GetString;
          else
            Value := ResultSet.GetString(I);
          end;


        except
        Value := ''
        end;

        liste.Add( Value);
      end;
  end;
finally
  statement.Close;
end;
end;

function Tmysql_d.Feldinhalt(sql:string;nummer:integer):string;
var
 liste:tstringlist;
begin
try
 liste:=tstringlist.create;
 zeile(sql,liste);
 result:=liste.Strings[nummer];
finally
 liste.Free;
end;
end;


procedure Tmysql_d.Feldarray(sql:string;nummer:array of integer; var result: array of string);
var
 liste:tstringlist;
 i:integer;
begin
try
 liste:=tstringlist.create;
 zeile(sql,liste);
 for i:=0 to liste.count-1 do result[i]:=liste.Strings[nummer[i]];
finally
 liste.Free;
end;
end;

function Tmysql_d.sql_exe_rowsvorhanden(sql:string):boolean;
begin
     if feldinhalt(sql,0)='' then result:=false else result:=true;
end;

function Tmysql_d.zeilen_zahl(sql:string):integer;
var
Statement: IZStatement;
i,j:integer;
value:string;
begin
  sql_normieren(sql);
	Statement := Connection_d.CreateStatement;
  ResultSet := Statement.ExecuteQuery(sql);
  resultset.last;
  result:=resultset.GetRow;

  statement.Close;

end;


function Tmysql_d.sql_exe(sql:string):boolean;
var
i:integer;
begin

 sql_normieren(sql);
 datamodul.sql_processor.Clear;
 datamodul.sql_processor.Script.Clear ;
 if pos('#',sql) >0 then //wenn zeilenumbruch
 begin
  try
    form_memo:=tform_memo.create(form_main);
    form_memo.Memo.Text:=sql;
    //showmessage (sql);
    form_memo.Caption:=' Aktualisierung der Tabellen mit:';
    form_memo.BitBtn_abb.Visible:=false;
    form_memo.ShowModal;
   finally
    form_memo.Release;
   end;
 end;
 datamodul.sql_processor.Script.Add(sql);
 try

 	datamodul.sql_processor.Execute;
 	result:=true;
 except
   if modus_debug then showmessage(sql+#10+'konnte nicht ausgef�hrt werden');
 	result:=false;  //hier bei  fehler
 end;
end;

function Tmysql_d.sql_exe_still(sql:string):boolean;
var
i:integer;
begin

 sql_normieren(sql);
 datamodul.sql_processor.Clear;
 datamodul.sql_processor.Script.Clear ;
 if pos('#',sql) >0 then //wenn zeilenumbruch
 begin
  try
    form_memo:=tform_memo.create(form_main);
    form_memo.Memo.Text:=sql;
    //showmessage (sql);
    form_memo.Caption:=' Aktualisierung der Tabellen mit:';
    form_memo.BitBtn_abb.Visible:=false;
    form_memo.ShowModal;
   finally
    form_memo.Release;
   end;
 end;
 datamodul.sql_processor.Script.Add(sql);
 try

 	datamodul.sql_processor.Execute;
 	result:=true;
 except
  if modus_debug then showmessage(sql+#10+'konnte nicht ausgef�hrt werden');
   result:=false;  //hier bei  fehler
 end;
end;

function Tmysql_d.sql_exe_grant(sql:string):boolean;
begin
try
   sql_normieren(sql);
   datamodul.connection_grant.Connect;
   datamodul.sql_processor_grant.Clear;
   datamodul.sql_processor_grant.Script.Clear ;
   datamodul.sql_processor_grant.Script.Add(sql);

   datamodul.sql_processor_grant.Execute;
   result:=true;
 except
   if modus_debug then showmessage(sql+#10+'konnte nicht ausgef�hrt werden');
   result:=false;  //hier bei  fehler
 end;
 try
  datamodul.connection_grant.Disconnect;
 except
 end; 
end;


function Tmysql_d.ds_entsperren(tabelle_name:string; nummer:int64;user_id:integer;tabelle:tzquery):boolean;
var
	sql,s,tabelle_hk:string;

begin
	result:=false;
	tabelle_hk:=hk+tabelle_name+hk;
  sql:='select count(*) from sperren where sperrobjekt= '+tabelle_hk+' and sperrobjekt_id= '+inttostr(nummer)+' and user_id= '+inttostr(user_id);
  s:=feldinhalt(sql,0);
  if s<>'0' then   //###speichern###
    begin
      // ###speichern### hier commit
     // datamodul.connection_main.Commit;

      if tabelle<>nil then     //beim beenden ist tabelle nil
        if stop_transaction(tabelle_name,nummer,tabelle) then aktualisieren(tabelle_name,tabelle);

	    sql:='delete from sperren where sperrobjekt= '+tabelle_hk+' and sperrobjekt_id= '+inttostr(nummer)+' and user_id= '+inttostr(user_id);
      sql_exe(sql);
      result:=true;
    end
  else
    begin
      loeschabfrage:=false;
      keine_abfrage:=false;
      result:=false ;
    end;
end;


function Tmysql_d.ds_sperren(tabelle_name:string; nummer:int64; user_id:integer):boolean;
var
	query,sql,tabelle_hk:string;

begin
  if tabelle_name='history' then
  begin
    result:=true;
    exit;
  end;
	result:=false;
	tabelle_hk:=hk+tabelle_name+hk;
   query:=format('select sperrobjekt from sperren where (sperrobjekt=%s and sperrobjekt_id=%d and user_id=%d )',[tabelle_hk,nummer,user_id]);
   try
      if mysql_d.Feldinhalt(query,0)='' then
      begin
         //###speichern### hier start transaction wenn noch nicht geschehen (neu)
         start_transaction(tabelle_name,nummer);
         sql:=format('insert into sperren values (%s ,%d,%d)',[tabelle_hk,nummer,user_id]);    //index �ber tabelle nummer user_id
         if sql_exe(sql) then result :=true
      end;
   except
    
   	result:=false;
   end;
end;

procedure tmysql_d.aktualisieren(tabelle_name:string;tabelle:tzquery);
var s_nummer:string;
found:boolean;
begin
   s_nummer:=tabelle.findfield('nummer').asstring;
   tabelle.refresh;
   found:=tabelle.Locate('nummer',s_nummer,[]);
   if pos(tabelle_name,'befund,labor,untersuchung,impfung,ambulanz,dokumente,diagnosen,besonderheiten,kontakte,artikel')>0 then
   begin
    //if found then baum.aktualisieren else baum.liste_lesen;
    baum.aktualisieren;
   end;
   if baum_k<>nil then baum_k.aktualisieren;
end;

function Tmysql_d.neue_nummer(tabelle:string):int64;
var
sql:string;
zeit:ttime;
j:int64 ;
i:integer;
felder:array [0..1] of string;
begin
	result:=0;
	//datensatz mit index 0 l�schen
		sql:=format('delete from %s where nummer=0',[tabelle]);
		sql_exe(sql);

		sql:='select aktuell from nummern_kreis where tab_name='+hk+tabelle+hk;

		j:=zeilen_zahl(sql);
      //showmessage('j='+inttostr(j));
		if j<1 then  //keine zeilen
		begin
			result:=-2; //tabelle nicht vorhanden;
                        //showmessage('Tabelle '+tabelle+' in nummern_kreis nicht vorhanden');
			exit;
		end;

		zeit:=time();
    i:=0;
		sql:='UPDATE nummern_kreis SET gesperrt=1 WHERE tab_name='+hk+tabelle+hk;
     try
       while (not sql_exe(sql) ) do  //
          begin
             application.ProcessMessages;
             if (time()>zeit+0.0001)  then
             begin
               showmessage('Bitte warten bis der Nummernkeis wieder freigegeben ist');
               application.ProcessMessages;
               zeit:=time();
               if i>5 then exit;//raise exception.Create('neue_nummer');//schleife wenn gesperrt
               inc(i);
             end;
          end;
       sql:='update nummern_kreis set aktuell=aktuell+1 where tab_name='+hk+tabelle+hk;
       if  not sql_exe(sql) then raise exception.Create('neue_nummer');//schleife wenn gesperrt

       sql:='select aktuell,bis from nummern_kreis where tab_name='+hk+tabelle+hk;
       mysql_d.Feldarray(sql,[0,1],felder);

       result:=strtoint64(felder[0]);
       j:=strtoint64(felder[1]);

	  finally

		sql:='update nummern_kreis set gesperrt=0 where tab_name='+hk+tabelle+hk;
		sql_exe(sql);
     if (result>0) and (((lowercase (tabelle)='firma') and ((j-result)<20)) or ((lowercase (tabelle)<>'firma') and ((j-result)<200))) then showmessage('Neuer Nummernkreis notwendig - bitte mit dem Support in Verbindung setzen! Tabelle/nummer: '+tabelle +'/'+ inttostr(result));
		 if result=0 then showmessage('Fehler beim Anlegen eines Datensatzes in Tabelle: '+tabelle);
	end;

end;

function tmysql_d.drop_table(tab_name:string):boolean;
var query,q1:string;
begin
try
      //query:=format('DELETE FROM %s',[tab_name]);
	  //result:=sql_exe(query);
      q1:=format('SHOW TABLE STATUS like "%s"',[tab_name]);

      while sql_exe_rowsvorhanden(q1) do

      begin
      	query:=format('drop table if exists %s',[tab_name]);
	  		result:=sql_exe(query);
      end;

except
end;
end;

function tmysql_d.struktur_von_db (quelle, ziel:string): boolean;
var
weiter:boolean;
query,re,fehler:string;
begin
result:=false;
weiter:=false;
//erzeugt die Tabelle ziel nach dem Muster von quelle

   //hier erst wenn sicher gel�scht
   while  not weiter do
   begin
      try
         drop_table(ziel);
         query:=format('show create table %s',[quelle]);
         pause(1);
         re:=feldinhalt(query,1);

         re:= stringreplace(re,#$A,'',[rfReplaceAll]);
         re:=stringreplace(re,quelle,ziel,[rfIgnoreCase]);

         result:=sql_exe(re); //tabellenstruktur erzeugen
         weiter:=true;
       except
         fehler:=' Tabellenstruktur konnte nicht erzeugt werden - nochmals versuchen?' ;
         if Messagedlg(fehler,mtConfirmation, [mbOK,mbcancel],0)=mrcancel then weiter:=true;
       end

   end;

end;

function tmysql_d.struktur_von_db_history (quelle, ziel:string): boolean;
var
weiter:boolean;
query,re,fehler:string;
begin
result:=false;
weiter:=false;
//erzeugt die Tabelle ziel nach dem Muster von quelle

   //hier erst wenn sicher gel�scht
   while  not weiter do
   begin
      try
         drop_table(ziel);
         query:=format('show create table %s',[quelle]);
         pause(1);
         re:=feldinhalt(query,1);

         re:= stringreplace(re,#$A,'',[rfReplaceAll]);
         re:=stringreplace(re,quelle,ziel,[rfIgnoreCase]);

         result:=sql_exe(re); //tabellenstruktur erzeugen
         weiter:=true;
       except
         fehler:=' Tabellenstruktur konnte nicht erzeugt werden - nochmals versuchen?' ;
         if Messagedlg(fehler,mtConfirmation, [mbOK,mbcancel],0)=mrcancel then weiter:=true;
       end

   end;

end;

function tmysql_d.db_von_db(quelle,ziel:string;leer:boolean):boolean;
var
query,re:string;
erfolg:boolean;
begin
erfolg:=false;

while not erfolg do
begin
	erfolg:=struktur_von_db(quelle,ziel); //nur struktur
    if not leer then
    begin
	    query:=format('insert into %s select * from %s',[ziel,quelle]);
		  erfolg:=sql_exe(query) and erfolg;
    end;
end;

end;

function tmysql_d.create_sql_von_db (quelle:string): string;
var
query,re:string;
p:integer;
begin
   query:=format('show create table %s',[quelle]);
   result:=feldinhalt(query,1);
   p:=pos(chr(10),result);
   result:=copy(result,p-1,length(result));

end;


function tmysql_d.anzahl_datensaetze (tabelle:tzquery):string;
var q :string;
    p:integer;
begin
  q:=tabelle.SQL.Text;
  p:=pos('from',q);
  q:=copy(q,p,length(q));
  q:='select count(*) '+q;
  result:=mysql_d.Feldinhalt( q,0);
end;



function tmysql_d.history_von2(quelle,ziel,nummer:string):boolean;
var
erfolg:boolean;
query,fname,dat_now,ins:string;
tab_log,hist_id,s:string;
nr,n,j:integer;
begin
//temp datei ziel aus quelle erzeugen
//neuer ds aus aktueller quelle
//alle aus log-datei einf�gen
//immer letzten Ds kopieren und dann mit den daten der log-datei �berschreiben
with datamodul do
begin
  tab_log:='log_'+quelle;
  nr:=1;

  dat_now:= dattime_to_sql(datetostr(now()),timetostr(now()));

  query:=format('drop table if exists %s',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  erfolg:=struktur_von_db(quelle,ziel);
  query:=format('insert into %s select * from %s where nummer=%s',[ziel,quelle,nummer]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('ALTER TABLE `%s` DROP primary key',[ziel]);
  erfolg:=sql_exe(query) and erfolg;


  query:=format('ALTER TABLE `%s` ADD COLUMN `nr` INT ZEROFILL NULL  after nummer,ADD INDEX `s1` (`nr`);',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('ALTER TABLE `%s` ADD COLUMN `dt` datetime after nr',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('ALTER TABLE `%s` ADD COLUMN `c_user` char(30) after dt',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('update %s set dt=%s',[ziel,dat_now]);
  erfolg:=sql_exe(query) and erfolg;


  //insert into tmp1  (select * from tmp1 where tmp1.nr=(select max(nr))ORDER BY nr DESC limit 1);
//update tmp1 set nr=(select max(nr)+1) where nr=(select max(nr)) ORDER BY nr DESC LIMIT 1;

  query:=format('update %s set nr=%d',[ziel,nr]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('select * from %s where nummer=%s order by dt desc',[tab_log,nummer]);
  sql_new( false,q_history,query,'');
  q_history.First;
  while not q_history.Eof do
  begin
     hist_id:=q_history.findfield('log_id').asstring;
     query:=format('insert into %s (select * from %s where nr=%d)   ',[ziel,ziel,nr]);
     erfolg:=sql_exe(query) and erfolg;
     query:=format('update %s set nr=(select max(nr)+1) where nr=(select max(nr)) ORDER BY nr DESC LIMIT 1;',[ziel]);
     erfolg:=sql_exe(query) and erfolg;
     inc(nr);

      for j:=3 to q_history.fieldcount -1 do
      begin
       fname:=q_history.FieldDefs.Items[j].Name;//  FieldDefs .Fields[j].asstring;
       if not ((fname='mdf') ) then
       //if q_history.Findfield(fname).asstring<>'' then
       if not q_history.Findfield(fname).IsNull then
       begin
           //query:=format('select uid from %0:s where log_id=%1:s)',[tab_log,hist_id]);
           //s:=mysql_d.Feldinhalt(query);
           //query:=format('select name from schluessel where nummer=(select uid from %0:s where log_id=%1:s)  )',[tab_log,hist_id]);
           //s:=mysql_d.Feldinhalt(query);

           //query:=format('update %0:s set %0:s.%1:s=(select %1:s from %2:s where log_id=%3:s),%0:s.c_user=(select uid from %2:s where log_id=%3:s)  where nr=%4:d;',[ziel,fname,tab_log,hist_id,nr]);
           query:=format('update %0:s set %0:s.%1:s=(select %1:s from %2:s where log_id=%3:s),%0:s.c_user=(select name from schluessel where nummer= (select uid from %2:s where log_id=%3:s))  where nr=%4:d;',[ziel,fname,tab_log,hist_id,nr]);
           erfolg:=sql_exe(query) and erfolg;
       end
       else
       begin
         {case q_history.Findfield(fname).DataType of
           ftsmallint,ftinteger,ftfloat,ftlargeint,ftword:ins:='0';
           ftdate,ftdatetime:ins:='2000-01-01';
           fttime:ins:='00:00:00';
           ftstring,ftFixedChar,ftWideString	:ins:='';
           ftboolean:ins:='0';
           ftmemo,ftFmtMemo	:ins:='';
           ftblob:ins:='';
           ftUnknown :ins:='';
          else
            ins:=''
          end;
         query:=format('update %0:s set %0:s.%1:s="%2:s" where nr=%3:d;',[ziel,fname,ins,nr]);
         erfolg:=sql_exe(query) and erfolg; }
       end;
      end;

    q_history.Next;
  end;


end;
end;

function tmysql_d.history_von(quelle,ziel,nummer:string):boolean;
var
erfolg:boolean;
query,fname,dat_now,ins,dt:string;
tab_log,hist_id,s:string;
nr,n,j:integer;
begin
//temp datei ziel aus quelle erzeugen
//neuer ds aus aktueller quelle
//alle aus log-datei einf�gen
//immer letzten Ds kopieren und dann mit den daten der log-datei �berschreiben
with datamodul do
begin
  tab_log:='log_'+quelle;
  nr:=1;

  dat_now:= dattime_to_sql(datetostr(now()),timetostr(now()));

  query:=format('drop table if exists %s',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  erfolg:=struktur_von_db(quelle,ziel);
  query:=format('insert into %s select * from %s where nummer=%s',[ziel,quelle,nummer]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('ALTER TABLE `%s` DROP primary key',[ziel]);
  erfolg:=sql_exe(query) and erfolg;


  query:=format('ALTER TABLE `%s` ADD COLUMN `nr` INT ZEROFILL NULL  after nummer,ADD INDEX `s1` (`nr`);',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('ALTER TABLE `%s` ADD COLUMN `dt` datetime after nr',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('ALTER TABLE `%s` ADD COLUMN `c_user` char(30) after dt',[ziel]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('update %s set dt=%s',[ziel,dat_now]);
  erfolg:=sql_exe(query) and erfolg;


  //insert into tmp1  (select * from tmp1 where tmp1.nr=(select max(nr))ORDER BY nr DESC limit 1);
//update tmp1 set nr=(select max(nr)+1) where nr=(select max(nr)) ORDER BY nr DESC LIMIT 1;

  query:=format('update %s set nr=%d',[ziel,nr]);
  erfolg:=sql_exe(query) and erfolg;

  query:=format('select * from %s where nummer=%s order by dt desc',[tab_log,nummer]);
  sql_new( false,q_history,query,'');
  dt:='';
  q_history.First;
  while not q_history.Eof do
  begin

     hist_id:=q_history.findfield('log_id').asstring;
     if dt<>q_history.findfield('dt').asstring then
     begin
       query:=format('insert into %s (select * from %s where nr=%d)   ',[ziel,ziel,nr]);
       erfolg:=sql_exe(query) and erfolg;
       query:=format('update %s set nr=(select max(nr)+1) where nr=(select max(nr)) ORDER BY nr DESC LIMIT 1;',[ziel]);
       erfolg:=sql_exe(query) and erfolg;
       inc(nr);
       dt:=q_history.findfield('dt').asstring;
     end;

      begin
       fname:=q_history.findfield('f_name').AsString;
       query:=format('update %0:s set %0:s.%1:s=(select %2:s from %3:s where log_id=%4:s),%0:s.dt=(select dt from %3:s where log_id=%4:s),%0:s.c_user=(select name from schluessel where nummer= (select uid from %3:s where log_id=%4:s))  where nr=%5:d;',[ziel,fname,'f_value',tab_log,hist_id,nr]);
       erfolg:=sql_exe(query) and erfolg;
      end;

    q_history.Next;
  end;


end;
end;

function tmysql_d.replace_query(quelle,ziel,wo:string):string;
var
tab_felder:tstringlist;
tab_name:string;
query,odk:string;
q:tzquery;
i:integer;
begin
try

    tab_felder:=tstringlist.create;
    tab_name:=ziel;
    tab_felder.Clear;
    query:=format('show columns from %s',[ziel]);

    q:=tzquery.create( datamodul);
    q.connection:=datamodul.connection_main;
    sql_normieren(query);
    q.Sql.Text:=query;
    q.Open;
    q.First;
    while not q.Eof do
    begin
      tab_felder.Add(q.findfield('field').asstring);
      q.Next;
    end;

    odk:='';
    for i:=1 to tab_felder.Count-1 do
      odk:=odk + format(' %0:s= (select %0:s from %1:s where nummer=(%2:s.nummer)),',[tab_felder[i],quelle,ziel]);

   odk:=copy(odk,1,length(odk)-1);

  if wo<>'' then wo:=format(' where nummer=%s ',[wo]);
  result:=format('insert into %0:s select * from %1:s %2:s on duplicate key update %3:s ',[ziel,quelle,wo,odk ]);

finally
  q.Free;
  tab_felder.free;
end;
end;


procedure tmysql_d.start_transaction(tabelle_name:string; nummer:int64);
var temptab,query:string;
begin
//if nummer=g_ds_neu then exit; //dann wurde ein DS angelegt
if no_transaction or ( pos(tabelle_name,'labor,gdt, befunde,rechnung,user,abteilung,a_platz,a_gefaehrdung,a_gefaehrdungsfaktor,a_projekte') >0) then exit;

with datamodul do

  if speichern_fragen then
    begin
      if not (datamodul.connection_main.InTransaction or connection_main.AutoCommit=false) then
        begin
          //connection_main.TransactIsolationLevel:=tiReadCommitted;//tiRepeatableRead;//
          //connection_main.AutoCommit:=false;
          //speichere aktuellen Datensatz tempor�r
          // create table temptab select * from tabelle where nummer=nummer
          mysql_d.ds_aenderung:=false;

          temptab:=format('transaction_%d',[user_id]);
          query:=format('drop table if exists %s',[temptab]);
          mysql_d.sql_exe(query);
          query:=format('create table %s select * from %s where nummer=%s',[temptab,tabelle_name, inttostr(nummer)]);
          mysql_d.sql_exe(query);
        end;
    end;
end;


function tmysql_d.stop_transaction(tabelle_name:string; nummer:int64;tabelle:tzquery):boolean;
var temptab,query, vorhanden:string;
ds_speichern:boolean;
begin
if speichern_fragen then
    begin

      try

      if (tabelle<>nil )and (tabelle.State in [dsedit,dsinsert]) then tabelle.Post; //sonst geht refre sh nicht 

      temptab:=format('transaction_%d',[user_id]);

      query:=format('show tables like "%s"  ',[temptab]);  //nicht vorhanden wenn neu erstellt wird
      vorhanden:=mysql_d.Feldinhalt(query,0);

        if (vorhanden<>'') then
        begin
          if loeschabfrage then
            begin
             ds_speichern:= (Messagedlg('L�schen des aktuellen Datensatzes?',mtConfirmation, [mbyes,mbno],1)=mrno);
            end
          else
           if keine_abfrage or not ds_aenderung then   //ds_aenderung muss bei Neuanlage true werden
            ds_speichern:=true
           else
            ds_speichern:=( Messagedlg('Speichern des aktuellen Datensaztes?',mtConfirmation, [mbyes,mbno],0)=mryes);
            //datamodul.connection_main.Commit
          if ds_speichern then
           //speichern nichts tun
          else
             begin //�berschreiben bzw. l�schen(ds_neu_nr:=0)
              //datamodul.connection_main.rollback;
              //�berschreibe den aktuellen Datensatz mit dem tempor�ren
              //replace into mitarbeiter select * from temptab where nummer=nummer;

              //showmessage(format('nr1:%d; nr2:%d; tab1:%s; tab2:%s',[ds_neu_nr,nummer,ds_neu_tab,tabelle_name]));
              //if tabelle<>nil then
                  if (ds_neu_nr=nummer) and (ds_neu_tab=tabelle_name) then  //sicherheitsabfrage wird nur bei neuanlage angelegt
                    begin
                      //tabelle.Delete;
                      query:=format('delete from %s where nummer= %s ',[tabelle_name, inttostr(nummer)]); //datensatz war neu74
                      mysql_d.sql_exe(query);
                      ds_geloescht:=true; // dann refresh in dbspeichern
                      protokoll('sys','abort_insert',query,true);
                    end  
                  else
                    begin
                      query:=format('replace into %s select * from %s ',[tabelle_name, temptab]);
                      mysql_d.sql_exe(query);
                    end;
              //tabelle.refresh;
              //refresh
              result:=true;
            end;
        end;
      finally

        //datamodul.connection_main.TransactIsolationLevel:=tiNone;
        //datamodul.connection_main.AutoCommit:=true;
        //l�sche tempor�re Tabelle
        query:=format('drop table if exists %s',[temptab]);
        mysql_d.sql_exe(query);
        ds_neu_nr:=0;
        ds_neu_tab:='';
        loeschabfrage:=false;
        keine_abfrage:=false;
      end;
    end
    else
    if (tabelle<>nil )and (tabelle.State in [dsedit,dsinsert]) then tabelle.Post;  //wenn keine Abfrage dann wird immer gespeichert = post

  //gilt in beiden F�llen s. oben
    if temptab<>'' then
    try
      query:=format('drop table if exists %s',[temptab]);
      mysql_d.sql_exe(query);
    except
      if form_main.m_erweitertefehlermeldungen.Checked then showmessage('Fehler bei:'+query);  
    end;
    ds_neu_nr:=0;
    ds_neu_tab:='';
    loeschabfrage:=false;
    keine_abfrage:=false;
end;

procedure tmysql_d.protokoll(art, funk, aktion: string; wf:boolean);
var
   query,db,un:string;
   w,dbver,lf,la,lu:integer;
begin

   if art='user' then db:='protokoll_user' else db:='protokoll_sys';

    lf:=255;
    la:=1000;
    lu:=255;

   //aktion:=sonderzeichenraus(aktion);  //sonst
   aktion:=stringreplace(aktion,'"','|',[rfreplaceall]);
   aktion:=stringreplace(aktion,char(39),'|',[rfreplaceall]);
   funk:=copy(funk,1,lf);
   aktion:=copy(aktion,1,la);
   un:=copy(user_name,1,lu);

   if wf then w:=1 else w:=0;
   query:=format('insert %s set funk="%s" , aktion="%s", resultat=%d,user_name="%s"',[db, funk,aktion,w,un]);
   try
   	sql_exe_still(query);
   except
   	//wenn prot-dat noch nicht angelegt ist
    showmessage('Nicht in Protokolldat. eingetragen: '+query);
   end;
end;

end.
