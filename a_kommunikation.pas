unit a_kommunikation;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls;

type
  TForm_kommunikation = class(TForm)
    Panel1: TPanel;
    BitBtn_cancel: TBitBtn;
    BitBtn_ok: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CheckListBox: TCheckListBox;
    Memo: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_kommunikation: TForm_kommunikation;

implementation

uses a_main;

{$R *.DFM}


procedure TForm_kommunikation.FormCreate(Sender: TObject);
var
i:integer;
begin
	 form_main.form_positionieren(tform(sender));
	 for i:=0 to com.benutzer_id_liste.count-1 do
	 begin
		 checklistbox.Items.Add(com.benutzer_liste[i]);
	 end;

end;

procedure TForm_kommunikation.BitBtn_okClick(Sender: TObject);
var i:integer;
begin
	for i:=0 to checklistbox.Items.Count-1 do
	begin
	  if checklistbox.Checked[i] then com.send_nachricht(com.computer_liste[i],'zeig_nachricht',memo.text,'');
	end;
	modalresult:=mrok;
end;

end.
