unit a_kalender;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, Spin, ExtCtrls, Grids, Calendar, Mask;

type
  TForm_kalender = class(TForm)
    Panel1: TPanel;
    Auswahlkalender: TMonthCalendar;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    MaskEdit1: TMaskEdit;
    SpeedButton1: TSpeedButton;
    procedure AuswahlkalenderDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit1Change(Sender: TObject);
    procedure AuswahlkalenderClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_kalender: TForm_kalender;

implementation

uses a_main, a_termine;

{$R *.DFM}

procedure TForm_kalender.AuswahlkalenderDblClick(Sender: TObject);
begin
maskedit1.Text:=datetostr(int(auswahlkalender.date));
modalresult:=mrok;
end;

procedure TForm_kalender.FormCreate(Sender: TObject);
begin
//form_main.form_positionieren(tform(sender));
form_main.form_positionieren(tform(sender));
auswahlkalender.Date:=date();

end;

procedure TForm_kalender.FormShow(Sender: TObject);
begin
if auswahlkalender.Date=strtodate('01.01.1900') then auswahlkalender.Date:=date();
AuswahlkalenderClick(sender);
maskedit1.SelectAll;
maskedit1.SetFocus;

end;

procedure TForm_kalender.MaskEdit1Change(Sender: TObject);
var
d:tdate;
begin
if not tmaskedit(sender).focused then exit;
try
d:=strtodate(maskedit1.text);
auswahlkalender.Date:=d;
except
end;
end;

procedure TForm_kalender.AuswahlkalenderClick(Sender: TObject);
begin
maskedit1.Text:=datetostr(int(auswahlkalender.date));
end;

procedure TForm_kalender.SpeedButton1Click(Sender: TObject);
begin
	maskedit1.text:='01.01.1900';
end;

end.
