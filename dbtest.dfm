object Form1: TForm1
  Left = 622
  Top = 123
  Width = 1305
  Height = 675
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 163
    Top = 72
    Width = 806
    Height = 163
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 85
    Top = 293
    Width = 60
    Height = 20
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 208
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
  end
  object ZConnection1: TZConnection
    ControlsCodePage = cGET_ACP
    AutoEncodeStrings = False
    Properties.Strings = (
      'controls_cp=GET_ACP')
    AutoCommit = False
    DesignConnection = True
    HostName = 'localhost'
    Port = 0
    Database = 'arbene'
    User = 'Arbene'
    Password = 'nixreinkommen'
    Protocol = 'MariaDB-5'
    Left = 64
    Top = 80
  end
  object ZQuery1: TZQuery
    Connection = ZConnection1
    SQL.Strings = (
      'select * from mitarbeiter')
    Params = <>
    Left = 64
    Top = 144
  end
  object DataSource1: TDataSource
    DataSet = ZQuery1
    Left = 104
    Top = 152
  end
  object ZUpdateSQL1: TZUpdateSQL
    UseSequenceFieldForRefreshSQL = False
    Left = 64
    Top = 352
  end
  object ZSQLProcessor1: TZSQLProcessor
    Params = <>
    Connection = ZConnection1
    Delimiter = ';'
    Left = 64
    Top = 392
  end
end
