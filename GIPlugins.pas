unit GIPlugins;

{$IFNDEF VER80}
  {$IFNDEF VER90}
    {$IFNDEF VER93}
      {$DEFINE D3}         // Delphi 3 or higher
      {$IFNDEF VER100}
        {$IFNDEF VER110}
          {$DEFINE D4}     // Delphi 4 or higher
          {$IFNDEF VER120}
            {$DEFINE D5}   // Delphi 5 or higher
            {$IFNDEF VER130}
              {$DEFINE D6}   // Delphi 6 or higher
              {$IFNDEF VER140}
                {$DEFINE D7}   // Delphi 7 or higher
              {$ENDIF}
            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

interface

uses Windows;

const
  // Ghost Installer versions
  VER_GI_200 = $20000;
  VER_GI_210 = $20100;
  VER_GI_220 = $20200;
  VER_GI_300 = $30000;
  VER_GI_310 = $30100;
  VER_GI_320 = $30200;
  VER_GI_330 = $30300;
  VER_GI_340 = $30400;
  VER_GI_350 = $30500;
  VER_GI_360 = $30600;
  VER_GI_370 = $30700;
  VER_GI_400 = $40000;
  VER_GI_410 = $40100;
  VER_GI_420 = $40200;
  VER_GI_450 = $40500;
  VER_GI_460 = $40600;
  VER_GI_470 = $40700;
  VER_GI_480 = $40800;

  // TPluginInfo versions
  VER_PLUGININFO_100 = $10000;
  VER_PLUGININFO_200 = $20000;

  // TPluginStartupInfo versions
  VER_PLUGINSTARTUPINFO_100 = $10000;
  VER_PLUGINSTARTUPINFO_200 = $20000;

  // events
  EV_LOAD                  = 1;
  EV_UNLOAD                = 2;
  EV_START                 = 3;
  EV_FINISH                = 4;
  EV_SHOWDIALOGS           = 5;
  EV_LANGUAGECHANGED       = 6;
  EV_BEFOREINSTALL         = 7;
  EV_AFTERINSTALL          = 8;
  EV_BEFOREPROCESSITEMS    = 9;
  EV_AFTERPROCESSITEMS     = 10;
  EV_BEFOREPROCESSITEM     = 11;
  EV_AFTERPROCESSITEM      = 12;
  EV_INSTALLPROGRESS       = 13;
  EV_FEATURESTATECHANGED   = 14;
  EV_INSTALLTYPECHANGED    = 15;
  EV_FILEVERSIONCONFLICT   = 16;
  EV_GETPACKAGE            = 17;
  EV_CHECKLAUNCHCONDITIONS = 18;
  EV_CALCFUNCTION          = 19;
  EV_QUERYREBOOT           = 20;
  EV_ABORTINSTALL          = 21;
  EV_ROLLBACK              = 22;
  EV_BEFORECMPUNINSTALL    = 23;
  EV_AFTERCMPUNINSTALL     = 24;
  EV_FUNCTIONREGISTERED    = 25;
  EV_SELECTINSTALLMODE     = 26;
  EV_CHECKGLOBALCONDITIONS = 27;

  // Engine control messages
  CM_BASE                  = $100;
  CM_ABORTINSTALL          = CM_BASE;
  CM_SUSPENDINSTALL        = CM_BASE+1;
  CM_RESUMEINSTALL         = CM_BASE+2;
  CM_PARSETEXT             = CM_BASE+3;
  CM_GETLANGUAGE           = CM_BASE+4;
  CM_SETLANGUAGE           = CM_BASE+5;
  CM_GETVARIABLE           = CM_BASE+6;
  CM_SETVARIABLE           = CM_BASE+7;
  CM_GETFEATURE            = CM_BASE+8;
  CM_GETFEATURESTATE       = CM_BASE+9;
  CM_SETFEATURESTATE       = CM_BASE+10;
  CM_GETINSTALLTYPE        = CM_BASE+11;
  CM_SETINSTALLTYPE        = CM_BASE+12;
  CM_SETPASSWORD           = CM_BASE+13;
  CM_GETPCPARAM            = CM_BASE+14;
  CM_GETPCGROUPNAMES       = CM_BASE+15;
  CM_GETPCPARAMNAMES       = CM_BASE+16;
  CM_GETPCCUSTOMCONFIG     = CM_BASE+17;
  CM_CALCEXPRESSION        = CM_BASE+18;
  CM_CALCBOOLEXPRESSION    = CM_BASE+19;
  CM_REGISTERFUNCTION      = CM_BASE+20;
  CM_ADDLOGRECORD          = CM_BASE+21;
  CM_GETINSTALLICON        = CM_BASE+22;
  CM_ISPASSWORDVALID       = CM_BASE+23;
  CM_CALCACTIONS           = CM_BASE+24;
  CM_GETFEATURESIZE        = CM_BASE+25;
  CM_GETSETUPSIZE          = CM_BASE+26;
  CM_DELETELOGRECORD       = CM_BASE+27;
  CM_GETINSTALLTYPESIZE    = CM_BASE+28;
  
  // message parameter types
  MPT_STRING  = 0;
  MPT_INTEGER = 1;
  MPT_BOOLEAN = 2;

  // feature states
  FS_DISABLED         = 0;
  FS_ENABLED          = 1;
  FS_PARTIALLYENABLED = 2;

  // installation item types
  IT_FILE      = 0;
  IT_REGISTRY  = 1;
  IT_INI       = 2;
  IT_SHORTCUT  = 3;
  IT_REGSERVER = 4;
  IT_REGFONT   = 5;
  IT_RUNAPP    = 6;

  // actions
  ACT_ADD    = 0;
  ACT_REMOVE = 1;

  // version conflict dialog result constants
  MR_YES      = 1;
  MR_NO       = 2;
  MR_YESTOALL = 3;
  MR_NOTOALL  = 4;

  // registry parameter types
  PT_STRING  = 0;
  PT_INTEGER = 1;
  PT_BINARY  = 2;
  PT_MSTRING = 3;

  // show state
  SC_NORMAL    = 0;
  SC_MINIMIZED = 1;
  SC_MAXIMIZED = 2;
  SC_HIDDEN    = 3;

  // when start a program
  WS_BEFOREINSTALL   = 0;
  WS_AFTERINSTALL    = 1;
  WS_BEFOREUNINSTALL = 2;
  WS_AFTERUNINSTALL  = 3;
  WS_BEFOREDIALOGS   = 4;

  // Standard UI plug-in control messages
  SCM_SHOWSTATUSTEXT     = 'CM_SHOWSTATUSTEXT';
  SCM_HIDESTATUSTEXT     = 'CM_HIDESTATUSTEXT';
  SCM_GETMAINWINDOW      = 'CM_GETMAINWINDOW';
  SCM_GETACTIVEWINDOW    = 'CM_GETACTIVEWINDOW';
  SCM_ADDCUSTOMDLGAFTER  = 'CM_ADDCUSTOMDLGAFTER';
  SCM_ADDCUSTOMDLGBEFORE = 'CM_ADDCUSTOMDLGBEFORE';
  SCM_GETURLLINK         = 'CM_GETURLLINK';

  // Standard UI plug-in events
  SEV_BEFOREDIALOG  = 'EV_BEFOREDIALOG';
  SEV_AFTERDIALOG   = 'EV_AFTERDIALOG';
  SEV_SHOWCUSTOMDLG = 'EV_SHOWCUSTOMDLG';

  // Standard UI return values
  RV_BACK   = 1;
  RV_NEXT   = 2;
  RV_CANCEL = 3;

  // Billboards plug-in events
  SEV_BEFOREBILLBOARDS = 'EV_BEFOREBILLBOARDS';
  SEV_AFTERBILLBOARDS  = 'EV_AFTERBILLBOARDS';


type
  TProcessMsgProc = function(Msg: dword; MsgName: PChar;
    InParams, OutParams: dword): dword; stdcall;
  TCharData = array [0..MAX_PATH] of char;
  TLargeCharData = array [0..$1000] of char;

  TPluginInfo = packed record
    InfoVersion: dword;
    RequiredGIVersion: dword;
    Flags: dword;
    PluginID: TCharData;
    PluginName: TCharData;
    PluginVersion: dword;
    PluginAuthor: TCharData;
    PluginDescription: TLargeCharData;
    PluginCopyright: TCharData;
    ProcessMsg: TProcessMsgProc;
  end;
  PPluginInfo = ^TPluginInfo;

  TGetPluginInfoProc = procedure(var PluginInfo: TPluginInfo); stdcall;

  TRegisterMsgProc = function(HPlugin: dword; MsgName: PChar): dword; stdcall;
  TSendMsgProc = function(HPlugin: dword; Msg, InParams, OutParams: dword;
    Broadcast, FreeInParams: LongBool): dword; stdcall;
  TSendMsgNameProc = function(HPlugin: dword; MsgName: PChar;
    InParams, OutParams: dword; Broadcast, FreeInParams: LongBool): dword; stdcall;
  TSendPrivateMsgProc = function(HPlugin: dword; Msg, InParams, OutParams: dword;
    FreeInParams: LongBool): dword; stdcall;
  TSendPrivateMsgNameProc = function(HPlugin: dword; MsgName: PChar;
    InParams, OutParams: dword; FreeInParams: LongBool): dword; stdcall;
  TCreateParamsBlockProc = function: integer; stdcall;
  TDestroyParamsBlockProc = procedure(BlockID: integer); stdcall;
  TGetParamCountProc = function(BlockID: integer): integer; stdcall;
  TSetParamCountProc = procedure(BlockID,ParamCount: integer); stdcall;
  TGetParamTypeProc = function(BlockID,ParamIndex: integer): integer; stdcall;
  TGetParamStrProc = function(BlockID,ParamIndex: integer): PChar; stdcall;
  TGetParamIntProc = function(BlockID,ParamIndex: integer): integer; stdcall;
  TGetParamBoolProc = function(BlockID,ParamIndex: integer): LongBool; stdcall;
  TSetParamStrProc = procedure(BlockID,ParamIndex: integer; ParamValue: PChar); stdcall;
  TSetParamIntProc = procedure(BlockID,ParamIndex: integer; ParamValue: integer); stdcall;
  TSetParamBoolProc = procedure(BlockID,ParamIndex: integer; ParamValue: LongBool); stdcall;
  TAddParamStrProc = procedure(BlockID: integer; ParamValue: PChar); stdcall;
  TAddParamIntProc = procedure(BlockID: integer; ParamValue: integer); stdcall;
  TAddParamBoolProc = procedure(BlockID: integer; ParamValue: LongBool); stdcall;

  TPluginStartupInfo = packed record
    InfoVersion: dword;
    GIVersion: dword;
    HPlugin: dword;
    RegisterMsg: TRegisterMsgProc;
    SendMsg: TSendMsgProc;
    SendMsgName: TSendMsgNameProc;
    SendPrivateMsg: TSendPrivateMsgProc;
    SendPrivateMsgName: TSendPrivateMsgNameProc;
    CreateParamsBlock: TCreateParamsBlockProc;
    DestroyParamsBlock: TDestroyParamsBlockProc;
    GetParamCount: TGetParamCountProc;
    SetParamCount: TSetParamCountProc;
    GetParamType: TGetParamTypeProc;
    GetParamStr: TGetParamStrProc;
    GetParamInt: TGetParamIntProc;
    GetParamBool: TGetParamBoolProc;
    SetParamStr: TSetParamStrProc;
    SetParamInt: TSetParamIntProc;
    SetParamBool: TSetParamBoolProc;
    AddParamStr: TAddParamStrProc;
    AddParamInt: TAddParamIntProc;
    AddParamBool: TAddParamBoolProc;
  end;
  PPluginStartupInfo = ^TPluginStartupInfo;

  TSetStartupInfoProc = procedure(StartupInfo: PPluginStartupInfo); stdcall;

  TGetPubEventsProc = procedure(EventList: PChar; MaxLen: integer); stdcall;

  
function CreateMsgParams(SI: PPluginStartupInfo; Params: array of const): integer;


implementation

uses SysUtils {$IFDEF D6}, Variants{$ENDIF};

function CreateMsgParams(SI: PPluginStartupInfo; Params: array of const): integer;

  procedure AddParam(BlockID: integer; const Param: TVarRec);
  var
    s: string;
  begin
    with Param do
      case VType of
        vtInteger:
          SI^.AddParamInt(BlockID,VInteger);
        vtBoolean:
          SI^.AddParamBool(BlockID,VBoolean);
        vtChar:
          begin
            s:=VChar;
            SI^.AddParamStr(BlockID,PChar(s));
          end;
        vtExtended:
          begin
            s:=FloatToStr(VExtended^);
            SI^.AddParamStr(BlockID,PChar(s));
          end;
        vtString:
          begin
            s:=VString^;
            SI^.AddParamStr(BlockID,PChar(s));
          end;
        vtPChar:
          SI^.AddParamStr(BlockID,VPChar);
        vtAnsiString:
          begin
            s:=string(VAnsiString);
            SI^.AddParamStr(BlockID,PChar(s));
          end;
        vtVariant:
          begin
            case VarType(VVariant^) and varTypeMask of
              varByte,varSmallint,varInteger:
                SI^.AddParamInt(BlockID,VVariant^);
              varSingle,varDouble:
                SI^.AddParamStr(BlockID,PChar(FloatToStr(VVariant^)));
              varDate:
                SI^.AddParamStr(BlockID,PChar(DateTimeToStr(VVariant^)));
              varBoolean:
                SI^.AddParamBool(BlockID,VVariant^);
              varString:
                SI^.AddParamStr(BlockID,PChar(string(VVariant^)));
            end;
          end;
        else
          raise Exception.Create('Wrong parameter type.');
      end;
  end;

var
  i,ParamCount: integer;
begin
  ParamCount:=High(Params);
  Result:=SI^.CreateParamsBlock;
  for i:=0 to ParamCount do AddParam(Result,Params[i]);
end;

end.
