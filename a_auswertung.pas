unit a_auswertung;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, Grids, DBGrids, dbgridEXT, ExtCtrls, DBCtrls,
	StdCtrls, Buttons, Mask, CheckLst,db,listen, QRCTRLS, DdeMan,ComObj,a_data,
	TreeView_ext, Menus, dbcombo_number,excels,variants,DateUtils;

type inithread=class(tthread)
		protected
		procedure execute; override;
	 public
			constructor create; virtual;

	end;


type
  TForm_auswertung = class(TForm)
    ToolBar1: TToolBar;
    ToolButton_neu: TToolButton;
    ToolButton2: TToolButton;
	  ToolButton_delete: TToolButton;
	  PageControl_auswertung: TPageControl;
    TabSheet_anzeige: TTabSheet;
    tabsheet_filtern: TTabSheet;
	  GroupBox1: TGroupBox;
    Panel8: TPanel;
    SpeedButton5: TSpeedButton;
	  SpeedButton7: TSpeedButton;
    TreeView: TTreeView;
    GroupBox2: TGroupBox;
    Notebook: TNotebook;
    GroupBox_erweitert: TGroupBox;
    DBgridEXT2: TDBgridEXT;
    Panel4: TPanel;
    DBEdit_icd_schluessel: TDBEdit;
	  DBEdit_bes_hauptkriterium: TDBEdit;
    BitBtn_bes_haupt: TBitBtn;
	  BitBtn2: TBitBtn;
    DBEdit_wert: TDBEdit;
    DBEdit_string: TDBEdit;
    DBComboBox2: TDBComboBox;
	  DBComboBox3: TDBComboBox;
	  DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label9: TLabel;
	  Label10: TLabel;
	  TabSheet_ergebnis: TTabSheet;
    DBgridEXT1: TDBgridEXT;
    Panel1: TPanel;
	  DBEdit3: TDBEdit;
	  DBComboBox7: TDBComboBox;
    DBRadioGroup2: TDBRadioGroup;
    dblookupcombobox_LISTE_labor: TDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Label18: TLabel;
    Impfstoff: TLabel;
    DBLookupComboBox_impfung_typ: TDBLookupComboBox;
    DBLookupComboBox_impfung_stoff: TDBLookupComboBox;
    DBEdit5: TDBEdit;
	  Label19: TLabel;
    Label20: TLabel;
	  BitBtn_export: TBitBtn;
    Label_export: TLabel;
    DBCheckBox_impf_all: TDBCheckBox;
	  DBCheckBox_labor_all: TDBCheckBox;
    DBLookupComboBox_befund_liste: TDBLookupComboBox;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
	  Label_anzahl: TLabel;
	  Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    DBEdit4: TDBEdit;
	  DBEdit6: TDBEdit;
    BitBtn3: TBitBtn;
    DBEdit_besonder_neben: TDBEdit;
    BitBtn_untwahl: TBitBtn;
    DBLookupComboBox_unt: TDBLookupComboBox;
    DBCheckBox2: TDBCheckBox;
    BitBtn_lab: TBitBtn;
	  BitBtn_impf: TBitBtn;
	  GroupBox_labwert: TGroupBox;
	  DBComboBox_relation: TDBComboBox;
    DBEdit_laborwert: TDBEdit;
    DBText1: TDBText;
    Panel_anzeige_li: TPanel;
	  GroupBox4: TGroupBox;
	  DBRadioGroup4: TDBRadioGroup;
    Tree_anzeige: TTreeView_ext;
	  PopupMenu: TPopupMenu;
    anzeigen1: TMenuItem;
	  Panel3: TPanel;
    RadioGroup_anzeigeart: TRadioGroup;
    DBText2: TDBText;
    DBText3: TDBText;
    Label102: TLabel;
	  Label103: TLabel;
    Label104: TLabel;
    DBEdit_s: TDBEdit;
    DBEdit_d: TDBEdit;
    DBComboBox_rr: TDBComboBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBRadioGroup5: TDBRadioGroup;
	  DBRadioGroup6: TDBRadioGroup;
    SpeedButton6: TSpeedButton;
	  SpeedButton8: TSpeedButton;
    sortierenaufsteigen1: TMenuItem;
	  sortierenabsteigend1: TMenuItem;
    demarkieren1: TMenuItem;
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    m_Abfragespeichern: TMenuItem;
    m_Abfrageladen: TMenuItem;
    N1: TMenuItem;
	  Auswertungverlassen1: TMenuItem;
    Bearbeiten1: TMenuItem;
    Filterneu: TMenuItem;
    Filterloeschen: TMenuItem;
    ToolButton1: TToolButton;
	  ToolButton3: TToolButton;
    m_Abfragenbearbeiten: TMenuItem;
    Abfragezurcksetzen1: TMenuItem;
    PopupMenu_ergebnis: TPopupMenu;
    GehezuMitarbeiterNameVornameGeburtsdatumauswhlen1: TMenuItem;
    ListBox_firma: TListBox;
    Panel5: TPanel;
    BitBtn1: TBitBtn;
	  Panel6: TPanel;
	  Panel7: TPanel;
	  Panel9: TPanel;
	  BitBtn_e1: TBitBtn;
    Label11: TLabel;
    DBEdit_groesse: TDBEdit;
	  DBEdit_gewicht: TDBEdit;
    DBComboBox1: TDBComboBox;
    DBRadioGroup_bmi: TDBRadioGroup;
    Label12: TLabel;
    DBEdit_bmi: TDBEdit;
    Label8: TLabel;
	  Label13: TLabel;
    Label14: TLabel;
    DBEdit_puls: TDBEdit;
	  Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit_fev: TDBEdit;
    DBEdit_fvc: TDBEdit;
    DBComboBox4: TDBComboBox;
    DBRadioGroup7: TDBRadioGroup;
    DBEdit_fev_fvc: TDBEdit;
	  Label24: TLabel;
    Label25: TLabel;
	  einfacheAbfragen1: TMenuItem;
    Tree: TTreeView_ext;
    DBCombo_num_befund_liste: TDBCombo_num;
    DBComboBox6: TDBComboBox;
    Label26: TLabel;
    DBComboBox5: TDBComboBox;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    CheckBox_negativ: TCheckBox;
    Label29: TLabel;
    Label30: TLabel;
    m_tools: TMenuItem;
    m_update_fehlgeschlagen: TMenuItem;
    m_ProtokollDatenbankaktualisierung: TMenuItem;
    m_Anmeldeprotokoll: TMenuItem;
	  procedure SpeedButton5Click(Sender: TObject);
	  procedure SpeedButton7Click(Sender: TObject);
	  procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure ToolButton_neuClick(Sender: TObject);
	  procedure ToolButton_deleteClick(Sender: TObject);
	  procedure BitBtn2Click(Sender: TObject);
	  procedure BitBtn_bes_hauptClick(Sender: TObject);
	  procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure DBLookupComboBox_impfung_typDropDown(Sender: TObject);
    procedure DBLookupComboBox_impfung_typCloseUp(Sender: TObject);
    procedure DBLookupComboBox_impfung_stoffEnter(Sender: TObject);
    procedure DBLookupComboBox_impfung_stoffExit(Sender: TObject);
    procedure dblookupcombobox_LISTE_laborDropDown(Sender: TObject);
    procedure dblookupcombobox_LISTE_laborCloseUp(Sender: TObject);
    procedure PageControl_auswertungChange(Sender: TObject);
    procedure BitBtn_exportClick(Sender: TObject);
    procedure TreeViewChanging(Sender: TObject; Node: TTreeNode;
		 var AllowChange: Boolean);
    procedure PageControl_auswertungChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure DBRadioGroup_posnegExit(Sender: TObject);
	  procedure BitBtn3Click(Sender: TObject);
	  procedure BitBtn_untwahlClick(Sender: TObject);
    procedure BitBtn_labClick(Sender: TObject);
	  procedure BitBtn_impfClick(Sender: TObject);
	  procedure CheckListBox_sortClickCheck(Sender: TObject);
    procedure DBgridEXT1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Tree_anzeigeDblClick(Sender: TObject);
    procedure anzeigen1Click(Sender: TObject);
	  procedure ToolButton_openClick(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure sortierenaufsteigen1Click(Sender: TObject);
    procedure sortierenabsteigend1Click(Sender: TObject);
    procedure demarkieren1Click(Sender: TObject);
	  procedure FilterneuClick(Sender: TObject);
	  procedure FilterloeschenClick(Sender: TObject);
	  procedure m_AbfragespeichernClick(Sender: TObject);
	  procedure m_AbfrageladenClick(Sender: TObject);
	  procedure Auswertungverlassen1Click(Sender: TObject);
	  procedure ToolButton1Click(Sender: TObject);
	  procedure m_AbfragenbearbeitenClick(Sender: TObject);
	  procedure Abfragezurcksetzen1Click(Sender: TObject);
	  procedure BitBtn1Click(Sender: TObject);
	  procedure FormDestroy(Sender: TObject);
	  procedure BitBtn_e1Click(Sender: TObject);
	  procedure DBEdit_groesseChange(Sender: TObject);
	  procedure einfacheAbfragen1Click(Sender: TObject);
    procedure Abfrage_zurueck(modus:integer);
    procedure abfrage_neu(id,s_inhalt:string);
    procedure CheckBox_negativClick(Sender: TObject);
    procedure m_update_fehlgeschlagenClick(Sender: TObject);
    procedure m_ProtokollDatenbankaktualisierungClick(Sender: TObject);
    procedure m_AnmeldeprotokollClick(Sender: TObject);
	private
	  { Private-Deklarationen }
		sql_query,abfr_name:string;
		th:inithread;
        db,de:string;
        excel_checked:boolean;
		{sperr_parent, vorsorge_parent, impfung_parent, labor_parent:integer;}
		procedure uil_sperren(nummer:integer);
		 procedure baum_einlesen(berechtigung:integer);
		 procedure baummarkieren;
		 procedure notebooksperren;
		 procedure notebookentsperren;
		 procedure auswertung;
		 procedure exportieren;
		 procedure baum_filter;
		 procedure toolbar_aktivieren;
      procedure besonderheit(parent:boolean);
      function untwahl(bereich:integer):integer;
			procedure abf_name_anzeig;
		 procedure krit_del;
		 procedure krit_ins;
       function einfache_Abfragen: boolean;
       procedure zeig_anzahl;
       function auswertung_check:boolean;
	public
	  { Public-Deklarationen }
		berechtigung:integer;
    procedure treekennzeichnen(knoten:ttreenode; index:integer);
	  Function sql_join_aus_liste(liste:tintegerlist):string;
	  Function sql_storno_aus_liste(liste:tintegerlist):string;
	 procedure listen_ini;
    procedure auswertung_laden;
	  procedure auswertung_speichern;
	  procedure notebook_refresh;

  end;

	function klein_zuerst (item1, item2 : pointer): integer ;
  function datum_us(datum:string):string;

var
	Form_auswertung: TForm_auswertung;

implementation

uses a_main,  a_icd, a_besonderheiten, a_kalender, a_unt_wahl,
  a_tabellen, a_speichern, a_f_wahl, a_einfache_abfragen;

{$R *.DFM}

constructor inithread.create;
begin
	inherited create(true); // CreateSuspended = true
	freeOnTerminate := true;
	Priority:=tpLowest;
end;

procedure iniThread.execute;
begin
	form_main.ToolButtonauswertung.Enabled:=false;
	form_auswertung.listen_ini;
	form_main.ToolButtonauswertung.Enabled:=true;
end;

procedure tform_auswertung.baum_einlesen(berechtigung:integer);
var
filterfeld:string;
i,a_modus:integer;
query:string;
nummer,p_nummer:integer;
neu:boolean;
begin
with datamodul do
begin
	 form_main.treedeaktivieren(treeview);
   query:='delete from sql_baum';
   mysql_d.sql_exe(query);
   q_sql_baum.refresh;
	 q_sql_baum.First;
	 i:=0;
	 while not q_sql_baum.eof do q_sql_baum.delete; //hart l�schen da Datei neu aufgebaut wird


	 query:=format('select * from sql_baum_vorlage where berechtigung >=%d',[berechtigung]);
	 sql_new(false,q_12, query,'nummer');

	 q_12.first;
	 while not q_12.Eof do
	 begin

		nummer:=q_12.findfield('nummer').asinteger;
        case nummer of
        20000..20200: neu:=show_impfung;
		    21000..21200: neu:=show_labor;
        22000..22080: neu:=show_ambulanz;
        else neu:=true;
        end;

        if neu then
        begin
          inc(i);
          neuer_datensatz(q_sql_baum,
          ['nummer','laufnummer','name','parent','modus','quelle','feldname','filter','s_einheit','tabelle','filterfeld','sql_join'],
          [nummer,i,q_12['name'],q_12['parent'],q_12['modus'],q_12['quelle'],q_12['feldname'],q_12['filter'],q_12['s_einheit'],q_12['tabelle'],q_12['filterfeld'],q_12['sql_join']] );
        end;
        q_12.next;
	 end;


	 if berechtigung=0 then // Auswertung-Gesamt
	 begin
		 q_bereich.First;
		 while not q_bereich.eof do
		 begin
			 //a_modus:=-1;
			 filterfeld:='';
			 a_modus:=8;
			 case q_bereich['modus'] of
			 0:begin
					filterfeld:='';
					a_modus:=8;
				end;
			 1: begin
					filterfeld:='wert';
					a_modus:=9;
				 end;
			 2:begin
					filterfeld:='i_berlist';
					a_modus:=10;
				end;
			 5:begin  //rr
					filterfeld:='i_1,i_2';
					a_modus:=12;
				end;
			 6:begin //groesse //gewicht
					filterfeld:='f_1,f_2,f_3';
					a_modus:=19;
				end;
			 7: begin //lufu
					filterfeld:='f_1,f_2,f_3';
					a_modus:=20;
				end;
          8: begin //wai
               filterfeld:='wert';
					 a_modus:=9;
             end;
			 end;
			 inc(i);
			 if a_modus>=0 then
			 begin
				nummer:=5100+q_bereich['nummer'];
				if q_bereich['parent']=0 then
					p_nummer:=5000+q_bereich['parent']
					else p_nummer:= 5100+q_bereich['parent'];
				neuer_datensatz(q_sql_baum,
                	['nummer','laufnummer','name','parent','modus','quelle','feldname','filter','s_einheit','tabelle','filterfeld','sql_join'],
				[nummer,i,q_bereich['name'],p_nummer,a_modus,'ds_berlist','listenfeld','i_bereich='+inttostr(q_bereich['nummer']),q_bereich['bezeichner_dim'],'akt_untersuchung',filterfeld,100] );
			 end;
			q_bereich.next;
		 end;
	 end;
	datamodul.q_sql_baum.Refresh;
	form_main.Bereicheinlesen(treeview,datamodul.q_sql_baum,'sql_laufnummer','name');
	treeview.Selected:=treeview.Items[0];
end;
end;



procedure TForm_auswertung.SpeedButton5Click(Sender: TObject);
begin
treeview.FullExpand;
end;

procedure TForm_auswertung.SpeedButton7Click(Sender: TObject);
begin
treeview.Fullcollapse;
end;

procedure TForm_auswertung.TreeViewChange(Sender: TObject;
  Node: TTreeNode);
begin
	baum_filter;
	toolbar_aktivieren;
end;

procedure tform_auswertung.toolbar_aktivieren;
begin

	//if datamodul.q_sql_abfragen.Eof and datamodul.q_sql_abfragen.Bof then
 if kein_datensatz_dataset( datamodul.q_sql_abfragen,'name') then
	begin
		notebooksperren;
		toolbutton_delete.Enabled:=false;
		if treeview.selected.HasChildren then toolbutton_neu.Enabled:=false else toolbutton_neu.Enabled:=true;
		Filterloeschen.enabled:=false;
	end
	else
	begin
		notebookentsperren;
		toolbutton_delete.Enabled:=true;
		if (not groupbox_erweitert.Visible) and (not treeview.selected.HasChildren ) then toolbutton_neu.Enabled:=false else toolbutton_neu.Enabled:=true;
		Filterloeschen.enabled:=true;
	end;

end;


procedure tform_auswertung.baum_filter;
var
nodedata:pnodepointer;
bernum,modus:integer;
source:tdatasource;
begin

	  //notebooksperren;
	  //datamodul.q_sql_baum.IndexName:='';
	  if treeview.selected=nil then exit;
	  if treeview.selected.data=nil then exit;
	  if datamodul.q_sql_baum.Active=false then exit;
	 try
		nodedata:=treeview.Selected.data;
		bernum:=nodedata^.nummer;
	 except
		bernum:=0;
	 end;
		//if bernum >22000 then exit; was soll das??
		//datamodul.q_sql_baum.first;
		datamodul.q_sql_baum.locate('nummer',bernum,[]);
		datamodul.q_sql_abfragen.Filtered:=false;
		datamodul.q_sql_abfragen.Filter:='sql_baum='+inttostr(datamodul.q_sql_baum['nummer']);
		datamodul.q_sql_abfragen.Filtered:=true;
		modus:=datamodul.q_sql_baum['modus'];
		if modus>notebook.Pages.Count-1 then exit;

		notebook.pageindex:=modus; //einstellen des formulars s. .baum_einlesen

		case modus of
		1:	begin
			end;
		2:	begin
			end;
		3:	begin
			dbcombo_num_befund_liste.ztab_name:=datamodul.q_sql_baum['quelle'];
			dbcombo_num_befund_liste.zfield_text:=datamodul.q_sql_baum['feldname'];
			dbcombo_num_befund_liste.lookup_laden;

			end;
		4:	begin
			end;
		5:	begin
			end;
		6:	begin
			end;
		7:	begin
			end;
		8:	begin
			  // befund auff�llig
			end;
		9:	begin
			  //befund auff�llig + wert
			end;

		10:begin
			//befund auff�llig + liste
			source:=datamodul.finddatasource(datamodul.q_sql_baum['quelle'])  ;
			if source=nil then exit;
			if datamodul.q_sql_baum['filter']<>null then
			begin
				source.DataSet.filtered:=false;
				source.DataSet.filter:=datamodul.q_sql_baum['filter'];
				source.DataSet.filtered:=true;
			end;
			dblookupcombobox_befund_LISTE.listField:='';
			dblookupcombobox_befund_liste.listSource:= source;
			dblookupcombobox_befund_LISTE.listField:=datamodul.q_sql_baum['feldname'];
			end;
	 11:begin

		  end;
	 14: begin
				 //lab_show;
		  end;
	 18:
		  begin
			  notebook_refresh;
		  end;
		end;
end;


procedure TForm_auswertung.ToolButton_neuClick(Sender: TObject);
begin
	if not treeview.Selected.HasChildren then  krit_ins;
end;

procedure tform_auswertung.krit_ins;
var i,nr:integer;
s_name,query,snr:string;
begin
with datamodul do
begin
	if (not groupbox_erweitert.Visible) and (treeview.selected.stateindex=3) then       //mehrfachauswahl ausgeblendet und ausgew�hlt
	begin
		krit_del;
		exit;
	end;


  query:=format('select max(nummer) from %s',[sql_abfragen_temp]);
  snr:=mysql_d.Feldinhalt(query,0) ;
  if snr='' then nr:=1
  else
    nr:=strtoint(snr)+1;

	s_name:=q_sql_baum['name'];


	q_sql_abfragen.last;    //wenn mehrere abfragen f�r item
	if q_sql_abfragen['name']<> null then
		i:=strtoint(copy(q_sql_abfragen['name'],length(q_sql_abfragen['name']),1))
	else i:=0;
	if i=9 then
	begin
		showmessage('Maximal 9 Abfragen erlaubt.');
		exit;
	end;
	s_name:=s_name+'~'+inttostr(i+1);
	neuer_datensatz(q_sql_abfragen,
	  ['nummer','sql_baum','modus','tabelle','feld','sql_join','bereich','relation',
		  'auffaellig','d_typ','name','alle','s_einheit'],
	  [nr,q_sql_baum['nummer'],q_sql_baum['modus'], q_sql_baum['tabelle'],q_sql_baum['filterfeld'],
	  q_sql_baum['sql_join'], q_sql_baum['nummer']-5100 ,'=',-1,0,s_name,0,q_sql_baum['s_einheit']]);

	  if notebook.pageindex=4 then q_sql_abfragen['d_typ']:=1;
    if notebook.pageindex=18 then q_sql_abfragen['memo']:=firmen_berechtigung;

	  if q_sql_abfragen.state in [dsedit, dsinsert] then q_sql_abfragen.post;
	  treekennzeichnen(treeview.selected,3);
	  toolbar_aktivieren;
	  notebook_refresh;
    q_sql_abfragen.Refresh;
  q_sql_abfragen.locate('nummer',nr,[]);
	end;
end;

procedure tform_auswertung.treekennzeichnen(knoten:ttreenode; index:integer);
begin
	if knoten.Parent<>nil then treekennzeichnen(knoten.parent,index);
	knoten.StateIndex:=index;
end;

procedure TForm_auswertung.ToolButton_deleteClick(Sender: TObject);
begin
if not treeview.Selected.HasChildren then krit_del;
end;

procedure tform_auswertung.krit_del;
begin
	//if not (datamodul.q_sql_abfragen.Eof and datamodul.q_sql_abfragen.Bof) then
    if datensatz( datamodul.q_sql_abfragen) then
	begin
	  datamodul.q_sql_abfragen.Delete; //hart l�schen tempor�re datei
	  baummarkieren;
	  //notebooksperren;
	  toolbar_aktivieren;
	end;
end;


function TForm_auswertung.auswertung_check:boolean;
begin
result:=true;

end;

procedure TForm_auswertung.auswertung;
function feld(asfeld:string):string;
var position:integer;
begin
position:=pos(' as ',asfeld);
result:=copy(asfeld,1,position-1);
end;

var
sql_text, sql_select,sql_join,sql_storno, sql_where,sql_w_beginn,sql_order,relation,
{vergleich,}hk,und_oder,le{,s_bereich, s_auffaellig},
icd_beginn,icd_ende,beginn,ende, datum_beginn, datum_ende,rel,
string1, string2,string3,string4,f_name_alt,sql_group,pf,pn,pv,query_proband,query,temp_dat,sql_where_proband,q1,f1,subwhere:string;
pg:tdate;
liste:tintegerlist;
i,nummer:integer;
daten:pnodepointer;
knoten: ttreenode;
relst: array [0..2] of string;

begin

if not auswertung_check then exit;

try
liste:=tintegerlist.create;
relst[0]:=' and ';
relst[1]:=' or ';
relst[2]:=' not ';
screen.Cursor:=crhourglass;
hk:=chr(39);
le:=' ';




f_name_alt:='###';
with datamodul do
begin
  
	if q_a3.state<> dsinactive then q_a3.Refresh;
	sql_select:='';
	sql_group:=' group by ';
	sql_order:='';
	for i:=0 to tree_anzeige.Items.Count-1 do
	begin

		if Tree_anzeige.Items[i].stateindex=5 then //ausgew�hlt zum sortieren aufsteigend
		begin
		 knoten :=Tree_anzeige.items[i];
		 daten:=knoten.Data;
		 nummer:=daten^.nummer;
		 q_a1.locate('nummer',integer(nummer),[]);
		 if q_a1['sortierung']<>null then
			sql_order:=sql_order+q_a1['sortierung'] +' , ';
		end;

		if Tree_anzeige.Items[i].stateindex=6 then //ausgew�hlt zum sortieren absteigend
		begin
		 knoten :=Tree_anzeige.items[i];
		 daten:=knoten.Data;
		 nummer:=daten^.nummer;
		 q_a1.locate('nummer',integer(nummer),[]);
		 if q_a1['sortierung']<>null then
			sql_order:=sql_order+q_a1['sortierung'] +' desc , ';
		end;
	end;
	sql_order:=copy(sql_order,1,length(sql_order)-3);//komma weg
	if sql_order<>'' then sql_order:=' order by '+sql_order;
	for i:=0 to tree_anzeige.Items.Count-1 do
	begin
		if Tree_anzeige.Items[i].stateindex>0 then //ausgew�hlt
		begin
		 knoten :=Tree_anzeige.items[i];
		 daten:=knoten.Data;

			q_a1.locate('nummer',integer(daten^.nummer),[]);
			if q_a1['dbfeld1']<>'' then sql_select:=sql_select+q_a1['dbfeld1'] +' , ';
			if q_a1['dbfeld1']<>'' then sql_group:=sql_group+feld(q_a1['dbfeld1'])+' , ';

			if q_a1['dbfeld2']<>'' then  sql_select:=sql_select+q_a1['dbfeld2'] +' , ';
			if q_a1['dbfeld3']<>'' then  sql_select:=sql_select+q_a1['dbfeld3'] +' , ';

			if q_a1['i_join1']>0 then liste.aktualisieren(q_a1['i_join1'],1);
			if q_a1['i_join2']>0 then liste.aktualisieren(q_a1['i_join2'],1);
		end;
	end;


  if checkbox_negativ.Checked then
  begin
    sql_select:='select mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum ';

	  sql_group:=' group by mitarbeiter.name , mitarbeiter.vorname , mitarbeiter.geb_dat ' ;

  end
  else
  begin
      if sql_select='' then
      begin
        screen.Cursor:=crdefault;
        pagecontrol_auswertung.ActivePage:=tabsheet_anzeige;
        showmessage('Noch keine Anzeige-Felder ausgew�hlt');
        exit;
      end;

      case radiogroup_anzeigeart.ItemIndex of
        0:  sql_select :='select '+sql_select  ;
        1:	begin
          sql_select :='select count(*) as Anzahl , '+sql_select;
          end;
        2:	begin
          sql_select :='select count(*) as Anzahl , '+sql_select;
          sql_order:=' order by anzahl ';
          end;
        3:	begin
          sql_select :='select count(*) as Anzahl , '+sql_select;
          sql_order:=' order by anzahl desc';
          end;
      end;

      sql_select:=copy(sql_select,1,length(sql_select)-3);//komma weg
      sql_group:=copy(sql_group,1,length(sql_group)-3);//komma weg

  end;


	sql_select:=sql_select+' from mitarbeiter ';


	sql_where:='  where ( mitarbeiter.nummer>'+hk+'0'+hk;
	sql_w_beginn:=sql_where;
	//q_sql_abfragen.IndexName:='abfrage_join';
   q_sql_abfragen.Filtered:=false;
	q_sql_abfragen.first;
	while not q_sql_abfragen.eof do
	begin
		if q_sql_abfragen['relation']<>null then
		begin
			if pos(f_name_alt, q_sql_abfragen['name'])>0 then und_oder :=' or ' else und_oder:=' ) and ( ';
			{if pos(f_name_alt, q_sql_abfragen['name'])>0 then und_oder :=' or '
					 else und_oder:=' )'+relst[ radiogroup_r1.itemindex] +'( ';}
			f_name_alt:=copy(q_sql_abfragen['name'],1,pos('~',q_sql_abfragen['name']));
			if q_sql_abfragen['sql_join']>0 then liste.aktualisieren(q_sql_abfragen['sql_join'],1);
			case q_sql_abfragen['modus'] of
			0: string1:='';
			1: begin
				string1:=floattostr(q_sql_abfragen['wert']);
				//if q_sql_abfragen['relation']='like' then string1:='%'+string1+'%';
				sql_where:=sql_where+und_oder
				  +q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+le+q_sql_abfragen['relation']+le
				  +hk+string1+hk;
				end;
			2: begin
         //proband
				  string1:=q_sql_abfragen['string'];
				  //if q_sql_abfragen['relation']='like' then string1:='%'+string1+'%';
				  subwhere:=q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+le+q_sql_abfragen['relation']+le
				  +hk+string1+hk;
          sql_where:=sql_where+und_oder+subwhere;
          if sql_where_proband='' then sql_where_proband:=subwhere else  sql_where_proband:=sql_where_proband+und_oder+subwhere;
				end;
			3: begin
			  if (q_sql_abfragen['alle']<>null) AND (q_sql_abfragen['alle']=1) THEN
				  begin
					string1:='0';
					relation:='>=' // alle
					end
				else
					begin
						string1:=inttostr(q_sql_abfragen['liste']);
						relation:='=';
					 end;
			  sql_where:=sql_where+und_oder
			  +q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+relation //q_sql_abfragen['relation']
			  +hk+string1+hk;
			  end;
			4: begin  //datum
				if q_sql_abfragen['datum_beginn']<>null then datum_beginn:=sql_datetostr(q_sql_abfragen['datum_beginn']) else datum_beginn:='"1000-1-1"';
				if q_sql_abfragen['datum_ende']<>null then datum_ende:=sql_datetostr(q_sql_abfragen['datum_ende']) else datum_ende:='"3000-1-1"' ;

				string1:=q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld'];
				sql_where:=sql_where+und_oder
				  +format('( %0:s >= %1:s ) and ( %0:s <= %2:s)',[string1,datum_beginn,datum_ende]) ;


			  END;
			5: begin
				string1:=q_sql_abfragen['icd_schluessel'];

				if copy(string1,5,3)<>'' then
					begin
						icd_ende:=' and '+q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+' < '+hk+copy(string1,5,3)+hk;
						rel:='>=';
					end
				 else
					begin
						icd_ende:='';
						rel:='=';
					end;
				icd_beginn:=q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+rel+ hk+copy(string1,1,3)+hk;
				sql_where:=sql_where+und_oder
				+'( '+icd_beginn+icd_ende+' ) ';
				end;
			6: begin
				string1:=inttostr(q_sql_abfragen['i_besonderheit']);
				sql_where:=sql_where+und_oder
			        +q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+q_sql_abfragen['relation']
				+hk+string1+hk;
				end;
			7: begin
        string1:=inttostr(q_sql_abfragen['i_besonderheit']);
				sql_where:=sql_where+und_oder
			        +q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+q_sql_abfragen['relation']
				+hk+string1+hk;
				end;
			8:begin
				//befund leer
                string1:=inttostr(q_sql_abfragen['bereich']);
				if q_sql_abfragen['auffaellig']>=0 then
						string2:=' akt_untersuchung.auffaellig='
						+hk+inttostr(q_sql_abfragen['auffaellig'])+hk+ ' and'
						else string2:='';
                                                sql_where:=sql_where+und_oder+
				'('+string2+' akt_untersuchung.i_bereich='+hk+string1+hk+' ) ';
				end;
			9:	begin
				//befund wert
				string1:=inttostr(q_sql_abfragen['bereich']);
				if q_sql_abfragen['auffaellig']>=0 then
						string2:=' akt_untersuchung.auffaellig='
						+hk+inttostr(q_sql_abfragen['auffaellig'])+hk+ ' and'
						else string2:='';
				 if q_sql_abfragen['wert']<>null  then
					string3:=' akt_untersuchung.wert'+q_sql_abfragen['relation']
					+hk+floattostr(q_sql_abfragen['wert'])+hk+'and'
					else string3:='';
				sql_where:=sql_where+und_oder+
				'('+string3+string2+' akt_untersuchung.i_bereich='+hk+string1+hk+' ) ';
				end;
			10:begin
				// befund liste
				string1:=inttostr(q_sql_abfragen['bereich']);
        q1:=format('select reli from bereich where nummer=%s',[string1]);
        f1:=mysql_d.Feldinhalt(q1,0); // rechts- links auspr�gung

				if q_sql_abfragen['auffaellig']>=0 then
						string2:=' akt_untersuchung.auffaellig='
						+hk+inttostr(q_sql_abfragen['auffaellig'])+hk+ ' and'
						else string2:='';
            
				if q_sql_abfragen['liste']<>null  then
        begin
					string3:=' (akt_untersuchung.i_berlist=' +hk+inttostr(q_sql_abfragen['liste'])+hk+') ';
					if f1='1' then string3:=string3+' or (akt_untersuchung.i_berlist_rechts='+hk+inttostr(q_sql_abfragen['liste'])+hk+')';
          string3:=string3+' and';
        end
				else string3:='';

				sql_where:=sql_where+und_oder+
				'('+string3+string2+' akt_untersuchung.i_bereich='+hk+string1+hk+' ) ';
				end;
			12:begin //Blutdruck
				if q_sql_abfragen['auffaellig']>=0 then
						string1:=' akt_untersuchung.auffaellig='
						+hk+inttostr(q_sql_abfragen['auffaellig'])+hk+ ' and'
						else string1:='';
				if q_sql_abfragen['i_1']>0 then
					string2:=' akt_untersuchung.i_1'+q_sql_abfragen['relation']
					+hk+inttostr(q_sql_abfragen['i_1'])+hk+'and'
					else string2:='';
				if q_sql_abfragen['i_2']>0  then
					string3:=' akt_untersuchung.i_2'+q_sql_abfragen['relation']
					+hk+inttostr(q_sql_abfragen['i_2'])+hk+'and'
					else string3:='';
				if q_sql_abfragen['i_3']>0  then
					string4:=' akt_untersuchung.i_3'+q_sql_abfragen['relation']
					+hk+inttostr(q_sql_abfragen['i_3'])+hk+'and'
					else string4:='';

				sql_where:=sql_where+und_oder+
				'('+string1+string2+string3+string4+' akt_untersuchung.i_bereich='+hk+inttostr(q_sql_abfragen['bereich'])+hk+' ) ';
				end;
			 19: begin  //groesse gewicht bmi
					  if q_sql_abfragen['auffaellig']>=0 then
						string1:=' akt_untersuchung.auffaellig='
						+hk+inttostr(q_sql_abfragen['auffaellig'])+hk+ ' and'
						else string1:='';
					 if q_sql_abfragen['f_1']>0 then
						 string2:=' akt_untersuchung.f_1'+q_sql_abfragen['relation']
						 +hk+floattostr(q_sql_abfragen['f_1'])+hk+'and'
						 else string2:='';
					 if q_sql_abfragen['f_2']>0  then
						 string3:=' akt_untersuchung.f_2'+q_sql_abfragen['relation']
						 +hk+floattostr(q_sql_abfragen['f_2'])+hk+'and'
						 else string3:='';
					 if q_sql_abfragen['f_3']>0  then
						 string4:=' akt_untersuchung.f_3'+q_sql_abfragen['relation']
						 +hk+floattostr(q_sql_abfragen['f_3'])+hk+'and'
						 else string4:='';
					 sql_where:=sql_where+und_oder+
					 '('+string1+string2+string3+string4+' akt_untersuchung.i_bereich='+hk+inttostr(q_sql_abfragen['bereich'])+hk+' ) ';

					 end;
			  20: begin  //lufu
					  if q_sql_abfragen['auffaellig']>=0 then
						string1:=' akt_untersuchung.auffaellig='
						+hk+inttostr(q_sql_abfragen['auffaellig'])+hk+ ' and'
						else string1:='';
					 if q_sql_abfragen['f_1']>0 then
						 string2:=' akt_untersuchung.f_1'+q_sql_abfragen['relation']
						 +hk+floattostr(q_sql_abfragen['f_1'])+hk+'and'
						 else string2:='';
					 if q_sql_abfragen['f_2']>0  then
						 string3:=' akt_untersuchung.f_2'+q_sql_abfragen['relation']
						 +hk+floattostr(q_sql_abfragen['f_2'])+hk+'and'
						 else string3:='';
					 if q_sql_abfragen['f_3']>0  then
						 string4:=' akt_untersuchung.f_3'+q_sql_abfragen['relation']
						 +hk+floattostr(q_sql_abfragen['f_3'])+hk+'and'
						 else string4:='';
					 sql_where:=sql_where+und_oder+
					 '('+string1+string2+string3+string4+' akt_untersuchung.i_bereich='+hk+inttostr(q_sql_abfragen['bereich'])+hk+' ) ';

					 end;
			11: //geburtsdatum
			 begin
				if q_sql_abfragen['datum_beginn']<>null then datum_beginn:=sql_datetostr(q_sql_abfragen['datum_beginn']) else datum_beginn:='"1000-1-1"';
				if q_sql_abfragen['datum_ende']<>null then datum_ende:=sql_datetostr(q_sql_abfragen['datum_ende']) else datum_ende:='"3000-1-1"' ;
				beginn:='mitarbeiter.geb_dat >= '+hk+datum_beginn+hk;
				ende:= ' and mitarbeiter.geb_dat <= '+hk+datum_ende+hk;
				sql_where:=sql_where+und_oder
				+'( '+beginn+ende+' ) ';
			 end;

			//'select * from stammdaten   join untersuchung  on (untersuchung.stammdaten = stammdaten.nummer) join akt_untersuchung on (akt_untersuchung.i_untersuchung = untersuchung.nummer) join bereich on (bereich.nummer = akt_untersuchung.i_bereich) join berlist on (berlist.nummer = akt_untersuchung.i_berlist)where ( stammdaten.nummer>'0' ) and ( ( akt_untersuchung.i_berlist='13' and  akt_untersuchung.i_bereich='26' )  )'

			  13:begin
					//impfung
					string1:=inttostr(q_sql_abfragen['bereich']);

					if q_sql_abfragen['liste']<>null  then
					string2:=' impfung.i_typ='
					+hk+inttostr(q_sql_abfragen['liste'])+hk
					else string2:='';
					if (q_sql_abfragen['wert']<>null) and (q_sql_abfragen['wert']>0)  then
					string3:=' and impfung.i_stoff='
					+hk+floattostr(q_sql_abfragen['wert'])+hk
					else string3:='';

					if (q_sql_abfragen['alle']<>null) and (q_sql_abfragen['alle']=1) then
					begin
              	string2:=' impfung.i_typ>=0 ';
						string3:='';
              end;
				sql_where:=sql_where+und_oder+
				'('+string2+string3+' ) ';
				  end;
			  14: begin
					//labor
					if q_sql_abfragen['liste']<>null  then
					string1:=' labor.i_typ='
					+hk+inttostr(q_sql_abfragen['liste'])+hk
					else string1:='';

					if q_sql_abfragen['wert']<>null  then
					string2:=' and labor.wert '+q_sql_abfragen['relation']
					+hk + floattostr(q_sql_abfragen['wert'])+hk
					else string2:='';

					if (q_sql_abfragen['alle']<>null) and (q_sql_abfragen['alle']=1) then
					begin
						string1:=' labor.i_typ>=0 ';
						string2:='';
					end;

					sql_where:=sql_where+und_oder+
						'('+string1+string2+' ) ';
					end;

           15: begin //untersuchung
                if (q_sql_abfragen['alle']<>null) AND (q_sql_abfragen['alle']=1) THEN
                   begin
                    string1:='0';
                    relation:='>=' // alle
                    end
                 else
                    begin
                       string1:=inttostr(q_sql_abfragen['liste']);
								      relation:='=';
                    end;
                sql_where:=sql_where+und_oder
					  +q_sql_abfragen['tabelle']+'.'+q_sql_abfragen['feld']+relation //q_sql_abfragen['relation']
                +hk+string1+hk;
		          end;
				 16:begin //boolean
						string1:=q_sql_abfragen['tabelle'];
						string2:=q_sql_abfragen['feld'];
						string3:=floattostr(q_sql_abfragen['wert']);
						sql_where:=sql_where +format( ' and (%s.%s = "%s") ',[string1,string2,string3]);
					 end;
				 17:begin //archiv
						string1:=q_sql_abfragen['tabelle'];
						string2:=q_sql_abfragen['feld'];
						if q_sql_abfragen['wert'] = 1 then string1:='X' else string1:='-';
						sql_where:=sql_where +format( ' and archiv="%s" ',[string1]);
					 end;
				18: begin //firma
						  string1:=q_sql_abfragen['memo'];
						  if string1<>'' then sql_where:=sql_where +format( ' and mitarbeiter.i_firma in (%s) ',[string1]);
              if string1<>'' then
                if sql_where_proband='' then sql_where_proband:=format( ' mitarbeiter.i_firma in (%s) ',[string1]) else  sql_where_proband:=sql_where_proband+') and '+format( ' mitarbeiter.i_firma in (%s) ',[string1]);
              //sql_where_proband:=  format( ' mitarbeiter.i_firma in (%s) ',[string1]);
					 end;

			end;
			{if not storno_show then
			begin
				string1:=q_sql_abfragen['tabelle'];
				if string1='' then string1:='mitarbeiter';
				sql_where:=sql_where +format( ' and (%s.storno=0 ) ',[string1]);
			end;}

		end;
		q_sql_abfragen.Next;
	end;

	 if sql_where=sql_w_beginn then
	begin
		screen.Cursor:=crdefault;
		pagecontrol_auswertung.ActivePage:=tabsheet_anzeige;
		showmessage('Noch keine Kriterien ausgew�hlt');
		exit;
	end;

     //pr�fen ob firma ausgew�hlt  , wenn nicht gew�htl dann alle firmen
   if pos('mitarbeiter.i_firma in (',sql_where)=0 then
   begin
      sql_where:= sql_where+ format(' ) and (mitarbeiter.i_firma in (%s))',[firmen_berechtigung]);
      sql_where_proband:= sql_where_proband +format(' ) and (mitarbeiter.i_firma in (%s))',[firmen_berechtigung]);
   end
   else
   begin
    sql_where:=sql_where+')';
    //sql_where_proband:=sql_where_proband+')';
   end;

	 //abh�ngige joins dazuf�gen
	 //sql_storno:=sql_storno_aus_liste(liste);

	 sql_join:=sql_join_aus_liste(liste);

	 sql_storno:=sql_storno_aus_liste(liste);

	if not storno_show then
			begin
				string1:=q_sql_abfragen['tabelle'];
				if string1='' then string1:='mitarbeiter';
				sql_where:=sql_where +format( ' and (mitarbeiter.storno=0 ) %s ',[sql_storno]);
        sql_where_proband:=sql_where_proband +' ) and (mitarbeiter.storno=0 )  ';

			end;


	sql_text:=sql_select+sql_join+sql_where;
	if radiogroup_anzeigeart.ItemIndex<>0 then sql_text :=sql_text +sql_group;

	sql_text:=sql_text+sql_order;
	try
		q_a2.Close;
		 sql_normieren(sql_text);
		q_a2.sql.text:=sql_text;
		q_a2.open;
	except
		showmessage('Fehler bei der Abfrage'+
						chr(13)+' -Anzeige-Felder aus Befunden, Vorsorgen, Impfungen und Laboruntersuchungen d�rfen nicht gemischt werden.'+
						chr(13)+' -->Einzelne Abfrage-Felder herausnehmen.');
	end;

  if  checkbox_negativ.checked then
  begin
  try
    temp_dat:=format('temp_proband_%d',[user_id]);
    query:=format('drop table if exists %s',[temp_dat]);
    mysql_d.sql_exe(query);
    query_proband:='select mitarbeiter.name as Name, mitarbeiter.vorname as Vorname, geb_dat as Geburtsdatum from mitarbeiter '+sql_join+'  where (' +sql_where_proband +' group by name,vorname,geburtsdatum';
    query:= format('CREATE TABLE %s %s',[temp_dat,query_proband]);
    mysql_d.sql_exe(query);
    query:=format('select * from %s',[temp_dat]);
    datamodul.sql_new(false,q_7,query,'');
    q_7.First;
    while not q_7.Eof do
    begin
      //pf:=q_a2.findfield('i_firma').asstring;
      pn:=q_7.findfield('name').asstring;
      pv:=q_7.findfield('vorname').asstring;
      pg:=q_7.findfield('geburtsdatum').AsDateTime ;
      //Locate('Company;Contact;Phone', VarArrayOf(['Sight Diver', 'P', '408-431-1000']), [loPartialKey]);

      if  q_a2.Locate('name,vorname,geburtsdatum',vararrayof([pn,pv,pg]),[loCaseInsensitive]) then
      begin
       q_7.Delete;
      end
      else
      q_7.Next;
     end;
     sql_new(false,q_a2,query,'');
    //mysql_d.drop_table(tab_name);

   except
    showmessage('Fehler bei der Verneinung'+#13+'Es m�ssen Proband-Name, Proband-Vorname und Proband-Geburtsdatum angezeigt werden');
    pagecontrol_auswertung.ActivePage:=TabSheet_anzeige;
   end;
   end;

end;
finally
liste.Free;

screen.Cursor:=crdefault;
end;
end;


Function tform_auswertung.sql_join_aus_liste(liste:tintegerlist):string;
var i,nummer:integer;
    verbind,tabname:string;
	  listeneintrag:pintegerliste;
    tabellen:tstringlist;
begin
try

tabellen:=tstringlist.Create;
with datamodul do
begin
	 if liste.Count>0 then
	 begin
	 i:=0 ;
	 repeat
		listeneintrag:=liste[i];
		nummer:=listeneintrag^.int1;
		q_sql_join.first;
		q_sql_join.locate('nummer',nummer,[]);
		if q_sql_join['benoetigt']>0 then liste.aktualisieren(q_sql_join['benoetigt'],1);
		inc(i);
	 until i>liste.count-1;
		 //sortieren
	 liste.Sort(klein_zuerst);
	 end;
	 //sql_join erzeugen


	for i:=0 to liste.count-1 do
	 begin
		listeneintrag:=liste[i];
		nummer:=listeneintrag^.int1;
		q_sql_join.first;
		if q_sql_join.locate('nummer',nummer,[]) then
      begin
        verbind:= q_sql_join.findfield('verbindung').asstring;
        tabname:=n_tes_wort(1,' ',verbind);
        if tabellen.IndexOf(tabname)=-1 then
          result:=result+ ' left outer join '+ verbind
        else showmessage('Die Tabelle '+tabname+' darf nicht mehrfach ausgew�hlt sein. Entfernen Sie die entsprechenden Anzeige-Felder'+ #13+'-->Das Ergebnis der Abfrage ist nicht korrekt!');

        tabellen.Add(tabname);
      end
      else showmessage('Join nicht gefunden: '+inttostr(nummer));
	 end;
end;
finally
tabellen.Clear;
tabellen.Free;
end;
end;


Function tform_auswertung.sql_storno_aus_liste(liste:tintegerlist):string;
var i,nummer:integer;
	  listeneintrag:pintegerliste;
	  verbindung,tabelle,query:string;
begin
with datamodul do
begin
	 //sortieren
	 liste.Sort(klein_zuerst);
	 //sql_join erzeugen
	for i:=0 to liste.count-1 do
	 begin
		listeneintrag:=liste[i];
		nummer:=listeneintrag^.int1;
		if q_sql_join.locate('nummer',nummer,[]) then
		begin
			if q_sql_join.findfield('benoetigt').asinteger=0 then
			begin
			  verbindung:=q_sql_join.findfield('verbindung').asstring;
			  tabelle:=n_tes_wort(1,' ',verbindung);
              if  pos (tabelle,'firma,mitarbeiter,untersuchung,impfung, labor,ambulanz,termine,befunde,dokumente, diagnosen,artikel,rechnung,besonderheiten ')>0  then result:=result+format(' and (%s.storno=0) ',[tabelle]);
			  {query:=format('select * from %s limit 1',[tabelle]);
			  sql_new(false,q_4,query,'');
			  if q_4.FindField('storno')<>nil then result:=result+format(' and (%s.storno=0) ',[tabelle])}
			end;
		end;
	 end;
end;
end;


procedure TForm_auswertung.BitBtn2Click(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
form_icd:=tform_icd.create(self);
try
if form_icd.showmodal=mrok then
begin
	datamodul.q_sql_abfragen.edit;
	datamodul.q_sql_abfragen['icd_schluessel']:=datamodul.q_icd_main['icd_schluessel'];
	datamodul.q_sql_abfragen['string']:=datamodul.q_icd_main['icd_text'];
end;
finally
form_main.timer_icd.enabled:=true;
form_icd.release;
end;
end;

procedure TForm_auswertung.BitBtn_bes_hauptClick(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
	besonderheit(true);
end;

procedure TForm_auswertung.besonderheit(parent:boolean);
begin
	datamodul.sql_new(true,datamodul.q_besonderheitenListe_tree,'select * from besonderheitenliste', 'reihenfolge');
	form_besonderheiten:=tform_besonderheiten.create(self);
   form_besonderheiten.ini(datamodul.q_besonderheitenListe_tree,'besonderheit');
try
	form_besonderheiten.tree.DragMode:=dmmanual;
	form_besonderheiten.multiselect:=false;
	form_besonderheiten.ToolBar.Visible:=false;
	form_besonderheiten.Panel_bottom.Visible:=true;

if form_besonderheiten.showmodal=mrok then
begin
  if  parent then
  begin
		if form_besonderheiten.Tree.Selected.HasChildren then
     begin
        datamodul.q_sql_abfragen.edit;
        datamodul.q_sql_abfragen['i_besonderheit']
           :=datamodul.q_besonderheitenListe_tree['nummer'];
        datamodul.q_sql_abfragen['string']
           :=datamodul.q_besonderheitenListe_tree['besonderheit'];
      end
      else showmessage('Kein Hauptkriterium');
  end
  else
  begin
     if form_besonderheiten.Tree.Selected.HasChildren then
     showmessage('Bitte als Hauptkriterium eintragen')
     else
     begin
      datamodul.q_sql_abfragen.edit;
      datamodul.q_sql_abfragen['i_besonderheit']
         :=datamodul.q_besonderheitenListe_tree['nummer'];
      datamodul.q_sql_abfragen['string']
			:=datamodul.q_besonderheitenListe_tree['besonderheit'];
 		end;
	end;

end;
finally
	form_besonderheiten.release;
end;
end;

function klein_zuerst (item1, item2 : pointer): integer ;
var
wert1,wert2: pintegerliste;
begin
wert1:=item1;
wert2:=item2;
result:=wert1^.int1-wert2^.int1;
//	result:=pintegerliste(item1)^.int1 -pintegerliste(item2)^.int1;
end;


function datum_us(datum:string):string;
begin
 result := copy(datum,4,2);
 result := result + '/';
 result := result + copy(datum,1,2);
 result := result + '/';
 result := result + copy(datum,7,4);
end;


procedure TForm_auswertung.DBRadioGroup_posnegExit(Sender: TObject);
begin
if datamodul.q_sql_abfragen.State in [dsedit,dsinsert]
	then begin
		datamodul.q_sql_abfragen.post;
		dbedit_laborwert.Refresh;
	end;
end;


procedure TForm_auswertung.SpeedButton3Click(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
if form_kalender.showmodal =mrok then
	begin
	datamodul.q_sql_abfragen.edit;
	datamodul.q_sql_abfragen['datum_beginn'] :=int(form_kalender.auswahlkalender.Date);
	end;

end;

procedure TForm_auswertung.SpeedButton1Click(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
if form_kalender.showmodal =mrok then
  begin
  datamodul.q_sql_abfragen.edit;
	datamodul.q_sql_abfragen['datum_ende'] :=int(form_kalender.auswahlkalender.Date);
  end;
end;

procedure TForm_auswertung.DBLookupComboBox_impfung_typDropDown(
  Sender: TObject);
begin
datamodul.q_typ.Filtered:=false;
	datamodul.q_typ.Filter:='bereich=2';
	datamodul.q_typ.Filtered:=true;
end;

procedure TForm_auswertung.DBLookupComboBox_impfung_typCloseUp(
  Sender: TObject);
begin
datamodul.q_typ.Filtered:=false;
end;

procedure TForm_auswertung.DBLookupComboBox_impfung_stoffEnter(
  Sender: TObject);
begin
with datamodul do
begin
	q_impfstoff.filtered:=false;
	if q_sql_abfragen['liste']<> null then
		q_impfstoff.filter:='i_typ='+inttostr(q_sql_abfragen['liste']);
	q_impfstoff.filtered:=true;
  q_impfstoff.first;
end;
end;

procedure TForm_auswertung.DBLookupComboBox_impfung_stoffExit(
  Sender: TObject);
begin
	datamodul.q_impfstoff.filtered:=false;
  datamodul.q_impfstoff.filter:='';
end;



procedure TForm_auswertung.dblookupcombobox_LISTE_laborDropDown(
	Sender: TObject);
begin
  datamodul.q_typ.Filtered:=false;
	datamodul.q_typ.Filter:='bereich=3';
  datamodul.q_typ.Filtered:=true;
end;

procedure TForm_auswertung.dblookupcombobox_LISTE_laborCloseUp(
  Sender: TObject);
begin
   datamodul.q_typ.Filtered:=false;
end;


procedure tform_auswertung.uil_sperren(nummer:integer);
begin
	//hier vorsoge_parent etc setzen
end;


procedure tform_auswertung.baummarkieren;
var
nummer,j:integer;
nodedata:pnodepointer;
knoten:TTreeNode;
begin
with datamodul do
begin
	q_sql_abfragen.Filtered:=false;
	for j:=0 to treeview.Items.count-1 do  treeview.Items[j].stateindex:=-1;
	q_sql_abfragen.first;
	while not q_sql_abfragen.eof do
	begin
		if q_sql_abfragen['sql_baum']<>null then
		nummer:=q_sql_abfragen['sql_baum']
		else nummer:=-1;
		for j:=0 to treeview.Items.count-1 do
		begin
			knoten:=treeview.Items[j];
			nodedata:=knoten.Data;
			if nodedata^.nummer=nummer then
			begin
				knoten.StateIndex:=3;
				treekennzeichnen(knoten,3);
				uil_sperren(nummer);
			end;
		end;

		q_sql_abfragen.next;
	end;

end;
	baum_filter;
end;

procedure tform_auswertung.notebooksperren;
//var i:integer;
begin
	 //for i:=0 to notebook.ControlCount-1 do form_main.sperren(notebook.Components[i]) ;
	 	 form_main.sperren( notebook.components[notebook.PageIndex]) ;

//	 form_main.sperren(notebook);
end;


procedure tform_auswertung.notebookentsperren;
//var i:integer;
begin
	 //for i:=0 to notebook.ControlCount-1 do form_main.entsperren(notebook.Components[i]) ;
	 //notebook.PageIndex
	 form_main.entsperren( notebook.components[notebook.PageIndex]) ;

		 //form_main.entsperren(notebook);
end;


procedure TForm_auswertung.PageControl_auswertungChange(Sender: TObject);

begin
if (pagecontrol_auswertung.ActivePage=tabsheet_ergebnis) and tabsheet_filtern.TabVisible then
begin
	toolbutton_neu.Enabled:=false;
	toolbutton_delete.Enabled:=false;
	filterneu.Enabled:=false;
	filterloeschen.Enabled:=false;
	auswertung;
  zeig_anzahl;

end;


if pagecontrol_auswertung.ActivePage=tabsheet_anzeige then
begin
	toolbutton_neu.Enabled:=false;
	toolbutton_delete.Enabled:=false;
  filterneu.Enabled:=false;
	filterloeschen.Enabled:=false;
end;


if pagecontrol_auswertung.ActivePage=tabsheet_filtern then
begin
	toolbutton_neu.Enabled:=true;
	toolbutton_delete.Enabled:=true;
	filterneu.Enabled:=true;
	filterloeschen.Enabled:=true;
  baum_filter;
end;

end;




procedure TForm_auswertung.exportieren;
var
zeile:string;
i,zaehler:integer;
f:extended;
begin
with datamodul, form_main do
begin
	com.excel_connect('Auswertung');

  excel.visible:=false;
	q_a2.First;
	for i:=0 to q_a2.fieldcount-1 do
	begin
		//com.xl_PutStrAt(1,i+1,q_a2.Fields[i].fieldname);
                //Excel.Sheets[1].Cells[1,i+1].Value :=q_a2.Fields[i].fieldname;
		//excel.PutStrAt(1,i+1,q_a2.Fields[i].fieldname);
     com.excel_PutStrAt(1,i+1,q_a2.Fields[i].fieldname);
	end;

	zaehler:=2;
	while not q_a2.eof do
	begin
		zeile:='';

		for i:=0 to q_a2.fieldcount-1 do
		begin

		  case q_a2.Fields[i].datatype of
		  ftInteger,ftSmallint,ftWord  :com.excel_PutIntAt(zaehler,i+1,q_a2.Fields[i].asinteger); //excel.PutIntAt(zaehler,i+1,q_a2.Fields[i].asinteger);
		  ftfloat: begin
						f:=q_a2.Fields[i].asfloat;
						com.excel_PutExtAt(zaehler,i+1,f);
						//com.xl_PutExtAt(zaehler,i+1,f);
					  end;
		  else
			//excel.PutStrAt(zaehler,i+1,q_a2.Fields[i].asstring);
			//com.xl_PutStrAt(zaehler,i+1,q_a2.Fields[i].asstring);
                        com.excel_PutStrAt(zaehler,i+1,q_a2.Fields[i].asstring);
		  end;
		end;
		inc(zaehler);
		q_a2.Next;
	end;
  com.exel_optimale_hoehe_breite;
  excel.visible:=true;
	com.excel_disconnect;
end;
end;


procedure TForm_auswertung.BitBtn_exportClick(Sender: TObject);
begin
exportieren;
end;

procedure TForm_auswertung.TreeViewChanging(Sender: TObject;
	Node: TTreeNode; var AllowChange: Boolean);
begin
try
	  if datamodul.q_sql_abfragen.state in [dsedit,dsinsert] then  datamodul.q_sql_abfragen.post;
except
end;
end;

procedure TForm_auswertung.PageControl_auswertungChanging(Sender: TObject;
	var AllowChange: Boolean);
begin
try
	if datamodul.q_sql_abfragen.state in [dsedit,dsinsert] then  datamodul.q_sql_abfragen.post;
except
end;  
end;

procedure TForm_auswertung.BitBtn3Click(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
besonderheit(false);//child
end;


function TForm_auswertung.untwahl(bereich:integer):integer;
var query,s:string;
begin
try
with datamodul do
begin
	result:=-1;
	sql_new(true,q_typ_tree,format('select * from typ where bereich = %d',[bereich]), 'reihenfolge');
	form_untwahl:=tform_untwahl.Create(self);
	form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.Tree.bereich_wert:=bereich;
	form_untwahl.tree.mysqlquery:=datamodul.q_typ_tree;
	form_untwahl.tree.mysql_feld:='untersuchung';
	form_untwahl.tree.liste_lesen;
	form_untwahl.tree.DragMode:=dmmanual;
	form_untwahl.multiselect:=false;
	form_untwahl.Panel_botttom.visible:=true;
  form_untwahl.Notebook.visible:=false;
	form_untwahl.ToolBar.visible:=false;
	//form_main.combolesen(form_untwahl.ComboBox_untersucher,q_untersucher,'untersucher','');
	case bereich of
	1: form_untwahl.caption:='Bescheinigung';
	2: form_untwahl.caption:='Impfung';
	3: form_untwahl.caption:='Labor';
	end;

	if form_untwahl.showmodal<>mrok then exit;
  result:=q_typ_tree['nummer'];
  query:=format('select untersuchung from typ where nummer=%d',[result]);
  s:=mysql_d.Feldinhalt(query,0);
  datamodul.q_sql_abfragen.edit;
  datamodul.q_sql_abfragen.findfield('string').asstring:=copy(s,1,30);
 datamodul.q_sql_abfragen['liste']:=result;
 datamodul.q_sql_abfragen['s_einheit']:=q_typ_tree['einheit']; //'string'
 datamodul.q_sql_abfragen.post;
end;
finally
  form_untwahl.release;
end;
end;

procedure TForm_auswertung.BitBtn_untwahlClick(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
 untwahl(1);

end;

procedure TForm_auswertung.BitBtn_labClick(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
 untwahl(3);
 //lab_show;
end;

procedure TForm_auswertung.BitBtn_impfClick(Sender: TObject);
begin
if kein_datensatz(datamodul.q_sql_abfragen) then exit;
 untwahl(2);
end;


procedure TForm_auswertung.CheckListBox_sortClickCheck(Sender: TObject);
begin
 //	if checklistbox_sort.Checked[checklistbox_sort.ItemIndex] then
 //   checklistbox.Checked[checklistbox_sort.ItemIndex]:=true;
end;

procedure TForm_auswertung.DBgridEXT1DblClick(Sender: TObject);
var
 ende: boolean;
begin
ende:=false;
with datamodul do
begin
	if (q_a2.FindField('name')=nil) or (q_a2.FindField('vorname')=nil) or (q_a2.FindField('geburtsdatum')=nil) then
  	begin
      if (q_a2.FindField('rechnungsnummer')<>nil) then
      begin
        ende:= form_main.gehezu_rechnung(q_a2.FindField('rechnungsnummer').asstring) ;

      end;
  	//showmessage('Es m�ssen "Name", "Vorname" und "Geburtsdatum" ausgew�hlt sein.');
   //exit;
  end
  else ende:= form_main.gehezu_name_Stammdaten(q_a2['name'],q_a2['vorname'],q_a2['geburtsdatum']);

end;
if ende then modalresult:=mrok;
end;

procedure TForm_auswertung.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
  excel_checked:=false;
	berechtigung:=2;

	if b.b_string_silent('Auswertung-Gesamt')>-1 then berechtigung:=0
   else
	 if b.b_string_silent('Auswertung-Auswertung-Arbeitssicherheit')>-1 then berechtigung:=2
   else
   if b.b_string_silent('Proband-Bescheinigung')>-1 then berechtigung:=2
   else
   if b.b_string_silent('Proband-Impfung')>-1 then berechtigung:=2 
   else
   if b.b_string_silent('Proband-Bescheinigung-AG')>-1 then berechtigung:=2 ;

   if berechtigung= 2 then
   begin
       tabsheet_filtern.TabVisible:=false;
       tabsheet_anzeige.TabVisible:=false;
   	   pagecontrol_auswertung.TabIndex:=2;
   end;

	listen_ini;
    db:='';//datetostr(now());
    de:=db;
	//th:=inithread.Create;
  
	//th.Resume;

end;



procedure tform_auswertung.listen_ini;
var
query:string;
begin
	query:=format('select * from %s where berechtigung >=%d order by reihenfolge',[sql_felder_temp,berechtigung]);//###sql_felder_temp  'sql_felder'
	if berechtigung=2 then exit;
	datamodul.sql_new(false,datamodul.q_a1,query,'');
	tree_anzeige.liste_lesen;

	pagecontrol_auswertung.ActivePage:=tabsheet_filtern;
	PageControl_auswertungChange(Self);
	baum_einlesen(berechtigung);

	baummarkieren;
 // notebooksperren;
	treeview.Selected:=treeview.Items[0];
	datamodul.sql_new(false,datamodul.q_a3,'select tabelle from sql_abfragen','');


end;

procedure TForm_auswertung.Tree_anzeigeDblClick(Sender: TObject);
var tree:ttreeview_ext;
query:string;
a:integer;
begin
with datamodul do
begin
	tree:=tree_anzeige;
	if tree.selected=nil then exit;


	if tree.selected.stateindex<>3 then tree.markieren(true,3)
	else tree.markieren(true,-1);
end;
end;

procedure TForm_auswertung.anzeigen1Click(Sender: TObject);
begin
	Tree_anzeigeDblClick(Sender);
end;

procedure TForm_auswertung.ToolButton_openClick(Sender: TObject);
var
query:string;
//stream:tstream;
begin
with datamodul do
begin
if datamodul.q_sql_abfragen.state in [dsedit,dsinsert] then  datamodul.q_sql_abfragen.post;

   query:='select * from abfragen order by name';
	 sql(true,q_1,query,'');
	 form_tabellen:=tform_tabellen.Create(self);
	 form_tabellen.Caption:='Auswertungen speichern und laden';
   form_tabellen.Notebook.PageIndex:=8;
   form_tabellen.ToolButton_edit.visible:=false;
   form_tabellen.ToolButton_save.visible:=false;


	 form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Name';
	 form_tabellen.DBGrid_tabellen.Columns[1].width:=200;
	 form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	 form_tabellen.DBEdit_abfragen_name.DataField:='name';
	 form_tabellen.DBMemo_abfragen.DataField:='memo';
   grid_einstellen(form_tabellen.dbgrid_tabellen);
   form_tabellen.showmodal;
	 form_tabellen.Release;

end;
end;

procedure tform_auswertung.auswertung_laden;
var
 tab_abfragen, tab_felder,query:string;
 stream:tstream;
 liste:tstringlist;
 i:integer;
begin
   if not datensatz(datamodul.q_1) then exit;
	 abf_name_anzeig;

	 tab_abfragen:='a_'+getbigint_str(datamodul.q_1,'nummer')+'ab';
	 tab_felder:='a_'+getbigint_str(datamodul.q_1,'nummer')+'fe';


   mysql_d.db_von_db(tab_abfragen, sql_abfragen_temp,false);

	 datamodul.q_sql_abfragen.refresh;
	 baummarkieren;



   mysql_d.db_von_db(tab_felder,sql_felder_temp,false);

	 datamodul.q_a1.refresh;
	 tree_anzeige.liste_lesen;

	 try
		 for i:=0 to tree_anzeige.Items.Count-1 do tree_anzeige.items[i].stateindex:=-1;
		 liste:=tstringlist.Create;
		 stream:=datamodul.q_1.CreateBlobStream(datamodul.q_1.FieldByName('liste'),bmRead);
		 liste.LoadFromStream(stream);
		 for i:=0 to liste.Count-1 do
      begin
			 if i<tree_anzeige.items.count then
			 begin
					tree_anzeige.items[i].stateindex:=strtoint(liste[i]);
			 end;
      end
		 //baum_einlesen(berechtigung);
	 finally
		stream.Free;
		liste.free;
	 end;

	 radiogroup_anzeigeart.ItemIndex:=datamodul.q_1['art'];
	 if datamodul.q_1['experte']=1 then groupbox_erweitert.visible:=true else groupbox_erweitert.visible:=false ;


end;


procedure tform_auswertung.auswertung_speichern;
var
 query,tab_abfragen, tab_felder:string;
 stream:tstream;
 liste:tstringlist;
 i:integer;
begin
	 abf_name_anzeig;

	 tab_abfragen:='a_'+getbigint_str(datamodul.q_1,'nummer')+'ab';
	 tab_felder:='a_'+getbigint_str(datamodul.q_1,'nummer')+'fe';
	 datamodul.q_1.edit;



   //mysql_d.db_von_db(tab_abfragen,sql_abfragen_temp,'nummer',false);
   mysql_d.db_von_db(sql_abfragen_temp,tab_abfragen,false);



   //mysql_d.db_von_db(tab_felder,sql_felder_temp,'nummer',false);
   mysql_d.db_von_db(sql_felder_temp,tab_felder,false);

	 try
		 liste:=tstringlist.Create;
		 for i:=0 to tree_anzeige.Items.Count-1 do liste.add(inttostr(tree_anzeige.items[i].stateindex));
		 stream:=datamodul.q_1.CreateBlobStream(datamodul.q_1.FieldByName('liste'),bmWrite);
		 liste.SaveToStream(stream);
	 finally
		stream.Free;
		liste.free;
	 end;

	 datamodul.q_1['art']:=radiogroup_anzeigeart.ItemIndex;
	 if groupbox_erweitert.visible then datamodul.q_1['experte']:=1 else datamodul.q_1['experte']:=0 ;
	 datamodul.q_1.post;

end;

procedure tform_auswertung.abf_name_anzeig;
begin

  begin
	abfr_name:=datamodul.q_1['name'];
	form_auswertung.Caption:='Auswertung: '+abfr_name;
  end


end;


procedure TForm_auswertung.SpeedButton8Click(Sender: TObject);
begin
tree_anzeige.fullcollapse;
end;

procedure TForm_auswertung.SpeedButton6Click(Sender: TObject);
begin
tree_anzeige.FullExpand;
end;

procedure TForm_auswertung.sortierenaufsteigen1Click(Sender: TObject);
begin
tree:=tree_anzeige;
if tree.selected.stateindex<>5 then tree.markieren(true,5)
else tree.markieren(true,-1);
end;

procedure TForm_auswertung.sortierenabsteigend1Click(Sender: TObject);
begin
tree:=tree_anzeige;
if tree.selected.stateindex<>6 then tree.markieren(true,6)
else tree.markieren(true,-1);
end;

procedure TForm_auswertung.demarkieren1Click(Sender: TObject);
begin
	tree.markieren(true,-1);
end;

procedure TForm_auswertung.FilterneuClick(Sender: TObject);
begin
	 krit_ins;
	//ToolButton_neuClick(Sender);
end;

procedure TForm_auswertung.FilterloeschenClick(Sender: TObject);
begin
	krit_del;
	//ToolButton_deleteClick(Sender);
end;

procedure TForm_auswertung.m_AbfragespeichernClick(Sender: TObject);
var
query:string;
begin

if (b.b_string_silent('Auswertung-Gesamt')<2)
		and (b.b_string_silent('Auswertung-Arbeitssicherheit')<2) then exit;
with datamodul do
try
	query:=format('select * from abfragen order by name',[]);
	 sql(true,q_1,query,'');
	form_speichern:=tform_speichern.create(self);
	if berechtigung=1 then form_speichern.RadioGroup_berechtigung.Enabled:=false;

	if q_1.Locate('name',abfr_name,[loCaseInsensitive	]) then
	begin
		 form_speichern.memo_speichern.text:=q_1['memo'];
		 form_speichern.edit_speichern.text:=q_1['name'];
		 form_speichern.RadioGroup_berechtigung.ItemIndex:=q_1['berechtigung'];
	end;

	if form_speichern.ShowModal<>mrok then exit;

	if q_1.Locate('name',form_speichern.edit_speichern.text,[loCaseInsensitive	]) then
	begin
		if Messagedlg(format('"%s" �berschreiben ',[form_speichern.edit_speichern.text]),mtConfirmation,
			 [mbOK,mbcancel],0)<>mrok then exit;
		 q_1.edit;
		 q_1['berechtigung']:=form_speichern.RadioGroup_berechtigung.ItemIndex;
		 q_1['memo']:=form_speichern.memo_speichern.text;
		 q_1.post;

	end
	else
	begin
		neuer_datensatz(q_1,['nummer','name','memo','berechtigung'],
			[null,form_speichern.edit_speichern.text,form_speichern.memo_speichern.text,form_speichern.RadioGroup_berechtigung.ItemIndex]);
		q_1.post;
	end;

	auswertung_speichern;
finally
	form_speichern.Release;
end;

end;

procedure TForm_auswertung.m_AbfrageladenClick(Sender: TObject);
var
 query:string;
//stream:tstream;
begin
with datamodul do
begin
if datamodul.q_sql_abfragen.state in [dsedit,dsinsert] then  datamodul.q_sql_abfragen.post;

	 query:=format('select * from abfragen where berechtigung>=%d order by name',[berechtigung]);
	 sql(true,q_1,query,'');
	 form_tabellen:=tform_tabellen.Create(self);
	 form_tabellen.Caption:='Auswertungen laden';
	 form_tabellen.Notebook.PageIndex:=8;
	 form_tabellen.Toolbar.visible:=false;
	 form_tabellen.panel_b.visible:=true;
	 form_tabellen.ToolButton_neu.visible:=false;

	 form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Name';
	 form_tabellen.DBGrid_tabellen.Columns[1].width:=200;
	 form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	 form_tabellen.DBEdit_abfragen_name.DataField:='name';
	 form_tabellen.DBMemo_abfragen.DataField:='memo';
	 grid_einstellen(form_tabellen.dbgrid_tabellen);
	 if form_tabellen.showmodal=mrok then auswertung_laden;


	 form_tabellen.Release;
   pagecontrol_auswertung.activepage:=tabsheet_filtern;
end;
end;

procedure TForm_auswertung.Auswertungverlassen1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_auswertung.ToolButton1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_auswertung.m_AbfragenbearbeitenClick(Sender: TObject);
var
 query:string;
begin
if (b.b_string_silent('Auswertung-Gesamt')<2)
		and (b.b_string_silent('Auswertung-Arbeitssicherheit')<2) then exit;
with datamodul do
begin
if datamodul.q_sql_abfragen.state in [dsedit,dsinsert] then  datamodul.q_sql_abfragen.post;

	 query:='select * from abfragen order by name';
	 sql(true,q_1,query,'');
	 form_tabellen:=tform_tabellen.Create(self);
	 form_tabellen.Caption:='Auswertungen bearbeiten';
	 form_tabellen.Notebook.PageIndex:=8;
	 form_tabellen.Toolbar.visible:=true;
	 form_tabellen.ToolButton_neu.visible:=false;
	 form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Name';
	 form_tabellen.DBGrid_tabellen.Columns[1].width:=200;
	 form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
	 form_tabellen.DBEdit_abfragen_name.DataField:='name';
	 form_tabellen.DBMemo_abfragen.DataField:='memo';
	 grid_einstellen(form_tabellen.dbgrid_tabellen);

	 form_tabellen.showmodal;


	 form_tabellen.Release;

end;

end;

procedure TForm_auswertung.Abfragezurcksetzen1Click(Sender: TObject);
begin
  abfrage_zurueck(1);
end;

procedure TForm_auswertung.Abfrage_zurueck(modus:integer);
begin

	 datamodul.q_sql_abfragen.filter:='';
	 datamodul.q_sql_abfragen.first;
	 application.processmessages;
	 while (not datamodul.q_sql_abfragen.eof) do
	 begin
		 datamodul.q_sql_abfragen.delete; //hart l�schen temp. Datei
	 end;

	 if modus=1 then tree_anzeige.alles_demarkieren;

	 application.ProcessMessages;
	 baummarkieren;
	 application.ProcessMessages;

end;


procedure TForm_auswertung.BitBtn1Click(Sender: TObject);
var
filter:string;
begin

form_f_wahl:=tform_f_wahl.create(self);
if form_f_wahl.ShowModal=mrok then
begin
	filter:=form_f_wahl.firmen_nummern;
	datamodul.q_sql_abfragen.edit;
	datamodul.q_sql_abfragen['memo']:=filter;
	datamodul.q_sql_abfragen.post;
end;
form_f_wahl.Release;
notebook_refresh;

end;


procedure tform_auswertung.notebook_refresh;
var
filter,query:string;
begin
	case notebook.PageIndex of
	18:begin
			if datensatz(datamodul.q_sql_abfragen) then
			begin
			  filter:=datamodul.q_sql_abfragen['memo'];
			  if filter='' then
			  begin
				 filter:= firmen_berechtigung;
				 {datamodul.q_sql_abfragen.edit;
				 datamodul.q_sql_abfragen['memo']:=filter;
				 datamodul.q_sql_abfragen.post;}
			  end;
			  query:=format('select * from firma where nummer in (%s)',[filter]);
			  datamodul.sql(true,datamodul.q_3,query,'firma');
			  if filter<>'' then form_main.itemslesen(listbox_firma.items,datamodul.q_3,'firma','');
			end
			else form_main.itemsdeaktivieren(listbox_firma.items);
		end;
	end;
end;



procedure TForm_auswertung.BitBtn_e1Click(Sender: TObject);
begin
with datamodul do
begin
	if   GroupBox_erweitert.Visible=false then
	begin
	  GroupBox_erweitert.Visible:=true;
	  bitbtn_e1.caption:='Zuklappen';
	end
	else
	begin
	  GroupBox_erweitert.Visible:=false;
	  bitbtn_e1.caption:='Erweitert';
	  q_sql_abfragen.first;
	  while not q_sql_abfragen.eof do q_sql_abfragen.Delete;//hart l�schen
	  baummarkieren;
	end;
	toolbar_aktivieren;
end;
end;

procedure TForm_auswertung.DBEdit_groesseChange(Sender: TObject);
var
gr,ge:real;
begin
	if (dbedit_groesse.Text='') or (dbedit_gewicht.Text='') then exit;
	gr:=strtofloat(dbedit_groesse.Text);
	ge:=strtofloat(dbedit_gewicht.text);
	if ge>3 then ge:=ge/100;
	if gr*ge=0 then exit;
	dbedit_bmi.text:=floattostr(ge/(gr*gr));
end;



procedure TForm_auswertung.einfacheAbfragen1Click(Sender: TObject);
begin
pagecontrol_auswertung.ActivePage:=TabSheet_ergebnis;
if einfache_abfragen then exportieren;
zeig_anzahl;

end;


function TForm_auswertung.einfache_Abfragen:boolean;
var
    firmen,temp_tab,temp_tab_a,query,ausgeschieden,abfrage_untersucher:string;
    datbeginn, datende,abfrage:string;
    firma, konto,mas,s:string;
    stunden_ist,stunden_soll, differenz,p,t: real;
  	i,i_firma, i_krit, i_zeitkonto,i_untersucher:integer;
  	datvon, datbis,sd,d1,d2:tdate;
  	b: boolean;
    tag,monat,jahr:word;
begin
with datamodul do
begin
screen.Cursor:=crhourglass;
try
    p:=0;
    if db='' then db:='01.01.1900';
    if de='' then de:='01.01.1900';
    form_einfache_abfragen:=tform_einfache_abfragen.create(self);
    form_einfache_abfragen.berechtigung:=berechtigung;
    form_einfache_abfragen.dateedit_beginn.text:=(db);
    form_einfache_abfragen.dateedit_ende.text:=(de);
    if db='01.01.1900' then form_einfache_abfragen.Zeiteneinstellen;
    form_einfache_abfragen.CheckBox_excel.Checked:=excel_checked;


	if form_einfache_abfragen.showmodal=mrok then
	begin
    excel_checked:=form_einfache_abfragen.CheckBox_excel.Checked;
    if (not form_einfache_abfragen.CheckBox_ausgeschieden.Checked) then ausgeschieden:=' and ( (mitarbeiter.ende>="2100-01-01") or (mitarbeiter.ende<="2000-01-01")) '
                                                                  else ausgeschieden:='';
                                                                  //(mitarbeiter.ende="2999-12-31") or
    if (not form_einfache_abfragen.CheckBox_archiviert.Checked) then ausgeschieden:=ausgeschieden +' and (mitarbeiter.archiv="-")';

    db:=(form_einfache_abfragen.dateedit_beginn.text);
    de:=(form_einfache_abfragen.dateedit_ende.text);
    //temp_tab:='temp_tab'+user_name;
  temp_tab:=format('temp_tab_%d',[user_id]);
	firmen:=form_einfache_abfragen.ffilter;
	datbeginn:=sql_datetostr(strtodate(form_einfache_abfragen.dateedit_beginn.text));
	datende:=sql_datetostr(strtodate(form_einfache_abfragen.dateedit_ende.text));

  abfrage_untersucher:='';
  i_untersucher:=form_einfache_abfragen.DBCombo_num_zeitabr.getnummer;
  if i_untersucher>0 then
  begin
    abfrage_untersucher:=format(' (firma_sollist.i_untersucher=%d) and ',[i_untersucher]);
  end;


	if prg_typ=3 then
	case form_einfache_abfragen.listbox_abfrage.itemindex of
	  0: begin //gesamtumsatz
			sql_query:='select rechnung.datum as Datum, rechnung.renu as Rechnungsnummer, rechnung.eurobetrag as netto,round(( rechnung.eurobetrag *rechnung.mws/100 +rechnung.eurobetrag)*100)/100 as brutto, mitarbeiter.name as Name, mitarbeiter.vorname as Vorname, ';
			sql_query:=sql_query+'mitarbeiter.geb_dat as Geburtsdatum from mitarbeiter left join rechnung on (mitarbeiter.nummer=rechnung.i_mitarbeiter)';
			sql_query:=sql_query+format(' where rechnung.datum>=%s and rechnung.datum<=%s and rechnung.storno=0',[datbeginn,datende]);
		  end;
	  1: begin //behandlung
			sql_query:=format('select untersuchung.datum as Datum , typ.untersuchung as Behandlung, untersuchung.euro as netto,round((untersuchung.euro*%d/100+untersuchung.euro)*100)/100 as brutto, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , ',[mws]);
			sql_query:=sql_query+' mitarbeiter.geb_dat as Geburtsdatum from mitarbeiter left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
			sql_query:=sql_query+format(' ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=4 and untersuchung.storno=0) and (mitarbeiter.storno=0) ',[datbeginn,datende]);

		  end;
	  2: begin //verkauf
			sql_query:='select artikel.datum as Datum , materialstamm.art_name as Artikel, artikel.menge as Menge,materialstamm.inhalt as Inhalt, materialstamm.einheit as Einheit , artikel.netto_m_rabatt as netto, ';
			sql_query:=sql_query+format('round((artikel.netto_m_rabatt*%d/100+artikel.netto_m_rabatt)*100)/100 as brutto, ',[mws]);
			sql_query:=sql_query+' mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum from mitarbeiter left outer join artikel on (artikel.i_mitarbeiter = mitarbeiter.nummer) left outer join materialstamm on ';
			sql_query:=sql_query+format(' (artikel.i_material=materialstamm.nummer)  where  ( artikel.datum >= %s ) and ( artikel.datum <= %s) and ( artikel.storno=0) and (mitarbeiter.storno=0) ',[datbeginn,datende]);


		  end;
	  3: begin //material gesamt
				sql_query:='select materialstamm.art_name as Name, materialstamm.art_nr as Nummer, materialstamm.inhalt as Inhalt,materialstamm.einheit as Einheit, materialstamm.lagerbestand as Lagerbestand, materialstamm.mindestbestand as Mindestbestand, ';
				sql_query :=sql_query+' materialstamm.einkaufspreis as Einkaufspreis, materialstamm.verkaufspreis as Verkaufspreis, lieferanten.name as Lieferant from materialstamm left join lieferanten on (materialstamm.i_lieferant=lieferanten.nummer)' ;
		  end;
	  4: begin //material zum Bestellen
				sql_query:='select materialstamm.art_name as Name, materialstamm.art_nr as Nummer, materialstamm.inhalt as Inhalt,materialstamm.einheit as Einheit, materialstamm.lagerbestand as Lagerbestand, materialstamm.mindestbestand as Mindestbestand, ';
				sql_query :=sql_query+' materialstamm.einkaufspreis as Einkaufspreis, materialstamm.verkaufspreis as Verkaufspreis, lieferanten.name as Lieferant from materialstamm left join lieferanten on (materialstamm.i_lieferant=lieferanten.nummer)';
				sql_query:=sql_query+' where materialstamm.lagerbestand<=materialstamm.mindestbestand' ;
		  end;
	  5: begin // Geburtstagsliste
			  sql_query:='select name as Name, vorname as Vorname, geb_dat as Geburtstag, (YEAR(curRENT_DATE)-YEAR(geb_dat))-(RIGHT(CURRENT_DATE,5)<RIGHT(geb_dat,5)) as Lebensalter from mitarbeiter where ';
			  sql_query:= sql_query+format(' (month(%0:s)*31+dayofmonth(%0s)  <=month(geb_dat)*31+dayofmonth(geb_dat)) and (month(%1:s)*31+dayofmonth(%1:s)  >=month(geb_dat)*31+dayofmonth(geb_dat)) order by (month(geb_dat)*31+dayofmonth(geb_dat))  ',[datbeginn,datende]);
		  end;
	 end
else
  begin
  i:=-1;
  abfrage:=form_einfache_abfragen.listbox_abfrage.Items[form_einfache_abfragen.listbox_abfrage.itemindex];
  if abfrage= 'Probanden' then i:=0;
  if abfrage= 'Geplante Bescheinigungen' then  i:=1;
  if abfrage= 'Geplante Impfungen' then i:=2;
  if abfrage= 'BG-Statistik' then i:=3;
  if abfrage= 'Rechnungen Proband' then i:=4;
  if abfrage= 'Rechnungen Firma' then i:=5;
  if abfrage= 'unbezahlte Rechnungen Proband' then i:=6;
  if abfrage= 'unbezahlte Rechnungen Firma' then i:=15;
  if abfrage= 'Geplante Labor-Untersuchungen' then i:=7;
  if abfrage= 'Abgeschlossene Bescheinigungen' then i:=8;
  if abfrage= 'Durchgef�hrte Impfungen' then i:=9;
  if abfrage= 'Durchgef�hrte Labor-Untersuchungen' then i:=10;
  if abfrage= 'Zeiten-Firma-Konten-Summe' then i:=11;
  if abfrage= 'Zeiten-Einzelnachweis' then i:=12;
  if abfrage= 'Zeitbilanz' then i:=13;
  if abfrage= 'Zeitbilanz erweitert' then i:=20;//13;
  if abfrage= 'Durchgef�hrte Untersuchungen' then i:=14;
  if abfrage= 'Abgeschlossene Bescheinigungen (nicht gedruckt)' then i:=16;
  if abfrage= 'Abgeschlossene Bescheinigungen AG' then i:=17;
  if abfrage= 'Geplante Bescheinigungen AG' then  i:=18;
  if abfrage= 'Statistik Arbeitsunf�lle' then  i:=19;


  //if abfrage= 'Zeitbilanz Technik' then i:=14;
	case i of
	  0: begin //Probanden
		  sql_query:='select firma.firma as F_name, mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , mitarbeiter.strasse as Strasse , mitarbeiter.plz as PLZ , mitarbeiter.ort as Wohnort from mitarbeiter ';
		  sql_query:=sql_query+format(' left outer join firma on (mitarbeiter.i_firma = firma.nummer)  where  (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and (mitarbeiter.storno=0) %s',[firmen,ausgeschieden]);
          datamodul.sql_new(false, datamodul.q_a2,sql_query,'');

		 end;
	  1: begin  //geplante bescheinigungen
		  sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , untersuchung.datum as Vo_Datum , left(typ.untersuchung,250) as Vo_Typ, ';
      sql_query:=sql_query+' mitarbeiter.p_nummer as Personalnummer, abteilung.kuerzel as Abt_Kuerzel, abteilung.langtext as Abt_Name  from mitarbeiter left outer join abteilung on (mitarbeiter.i_abteilung=abteilung.nummer) ';
		  sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
		  sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=1 and untersuchung.storno=0) and (mitarbeiter.storno=0)  %s ',[firmen,datbeginn,datende,ausgeschieden]);
        datamodul.sql_new(false, datamodul.q_a2,sql_query,'');


		  end;
	  2:begin   //Geplante Impfungen
			sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , impfung.datum as I_Datum , left(typ.untersuchung,250) as I_Typ, ';// from mitarbeiter ';
      sql_query:=sql_query+' mitarbeiter.p_nummer as Personalnummer, abteilung.kuerzel as Abt_Kuerzel, abteilung.langtext as Abt_Name  from mitarbeiter left outer join abteilung on (mitarbeiter.i_abteilung=abteilung.nummer) ';
      sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) ';
			sql_query:=sql_query+format('   where (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and ( ( impfung.datum >= %s ) and ( impfung.datum <= %s) ) and ( impfung.i_status=1 and (impfung.storno=0 )  ) and (mitarbeiter.storno=0)  %s ',[firmen,datbeginn,datende,ausgeschieden]);
      datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
			end;
	  3:begin  //bg-liste
			sql_query:='select count(*) as Anzahl , left(typ.untersuchung,250) as Vo_Typ , untersuchungsart.name as Vo_Art , beurteilung.text as Vo_Beurteilung from mitarbeiter  left outer join firma on (mitarbeiter.i_firma = firma.nummer) ';
			sql_query:= sql_query+' left outer join untersuchung on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer) ';
			sql_query:=sql_query+' left outer join beurteilung on (beurteilung.nummer=untersuchung.i_beurteilung) left outer join untersuchungsart on (untersuchung.i_uart=untersuchungsart.nummer)';
			sql_query:=sql_query+format('  where (mitarbeiter.i_firma in (%s)) and ( ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and (untersuchung.storno=0 )  ) and ( untersuchung.i_status=4 ) and (mitarbeiter.storno=0) %s ',[firmen,datbeginn,datende,ausgeschieden]);
			sql_query:=sql_query+ ' group by typ.untersuchung , untersuchungsart.name , beurteilung.text';
      datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
		 end;

     4:begin //rechnungen proband
       sql_query:='select mitarbeiter.name as Name, mitarbeiter.vorname as Vorname, mitarbeiter.geb_dat as Geburtsdatum,rechnung.datum as Rechnungsdatum, rechnung.renu as Rechnungsnummer, rechnung.eurobetrag as Betrag, rechnung.dat_verschickt as verschickt,  '+
             format(' rechnung.dat_bezahlt as bezahlt from mitarbeiter left join rechnung on (rechnung.i_mitarbeiter=mitarbeiter.nummer) where   mitarbeiter.i_firma in (%s) and ( ( rechnung.datum >= %s ) and ( rechnung.datum <= %s)  and (rechnung.storno=0) )  ',[firmen,datbeginn,datende]);
             datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
      end;
     5:begin //rechnungen firma
        sql_query:='select firma.firma, rechnung.datum as Rechnungsdatum, rechnung.renu as Rechnungsnummer, rechnung.eurobetrag as Betrag ,rechnung.dat_verschickt, rechnung.dat_bezahlt from firma left join rechnung on (rechnung.i_firma=firma.nummer) '+
        format(' where  rechnung.i_firma in (%s) and rechnung.i_firma>=0  and ( ( rechnung.datum >= %s ) and ( rechnung.datum <= %s) and (rechnung.storno=0)) order by firma.firma,rechnung.dat_bezahlt  ',[firmen,datbeginn,datende]);
        datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
      end;
    6:begin // unbezahlte rechnungen Proband
    				sql_query:='select mitarbeiter.name as Name, mitarbeiter.vorname as Vorname, mitarbeiter.geb_dat as Geburtsdatum,rechnung.datum as Rechnungsdatum, rechnung.renu as Rechnungsnummer, rechnung.brutto as Betrag, rechnung.dat_verschickt as verschickt,  '+
             format(' rechnung.dat_bezahlt as bezahlt from mitarbeiter left join rechnung on (rechnung.i_mitarbeiter=mitarbeiter.nummer) where   mitarbeiter.i_firma in (%s) and ( ( rechnung.datum >= %s ) and ( rechnung.datum <= %s) ',[firmen,datbeginn,datende])
                    + ' and (rechnung.storno=0) and (rechnung.dat_bezahlt="1900-01-01") )  ';
             datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
      end;
    15:begin // unbezahlte rechnungen  firma
        sql_query:='select firma.firma, rechnung.datum as Rechnungsdatum, rechnung.renu as Rechnungsnummer, rechnung.brutto as Betrag ,rechnung.dat_verschickt, rechnung.dat_bezahlt from firma left join rechnung on (rechnung.i_firma=firma.nummer) '+
        format(' where  rechnung.i_firma in (%s) and rechnung.i_firma>=0  and ( ( rechnung.datum >= %s ) and ( rechnung.datum <= %s) and (rechnung.dat_bezahlt="1900-01-01") and (rechnung.storno=0)) order by firma.firma,rechnung.dat_bezahlt  ',[firmen,datbeginn,datende]);
        datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
      end;
    7:begin //f�lliges labor
			sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , labor.datum as Lab_Datum , left(typ.untersuchung,250) as Lab_Typ, ';// from mitarbeiter ';
      sql_query:=sql_query+' mitarbeiter.p_nummer as Personalnummer, abteilung.kuerzel as Abt_Kuerzel, abteilung.langtext as Abt_Name  from mitarbeiter left outer join abteilung on (mitarbeiter.i_abteilung=abteilung.nummer) ';
			sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (labor.i_typ = typ.nummer)  ';
			sql_query:=sql_query+format('   where (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and ( ( labor.datum >= %s ) and ( labor.datum <= %s) ) and ( labor.i_status=1 and (labor.storno=0 )  ) and (mitarbeiter.storno=0) %s ',[firmen,datbeginn,datende,ausgeschieden]);
      datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
			end;
    8: begin //'Abgeschlossene Bescheinigungen'
        //probanden suchen
          sql_query:='select mitarbeiter.nummer, untersuchung.datum as Vo_Datum , left(typ.untersuchung,250) as Vo_Typ from mitarbeiter ';
		     sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
		     sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=4 and untersuchung.storno=0) and (mitarbeiter.storno=0) %s group by mitarbeiter.nummer ',[firmen,datbeginn,datende, ausgeschieden]);
            datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
           mas:='-1';
           datamodul.q_a2.First;
           while not datamodul.q_a2.eof do
           begin
           	mas:=mas+','+datamodul.q_a2.findfield('nummer').AsString;
            datamodul.q_a2.next;
           end;

          sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , untersuchung.datum as Vo_Datum , status.status as Status, left(typ.untersuchung,250) as Vo_Typ ';
          sql_query:=sql_query+', beurteilung.text as Beurteilung ,untersuchung.memo as Voraussetzung  ';
		    sql_query:=sql_query+'from mitarbeiter left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer) ';
          sql_query:=sql_query+' left outer join beurteilung  on (untersuchung.i_beurteilung = beurteilung.nummer) left outer join status  on (untersuchung.i_status = status.nummer) where ';
		    sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s) and untersuchung.storno=0 and untersuchung.i_status=4 and (mitarbeiter.nummer in (%s)) and (mitarbeiter.archiv="-") order by mitarbeiter.name, mitarbeiter.vorname,untersuchung.datum',[firmen,datbeginn,mas]);


          datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
       end;
       16: begin //'Abgeschlossene Bescheinigungen (nicht gedruckt)'
        //probanden suchen
          sql_query:='select  untersuchung.datum as Vo_Datum , left(typ.untersuchung, 250) as Vo_Typ,mitarbeiter.name as name, mitarbeiter.vorname as vorname, mitarbeiter.geb_dat as Geburtsdatum from mitarbeiter ';
		     sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
          sql_query:=sql_query+format('(mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=4 and untersuchung.storno=0) and (mitarbeiter.storno=0) and (untersuchung.dat_gedruckt="1900-01-01")',[firmen,datbeginn,datende]);
		     sql_query:=sql_query+format('  %s order by mitarbeiter.name, mitarbeiter.vorname, mitarbeiter.geb_dat ',[ ausgeschieden]);
            datamodul.sql_new(false, datamodul.q_a2,sql_query,'');

       end;
    9: begin //durchgef impf
         sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , impfung.datum as I_Datum , left(typ.untersuchung,250) as I_Typ from mitarbeiter ';
			 sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) ';
			 sql_query:=sql_query+format('   where (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and ( ( impfung.datum >= %s ) and ( impfung.datum <= %s) ) and ( impfung.i_status=4 and (impfung.storno=0 )  ) and (mitarbeiter.storno=0)  %s',[firmen,datbeginn,datende, ausgeschieden]);
       datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
       end;
    10:begin //durchgef lab
        sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , labor.datum as Lab_Datum , left(typ.untersuchung,250) as Lab_Typ from mitarbeiter ';
			sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (labor.i_typ = typ.nummer)  ';
			sql_query:=sql_query+format('   where (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and ( ( labor.datum >= %s ) and ( labor.datum <= %s) ) and ( labor.i_status=4 and (labor.storno=0 )  ) and (mitarbeiter.storno=0)  %s ',[firmen,datbeginn,datende, ausgeschieden]);
            datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
		  end;
    11:begin   //�bersicht abgeleistete Stunden   Summiert
        sql_query:='select firma.firma as Firma ,abr_zeitkonto.name as Zeitkonto,abr_anlass_krit.name as Abrechnungskriterium, round( (sum(firma_sollist.stunden) +sum(firma_sollist.minuten)/60),2) as h_vor_Ort,';
        sql_query:=sql_query+ ' round(( sum(firma_sollist.stunden*zeitansatz/100) +sum(firma_sollist.minuten*zeitansatz/100)/60),2) as h_Zeitansatz from firma_sollist ';
        sql_query:=sql_query+' left outer join firma on (firma_sollist.i_firma = firma.nummer) left outer join abr_anlass_krit on  (abr_anlass_krit.nummer=firma_sollist.i_kriterium) left outer join abr_zeitkonto on  (abr_zeitkonto.nummer=firma_sollist.i_zeitkonto) ';
        sql_query:=sql_query+format('   where  %s (firma.storno=0 ) and (firma_sollist.i_firma in (%s)) and ( ( firma_sollist.datum >= %s ) and ( firma_sollist.datum <= %s) )  and (firma_sollist.storno=0 ) group by firma.firma ,firma_sollist.i_zeitkonto,firma_sollist.i_kriterium  ',[abfrage_untersucher,firmen,datbeginn,datende]);
        datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
        end;
     12: begin    //firma   konto datum stunden user anlass Kriterium
          sql_query:='select  firma.firma  as Firma, abr_zeitkonto.name as Zeitkonto, abr_anlass_krit.name as Kriterium,untersucher.untersucher as Erbringer, datum as Datum ,round((stunden+minuten/60),2) as h_vor_Ort, ';
          sql_query:=sql_query+ 'round(((stunden+minuten/60)*zeitansatz/100),2) as h_Zeitansatz, anlass as Anlass, ';
          sql_query:=sql_query+' left( firma_sollist.memo,256) as Anmerkung,firma_sollist.km_gefahren as gefahrene_km, firma_sollist.km_modus as Privatfahrzeug, firma_sollist.ueberstunden as Zusatzleistung from  firma_sollist' ;
          sql_query:=sql_query+' left outer join firma on (firma_sollist.i_firma = firma.nummer) left outer join abr_anlass_krit on (abr_anlass_krit.nummer= firma_sollist.i_kriterium) left outer join untersucher on (untersucher.nummer= firma_sollist.i_untersucher) ';
          sql_query:=sql_query+' left outer join abr_zeitkonto on  (abr_zeitkonto.nummer=firma_sollist.i_zeitkonto) ';
          sql_query:=sql_query+format('   where %s  (firma.storno=0 ) and (firma_sollist.i_firma in (%s)) and ( ( firma_sollist.datum >= %s ) and ( firma_sollist.datum <= %s) )  and (firma_sollist.storno=0 ) order by firma, datum    ',[abfrage_untersucher,firmen,datbeginn,datende]);
          //left outer join firma on (firma_sollist.i_firma = firma.nummer)    where (firma.storno=0 ) and (firma_sollist.i_firma in (-1,2,3,4,5,6,7,8,9,10,11,12)) and ( ( firma_sollist.datum >= "2000-01-01" ) and ( firma_sollist.datum <= "2009-10-24") )  and (firma_sollist.storno=0 ) order by i_firma, datum
          datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
        end;
     13:
        begin     //zeitbilanz medizin
          {query:=format('drop table if exists %s',[temp_tab]);
          query:=format('create table %s select nummer,firma from firma  where  firma.nummer in (%s)',[temp_tab,firmen]);
          query:=format('alter TABLE  %s  add Stunden_ist float(6,2), add Stunden_soll float(6,2),add Stunden_todo float(6,2)',[temp_tab]);
          query:= format('select * from  %s',[temp_tab]);}
          //temp_tab_a:=temp_tab+'13';
          temp_tab_a:=format('temp_tab_%d_13',[user_id]);
          datamodul.q_a3.Close;
          datamodul.q_a2.Close;
          dbgridext1.Refresh;
          query:=format('drop table if exists %s',[temp_tab_a]);
          mysql_d.sql_exe(query);
          query:= format('create table  %s (nummer int(11) not null auto_increment  primary key, i_firma int(11) not null default 0, Firma char(50) not null default "",i_konto int(10) not null default 0, Konto char(50) not null default "", ',[temp_tab_a]);
                  query:=query+ ' Stunden_ist decimal(6,2) not null default 0, Stunden_soll decimal(6,2) not null default 0, Differenz decimal(6,2) not null default 0)';
          mysql_d.sql_exe(query);
          query:= format('select * from  %s ',[temp_tab_a]);
          //datamodul.sql_new(false, datamodul.q_a2,query,'');
          datamodul.sql_new(false, datamodul.q_6,query,'');


          query:=format('select * from firma where (firma.storno=0) and (firma.nummer in (%s))',[firmen]);
          datamodul.sql_new(false, datamodul.q_4,query,'firma');
          datamodul.q_4.first;
          while not datamodul.q_4.Eof do
          begin
          	//istzeiten suchen
            firma:=datamodul.q_4.findfield('firma').asstring;
            i_firma:=datamodul.q_4.findfield('nummer').asinteger;

            query:='select firma_sollist.i_firma, (sum(firma_sollist.stunden*zeitansatz/100) +sum(firma_sollist.minuten*zeitansatz/100)/60) as Erbracht, abr_zeitkonto.name as Zeitkonto from firma_sollist ';
            query:=query+ ' left join abr_zeitkonto on (abr_zeitkonto.nummer=firma_sollist.i_zeitkonto ) ';
            query:=query+format(' where %s firma_sollist.i_firma=%d and ( ( firma_sollist.datum >= %s ) and ( firma_sollist.datum <= %s) )  and (firma_sollist.storno=0 ) group  by firma_sollist.i_firma, firma_sollist.i_zeitkonto order by abr_zeitkonto.name',[abfrage_untersucher,i_firma,datbeginn,datende]);
            datamodul.sql_new(false, datamodul.q_5,query,'');
          	datamodul.q_5.first;

            while not datamodul.q_5.eof do
            begin

               konto:=datamodul.q_5.findfield('zeitkonto').asstring;
               stunden_ist:=datamodul.q_5.findfield('erbracht').asfloat;
               stunden_soll:=0;
               differenz:=0;
               neuer_datensatz(datamodul.q_6, ['i_firma','Firma','Konto','Stunden_ist', 'Stunden_Soll', 'Differenz'], [i_firma,firma, konto,stunden_ist,stunden_soll, differenz ]);
               datamodul.q_5.next;
            end;
            // sollzeiten suchen
            //jeweils f�r Medizin technik ..

            query:=format('select  *,abr_zeitkonto.name as abr_zeitkonto  from firma_zeiten_soll left join abr_zeitkonto on (abr_zeitkonto.nummer=firma_zeiten_soll.i_zeitkonto ) where ((i_firma=%d ) and (firma_zeiten_soll.storno=0)) ',[i_firma]);
            datamodul.sql_new(false, datamodul.q_5,query,'');
            datamodul.q_5.first;

            while not  DataModul.q_5.eof do
            begin
                //i_krit:=DataModul.q_5.findfield('i_abrechnungskriterium').asinteger;
                p:=0;
                konto:=DataModul.q_5.findfield('abr_zeitkonto').asstring;

                datvon:=DataModul.q_5.findfield('von').asdatetime;
                datbis:=DataModul.q_5.findfield('bis').asdatetime;
                sd:=DataModul.q_5.findfield('einsatzstunden').asfloat/(1+trunc(DataModul.q_5.findfield('bis').asdatetime) -trunc( DataModul.q_5.findfield('von').asdatetime));

                d1:=strtodate(form_einfache_abfragen.dateedit_beginn.text);

                if form_einfache_abfragen.CheckBox_soll_jahr.Checked then
                begin
                   //decodedate(d1,jahr, monat, tag);
                   jahr:=yearof(d1);
                   //d2:=strtodate(format('31.12.%d',[jahr]));
                   TryEncodeDate(jahr,12,31,tdatetime(d2));
                   TryEncodeDate(jahr,1,1,tdatetime(d1));
                end
                else
                  d2:=strtodate(form_einfache_abfragen.dateedit_ende.text);

                if (d1>=datvon) and (d1<=datbis) and (d2>=datbis )   then
                begin
                    p:=datbis-d1+1;
                end;
                if (d1>=datvon) and (d2<=datbis ) then
                begin
                    p:=d2-d1+1;
                end;
                if (d1<=datvon ) and (d2<=datbis ) and  (d2>=datvon ) then
                begin
                    p:=d2-datvon+1;
                end;
                if (d1<=datvon) and (d2>=datbis ) then
                begin
                    p:=datbis-datvon+1;
                end;

                t:=p*sd;

                if t>0 then
                begin
                  b:=datamodul.q_6.Locate('i_firma, konto',vararrayof([i_firma,konto]),[]);
                  if b then
                  begin
                     datamodul.q_6.edit;
                     datamodul.q_6.FindField('Stunden_Soll').asfloat:=datamodul.q_6.FindField('Stunden_Soll').asfloat+t;
                     datamodul.q_6.Post;
                  end
                  else
                  begin
                      neuer_datensatz(datamodul.q_6, ['i_firma','Firma','Konto','Stunden_ist', 'Stunden_Soll', 'Differenz'], [i_firma,firma, konto,0,t, 0 ]);
                      datamodul.q_6.Post;
                  end;
               end;

              DataModul.q_5.next;
            end;
            datamodul.q_4.next;
          end;

          { datamodul.q_6.first;
           while not datamodul.q_6.eof do
           begin
              datamodul.q_6.edit;
              datamodul.q_6.FindField('differenz').asfloat:=datamodul.q_6.FindField('stunden_soll').asfloat-datamodul.q_6.FindField('stunden_ist').asfloat ;
              datamodul.q_6.post;
              datamodul.q_6.next;
           end; }
           query:= format('update  %s set differenz=stunden_soll-stunden_ist',[temp_tab_a]);
           mysql_d.sql_exe(query);
           //datamodul.sql_new(false, datamodul.q_6,query,'');


           query:= format('select Firma, Konto, Stunden_ist, Stunden_soll, Differenz from  %s order by firma, konto',[temp_tab_a]);
           datamodul.sql_new(false, datamodul.q_a2,query,'');


        end;
      14:
        begin      //Melms Tagesliste
          //untersuchungen und folgeuntersuchungen
          //temp_tab_a:=temp_tab+'14';
          temp_tab_a:=format('temp_tab_%d_14',[user_id]);
          datamodul.q_a3.Close;
          datamodul.q_a2.Close;
          dbgridext1.Refresh;
          d1:=strtodate(form_einfache_abfragen.dateedit_ende.text);
          d1:=d1+1;

          datende:=sql_datetostr(d1);

          // datende muss
          query:=format('drop table if exists %s',[temp_tab_a]);
          mysql_d.sql_exe(query);
          query:= format('create table  %s (nummer int(11) not null auto_increment  primary key,  Firma char(50),Name char(50), Vorname char(50),Geburtsdatum date,UIL char(12),Untersuchungs_Datum date, Status char(13), Untersuchung char(255 ),rf tinyint(1))',[temp_tab_a]);
          mysql_d.sql_exe(query);
          query:= format('select * from  %s ',[temp_tab_a]);
          datamodul.sql_new(false, datamodul.q_a3,query,'');
          //untersuchungen und folgeuntersuchungen

          sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , untersuchung.datum as Vo_Datum , left(typ.untersuchung,250) as Vo_Typ ';
          sql_query:=sql_query+', beurteilung.text as Beurteilung ,untersuchung.memo as Voraussetzung, status.status as Status  ';
		    sql_query:=sql_query+'from mitarbeiter left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer) ';
          sql_query:=sql_query+'	left outer join beurteilung  on (untersuchung.i_beurteilung = beurteilung.nummer) left outer join status  on (untersuchung.i_status = status.nummer)	 where ';
		    sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.ts >= %s )  and ( untersuchung.ts <= %s ) and (untersuchung.storno=0) %s order by mitarbeiter.name, mitarbeiter.vorname,untersuchung.datum',[firmen,datbeginn,datende,ausgeschieden]);
          datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
          q_a2.First;
          while not q_a2.eof  do
          begin
            s:=format(' %s | %s',[q_a2.findfield('vo_typ').asstring, q_a2.findfield('Beurteilung').asstring]);
            q_a3.InsertRecord([null,q_a2.findfield('f_name').asstring,q_a2.findfield('name').asstring,q_a2.findfield('vorname').asstring,q_a2.findfield('Geburtsdatum').asdatetime,'Untersuchung', q_a2.findfield('Vo_datum').asdatetime,q_a2.findfield('status').asstring,s,1]);
          	q_a2.Next;
          end;


          //impfung
          sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , impfung.datum as I_Datum , left(typ.untersuchung,250) as I_Typ, status.status as Status  from mitarbeiter ';
			 sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) ';
          sql_query:=sql_query+' left outer join status  on (impfung.i_status = status.nummer) ';
			 sql_query:=sql_query+format('   where (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and ( ( impfung.ts >= %s ) and ( impfung.ts <= %s) )  and (impfung.storno=0   ) and (mitarbeiter.storno=0) %s ',[firmen,datbeginn,datende, ausgeschieden]);
          datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
          q_a2.First;
          while not q_a2.eof  do
          begin
            s:=format(' %s ',[q_a2.findfield('I_typ').asstring]);
            q_a3.InsertRecord([null,q_a2.findfield('f_name').asstring,q_a2.findfield('name').asstring,q_a2.findfield('vorname').asstring,q_a2.findfield('Geburtsdatum').asdatetime,'Impfung', q_a2.findfield('I_datum').asdatetime,q_a2.findfield('status').asstring,s,2]);
          	q_a2.Next;
          end;
          //labor
          sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , labor.datum as Lab_Datum , left(typ.untersuchung,250) as Lab_Typ,status.status as Status  from mitarbeiter ';
			sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (labor.i_typ = typ.nummer)  ';
      sql_query:=sql_query+' left outer join status  on (labor.i_status = status.nummer) ';
			sql_query:=sql_query+format('   where (firma.storno=0 ) and (mitarbeiter.i_firma in (%s)) and ( ( labor.ts >= %s ) and ( labor.ts <= %s) ) and (labor.storno=0 ) and (mitarbeiter.storno=0) %s ',[firmen,datbeginn,datende, ausgeschieden]);
         datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
         q_a2.First;
          while not q_a2.eof  do
          begin
            s:=format(' %s ',[q_a2.findfield('lab_typ').asstring]);
            q_a3.InsertRecord([null,q_a2.findfield('f_name').asstring,q_a2.findfield('name').asstring,q_a2.findfield('vorname').asstring,q_a2.findfield('Geburtsdatum').asdatetime,'Labor', q_a2.findfield('lab_datum').asdatetime,q_a2.findfield('status').asstring,s,3]);
          	q_a2.Next;
         end;

          query:= format('select Firma, Name, Vorname, Geburtsdatum,  Untersuchungs_Datum,uil , Status, Untersuchung from  %s order by firma, name, vorname, geburtsdatum,untersuchungs_datum,rf ',[temp_tab_a]);
          datamodul.sql_new(false, datamodul.q_a2,query,'');
        end;

    17: begin //'Abgeschlossene Bescheinigungen AG'
        //probanden suchen
          sql_query:='select mitarbeiter.nummer, untersuchung.datum as Vo_Datum , left(typ.untersuchung,250) as Vo_Typ from mitarbeiter ';
		     sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
		     sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=4 and untersuchung.storno=0) and (mitarbeiter.storno=0) %s group by mitarbeiter.nummer ',[firmen,datbeginn,datende, ausgeschieden]);
            datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
           mas:='-1';
           datamodul.q_a2.First;
           while not datamodul.q_a2.eof do
           begin
           	mas:=mas+','+datamodul.q_a2.findfield('nummer').AsString;
            datamodul.q_a2.next;
           end;

          sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , untersuchung.datum as Vo_Datum , status.status as Status, left(typ.untersuchung,250) as Vo_Typ ';
		    sql_query:=sql_query+'from mitarbeiter left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer) ';
          sql_query:=sql_query+'	 left outer join beurteilung  on (untersuchung.i_beurteilung = beurteilung.nummer) left outer join status  on (untersuchung.i_status = status.nummer)		 where ';
		    sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s )   and untersuchung.storno=0  and (mitarbeiter.nummer in (%s)) and (mitarbeiter.archiv="-") order by mitarbeiter.name, mitarbeiter.vorname,untersuchung.datum',[firmen,datbeginn,mas]);

          {sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , untersuchung.datum as Vo_Datum , left(typ.untersuchung,250) as Vo_Typ from mitarbeiter ';
		    sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
		    sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=4 and untersuchung.storno=0) and (mitarbeiter.storno=0) ',[firmen,datbeginn,datende]);}
          datamodul.sql_new(false, datamodul.q_a2,sql_query,'');

          //zust_arbmed_besch_vorhanden(datamodul.q_untersuchung.findfield('i_weitergabe').asinteger,datamodul.q_untersuchung.findfield('i_typ').asinteger,datamodul.q_untersuchung.findfield('datum').AsDateTime  ) then
       end;
       18: begin //'Geplante Bescheinigungen AG'
            sql_query:='select firma.firma as F_Name , mitarbeiter.name as Name , mitarbeiter.vorname as Vorname , mitarbeiter.geb_dat as Geburtsdatum , untersuchung.datum as Vo_Datum , left(typ.untersuchung,250) as Vo_Typ from mitarbeiter ';
            sql_query:=sql_query+' left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer) left outer join typ on (untersuchung.i_typ=typ.nummer)  where ';
            sql_query:=sql_query+format(' (mitarbeiter.i_firma in (%s)) and ( untersuchung.datum >= %s ) and ( untersuchung.datum <= %s) and ( untersuchung.i_status=1 and untersuchung.storno=0) and (mitarbeiter.storno=0)  %s ',[firmen,datbeginn,datende,ausgeschieden]);
              datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
    		  end;
       19: begin   //'Statistik Arbeitsunf�lle'
            sql_query:='select ambulanz.datum as Amulanz_datum , ambulanz.urzeit as Ambulanz_Zeit , arbeitsunfall.u_datum as Unfall_Datum ,';
            sql_query:=sql_query+' CASE DAYOFWEEK(arbeitsunfall.u_datum) WHEN 1 THEN "Sonntag" WHEN 2 THEN "Montag" WHEN 3 THEN "Dienstag" WHEN 4 THEN "Mittwoch" WHEN 5 THEN "Donnerstag" WHEN 6 THEN "Freitag" WHEN 7 THEN "Samstag" END Unfall_Wochentag ,';
            sql_query:=sql_query+' arbeitsunfall.u_zeit as Unfall_Uhrzeit ,mitarbeiter.name as Name, Mitarbeiter.vorname as Vorname, mitarbeiter.geb_dat as Geburtsdatum,  untersucher.untersucher as Behandler , ambulanz.s00 as Koerperteil_1 , ';
            sql_query:=sql_query+' ambulanz.s01 as Koerperteil_2 , ambulanz.s10 as Art_1 , ambulanz.s11 as Art_2 , amb_weiter.behandlung as Weit_Beh , arbeitsunfall.gebaeude as Gebaeude , arbeitsunfall.maschine as Maschine , ';
            sql_query:=sql_query+' case arbeitsunfall.b_leiharbeiter when 1 then "Leiharbeitnehmer" end Leiharbeitnehmer  ';
            sql_query:=sql_query+' from mitarbeiter left outer join ambulanz on (mitarbeiter.nummer=ambulanz.i_mitarbeiter) left outer join arbeitsunfall on (arbeitsunfall.nummer=ambulanz.nummer) ';
            sql_query:=sql_query+' left outer join amb_weiter on (ambulanz.i_amb_weiter = amb_weiter.nummer) left outer join untersucher on (untersucher.nummer=ambulanz.i_untersucher) ' ;
            sql_query:=sql_query+format('where ( mitarbeiter.nummer>"0" and (ambulanz.b_arbeitsunfall = "1")  ) and (mitarbeiter.i_firma in (%s)) and ( ( ambulanz.datum >= %s ) and ( ambulanz.datum <= %s) )  and (mitarbeiter.storno=0 )  and (ambulanz.storno=0) %s ',[firmen,datbeginn,datende,ausgeschieden]);
            datamodul.sql_new(false, datamodul.q_a2,sql_query,'');
          end;
       20:begin     //'Zeitbilanz erweitert', zeitbilanz medizin und untersucher


          temp_tab_a:=format('temp_tab_%d_13',[user_id]);
          datamodul.q_a3.Close;
          datamodul.q_a2.Close;
          dbgridext1.Refresh;
          query:=format('drop table if exists %s',[temp_tab_a]);
          mysql_d.sql_exe(query);
          query:= format('create table  %s (nummer int(11) not null auto_increment  primary key, i_firma int(11) not null default 0, Firma char(50) not null default "",i_konto int(10) not null default 0, Konto char(50) not null default "", ',[temp_tab_a]);
                  query:=query+ ' Stunden_ist decimal(6,2) not null default 0, Stunden_soll decimal(6,2) not null default 0, Differenz decimal(6,2) not null default 0)';
          mysql_d.sql_exe(query);
          query:= format('select * from  %s ',[temp_tab_a]);
          datamodul.sql_new(false, datamodul.q_6,query,'');

          query:=format('select * from firma where (firma.storno=0) and (firma.nummer in (%s))',[firmen]);
          datamodul.sql_new(false, datamodul.q_4,query,'firma');
          datamodul.q_4.first;
          while not datamodul.q_4.Eof do
          begin
          	//istzeiten suchen
            firma:=datamodul.q_4.findfield('firma').asstring;
            i_firma:=datamodul.q_4.findfield('nummer').asinteger;

            query:='select firma_sollist.i_firma, (sum(firma_sollist.stunden*zeitansatz/100) +sum(firma_sollist.minuten*zeitansatz/100)/60) as Erbracht, abr_zeitkonto.name as Zeitkonto from firma_sollist ';
            query:=query+ ' left join abr_zeitkonto on (abr_zeitkonto.nummer=firma_sollist.i_zeitkonto ) ';
            query:=query+format(' where  firma_sollist.i_firma=%d and ( ( firma_sollist.datum >= %s ) and ( firma_sollist.datum <= %s) )  and (firma_sollist.storno=0 ) group  by firma_sollist.i_firma, firma_sollist.i_zeitkonto order by abr_zeitkonto.name',[i_firma,datbeginn,datende]);
            datamodul.sql_new(false, datamodul.q_5,query,'');
          	datamodul.q_5.first;

            while not datamodul.q_5.eof do
            begin

               konto:=datamodul.q_5.findfield('zeitkonto').asstring;
               stunden_ist:=datamodul.q_5.findfield('erbracht').asfloat;
               stunden_soll:=0;
               differenz:=0;
               neuer_datensatz(datamodul.q_6, ['i_firma','Firma','Konto','Stunden_ist', 'Stunden_Soll', 'Differenz'], [i_firma,firma, konto,stunden_ist,stunden_soll, differenz ]);
               datamodul.q_5.next;
            end;
            // sollzeiten suchen
            //jeweils f�r Medizin technik ..

            query:=format('select  *,abr_zeitkonto.name as abr_zeitkonto  from firma_zeiten_soll left join abr_zeitkonto on (abr_zeitkonto.nummer=firma_zeiten_soll.i_zeitkonto ) where ((i_firma=%d ) and (firma_zeiten_soll.storno=0)) ',[i_firma]);
            datamodul.sql_new(false, datamodul.q_5,query,'');
            datamodul.q_5.first;

            while not  DataModul.q_5.eof do
            begin
                //i_krit:=DataModul.q_5.findfield('i_abrechnungskriterium').asinteger;
                p:=0;
                konto:=DataModul.q_5.findfield('abr_zeitkonto').asstring;

                datvon:=DataModul.q_5.findfield('von').asdatetime;
                datbis:=DataModul.q_5.findfield('bis').asdatetime;
                sd:=DataModul.q_5.findfield('einsatzstunden').asfloat/(1+trunc(DataModul.q_5.findfield('bis').asdatetime) -trunc( DataModul.q_5.findfield('von').asdatetime));

                d1:=strtodate(form_einfache_abfragen.dateedit_beginn.text);

                if form_einfache_abfragen.CheckBox_soll_jahr.Checked then
                begin
                   //decodedate(d1,jahr, monat, tag);
                   jahr:=yearof(d1);
                   //d2:=strtodate(format('31.12.%d',[jahr]));
                   TryEncodeDate(jahr,12,31,tdatetime(d2));
                   TryEncodeDate(jahr,1,1,tdatetime(d1));
                end
                else
                  d2:=strtodate(form_einfache_abfragen.dateedit_ende.text);

                if (d1>=datvon) and (d1<=datbis) and (d2>=datbis )   then
                begin
                    p:=datbis-d1+1;
                end;
                if (d1>=datvon) and (d2<=datbis ) then
                begin
                    p:=d2-d1+1;
                end;
                if (d1<=datvon ) and (d2<=datbis ) and  (d2>=datvon ) then
                begin
                    p:=d2-datvon+1;
                end;
                if (d1<=datvon) and (d2>=datbis ) then
                begin
                    p:=datbis-datvon+1;
                end;

                t:=p*sd;

                if t>0 then
                begin
                  b:=datamodul.q_6.Locate('i_firma, konto',vararrayof([i_firma,konto]),[]);
                  if b then
                  begin
                     datamodul.q_6.edit;
                     datamodul.q_6.FindField('Stunden_Soll').asfloat:=datamodul.q_6.FindField('Stunden_Soll').asfloat+t;
                     datamodul.q_6.Post;
                  end
                  else
                  begin
                      neuer_datensatz(datamodul.q_6, ['i_firma','Firma','Konto','Stunden_ist', 'Stunden_Soll', 'Differenz'], [i_firma,firma, konto,0,t, 0 ]);
                      datamodul.q_6.Post;
                  end;
               end;

              DataModul.q_5.next;
            end;
           //hier einf�gen istzeiten gruppiert nach erbringer
            query:='select firma_sollist.i_firma, (sum(firma_sollist.stunden*zeitansatz/100) +sum(firma_sollist.minuten*zeitansatz/100)/60) as Erbracht, untersucher.untersucher as untersucher from firma_sollist ';
            query:=query+ ' left join untersucher on (untersucher.nummer=firma_sollist.i_untersucher ) ';
            query:=query+format(' where firma_sollist.i_firma=%d and ( ( firma_sollist.datum >= %s ) and ( firma_sollist.datum <= %s) )  and (firma_sollist.storno=0 ) group  by firma_sollist.i_firma, firma_sollist.i_untersucher order by untersucher.untersucher',[i_firma,datbeginn,datende]);
            datamodul.sql_new(false, datamodul.q_5,query,'');
          	datamodul.q_5.first;

            while not datamodul.q_5.eof do
            begin

               konto:='___'+datamodul.q_5.findfield('untersucher').asstring;
               stunden_ist:=datamodul.q_5.findfield('erbracht').asfloat;
               stunden_soll:=0;
               differenz:=0;
               neuer_datensatz(datamodul.q_6, ['i_firma','Firma','Konto','Stunden_ist', 'Stunden_Soll', 'Differenz'], [i_firma,firma, konto,stunden_ist,stunden_soll, differenz ]);
               datamodul.q_5.next;
            end;


            datamodul.q_4.next;
          end;


           {datamodul.q_6.first;
           while not datamodul.q_6.eof do
           begin
              if  copy(datamodul.q_6.FindField('konto').asstring,1,3)<>'___' then
              begin
                datamodul.q_6.edit;
                datamodul.q_6.FindField('differenz').asfloat:=datamodul.q_6.FindField('stunden_soll').asfloat-datamodul.q_6.FindField('stunden_ist').asfloat ;
                datamodul.q_6.post;
              end;
              datamodul.q_6.next;
           end;  }

           query:= format('update  %s set differenz=stunden_soll-stunden_ist where SUBSTRING(konto,1,3)<>"___" ',[temp_tab_a]);
           mysql_d.sql_exe(query);

           query:= format('select Firma, Konto, Stunden_ist, Stunden_soll, Differenz from  %s order by firma, konto',[temp_tab_a]);
           datamodul.sql_new(false, datamodul.q_a2,query,'');


          end;

    end;
	end;
	pagecontrol_auswertung.ActivePage:=tabsheet_ergebnis;

	end;
finally
   result:=form_einfache_abfragen.checkbox_excel.checked;
	form_einfache_abfragen.release;
  screen.cursor:=crdefault;

end;
end;
end;

procedure TForm_auswertung.FormDestroy(Sender: TObject);
begin
form_main.itemsdeaktivieren(listbox_firma.items);
tree_anzeige.liste_loeschen;
form_main.treedeaktivieren(treeview);
form_main.treedeaktivieren(tree);
end;

procedure tform_auswertung.zeig_anzahl;
begin
if datamodul.q_a2.Active then
	begin
	 datamodul.q_a2.first;
	 label_anzahl.Caption:='Anzahl: '+inttostr(datamodul.q_a2.recordcount);
 end;
end;


procedure tform_auswertung.abfrage_neu(id,s_inhalt:string);
var
nr,i:integer;
s_nr:string;
query,s_name:string;
nodedata:pnodepointer;
begin
with datamodul do
begin

query:=format('select max(nummer) from %s',[sql_abfragen_temp]);
s_nr:=mysql_d.Feldinhalt(query,0);
if s_nr='' then nr:=1 else nr:=strtoint(s_nr)+1;
//q_sql_abfragen.Last;
//nr:=q_sql_abfragen.findfield('nummer').asinteger+1;

q_sql_baum.Locate('name',id,[]);

if id='Proband-Abteilung' then
begin

end;

s_name:=id+'~1';

    if id='Firma' then
    begin
        neuer_datensatz(q_sql_abfragen,
      ['nummer','sql_baum','modus','tabelle','feld','sql_join','bereich','relation',
        'auffaellig','d_typ','name','alle','s_einheit','memo'],
      [nr,q_sql_baum['nummer'],q_sql_baum['modus'], q_sql_baum['tabelle'],q_sql_baum['filterfeld'],
      q_sql_baum['sql_join'], q_sql_baum['nummer']-5100 ,'=',-1,0,s_name,0,q_sql_baum['s_einheit'],s_inhalt]);

    end
    else
      neuer_datensatz(q_sql_abfragen,
      ['nummer','sql_baum','modus','tabelle','feld','sql_join','bereich','relation',
        'auffaellig','d_typ','name','alle','s_einheit','string'],
      [nr,q_sql_baum['nummer'],q_sql_baum['modus'], q_sql_baum['tabelle'],q_sql_baum['filterfeld'],
      q_sql_baum['sql_join'], q_sql_baum['nummer']-5100 ,'=',-1,0,s_name,0,q_sql_baum['s_einheit'],s_inhalt]);

    q_sql_abfragen.Post;
    
    for i:=0 to treeview.Items.Count-1 do
    begin

      nodedata:=treeview.items[i].data;
		  if nodedata^.nummer = q_sql_baum['nummer'] then treekennzeichnen(treeview.items[i],3);
    end;
end;
end;
procedure TForm_auswertung.CheckBox_negativClick(Sender: TObject);
begin
 if checkbox_negativ.Checked then
     TabSheet_anzeige.TabVisible :=false else TabSheet_anzeige.TabVisible :=true;
end;

procedure TForm_auswertung.m_update_fehlgeschlagenClick(Sender: TObject);
var
query:string;
begin
pagecontrol_auswertung.ActivePage:=TabSheet_ergebnis;

   query:='select ts as "Zeit", aktion as "SQL-Befehl", case  when resultat="1" then "OK" else "Fehler" end as "Ergebnis" from protokoll_sys where funk like "dbupd" and resultat=0';
   datamodul.sql_new(false, datamodul.q_a2,query,'');

zeig_anzahl;
end;

procedure TForm_auswertung.m_ProtokollDatenbankaktualisierungClick(
  Sender: TObject);
var
query:string;
begin
pagecontrol_auswertung.ActivePage:=TabSheet_ergebnis;

   query:='select ts as "Zeit", aktion as "SQL-Befehl", case  when resultat="1" then "OK" else "Fehler" end as Ergebnis from protokoll_sys where funk like "dbupd%"';
   datamodul.sql_new(false, datamodul.q_a2,query,'');
zeig_anzahl;
end;


procedure TForm_auswertung.m_AnmeldeprotokollClick(Sender: TObject);
var
query:string;
begin
pagecontrol_auswertung.ActivePage:=TabSheet_ergebnis;

  query:='select ts as "Anmeldezeit",user_name as "User",funk as "Aktion"  from protokoll_user order by ts desc ';
  datamodul.sql_new(false, datamodul.q_a2,query,'');
  zeig_anzahl;

end;

end.

