unit arbene_TLB;

// ************************************************************************ //
// WARNUNG                                                                    
// -------                                                                    
// Die in dieser Datei deklarierten Typen wurden aus Daten einer Typbibliothek
// generiert. Wenn diese Typbibliothek explizit oder indirekt (�ber eine     
// andere Typbibliothek) reimportiert wird oder wenn die Anweisung            
// 'Aktualisieren' im Typbibliotheks-Editor w�hrend des Bearbeitens der     
// Typbibliothek aktiviert ist, wird der Inhalt dieser Datei neu generiert und 
// alle manuell vorgenommenen �nderungen gehen verloren.                           
// ************************************************************************ //

// PASTLWTR : 1.2
// Datei generiert am 25.11.2017 22:08:16 aus der unten beschriebenen Typbibliothek.

// ************************************************************************  //
// Typbib: C:\prg\arbene\Arbene.tlb (1)
// LIBID: {05806D45-41B4-4568-9200-63CE20CC3102}
// LCID: 0
// Hilfedatei: 
// Hilfe-String: arbene (Bibliothek)
// DepndLst: 
//   (1) v1.0 stdole, (C:\Windows\System32\stdole32.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit mu� ohne Typ�berpr�fung f�r Zeiger compiliert werden. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// In dieser Typbibliothek deklarierte GUIDS . Es werden folgende         
// Pr�fixe verwendet:                                                     
//   Typbibliotheken     : LIBID_xxxx                                     
//   CoClasses           : CLASS_xxxx                                     
//   DISPInterfaces      : DIID_xxxx                                      
//   Nicht-DISP-Schnittstellen: IID_xxxx                                       
// *********************************************************************//
const
  // Haupt- und Nebenversionen der Typbibliothek
  arbeneMajorVersion = 1;
  arbeneMinorVersion = 0;

  LIBID_arbene: TGUID = '{05806D45-41B4-4568-9200-63CE20CC3102}';

  IID_Iapp: TGUID = '{9249D0D4-B91E-4809-B16D-8B8B7754AE80}';
  CLASS_app: TGUID = '{E2662688-B44A-4229-9E00-EC6218938682}';
type

// *********************************************************************//
// Forward-Deklaration von in der Typbibliothek definierten Typen         
// *********************************************************************//
  Iapp = interface;
  IappDisp = dispinterface;

// *********************************************************************//
// Deklaration von in der Typbibliothek definierten CoClasses             
// (HINWEIS: Hier wird jede CoClass zu ihrer Standardschnittstelle        
// zugewiesen)                                                            
// *********************************************************************//
  app = Iapp;


// *********************************************************************//
// Schnittstelle: Iapp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {9249D0D4-B91E-4809-B16D-8B8B7754AE80}
// *********************************************************************//
  Iapp = interface(IDispatch)
    ['{9249D0D4-B91E-4809-B16D-8B8B7754AE80}']
    procedure tb; safecall;
  end;

// *********************************************************************//
// DispIntf:  IappDisp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {9249D0D4-B91E-4809-B16D-8B8B7754AE80}
// *********************************************************************//
  IappDisp = dispinterface
    ['{9249D0D4-B91E-4809-B16D-8B8B7754AE80}']
    procedure tb; dispid 1;
  end;

// *********************************************************************//
// Die Klasse Coapp stellt die Methoden Create und CreateRemote zur      
// Verf�gung, um Instanzen der Standardschnittstelle Iapp, dargestellt von
// CoClass app, zu erzeugen. Diese Funktionen k�nnen                     
// von einem Client verwendet werden, der die CoClasses automatisieren    
// m�chte, die von dieser Typbibliothek dargestellt werden.               
// *********************************************************************//
  Coapp = class
    class function Create: Iapp;
    class function CreateRemote(const MachineName: string): Iapp;
  end;

implementation

uses ComObj;

class function Coapp.Create: Iapp;
begin
  Result := CreateComObject(CLASS_app) as Iapp;
end;

class function Coapp.CreateRemote(const MachineName: string): Iapp;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_app) as Iapp;
end;

end.
