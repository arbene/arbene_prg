unit a_line_edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TForm_line_edit = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit: TEdit;
    CheckBox: TCheckBox;
    Label_1: TLabel;
    Label_2: TLabel;
    Label_3: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_line_edit: TForm_line_edit;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_line_edit.FormActivate(Sender: TObject);
begin
edit.SetFocus;
end;

procedure TForm_line_edit.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
case prg_typ of
     3,4: checkbox.Visible:=false;
end;     
end;

procedure TForm_line_edit.FormShow(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

end.
