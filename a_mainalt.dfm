�
 TFORM_MAIN 0�r  TPF0
TForm_main	Form_mainLeft� TopxWidthHeightzCaption
VorImpfLabColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Menu	MainMenu1OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TPanel
Panel_mainLeft Top7Width�HeightAlignalClient
BevelOuterbvNoneCaption
Panel_mainTabOrder  TPageControlPageControl_mainLeft Top Width�Height
ActivePageTabSheet_laborAlignalClientTabOrder  	TTabSheetTabSheet_NameCaptionName TPanelPanel_name_listeLeft Top Width� Height�AlignalLeftCaptionPanel_name_listeTabOrder  	TGroupBoxGroupBox_mandantLeftTopWidth� Height`AlignalTopCaptionFirmaTabOrder  
TDBgridEXT
DBgridEXT4LeftTopWidth� HeightOAlignalClientColorclInfoBk
DataSourceDataModule1.DataSource_FirmaDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameFirmaWidthtVisible	 Expanded	FieldNameOrtWidthHVisible	     	TGroupBoxGroupBox_n_listeLeftTopaWidth� Height�AlignalClientCaption
NamenlisteTabOrder TPanelPanel_stamm_listLeftTopIWidth� Height9AlignalBottom
BevelOuterbvNoneCaptionPanel_stamm_listTabOrder  TBitBtnBitBtn1LeftTopWidthJHeightCaptionNeuTabOrder OnClickBitBtn1ClickKindbkAll  TBitBtnBitBtn2LeftTopWidthKHeightCaptionL�schenTabOrderKindbkCancel   
TDBgridEXT
DBgridEXT5LeftTopWidth� Height:AlignalClientColorclInfoBk
DataSource!DataModule1.DataSource_StammdatenDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameNameWidth]Visible	 Expanded	FieldNameVornameWidth`Visible	      TPageControlPageControl_nameLeft� Top WidthHeight�
ActivePageTabSheet_name_persAlignalClientTabOrder 	TTabSheetTabSheet_name_persCaptionPersonalien TLabelPLZLeft Top� WidthHeightCaptionPLZ  TLabelStrasseLeft TopPWidth#HeightCaptionStrasse  TLabelGeburtsdatumLeftPTopWidthBHeightCaptionGeburtsdatum  TLabelVornameLeft� TopWidth*HeightCaptionVorname  TLabelNameLeft TopWidthHeightCaptionName  TLabelPersonalnummerLeft Top� WidthNHeightCaptionPersonalnummer  TLabelOrtLeft� Top� WidthHeightCaptionOrt  TDBEditDBEdit_nameLeft Top(WidthyHeight	DataFieldName
DataSource!DataModule1.DataSource_StammdatenTabOrder   TDBEditFrLeft� Top&WidthyHeight	DataFieldVorname
DataSource!DataModule1.DataSource_StammdatenTabOrder  TDBEditDBEdit_gebdatLeftPTop(WidthWHeight	DataFieldGeb_dat
DataSource!DataModule1.DataSource_StammdatenTabOrder  TDBEdit
DBEdit_plzLeft#Top� WidthKHeight	DataFieldPLZ
DataSource!DataModule1.DataSource_StammdatenTabOrder  TDBEdit
DBEdit_ortLeft� Top� Width� Height	DataFieldOrt
DataSource!DataModule1.DataSource_StammdatenTabOrder  TDBEditDBEdit_strasseLeft#Top`Width� Height	DataFieldStrasse
DataSource!DataModule1.DataSource_StammdatenTabOrder  TDBEditDBEdit_persnummerLeft Top� Width� Height	DataFieldPersonalnummer
DataSource!DataModule1.DataSource_StammdatenTabOrder   	TTabSheetTabSheet_name_apCaptionArbeitsplatz
ImageIndex    	TTabSheetTabSheet_VorsorgeCaptionVorsorge
ImageIndex 	TGroupBoxGroupBox_Vorsorg_listeLeft Top WidthHeight�AlignalLeftCaptionDatum / UntersuchungTabOrder  TPanelPanel_vorsorge_listLeftTop�WidthHeight3AlignalBottom
BevelOuterbvNoneCaptionPanel1TabOrder  TBitBtnBitBtn3LeftTopWidthJHeightCaptionNeuTabOrder OnClickBitBtn3ClickKindbkAll  TBitBtnBitBtn4LeftTopWidthKHeightCaptionL�schenTabOrderKindbkCancel   
TDBgridEXT
DBgridEXT2LeftTopWidthHeight�AlignalClientColorclInfoBk
DataSource#DataModule1.DataSource_untersuchungDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameDatumWidth6Visible	 Expanded	FieldNameUntersuchungWidth{Visible	 Expanded	FieldNameStatusWidthMVisible	     TPageControlPageControl_vorsorgeLeftTop Width�Height�
ActivePageTabSheet_vorsorge_UntersuchungAlignalClientTabOrder 	TTabSheetTabSheet_vorsorge_bewertungCaptionUntersuchungsergebnis TLabelUntersuchungsdatumLeft TopHWidtheHeightCaptionUntersuchungsdatum  TLabelUntersuchungstypLeft TopWidthVHeightCaptionUntersuchungstyp  TLabelStatusLeft0TopWidthHeightCaptionStatus  TSpeedButtonSpeedButton1Left� Top`WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  	TGroupBoxGroupBox_vorsorge_bewertungLeft Top� Width�Height� AlignalBottomCaption	BewertungTabOrder  TLabelLabel13LeftTop� Width6HeightCaption	Anmerkung  TDBRichEditDBRichEdit_bewertungLeftTop� Width�HeightA	DataFieldBeurteilung
DataSource#DataModule1.DataSource_untersuchungTabOrder   TDBRadioGroupDBRadioGroup_bewertungLeftTopWidth�HeightiCtl3D		DataFieldI_Beurteilung
DataSource#DataModule1.DataSource_untersuchungItems.StringsKeine gesundheitlichen BedenkenJGesundheitliche Bedenken unter bestimmten Voraussetzungen (s. Anmerkungen)4Befristete gesundheitliche Bedenken (s. Anmerkungen)!Dauernde gesundheitliche Bedenken ParentCtl3DTabOrderValues.Strings123456    TDBEditDBEdit_u_dateLeft Top`WidthyHeight	DataFieldDatum
DataSource#DataModule1.DataSource_untersuchungTabOrder  TDBLookupComboBoxDBLookupComboBox5Left0Top Width� Height	DataFieldI_Status
DataSource#DataModule1.DataSource_untersuchungKeyFieldNummer	ListFieldStatus
ListSourceDataModule1.DataSource_statusTabOrder  	TGroupBox	GroupBox4Left TopaWidth�HeightiAlignalBottomCaptionNachuntersuchungTabOrder TLabelLabel_nachuntersuchungLeft8TopWidth� HeightCaptionFrist f�r die Nachuntersuchung  TDBLookupComboBoxDBLookupComboBox9Left8Top4Width� Height	DataFieldI_frist
DataSource#DataModule1.DataSource_untersuchungKeyFieldI-Frist	ListFieldFrist
ListSourceDataModule1.DataSource_fristenTabOrder   TBitBtnBitBtn_unters_planenLeft Top0Width� HeightCaptionNachuntersuchung planenTabOrderOnClickBitBtn_unters_planenClickKindbkAll   TDBLookupComboBoxDBLookupComboBox_u_typLeft Top Width� Height	DataFieldI_typ
DataSource#DataModule1.DataSource_untersuchungKeyFieldNummer	ListFieldUntersuchungstyp
ListSource'DataModule1.DataSource_untersuchungstypTabOrder   	TTabSheetTabSheet_vorsorge_UntersuchungCaptionBefund
ImageIndex TToolBarToolBar2Left Top Width�Height(ButtonHeight"CaptionToolBar2TabOrder  TToolBarToolBar_befundLeft TopWidth'Height"HelpContextPAutoSize	BorderWidthCaptionToolBar_befundImages
ImageList2ParentShowHintShowHint	TabOrder Wrapable TToolButton
OpenbuttonLeft TopHintAImportieren einer RTF-Datei (bestehender Text wird �berschrieben)Caption
Openbutton
ImageIndex OnClickOpenbuttonClick  TToolButtonclosebuttonLeftTopHintExportieren des TextesCaptionclosebutton
ImageIndexOnClickclosebuttonClick  TToolButtonToolButton8Left.TopHintDrucken des TextesCaptionToolButton6
ImageIndex	OnClickToolButton8Click  TToolButtonToolButton14LeftETopWidthCaptionToolButton14
ImageIndex	StyletbsSeparator  TToolButton
UndobuttonLeftITopHint"Letzten Schritt r�ckg�ngig machen.Caption
Undobutton
ImageIndex
OnClickUndobuttonClick  TToolButton
CopybuttonLeft`TopHint Kopieren ins Clippboard [STRG+C]Caption
Copybutton
ImageIndexOnClickCopybuttonClick  TToolButtonPastebuttonLeftwTopHint%Einf�gen aus dem Clippboard. [STRG+V]CaptionPastebutton
ImageIndexOnClickPastebuttonClick  TToolButtonToolButton9Left� TopWidthCaptionToolButton1
ImageIndexStyletbsSeparator  	TComboBoxFontNameLeft� TopWidthnHeightHint
SchriftartCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ItemHeightParentCtl3D
ParentFontTabOrder TextFontNameOnChangeFontNameChange  TToolButtonToolButton11Left TopWidthCaptionToolButton11
ImageIndexStyletbsSeparator  TEditFontSizeLeftTopWidthHeightHintZeichengr��eFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderText0OnChangeFontSizeChange  TUpDownUpDown1LeftTopWidthHeight	AssociateFontSizeMin Position TabOrderWrap  TToolButtonToolButton10Left)TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButton
BoldButtonLeft-TopHintFett
AllowAllUp	Caption
BoldButton
ImageIndexStyletbsCheckOnClickBoldButtonClick  TToolButtonItalicButtonLeftDTopHintKursiv
AllowAllUp	CaptionItalicButton
ImageIndexStyletbsCheckOnClickItalicButtonClick  TToolButtonUnderlineButtonLeft[TopHintUnterstrichen
AllowAllUp	CaptionUnderlineButton
ImageIndexStyletbsCheckOnClickUnderlineButtonClick  TToolButtonToolButton_colorLeftrTopHint	TextfarbeCaptionToolButton_color
ImageIndexOnClickToolButton_colorClick  TToolButtonToolButton16Left�TopWidthCaptionToolButton16
ImageIndexStyletbsSeparator  TToolButton	LeftAlignLeft�TopHintLinksb�ndigCaption	LeftAlignGrouped	
ImageIndexStyletbsCheckOnClickLeftAlignClick  TToolButtonCenterAlignTagLeft�TopHint
ZentrierenCaptionCenterAlignGrouped	
ImageIndexStyletbsCheckOnClickCenterAlignClick  TToolButton
RightAlignTagLeft�TopHintRechtsb�ndigCaption
RightAlignGrouped	
ImageIndexStyletbsCheckOnClickRightAlignClick  TToolButtonToolButton12Left�TopWidthCaptionToolButton8
ImageIndex	StyletbsSeparator  TToolButtonBulletsButtonLeft�TopHint
Aufz�hlungCaptionBulletsButton
ImageIndexStyletbsCheckOnClickBulletsButtonClick    TPanelPanel_vorsorge_untersuchLeft Top�Width�Height)AlignalBottom
BevelOuterbvNoneCaptionPanel_vorsorge_untersuchTabOrder  TDBRichEditdbrichedit_befundLeft Top(Width�HeightyAlignalClient	DataFieldBefund
DataSource#DataModule1.DataSource_untersuchungTabOrder     	TTabSheetTabSheet_ImpfungenCaption	Impfungen
ImageIndex 	TGroupBoxGroupBox_titerLeft Top WidthHeight�AlignalLeftCaptionDatumTabOrder  TPanelPanel_impfung_listLeftTop�WidthHeight'AlignalBottom
BevelOuterbvNoneCaptionPanel1TabOrder  TBitBtnBitBtn7Left TopWidthJHeightCaptionNeuTabOrder OnClickBitBtn7ClickKindbkAll  TBitBtnBitBtn8LeftTop	WidthKHeightCaptionL�schenTabOrderKindbkCancel   
TDBgridEXT
DBgridEXT1LeftTopWidthHeight�AlignalClientColorclInfoBk
DataSourceDataModule1.DataSource_impfungDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameDatumWidthCVisible	 Expanded	FieldName	ImpfstoffWidth~Visible	 Expanded	FieldNameStatusWidth<Visible	     TPanelPanel_impfung_datenLeftTop Width�Height�AlignalClient
BevelOuterbvNoneTabOrder TLabelLabel4LeftHTop Width"HeightCaptionImpftyp  TLabel	ImpfstoffLeftHTop� Width(HeightCaption	Impfstoff  TLabelLabel6LeftTop Width"HeightCaptionCharge  TLabelLabel7LeftHTop WidthHeightCaptionDosis  TLabelLabel8LeftHTop`Width1HeightCaption	Impfdatum  TLabelLabel9LeftPTop WidthHeightCaptionStatus  TLabelImpfabfolgeLeftTop� Width7HeightCaptionImpfabfolge  TSpeedButtonSpeedButton2Left� ToppWidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton2Click  TDBEditDBEdit_impf_datumLeftIToppWidthyHeight	DataFieldDatum
DataSourceDataModule1.DataSource_impfungTabOrder   TDBEditDBEdit_impf_dosisLeftITopWidthyHeight	DataFieldDosis
DataSourceDataModule1.DataSource_impfungTabOrder  TDBEditDBEdit_impf_chargeLeftTopWidthyHeight	DataFieldCharge
DataSourceDataModule1.DataSource_impfungTabOrder  TDBLookupComboBoxDBLookupComboBox1LeftHTop0Width� Height	DataFieldI_typ
DataSourceDataModule1.DataSource_impfungKeyFieldNummer	ListFieldImpfung
ListSourceDataModule1.DataSource_impftypTabOrder  TDBLookupComboBoxDBLookupComboBox2LeftPTop0Width� Height	DataFieldI_status
DataSourceDataModule1.DataSource_impfungKeyFieldNummer	ListFieldStatus
ListSourceDataModule1.DataSource_statusTabOrder  TDBLookupComboBoxDBLookupComboBox3LeftTop� Width� Height	DataFieldI_Impfabfolge
DataSourceDataModule1.DataSource_impfungKeyFieldNummer	ListFieldImpfabfolge
ListSource"DataModule1.DataSource_impfabfolgeTabOrder  TDBLookupComboBoxDBLookupComboBox4LeftHTop� Width� Height	DataFieldI_stoff
DataSourceDataModule1.DataSource_impfungKeyFieldNummer	ListField	Impfstoff
ListSource DataModule1.DataSource_impfstoffTabOrder  	TGroupBox	GroupBox3Left Top�Width�Height^AlignalBottomCaptionNachuntersuchungTabOrder TLabelLabel3Left(TopWidth� HeightCaptionFrist f�r die Nachuntersuchung  TDBLookupComboBoxDBLookupComboBox_impffristLeft0Top4Width� Height	DataFieldI_frist
DataSourceDataModule1.DataSource_impfungKeyFieldI-Frist	ListFieldFrist
ListSourceDataModule1.DataSource_fristenTabOrder   TBitBtnBitBtn_impf_planenLeft� Top0Width� HeightCaptionNachuntersuchung planenTabOrderOnClickBitBtn_impf_planenClickKindbkAll     	TTabSheetTabSheet_laborCaptionLabor
ImageIndex 	TGroupBox	GroupBox1Left Top WidthHeight�AlignalLeftCaptionDatumTabOrder  TPanelPanel_Labor_listLeftTop�WidthHeight'AlignalBottom
BevelOuterbvNoneCaptionPanel1TabOrder  TBitBtnBitBtn9LeftTopWidthQHeightCaptionNeuTabOrder OnClickBitBtn9ClickKindbkAll  TBitBtnBitBtn10LeftTop	WidthRHeightCaptionL�schenTabOrderKindbkCancel   
TDBgridEXT
DBgridEXT3LeftTopWidthHeight�AlignalClientColorclInfoBk
DataSourceDataModule1.DataSource_laborDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameDatumWidth6Visible	 Expanded	FieldName	ParameterWidthoVisible	 Expanded	FieldNameStatusWidthZVisible	     TPanelPanel_labor_datenLeftTop Width�Height�AlignalClient
BevelOuterbvNoneTabOrder TSpeedButtonSpeedButton3Left� Top`WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton3Click  TLabelLabel1LeftTopWidthHeightCaptionStatus  TLabelLabel2Left0TopWidth0HeightCaption	Parameter  TLabelLabel5Left� Top� Width HeightCaptionEinheit  TLabelLabel10Left0Top� Width/HeightCaption	Laborwert  TLabelLabel11Left0TopPWidthHeightCaptionDatum  TDBEditDBEdit_lab_datumLeft1Top`WidthyHeight	DataFieldDatum
DataSourceDataModule1.DataSource_laborTabOrder   TDBEditDBEdit_labor_wertLeft0Top� Width� Height	DataFieldWert
DataSourceDataModule1.DataSource_laborTabOrder  TDBLookupComboBoxDBLookupComboBox6Left0Top(Width� Height	DataField	Parameter
DataSourceDataModule1.DataSource_labor	ListField	ParameterTabOrder  TDBLookupComboBoxDBLookupComboBox7Left� Top� WidthYHeight	DataField	I_Einheit
DataSourceDataModule1.DataSource_laborKeyFieldNummer	ListFieldEinheit
ListSourceDataModule1.DataSource_einheitTabOrder  TDBLookupComboBoxDBLookupComboBox8LeftTop(Width� Height	DataFieldI_status
DataSourceDataModule1.DataSource_laborKeyFieldNummer	ListFieldStatus
ListSourceDataModule1.DataSource_statusTabOrder  	TGroupBoxGroupBox_verlaufLeft TopWidth�Height� AlignalBottomCaptionVerlaufTabOrder 
Tsp_XYPlotPlotLeftTopWidth�Height� AlignalClientColor	clBtnFaceParentColorTabOrder LeftAxis.Margin LeftAxis.Max       �@LeftAxis.TicksCount
LeftAxis.LineAttr.ColorclBlackLeftAxis.LineAttr.Visible	LeftAxis.GridAttr.ColorclGrayLeftAxis.GridAttr.Visible	LeftAxis.LabelFormat###0.##LeftAxis.SFlagsLeftAxis.SLinePos� �  RightAxis.MarginRightAxis.Max       �@RightAxis.LineAttr.ColorclBlackRightAxis.LineAttr.Visible	RightAxis.GridAttr.ColorclGrayRightAxis.GridAttr.VisibleRightAxis.LabelFormat###0.##RightAxis.SFlags9RightAxis.SLinePos�� �  BottomAxis.MarginBottomAxis.Min       ��?BottomAxis.Max       �@BottomAxis.LineAttr.ColorclBlackBottomAxis.LineAttr.Visible	BottomAxis.GridAttr.ColorclGrayBottomAxis.GridAttr.Visible	BottomAxis.LabelFormat###0.##BottomAxis.SFlags BottomAxis.SLinePos� � TopAxis.MarginTopAxis.Max       �@TopAxis.LineAttr.ColorclBlackTopAxis.LineAttr.Visible	TopAxis.GridAttr.ColorclGrayTopAxis.GridAttr.VisibleTopAxis.LabelFormat###0.##TopAxis.SFlags8TopAxis.SLinePos� BorderStylebs_None
FieldColorclWhite   	TGroupBox	GroupBox2Left Top� Width�Height^AlignalBottomCaptionNachuntersuchungTabOrder TLabelLabel12Left(TopWidth� HeightCaptionFrist f�r die Nachuntersuchung  TDBLookupComboBoxDBLookupComboBox_laborfristLeft(Top+Width� Height	DataFieldI_frist
DataSourceDataModule1.DataSource_laborKeyFieldI-Frist	ListFieldFrist
ListSourceDataModule1.DataSource_fristenTabOrder   TBitBtnBitBtn_labor_planenLeftTopWidth� HeightCaptionNachuntersuchung planenTabOrderOnClickBitBtn_labor_planenClickKindbkAll     	TTabSheetTabSheet_TerminuebersichtCaptionTermin�bersicht
ImageIndex TMonthCalendarMonthCalendar1Left Top(Width� Height� CalColors.MonthBackColorclSilverDate X	�zZߍ@TabOrder WeekNumbers	  TRadioGroupRadioGroup_kalenderLeft Top� Width� Height� CaptionTermin�bersicht	ItemIndex Items.StringsTagWocheMonatJahr TabOrder  TStringGridStringGrid1Left� Top(Width�HeightyColCountDefaultColWidthPDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine TabOrder	ColWidthsP� f 
RowHeights      TPanel	Panel_topLeft TopWidth�HeightAlignalTop
BevelOuterbvNoneTabOrder TDBTextDBText_nameLeft(TopWidthiHeight	DataFieldName
DataSource!DataModule1.DataSource_StammdatenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText_vornameLeft� TopWidthAHeightColor	clBtnFace	DataFieldVorname
DataSource!DataModule1.DataSource_StammdatenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold ParentColor
ParentFont   TToolBarToolBar_mainLeft Top Width�HeightCaptionToolBarColor	clBtnFaceParentColorTabOrder TToolButtonToolButton1Left TopCaptionToolButton1
ImageIndex   TToolButtonToolButton2LeftTopCaptionToolButton2
ImageIndex  TToolButtonToolButton3Left.TopCaptionToolButton3
ImageIndex  TToolButtonToolButton4LeftETopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton5LeftMTopCaptionToolButton5
ImageIndex  TToolButtonToolButton6LeftdTopCaptionToolButton6
ImageIndex  TToolButtonToolButton7Left{TopCaptionToolButton7
ImageIndex   
TStatusBar
StatusBar1Left Top9Width�HeightPanels SimplePanel  	TMainMenu	MainMenu1Left�Top% 	TMenuItemDatei1CaptionDatei 	TMenuItem
Mandanten1CaptionFirmendatenOnClickMandanten1Click  	TMenuItemUntersuchungen1CaptionUntersuchungen  	TMenuItem
Impfungen1Caption	Impfungen  	TMenuItemTiter1CaptionTiter  	TMenuItemN2Caption-  	TMenuItemDrucken1CaptionDrucken  	TMenuItemN1Caption-  	TMenuItemBeenden1CaptionBeenden   	TMenuItemAuswertungen1CaptionAuswertungen   
Tsp_XYLineLinePlotPlotLineAttr.ColorclRedLineAttr.StylepsInsideFrameLineAttr.WidthLineAttr.Visible	PointAttr.KindptRectanglePointAttr.VisibleLeft�Top(  
TImageList
ImageList2Left$Top$Bitmap
�  IL     �������������BM6      6  (   @   @                                  �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ���                                         � �               �  �                                                           ��������                  �   �   �                 ����      �����������������                      � �       ��������������������������  �                             ����   �����������������          � �          �  � �������� ����������������                             �   ��������  ����������������             �   �           ��   ����������������      �               �       ������   ����������������                               �        ��   ����������������                 	               ��        �   ����������������                               � �  � ��   ����������������    �     �              �  �        ��        ����������������             �     �                          ����������������                    �                         ����������������     �   �     �   �                                                                  ��������                                          ��������                                       ��������                                                     ��������               �������                           ��  �                  ��������                 ��������   ���                  �������            ��������   ���                                          �  �   �   �  �                 ��������                 ��������  ���                   �    � �                 �    � � �����                     ��������                 ���������������                    �  �                     �  �      ���                   ���� ��                      ���� ��   ���                   � � �                       � � �   ����                   ����                         ����   �����                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             �                                            ��������           ��                                           ��������       �������                                          �  �   �  ���� ��������                                         ��������  ���� ���������                                    �   �    � �  �  � ��������                                     ��  ��������  ���� �������                                    ����� �  �      �  ����� ��                                     ������ ��� ��   �������� �                                      �������  � �    �  �                                            ������ ���      ���� ��                                         �����           �  � �                                            ��            ����                                              �                                                                                                                            BM>       >   (   @   @                                ��� ����    � ���� �� ��                                      ����   ��     ���        �             �      �      ��  �   ������ ��  ��� ��  ��� �  ��� ��  ��  ��  ��  ��  ��  ��  ��  ���� #����� �����  ���� #����� c���� Ï��?�������������������������������������?��������?�?���������������������?�����������������������?�?�������������������������� ������� �������  �����  ����   �����@ �����  ����   ��  ���? C ���  ���� �������������������������                        TFontDialogFontDialog1Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MinFontSize MaxFontSize LeftYTop&  TOpenDialog
OpenDialogFilterFRich Text Files (*.RTF)|*.RTF|Text Files (*.TXT)|*.TXT|Alles (*.*)|*.*
InitialDirc:\LeftwTop%  TPrintDialogPrintDialogCopiesLeft�Top$  TSaveDialog
SaveDialogFilter6Rich Text Files (*.RTF)|*.RTF|Text Files (*.TXT)|*.TXT
InitialDirc:\Left�Top%  TColorDialogColorDialogCtl3D	Left�Top'   