unit a_gefaehrdungen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, a_data,
  ExtCtrls, StdCtrls, ComCtrls, ToolWin, dbtreeview_kaskade, DBCtrls, Mask,db,
  Buttons, OleCtnrs, olecontainerext, OleCtrls, PdfLib_TLB, FileCtrl,shellapi,Variants,
  Grids, DBGrids, dbgridEXT;

type
  TForm_gefaehrdungen = class(TForm)
    ToolBar1: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_change: TToolButton;
    ToolButton4: TToolButton;
    ToolButton_save: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton9: TToolButton;
    ToolButton_abort: TToolButton;
    ToolButton11: TToolButton;
    ToolButton_ok: TToolButton;
    Panel_left: TPanel;
    Splitter: TSplitter;
    Panel_such: TPanel;
    dbtreeview: Tdbtreeview_k;
    Label_suchen: TLabel;
    Edit_such: TEdit;
    SpeedButton1: TSpeedButton;
    FileListBox: TFileListBox;
    Panel_re: TPanel;
    Notebook: TNotebook;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    DBEdit_gef1: TDBEdit;
    DBEdit2: TDBEdit;
    DBComboBox_pdf2: TDBComboBox;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit_gef2: TDBEdit;
    DBEdit1: TDBEdit;
    DBComboBox_pdf: TDBComboBox;
    BitBtn_acrobat: TBitBtn;
    Panel_gef: TPanel;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    DBMemo_gef3: TDBMemo;
    GroupBox2: TGroupBox;
    DBMemo_massnahme: TDBMemo;
    GroupBox3: TGroupBox;
    DBgridEXT1: TDBgridEXT;
    GroupBox4: TGroupBox;
    DBgridEXT2: TDBgridEXT;
    Panel1: TPanel;
    BitBtn_hin_notw: TBitBtn;
    BitBtn_weg_notw: TBitBtn;
    Panel2: TPanel;
    BitBtn_hin_fak: TBitBtn;
    BitBtn_weg_fak: TBitBtn;
    Label6: TLabel;
    DBEdit_e1_www: TDBEdit;
    DBEdit_e2_www: TDBEdit;
    Label8: TLabel;
    BitBtn_e1_www: TBitBtn;
    BitBtn_e2_www: TBitBtn;
    procedure dbtreeviewChange(Sender: TObject; Node: TTreeNode);
    procedure FormShow(Sender: TObject);
    procedure dbtreeviewChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure BitBtn_abt_neuClick(Sender: TObject);
    procedure BitBtn_ap_neuClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
    procedure Arbene_gefaehrdungsfaktorLButtonDown(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton_newClick(Sender: TObject);
    procedure ToolButton_okClick(Sender: TObject);
    procedure ToolButton_abortClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbtreeviewDblClick(Sender: TObject);
    procedure dbtreeviewDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure dbtreeviewDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ToolButton_changeClick(Sender: TObject);
    procedure Edit_suchEnter(Sender: TObject);
    procedure BitBtn_acrobatClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn_hin_notwClick(Sender: TObject);
    procedure BitBtn_weg_notwClick(Sender: TObject);
    procedure BitBtn_hin_fakClick(Sender: TObject);
    procedure BitBtn_e1_wwwClick(Sender: TObject);
  private
    { Private-Deklarationen }
    pdf_load,pfad_pdf:string;
    procedure gef1_neu;
    procedure gef2_neu;
    procedure gef3_neu;
    procedure bescheinigungen_aktualisieren;
    procedure bescheinigung_hinzufuegen(modus:integer);
  public
    { Public-Deklarationen }
  end;

var
  Form_gefaehrdungen: TForm_gefaehrdungen;

implementation
uses  a_main, a_u_wahl, a_unt_wahl;
{$R *.DFM}

procedure TForm_gefaehrdungen.dbtreeviewChange(Sender: TObject; Node: TTreeNode);
var
i:int64;
dat:string;
begin
if not dbtreeview.change_allowed then exit;
  case dbtreeview.a_ebene of
  1:begin
         notebook.PageIndex:=0;
         //tabsheet_main.caption:='Gefährdungs-Klasse';
    end;
  2:begin

         notebook.PageIndex:=1;
         //tabsheet_main.caption:='Gefährdungs-Gruppe';

  end;
  3: begin
     notebook.PageIndex:=2;
     bescheinigungen_aktualisieren;
     //tabsheet_main.caption:='Gefährdung / Belastung';
     end;
end;
toolbutton_new.Enabled:=true;
	toolbutton_change.Enabled:=true;
    toolbutton_del.Enabled:=false;
    toolbutton_save.Enabled:=false;
end;

procedure TForm_gefaehrdungen.FormShow(Sender: TObject);
var
dat:string;
begin
     form_main.form_positionieren(tform(sender));
     dbtreeviewChange(Sender, dbtreeview.selected);
     bescheinigungen_aktualisieren;
end;

procedure TForm_gefaehrdungen.dbtreeviewChanging(Sender: TObject;
  Node: TTreeNode; var AllowChange: Boolean);
begin
     ToolButton_saveClick(sender);

end;

procedure TForm_gefaehrdungen.ToolButton_saveClick(Sender: TObject);
begin
case dbtreeview.a_ebene of
  1:if datamodul.q_7.state in [dsedit, dsinsert] then
      begin
           datamodul.q_7.post;
           dbtreeview.name_neu(dbtreeview.akt_knoten[dbtreeview.a_ebene]);
      end;
  2:  begin
        //form_main.olecontainer_speichern(true,datamodul.q_8,'ole',arbene_gefaehrdungsfaktor);

        if datamodul.q_8.state in [dsedit, dsinsert] then
        begin
         datamodul.q_8.post;
         dbtreeview.name_neu(dbtreeview.akt_knoten[dbtreeview.a_ebene]);
      end;
      end;
  3:if datamodul.q_9.state in [dsedit, dsinsert] then
  begin
       datamodul.q_9.post;
       dbtreeview.name_neu(dbtreeview.akt_knoten[dbtreeview.a_ebene]);
  end;

end;
  form_main.sperren(notebook);

  toolbutton_new.Enabled:=true;
	toolbutton_change.Enabled:=true;
    toolbutton_del.Enabled:=false;
    toolbutton_save.Enabled:=false;
end;

procedure TForm_gefaehrdungen.BitBtn_abt_neuClick(Sender: TObject);
var
i:integer;
nr: int64;
begin
  ToolButton_saveClick(Sender);
  case dbtreeview.a_ebene of
  0,1:begin
          nr:= neuer_datensatz(datamodul.q_7,['nummer','name'],[null,'']) ;
           datamodul.q_7.post;
           dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_7,nr,'',1);
      end;
  2:begin
    nr:=getbigint(datamodul.q_7,'nummer');
    nr:=neuer_datensatz(datamodul.q_8,['nummer','name','i_gef1'],[null,'',inttostr(nr)]) ;
    dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[2] ,datamodul.q_8,nr,'',2);

    end;
  3:begin
    nr:=getbigint(datamodul.q_8,'nummer');
    nr:=neuer_datensatz(datamodul.q_9,['nummer','name','i_gef2'],[null,'',inttostr(nr)]) ;
    dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_9,nr,'',3);
    end;
end;

end;


procedure TForm_gefaehrdungen.gef1_neu;
var
i:integer;
nr: int64;
begin
     nr:= neuer_datensatz(datamodul.q_7,['nummer','name'],[null,'']) ;
     datamodul.q_7.post;
     dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_7,nr,'',1);
     dbedit_gef1.SetFocus;
end;

procedure tform_gefaehrdungen.gef2_neu;
var
i:integer;
nr: int64;
begin
    nr:=getbigint(datamodul.q_7,'nummer');
    nr:=neuer_datensatz(datamodul.q_8,['nummer','name','i_gef1'],[null,'',inttostr(nr)]) ;
    if dbtreeview.a_ebene=1 then
    dbtreeview.Selected:=dbtreeview.kind_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_8,nr,'',2,1)
    else
    dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[2] ,datamodul.q_8,nr,'',2);
    dbedit_gef2.SetFocus;

end;

procedure tform_gefaehrdungen.gef3_neu;
var
i:integer;
nr: int64;
begin
    nr:=getbigint(datamodul.q_8,'nummer');
    nr:=neuer_datensatz(datamodul.q_9,['nummer','name','i_gef2'],[null,'',inttostr(nr)]) ;
    if dbtreeview.a_ebene=2 then
    dbtreeview.Selected:=dbtreeview.kind_einfuegen( dbtreeview.akt_knoten[2] ,datamodul.q_9,nr,'',3,1)
    else
    dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[3] ,datamodul.q_9,nr,'',3);
    dbmemo_gef3.SetFocus;
end;

procedure TForm_gefaehrdungen.BitBtn_ap_neuClick(Sender: TObject);
var
i:integer;
nr:int64;
begin
ToolButton_saveClick(Sender);
  case dbtreeview.a_ebene of
  1:begin
    nr:=getbigint(datamodul.q_7,'nummer');
    nr:=neuer_datensatz(datamodul.q_8,['nummer','name','i_gef1'],[null,'',inttostr(nr)]) ;
    dbtreeview.Selected:=dbtreeview.kind_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_8,nr,'',2,1);
    end;
  2,3:begin
    nr:=getbigint(datamodul.q_8,'nummer');
    nr:=neuer_datensatz(datamodul.q_9,['nummer','name','i_gef2'],[null,'',inttostr(nr)]) ;
    dbtreeview.Selected:=dbtreeview.kind_einfuegen( dbtreeview.akt_knoten[2] ,datamodul.q_9,nr,'',3,1);
    end;
end;

end;



procedure TForm_gefaehrdungen.ToolButton_delClick(Sender: TObject);
begin
if (MessageDlg( Format('Soll " %s " gelöscht werden?',
            [dbtreeview.selected.text]),mtConfirmation, [mbYes,mbNo], 0)<>mryes) then exit;
case dbtreeview.a_ebene of
  1: begin
         if  dbtreeview.selected.HasChildren then
         begin
              showmessage('Bitte zuerst "Gefährdungsgruppe" löschen');
              exit;
         end;
         datamodul.q_7.edit;
         datamodul.q_7['storno']:=1;
         datamodul.q_7.post;
         dbtreeview.selected:=dbtreeview.knoten_entfernen(dbtreeview.selected);
         dbtreeview.change(dbtreeview.Selected);
     end;
  2: begin
         if  dbtreeview.selected.HasChildren then
         begin
              showmessage('Bitte zuerst "Gefährdung / Belastung" löschen');
              exit;
         end;
         datamodul.q_8.edit;
         datamodul.q_8['storno']:=1;
         datamodul.q_8.post;
         dbtreeview.selected:=dbtreeview.knoten_entfernen(dbtreeview.selected);
         dbtreeview.change(dbtreeview.Selected);

     end;
  3:begin
         datamodul.q_9.edit;
         datamodul.q_9['storno']:=1;
         datamodul.q_9.post;
         dbtreeview.selected:=dbtreeview.knoten_entfernen(dbtreeview.selected);
         dbtreeview.change(dbtreeview.Selected);
     end;
end;
toolbutton_new.Enabled:=true;
	toolbutton_change.Enabled:=true;
    toolbutton_del.Enabled:=false;
    toolbutton_save.Enabled:=false;
end;

procedure TForm_gefaehrdungen.Arbene_gefaehrdungsfaktorLButtonDown(
  Sender: TObject);
begin
{if  arbene_gefaehrdungsfaktor.State in [osEmpty	] then
     begin
         form_main.dok_erstellen( arbene_gefaehrdungsfaktor,12,false,'drucken','nicht_schliessen','','a_gefaehrdungsfaktor',datamodul.q_5,[''],['']);
         //datamodul.q_vorlagen
         //olecontainer_laden(datamodul.q_vorlagen,'ole',arbene_ap);
     end
     else arbene_gefaehrdungsfaktor.DoVerb(ovshow);}
end;

procedure TForm_gefaehrdungen.Edit_suchKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

begin
if key<>vk_return then exit;
dbtreeview.suche_datensatz(edit_such.text);

end;



procedure TForm_gefaehrdungen.ToolButton_newClick(Sender: TObject);
var
   f_u_wahl:tform_u_wahl;
begin
try
   ToolButton_saveClick(Sender);
   f_u_wahl:=tform_u_wahl.Create(self);
   f_u_wahl.RadioGroup.Columns:=1;
   f_u_wahl.RadioGroup.Font.Size:=10;
   f_u_wahl.Panel_date.visible:=false;
   case dbtreeview.a_ebene of
   0:begin
          f_u_wahl.RadioGroup.Items.Add('Gefährdungsklasse');
     end;
   1:begin
          f_u_wahl.RadioGroup.Items.Add('Gefährdungsklasse');
          f_u_wahl.RadioGroup.Items.Add('Gefährdungsgruppe');
     end;
   2:begin
          f_u_wahl.RadioGroup.Items.Add('Gefährdungsgruppe');
          f_u_wahl.RadioGroup.Items.Add('Gefährdung / Belastung');
     end;
   3:begin
          f_u_wahl.RadioGroup.Items.Add('Gefährdung / Belastung');
     end;
   end;
   f_u_wahl.RadioGroup.itemindex:=0;
   if f_u_wahl.ShowModal<>mrok then exit;
   case dbtreeview.a_ebene of
   0: if f_u_wahl.RadioGroup.Itemindex=0 then gef1_neu;
   1:begin
          if f_u_wahl.RadioGroup.Itemindex=0 then gef1_neu else   gef2_neu;
     end;
   2:begin
          if f_u_wahl.RadioGroup.Itemindex=0 then gef2_neu else gef3_neu ;
     end;
   3:begin
          gef3_neu;
     end;
	 end;
	 dbtreeview.onchange(self,dbtreeview.Selected);
	 form_main.entsperren(notebook);

    toolbutton_new.Enabled:=false;
	toolbutton_change.Enabled:=false;
    toolbutton_del.Enabled:=true;
    toolbutton_save.Enabled:=true;
      //ToolButton_changeClick(Sender);
finally
f_u_wahl.release;
end;
end;

procedure TForm_gefaehrdungen.ToolButton_okClick(Sender: TObject);
begin
     ToolButton_saveClick(sender);
     modalresult:=mrok;
end;

procedure TForm_gefaehrdungen.ToolButton_abortClick(Sender: TObject);
begin
     ToolButton_saveClick(sender);
     modalresult:=mrabort;
end;

procedure TForm_gefaehrdungen.SpeedButton1Click(Sender: TObject);
begin
dbtreeview.suche_datensatz(edit_such.text);
end;

procedure TForm_gefaehrdungen.FormCreate(Sender: TObject);
var i:integer;

begin
panel_left.width:=splitter_gefaehrdungen;
pfad_pdf:=wurzelpfad(application.ExeName)+'\infotexte\';
dbtreeview.liste_lesen(1);
if fileexists(pfad_pdf+'*.pdf') then
begin
	filelistbox.directory:=pfad_pdf;
end;
filelistbox.mask:='*.pdf';

for i:=0 to filelistbox.Items.count-1 do
begin
     dbcombobox_pdf.Items.Add(filelistbox.Items[i]);
     dbcombobox_pdf2.Items.Add(filelistbox.Items[i]);
end;

toolbutton_new.Enabled:=true;
toolbutton_change.Enabled:=true;
toolbutton_del.Enabled:=false;
toolbutton_save.Enabled:=false;
end;

procedure TForm_gefaehrdungen.dbtreeviewDblClick(Sender: TObject);
begin
//if dbtreeview.a_ebene=3 then  ToolButton_okClick(Sender);
end;

procedure TForm_gefaehrdungen.dbtreeviewDragDrop(Sender, Source: TObject;
  X, Y: Integer);
var
   daten_sender, daten_source,d:pnodepointer;
   e_sender, e_source:integer;
   nr:int64;
  AnItem,p: TTreeNode;
  AttachMode: TNodeAttachMode;
  HT: THitTests;
begin

   if not( sender is ttreenode)  then exit;
   if not( source is ttreenode)  then exit;
   daten_sender:=ttreenode(sender).data;
   daten_source:=ttreenode(source).data;
   e_sender:=daten_sender.ebene;
   e_source:=daten_source.ebene;
   if e_sender <>e_source then exit;
   

end;

procedure TForm_gefaehrdungen.dbtreeviewDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
var
anitem:ttreenode;
begin
if ttreenode(sender) =ttreenode(source) then  Accept :=true else
	 accept:=false;
   AnItem := dbtreeview.GetNodeAt(X, Y);
end;

procedure TForm_gefaehrdungen.ToolButton_changeClick(Sender: TObject);
begin
form_main.entsperren(notebook);

toolbutton_new.Enabled:=false;

toolbutton_change.Enabled:=false;
toolbutton_del.Enabled:=true;
toolbutton_save.Enabled:=true;

{case dbtreeview.a_ebene of
  1: datamodul.q_7.Refresh;//CurrRow;
  2: datamodul.q_8.Refresh;//CurrRow;
  3: datamodul.q_9.Refresh;//CurrRow;
end;  }

end;

procedure TForm_gefaehrdungen.Edit_suchEnter(Sender: TObject);
begin
//dbtreeview.Selected:=dbtreeview.Items[0];
//dbtreeview.change(dbtreeview.selected);
end;

procedure TForm_gefaehrdungen.BitBtn_acrobatClick(Sender: TObject);
var
   p,s:string;
begin
        if notebook.pageindex=0 then s:=trim(dbcomboBox_pdf2.Text) else s:=trim(dbcomboBox_pdf.Text);
        if s='' then exit;
        p:=pfad_pdf+s;
        shellexecute_se(0,'open',pchar(p),pchar(''),nil,SW_showmaximized);
end;

procedure TForm_gefaehrdungen.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     ToolButton_saveClick(sender);
     splitter_gefaehrdungen:=panel_left.width;
end;

procedure TForm_gefaehrdungen.FormDestroy(Sender: TObject);
begin
     dbtreeview.liste_loeschen;
end;

procedure TForm_gefaehrdungen.bescheinigungen_aktualisieren;
var
query,ig3:string;
begin
with datamodul do
begin
  ig3:=q_9.findfield('nummer').AsString;

  query:='select left(typ.untersuchung,150) as bescheinigung, case typ.i_arbmedvv when 0 then "Eignung" when 1 then "ArbmedVV" end grundlage, gefaehrdung_vorsorge.nummer as nummer from gefaehrdung_vorsorge left join typ on (gefaehrdung_vorsorge.i_typ=typ.nummer)';
  query:=query+format(' where (i_gef3=%s and modus=0 and gefaehrdung_vorsorge.storno=0)',[ig3]);

  sql_new(false,q_15,query,'');
  query:='select left(typ.untersuchung,150) as bescheinigung, case typ.i_arbmedvv when 0 then "Eignung" when 1 then "ArbmedVV" end grundlage, gefaehrdung_vorsorge.nummer as nummer from gefaehrdung_vorsorge left join typ on (gefaehrdung_vorsorge.i_typ=typ.nummer)';
  query:=query+format(' where (i_gef3=%s and modus=1 and gefaehrdung_vorsorge.storno=0)',[ig3]);

  sql_new(false,q_16,query,'');

end;
end;

procedure TForm_gefaehrdungen.BitBtn_hin_notwClick(Sender: TObject);
begin
  bescheinigung_hinzufuegen(0);
end;

procedure TForm_gefaehrdungen.BitBtn_hin_fakClick(Sender: TObject);
begin
   bescheinigung_hinzufuegen(1);
end;

procedure TForm_gefaehrdungen.bescheinigung_hinzufuegen(modus:integer);
var
  s_typ,s_gef3 ,query:string;
  i:integer;
  knoten: ttreenode;
  daten:pnodepointer;
  i_paw,i_arbmedvv,i_typ:integer;
  i_gef3: int64;
  felder :array [0..2] of string;
begin
try
query:='select * from gefaehrdung_vorsorge';
datamodul.sql_new(false,datamodul.q_14,query,'');

i_gef3:=getbigint( datamodul.q_9,'nummer');
s_gef3:=datamodul.q_9.findfield('nummer').AsString;
//####################
 form_untwahl:=tform_untwahl.Create(self);
 form_untwahl.Notebook.Width:=0 ;
  form_untwahl.ini(1,now());
  form_untwahl.Notebook.PageIndex:=8;
  form_untwahl.multiselect:=true;

  form_untwahl.einzelauswahl:=true;
  //form_main.combolesen( form_untwahl.ComboBox_uart,'untersuchungsart2','name','nummer','');
	//form_main.combogehezu(form_untwahl.ComboBox_untersucher,akt_untersucher);
	//form_main.entsperren(form_untwahl.Notebook );

	if form_untwahl.showmodal<>mrok then exit;

	if form_untwahl.Tree.Selected=nil then exit;
  form_untwahl.Tree.Selected.StateIndex:=1;

	for i:=0 to  form_untwahl.Tree.Items.Count-1 do
	begin
		if form_untwahl.Tree.Items[i].stateindex>0 then //ausgewählt
		begin
       knoten :=form_untwahl.Tree.items[i];
       daten:=knoten.Data;
       i_typ:=daten^.nummer;
       query:=format('select i_paw,i_arbmedvv,ueberschrift from typ where nummer=%d',[i_typ]);
       mysql_d.Feldarray(query,[0,1,2],felder);

       i_paw:=strtoint(felder[0]);
       i_arbmedvv:=strtoint(felder[1]);
       if felder[2]='0' then   //keine überschrift
       begin
          neuer_datensatz(datamodul.q_14,['nummer','i_typ','i_gef3','modus'],[null,i_typ,s_gef3,modus]);
          datamodul.q_14.Post;
       end;
      end;
	end;


//###########################

           

finally
  //form_main.combodeaktivieren(form_untwahl.ComboBox_uart );
  form_untwahl.Release;
  bescheinigungen_aktualisieren;
end;
end;

procedure TForm_gefaehrdungen.BitBtn_weg_notwClick(Sender: TObject);
var
nr,query:string;
begin
  if sender =bitbtn_weg_notw then
  nr:=datamodul.q_15.FindField('nummer').AsString
  else
  nr:=datamodul.q_16.FindField('nummer').AsString;
  query:=format('update gefaehrdung_vorsorge set storno =1 where nummer=%s',[nr]);
  mysql_d.sql_exe(query);
  bescheinigungen_aktualisieren;
end;



procedure TForm_gefaehrdungen.BitBtn_e1_wwwClick(Sender: TObject);
var
html_name:string;
begin
if sender = BitBtn_e1_www then
	html_name:=dbedit_e1_www.Text
  else
  html_name:=dbedit_e2_www.Text ;
  if trim(html_name)<>'' then shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal);
end;

end.
