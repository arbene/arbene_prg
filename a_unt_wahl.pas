unit a_unt_wahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_data,
  ComCtrls, TreeView_ext, ToolWin, ExtCtrls, StdCtrls, DBCtrls, Mask,
  Buttons, Grids, DBGrids, dbgridEXT, audiogramm,Bersonderheit_edit, Spin,db,
  dbcombo_number,Variants, DBEdit_time, Menus;


type
	TForm_untwahl = class(TForm)
	  Panel_botttom: TPanel;
    Panel_main: TPanel;
    ToolBar: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton4: TToolButton;
    ToolButton_delete: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_close: TToolButton;
    Notebook: TNotebook;
    Label4: TLabel;
    Label7: TLabel;
    Label13: TLabel;
    DBEdit_unt_dauer: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label_einheit: TLabel;
    DBEdit_labeinheit: TDBEdit;
	  DBEdit4: TDBEdit;
    ToolButton_save: TToolButton;
	  ToolButton6: TToolButton;
    DBCheckBox_group: TDBCheckBox;
    SpeedButton3: TSpeedButton;
    Label1: TLabel;
    Label45: TLabel;
	  ComboBox_untersucher: TComboBox;
    MaskEdit_datum: TMaskEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    PageControl: TPageControl;
    TabSheet_1: TTabSheet;
    TabSheet_2: TTabSheet;
    Label3: TLabel;
    Panel2: TPanel;
    GroupBox_memo: TGroupBox;
    Notebook_befunde: TNotebook;
    Label5: TLabel;
    Einheit: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Panel3: TPanel;
    SpeedButton_up: TSpeedButton;
    SpeedButton_down: TSpeedButton;
    BitBtn3: TBitBtn;
    BitBtn_veraendern: TBitBtn;
    BitBtn4: TBitBtn;
    Panel4: TPanel;
    Panel5: TPanel;
    audiogramm1: Taudiogramm;
	  Label6: TLabel;
    Label12: TLabel;
    DBEdit_name: TDBEdit;
    DBRadioGroup_erscheinungsbild: TDBRadioGroup;
    DBEdit_bez: TDBEdit;
	  DBEdit_bezdim: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    RichEdit_befunde: TRichEdit;
    DBEdit_kuerzel: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    DBMemo_nm: TDBMemo;
    DBMemo_nf: TDBMemo;
    StatusBar: TStatusBar;
    GroupBox1: TGroupBox;
    Label26: TLabel;
    DBEdit_euro_bef: TDBEdit;
    DBEdit_goae_bef: TDBEdit;
    DBCheckBox_ext: TDBCheckBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    GroupBox2: TGroupBox;
    Label27: TLabel;
    DBEdit_euro_lab: TDBEdit;
    DBEdit_goae_lab: TDBEdit;
    RadioButtonl1: TRadioButton;
    RadioButtonl2: TRadioButton;
    DBComboBox1: TDBComboBox;
    DBEdit3: TDBEdit;
    Label18: TLabel;
    DBGrid1: TDBGrid;
    DBCheckBox_reli: TDBCheckBox;
    button_labor: TButton;
    Button_goae: TButton;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Panel_li: TPanel;
    Panel6: TPanel;
    Label_suchen: TLabel;
    SpeedButton4: TSpeedButton;
    Edit_such: TEdit;
    Tree: TTreeView_ext;
    Label19: TLabel;
    Label24: TLabel;
    GroupBox_zeit: TGroupBox;
    GroupBox4: TGroupBox;
    DBMemo_zeit_leistung: TDBMemo;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    DBMemo_aufwand: TDBMemo;
    DBCheckBox_Aufwand_all: TDBCheckBox;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    DBCheckBox_ausblenden: TDBCheckBox;
    DBMemo_memo_vorsorge: TDBMemo;
    Label32: TLabel;
    DBMemo_labor: TDBMemo;
    PopupMenu_untwahl: TPopupMenu;
    m_alleitems: TMenuItem;
    DBMemo_impfung: TDBMemo;
    DBMemo_untersuchung: TDBMemo;
    Label33: TLabel;
    PopupMenu_richedit: TPopupMenu;
    dropdown_richedit_Fett: TMenuItem;
    dropdown_richedit_Kursiv: TMenuItem;
    dropdown_richedit_Unterstrichen: TMenuItem;
    dropdown_richedit_zurcksetzen: TMenuItem;
    BitBtn_laborhinweise: TBitBtn;
    DBMemo_tbs: TDBMemo;
    DBEdit_tbs_name: TDBEdit;
    Label34: TLabel;
    Label35: TLabel;
    GroupBox_tbs_auswahl: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    ComboBox_tbs_gi: TComboBox;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    GroupBox_labor_hinweis: TGroupBox;
    DBGrid_labor_hinweise: TDBGrid;
    BitBtn_uebernehmen: TBitBtn;
    GroupBox_taetigkeiten: TGroupBox;
    Panel8: TPanel;
    DBGrid_taetigkeiten: TDBGrid;
    Panel9: TPanel;
    Label25: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBCombo_num_abrechnungskriterium: TDBCombo_num;
    DBCombo_num_abrechnung_zeitkonto: TDBCombo_num;
    DBEdit_zeitansatz: TDBEdit;
    DBMemo_zeit_aufgabenfeld: TDBMemo;
    BitBtn_taetigkeiten: TBitBtn;
    DBEdit_zeit: TDBEdit_time;
    spinedit_tbs_uw: TEdit;
    spinedit_tbs_ow: TEdit;
    spinedit_tbs_uww: TEdit;
    spinedit_tbs_oww: TEdit;
	  procedure ToolButton_closeClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
	  procedure FormShow(Sender: TObject);
    procedure ToolButton_newClick(Sender: TObject);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure ToolButton_deleteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeDblClick(Sender: TObject);
    procedure TreeChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
	  procedure SpeedButton3Click(Sender: TObject);
	  procedure BitBtn_okClick(Sender: TObject);
	  procedure BitBtn3Click(Sender: TObject);
	  procedure BitBtn_veraendernClick(Sender: TObject);
	  procedure BitBtn4Click(Sender: TObject);
	  procedure SpeedButton_upClick(Sender: TObject);
	  procedure SpeedButton_downClick(Sender: TObject);
	  procedure PageControlChange(Sender: TObject);
	  procedure MaskEdit_laborExit(Sender: TObject);
	  procedure DBEdit_laborChange(Sender: TObject);
	  procedure DBEdit_nameChange(Sender: TObject);
	  procedure MaskEdit_befundExit(Sender: TObject);
	  procedure RadioButton1Click(Sender: TObject);
	  procedure RadioButtonl1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure button_laborClick(Sender: TObject);
    procedure Button_goaeClick(Sender: TObject);
    procedure Edit_suchEnter(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton4Click(Sender: TObject);
    procedure ini(bereich:integer;datum:tdate);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid1EditButtonClick(Sender: TObject);
    procedure DBGrid1Exit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn_abbClick(Sender: TObject);
    procedure DBEdit_zeitansatzChange(Sender: TObject);
    procedure DBEdit_zeitansatzKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn_taetigkeitenClick(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure m_alleitemsClick(Sender: TObject);
    procedure RichEdit_befundeEnter(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BitBtn_laborhinweiseClick(Sender: TObject);
    procedure DBEdit_tbs_untere_grenzeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpinEdit_tbs_uw1Exit(Sender: TObject);
    procedure SpinEdit_tbs_ow1Exit(Sender: TObject);
    procedure ComboBox_tbs_giExit(Sender: TObject);
    procedure SpinEdit_tbs_uww1Exit(Sender: TObject);
    procedure SpinEdit_tbs_oww1Exit(Sender: TObject);
    procedure BitBtn_uebernehmenClick(Sender: TObject);
    procedure PageControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure spinedit_tbs_uwKeyPress(Sender: TObject; var Key: Char);
    procedure spinedit_tbs_uwChange(Sender: TObject);
    procedure DBCombo_num_abrechnungskriteriumEnter(Sender: TObject);
    procedure DBCombo_num_abrechnungskriteriumExit(Sender: TObject);

	private
	  { Private-Deklarationen }
	  procedure reihenfolge(richtung:integer);
	  procedure berlist_filtern;
	  procedure speichern;
	  procedure memoladen;
      procedure formularsperren;
      procedure formularentsperren;

	public
	  { Public-Deklarationen }
	  multiselect,einstellung:boolean;
	  parent_markieren:boolean;
     einzelauswahl:boolean;
     snr:string;
     anzeigen:boolean;
     
	end;

var
	Form_untwahl: TForm_untwahl;

implementation

uses a_main, a_kalender, a_richedit_menue;

{$R *.DFM}

procedure tform_untwahl.formularsperren;
begin
form_main.sperren(notebook.Components[notebook.PageIndex]);
//form_main.sperren(notebook);
toolbutton_new.Enabled:=true;
toolbutton_edit.Enabled:=true;
toolbutton_save.Enabled:=false;
toolbutton_delete.Enabled:=false;
richedit_befunde.PopupMenu:=nil;
end;

procedure tform_untwahl.formularentsperren;
begin
form_main.entsperren(notebook);
toolbutton_new.Enabled:=false;
toolbutton_edit.Enabled:=false;
toolbutton_save.Enabled:=true;
toolbutton_delete.Enabled:=true;
richedit_befunde.PopupMenu:=form_main.PopupMenu_richedit;
end;

procedure  tform_untwahl.speichern;
begin
 //if form_untwahl.tag=0 then exit;
 case notebook.pageindex of

 4:begin
		//form_main.strings_speichern(datamodul.q_bereich,'memo',richedit_befunde.Lines); ######06.03.2017
    richeditspeichern( datamodul.q_bereich,'memo',richedit_befunde);
     if datamodul.q_bereich.state in [dsedit, dsinsert] then  datamodul.q_bereich.post;

	 end;
 end;
 tree.tabelle_post;

end;



procedure TForm_untwahl.ToolButton_closeClick(Sender: TObject);
begin
//speichern;
//modalresult:=mrok;
BitBtn_okClick(Sender);
end;


procedure TForm_untwahl.ToolButton_editClick(Sender: TObject);
begin
with datamodul do
begin
	//form_main.entsperren(notebook);
    formularentsperren;
    panel1.Enabled:=false;
end;
end;

procedure TForm_untwahl.TreeChange(Sender: TObject; Node: TTreeNode);
var
daten:pnodepointer;
knoten:ttreenode;
query,snr:string;
begin
 if not anzeigen then exit;
  if notebook.PageIndex=2 then  //labor
	begin
	   snr:=DataModul.q_typ_tree.findfield('nummer').asstring;
    //auswahl aktueller hinweis
    query:='select textbausteine.*,SUBSTR(tbs,1,500) as tbs_s,tabelle_tbs.i1,tabelle_tbs.i2,tabelle_tbs.i1w,tabelle_tbs.i2w,tabelle_tbs.s1 as tbs_s1 from textbausteine left join tabelle_tbs on (tabelle_tbs.i_tbs =textbausteine.nummer) ';
    query:=query+format(' left join typ on(typ.nummer=tabelle_tbs.i_tab_nummer) where typ.nummer=%s  ',[snr]);
    query:=query + ' and tabelle_tbs.storno=0  and textbausteine.i_kapitel>=0 ' ;
    datamodul.sql_new(false,datamodul.q_hinweis,query,'');
    if datensatz(datamodul.q_hinweis) then GroupBox_labor_hinweis.Visible:=true else  GroupBox_labor_hinweis.Visible:=false;
	end;

  if not anzeigen then exit;
	//if form_untwahl.tag=0 then exit;
	//if toolbar.visible then form_main.p_sperren(datamodul.q_typ,user_id,[panel_main]);

	if notebook.PageIndex=2 then  //labor
	begin
		if dbedit_euro_lab.text<>'0' then radiobuttonl2.Checked:=true else radiobuttonl1.Checked:=true ;
		RadioButtonl1Click(nil);
    
	end;

	if notebook.PageIndex=4 then
	begin
		pagecontrol.ActivePage:=tabsheet_1;
		memoladen;
		if prg_typ<>3 then
		begin
		  if dbedit_euro_bef.text<>'0' then radiobutton2.Checked:=true else radiobutton1.Checked:=true ;
		  RadioButton1Click(nil);
		end;
	end;

  if notebook.PageIndex=5 then  //aufgabenfelder
	begin
      snr:=datamodul.q_3.findfield('nummer').asstring;
      query:=format('select left(aufwand.name,255) as taetigkeit  from anlass_aufwand left join aufwand on (anlass_aufwand.i_aufwand =aufwand.nummer) where i_anlass=%s and anlass_aufwand.storno=0 and aufwand.storno=0',[snr]);
      datamodul.sql_new(false,datamodul.q_9,query, '');
      if datensatz(datamodul.q_9) then GroupBox_taetigkeiten.Visible:=true else GroupBox_taetigkeiten.Visible:=false;
  end;


  if notebook.PageIndex=9 then   //textbausteine
  begin
  spinedit_tbs_uw.text:='0';
  spinedit_tbs_ow.text:='0';
  SpinEdit_tbs_oww.text:='0';
  SpinEdit_tbs_uww.text:='0';
  ComboBox_tbs_gi.Text:='';
  if (node<>nil) and (node.StateIndex=3) then
  begin
    daten:=node.data;
    //f:=daten^.felder;

    if daten^.liste<>nil then
    begin
      spinedit_tbs_uw.text:=(daten^.liste[0]);
      spinedit_tbs_ow.text:=(daten^.liste[1]);
      spinedit_tbs_uww.text:=(daten^.liste[2]);
      spinedit_tbs_oww.text:=(daten^.liste[3]);
      ComboBox_tbs_gi.Text:=daten^.liste[4];
    end ;

  end;
  end;

end;

procedure TForm_untwahl.FormShow(Sender: TObject);
begin
     //if toolbar.visible then form_main.p_sperren(datamodul.q_typ,user_id,[panel_main]);
     //sonst l�st sichs nicth entsperren
     if trim(edit_such.text)<>''then
      begin
      tree.suche_datensatz(edit_such.text);
      tree.SetFocus;
      end
     else
      begin
      if tree.Items.Count>0 then tree.Selected:=tree.TopItem;
      Edit_such.SetFocus;
      end;
end;

procedure TForm_untwahl.ToolButton_newClick(Sender: TObject);
var
nr,i_aufwand,i_tbs:int64;
p,rf,query,tab_nr:string;
knoten: ttreenode;
daten:pnodepointer;
list:tstringlist;
begin
//tree.neu('',1);
//form_main.entsperren(notebook);
if notebook.pageindex = 9 then
begin
  anzeigen:=false;
end;
 speichern;
formularentsperren;
case notebook.pageindex of

2: begin //kabor
     p:= datamodul.q_typ_tree.findfield('parent').asstring;
     rf:=datamodul.q_typ_tree.findfield('reihenfolge').asstring;
     if p='' then p:='0';
     if rf='' then rf:='0';
     nr:=neuer_datensatz(datamodul.q_typ_tree,['nummer','bereich','reihenfolge','parent'],[null,3,rf,p]);
     datamodul.q_typ_tree.post;
     nr:=datamodul.q_typ_tree.findfield('nummer').asinteger;
   end;
3: begin
   end;
4: begin //befunde
   p:= datamodul.q_bereich.findfield('parent').asstring;
   rf:=datamodul.q_bereich.findfield('reihenfolge').asstring;
   if p='' then p:='0';
     if rf='' then rf:='0';
   nr:=neuer_datensatz(datamodul.q_bereich,['nummer','name','parent','modus','reihenfolge'],[null,'',p,0,rf]);
   datamodul.q_bereich.post;
   nr:=datamodul.q_bereich.findfield('nummer').asinteger;
   richedit_befunde.Lines.Clear;
   end;
5: begin //abrechnungsanl�sse
     p:= datamodul.q_3.findfield('parent').asstring;
     rf:=datamodul.q_3.findfield('reihenfolge').asstring;
     if p='' then p:='0';
     if rf='' then rf:='0';
     nr:=neuer_datensatz(datamodul.q_3,['nummer','name','parent','reihenfolge','zeitvorgabe'],[null,'',p,rf,'0']);
     datamodul.q_3.post;
     nr:=getbigint( datamodul.q_3,'nummer');

   end;
7: begin //abrechnungs-t�tigkeiten
     p:= datamodul.q_4.findfield('parent').asstring;
     rf:=datamodul.q_4.findfield('reihenfolge').asstring;
     if p='' then p:='0';
     if rf='' then rf:='0';
     nr:=neuer_datensatz(datamodul.q_4,['nummer','name','parent','reihenfolge'],[null,'',p,rf]);
     datamodul.q_4.post;
     nr:=getbigint( datamodul.q_4,'nummer');
     if knoten<>nil then knoten.StateIndex:=3;
   end;
9: begin  //hinweise
     // tab_nr:=datamodul.q_5.findfield('i_tab_nummer').asstring;
      p:= datamodul.q_5.findfield('parent').asstring;
     rf:=datamodul.q_5.findfield('reihenfolge').asstring;
     if p='' then p:='0';
     if rf='' then rf:='0';
     nr:=neuer_datensatz(datamodul.q_5,['nummer','name','parent','reihenfolge'],[null,'',p,rf]);
     datamodul.q_5.post;
     nr:=getbigint( datamodul.q_5,'nummer');

   end;

end;


tree.liste_lesen;
tree.reihenfolge_neu;
//jetzt markieren
case notebook.pageindex of
7: begin

  		query:=format('select i_aufwand from anlass_aufwand where i_anlass=%s and storno=0',[snr]);
      datamodul.sql_new(false,datamodul.q_7,query, '');
      datamodul.q_7.first;
      while not datamodul.q_7.eof do
      begin
      	i_aufwand:=getbigint( datamodul.q_7,'i_aufwand');
         knoten:=Tree.suche_nummer(i_aufwand);
         if knoten<>nil then knoten.StateIndex:=3;
      	datamodul.q_7.Next;
      end;
      knoten:=tree.suche_nummer(nr);
      if knoten<>nil then knoten.StateIndex:=3;
 end;
9:  begin //noch mal lesen    wei main
    //form_untwahl_tbs.Panel_botttom.visible:=true;
  		//query:=format('select * from tabelle_tbs where i_tab_nummer=%s and storno=0',[snr]);
      //datamodul.sql_new(false,datamodul.q_2,query, '');

      datamodul.q_2.first;
      while not datamodul.q_2.eof do
      begin
          i_tbs:=getbigint( datamodul.q_2,'i_tbs');
          knoten:=Tree.suche_nummer(i_tbs);
          if knoten<>nil then
          begin
            knoten.StateIndex:=3;
            daten:=knoten.data;

            list :=tstringlist.Create;
            list.Clear;
            list.Add(datamodul.q_2.findfield('i1').asstring);
            list.Add(datamodul.q_2.findfield('i2').asstring);
            list.Add(datamodul.q_2.findfield('i1w').asstring);
            list.Add(datamodul.q_2.findfield('i2w').asstring);
            list.Add(datamodul.q_2.findfield('s1').asstring);
            daten^.liste:=list;
            daten^.datum:=now();
            knoten.data:=daten;

          end;
      	datamodul.q_2.Next;
      end;
    end;

end;
  //

  knoten:=tree.suche_nummer(nr); //changing nicht erlaubt beri 9 sonst ist die liste weg
  memoladen;
  ToolButton_editClick(Sender);
  
if notebook.pageindex = 9 then
begin
 spinedit_tbs_uw.text:='0';
  spinedit_tbs_ow.text:='0';
  SpinEdit_tbs_oww.text:='0';
  SpinEdit_tbs_uww.text:='0';
  ComboBox_tbs_gi.Text:='';
  anzeigen:=true;
end;

panel1.Enabled:=false;

end;

procedure TForm_untwahl.ToolButton_saveClick(Sender: TObject);
begin
speichern;
formularsperren;
panel1.Enabled:=true;
end;

procedure TForm_untwahl.ToolButton_deleteClick(Sender: TObject);
var
query,query1,query2:string;
nodedata: pnodepointer;
bernum:integer;
begin
if tree.Selected.HasChildren then
begin
	showmessage('Bitte zuerst abh�ngige Knoten l�schen');
	exit;
end;

	nodedata:=tree.Selected.data;
	bernum:=nodedata^.nummer;

case notebook.PageIndex of
2: begin  //typ_tree
		 query1:=format('select nummer from labor where i_typ =%d',[bernum]);
      query2:=format('select nummer from unt_labor where i_typ =%d',[bernum]);
    if  (mysql_d.Feldinhalt(query1,0)<>'') or (mysql_d.Feldinhalt(query2,0)<>'') then
	  begin
		  showmessage('Es existieren bereits Labor-Befunde mit diesem Kriterium, L�schen nicht m�glich');
		  exit;
	  end;
	  tree.loeschen;
	 end;

4: begin   //befunde

	  query:=format('select nummer from akt_untersuchung where i_bereich=%d',[bernum]);
	  if  mysql_d.Feldinhalt(query,0)<>'' then
	  begin
		  showmessage('Es existieren bereits Befunde mit diesem Kriterium, L�schen nicht m�glich');
		  exit;
	  end;
	  query:=format('select nummer from berlist where i_bereich=%d',[bernum]);
	  if  mysql_d.Feldinhalt(query,0)='' then tree.loeschen
		  else showmessage('Die Listeneintr�ge m�ssen zuerst gel�schet werden.');
	end;

else
	if dateiloeschen( tree.mysqlquery,false,false) then
	begin
		dispose(nodedata);
		tree.Selected.Delete;
        //form_main.sperren(notebook);
        formularsperren;
	end;
end;
panel1.Enabled:=true;
end;

procedure TForm_untwahl.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
multiselect:=true;
parent_markieren:=true;
MaskEdit_datum.Text:=datetostr(date());
einzelauswahl:=false;
anzeigen:=true;
//maskedit_labor.EditMask:=stringreplace(maskedit_labor.EditMask,',',decimalseparator,[rfreplaceall]);
//maskedit_befund.EditMask:=stringreplace(maskedit_befund.EditMask,',',decimalseparator,[rfreplaceall]);
//form_main.sperren(notebook);
formularsperren;
end;

procedure TForm_untwahl.TreeDblClick(Sender: TObject);
begin
if tree.items.count=0 then exit;
if einstellung then
begin
  //ToolButton_editClick(Sender);
  exit;
end;
if not (multiselect ) then
begin
   BitBtn_okClick(sender);
   exit;
end;


if einzelauswahl then
begin
	if tree.selected.stateindex=-1 then tree.selected.stateindex:=3
	else tree.selected.stateindex:=-1;
end
else
   if tree.selected.stateindex=-1 then tree.markieren(parent_markieren,3)
   else tree.markieren(parent_markieren,-1);
   
tree.selected.Expand(true);

end;

procedure TForm_untwahl.TreeChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
var
daten:pnodepointer;
f: stringarray ;
knoten:ttreenode;
begin
if not anzeigen then exit;
if notebook.PageIndex=9 then
begin
  knoten:=tree.Selected;
  if (knoten<>nil)   then
  begin
    if  (knoten.StateIndex=3) then
    begin

      if strtofloat(spinedit_tbs_uw.text)>strtofloat(spinedit_tbs_ow.text) then  spinedit_tbs_ow.text:= '99999';
      if strtofloat(spinedit_tbs_uww.text)>strtofloat(spinedit_tbs_oww.text) then  spinedit_tbs_oww.text:= '99999';

      daten:=knoten.data;
      if daten^.liste<>nil then
      begin
        daten^.liste.clear;
        daten^.liste.Free;
      end;
      daten^.liste :=tstringlist.Create;
      daten^.liste.Add((spinedit_tbs_uw.text));
      daten^.liste.Add((spinedit_tbs_ow.text));
      daten^.liste.Add((spinedit_tbs_uww.text));
      daten^.liste.Add((spinedit_tbs_oww.text));
      daten^.liste.Add(ComboBox_tbs_gi.Text);
      knoten.data:=daten;
    end;
  end;
end;

 speichern;
 formularsperren;
end;

procedure TForm_untwahl.SpeedButton3Click(Sender: TObject);
begin
	if form_kalender.showmodal =mrok then
	begin
		maskedit_datum.text :=datetostr(int(form_kalender.auswahlkalender.Date));
	end;
end;

procedure TForm_untwahl.BitBtn_okClick(Sender: TObject);
var
knoten:ttreenode;
daten:pnodepointer;
s1,s2,s3:string;
list:tstringlist;
begin
{if datamodul.q_typ_tree['einheit']<>'group' then modalresult:=mrok
else showmessage('Gruppe');}
if notebook.PageIndex=9 then
begin
knoten:=tree.Selected;
if (knoten<>nil)   then
begin
  if  (knoten.StateIndex=3) then
  begin
    daten:=knoten.data;

    if daten^.liste<>nil then
    begin
       list:=daten^.liste;
       list.Clear;
       list.Free;
    end;

    list :=tstringlist.Create;
   list.Add((spinedit_tbs_uw.text));
    list.Add((spinedit_tbs_ow.text));
    list.Add((spinedit_tbs_uww.text));
    list.Add((spinedit_tbs_oww.text));
    list.Add(ComboBox_tbs_gi.Text);
    daten^.liste:=list;
   knoten.data:=daten;
  end;
end;
end;
speichern;
modalresult:=mrok;
end;

procedure TForm_untwahl.BitBtn3Click(Sender: TObject);
var
nodedata: pnodepointer;
bernum,rf_last:integer;
query:string;
begin
   form_bes_edit:=tform_bes_edit.Create(self);

   nodedata:=tree.Selected.data;
   bernum:=nodedata^.nummer;
   Form_bes_edit.CheckBox.Visible:=false;

   if Form_bes_edit.showmodal=mrok then
   begin

      datamodul.q_berlist.Last;
      if datamodul.q_berlist['nummer']<>null then rf_last:=datamodul.q_berlist['reihenfolge']+1
      else
      rf_last:=1;

      if (neuer_datensatz(datamodul.q_berlist,['nummer','i_bereich','listenfeld','reihenfolge','c_auffaellig']
         ,[null,bernum,form_bes_edit.Edit.text,rf_last,'normal'])=-1) then exit;
     datamodul.q_berlist.post;
     datamodul.q_berlist.refresh;
     datamodul.q_berlist.last;
   end;
   form_bes_edit.Release;
   berlist_filtern;
end;


procedure TForm_untwahl.BitBtn_veraendernClick(Sender: TObject);
begin
form_bes_edit:=tform_bes_edit.Create(self);

Form_bes_edit.CheckBox.Visible:=false;
IF datamodul.q_berlist['listenfeld']<>null then
	Form_bes_edit.Edit.text:=datamodul.q_berlist['listenfeld']
	else  Form_bes_edit.Edit.text:='';
if Form_bes_edit.showmodal=mrok then
	begin
	datamodul.q_berlist.Edit;
	datamodul.q_berlist['listenfeld']:=form_bes_edit.Edit.text;
	datamodul.q_berlist.post;
	end;
form_bes_edit.Release;
end;

procedure TForm_untwahl.BitBtn4Click(Sender: TObject);
begin
			dateiloeschen(datamodul.q_berlist,false,false);
         berlist_filtern;
end;

procedure TForm_untwahl.SpeedButton_upClick(Sender: TObject);
begin
 reihenfolge(0);
end;

procedure TForm_untwahl.SpeedButton_downClick(Sender: TObject);
begin
	reihenfolge(1);
end;

procedure Tform_untwahl.reihenfolge(richtung:integer);
var
reihenfolge1,reihenfolge2:integer;
nummer1:integer;
begin
with datamodul do
begin
	{if datamodul.q_berlist.State=dsedit then
			begin
			q_berlist.Post;
			exit;
		  end;      }
	if (richtung=0) and (q_berlist.Bof) then exit;
	if (richtung=1) and (q_berlist.eof) then exit;
	nummer1:=q_berlist['nummer'];
	reihenfolge1:=q_berlist['reihenfolge'];
	if (richtung=0) then q_berlist.Prior else q_berlist.Next;

	reihenfolge2:=q_berlist['reihenfolge'];
	q_berlist.edit;
	q_berlist['reihenfolge']:=reihenfolge1;
	q_berlist.post;
	//q_berlist.first;
	q_berlist.locate('nummer',nummer1,[]);
	q_berlist.edit;
	q_berlist['reihenfolge']:=reihenfolge2;
	q_berlist.post;
	q_berlist.refresh;
	q_berlist.Locate( 'nummer', nummer1,[]);
end;
end;


procedure TForm_untwahl.berlist_filtern;
var
bernum:integer;
query:string;
begin
  bernum:=datamodul.q_bereich['nummer'];
  query:=format('select * from berlist where i_bereich=%s order by reihenfolge',[inttostr(bernum)]);
  datamodul.sql_new(true,datamodul.q_berlist,query,'');
  datamodul.q_berlist.first;
end;


procedure TForm_untwahl.PageControlChange(Sender: TObject);
begin
//if form_untwahl.tag=0 then exit;
if pagecontrol.ActivePage=tabsheet_2 then
begin
	notebook_befunde.PageIndex:= DBRadioGroup_erscheinungsbild.ItemIndex;
	berlist_filtern;
end;
end;

procedure TForm_untwahl.MaskEdit_laborExit(Sender: TObject);
var r:real;
	  s:string;
begin
	 s:=stringreplace(tmaskedit(sender).text,' ','',[rfReplaceAll]);
	 s:=stringreplace(s,',','',[rfReplaceAll]);
	 s:=stringreplace(s,'.','',[rfReplaceAll]);
	 datamodul.q_typ_tree.edit;
	 datamodul.q_typ_tree['eurocent']:=strtoint(s );

	 r:=datamodul.q_typ_tree['eurocent'] /100;
	 tmaskedit(sender).EditText:=floattostrf(r,ffFixed,9,2);

end;

procedure TForm_untwahl.DBEdit_laborChange(Sender: TObject);
var r:real;
begin
	 //if datamodul.q_typ_tree['eurocent']<>null then r:=datamodul.q_typ_tree['eurocent'] /100
	 //else r:=0;
	 //maskedit_labor.EditText:=floattostrf(r,ffFixed,9,2);
end;

procedure TForm_untwahl.DBEdit_nameChange(Sender: TObject);
	var
	 r:real;
begin
	 //if datamodul.q_bereich['eurocent']<>null then  r:=datamodul.q_bereich['eurocent'] /100
	 //else r:=0;
	 //maskedit_befund.editText:=floattostrf(r,ffFixed,9,2);
end;

procedure TForm_untwahl.MaskEdit_befundExit(Sender: TObject);
var r:real;
	  s:string;
begin
	 s:=stringreplace(tmaskedit(sender).text,' ','',[rfReplaceAll]);
	 s:=stringreplace(s,',','',[rfReplaceAll]);
	 s:=stringreplace(s,'.','',[rfReplaceAll]);
	 datamodul.q_bereich.edit;
	 datamodul.q_bereich['eurocent']:=strtoint(s );

	 r:=datamodul.q_bereich['eurocent'] /100;
	 tmaskedit(sender).EditText:=floattostrf(r,ffFixed,9,2);

end;

procedure TForm_untwahl.RadioButton1Click(Sender: TObject);
begin
	datamodul.q_bereich.Edit;
	if radiobutton1.Checked then
	begin
	  dbedit_goae_bef.Enabled:=true;
	  dbedit_euro_bef.Enabled:=false;
	  datamodul.q_bereich['euro']:=0;

	end
	else
	begin
	  dbedit_goae_bef.Enabled:=false;
	  dbedit_euro_bef.Enabled:=true;
	  datamodul.q_bereich['goae']:='';

	end

end;

procedure TForm_untwahl.RadioButtonl1Click(Sender: TObject);
begin
	datamodul.q_typ_tree.Edit;
if radiobuttonl1.Checked then
	begin
	  dbedit_goae_lab.Enabled:=true;
	  dbedit_euro_lab.Enabled:=false;
	  datamodul.q_typ_tree['euro']:=0;

	end
	else
	begin
	  dbedit_goae_lab.Enabled:=false;
	  dbedit_euro_lab.Enabled:=true;
	  datamodul.q_typ_tree['goae']:='';

	end;
end;

procedure tform_untwahl.memoladen;
begin
if notebook.PageIndex=4 then
	begin
			richeditladen(datamodul.q_bereich,'memo',richedit_befunde);
	end;
end;

procedure TForm_untwahl.FormDestroy(Sender: TObject);
begin

form_main.combodeaktivieren(ComboBox_untersucher);
tree.liste_loeschen;
end;

procedure TForm_untwahl.button_laborClick(Sender: TObject);
var
query,goae:string;
i_typ:integer;
begin
  i_typ:=datamodul.q_typ_tree['nummer'];
  //go�
  if datamodul.q_typ_tree['goae']<>null then goae:=datamodul.q_typ_tree['goae'] else goae:='';
  query:=format(' update labor set goae="%s" where i_typ =%d',[goae,i_typ]);
  mysql_d.sql_exe(query);
  //euro
  if datamodul.q_typ_tree['euro']<>null then goae:=datamodul.q_typ_tree['euro'] else goae:='';
  goae:=sql_string_float(goae);
  query:=format(' update labor set euro="%s" where i_typ =%d',[goae,i_typ]);
  mysql_d.sql_exe(query);

  datamodul.q_labor.refresh;
end;

procedure TForm_untwahl.Button_goaeClick(Sender: TObject);
var
query,goae:string;
i_typ:integer;
begin
  i_typ:=datamodul.q_bereich['nummer'];
  if datamodul.q_bereich['goae']<>null then goae:=datamodul.q_bereich['goae'] else goae:='0';
  query:=format(' update akt_untersuchung set goae="%s" where i_bereich =%d',[goae,i_typ]);
  mysql_d.sql_exe(query);

  if datamodul.q_bereich['euro']<>null then goae:=datamodul.q_bereich['euro'] else goae:='0';
  goae:=sql_string_float(goae);
  query:=format(' update akt_untersuchung set euro="%s" where i_bereich =%d',[goae,i_typ]);
  mysql_d.sql_exe(query);
  datamodul.q_akt_untersuchung.refresh;

end;

procedure TForm_untwahl.Edit_suchEnter(Sender: TObject);
begin
if tree.items.count=0 then exit;
//tree.Selected:=tree.Items[0];
//tree.change(tree.selected);
end;

procedure TForm_untwahl.Edit_suchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key<>vk_return then exit;
     tree.suche_datensatz(edit_such.text);
end;

procedure TForm_untwahl.SpeedButton4Click(Sender: TObject);
begin
tree.suche_datensatz(edit_such.text);
end;

procedure TForm_untwahl.ini(bereich:integer;datum:tdate);
begin
form_untwahl.tag:=1;
	datamodul.sql_new(true,datamodul.q_typ_tree,format('select * from typ where bereich = %d',[bereich]), 'reihenfolge');
	form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.Tree.bereich_wert:=bereich;
	form_untwahl.tree.mysqlquery:=datamodul.q_typ_tree;
	form_untwahl.tree.mysql_feld:='untersuchung';
	form_untwahl.notebook.visible:=true;
	form_untwahl.notebook.pageindex:=3;
	form_untwahl.tree.liste_lesen;
	form_untwahl.tree.DragMode:=dmmanual;
	form_untwahl.multiselect:=true;
    
	form_untwahl.MaskEdit_datum.Text:=datetostr(datum);
	form_main.combolesen( {form_untwahl.}ComboBox_untersucher,'untersucher','untersucher','untersucher','');
	form_untwahl.ToolBar.Visible:=false;
	form_untwahl.Panel_botttom.Visible:=true;
    //form_main.entsperren(notebook);
    formularentsperren;
	case bereich of
		1: {form_untwahl.}caption:='Bescheinigung';
		2: {form_untwahl.}caption:='Impfung';
		3: {form_untwahl.}caption:='Labor';
	end;

end;

procedure TForm_untwahl.DBGrid1ColExit(Sender: TObject);
begin
if datamodul.q_berlist.state in [dsedit, dsinsert] then  datamodul.q_berlist.post;
end;

procedure TForm_untwahl.DBGrid1EditButtonClick(Sender: TObject);
begin
	 datamodul.q_berlist.edit;
end;

procedure TForm_untwahl.DBGrid1Exit(Sender: TObject);
begin
if datamodul.q_berlist.state in [dsedit, dsinsert] then  datamodul.q_berlist.post;
end;

procedure TForm_untwahl.FormClose(Sender: TObject;
	var Action: TCloseAction);
begin
//speichern;   ###090807
//form_main.combodeaktivieren( ComboBox_untersucher);
 form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
 form_richedit_menue.Parent:=nil;
end;

procedure TForm_untwahl.BitBtn_abbClick(Sender: TObject);
begin
  speichern;
  modalresult:=mrabort;
end;

procedure TForm_untwahl.DBEdit_zeitansatzChange(Sender: TObject);
var i:integer;
begin
{try
i:= strtoint(DBEdit_zeitansatz.Text);
except
DBEdit_zeitansatz.Text:='100';
end;}
end;

procedure TForm_untwahl.DBEdit_zeitansatzKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
t:string;
begin
if ( key in [58..126])  then
begin
 DBEdit_zeitansatz.Text:='100';
end;

if key in [48..57] then
begin
  t:=trim(DBEdit_zeitansatz.Text);
  if length(t)>3
  then DBEdit_zeitansatz.Text:='';//chr(key);
end;
end;

procedure TForm_untwahl.BitBtn_taetigkeitenClick(Sender: TObject);
var
snr: string;
begin
    BitBtn_taetigkeiten.Enabled:=false;
   snr:=datamodul.q_3.findfield('nummer').asstring;
   form_main.tateigkeit_auswahl('einstellen',snr );
   datamodul.q_9.Refresh;
   if datensatz(datamodul.q_9) then GroupBox_taetigkeiten.Visible:=true else GroupBox_taetigkeiten.Visible:=false;
   BitBtn_taetigkeiten.Enabled:=true;

end;

procedure TForm_untwahl.SpeedButton7Click(Sender: TObject);
begin
tree.FullCollapse;

end;

procedure TForm_untwahl.SpeedButton5Click(Sender: TObject);
begin
tree.FullExpand ;
end;

procedure TForm_untwahl.m_alleitemsClick(Sender: TObject);
var
knoten,letzter_knoten:ttreenode;

procedure knotenmarkieren;
begin
if knoten.stateindex=-1 then knoten.stateindex:=3
    else knoten.stateindex:=-1;
end;
//###############################################
begin
if tree.Selected.HasChildren then
begin
   knoten:=tree.selected.getFirstChild;
   letzter_knoten:=tree.selected.GetLastChild;

   while knoten<>letzter_knoten do
   begin
    knotenmarkieren ;
    knoten:=knoten.getNext;
  end;
    knotenmarkieren ;

end;

end;

procedure TForm_untwahl.RichEdit_befundeEnter(Sender: TObject);
begin
rich_akt:=sender as trichedit;
end;

procedure TForm_untwahl.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 //form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
 //form_richedit_menue.Parent:=nil;
end;

procedure TForm_untwahl.BitBtn_laborhinweiseClick(Sender: TObject);
var
snr:string;
begin
speichern;
formularsperren;

    snr:=DataModul.q_typ_tree.findfield('nummer').asstring;
    form_main.tbs_auswahl('einstellen',3,snr,'' );
    datamodul.q_hinweis.Refresh;
    if datensatz(datamodul.q_hinweis) then GroupBox_labor_hinweis.Visible:=true else  GroupBox_labor_hinweis.Visible:=false;
end;

procedure TForm_untwahl.DBEdit_tbs_untere_grenzeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i:integer;
begin
try

  //i:=strtoint(text);
  //if i<0 then text:='0';
  //if i>100 then text:='100';
except
  text:='0';
end;
end;

procedure TForm_untwahl.SpinEdit_tbs_uw1Exit(Sender: TObject);
begin
if  strtofloat(SpinEdit_tbs_uw.text)>0 then tree.selected.stateindex:=3;
end;

procedure TForm_untwahl.SpinEdit_tbs_ow1Exit(Sender: TObject);
begin
  if  strtofloat(SpinEdit_tbs_ow.text)>0 then tree.selected.stateindex:=3;
end;

procedure TForm_untwahl.ComboBox_tbs_giExit(Sender: TObject);
begin
  if  comboBox_tbs_gi.Text<>'' then tree.selected.stateindex:=3;
end;

procedure TForm_untwahl.SpinEdit_tbs_uww1Exit(Sender: TObject);
begin
if  strtofloat(SpinEdit_tbs_uww.text)>0 then tree.selected.stateindex:=3;
end;

procedure TForm_untwahl.SpinEdit_tbs_oww1Exit(Sender: TObject);
begin
  if  strtofloat(SpinEdit_tbs_oww.text)>0 then tree.selected.stateindex:=3;

end;

procedure TForm_untwahl.BitBtn_uebernehmenClick(Sender: TObject);
begin
  spinedit_tbs_uww.text:= spinedit_tbs_uw.text;
  spinedit_tbs_oww.text:=spinedit_tbs_ow.text;
end;

procedure TForm_untwahl.PageControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
speichern;
formularsperren;
end;

procedure TForm_untwahl.spinedit_tbs_uwKeyPress(Sender: TObject;
  var Key: Char);
begin

  if key=',' then key:=DecimalSeparator;
  if key='.' then key:=DecimalSeparator;
  if (key=decimalseparator) and (pos(decimalseparator,tedit(sender).text)>0) then key:=#0;
  if not (Key in ['0'..'9', DecimalSeparator, Char(VK_BACK)]) then
  Key := #0;
end;

procedure TForm_untwahl.spinedit_tbs_uwChange(Sender: TObject);
var
  Error: Integer;
  Dummy: double	 ;

begin
  {val(Trim(tedit(sender).Text), Dummy, Error);
  if Error <> 0 then
    tedit(sender).Color:=clRed
  else
    tedit(sender).Color:=clWindow;  }
end;

procedure TForm_untwahl.DBCombo_num_abrechnungskriteriumEnter(
  Sender: TObject);
var
query,snr,nummern:string;
begin
//if pagecontrol_firma.ActivePage<>tabsheet_sollist then exit;
nummern:='';
 snr:=datamodul.q_3.findfield('i_zeitkonto').AsString;
 query:=format ('select i_kriterium from abr_konto_kriterium where i_konto=%s and storno=0 ',[snr]);
 datamodul.sql(false,datamodul.q_1,query,'');
 datamodul.q_1.First;
 while not datamodul.q_1.eof do
  begin
    nummern:=nummern+','+datamodul.q_1.findfield('i_kriterium').AsString;
    datamodul.q_1.Next;
  end;
  nummern:=copy(nummern,2,length(nummern)-1);
 if (snr<>'') and (nummern<>'') then
  begin
    query:=format(' nummer in (%s) ',[nummern]);
    DBCombo_num_abrechnungskriterium.zwhere:=query;
    DBCombo_num_abrechnungskriterium.lookup_laden;
  end;

end;

procedure TForm_untwahl.DBCombo_num_abrechnungskriteriumExit(
  Sender: TObject);
begin
    DBCombo_num_abrechnungskriterium.zwhere:='';
    DBCombo_num_abrechnungskriterium.lookup_laden;
end;

end.
