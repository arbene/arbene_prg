unit anonym;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls,  ZQuery, ZMySqlQuery, Db, ZTransact, ZMySqlTr, ZConnect,
  ZMySqlCon;

type
  TForm1 = class(TForm)
    Button1: TButton;
    db: TZMySqlDatabase;
    table: TZMySqlQuery;
	  ZMySqlTransact1: TZMySqlTransact;
    t: TZMySqlQuery;
    Label1: TLabel;
    Button2: TButton;
	  termine: TZMySqlQuery;
    q: TZMySqlQuery;
    procedure Button1Click(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure weiter;
    procedure Button2Click(Sender: TObject);
  private
	  { Private-Deklarationen }
	  function getbigint_str(tabelle: tzdataset;feldname:string):string;

  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;
implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
max,rnd:integer;
begin
table.first;
while not table.eof do
begin
label1.caption:=table['name'];
application.ProcessMessages;
table.edit;

weiter;

if t['name']<>null then table['name']:=t['name'];
weiter;
if t['vorname']<>null then table['vorname']:=t['vorname'];
weiter;
if t['geb_dat']<>null then table['geb_dat']:=t['geb_dat'];
weiter;
if t['strasse']<>null then table['strasse']:=t['strasse'];
table['p_nummer']:='';
table['rv_nummer']:='';
table['telefon_p']:='';
if table['email_f']<>'' then table['email_f']:=table['name']+'@firma.de';
table.post;
table.next;
end;

end;

procedure tform1.weiter;
var rnd, i:integer;
begin
rnd:=random(200);
for i:=1 to rnd do
begin
 t.next;
 if t.eof then t.First;
end;

end;


procedure TForm1.FormCreate(Sender: TObject);
begin
	db.Connect;
	table.Open;
	termine.open;
	t.open;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
max,rnd,p:integer;
nr,info:string;
begin
termine.first;
while not termine.eof do
begin
nr:=getbigint_str( termine,'i_mitarbeiter');
table.Locate('nummer',nr,[]);

termine.edit;
termine['name']:=table['name'];
termine['vorname']:=table['vorname'];
info:=termine['info'];
p:=pos('/',info);
termine['info']:=termine['name']+copy(info,p,length(info));
termine.post;
termine.next;
end;


end;

function tform1.getbigint_str(tabelle: tzdataset;feldname:string):string;
var
wert:int64;
begin
wert:=0;
try
if tabelle.findfield(feldname)<>nil then
      	tabelle.GetFieldData(tabelle.fieldbyname(feldname),@wert);
except
end;
result:=inttostr(wert);
end;

end.
