unit a_textbausteine;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ComCtrls, TreeView_ext, ExtCtrls, Buttons, ToolWin,
  Mask,db,olectnrs, dbcombo_number, variants;

type
  TForm_textbausteine = class(TForm)
    ToolBar_main: TToolBar;
    ToolButton21: TToolButton;
    ToolButton_neu: TToolButton;
    ToolButton9: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton43: TToolButton;
    ToolButton_sichern: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_delete: TToolButton;
    ToolButton2: TToolButton;
    ToolButton1: TToolButton;
    Panel_bottom: TPanel;
    BitBtn_insert: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel_re: TPanel;
    GroupBox_kapitel: TGroupBox;
    TreeView: TTreeView_ext;
    DBMemo: TDBMemo;
    GroupBox1: TGroupBox;
    DBEdit_titel: TDBEdit;
    ComboBox_thema: TComboBox;
    GroupBox_thema: TGroupBox;
    DBCombo_num_thema: TDBCombo_num;
    GroupBox_tbs_such: TGroupBox;
    CheckBox_alpha: TCheckBox;
    Edit_text_such: TEdit;
    SpeedButton_text_such: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_insertClick(Sender: TObject);
    procedure ToolButton_neuClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton_sichernClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton_deleteClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
    procedure DBCombo_num_kapitelChange(Sender: TObject);
    procedure ComboBox_themaChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Edit_text_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton_text_suchClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
	  { Private-Deklarationen }
  public

    { Public-Deklarationen }
    text:string;
    i_kapitel,modus:integer;
    procedure filtern;

  end;

var
  Form_textbausteine: TForm_textbausteine;

implementation

uses a_main, a_data, a_line_edit;

{$R *.DFM}

procedure TForm_textbausteine.FormCreate(Sender: TObject);
begin

  form_main.form_positionieren(tform(sender));
  DBCombo_num_thema.lookup_laden;

  form_main.combolesen( combobox_thema,'kapitel','kapitel','kapitel','alle');
  if combobox_thema.items.count>=0 then combobox_thema.itemindex:=0;

  //if dbcombo_num_kapitel.items.count>=0 then dbcombo_num_kapitel.itemindex:=0;

  filtern;
  form_main.sperren(panel_re);
  toolbutton_delete.Enabled:=false;
  toolbutton_sichern.enabled:=false;
  toolbutton_edit.Enabled:=true;
  toolbutton_neu.enabled:=true;
  modus:=0;
end;

procedure TForm_textbausteine.BitBtn_insertClick(Sender: TObject);
begin
//
  text:=dbmemo.Text;
  modalresult:=mrok;
end;

procedure TForm_textbausteine.filtern;
var
query,kapitel,rf:string;
nr:int64;
begin
	 if datamodul.q_6.State in [dsinsert, dsedit] then  datamodul.q_6.post;
	 if datensatz(datamodul.q_6) then nr:=getbigint(datamodul.q_6,'nummer') else nr:=0;
	 kapitel:=combobox_thema.Text;
   if checkbox_alpha.Checked then rf:= 'name'
   else rf:= 'reihenfolge';

   case i_kapitel of
   -1:  query:='select * from textbausteine where i_kapitel=-1 order by '+rf;
   else
     begin
       if ((kapitel<>'alle') and (kapitel<>'')) then
        begin
          query:=format('select nummer from kapitel where kapitel="%s"',[kapitel]);
          i_kapitel:=strtoint(mysql_d.Feldinhalt(query,0));
          query:=format('select * from textbausteine where i_kapitel=%d order by %s ',[i_kapitel,rf]);
        end
       else
         begin
          i_kapitel:=0;
          query:='select * from textbausteine where i_kapitel>-1 order by '+rf;
         end;
         end;
   end;


	 datamodul.sql_new(true,datamodul.q_6,query,'');
    if checkbox_alpha.Checked then treeview.liste_lesen_alphabetisch else treeview.liste_lesen;
	 if treeview.suche_nummer(nr)=nil then if treeview.Items.count>0 then treeview.Selected:=treeview.Items[0];   //not  treeview.suche_nummer(nr) 
end;

procedure TForm_textbausteine.ToolButton_neuClick(Sender: TObject);
var
rf:integer;
nr: int64;
pa:string;
begin
		form_line_edit:=tform_line_edit.create(self);
		//form_line_edit.helpcontext:=2500;
		form_line_edit.CheckBox.visible:=false;

		form_line_edit.Caption:='Name f�r Textbaustein';
		form_line_edit.showmodal;
		if form_line_edit.Edit.text='' then exit;


     if datensatz(datamodul.q_6) then
     begin
       rf:=datamodul.q_6.findfield('reihenfolge').asinteger;
       pa:=datamodul.q_6.findfield('parent').asstring;
     end
     else
     begin
		  rf:=0;
		  pa:='0';
     end;

		nr:=neuer_datensatz(datamodul.q_6,['nummer','name','i_kapitel','tbs','reihenfolge','parent']
		,[null,form_line_edit.Edit.text,i_kapitel,'',rf,pa]) ;
		datamodul.q_6.post;
		//nr:=datamodul.q_6.findfield('nummer').asinteger;
		form_line_edit.Release;
		//treeview.reihenfolge_neu;
		//filtern;
		datamodul.q_6.tag:=1;
		treeview.liste_lesen;
		treeview.reihenfolge_neu;
		treeview.suche_nummer(nr);
		datamodul.q_6.tag:=0;

		toolbutton_delete.enabled:=false;
        toolbutton_edit.Enabled:=false;
        toolbutton_sichern.Enabled:=true;
        toolbutton_neu.enabled:=false;
		datamodul.q_6.edit;
		form_main.entsperren(panel_re);
		dbmemo.SetFocus;
end;

procedure TForm_textbausteine.ToolButton1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_textbausteine.ToolButton_sichernClick(Sender: TObject);
begin

		if datamodul.q_6.State in [dsinsert, dsedit] then  datamodul.q_6.post;
		filtern;
		form_main.sperren(panel_re);

        toolbutton_delete.Enabled:=false;
    	toolbutton_sichern.enabled:=false;
        toolbutton_edit.Enabled:=true;
        toolbutton_neu.enabled:=true;
end;

procedure TForm_textbausteine.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 if datamodul.q_6.State in [dsinsert, dsedit] then  datamodul.q_6.post;
 form_main.combodeaktivieren( combobox_thema);
end;

procedure TForm_textbausteine.ToolButton_deleteClick(Sender: TObject);
begin
	dateiloeschen(datamodul.q_6, false,false);
	filtern;
    toolbutton_delete.Enabled:=false;
    toolbutton_sichern.enabled:=false;
    toolbutton_edit.Enabled:=true;
    toolbutton_neu.enabled:=true;
end;

procedure TForm_textbausteine.ToolButton_editClick(Sender: TObject);
begin
	form_main.entsperren(panel_re);
    toolbutton_delete.Enabled:=true;
    toolbutton_sichern.enabled:=true;
    toolbutton_edit.Enabled:=false;
    toolbutton_neu.enabled:=false;
	dbmemo.SetFocus;
end;

procedure TForm_textbausteine.TreeViewDblClick(Sender: TObject);
begin
 BitBtn_insertClick(Sender);
end;

procedure TForm_textbausteine.DBCombo_num_kapitelChange(Sender: TObject);
begin
		filtern;
end;

procedure TForm_textbausteine.ComboBox_themaChange(Sender: TObject);
begin
	filtern;
end;

procedure TForm_textbausteine.BitBtn2Click(Sender: TObject);
begin
text:='';
end;

procedure TForm_textbausteine.Edit_text_suchKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=vk_return then SpeedButton_text_suchClick(sender);
end;

procedure TForm_textbausteine.SpeedButton_text_suchClick(Sender: TObject);
begin
treeview.suche_datensatz(edit_text_such.text);
end;

procedure TForm_textbausteine.FormDestroy(Sender: TObject);
begin
treeview.liste_loeschen;
end;

end.
