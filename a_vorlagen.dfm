�
 TFORM_VORLAGEN 0h'  TPF0TForm_vorlagenForm_vorlagenLeftFTop� Width)Height�HelpContext BorderIconsbiSystemMenu CaptionWord - VorlagenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight TToolBarToolBar_mainLeft Top WidthHeight(AutoSize	ButtonHeight$ButtonWidth7CaptionToolBarColor	clBtnFaceImagesForm_main.ImageList_mainParentColorShowCaptions	TabOrder  TToolButtonToolButton21Left TopWidthCaptionToolButton21
ImageIndexStyletbsSeparator  TToolButtonToolButton_neuLeftTopHintDatensatz neu anlegenCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonToolButton9Left;TopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeftCTopCaption   Ändern
ImageIndexOnClickToolButton_editClick  TToolButtonToolButton43LeftzTopWidthCaptionToolButton43
ImageIndexStyletbsSeparator  TToolButtonToolButton_sichernLeft� TopHintDatensatz speichernCaption	Speichern
ImageIndexParentShowHintShowHint	OnClickToolButton_sichernClick  TToolButtonToolButton7Left� TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeft� TopHint   Datensatz löschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_deleteClick  TToolButtonToolButton2Left� TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton1LeftTopCaption	Verlassen
ImageIndexOnClickToolButton1Click   TPanelPanel_bottomLeft Top�WidthHeight#AlignalBottomTabOrder TBitBtn	BitBtn_okLeft� TopWidth� HeightCaptionOKModalResultTabOrder OnClickBitBtn_okClick
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn2LeftTopWidth� HeightTabOrderKindbkCancel   TTabControlTabControl1Left Top(WidthHeightAlignalTopDockSite	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderTabs.StringsWord-Vorlage      Datei       	Anwendung TabIndex OnChangeTabControl1Change  	TNotebook	Notebook1Left TopAWidthHeightEHelpContext AlignalClientTabOrder TPage Left Top Captionword TPanelPanel_liLeft Top WidthFHeightEAlignalLeftCaptionPanel_liTabOrder  	TGroupBoxGroupBox_typLeftTopWidthDHeight4AlignalTopCaptionVorlagen TypTabOrder  	TComboBoxComboBox_typLeftTopWidth$HeightStylecsDropDownListDropDownCount
ItemHeightTabOrder OnChangeComboBox_typChangeOnClickComboBox_typClickOnSelectComboBox_typSelectItems.StringsDokumente FirmaDokumente ProbandBescheinigung Vorsorge FirmaBescheinigung Vorsorge Proband(Bescheinigung Eignungsuntersuchung Firma*Bescheinigung Eignungsuntersuchung Proband"Einladung zu Untersuchungsterminen Labormitteilung an den Probanden-   Übersicht Untersuchunen, Impfungen, AmbulanzUnfallanzeigeUnfallanzeige - KurzmeldungPersonalienRechnung FirmaRechnung Proband-   Dokument in Abteilung/Gefährdungsbeurteilung    TRadioGroupRadioGroup_allgemeinLeftTopbWidthDHeight!AlignalTopColumns	ItemIndex Items.Stringsalle Firmenaktuelle Firma TabOrderOnClickRadioGroup_allgemeinClick  TTreeView_extTreeViewLeftTop� WidthDHeight�AlignalClientDragModedmAutomaticFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style HideSelectionHotTrack	Indent
ParentFont	PopupMenuPopupMenu_oleReadOnly		RowSelect	StateImagesForm_main.ImageList_bereichTabOrderOnChangeTreeViewChange
OnChangingTreeViewChanging
OnDblClickTreeViewDblClickbereich_wert bereich_wert2 
mysqlqueryDataModul.q_vorlagen
mysql_feldnamemysqldatasourceDataModul.ds_vorlagen  	TGroupBoxGroupBox_tbs_suchLeftTop5WidthDHeight-AlignalTopCaptionSortieren / SuchenTabOrder TSpeedButtonSpeedButton_text_suchTagLeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsOnClickSpeedButton_text_suchClick  	TCheckBoxCheckBox_alphaLeft	TopWidthwHeightCaptionalphabetisch sortiertTabOrder OnClickCheckBox_alphaClick  TEditEdit_text_suchTagLeft� TopWidth� HeightTabOrder	OnKeyDownEdit_text_suchKeyDown    TPanelPanel_reLeftFTop Width�HeightEAlignalClientTabOrder 
TScrollBox
ScrollBox1LeftTop� Width�Height�AlignalClientTabOrder  TOLeContainerextArbeitsmedizin_VorlagenLeft Top WidthWHeight AllowInPlaceAllowActiveDocAutoActivateaaManualAutoVerbMenuCaptionArbeitsmedizin_Vorlagen
CopyOnSaveCtl3DParentCtl3D	PopupMenuPopupMenu_oleTabOrder OnLButtonDown"Arbeitsmedizin_VorlagenLButtonDown   	TNotebookNotebook_beschreibungLeftTopWidth�Height� AlignalTopTabOrder TPage Left Top Caption
definition 	TGroupBoxGroupBox_defLeft Top Width�Height� AlignalClientTabOrder  TLabelLabel1LeftTopWidthHeightCaptionTitel  TLabelLabel_kapitelLeftTopBWidth!HeightCaptionThema  TDBEditDBEdit_firma_text_titelLeft0TopWidthHeight	DataFieldName
DataSourceDataModul.ds_vorlagenTabOrder   TDBCheckBoxDBCheckBox_ambLeftHTopWidth� HeightCaption   Sichtbar für Ambulanz	DataField
b_ambulanz
DataSourceDataModul.ds_vorlagenTabOrderValueChecked1ValueUnchecked0  TDBCombo_numDBCombo_num_kapitelLeft/Top=WidthHeightalle_einfuegenkeines_einfuegen	DataField	i_kapitel
DataSourceDataModul.ds_vorlagenzconnectionDataModul.connection_main	ztab_namekapitelzfield_textkapitelzfield_indexnummerzorderkapitelzstornoDropDownCount
ItemHeightTabOrder    TPage Left Top Captionwahl 	TGroupBoxGroupBox_wahlLeft Top Width�Height� AlignalClientCaption   Vorgaben gewähltes DokumentTabOrder  TLabelLabel6LeftTopFWidthHeightCaptionDatum  TSpeedButtonSpeedButton_datTagLeft� Top@WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_datClick  TLabelLabel5Left(TopWidthHeightCaptionTitel  TLabelLabel7LeftTop,Width!HeightCaptionThema  TLabelLabel8LeftTop[Width&HeightCaptionStempel  TEdit
Edit_datumLeftJTopBWidthbHeightReadOnly	TabOrder  	TComboBoxComboBox_thema_wahlTagLeftJTop*WidthHeight
ItemHeightTabOrder  	TComboBoxComboBox_unterschrift_wahlTagLeftJTopYWidthHeight
ItemHeightTabOrder  TEditEdit_titel_wahlTagLeftJTopWidthHeightTabOrder   	TCheckBoxCheckBox_speichernLeftJTopoWidth� HeightCaptionDokument wird nicht gespeichertTabOrderVisible       TPage Left Top Captiondat TLabelLabel3Left;TophWidth)HeightCaption
Dateititel  TLabelLabel4Left<Top� Width3HeightCaption	Dateiname  TLabelLabel_laengeLeft)Top|WidthHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TEdit	dokutitelLeft;TopxWidth�HeightTabOrder OnChangedokutitelChange  TBitBtnBitBtn_datwahlLeft4Top'WidthiHeight(Caption   Datei wählenTabOrderOnClickBitBtn_datwahlClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU_�����UU     UUUwwwww_UP����UWu�UUUu�P𸸸��UW�_UUUW_P�����W�u����P��    W�WwwwwuP�����UUW�UUUW�UP�����UUW�UU_�UUP��� UUW_UUwuUUU��UUUUUu��UUUUUp UUUUUWwuUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU	NumGlyphs  TEditEdit_datnameLeft;Top� Width�HeightReadOnly	TabOrder   TPage Left Top Captionobjekt TLabelLabel2LeftTop� Width�HeightCaptionX   nach "OK" öffnet sich ein Dialog zur Auswahl des Dokumententyps z.B. Adobe Acrobat etc.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont    TTimerTimer1EnabledOnTimerTimer1TimerLeft(Top  TOpenDialog
OpenDialogOnShowOpenDialogShow
DefaultExt*.pdfFilter(PDF|*.pdf|doc|*.doc|docx|*.docx|alle|*.*OptionsofHideReadOnlyofEnableIncludeNotifyofEnableSizing OnSelectionChangeOpenDialogSelectionChangeLeftxTop  
TPopupMenuPopupMenu_oleLeft�Top 	TMenuItemInfozumDokument1CaptionInfo zum DokumentOnClickInfozumDokument1Click   TTimer
Timer_openEnabledLeft Top   