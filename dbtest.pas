unit dbtest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZConnection, DB, Grids, DBGrids, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, StdCtrls, ZAbstractConnection, ZSqlProcessor,
  ZSqlUpdate;

type
  TForm1 = class(TForm)
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Button1: TButton;
    Button2: TButton;
    ZUpdateSQL1: TZUpdateSQL;
    ZSQLProcessor1: TZSQLProcessor;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
zconnection1.Disconnect;
zconnection1.Connect;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
zquery1.Close;
zquery1.Open;
end;

end.
