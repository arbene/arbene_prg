unit a_f_wahl;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, ExtCtrls, ComCtrls,dbgrids, TreeView_ext,listen;

type
	TForm_f_wahl = class(TForm)
	  Panel_bottom: TPanel;
	  BitBtn1: TBitBtn;
	  BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Tree: TTreeView_ext;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label99: TLabel;
    Label110: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    Label8: TLabel;
    SpeedButton11: TSpeedButton;
    Edit_kuerzel: TEdit;
    Edit_plz2: TEdit;
    Edit_plz1: TEdit;
    Edit_ort: TEdit;
    ComboBox_branche: TComboBox;
    ComboBox_gruppe: TComboBox;
    ComboBox_bg: TComboBox;
    ComboBox_ga: TComboBox;
    Edit_name: TEdit;
    Panel1: TPanel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label9: TLabel;
    Label10: TLabel;
    Panel2: TPanel;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox3: TGroupBox;
    CheckBox_rzeigen: TCheckBox;
    CheckBox_rfiltern: TCheckBox;
    Label13: TLabel;
    CheckBox_uebereinstimmung: TCheckBox;
	  procedure FormCreate(Sender: TObject);
	  procedure FormDestroy(Sender: TObject);
    procedure Edit_kuerzelChange(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure TreeDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	private
	  { Private-Deklarationen }
      liste1:tstringlist;
      liste2:tstringlist;
      liste3:tstringlist;
      liste4:tstringlist;
      fnummer:integer;
	public
	  { Public-Deklarationen }
	  firmen_nummern:string;
	  f_nummer:integer;
	  procedure filtern;
	end;

var
	Form_f_wahl: TForm_f_wahl;

implementation

uses a_main, a_data, a_listwahl;

{$R *.DFM}

procedure TForm_f_wahl.FormCreate(Sender: TObject);
var
query:string;
begin
with datamodul do
begin
   form_main.form_positionieren(tform(sender));
   liste1:=tstringlist.create;
   liste2:=tstringlist.create;
   liste3:=tstringlist.create;
   liste4:=tstringlist.create;
   tree.liste_lesen;
   query:='select nummer,firma from firma where nummer in ('+firmen_berechtigung+')';
	  sql(true,q_5,query,'firma');
	  form_main.combolesen(combobox_bg,'berufsgenossenschaft','bg_name','bg_name','alle');
	  form_main.combolesen(combobox_ga,'gewerbeaufsicht','ge_name','ge_name','alle');
	  form_main.combolesen(combobox_branche,'branche','branche','branche','alle');
	  form_main.combolesen(combobox_gruppe,'f_gruppe','f_gruppe','f_gruppe','alle');
    fnummer:=-1;
	  //filtern;
end;
end;


procedure TForm_f_wahl.FormDestroy(Sender: TObject);
begin
    liste1.free;
    liste2.free;
    liste3.free;
    liste4.free;
		form_main.combodeaktivieren(combobox_bg);
	  form_main.combodeaktivieren(combobox_ga);
	  form_main.combodeaktivieren(combobox_branche);
	  form_main.combodeaktivieren(combobox_gruppe);
end;

procedure tform_f_wahl.filtern;
var
    i,e,n:integer;
	query,s_nummer,und_oder:string;
	c_wert,f_nummer:int64;
    knoten: ttreenode;
	daten:pnodepointer;
    kw1,kw2:boolean;
begin
kw1:=true;
kw2:=true;
n:=0;
with datamodul do
begin
	edit_name.Text:=trim(edit_name.text);
	edit_kuerzel.text:=trim(edit_kuerzel.text);
	edit_ort.text:=trim(edit_ort.text);
	edit_plz1.text:=trim(edit_plz1.text);
	edit_plz2.text:=trim(edit_plz2.text);
  query:='';
  if checkbox_uebereinstimmung.checked then
  begin
    if ( edit_name.text<>'') then query :=query+ ' firma = "'+edit_name.text+'" and';
    if edit_kuerzel.text<>'' then query :=query+ ' kuerzel = "'+edit_kuerzel.text+'" and';
    if edit_ort.text<>'' then query :=query+ ' ort = "'+edit_ort.text+'" and';
  end  
  else
  begin
    //query:='select nummer, firma from firma where ';
    if ( edit_name.text<>'') then query :=query+ ' firma like "%'+edit_name.text+'%" and';
    if edit_kuerzel.text<>'' then query :=query+ ' kuerzel like "%'+edit_kuerzel.text+'%" and';
    if edit_ort.text<>'' then query :=query+ ' ort = "%'+edit_ort.text+'%" and';
  end;

	if edit_plz1.text<>'' then query :=query+ ' plz >="'+edit_plz1.text+'" and';
	if edit_plz2.text<>'' then query :=query+ ' plz <="'+edit_plz2.text+'" and';
	c_wert:=form_main.combowert(combobox_bg);
	if c_wert>=0 then query:= query+ ' i_bg = '+inttostr(c_wert) +' and';
	c_wert:=form_main.combowert(combobox_ga);
	if c_wert>=0 then query:= query+ ' i_gewerbeaufsicht = '+inttostr(c_wert)+' and';
	c_wert:=form_main.combowert(combobox_branche);
	if c_wert>=0 then query:= query+ ' i_branche = '+inttostr(c_wert)+' and';
	c_wert:=form_main.combowert(combobox_gruppe);
	if c_wert>=0 then query:= query+ ' i_f_gruppe = '+inttostr(c_wert)+' and';

//	query:=copy(query,1,length(query)-3); //letztes and l�schen
    liste1.clear;
    if query<>''  then
    begin
    kw1:=false;
       query:=query+ ' nummer >0';
       query:='select nummer, firma from firma where ' +query;
		sql_new(true,q_5,query,'firma');
		q_5.first;
		while not q_5.eof do
		begin
     	   liste1.Add(inttostr(q_5['nummer']));
			// firmen_nummern:=firmen_nummern+','+inttostr(q_5['nummer']) ;
			q_5.next;
		end;
    end;




	//firmen_nummern:=copy(firmen_nummern, 2,length(firmen_nummern)); //erstes komma weg
    // jetzt tree

    e:=0;
    if radiobutton3.Checked then und_oder:=' or ' else und_oder:=' and ';
    query:='';


    //if radiobutton3.Checked then liste2.clear;// oder: liste 2 leer - und - jetzt muss liste2 gesamtliste sein.  nicht l�schen
    liste2.clear;
    n:=0;
    for i:=0 to  Tree.Items.Count-1 do
	   begin
		 if (Tree.Items[i].stateindex>0) or (Tree.Items[i].selected) then //ausgew�hlt
			begin
                 kw2:=false;
                 inc(n);
				 knoten :=Tree.items[i];
				 daten:=knoten.Data;
				 f_nummer:=daten^.nummer;
                 s_nummer:=inttostr(f_nummer);

                 query:=format('select i_firma from firma_filter where i_firma_filter_vorlage=%s',[s_nummer]);
                 sql_new(true,datamodul.q_5,query,'');


                 while not datamodul.q_5.Eof do
                 begin
                 	s_nummer:=datamodul.q_5.findfield('i_firma').asstring;

                    if radiobutton3.Checked then //oder
                      begin
                    	if liste2.indexof(s_nummer)=-1 then  liste2.Add(s_nummer);
                       end
                    else
                       liste2.Add(s_nummer);

                 datamodul.q_5.next;
                end;
                e:=1;
            end;
	 end;


    if radiobutton4.Checked then//und
    begin
    	liste3.clear;
        for i:=0 to liste2.count-1 do   //liste 2 nach 3 kopieren
        begin
        	liste3.Add(liste2[i]);
        end;
        liste2.clear;
    	liste3.Sort;
        if n=0 then n:=1;
        for i:=0 to (liste3.Count-n) do
        begin
          s_nummer:=liste3[i];
          if liste3[i+n-1]=s_nummer then liste2.Add(s_nummer);    // es werden nur die �bernommen die n mal vorkommen
        end;
    end;




    //alle m�glichen firmen (berechtigung)

    query:='select nummer,firma from firma where nummer in ('+firmen_berechtigung+')';
    sql_new(true,q_5,query,'firma');
   liste3.clear;

	while not q_5.eof do
	begin
        s_nummer:=inttostr(q_5['nummer']);
        if liste3.indexof(s_nummer)=-1 then liste3.Add(s_nummer);
        if kw1 then        if liste1.indexof(s_nummer)=-1 then liste1.Add(s_nummer);
        if kw2 then        if liste2.indexof(s_nummer)=-1 then liste2.Add(s_nummer);
		q_5.next;
	end;



    liste4.clear; //endg�ltige Liste
    for i:=0 to (liste3.Count-1) do //alle m�glichen
    begin
     s_nummer:=liste3[i];
     if   radiobutton1.Checked then  //oder
     begin //oder
       if (liste1.indexof(s_nummer)<>-1) or (liste2.indexof(s_nummer)<>-1) then  liste4.Add(s_nummer);
     end
     else
     begin
     	if (liste1.indexof(s_nummer)<>-1) and (liste2.indexof(s_nummer)<>-1) then  liste4.Add(s_nummer);
     end;

    end;
   firmen_nummern:='-2';

   if  checkbox_rfiltern.checked then
   	firmen_nummern:=firmen_nummern+','+inttostr(fnummer)  //nur ausgew�hlte Firma
   else
      for i:=0 to liste4.Count-1 do firmen_nummern:=firmen_nummern+','+liste4[i];

  // if kw then  for i:=0 to liste3.Count-1 do firmen_nummern:=firmen_nummern+','+liste3[i];
end;


end;


procedure TForm_f_wahl.Edit_kuerzelChange(Sender: TObject);
begin
   //	filtern;
end;

procedure TForm_f_wahl.SpeedButton11Click(Sender: TObject);
var
fname:string;

begin

form_main.firmawaehlen(fname,fnummer);
edit_name.Text:=fname;

filtern;
end;



procedure TForm_f_wahl.BitBtn1Click(Sender: TObject);
begin
filtern;
end;

procedure TForm_f_wahl.TreeDblClick(Sender: TObject);
begin
if tree.selected.stateindex=-1 then tree.markieren(false,3)
else tree.markieren(false,-1);
end;

procedure TForm_f_wahl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     tree.liste_loeschen;
	  form_main.combodeaktivieren(combobox_bg);
	  form_main.combodeaktivieren(combobox_ga);
	  form_main.combodeaktivieren(combobox_branche);
	  form_main.combodeaktivieren(combobox_gruppe);
end;

end.
