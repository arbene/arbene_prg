�
 TFORM_MAIN 0%  TPF0
TForm_main	Form_mainLeft� Top� Width�Height�CaptionArbene deinstallierenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1Left(TopHWidth�HeightCaptionZ   Bittel löschen Sie gegebenefalls Programmdateien und Datenbankdateien von Arbene manuell.  TLabelLabel2Left0Top� Width� HeightCaptionDer MYSQL-Dienst wird entfernt  TLabelLabel3Left(Top0Width[HeightCaptionI   Es werden keine Daten gelöscht, sondern nur die Datenbank deinstalliert.  TLabelLabel4Left(TopWidthHeightCaption>Starten Sie vor der Benutzung dises Programms den Rechner neu!Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold ParentColor
ParentFont  TLabelLabel5Left TopxWidth� HeightCaption%   Bitte die Datenbankversion auswählen  TButtonButton1Left Top� Width� HeightCaptionDeinstallation startenTabOrder OnClickButton1Click  TMemoMemo1Left Top� Width�Height� ReadOnly	TabOrder  TBitBtnBitBtn1Left Top�Width� HeightTabOrderOnClickBitBtn1ClickKindbkClose  	TCheckBox	CheckBox2Left!Top� WidthHeightCaptionADatenbank MySql beenden und den automatischen Start  deaktivierenTabOrder  	TComboBoxComboBoxLeft Top� Width9Height
ItemHeightTabOrderText
MySql 3.23Items.Strings
MySql 3.23MariaDB 5.5    