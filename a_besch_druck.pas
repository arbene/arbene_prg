unit a_besch_druck;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls,OleCtnrs,shellApi, ComCtrls;

type
  TForm_kartei_druck = class(TForm)
    Panel2: TPanel;
    BitBtn_druck: TBitBtn;
    BitBtn2: TBitBtn;
    SpeedButton_2: TSpeedButton;
    MaskEdit_dat: TMaskEdit;
    SpeedButton_1: TSpeedButton;
    MaskEdit_dat_bis: TMaskEdit;
    Label1: TLabel;
    Label3: TLabel;
    Label_firma: TLabel;
    CheckBox_close: TCheckBox;
    CheckBox_vo_pro: TCheckBox;
    CheckBox_vo_fi: TCheckBox;
    CheckBox_ei_pro: TCheckBox;
    CheckBox_ei_fi: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    CheckBox_n_d: TCheckBox;
    CheckBox_nicht_sdruck: TCheckBox;
    CheckBox_bescheinigung_druck_immer: TCheckBox;
    Label6: TLabel;
    Label7: TLabel;
    CheckBox_drucken: TCheckBox;
    Label8: TLabel;
    Label9: TLabel;
    CheckBox_speichern_datei: TCheckBox;
    CheckBox_speichern_karteibaum: TCheckBox;
    CheckBox_email: TCheckBox;
    Button_nur_proband: TButton;
    Button_nur_firma: TButton;
    SpeedButton_3: TSpeedButton;
    MaskEdit_erstelldatum: TMaskEdit;
    Label5: TLabel;
    procedure BitBtn_druckClick(Sender: TObject);
    procedure SpeedButton_2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure druck_einzeln;
    procedure druck_alle;
    procedure warten;
    procedure CheckBox_bescheinigung_druck_immerClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure email_liste;
    procedure Button_nur_probandClick(Sender: TObject);
    procedure Button_nur_firmaClick(Sender: TObject);
  private
    { Private-Deklarationen }
    stop:boolean;
    drucktgerade:boolean;
    liste:tstringlist;
  public
    { Public-Deklarationen }
    einzeln:boolean;
    weiter:boolean;
    modus:integer;
  end;

var
  Form_kartei_druck: TForm_kartei_druck;

implementation

uses a_data, a_main, a_kalender, a_smtp;

{$R *.DFM}

procedure TForm_kartei_druck.BitBtn_druckClick(Sender: TObject);
var
datum1,datum2:tdate;
arbmed_besch_o:boolean;
begin
 if drucktgerade then
 begin
    weiter:=true;
    exit;
 end
 else

 try   //datumfehler abfangen
 	datum1:=strtodate (maskedit_dat.text);
   datum2:=strtodate (maskedit_dat_bis.text);
   if datum1>datum2 then
   	begin
      	showmessage('Bitte Datum vertauschen');
         exit;
      end;
 except
 	showmessage('Bitte das Datum korrigieren');
   exit;
 end;

 liste.Clear;

 try
   stop:=false;
   drucktgerade:=true;
   arbmed_besch_o:=arbmed_besch_offen;  //anhalten zum druck
   arbmed_besch_offen:=checkbox_nicht_sdruck.Checked;

   bescheinigung_in_kartei:=checkbox_speichern_karteibaum.Checked;
   speicher_bescheinigungen:=checkbox_speichern_datei.Checked;
   drucke_bescheinigungen:=checkbox_drucken.Checked;
   email_bescheinigungen:=CheckBox_email.Checked;

 if einzeln then druck_einzeln else druck_alle;
 finally
  arbmed_besch_offen:=arbmed_besch_o;
  stop:=true;
  drucktgerade:=false;
 end;

 if checkbox_email.Checked then email_liste;

 if CheckBox_close.Checked then modalresult:=mrok;

end;

procedure tform_kartei_druck.email_liste;
var
i,i_firma:integer;
adresse:string;
i_mitarbeiter:int64;
begin
  i_firma:=datamodul.q_firma.findfield('nummer').asinteger;
  i_mitarbeiter:=getbigint( datamodul.q_mitarbeiter,'nummer');
	form_smtp:=tform_smtp.create(self);
	 form_smtp.Memo.Text:='Siehe Anhang (PDF-Dateien)';
   for i:=0 to liste.Count-1 do
   	 form_smtp.ComboBoxAttachedFiles.items.add( liste[i]);
	 form_smtp.ComboBoxAttachedFiles.itemIndex := 0;
	 form_smtp.EditSubject.Text:='arbeitsmedizinische Bescheinigungen';

   //pirvat bekommt alles
   // Proband  privat
   adresse:=datamodul.q_mitarbeiter.findfield('email_p').asstring;
   if (checkbox_vo_pro.Checked or checkbox_ei_pro.Checked) and (adresse<>'') then
   form_smtp.memo_to.Lines.Add(adresse);

   //Proband firma
   adresse:=datamodul.q_mitarbeiter.findfield('email_f').asstring;
   if (checkbox_vo_pro.Checked or checkbox_ei_pro.Checked) and (adresse<>'') then
   form_smtp.memo_to.Lines.Add(adresse);

   // nur wenn keine probandenbezogenen Bescheinigungen
   if not (checkbox_vo_pro.Checked or checkbox_ei_pro.Checked) then
   begin
     //ansprechpartner
     if (checkbox_vo_fi.Checked or checkbox_ei_fi.Checked) then
      email_von_ansprechpartner ( form_smtp.memo_to.Lines,i_firma,1);

     //abteilung

     if (checkbox_vo_fi.Checked or checkbox_ei_fi.Checked)  then
     begin
      email_von_ma_abt(form_smtp.memo_to.Lines, i_mitarbeiter);
     end;
   end;

	 form_smtp.ShowModal;

end;

procedure TForm_kartei_druck.druck_alle;
  var
  dat_bis,dat_von:tdate;
  t:ttime;
  //adressat,typ:string;
begin
  try
    dat_bis:=strtodate(maskedit_dat_bis.text);
    dat_von:=strtodate(maskedit_dat.text);
  except
     showmessage('Bitte Datum angeben');
     exit;
  end;
   //stop:=false;
  try


   be_druck_immer:=0;
   datamodul.q_firma.first;    //entsprechend den gefilterten Firmen

   while not datamodul.q_firma.eof do
   begin
    label_firma.Caption:=datamodul.q_firma['firma'];
    form_main.mitarbeiter_filtern;
    datamodul.q_mitarbeiter.first;

    while not datamodul.q_mitarbeiter.eof do
    begin
         if stop then
         begin
              exit;
              //modalresult:=mrok;
         end;
         druck_einzeln;

         datamodul.q_mitarbeiter.Next;
         //application.BringToFront;
         arbene_vor;
         application.ProcessMessages;

    end;
    datamodul.q_firma.Next;
    end;
 finally
  
  modalresult:=mrok;
 end;
end;

procedure TForm_kartei_druck.druck_einzeln;
  var
  dat_bis,dat_von,dat_erstellt:tdate;
  adressat,typ,pdf_datname_email:string;
  i:integer;
  gedruckt:boolean;

function datname_email(vorspann:string):string;
var
name,vorname, gebdat:string;
i:integer;
begin
//proband name, vorname geb_dat +zahl
//wenn nicht ausgewählt leerer string
if checkbox_email.Checked then
begin
  result:=format('%sa_%s-%s-%s-%s-',[pfad_temp,vorspann, datamodul.q_mitarbeiter.FindField('name').asstring,datamodul.q_mitarbeiter.FindField('vorname').asstring,datamodul.q_mitarbeiter.FindField('geb_dat').asstring ]);
  i:=0;
  while fileexists(result+inttostr(i)+'.pdf') do
  begin
    inc(i);
  end;
  result:=result+inttostr(i);
end
else
result:='';  
end;


//##############################
begin
			dat_bis:=strtodate(maskedit_dat_bis.text);
      dat_von:=strtodate(maskedit_dat.text);
      dat_erstellt:=strtodate(maskedit_erstelldatum.text);

         pdf_datname_email:=datname_email('Vorsorge_Proband');

         gedruckt:=false;
         if checkbox_vo_pro.Checked then
         begin
             gedruckt:=form_main.bescheinigung_erstellen (dat_erstellt, dat_von,dat_bis, modus,'vorsorge','proband',checkbox_n_d.checked, true,CheckBox_drucken.checked,CheckBox_nicht_sdruck.Checked,checkbox_speichern_karteibaum.checked,pdf_datname_email);
             if ((pdf_datname_email<>'') and gedruckt) then liste.Add(pdf_datname_email+'.pdf');
         end;

         application.ProcessMessages;

         if checkbox_vo_fi.Checked then
         begin
            pdf_datname_email:=datname_email('Vorsorge_Firma');
            gedruckt:=form_main.bescheinigung_erstellen(dat_erstellt,dat_von,dat_bis, modus,'vorsorge','firma',checkbox_n_d.checked,checkbox_bescheinigung_druck_immer.checked,CheckBox_drucken.checked,CheckBox_nicht_sdruck.Checked,checkbox_speichern_karteibaum.checked,pdf_datname_email);
            if ((pdf_datname_email<>'') and gedruckt) then liste.Add(pdf_datname_email+'.pdf');

         end;
         application.ProcessMessages;

         if checkbox_ei_pro.Checked then
         begin
           pdf_datname_email:=datname_email('Eignung_Proband');
            gedruckt:=form_main.bescheinigung_erstellen(dat_erstellt,dat_von,dat_bis, modus,'eignung','proband',checkbox_n_d.checked,true,CheckBox_drucken.checked,CheckBox_nicht_sdruck.Checked,checkbox_speichern_karteibaum.checked,pdf_datname_email);
            if ((pdf_datname_email<>'') and gedruckt) then liste.Add(pdf_datname_email+'.pdf');
         end;
         application.ProcessMessages;

         if checkbox_ei_fi.Checked then
         begin
            pdf_datname_email:=datname_email('Eignung_Firma');
            gedruckt:=form_main.bescheinigung_erstellen(dat_erstellt,dat_von,dat_bis, modus,'eignung','firma',checkbox_n_d.checked,checkbox_bescheinigung_druck_immer.checked,CheckBox_drucken.checked,CheckBox_nicht_sdruck.Checked,checkbox_speichern_karteibaum.checked,pdf_datname_email);
           if ((pdf_datname_email<>'') and gedruckt) then liste.Add(pdf_datname_email+'.pdf');
         end;
         application.ProcessMessages;


         //form_main.bescheinigung_erstellen(dat_von,dat_bis, 0,typ,adressat);
 end;

procedure TForm_kartei_druck.SpeedButton_2Click(
  Sender: TObject);
begin
  form_kalender.showmodal;
  if sender=speedbutton_1 then 	maskedit_dat_bis.text :=datetostr(int(form_kalender.auswahlkalender.date));
   if sender=speedbutton_2 then 	maskedit_dat.text :=datetostr(int(form_kalender.auswahlkalender.date));
   if sender=speedbutton_3 then 	maskedit_erstelldatum.text :=datetostr(int(form_kalender.auswahlkalender.date));
end;

procedure TForm_kartei_druck.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
stop:=true;
checkbox_nicht_sdruck.Checked:=arbmed_besch_offen;
CheckBox_bescheinigung_druck_immer.checked:=berechtigung_druck_immer;
checkbox_speichern_karteibaum.Checked:=bescheinigung_in_kartei;
checkbox_speichern_datei.Checked:=speicher_bescheinigungen;
checkbox_drucken.Checked:=drucke_bescheinigungen;
CheckBox_email.Checked:=email_bescheinigungen;
liste:=tstringlist.create;
end;

procedure TForm_kartei_druck.BitBtn2Click(Sender: TObject);
begin
if (stop =false) then stop:=true
else modalresult:=mrok;
end;


procedure TForm_kartei_druck.warten;
begin
 if checkbox_nicht_sdruck.Checked  then
         begin
          weiter:=false;
          while ((form_main.Arbeitsmedizin.State in  [osopen	]) and (not weiter) ) do
          begin

            application.ProcessMessages;
            if stop then
            begin
              modalresult:=mrok;
              exit;
            end;
           end;
         end;
end;

procedure TForm_kartei_druck.CheckBox_bescheinigung_druck_immerClick(
  Sender: TObject);
begin
berechtigung_druck_immer:=checkbox_bescheinigung_druck_immer.checked;
end;

procedure TForm_kartei_druck.FormDestroy(Sender: TObject);
begin
liste.clear;
liste.Free;
end;

procedure TForm_kartei_druck.Button_nur_probandClick(Sender: TObject);
begin
  checkbox_vo_pro.Checked:=true;
  checkbox_ei_pro.Checked:=true;
  checkbox_vo_fi.Checked:=false;
  checkbox_ei_fi.Checked:=false;
end;

procedure TForm_kartei_druck.Button_nur_firmaClick(Sender: TObject);
begin
  checkbox_vo_pro.Checked:=false;
  checkbox_ei_pro.Checked:=false;
  checkbox_vo_fi.Checked:=true;
  checkbox_ei_fi.Checked:=true;

end;

end.
