unit start;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons,shellapi;

type
  TForm1 = class(TForm)
    Image1: TImage;
    BitBtn_komplett: TBitBtn;
    BitBtn_update: TBitBtn;
    BitBtn_acrobat: TBitBtn;
    BitBtn_fernwartung: TBitBtn;
    BitBtn_info: TBitBtn;
    BitBtn_msinst: TBitBtn;
    Memo1: TMemo;
    Memo2: TMemo;
    Memo3: TMemo;
    Memo4: TMemo;
    Memo5: TMemo;
    Memo6: TMemo;
    BitBtn_ende: TBitBtn;
    Memo7: TMemo;
    BitBtn_infotexte: TBitBtn;
    procedure BitBtn_endeClick(Sender: TObject);
    procedure BitBtn_infoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_komplettClick(Sender: TObject);
    procedure BitBtn_updateClick(Sender: TObject);
    procedure BitBtn_acrobatClick(Sender: TObject);
    procedure BitBtn_msinstClick(Sender: TObject);
    procedure BitBtn_fernwartungClick(Sender: TObject);
    procedure BitBtn_infotexteClick(Sender: TObject);
  private
    { Private-Deklarationen }
    function wurzelpfad(pfad: string): string;
    function stringwenden(wort: string): string;
    procedure winrun(pfad, parameter: string);
  public
    { Public-Deklarationen }
    betriebssystem:integer;
    w_pfad:string;
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.BitBtn_endeClick(Sender: TObject);
begin
application.Terminate;
end;

procedure TForm1.BitBtn_infoClick(Sender: TObject);
Var p:string;
begin
     p:=w_pfad+'\doku\arbene.html';
     shellexecute(form1.handle,'open',pchar(p),pchar(''),nil,SW_showmaximized);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
s1,s2,query:string;
versionsinfo: OSVERSIONINFO;
begin
	versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;

	w_pfad:=wurzelpfad(application.ExeName);
end;

function tform1.wurzelpfad(pfad: string): string;
var
back: integer;
begin
pfad:=copy(pfad, 1, length(pfad)-1);
pfad:=stringwenden(pfad);
back:=pos('\', pfad);
pfad:=copy(pfad, back+1, length(pfad));
result:=stringwenden(pfad);
if  pos('\', result)=0 then result:=result+'\';
end;

function tform1.stringwenden(wort: string): string;
var
 laenge, i: integer;
begin
laenge:=length(wort);
result:='';
for i:=laenge downto 1 do
begin
	result:=result+copy(wort,i,1);
end;
end;


procedure Tform1.winrun(pfad, parameter: string);
var

 erzeugt: boolean;
 datei,command :pchar;
 startinfo: tstartupinfo;
 processinfo: tprocessinformation;
begin

	datei:= pchar(pfad);
	command:=pchar(parameter);
	//if (processinfo.hProcess=0) then //processinfo:=runexe(datei,nil);
	begin
	startinfo.cb:=68;
	startinfo.lpReserved:=nil;
	startinfo.lpDesktop:=nil;
	startinfo.lpTitle:=nil;
	startinfo.dwX:=0;
	startinfo.dwY:=0;
	startinfo.dwXSize:=0;
	startinfo.dwYsize:=0;
	startinfo.dwXCountChars:=0;
	startinfo.dwYCountChars:=0;
	startinfo.dwFillAttribute:=0;
	startinfo.dwFlags:=129;
	startinfo.wShowWindow:=9;
	startinfo.cbReserved2:=0;
	startinfo.lpReserved2:=nil;
	startinfo.hStdInput:=0;
	startinfo.hStdOutput:=0;
	startinfo.hStdError:=0;
	try
	erzeugt:=createprocess(datei,command,nil,nil,false,create_default_error_mode,nil,nil,startinfo,processinfo);
	if not erzeugt then processinfo.hProcess:=0;
	except
	 processinfo.hProcess:=0;
	end;

  end;
end;

procedure TForm1.BitBtn_komplettClick(Sender: TObject);
var p:string;
begin
p:=w_pfad+'\inst_programme\arbene_setup.exe';
winrun(p,'');
end;

procedure TForm1.BitBtn_updateClick(Sender: TObject);
var p:string;
begin
p:=w_pfad+'\inst_programme\arbene_update.exe';
winrun(p,'');

end;

procedure TForm1.BitBtn_acrobatClick(Sender: TObject);
var p:string;
begin
p:=w_pfad+'\inst_programme\AdbeRdr60_deu.exe';
winrun(p,'');

end;

procedure TForm1.BitBtn_msinstClick(Sender: TObject);
var p:string;
begin
if betriebssystem =VER_PLATFORM_WIN32_NT then p:=w_pfad+'\inst_programme\instmsiw.exe'
else p:=w_pfad+'\inst_programme\instmsia.exe';
winrun(p,'');


end;

procedure TForm1.BitBtn_fernwartungClick(Sender: TObject);
var p:string;
begin
p:=w_pfad+'\inst_programme\vnc-3.3.7-x86_win322.exe';
winrun(p,'');

end;

procedure TForm1.BitBtn_infotexteClick(Sender: TObject);
var p:string;
begin
p:=w_pfad+'\inst_programme\arbene_info_texte.exe';
winrun(p,'');
end;

end.
