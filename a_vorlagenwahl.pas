unit a_vorlagenwahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, FileCtrl, Buttons, ExtCtrls, a_main;

type
  TForm_vorlagenwahl = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Edit_titel: TEdit;
    GroupBox2: TGroupBox;
    FileListBox: TFileListBox;
    procedure FormCreate(Sender: TObject);
    procedure FileListBoxDblClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_vorlagenwahl: TForm_vorlagenwahl;

implementation

{$R *.DFM}

procedure TForm_vorlagenwahl.FormCreate(Sender: TObject);
begin
	 filelistbox.Directory:=vorlagenverzeichnis;
end;

procedure TForm_vorlagenwahl.FileListBoxDblClick(Sender: TObject);
begin
modalresult:=mrok;
end;

end.
