program ArbeneTools;

uses
  Forms,
  abackup_main in 'abackup_main.pas' {Form_main},
  zip in 'zip.pas',
  a_pfadwahl in 'a_pfadwahl.pas' {Form_pfadwahl};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Datenbank-Tools';
  Application.CreateForm(TForm_main, Form_main);
  Application.CreateForm(TForm_pfadwahl, Form_pfadwahl);
  Application.Run;
end.
