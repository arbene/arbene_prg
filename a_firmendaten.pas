unit a_firmendaten;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_datamodul,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Grids, DBGrids, dbgridEXT, Mask,
  ComCtrls, ToolWin,db, Spin;

type
  TForm_firmendaten = class(TForm)
    ToolBar_firma: TToolBar;
    ToolButton_neu: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton4: TToolButton;
    ToolButton_cancel: TToolButton;
    PageControl_main: TPageControl;
    TabSheet_Adresse: TTabSheet;
    TabSheet_zeit: TTabSheet;
    TabSheet_Ansprechpartner: TTabSheet;
    ToolButton6: TToolButton;
    ToolButton_save: TToolButton;
    ToolButton8: TToolButton;
    ToolButton_del: TToolButton;
    GroupBox_firma: TGroupBox;
    Panel_ausw: TPanel;
    DBgridEXT1: TDBgridEXT;
    Panel2: TPanel;
    GroupBox_ansprech: TGroupBox;
    Panel3: TPanel;
    DBgridEXT2: TDBgridEXT;
    Panel4: TPanel;
    DBEdit_funktion: TDBEdit;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox_ansp_anmerk: TGroupBox;
    DBMemo_an_memo: TDBMemo;
    DBEdit_name: TDBEdit;
    DBEdit_an_telefon: TDBEdit;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit_firma: TDBEdit;
    DBEdit_telefon: TDBEdit;
    DBEdit_homepage: TDBEdit;
    DBEdit_plz: TDBEdit;
    DBEdit_strasse: TDBEdit;
    DBEdit_ort: TDBEdit;
    DBEdit_fax: TDBEdit;
    DBEdit_email: TDBEdit;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    PageControl_zeit: TPageControl;
    TabSheet_soll: TTabSheet;
    TabSheet_ist: TTabSheet;
    GroupBox2: TGroupBox;
    DBgridEXT3: TDBgridEXT;
    Panel6: TPanel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    DBgridEXT4: TDBgridEXT;
    DBEdit1: TDBEdit;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    GroupBox4: TGroupBox;
    DBEdit2: TDBEdit;
    GroupBox5: TGroupBox;
    DBMemo3: TDBMemo;
    GroupBox6: TGroupBox;
    ToolButton_exit: TToolButton;
    ToolButton11: TToolButton;
    SpeedButton2: TSpeedButton;
    DBMemo2: TDBMemo;
    SpeedButton1: TSpeedButton;
    Label_firma: TLabel;
    Label_sollgesamt: TLabel;
    Label_istgesamt: TLabel;
    Label_fehlt: TLabel;
    DBEdit_sollh: TDBEdit;
    DBEdit_sollm: TDBEdit;
    DBEdit_isth: TDBEdit;
    DBEdit_istm: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    procedure ToolButton_neuClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure ToolButton_cancelClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton_exitClick(Sender: TObject);
    procedure PageControl_mainChange(Sender: TObject);
    procedure DBEdit_isthChange(Sender: TObject);
    procedure DBEdit_istmChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure PageControl_mainChanging(Sender: TObject;
      var AllowChange: Boolean);
  private
    { Private-Deklarationen }

    procedure firmaspeichern(direkt: boolean);
    procedure allessperren;
  public
    { Public-Deklarationen }
    procedure zeitabrechnung;
    function zeit_str(stunden,minuten:integer):string;
end;

var
  Form_firmendaten: TForm_firmendaten;

implementation

uses a_main, a_kalender;

{$R *.DFM}

procedure TForm_firmendaten.ToolButton_neuClick(Sender: TObject);
begin
if pagecontrol_main.activepage=tabsheet_adresse then
begin
	datamodule1.Table_Firma.AppendRecord([nil,'','']);
  form_main.entsperren(tabsheet_adresse);

end;
if pagecontrol_main.activepage=tabsheet_ansprechpartner then
begin
	datamodule1.Table_Firma_ansprech.AppendRecord([nil,datamodule1.Table_Firma['nummer']]);
	form_main.entsperren(tabsheet_ansprechpartner);
end;
if (pagecontrol_main.activepage=tabsheet_zeit) and (pagecontrol_zeit.activepage=tabsheet_soll) then
begin
	datamodule1.Table_Firma_zeit_soll.AppendRecord([nil,datamodule1.Table_Firma['nummer'],date,0,0]);
	form_main.entsperren(tabsheet_soll);
end;
if (pagecontrol_main.activepage=tabsheet_zeit) and (pagecontrol_zeit.activepage=tabsheet_ist) then
begin
	datamodule1.Table_Firma_zeit_ist.AppendRecord([nil,datamodule1.Table_Firma['nummer'],date,0,0]);
  form_main.entsperren(tabsheet_ist);
end;

end;

procedure TForm_firmendaten.ToolButton_editClick(Sender: TObject);
begin
if pagecontrol_main.activepage=tabsheet_adresse then
begin
	form_main.entsperren(tabsheet_adresse);
end;
if pagecontrol_main.activepage=tabsheet_ansprechpartner then
begin
	form_main.entsperren(tabsheet_ansprechpartner);
end;
if (pagecontrol_main.activepage=tabsheet_zeit) and (pagecontrol_zeit.activepage=tabsheet_soll) then
begin
	form_main.entsperren(tabsheet_soll);
end;
if (pagecontrol_main.activepage=tabsheet_zeit) and (pagecontrol_zeit.activepage=tabsheet_ist) then
begin
	form_main.entsperren(tabsheet_ist);
end;
end;

procedure TForm_firmendaten.ToolButton_saveClick(Sender: TObject);
begin
	firmaspeichern(true);
end;

procedure TForm_firmendaten.ToolButton_cancelClick(Sender: TObject);
begin
with datamodule1 do
begin
	if Table_Firma.state=dsedit then  cancel_loeschen(Table_Firma) ;
  if Table_Firma_ansprech.state=dsedit then cancel_loeschen(Table_Firma_ansprech);
  if Table_Firma_zeit_soll.state=dsedit then cancel_loeschen(Table_Firma_zeit_soll);
  if Table_Firma_zeit_ist.state=dsedit then cancel_loeschen(Table_Firma_zeit_ist);
  allessperren;
end;
end;

procedure TForm_firmendaten.ToolButton_delClick(Sender: TObject);
begin
if Messagedlg('Soll der aktuelle Datensatz gel�scht werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
if pagecontrol_main.activepage=tabsheet_adresse then
begin
	datamodule1.Table_Firma.delete;
	form_main.sperren(tabsheet_adresse);
end;
if pagecontrol_main.activepage=tabsheet_ansprechpartner then
begin
	datamodule1.Table_firma_ansprech.delete;
	form_main.sperren(tabsheet_ansprechpartner);
end;
if (pagecontrol_main.activepage=tabsheet_zeit) and (pagecontrol_zeit.activepage=tabsheet_soll) then
begin
	datamodule1.Table_Firma_zeit_soll.delete;
	form_main.sperren(tabsheet_soll);
end;
if (pagecontrol_main.activepage=tabsheet_zeit) and (pagecontrol_zeit.activepage=tabsheet_ist) then
begin
	datamodule1.Table_Firma_zeit_ist.delete;
	form_main.sperren(tabsheet_ist);
end;

end;

procedure TForm_firmendaten.FormCreate(Sender: TObject);
begin
  form_main.form_positionieren(tform(sender));
  grid_suchen(sender);
  allessperren;
end;
procedure TForm_firmendaten.allessperren;
begin
  form_main.sperren(tabsheet_adresse);
	form_main.sperren(tabsheet_ansprechpartner);
	form_main.sperren(tabsheet_ist);
	form_main.sperren(tabsheet_soll);
end;

procedure TForm_firmendaten.ToolButton_exitClick(Sender: TObject);
begin
	firmaspeichern(false);
  form_main.firmalesen;  
  modalresult:=mrok;
end;

procedure TForm_firmendaten.zeitabrechnung;
var
i_firma,sh,sm,ih,im:integer;
soll,ist: tdatetime;
book_soll, book_ist: tbookmark;
begin
sh:=0; sm:=0; ih:=0;im:=0;
//if datamodule1.Table_firma_zeit_soll['i_firma']=null then exit;
try
	book_soll:=datamodule1.Table_firma_zeit_soll.GetBookmark ;
	book_ist:=datamodule1.Table_firma_zeit_ist.GetBookmark ;
	datamodule1.Table_firma_zeit_soll.First;
	i_firma:=datamodule1.Table_firma['nummer'];
	while (datamodule1.Table_firma_zeit_soll['i_firma']=i_firma) and (not datamodule1.Table_firma_zeit_soll.Eof) do
	begin
		if  datamodule1.Table_firma_zeit_soll['stunden']<>null then sh:=sh+datamodule1.Table_firma_zeit_soll['stunden'] ;
     if datamodule1.Table_firma_zeit_soll['minuten']<> null then sm:=sm+datamodule1.Table_firma_zeit_soll['minuten'];
 	 	datamodule1.Table_firma_zeit_soll.next;
	end;

	datamodule1.Table_firma_zeit_ist.First;
	while (datamodule1.Table_firma_zeit_ist['i_firma']=i_firma) and (not datamodule1.Table_firma_zeit_ist.Eof) do
	begin
		if datamodule1.Table_firma_zeit_ist['stunden']<> null then ih:=ih+datamodule1.Table_firma_zeit_ist['stunden'];
 		if datamodule1.Table_firma_zeit_ist['minuten']<>null then im:=im+datamodule1.Table_firma_zeit_ist['minuten'];
	  datamodule1.Table_firma_zeit_ist.next;
	end;
	Label_sollgesamt.caption:='Soll Gesamtstunden: '+zeit_str(sh,sm) +' h';
	Label_istgesamt.caption:='Ist Gesamtstunden: '+zeit_str(ih,im) +' h';
	Label_fehlt.caption:='Noch zu leisten: '+zeit_str(sh-ih,sm-im)+' h';
finally
	datamodule1.Table_firma_zeit_soll.GotoBookmark(book_soll);
	datamodule1.Table_firma_zeit_ist.GotoBookmark(book_ist);
	datamodule1.Table_firma_zeit_soll.FreeBookmark(book_soll);
	datamodule1.Table_firma_zeit_ist.freebookmark(book_ist);
end;
end;


procedure TForm_firmendaten.PageControl_mainChange(Sender: TObject);
begin
if pagecontrol_main.ActivePage=tabsheet_zeit then zeitabrechnung;
end;

function TForm_firmendaten.zeit_str(stunden,minuten:integer):string;
var
Year, Month, Day, Hour, Min, Sec, MSec: Word;
tage:integer;
s_minuten:string;

begin
hour:=minuten div 60;
min:= (minuten mod 60);


if min<10 then s_minuten:='0'+inttostr(min) else s_minuten:=inttostr(min);
result:=inttostr(hour+stunden)+':'+ s_minuten;
end;

procedure TForm_firmendaten.DBEdit_isthChange(Sender: TObject);
var
i:integer;
begin
try

i:=strtoint(DBEdit_isth.Text);
except
	showmessage('keine Zahl');
  DBEdit_isth.Text:='';
end;
if  (i<0 )then
begin
	showmessage('Bitte Zahl gr��er 0 angeben.');
  DBEdit_isth.Text:='0'
end;
end;

procedure TForm_firmendaten.DBEdit_istmChange(Sender: TObject);
var
i:integer;
begin
try
i:=strtoint(DBEdit_isth.Text);
except
	showmessage('keine Zahl');
  DBEdit_isth.Text:='0';
end;
if (i>60) or (i<0 )then
begin
	showmessage('Bitte Zahl zwischen 0 und 60 angeben.');
  DBEdit_isth.Text:='0'
end;
end;

procedure TForm_firmendaten.SpeedButton1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
   begin
   datamodule1.table_firma_zeit_soll.edit;
   datamodule1.table_firma_zeit_soll['datum'] :=(form_kalender.MonthCalendar.date);
   end;
end;

procedure TForm_firmendaten.SpeedButton2Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
   begin
      datamodule1.table_firma_zeit_soll.edit;
		   datamodule1.table_firma_zeit_ist['datum'] :=(form_kalender.MonthCalendar.date);
  end;      
end;

procedure TForm_firmendaten.firmaspeichern(direkt: boolean);
begin
direktspeichern:=direkt;
if datamodule1.Table_Firma.state=dsedit then  datamodule1.Table_Firma.Post ;
  if datamodule1.Table_Firma_ansprech.state=dsedit then datamodule1.Table_Firma_ansprech.post;
  if datamodule1.Table_Firma_zeit_soll.state=dsedit then
      begin
  	 datamodule1.Table_Firma_zeit_soll.post;
  	zeitabrechnung;
      end;
  if datamodule1.Table_Firma_zeit_ist.state=dsedit then
  begin
	  datamodule1.Table_Firma_zeit_ist.post;
    zeitabrechnung;
  end;
  allessperren;
end;
procedure TForm_firmendaten.PageControl_mainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    firmaspeichern(false);
end;

end.
