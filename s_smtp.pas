unit a_smtp;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ComCtrls,  ExtCtrls, Buttons, MimeMess ,SMTPsend,POP3send;

type
  TForm_smtp = class(TForm)
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StatusBar: TStatusBar;
    Panel2: TPanel;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EditSubject: TEdit;
    ComboBoxAttachedFiles: TComboBox;
    ButtonAddAttachedFile: TButton;
    ButtonClearAttachedFiles: TButton;
	  Memo_to: TMemo;
    ComboBox_adressbuch: TComboBox;
    Label3: TLabel;
    PageControl: TPageControl;
    TabSheet_text: TTabSheet;
    TabSheet_anhang: TTabSheet;
    Memo: TMemo;
    Panel_send: TPanel;
	  RichEdit: TRichEdit;
    BitBtn3: TBitBtn;
    SpeedButton1: TSpeedButton;
	  procedure ButtonAddAttachedFileClick(Sender: TObject);
	  procedure ButtonClearAttachedFilesClick(Sender: TObject);
	  procedure BitBtn1Click(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure ComboBox_adressbuchClick(Sender: TObject);
	  procedure PageControlChange(Sender: TObject);
	  procedure ComboBoxAttachedFilesChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
	private
	  { Private declarations }
	  sendto:string;
	  function email_ansprechpartner() :string;
	  procedure richedit_laden;
	 function zeile_normieren(zeile:string):string;
  function is_email_adr(zeile:string):boolean;
	public
	  { Public declarations }
	  mime:TMIMEMess;
	  pop:tpop3send;
	  function send(sichtbar:boolean):boolean;
	  function mime_add_adressen:boolean;
	  function test_connect:boolean;

	end;

var
	Form_smtp: TForm_smtp;

implementation

uses s_main;



{$R *.DFM}
type SMTPError = class( Exception);

procedure TForm_smtp.ButtonAddAttachedFileClick(Sender: TObject);
begin
	 if openDialog1.execute then
	 begin
		ComboBoxAttachedFiles.items.add( OpenDialog1.FileName);
		ComboBoxAttachedFiles.itemIndex := 0;
	 end;
end;

procedure TForm_smtp.ButtonClearAttachedFilesClick(Sender: TObject);
var
i:integer;
begin
	i:=ComboBoxAttachedFiles.ItemIndex;
	ComboBoxAttachedFiles.Items.Delete(i);
	comboboxattachedfiles.Refresh;

end;


function TForm_smtp.send(sichtbar:boolean):boolean;
procedure keineverbindung;
begin
	showmessage('Keine Verbindung zum Mail-Server');
	//if sichtbar then form_email.addmail(0,memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items);
	result:=false;
end;


var
i:integer;
email_name, sendto,email_smtp:string;

begin
	 email_name:='klawitter';
	 sendto:='klawitter@arbene.de';
	 email_smtp:='';
	 result:=false;

	 if sichtbar then
	 begin

	 end;
	 //form_email.popini;
	 screen.cursor := crHourGlass;

	 if  not mime_add_adressen then exit;

	 if trim(sendto)='' then
	 begin
		showmessage('Mail kann nicht verschickt werden -keine korrekte email-adresse vorhanden');
		exit;
	 end;
	 statusbar.Panels[0].text:='Verbindung POP3-Server';
	 application.ProcessMessages;
	 result:=test_connect;
	 if not result then
	 begin
		keineverbindung;
		exit;
	 end;
		 statusbar.Panels[0].text:='Mail vorbereiten';
	 application.ProcessMessages;
	 mime.Clear;
	 mime.PartList.clear;
	 mime.Header.Subject := EditSubject.text;
	 mime.Header.From:= 'arbene';
	 mime.Header.ToList.Add(sendto);// :=sendto;
	 mime.Header.Date:=now();
	 //mime.Header.CCList.Add('');

	 mime.AddPartText(tstringlist(memo.lines));

	 for i:=0 to ComboBoxAttachedFiles.Items.Count-1 do  mime.AddPartBinary(ComboBoxAttachedFiles.Items[i]);
	 mime.EncodeMessage;
	 mime.FinalizeHeaders;

	 statusbar.Panels[0].text:='Versenden';
	 application.ProcessMessages;

	 if SendToRaw(email_name, sendto, email_smtp, mime.Lines, '', '') then //email_useraus
	 begin
		  //	form_email.Sb.Panels[0].Text := '';
			result:=true;

	 end
		else keineverbindung;

	 screen.cursor := crDefault;
	 statusbar.Panels[0].text:='Versenden abgeschlossen';
	 end;


function tform_smtp.mime_add_adressen:boolean;
var
i:integer;
zeile:string;
begin
result:=true;
sendto:='';
 i:=0;
	 while i< memo_to.Lines.Count do
	 begin
		zeile:=memo_to.Lines[i];
		zeile:=zeile_normieren(zeile);
		if zeile <>'' then
		begin
			if is_email_adr(zeile) then sendto:=SendTo+zeile+ ' , '
			else
			begin
				showmessage('"'+zeile +'" ist keine korrekte email-Adresse');
				result:=false;
			end;
			inc(i);
		end
		else
			 memo_to.lines.Delete(i);
	end;
	if memo_to.lines.count=0 then
	begin
		showmessage('Keine g�ltige email-Adresse vorhanden');
		result:=false;
	end;

end;

procedure TForm_smtp.BitBtn1Click(Sender: TObject);
begin

	panel_send.Visible:=true;
	if send(true) then modalresult:=mrok
	else
	begin
	showmessage('Die mail konnte nicht verschickt werden, verschicken Sie sie aus der Mailbox.');
	modalresult:=mrcancel;
	end;
	panel_send.Visible:=false;
end;

procedure TForm_smtp.FormCreate(Sender: TObject);
begin
//form_main.form_positionieren(tform(sender));
ComboBox_adressbuch.ItemIndex:=0;
sendto:='';
mime:=TMIMEMess.create;
pop:=tpop3send.create;
end;





procedure TForm_smtp.PageControlChange(Sender: TObject);
begin
if pagecontrol.ActivePage=tabsheet_anhang then richedit_laden;
end;







procedure TForm_smtp.FormDestroy(Sender: TObject);
begin
mime.free;
pop.free;
end;


function tform_smtp.test_connect:boolean;
var s:string;
begin
	 result:=true;
	 if email_pop+email_userein='' then exit;
	 pop.Password:=email_passwortein;
	 pop.Username:= email_userein;
	 pop.POP3Host:=email_pop;
	 result:= pop.Login;
	 pop.Logout;
end;

function tform_smtp.zeile_normieren(zeile:string):string;
 var
	 position:integer;
 begin
		position:=pos(',',zeile);
		 while position >0 do begin
		  delete(zeile,position,1);
		  insert(' ',zeile,position);
		  position:=pos(' ',zeile);
		 end;
		result:=trim(zeile);
 end;

function tform_smtp.is_email_adr(zeile:string):boolean;
var p:integer;
begin
	result:=false;
	p:=pos('@',zeile);
	if p<1 then exit;
	zeile:=copy(zeile,p+1,length(zeile));
	p:=pos('.',zeile);
	if p<1 then exit;
	zeile:=copy(zeile,p+1,length(zeile));
	if length(zeile)=0 then exit;
	result:=true;
end;


end.
