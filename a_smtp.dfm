�
 TFORM_SMTP 0�  TPF0
TForm_smtp	Form_smtpLeft`Top� Width�HeightQHelpContext�*CaptionMail schreibenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	OnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TPanelPanel1Left Top�Width�Height(AlignalBottomTabOrder  TBitBtnBitBtn1LeftWTopWidth� HeightCaptionVerschickenDefault	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder OnClickBitBtn1Click
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn2Left1TopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderKindbkAbort  TBitBtnBitBtn3Left(TopWidth� HeightCaption   später verschickenDefault	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderVisibleOnClickBitBtn3Click
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs   
TStatusBar	StatusBarLeft TopWidth�HeightPanelsWidth,    TPanelPanel2Left Top Width�Height� AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel1LeftXTopGWidth\HeightCaption   Angefügte Dateien:  TLabelLabel7LeftTopWidthHeightCaptionAn:  TLabelLabel8LeftTop\Width"HeightCaptionBetreff:  TLabelLabel3LeftXTopWidthDHeightCaption   Adressbücher:  TSpeedButtonSpeedButton1TagLeft9TopWidthpHeightCaptionName Suchen
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsOnClickComboBox_adressbuchClick  TEditEditSubjectLeftTopjWidthHeightTabOrder  	TComboBoxComboBoxAttachedFilesLeftSTopjWidth�HeightStylecsDropDownList
ItemHeightTabOrderOnChangeComboBoxAttachedFilesChange  TButtonButtonAddAttachedFileLeftTopMWidth9HeightCaption   HinzufügenTabOrderOnClickButtonAddAttachedFileClick  TButtonButtonClearAttachedFilesLeftZTopMWidth9HeightCaption	EntfernenTabOrderOnClickButtonClearAttachedFilesClick  TMemoMemo_toLeftTopWidthHeightD
ScrollBars
ssVerticalTabOrderWordWrap  	TComboBoxComboBox_adressbuchLeftXTopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontTabOrder OnClickComboBox_adressbuchClick
OnDblClickComboBox_adressbuchClickItems.Stringse-mail Firmae-mail privat Ansprechpartner 	Abteilung   TPanel
Panel_sendLeft�TopWidth3Height4
BevelOuterbvNoneTabOrderVisible  TButtonButton_anzeigenLeft�TopMWidth8HeightCaptionAnzeigenTabOrderOnClickButton_anzeigenClick   TPageControlPageControlLeft Top� Width�Heightf
ActivePageTabSheet_textAlignalClientTabOrderOnChangePageControlChange 	TTabSheetTabSheet_textCaption
email-Text TMemoMemoLeft Top Width�HeightJTabStopAlignalClient
ScrollBars
ssVerticalTabOrder WantTabs	   	TTabSheetTabSheet_anhangCaptionAnhang
ImageIndex 	TRichEditRichEditLeft Top Width�Height� AlignalClientReadOnly	
ScrollBars
ssVerticalTabOrder     TOpenDialogOpenDialog1Left�Top8   