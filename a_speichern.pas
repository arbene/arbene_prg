unit a_speichern;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, ExtCtrls, Buttons;

type
  TForm_speichern = class(TForm)
    Panel_b: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Notebook: TNotebook;
    Label22: TLabel;
    Label23: TLabel;
    BitBtn_auswahl: TBitBtn;
    Edit_speichern: TEdit;
    Memo_speichern: TMemo;
    RadioGroup_berechtigung: TRadioGroup;
    procedure BitBtn_auswahlClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_speichern: TForm_speichern;

implementation

uses a_data, a_listwahl, a_main, a_auswertung;

{$R *.DFM}

procedure TForm_speichern.BitBtn_auswahlClick(Sender: TObject);
var
query:string;
begin
try
	 with datamodul do
	 begin
		 query:=format('select * from abfragen where berechtigung>=%d ',[form_auswertung.berechtigung]);
		 sql_new(true,q_2,query,'name');
		 form_liste:=tform_liste.create(self);
		 form_liste.DBgridEXT.PopupMenu:=nil;
		 form_liste.DBgridEXT.DataSource:=ds_2;
		 form_liste.s_name:='name';
		 form_liste.DBgridEXT.Columns[0].fieldname:='name';
		 if form_liste.showmodal<>mrok then exit;
		 edit_speichern.Text:=q_2['name'];
		 memo_speichern.Text:=q_2['memo'];
	 end;
finally
	form_liste.release;
end;
end;

procedure TForm_speichern.FormCreate(Sender: TObject);
begin
if prg_typ=2 then radiogroup_berechtigung.visible:=false;
form_main.form_positionieren(tform(sender));

end;

end.
