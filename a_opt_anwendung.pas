unit a_opt_anwendung;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ToolWin;

type
  TForm_optionen_anwendung = class(TForm)
    ToolBar1: TToolBar;
    ToolButton_cancel: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_quit: TToolButton;
	  PageControl1: TPageControl;
    TabSheet1: TTabSheet;
	  CheckBox_status: TCheckBox;
	  CheckBox_email: TCheckBox;
    CheckBox_archiv_show: TCheckBox;
	  CheckBox_arbmed_besch: TCheckBox;
    CheckBox_word_close: TCheckBox;
    CheckBox_rechnung: TCheckBox;
    TabSheet_abrechnung: TTabSheet;
    CheckBox_voso_ab: TCheckBox;
    CheckBox_bef_ab: TCheckBox;
    CheckBox_impf_ab: TCheckBox;
    CheckBox_lab_ab: TCheckBox;
    Label1: TLabel;
    CheckBox_jahr: TCheckBox;
    CheckBox_speichern_fragen: TCheckBox;
    procedure ToolButton_quitClick(Sender: TObject);
    procedure ToolButton_cancelClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_optionen_anwendung: TForm_optionen_anwendung;

implementation

{$R *.DFM}

procedure TForm_optionen_anwendung.ToolButton_quitClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_optionen_anwendung.ToolButton_cancelClick(Sender: TObject);
begin
modalresult:=mrabort;
end;

end.
