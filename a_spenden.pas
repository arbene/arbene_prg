unit a_spenden;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,jpeg,shellapi;

type
  TForm_spenden = class(TForm)
    Button_paypal: TButton;
    Edit: TEdit;
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    Label1: TLabel;
    RichEdit1: TRichEdit;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button_paypalClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_spenden: TForm_spenden;

implementation

uses a_main, a_richedit_menue;

{$R *.dfm}

procedure TForm_spenden.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
  if not ist_hauptsystem then edit.visible:=false;

end;

procedure TForm_spenden.Button_paypalClick(Sender: TObject);
var
html_name: string;
begin
  	html_name:='http://www.arbene.de/index.php/spenden';
	shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal);
end;

procedure TForm_spenden.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
 form_richedit_menue.Parent:=nil;
end;

end.
