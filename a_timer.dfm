object Form_timer: TForm_timer
  Left = 751
  Top = 246
  Width = 636
  Height = 374
  Caption = 'Timer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 208
    Top = 45
    Width = 22
    Height = 13
    Caption = 'Start'
  end
  object Label5: TLabel
    Left = 297
    Top = 45
    Width = 29
    Height = 13
    Caption = 'Dauer'
  end
  object SpeedButton_1: TSpeedButton
    Left = 120
    Top = 66
    Width = 41
    Height = 33
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333700000733333333F777773FF3333333007F0F70
      0333333773373377FF3333300FFF7FFF003333773F3333377FF33300F0FFFFF0
      F00337737333F37377F33707FFFF0FFFF70737F33337F33337FF300FFFFF0FFF
      FF00773F3337F333377F30707FFF0FFF70707F733337F333737F300FFFF09FFF
      FF0077F33377F33337733707FF0F9FFFF70737FF3737F33F37F33300F0FF9FF0
      F003377F7337F373773333300FFF9FFF00333377FF37F3377FF33300007F9F70
      000337777FF7FF77773333703070007030733373777777737333333333330333
      333333333337FF33333333333330003333333333337773333333}
    NumGlyphs = 2
    OnClick = SpeedButton_1Click
    OnDblClick = SpeedButton_1DblClick
  end
  object Label1: TLabel
    Left = 64
    Top = 71
    Width = 13
    Height = 25
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SpeedButton_2: TSpeedButton
    Left = 120
    Top = 122
    Width = 41
    Height = 33
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333700000733333333F777773FF3333333007F0F70
      0333333773373377FF3333300FFF7FFF003333773F3333377FF33300F0FFFFF0
      F00337737333F37377F33707FFFF0FFFF70737F33337F33337FF300FFFFF0FFF
      FF00773F3337F333377F30707FFF0FFF70707F733337F333737F300FFFF09FFF
      FF0077F33377F33337733707FF0F9FFFF70737FF3737F33F37F33300F0FF9FF0
      F003377F7337F373773333300FFF9FFF00333377FF37F3377FF33300007F9F70
      000337777FF7FF77773333703070007030733373777777737333333333330333
      333333333337FF33333333333330003333333333337773333333}
    NumGlyphs = 2
    OnClick = SpeedButton_2Click
  end
  object Label3: TLabel
    Left = 64
    Top = 127
    Width = 13
    Height = 25
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SpeedButton_3: TSpeedButton
    Left = 120
    Top = 178
    Width = 41
    Height = 33
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333700000733333333F777773FF3333333007F0F70
      0333333773373377FF3333300FFF7FFF003333773F3333377FF33300F0FFFFF0
      F00337737333F37377F33707FFFF0FFFF70737F33337F33337FF300FFFFF0FFF
      FF00773F3337F333377F30707FFF0FFF70707F733337F333737F300FFFF09FFF
      FF0077F33377F33337733707FF0F9FFFF70737FF3737F33F37F33300F0FF9FF0
      F003377F7337F373773333300FFF9FFF00333377FF37F3377FF33300007F9F70
      000337777FF7FF77773333703070007030733373777777737333333333330333
      333333333337FF33333333333330003333333333337773333333}
    NumGlyphs = 2
    OnClick = SpeedButton_3Click
  end
  object Label4: TLabel
    Left = 64
    Top = 183
    Width = 13
    Height = 25
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BitBtn_1: TBitBtn
    Left = 400
    Top = 74
    Width = 75
    Height = 25
    Caption = 'Zur'#252'cksetzen'
    TabOrder = 0
    OnClick = BitBtn_1Click
  end
  object Edit_start_1: TEdit
    Left = 192
    Top = 74
    Width = 73
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
  end
  object Edit_dauer_1: TEdit
    Left = 296
    Top = 74
    Width = 73
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
  end
  object BitBtn_2: TBitBtn
    Left = 400
    Top = 130
    Width = 75
    Height = 25
    Caption = 'Zur'#252'cksetzen'
    TabOrder = 3
    OnClick = BitBtn_2Click
  end
  object Edit_start_2: TEdit
    Left = 192
    Top = 130
    Width = 73
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
  end
  object Edit_dauer_2: TEdit
    Left = 296
    Top = 130
    Width = 73
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
  end
  object BitBtn_3: TBitBtn
    Left = 400
    Top = 186
    Width = 75
    Height = 25
    Caption = 'Zur'#252'cksetzen'
    TabOrder = 6
    OnClick = BitBtn_3Click
  end
  object Edit_start_3: TEdit
    Left = 192
    Top = 186
    Width = 73
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 7
  end
  object Edit_dauer_3: TEdit
    Left = 296
    Top = 186
    Width = 73
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 8
  end
  object BitBtn1: TBitBtn
    Left = 168
    Top = 288
    Width = 289
    Height = 25
    Caption = 'Schlie'#223'en (Timer l'#228'uft weiter)'
    ModalResult = 1
    TabOrder = 9
    Glyph.Data = {
      E6000000424DE60000000000000076000000280000000E0000000E0000000100
      0400000000007000000000000000000000001000000000000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3300333333333333330033333333333333003333333333333300333333333333
      330033333333333333003300000000003300330FFFFFFFF03300330000000000
      3300333333333333330033333333333333003333333333333300333333333333
      33003333333333333300}
  end
  object Timer1: TTimer
    Tag = 1
    Enabled = False
    OnTimer = Timer1Timer
    Left = 504
    Top = 74
  end
  object Timer3: TTimer
    Tag = 3
    Enabled = False
    OnTimer = Timer1Timer
    Left = 504
    Top = 186
  end
  object Timer2: TTimer
    Tag = 2
    Enabled = False
    OnTimer = Timer1Timer
    Left = 504
    Top = 130
  end
end
