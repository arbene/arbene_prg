unit a_repo_edit_m;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Edit_waehrung, Mask, Buttons, ExtCtrls, Spin;

type
  TForm_repo_edit_m = class(TForm)
    SpeedButton_besonderheiten: TSpeedButton;
    Label1: TLabel;
    Label4: TLabel;
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    MaskEdit_dat: TMaskEdit;
    Edit_faktor: TEdit_waehrung;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Label9: TLabel;
    ComboBox_ust: TComboBox;
    Label2: TLabel;
    procedure SpeedButton_besonderheitenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_repo_edit_m: TForm_repo_edit_m;

implementation

uses a_kalender, a_main;

{$R *.dfm}

procedure TForm_repo_edit_m.SpeedButton_besonderheitenClick(
  Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
      maskedit_dat.Text:=datetostr(int(form_kalender.auswahlkalender.date));
	 end;
end;

procedure TForm_repo_edit_m.FormCreate(Sender: TObject);
begin
combobox_ust.Items.Add('0');
combobox_ust.Items.Add(inttostr(mws_reduziert));
combobox_ust.Items.Add(inttostr(mws));
end;

end.
