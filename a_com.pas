unit a_com;

interface
uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons,DBCtrls, Mask, Menus, ComCtrls, ToolWin, dbtables, a_data,
	db, olectnrs,ComObj, DdeMan, listen,a_auswertung,math,a_nachricht,
	excel2000,activex,{pdflib_tlb,}ZDataset,a_vonbis,variants;
type
	tcom= class(tobject) // kommunikation mit dem oleobjekt

	private
		serienbrief:boolean;
		dokutext:string;
    storys: array[1..4] of integer;

		//bookmark_select:string;
		procedure sereinbriefdatei_schreiben(datei:string;modus:integer);
		//function strausdatei(tabelle:tdataset; feldname:string): string;
    function strausdatei(tabelle:tzquery; feldname:string): string;
		function xl_zelle(y,x:integer):string;
	public
		xla: excel2000._application;
		xlw: excel2000._workbook;
    datum_dok_erstellt:string;
     //pdf: pdflib_tlb.tPdf;
		//ch:_chart;

		unknown:Iunknown;
		lcid:integer;
		wobjekt,eobjekt,o,ch,rnge,pdfobjekt: olevariant;
		wobjekt_typ, wobjekt_name: string;
		bookmark_list,serienbrief_list,bookmark_list_all,benutzer_id_liste,benutzer_liste,computer_liste: tstringlist;
    bookmarklisten: array[1..4] of tstringlist;
		constructor create;
		destructor free;
		procedure olemakro(makro:integer);
     procedure laboreinfuegen;
     procedure labor_einfuegen_werte;
     procedure labor_einfuegen_hinweise;
     procedure diagnosen_einfuegen;
     procedure stempel_einfuegen;
     procedure arbeitsberichteinfuegen;
     procedure datum_erstellt_einfuegen;
     procedure uil_einfuegen(uil:integer;von,bis: tdate);
     function suche_word_bookmarks:tstringlist;
     procedure kapitel_memo(bm,dok:string;zquery:tzquery;feld:string);
     function kapitel_boukmarks(i_mitarbeiter:int64):integer;
     procedure kapitel_anamnese(bm,dok:string;i_mitarbeiter:int64);
     procedure kapitel_befunde(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
     procedure kapitel_labor(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
     procedure kapitel_diagnosen(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
     procedure kapitel_tabelle(tabelle,bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
     procedure kapitel_besonderheit(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
     procedure kapitel_fuege_ein_dat(bm,dok,datname:string);
     procedure kapitel_fuege_ein_string(bm,dok,t:string);
     procedure vormarke(mark:string);
     procedure erstelle_kapitel(bm,dok:string;i_mitarbeiter:int64; von, bis:tdate);
     function  zeilen_loeschen(zeichen:string):integer;
     function  zeile_loeschen(zeichen:string):integer;
		procedure dateieinfuegen(datei: string);
		procedure graphikeinfuegen(datei:string);
		procedure texteinfuegen(text:string;absatz:boolean);
     procedure tabelle_neu(spalten:integer);
		procedure bookmark_einfuegen(bookmark,text:string;all:boolean);
    procedure bookmark_einfuegen_datei(bookmark,text,datei:string;all:boolean);
    procedure bookmark_einfuegen_alsdatei(bookmark,text:string;all:boolean);
		function  wordobjekterstellen(olecontainer:tolecontainer): boolean;

		procedure bookmarks_lesen;
     procedure bookmarks_lesen_serienbrief;
     procedure bookmarks_lesen_ersetzen;
		//procedure bookmarks_einfuegen(firma,mitarbeiter:integer; const platzhalter,variable:array of variant);
		procedure bookmarks_einfuegen_tabelle(tab_name: string; nummer:int64;modus:integer);
		procedure dokutextlesen;
		procedure doku_drucken;
    procedure doku_drucken_pdf;
		procedure doku_drucken_seite;
		procedure doku_schliessen;
		procedure word_schliessen;
		procedure doku_speichern(datei:string);
		function  doku_txt:string;
		procedure  doku_memo(var memo: tmemo);
		procedure neuer_text;
		procedure schrift(fName:string; fSize:integer; fBold:boolean);
		function  query_create(liste:tstringlist;sql_where,jointab:string):string;
		procedure export_excel(query:string);

		function  xl_connect: boolean;
      function excel_connect(name:string):boolean;
      function excel_addworkbook(wbname:string):boolean;
		function   xl_addworkbook:boolean;
		procedure xl_disconnect;
		procedure xl_anzeigen;
     procedure excel_PutStrAt(row,column:integer;s:string);
     procedure excel_PutintAt(row,column:integer;i:integer);
		procedure excel_PutextAt(row,column:integer;f:real);
                
		function  excel_getdataAt(row,column:integer):string;
      procedure exel_optimale_hoehe_breite;
     	procedure excel_disconnect;
		procedure xl_PutStrAt(y,x:integer;s:string);

		procedure xl_PutintAt(y,x:integer;i:integer);
		procedure xl_PutextAt(y,x:integer;f:real);
		function  xl_getdataAt(y,x:integer):string;

		procedure chart_add_serie(xvalues,values:variant;namen:string);
		procedure chart_erzeugen(Titel:string);
		function audio_graph(q:tzquery):integer;

     procedure pdf_disconnect;
     function pdf_connect: boolean;
     procedure pdf_ladedatei(datname:widestring);
     procedure word_sichtbar;
     procedure word_unsichtbar;
     procedure excel_vordergrund;

     procedure pdf_objekt_erstellen(container:tolecontainer);
     procedure pdf_objekt_speichern(container:tolecontainer;pfad:string);
     function SearchAndReplaceInStory(rngStory :olevariant; strSearch: String ; strReplace : String;rp:integer ):integer;
end;

  var
	memo:tmemo;

implementation
uses a_main,a_report,a_checkbox;

constructor tcom.create;

begin
	inherited create;
  

	bookmark_list:=tstringlist.create;
	bookmark_list_all:=tstringlist.create;
	serienbrief_list:=tstringlist.create;
	benutzer_id_liste:=tstringlist.create;
	benutzer_liste:=tstringlist.create;
	computer_liste:=tstringlist.create;
  bookmarklisten[1]:=tstringlist.create;
  bookmarklisten[2]:=tstringlist.create;
  bookmarklisten[3]:=tstringlist.create;
  bookmarklisten[4]:=tstringlist.create;
  storys[1]:=1;    //hautpt
  storys[2]:=7;    //kopr
  storys[3]:=9;    //fuss
  storys[4]:=5;    //textfeld
  wobjekt:=null;
  eobjekt:=null;
  o:=null;
  ch:=null;
  rnge:=null;
  pdfobjekt:=null;

  {with datamodul do
  begin
  q_bookmarks.First;
	bookmark_select:= q_bookmarks['feld_def'];
	q_bookmarks.Next;
  while not q_bookmarks.eof do
  begin
  	if q_bookmarks['feld_def']<>null then bookmark_select:=bookmark_select+' , ' +q_bookmarks['feld_def'];
     q_bookmarks.Next;
  end;
	end;}
end;

destructor tcom.free;
begin

	bookmark_list.Clear;
	bookmark_list.Free;
	bookmark_list_all.Free;
  bookmarklisten[1].free;
  bookmarklisten[2].free;
  bookmarklisten[3].free;
  bookmarklisten[4].free;
	serienbrief_list.clear;
	serienbrief_list.free;
	benutzer_liste.Free;
	benutzer_id_liste.free;
	computer_liste.free;
	wobjekt:=Unassigned;
	eobjekt:=Unassigned;
  pdfobjekt:=unassigned;
end;





function tcom.xl_zelle(y,x:integer):string;
var
	r,f:integer;
	a:string;
begin
		x:=x-1; //dann a1=1,1
		if (y<0) or (x<0) then
		begin
		result:='A1';
		exit;
		end;
		r:=x mod 26;
		f:=x div 26;
		if f>0 then a:=chr(f+65) else a:='';
		result:=a+chr(r+65)+inttostr(y);
end;

procedure tcom.xl_disconnect;
begin
xla:=nil;
xlw:=nil;
end;


procedure tcom.excel_disconnect;
begin
     excel:=unassigned;
end;

function tcom.xl_connect: boolean;
var
	  res:hresult;
begin
	try
	  res:=getactiveobject(excel2000.CLASS_ExcelApplication,nil,unknown);
	  if res=mk_e_unavailable then
	  begin

    xla:=excel2000.CoExcelApplication.create;
	  lcid:=getuserdefaultlcid;

	  end
	  else
	  begin
		  olecheck(res);
		  olecheck(unknown.queryinterface(_application,xla));
	  end;
	except
		showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
	end;
	//xla.Visible[lcid]:=true;
	//xlw:=xla.Workbooks.Add(xlwbatworksheet,lcid);
	//rnge:=xla.range['A1','A2'];

end;




procedure tcom.xl_anzeigen;
begin
xla.Visible[lcid]:=true;
end;

function tcom.xl_addworkbook:boolean;
begin
xlw:=xla.Workbooks.Add(xlwbatworksheet,lcid);
rnge:=xla.range['A1','A2'];
end;


//################################################ PDF

procedure tcom.pdf_disconnect;
begin
//pdf:=nil;
end;


function tcom.pdf_connect: boolean;
var
	  res:hresult;
begin
	//pdf:=pdflib_tlb.TPdf.Create(tcomponent(form_main));
end;

procedure tcom.pdf_ladedatei(datname:widestring);
begin
    // pdf.LoadFile( datname);
end;


procedure tcom.pdf_objekt_erstellen(container:tolecontainer);
var
verbs:tstrings;
i:integer;
begin
  pdfobjekt:=Unassigned; //null

  verbs:=container.objectverbs;// .OleObject.
  i:=verbs.Count;
  //for i:=0 to i-1 do showmessage(verbs[i]);
  container.DoVerb(0);
	pdfobjekt:=container.OleObject;
end;

procedure tcom.pdf_objekt_speichern(container:tolecontainer;pfad:string);
var
pdf_dok:olevariant;
begin
   pdf_objekt_erstellen(container);

   pdf_dok:=pdfobjekt.getActiveDoc;
   pdf_dok.save(1,'c:\temp\test11.pdf');
end;

//################################################ WORD
function tcom.wordobjekterstellen(olecontainer:tolecontainer): boolean;   //Aufruf noch unabh�ngig von der Anwedung   also auch openoffice
var
	olecontainer_typ: string;
begin
	result:=true;

	if lowercase(copy(OLeContainer.oleclassname,1,13))<>'word.document' then        //hier dann trennung openoffice
	begin
		result:=false;
		exit;
	end;

try
 if lowercase(copy( OLeContainer.oleclassname,1,13))='word.document' then  olecontainer_typ:='word8';  //auch f�r   Word.Document.9, 10 etc
 //if OLeContainer.oleclassname='Word.Document.8' then olecontainer_typ:='word8';
 if OLeContainer.oleclassname='Word.Document.6' then
 begin
	olecontainer_typ:='word6';
	showmessage('Sie ben�tigen mindestens Word 97 (Word8)');
 end;

except

end;
	wobjekt_typ:='';
	wobjekt_name:='';

try
	if olecontainer_typ='word8' then
	begin
		  wobjekt:=Unassigned; //null
		  wobjekt:=olecontainer.OleObject.application;
		  wobjekt_name:=wobjekt.activedocument.name;
		  wobjekt_typ:='word8';
	end;

	except
	  //application.bringtofront;
     arbene_vor;
	  Messagedlg('Konnte kein Wordobjekt erstellen. Bitte alle Word-Anwendungen schlie�en.+#10'+SysErrorMessage(GetLastError) ,mtinformation, [mbOK],0);
	  result:=false;
	end;
    //ShowMessage(SysErrorMessage(GetLastError));
end;




procedure tcom.neuer_text ;
begin
if wobjekt_typ='word8' then
		begin
		  wobjekt.documents.add;
	 end;
end;


function tcom.suche_word_bookmarks:tstringlist;
var
c,i:integer;
bn:string;
begin
    result:=tstringlist.create;
    bn:=wobjekt.ActiveDocument.name;
    result.Add(bn);
    c:=wobjekt.ActiveDocument.Bookmarks.count;
    for i:=1 to c do
    begin
         bn:=wobjekt.ActiveDocument.Bookmarks.item(i).name;
         bn:=lowercase(bn);
         result.add(bn);
    end;
end;

function tcom.kapitel_boukmarks(i_mitarbeiter:int64):integer;
var
liste: tstringlist;
i:integer;
dat_von, dat_bis:tdate;
begin
liste:=suche_word_bookmarks;
 //liste[0]: dokumame
if liste.Count>1 then
begin


	try
		form_datumvonbis:=tform_datumvonbis.create(form_main );

		form_datumvonbis.MaskEdit_von.text:=datetostr(form_main.dbtreeview.first_date);
		form_datumvonbis.MaskEdit_bis.text:=datetostr(now);
		//application.BringToFront;
      arbene_vor;
		if form_datumvonbis.ShowModal<>mrok then exit;

		 dat_von:=strtodate(form_datumvonbis.MaskEdit_von.text);
		 dat_bis:=strtodate(form_datumvonbis.MaskEdit_bis.text);
	finally

		form_datumvonbis.Release;
	end;

end;

for i:=1 to liste.count-1 do
begin
     erstelle_kapitel(liste[i],liste[0], i_mitarbeiter,dat_von,dat_bis);
end;

result:=liste.count;
liste.Free;
end;


procedure tcom.erstelle_kapitel(bm,dok:string;i_mitarbeiter:int64;von,bis:tdate);
//durchsuche dokumente nach bm
//befunde, labor, diagnosen eigene routine zur formatierung
//bm:bookmark, dok: word-dokumentennahme
begin
if bm='anamnese' then kapitel_anamnese(bm,dok,i_mitarbeiter);
if bm='labor' then kapitel_labor(bm,dok,i_mitarbeiter,von,bis);
if bm='diagnosen' then kapitel_diagnosen(bm,dok,i_mitarbeiter,von,bis);

kapitel_besonderheit(bm,dok,i_mitarbeiter,von,bis);
kapitel_tabelle('dokumente',bm,dok,i_mitarbeiter,von,bis);
kapitel_befunde(bm,dok,i_mitarbeiter,von,bis);



 { if pos(bm,'befunde_aufnahme befunde_enlassung labor diagnosen') >0 then
  begin
    if bm='anamnese' then kapitel_anamnese(bm,dok,i_mitarbeiter);
    if bm='aufnahmebefund' then kapitel_befunde(bm,dok,i_mitarbeiter,von,bis);
    if bm='entlassbefund' then kapitel_befunde(bm,dok,i_mitarbeiter,von,bis);
    if bm='zwischenbefund' then kapitel_befunde(bm,dok,i_mitarbeiter,von,bis);
    if bm='labor' then kapitel_labor(bm,dok,i_mitarbeiter,von,bis);
    if bm='diagnosen' then kapitel_diagnosen(bm,dok,i_mitarbeiter,von,bis);
  end
  else
      begin
           kapitel_tabelle('dokumente',bm,dok,i_mitarbeiter,von,bis);
           kapitel_besonderheit(bm,dok,i_mitarbeiter,von,bis);
      end;}
end;


procedure tcom.kapitel_memo(bm,dok:string;zquery:tzquery;feld:string);
var
dat:string;
begin
     if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.1', [ pfad_temp]));
     dat:=pfad_temp+'insert.doc';
     form_main.strings_laden(zquery,feld,form_main.richedit.Lines);
     if form_main.richedit.Lines.CommaText<>'' then
     begin
       form_main.richedit.Lines.SaveToFile(dat);
       kapitel_fuege_ein_dat(bm,dok,dat);
     end;
end;


procedure tcom.kapitel_anamnese(bm,dok:string;i_mitarbeiter:int64);
var
query:string;
begin
with datamodul do
begin
  //lade dokumente

  query:=format('select * from anamnese where nummer=%s ',[inttostr(i_mitarbeiter)]);
  sql_new(false,q_5,query,'');
  q_5.First;
  while not q_5.eof do
  begin
    try
      kapitel_memo(bm,dok,q_5,'memo');
    except
    end;
    q_5.Next;
  end;
end;
end;

procedure tcom.kapitel_befunde(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
//liste befundem
//liste akt_untersuchung in richtiger Reihenfolge
//f�ge befunde ein sowie word-text
var
query,dat,sql_datum_von, sql_datum_bis,sbef_nr:string;
modus:integer;
s,s1,s2,s3,s4,s5,s6,s7,s8,sl,sr:string;
r1,r2,r3,r4,r5:real;
i1,i2,i3,i:integer;

procedure  befund_drucken;
begin
  s1:=datamodul.q_5.fieldbyname('datum').asstring;
  s:=format( 'Befunde vom %s: ',[s1]);
  kapitel_fuege_ein_string(bm,dok,s);
end;

procedure memo_drucken;
begin

     form_main.strings_laden(datamodul.q_6,'memo',form_main.richedit.Lines);
     if form_main.richedit.Lines.CommaText<>'' then
     begin
       form_main.richedit.Lines.SaveToFile(dat);
       kapitel_fuege_ein_dat(bm,dok,dat);
     end;
end;

procedure anmerkung_drucken;
begin
     i1:=datamodul.q_6['auffaellig'];
	   s:=format( '%s: ',[s3]);
     kapitel_fuege_ein_string(bm,dok,s);
     memo_drucken
end;

procedure wert_drucken;
begin
with datamodul do
begin
	if varisnull(q_6['bezeichner']) then 	s1:='' else    s1:=(q_6['bezeichner']);
	if varisnull(q_6['bezeichner_dim']) then 	s2:='' else s2:=(q_6['bezeichner_dim']);
	r1:=q_6['wert'];
	i1:=q_6['auffaellig'];
	s:=format( '%s: %s  %s %s',[s3,s1,floattostr(r1),s2]);
  kapitel_fuege_ein_string(bm,dok,s);
	memo_drucken;
end;
end;

procedure liste_drucken;
begin
with datamodul do
begin
	if varisnull(q_6['bezeichner']) then 	s1:='' else    s1:=(q_6['bezeichner']);
	if varisnull(q_6['i_berlist']) then 	s2:='' else
		begin
			 i:=q_6['i_berlist'];
			 query:=format('select listenfeld from berlist where nummer=%d',[i]);
			 sl:= mysql_d.Feldinhalt(query,0);
 			 i:=q_6['i_berlist_rechts'];
				query:=format('select listenfeld from berlist where nummer=%d',[i]);
			 sr:= mysql_d.Feldinhalt(query,0);
		end;
	i1:=q_6['auffaellig'];

	if q_6['reli']=0 then
	  begin
		s:=format( '%s: %s  %s',[s3,s1,sl]);
	  end
	else
	begin
		s:=format( '%s LINKS: %s  %s    RECHTS: %s',[s3,s1,sl,sr]);
	end;

  kapitel_fuege_ein_string(bm,dok,s);
	memo_drucken;


end;
end;

procedure audiogramm_drucken;
begin
end;


procedure  sehtest_drucken;
begin
end;

procedure  rr_drucken;
begin
with datamodul do
begin
	if varisnull(q_6['i_1']) then 	i1:=0 else    i1:=(q_6['i_1']);
	if varisnull(q_6['i_2']) then 	i2:=0 else i2:=(q_6['i_2']);
	if varisnull(q_6['i_3']) then 	i3:=0 else i3:=(q_6['i_3']);

	s:=format('%s: RR: %d / %d mmHg Puls: %d', [s3,i1,i2,i3]);
  kapitel_fuege_ein_string(bm,dok,s);
	memo_drucken;
end;
end;

procedure  bmi_drucken;
begin
with datamodul do
begin
	if varisnull(q_6['f_1']) then 	r1:=0 else r1:=(q_6['f_1']);
	if varisnull(q_6['f_2']) then 	r2:=0 else r2:=(q_6['f_2']);
	if varisnull(q_6['f_3']) then 	r3:=0 else r3:=(q_6['f_3']);
    if varisnull(q_6['f_4']) then 	r4:=0 else r4:=(q_6['f_4']);
    if varisnull(q_6['f_5']) then 	r5:=0 else r5:=(q_6['f_5']);

	//s:=format('%s: Gr��e: %8.1f cm Gewicht: %8.1f kg Bauchumfang: %8.1f  WHtR: %8.1f BMI: %8.1f kg/m�', [s3,r1,r4,r2,r5,r3]);
    s:=format('%s: Gr��e: %8.1f cm, Gewicht: %8.1f kg, Bauchumfang: %8.1f,  WHtR: %8.1f, BMI: %8.1f kg/m�', [s3,r1,r2,r4,r5,r3]);
  kapitel_fuege_ein_string(bm,dok,s);
	memo_drucken;
end;
end;

procedure   Lufu_drucken;
begin
end;

procedure   wai_drucken;
begin
with datamodul do
begin
	r1:=q_6['wert'];
	i1:=q_6['auffaellig'];
	s:=format( 'Work Ability Index: %s  ',[floattostr(r1)]);
  kapitel_fuege_ein_string(bm,dok,s);
	memo_drucken;
end;
end;

//###################################################################################
begin
with datamodul do
begin
  //lade dokumente
  wobjekt.documents.item(dok).activate;
  vormarke(bm);
  sql_datum_von:=  dat_delphi_to_sql(datetostr(dat_von));
  sql_datum_bis:=  dat_delphi_to_sql(datetostr(dat_bis));
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.2', [ pfad_temp]));
  dat:=pfad_temp+'insert.doc';
  query:=format('select * from befunde where i_mitarbeiter=%s and lower(kapitel)="%s" and datum>="%s" and datum<="%s" order by datum',[inttostr(i_mitarbeiter),bm,sql_datum_von,sql_datum_bis]);
  sql_new(true,q_5,query,'');
  q_5.First;
  while not q_5.eof do
  begin
       sbef_nr:=getbigint_str(q_5,'nummer');
       s:=  'select akt_untersuchung.*, bereich.name , bereich.reihenfolge, bereich.modus , bereich.bezeichner, bereich.bezeichner_dim , bereich.reli';
       query:=s+format(' from akt_untersuchung  left join bereich on (akt_untersuchung.i_bereich=bereich.nummer) where (akt_untersuchung.i_untersuchung=%s and akt_untersuchung.storno=0) order by bereich.reihenfolge',[sbef_nr]);
       sql_new(false,q_6,query,'');
       q_6.first;
       befund_drucken;
       while not q_6.eof do
       begin
        s3:=q_6.fieldbyname('name').asstring;
        if q_6['modus'] <>null then modus:=q_6['modus'] else modus:=0;
			  case modus of
			  0:begin
					//if q_1['i_bereich']>0 then
					   anmerkung_drucken;
				 end;
			  1:	begin  //Zahl
					  wert_drucken;
					end;
			  2:begin  //liste
					 liste_drucken;
				  end;
				3: begin    //audiogramm
              audiogramm_drucken;
				  end;
				4: begin //sehtest
				  sehtest_drucken;
				  end;
				 5:begin
					 rr_drucken
					end;
				 6: begin //gr�sse gewicht
					  bmi_drucken;
					 end;
				 7: begin //lufu
						Lufu_drucken;
					 end;
            8: wai_drucken;
            end;
          q_6.next;
       end;
      q_5.next;
  end;
end;
end;



procedure tcom.kapitel_labor(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
var
sql_datum_von, sql_datum_bis,query,s, auffaellig, unauffaellig,z, untersuchung,einheit,
ergebnistext,grenzwertindikator,norm:string;
dat_zuletzt:tdate;
wert:real;

procedure schreibe_lab_ueberschrift(datum:tdate);
//z.b. Labor vom...
begin
	   s:=format( 'Labor vom %s: ',[datetostr(datum)]);
     kapitel_fuege_ein_string(bm,dok,s);
end;

procedure schreibe_lab_auffaellig;
begin
    auffaellig:='Auff�llig: '+auffaellig;
    kapitel_fuege_ein_string(bm,dok,auffaellig);
end;

procedure schreibe_lab_unauffaellig;
begin
   unauffaellig:='Unauff�llig: '+unauffaellig;
   kapitel_fuege_ein_string(bm,dok,unauffaellig);
end;

function lab_unauffaellig(wert:real; norm,gi:string):boolean;
var
klein, gross:real;
p:integer;
begin
	norm:=stringreplace(norm,'.', ',',[rfreplaceall]);
  if pos('+',gi)>0 then
  begin
       result:=false;
       exit;
  end;
	norm:=lowercase(trim(norm));
	p:=pos(chr(13),norm);
	if p>0 then norm:=copy(norm,1,p-1);
	p:=pos('-', norm);
  if p >0 then
  begin
	  klein:=strtofloat(trim(copy(norm,1,p-1)));
    gross:=strtofloat(trim(copy(norm,p+1,length(norm))));
  end;

  p:=pos('>', norm);
  if p >0 then
  begin
    klein:=strtofloat(copy(norm,p+1,length(norm)));
    gross:=99999999999999999;
  end;

  p:=pos('<', norm);
  if p >0 then
  begin
    gross:=strtofloat(copy(norm,p+1,length(norm)));
    klein:=0;
  end;
  p:=pos('p', norm);
  if p >0 then
	begin
    klein:=0.00001;
    gross:=999999999999999;
  end;
  p:=pos('n', norm);
  if p >0 then
  begin
       klein:=0;
       gross:=0;
  end;

  result:= ((klein<=wert) and (wert<=gross));

end;


//##############################################################################
begin
//liste labor auf
//nach Datum Normalbereich:........, auff�llig:......................
//
with datamodul do
begin
  wobjekt.documents.item(dok).activate;
  vormarke(bm);
  sql_datum_von:=  dat_delphi_to_sql(datetostr(dat_von));
  sql_datum_bis:=  dat_delphi_to_sql(datetostr(dat_bis));
  query:=format('select labor.*, typ.untersuchung, typ.reihenfolge from labor left join typ on(typ.nummer=labor.i_typ) where labor.storno=0 and i_mitarbeiter=%s and datum>="%s" and datum<="%s" order by datum, reihenfolge',[inttostr(i_mitarbeiter),sql_datum_von,sql_datum_bis]);
  sql_new(false,q_5,query,'');
  q_5.First;
  dat_zuletzt:=q_5.FindField('datum').asdatetime;
  while not q_5.eof do
  begin
    untersuchung:=q_5.FindField('untersuchung').asstring;
    wert:=q_5.FindField('wert').asfloat;
	  einheit:=q_5.FindField('einheit').asstring;
    grenzwertindikator:=q_5.FindField('grenzwertindikator').asstring;
    ergebnistext:=trim(q_5.FindField('ergebnistext').asstring);
    norm:=q_5.FindField('norm').asstring;
    if (einheit='+-') or (einheit='') then  //keine Einheit--> nur ergebnistext
       z:=format(', %s: %s',[untersuchung, ergebnistext])
    else
			z:=format(', %s: %s %s [Norm: %s] %s',[untersuchung,floattostr(wert),einheit, norm, ergebnistext]);

			z:= stringreplace(z,#$D,' ',[rfreplaceall]);
			z:= stringreplace(z,#$A,' ',[rfreplaceall]);
			z:= stringreplace(z,'  ',' ',[rfreplaceall]);
			
	  if lab_unauffaellig(wert, norm,grenzwertindikator) then
	  unauffaellig:=unauffaellig+z else auffaellig:=auffaellig+z;

    q_5.next;
    if (dat_zuletzt<>q_5.FindField('datum').asdatetime) or q_5.eof  then
    begin
         dat_zuletzt:=q_5.FindField('datum').asdatetime;
         schreibe_lab_ueberschrift(dat_zuletzt);
         schreibe_lab_auffaellig;
         schreibe_lab_unauffaellig;
         auffaellig:='';
         unauffaellig:='';
    end;

  end;
end;
end;



procedure tcom.kapitel_diagnosen(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
var
sql_datum_von, sql_datum_bis,query,s,icd_text, icd_nr,sternnummer,zusatznummer:string;

begin
with datamodul do
begin
  wobjekt.documents.item(dok).activate;
  vormarke(bm);
  sql_datum_von:=  dat_delphi_to_sql(datetostr(dat_von));
  sql_datum_bis:=  dat_delphi_to_sql(datetostr(dat_bis));
  query:=format('select * from diagnosen where storno=0 and i_mitarbeiter=%s and datum>="%s" and datum<="%s" order by icd',[inttostr(i_mitarbeiter),sql_datum_von,sql_datum_bis]);
  sql_new(false,q_5,query,'');
  q_5.First;
  while not q_5.eof do
  begin
    icd_text:=trim(q_5.findfield('icd_text').asstring);
    icd_nr:=trim(q_5.findfield('icd').asstring);
    sternnummer:=trim(q_5.findfield('sternnummer').asstring);
    zusatznummer:=trim(q_5.findfield('zusatznummer').asstring);
    s:=format('%s [%s %s %s]',[icd_text, icd_nr,sternnummer,zusatznummer]);
    kapitel_fuege_ein_string(bm,dok,s);
    q_5.next;
  end;
end;
end;


procedure tcom.kapitel_tabelle(tabelle,bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
var
query,dat,sql_datum_von, sql_datum_bis:string;
begin
with datamodul do
begin
  //lade dokumente
  sql_datum_von:=  dat_delphi_to_sql(datetostr(dat_von));
  sql_datum_bis:=  dat_delphi_to_sql(datetostr(dat_bis));
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.3', [ pfad_temp]));
	dat:=pfad_temp+'insert.doc';


	query:=format('select * from %s left join kapitel on(%0:s.i_kapitel = kapitel.nummer) where i_mitarbeiter=%s and lower(kapitel.kapitel)="%s" and datum>="%s" and datum<="%s" and %0:s.storno=0 order by datum',[tabelle,inttostr(i_mitarbeiter),bm,sql_datum_von,sql_datum_bis]);
	sql_new(false,q_5,query,'');
  q_5.First;
  while not q_5.eof do
  begin
    try
			  form_main.olecontainer_laden(q_5,'ole',form_main.Arbeitsmedizin);
			  form_main.Arbeitsmedizin.DoVerb(0);
			  com.zeilen_loeschen('//') ;
			  doku_speichern(dat);
			  form_main.Arbeitsmedizin.Close;
			  kapitel_fuege_ein_dat(bm,dok,dat);
	  except
	  	//z.b leerer olecontainer
	  end;
    q_5.Next;
  end;
end;
end;


procedure tcom.kapitel_besonderheit(bm,dok:string;i_mitarbeiter:int64;dat_von,dat_bis:tdate);
var
query,dat,sql_datum_von, sql_datum_bis,s,d,d_bis,besonderheit:string;
datum:tdate;
begin
with datamodul do
begin
  //lade dokumente
  sql_datum_von:=  dat_delphi_to_sql(datetostr(dat_von));
  sql_datum_bis:=  dat_delphi_to_sql(datetostr(dat_bis));
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.4', [ pfad_temp]));
  dat:=pfad_temp+'insert.doc';
  query:='select besonderheiten.*, besonderheitenliste.besonderheit from besonderheiten left join besonderheitenliste on (besonderheitenliste.nummer=besonderheiten.i_besonderheit)';
  query:=query+format(' where i_mitarbeiter=%s and lower(kapitel)="%s" and datum>="%s" and datum<="%s" order by datum',[inttostr(i_mitarbeiter),bm,sql_datum_von,sql_datum_bis]);
  sql_new(false,q_5,query,'');
  q_5.First;
  while not q_5.eof do
  begin
    try
      datum:=q_5.findfield('datum').asdatetime;
      d:=datetostr(datum);
      datum:=q_5.findfield('datum_bis').asdatetime;
      d_bis:=datetostr(datum);
      besonderheit:=q_5.findfield('besonderheit').asstring;
      s:=format(' %s vom %s',[besonderheit,d]);
      if d_bis<>'31.12.2999'then s:=s+ format(' bis %s ',[d_bis]);
      kapitel_fuege_ein_string(bm,dok,s);
      kapitel_memo(bm,dok,q_5,'memo');
    except
    end;
    q_5.Next;
  end;
end;
end;


procedure tcom.kapitel_fuege_ein_string(bm,dok,t:string);
begin
     wobjekt.documents.item(dok).activate;
     vormarke(bm);
		texteinfuegen(t,true);
end;

procedure tcom.kapitel_fuege_ein_dat(bm,dok,datname:string);
begin
     wobjekt.documents.item(dok).activate;
     vormarke(bm);
     dateieinfuegen(datname);
end;


procedure tcom.vormarke(mark:string);
begin
try
    wobjekt.Selection.GoTo( What:=-1{wdGoToBookmark}, Name:=mark);
    wobjekt.ActiveDocument.Bookmarks.DefaultSorting := 1;
    wobjekt.ActiveDocument.Bookmarks.ShowHidden := True;
    wobjekt.Selection.MoveLeft( Unit:=1{wdCharacter}, Count:=1);
except
end;    
end;


function tcom.zeilen_loeschen(zeichen:string):integer;
var r:integer;
begin
	r:=1;
	while r<>0 do
	begin
	  r:=zeile_loeschen(zeichen);
	  inc(result);
	end;
end;

function tcom.zeile_loeschen(zeichen:string):integer;
begin
	  wobjekt.Selection.Find.ClearFormatting;
	  wobjekt.Selection.Find.Text :=zeichen;
	  wobjekt.Selection.Find.Replacement.Text := '';
	  wobjekt.Selection.Find.Forward := True;
	  wobjekt.Selection.Find.Wrap := 1;
	  wobjekt.Selection.Find.Format := False;
	  wobjekt.Selection.Find.MatchCase := False;
	  wobjekt.Selection.Find.MatchWholeWord := true;
	  wobjekt.Selection.Find.MatchWildcards := False;
	  wobjekt.Selection.Find.MatchSoundsLike := False;
	  wobjekt.Selection.Find.MatchAllWordForms := False;
    result:=wobjekt.Selection.Find.Execute;//2
    if result<>0 then
    begin
     wobjekt.Selection.HomeKey( Unit:=5);
     wobjekt.Selection.EndKey ( Unit:=5, Extend:=1);
     wobjekt.Selection.Delete (Unit:=1, Count:=1);
    end;
end;


procedure tcom.dateieinfuegen(datei: string);
begin
//if wobjekt_typ='word8' then
//		begin
try
		 // wobjekt.documents.item(wobjekt_name).activate;
     // wobjekt.Selection.InsertParagraphAfter;// TypeParagraph;
      //wobjekt.Selection.Collapse( Direction:=0);
		  wobjekt.Selection.InsertFile(FileName:=datei, Range:='', ConfirmConversions:= False, Link:=False, Attachment:=False);
except
      showmessage(format('Fehler beim Einf�gen der folgenden Datei: %s',[datei]));
end;

//	 end;
end;


procedure tcom.texteinfuegen(text:string;absatz:boolean);
var t:string;
begin
try
	 wobjekt.documents.item(wobjekt_name).activate;
    //wobjekt.application.activate;
	 if absatz then
	 begin
		wobjekt.selection.endkey;
		wobjekt.selection.typeparagraph;
	 end;
	 t:=copy(text,1,100);
	 while t<>'' do
	 begin
			wobjekt.selection.typetext(t);
			text:=copy(text,101,length(text));
			t:=copy(text,1,100);
	 end;
	 if absatz then wobjekt.selection.moveright;
    //wobjekt.application.activate;
except
	showmessage('Einf�gen ist nicht m�glich - es ist kein Word-Dokument ge�ffnet');
end;
end;

procedure tcom.tabelle_neu(spalten:integer);
begin

end;


procedure tcom.graphikeinfuegen(datei:string);
var
s:variant;
h,f:real;
begin
try
	 wobjekt.documents.item(wobjekt_name).activate;
	 wobjekt.selection.endkey;
	 s:=wobjekt.ActiveDocument.InlineShapes.AddPicture(fileName:=datei,SaveWithDocument:=true,range:=wobjekt.selection.range);
	 h:= s.height;
	 if h>200 then
	 begin
		 f:=200 / h;
		 s.width:=s.width * f;
		 s.height:=200;
	 end;
	 wobjekt.selection.moveright;
	 wobjekt.selection.typeparagraph;
except
showmessage('Fehler beim Graphikeinf�gen');
end;
end;

procedure tcom.bookmark_einfuegen_alsdatei(bookmark,text:string;all:boolean);
var
r: thtml_report;
f_name:string;
begin
try
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.5', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
  r:=thtml_report.create(f_name);
  r.text(text);
finally
  r.free;
end;
  bookmark_einfuegen_datei(bookmark,'',f_name,false);  //leerer text
end;

procedure tcom.bookmark_einfuegen_datei(bookmark,text,datei:string;all:boolean);
var
position,wansicht:integer;

procedure suchersetz(alt,text:string;alle:boolean);
var tf,j,z,a:integer;
rf: array[1..4] of integer;
t2:string;
Search_Rng: OleVariant;
begin
    rf[1]:=2;
    rf[2]:=3;
    rf[3]:=4;
    rf[4]:=1;
    t2:= '#'+alt+'#';
		wobjekt.Selection.Find.ClearFormatting;
	  wobjekt.Selection.Find.Replacement.ClearFormatting;
	  wobjekt.Selection.Find.Text :=t2;
	  wobjekt.Selection.Find.Replacement.Text := copy( text,1,255);  //achtung 255 neu 10/2018
	  wobjekt.Selection.Find.Forward := True;
	  wobjekt.Selection.Find.Wrap := 1;
	  wobjekt.Selection.Find.Format := False;
	  wobjekt.Selection.Find.MatchCase := False;
	  wobjekt.Selection.Find.MatchWholeWord := true;
	  wobjekt.Selection.Find.MatchWildcards := False;
	  wobjekt.Selection.Find.MatchSoundsLike := False;
	  wobjekt.Selection.Find.MatchAllWordForms := False;
    for j:=1 to 4  do   //1
     begin
      try
      //if   wobjekt.ActiveDocument.StoryRanges.item(storys[rf[j]]).text<>'' then
      if (bookmarklisten[rf[j]].IndexOf(alt)>=0) or alle then
      begin
        wobjekt.ActiveDocument.StoryRanges.item(storys[rf[j]]).Select;
        if all then
            a:=wobjekt.Selection.Find.Execute( Replace:=2)
            //a:=wobjekt.ActiveDocument.StoryRanges.item(storys[j]).find.Execute( Replace:=2)
            //a:=SearchAndReplaceInStory(wobjekt.ActiveDocument.StoryRanges.item(storys[j]),alt,text,2)
        else
           a:= wobjekt.Selection.Find.Execute( Replace:=1);//2
           //a:=wobjekt.ActiveDocument.StoryRanges.item(storys[j]).find.Execute( Replace:=1);
           //a:=SearchAndReplaceInStory(wobjekt.ActiveDocument.StoryRanges.item(storys[j]),alt,text,1);
      end;     
      except
      //element nicht vorhanden
      end;
      if a<>0 then
      begin
          //wobjekt.ActiveDocument.StoryRanges.item(storys[rf[j]]).Select;
          //a:=wobjekt.Selection.Find.Execute( Replace:=2);
         dateieinfuegen(datei);
      end;
    end;
    //wobjekt.ActiveDocument.StoryRanges.item(storys[1]).Select;
    //wobjekt.ActiveWindow.View.Type:=3;
    //wobjekt.Selection.endkey( Unit:=6);      sonst werden dateien nicht  an die aktuelle position eingelesen
    try
      wobjekt.selection.collapse;
    except
      e_status('Fehler bei der Ausf�hrung von: wobjekt.selection.collapse');
    end;
end;
//main
begin
    wansicht:=wobjekt.ActiveWindow.View.Type;
	  text:=stringreplace(text,chr(13),'#|cr|#',[rfReplaceAll] );
    position:= pos('#|cr|#',text);

	  while length(text)>100 do
	  begin
		 suchersetz(bookmark,copy(text,1,100)+bookmark,false);
		 text:=copy(text,101,length(text));
	  end;
	  suchersetz(bookmark,text,false);
	 if position>0 then
	 begin
   try
     suchersetz('|cr|','^p',true);  //word 2000
   except
     suchersetz('|cr|','^a',true); //word 97
   end;
   end;

   //normalansicht
   try
   wobjekt.ActiveWindow.View.Type:=wansicht; //1:normal, 2:gliederungsansicht, 3: seitenansicht, 4:druckvorschau, 5: masteransicht, 6: webansicht, 7: leseansicht
   except
    e_status('Fehler bei der Ausf�hrung von:bookmark_einfuegen_datei - wobjekt.ActiveWindow.View.Type:='+inttostr(wansicht));
   end;
   try
    wobjekt.selection.HomeKey(Unit:=6);
    wobjekt.run('arbene_tabelle');
    wobjekt.selection.HomeKey(Unit:=6);
   except
   end;
end;

function tcom.SearchAndReplaceInStory(rngStory : olevariant; strSearch : String ; strReplace:  String;rp:integer ):integer;
begin
 if rngStory.text<>'' then
 begin

    rngStory.Find.ClearFormatting;
    rngStory.Find.Replacement.ClearFormatting;
    rngStory.Find.Text :=strsearch;
    rngStory.Find.Replacement.Text := strreplace;
    rngStory.Find.Forward := True;
    rngStory.Find.Wrap := 1;
   
    result:=rngStory.Find.Execute( Replace:=rp)
 end;
end;

procedure tcom.bookmark_einfuegen(bookmark,text:string;all:boolean);  //keine ##
var
position,wansicht:integer;

procedure suchersetz(alt,text:string;alle : boolean);
var tf,j,z,a:integer;
alt2:string;
//rf: array[1..4] of integer;
Search_Rng: OleVariant;
begin
    alt2:='#'+alt+'#';

    for j:=1 to 4  do   //1
     begin
      try
      beep();
        //wobjekt.ActiveDocument.StoryRanges.item(storys[rf[j]]).Select;
        if (bookmarklisten[j].IndexOf(alt)>=0) or alle then
        if all then
            //a:=wobjekt.Selection.Find.Execute( Replace:=2)
            //a:=wobjekt.ActiveDocument.StoryRanges.item(storys[j]).find.Execute( Replace:=2)
            a:=SearchAndReplaceInStory(wobjekt.ActiveDocument.StoryRanges.item(storys[j]),alt2,text,2)
        else
           //a:= wobjekt.Selection.Find.Execute( Replace:=1);//2
           //a:=wobjekt.ActiveDocument.StoryRanges.item(storys[j]).find.Execute( Replace:=1);
           a:=SearchAndReplaceInStory(wobjekt.ActiveDocument.StoryRanges.item(storys[j]),alt2,text,1);
       beep();
       beep();    
      except
      //element nicht vorhanden
      end;
    end;
    //wobjekt.ActiveDocument.StoryRanges.item(storys[1]).Select;
    //wobjekt.ActiveWindow.View.Type:=3;
    //wobjekt.Selection.endkey( Unit:=6);      sonst werden dateien nicht  an die aktuelle position eingelesen
    wobjekt.selection.collapse;
end;
//#########################  main

begin
    wansicht:=wobjekt.ActiveWindow.View.Type;
	  text:=stringreplace(text,chr(13),'#|cr|#',[rfReplaceAll] );
    position:= pos('#|cr|#',text);

	  while length(text)>100 do
	  begin
		 suchersetz(bookmark,copy(text,1,100)+'#'+bookmark+'#',false);
		 text:=copy(text,101,length(text));
	  end;
	  suchersetz(bookmark,text,false);
	 if position>0 then
	 begin
   try
     suchersetz('|cr|','^p',true);  //word 2000
   except
     suchersetz('|cr|','^a',true); //word 97
   end;
   end;

   try
   wobjekt.ActiveWindow.View.Type:=wansicht; //1:normal, 2:gliederungsansicht, 3: seitenansicht, 4:druckvorschau, 5: masteransicht, 6: webansicht, 7: leseansicht
   except
    e_status('Fehler bei der Ausf�hrung von:bookmark_einfuegen- wobjekt.ActiveWindow.View.Type:='+inttostr(wansicht));
   end;
end;


procedure tcom.olemakro(makro:integer);       //bisher nur definiert keine Ahnung ob �s so funkt
var
	Arbenedot: string;
	geaendert: boolean;
begin
try
Arbenedot:=vorlagenverzeichnis+'\Arbene.dot';
geaendert:=form_main.Arbeitsmedizin_firma.Modified;
if wobjekt_typ='word6' then
	case makro of
	0:;
	1:;
	2:;
	end;

if wobjekt_typ='word8' then
 case makro of
	0:;
	1: begin

			if FileExists(Arbenedot) then wobjekt.AddIns.Add(FileName:=Arbenedot, Install:=True);
			//##wobjekt.ActiveDocument.CopyStylesFromTemplate(Template:= einstellform.vorlagenpfad.caption+'\hamster.dot');
			if wobjekt.ActiveDocument.Bookmarks.Exists('Archiv') then
			  wobjekt.selection.GoTo(What:=-1, Name:='Archiv');
			//if trim(word_startmakroText)<>'' then wobjekt.run(trim(word_startmakroText));
		end;
	2:  begin

		  wobjekt.documents.item(wobjekt_name).printout;
		  form_main.arbeitsmedizin_firma.Modified:=geaendert;
		end;
	end;
except
end;
end;

procedure tcom.bookmarks_lesen;
var
position:integer;
wort:string;
begin
	dokutextlesen;
	bookmark_list.Clear;
	position :=pos('�',dokutext);
	if position >0 then //serienbrief
	begin
		position :=pos('�',dokutext);
		while position>0 do
		begin
			serienbrief:=true;
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('�',dokutext);
			wort:= copy(dokutext,1,position-1);
			bookmark_list.Add(wort);
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('�',dokutext);
		end;
	end
	else
	begin      //ersetzen
		position :=pos('#',dokutext);
		while position>0 do
		begin
			serienbrief:=false;
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('#',dokutext);
			wort:= copy(dokutext,1,position-1);
			wort:=lowercase(wort);
			bookmark_list.Add(wort);
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('#',dokutext);
		end;
	end;


end;

procedure tcom.bookmarks_lesen_ersetzen;
var
position:integer;
wort:string;
begin
	dokutextlesen;
	bookmark_list.Clear;
	position :=pos('#',dokutext);
	if position >0 then //serienbrief
	begin      //ersetzen
		while position>0 do
		begin
			serienbrief:=false;
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('#',dokutext);
			wort:= copy(dokutext,1,position-1);
			wort:=lowercase(wort);
			bookmark_list.Add(wort);
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('#',dokutext);
		end;
	end;


end;

procedure tcom.bookmarks_lesen_serienbrief;
var
position:integer;
wort:string;
begin
	dokutextlesen;
	bookmark_list.Clear;
	position :=pos('�',dokutext);
	if position >0 then //serienbrief
	begin
		position :=pos('�',dokutext);
		while position>0 do
		begin
			serienbrief:=true;
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('�',dokutext);
			wort:= copy(dokutext,1,position-1);
			bookmark_list.Add(wort);
			dokutext:=copy(dokutext,position+1,length(dokutext));
			position :=pos('�',dokutext);
		end;
	end;


end;


procedure tcom.bookmarks_einfuegen_tabelle(tab_name: string; nummer:int64;modus:integer);
var
scount,i,j,nr,p,tf,z:integer;
query,feld,sql_select,sql_query,sql_where,suchtab ,dattemp,s:string;
joinliste:tintegerlist;
begin
with datamodul do
try
  form_checkbox:=tform_checkbox.create(application);
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.6', [ pfad_temp]));
  dattemp:=pfad_temp+'s_brief.txt';
	joinliste:=tintegerlist.Create;
	serienbrief_list.Clear;
	bookmark_list_all.clear;
	sql_select:='select ';

	sql_where:=format(' where %s.nummer="%d" ',[tab_name, nummer]);
	suchtab:=tab_name+'.';

	//alle Felder in liste schreiben
	for i:=0 to bookmark_list.count-1 do
	begin
	  if q_sql_felder.locate('name1',bookmark_list[i],[loCaseInsensitive]) then
     begin
		  if bookmark_list_all.IndexOf(bookmark_list[i])<0 then     //2014-05-10    hier muss noch etwas �berarbeitet werden! so dass  das ersetzten �berall klappt
		  bookmark_list_all.Add(bookmark_list[i]);
     end
	  else
	  begin //Arbene-serienbrief-felder , datum
		if q_serienfelder.Locate('feldname',bookmark_list[i],[loCaseInsensitive]) then
		 begin
			nr:=q_serienfelder['nummer'];
			query:=format('select feldinhalt from serienfeldinhalte where i_serienfeld =%d ',[nr]);
			sql_new(true,q_serienfeldinhalte,query,'');
			q_serienfeldinhalte.first;
			while not q_serienfeldinhalte.eof do
			begin
			  if q_serienfeldinhalte<>nil then feld:=q_serienfeldinhalte['feldinhalt'] else feld:='';

			  if q_sql_felder.locate('name1',feld,[loCaseInsensitive]) then
			  if bookmark_list_all.IndexOf(feld)<0 then
			  bookmark_list_all.Add(feld);
        
			  q_serienfeldinhalte.next;
			end;
		 end;
    //################################## extrafelder einf�gen  sind nicht in der Tabelle
		 if bookmark_list[i] ='datum' then bookmark_list_all.Add('datum');
     if bookmark_list[i]='arbeitsbericht' then bookmark_list_all.Add('arbeitsbericht');
    if bookmark_list[i]='impfungen_alle' then bookmark_list_all.Add('impfungen_alle');
    if bookmark_list[i]='labor_alle' then bookmark_list_all.Add('labor_alle');
    if bookmark_list[i]='impfungen_geplant' then bookmark_list_all.Add('impfungen_geplant');
    if bookmark_list[i]='impfungen_geplant_lang' then bookmark_list_all.Add('impfungen_geplant_lang');
    if bookmark_list[i]='labor_geplant' then bookmark_list_all.Add('labor_geplant');
    if bookmark_list[i]='labor_werte' then bookmark_list_all.Add('labor_werte');
    if bookmark_list[i]='labor_hinweise' then bookmark_list_all.Add('labor_hinweise');
	  end;
	 end;


	 sql_query:=query_create(bookmark_list_all,sql_where,tab_name);

   try
     sql_new(false,q_1,sql_query,'');

   q_1.first; //2011/3/17 vorher last damit konnten die vo_untersucher nicht gezogen werden wenn untersuchungen in der Zukunft noch keine hatten,
                //jetzt in strausdatei so lange suchen bis feld gef�llt

  except
    showmessage ('Ein Platzhalter in der Vorlage ist nicht zul�ssig');
    exit;
   end;              

	if not serienbrief then //word - serienbrief
	begin
	  scount:= wobjekt.activedocument.shapes.count;
	  wobjekt.Selection.Find.ClearFormatting;
	  wobjekt.Selection.Find.Forward := True;

	  wobjekt.Selection.Find.MatchCase := False;
	  wobjekt.Selection.Find.MatchWholeWord := true;

	  wobjekt.Selection.Find.MatchAllWordForms := False;
	  wobjekt.Selection.Find.Wrap := 1;
	end;

	//datenbankinhalt bzw leer f�r joker
	for i:=0 to bookmark_list.Count-1 do
	begin
		feld:=strausdatei(q_1,bookmark_list[i]); //Spezialf�lle werte und arbeitsbericht .... werden hier behandelt    suche nach ######spezialfelder dort muss auch noch eingetragen werden!!!!!
		//feld:=copy(feld,1,255); //sp�ter
		if feld<>'##' then
    if not serienbrief then
			begin  //1   //Spezialf�lle in
         if bookmark_list[i]= 'werte' then  laboreinfuegen  // #werte#
         else if bookmark_list[i]= 'labor_werte' then  labor_einfuegen_werte  // #labor_werte#
         else if bookmark_list[i]= 'labor_hinweise' then  labor_einfuegen_hinweise  // #labor_hinweise#
         else if  bookmark_list[i]= 'arbeitsbericht' then  arbeitsberichteinfuegen //#arbeitsbericht#
         else if  bookmark_list[i]= 'datum_erstellt' then  datum_erstellt_einfuegen  //#datum_erstellt#     wird in der bescheinigungserstellung verarbeitet
         else if  bookmark_list[i]= 'bescheinigungen_geplant' then  uil_einfuegen(1,0,99999)
         else if  bookmark_list[i]= 'impfungen_alle' then  uil_einfuegen(2,0,99999)
         else if  bookmark_list[i]= 'labor_alle' then  uil_einfuegen(3,0,99999)
         else if  bookmark_list[i]= 'impfungen_geplant_lang' then  uil_einfuegen(5,0,99999)
         else if  bookmark_list[i]= 'impfungen_geplant' then  uil_einfuegen(7,0,99999)
         else if  bookmark_list[i]= 'labor_geplant' then  uil_einfuegen(6,0,99999)
         else if  bookmark_list[i]= 'stempel' then  stempel_einfuegen
         else if  bookmark_list[i]= 'diagnosen' then  diagnosen_einfuegen
      else
     //###############################
     begin  //normalfall
          //hier anpassungen
          if  (bookmark_list[i]= 'f_postfach') and (trim(feld)<>'') then feld:='Postfach: '+feld  ;


          //jezt �bergabe an word
					 wobjekt.Selection.Find.Text :='#'+bookmark_list[i]+'#';
           feld:=copy(feld,1,255);
           wobjekt.Selection.Find.Replacement.Text := feld;   //darf nur 255 lang sein

         //showmessage(feld);

					for j:=1 to 4  do   //1
					 begin
            try
              if bookmarklisten[j].IndexOf(bookmark_list[i])>=0 then
              begin

                {wobjekt.ActiveDocument.StoryRanges.item(storys[j]).Select;
                wobjekt.Selection.Find.Execute( Replace:=2);}
                //wobjekt.ActiveDocument.StoryRanges.item(storys[j]).find.Execute( Replace:=2);
                SearchAndReplaceInStory(wobjekt.ActiveDocument.StoryRanges.item(storys[j]),'#'+bookmark_list[i]+'#',feld,2);
              end;
         // showmessage(feld);

            except
            //element nicht vorhanden
            end;

          end;
      end;
			end       //1
			else  serienbrief_list.Add(feld)
		else serienbrief_list.Add('')   ; //serienbrief  wenn## nicht in die Serienbriefdat kommt verschiebt sich alles
	end;
  wobjekt.ActiveDocument.StoryRanges.item(storys[1]).Select; //###########
  wobjekt.ActiveWindow.View.Type:=3;
	//if scount>0 then wobjekt.ActiveDocument.StoryRanges.item(1).Select;
	//wobjekt.Selection.homekey( Unit:=6);  //################
  wobjekt.selection.collapse;
	if serienbrief then
	begin
	  sereinbriefdatei_schreiben(dattemp,modus);
		// sereienbrief erstellen und mit der datei verbinden
	  wobjekt.ActiveDocument.MailMerge.MainDocumentType :=0;
	  wobjekt.ActiveDocument.MailMerge.OpenDataSource(Name:=dattemp,ConfirmConversions:=False,LinkToSource:=True, readonly:=True, Revert:=True);
	  wobjekt.ActiveDocument.MailMerge.EditMainDocument;
	  wobjekt.ActiveDocument.MailMerge.Destination := 0;
	  wobjekt.ActiveDocument.MailMerge.Execute;
	end;

	
finally
	joinliste.free;
  form_checkbox.Free;
   //ShowMessage(SysErrorMessage(GetLastError));
end;
end;


procedure tcom.sereinbriefdatei_schreiben(datei:string;modus:integer);
var
i,j,zeilen_max, feld_max,nummer,position:integer;
wort,query,feld,feldname: string;
grid: array of array of string;
textdat: textfile;
begin
with datamodul do

begin
//Anzahl der Zeilen

zeilen_max:=1;
feld_max:=bookmark_list.count-1;
for i:=0 to feld_max do
begin
	if q_serienfelder.Locate('feldname',bookmark_list[i],[loCaseInsensitive]) then
	begin
	  nummer:=q_serienfelder['nummer'];
	  query:=format('select count(nummer) from serienfeldinhalte where i_serienfeld =%d',[nummer]);
	  zeilen_max:=max(strtoint(mysql_d.Feldinhalt(query,0)),zeilen_max);
	end;
end;

//array dimensionieren
setlength(grid,feld_max+1,zeilen_max);
// ARRAY F�LLEN
for j:=0 to zeilen_max-1 do
	for i:=0 to feld_max do
	begin
	  if q_serienfelder.Locate('feldname',bookmark_list[i],[loCaseInsensitive]) then
	  begin
		 nummer:=q_serienfelder['nummer'];
		 query:=format('select feldinhalt from serienfeldinhalte where i_serienfeld =%d and reihenfolge =%d',[nummer,j]);
		 feldname:=mysql_d.Feldinhalt(query,0);
		 feld:=strausdatei(q_1,feldname);
		 if feld='##' then feld:=''; //ergebnis von strausdat kan ## sein
		 if (copy(feld,1,1)='"') and (copy(feld,length(feld),1)='"') then
		 feld:=copy(feld,2,length(feld)-2);
	  end
	  else feld :=serienbrief_list[i];

	  //| entfernen sonst fehler beim Serienbrief
	  position:=pos('|', feld);
	  while position>0 do
	  begin
			delete(feld, position,1);
			position:=pos('|', feld);
	  end;

	  grid[i,j]:=feld;
	  //listlist[j].add(feld);
	end;



	if fileexists(datei) then deletefile(datei);
	if fileexists(datei) then
	begin
		showmessage('Bitte die Datei: '+datei+' schlie�en; sie  konnte nicht autom. geschlossen werden.');
		exit;
	end;
	assignfile(textdat, datei);
 try

	rewrite(textdat);
	  wort:='';
	  for i:=0 to feld_max do
	  begin
		  if i<feld_max then wort:=wort+bookmark_list[i]+'|' else wort:=wort+bookmark_list[i];//+chr(13)+chr(10);
	  end;
	  writeln(textdat,wort);
	  //showmessage(wort);
	  for j:=0 to zeilen_max-1 do
	  begin
		  wort:='';
		 for i:=0 to feld_max do
		 begin
			 if i<feld_max then wort:=wort+grid[i,j]+'|' else wort:=wort+grid[i,j];//+chr(13)+chr(10);
		 end;
		 writeln(textdat,wort);
	  end;
finally
		closefile(textdat);
end;
end;
end;

procedure tcom.dokutextlesen;
var
storyanzahl,storytyp ,i,j, i_scount,tf,z,position:integer;
akt_text,wort:string;
//storys: array[1..3] of integer;
begin
try                               

      dokutext:='';

			wobjekt.activewindow.view.ShowFieldCodes:=false;
			//i_scount:= wobjekt.activedocument.shapes.count;
			storyanzahl:=wobjekt.activedocument.StoryRanges.count;
			//storyanzahl:=1;
			storytyp:=1;

			//while (storyanzahl>0) and (storytyp<=12)do //in der Liste sind einige ranges evtl. nicht definiert
      for i:=  1 to  4 do
			begin
				try
            bookmarklisten[i].Clear;
            akt_text:=wobjekt.activedocument.StoryRanges.Item(storys[i]).text;
				    dokutext:=dokutext+akt_text;

            position :=pos('#',akt_text);
            while position>0 do
            begin
              akt_text:=copy(akt_text,position+1,length(akt_text));
              position :=pos('#',akt_text);
              wort:= copy(akt_text,1,position-1);
              wort:=lowercase(wort);
              bookmarklisten[i].add(wort);
              akt_text:=copy(akt_text,position+1,length(akt_text));
              position :=pos('#',akt_text);
            end;

				//dec(storyanzahl);
				except
				end;
				//inc(storytyp);
			end;

		 wobjekt.selection.HomeKey(Unit:=6);
except
end;
end;


procedure tcom.doku_drucken_pdf;
var
t:ttime;
pr:string;
begin
try
    pr:=wobjekt.activeprinter;
    wobjekt.activeprinter:='qvPDF';
	  wobjekt.activedocument.printout;
    wobjekt.activeprinter:=pr;
except
end;
end;

procedure tcom.doku_drucken;
var
t:ttime;
begin
try
    pause(100);//t:=time();
    //while time()< t+0.000005 do; //5/10 sec
	  wobjekt.activedocument.printout;
except
end;
end;

procedure tcom.doku_drucken_seite;
begin
try
	  wobjekt.printout(Range:=2);
except
end;
end;

procedure tcom.doku_schliessen;
begin
try
	  wobjekt.activedocument.close(0);

except
end;
end;

procedure tcom.word_schliessen;
begin
try
    if  not VarIsNull(wobjekt) then
    begin
      wobjekt.quit;
      wobjekt:=Unassigned;
    end;
except
end;
end;


procedure tcom.doku_speichern(datei:string);
begin
try
    wobjekt.selection.HomeKey(Unit:=6);
	  wobjekt.activedocument.saveas(Filename:=datei,FileFormat:=6);
    //warte_bis_datei_lesbar(datei);
except
end;
end;

function tcom.doku_txt:string;
begin
	dokutextlesen;
	result:=stringreplace( dokutext,chr(13),chr(13)+chr(10),[rfreplaceall]);
end;

procedure tcom.doku_memo(var memo: tmemo);
var
position:integer;
begin
	memo.lines.Clear;
	dokutextlesen;
	position:=pos(chr(13),dokutext);
	while position>0 do
	begin
		memo.lines.Add(copy(dokutext,1,position-1));
		dokutext:=copy(dokutext,position+1,length(dokutext));
		position:=pos(chr(13),dokutext);
	end;
	if dokutext<>'' then memo.lines.add(dokutext);

end;

procedure tcom.schrift(fName:string; fSize:integer; fBold:boolean);
begin
	 if fname<>'' then wobjekt.selection.font.name:=fname;
	 if fsize>3 then   wobjekt.selection.font.Size:=fsize;
	 wobjekt.selection.font.bold:=fbold;
end;

procedure tcom.laboreinfuegen;
var
us: thtml_uebersicht;
f_name:string;
begin

try
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.7', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
	us:=thtml_uebersicht.create(f_name);
	us.report(form_main.dbtreeview.Items,4,'','',0);
finally
	us.free;
end;
	  //com.bookmark_einfuegen('#werte#','',false); //geht nur an die Stelle und ersetzt werte
	  //com.dateieinfuegen(f_name);
     com.bookmark_einfuegen_datei('werte','',f_name,false); //geht nur an die Stelle und ersetzt werte

end;

procedure tcom.labor_einfuegen_hinweise;
var
i:integer;
snr:int64;
filter, filterfeld, filterwert,query,s,s1,s2,s3,s4,s5,s6,s7,s8,f_name:string;
i_mitarbeiter:int64;
r1,r2:real;
lab_auffaellig:boolean;
datum:tdate;
r:thtml_report;
begin
with datamodul do
try
  //HInweise m�ssen zuvor in der Kartei zum laborwert hinzugef�gt werden
  //stehen dann in der Tabelle tabelle_hinweise  referenz labor.nummer->tab_hinweise.i_tab_nummer  Hinweise dan in tab_nummmer->i_tbs in  textbausteine

  // select alle labor mit aktuellem Datum f�r den aktuellen Ma ->join tab_hinweise ->join textbausteine
  //schreibe alle in datei
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.8', [ pfad_temp]));
  f_name:=pfad_temp+'lab_hinweise.html';
  i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');
  r:=thtml_report.create(f_name);
  r.css_ini(l_labor_hinweise);
  query:=format('select labor.datum from labor  where labor.i_mitarbeiter=%s and labor.storno=0 and labor.i_status=4 group by datum order by datum desc   ',[inttostr(i_mitarbeiter) ]);
  sql_new(false,q_3,query,'');

    //checkbox f�llen wenn leer
  q_3.first;
  if form_checkbox.CheckListBox.Items.Count=0 then
  begin
    while not q_3.Eof do
    begin
      form_checkbox.CheckListBox.Items.Add(q_3.findfield('datum').AsString);
      q_3.Next;
    end;
    for i:=0 to form_checkbox.checklistbox.Items.Count-1 do form_checkbox.checklistbox.checked[i]:=true;
    form_checkbox.einstellen('Datum ausw�hlen');
    form_checkbox.ShowModal;
  end;


  q_3.first;
  while not q_3.Eof do //f�r jedes datum    lie�e sich hier w�hlen
  begin
    if form_checkbox.is_checked(q_3.findfield('datum').asstring) then
    begin
        s:=format (' and labor.datum ="%s" ',[dat_delphi_to_sql(q_3.findfield('datum').asstring)]);

        query:='select labor.*, textbausteine.*  from labor left join tabelle_hinweise on(labor.nummer=tabelle_hinweise.i_tab_nummer) left join textbausteine on (tabelle_hinweise.i_tbs=textbausteine.nummer) '+
                format('  where labor.i_status=4 and tabelle_hinweise.storno=0 and labor.i_mitarbeiter=%s %s and labor.storno=0 %s',[inttostr(i_mitarbeiter),s,filter ]);
        sql_new(false,q_2,query,'');
        q_2.first;
        s:= format('%s Labor ',[datetostr(datum)]);
         r.t_css_beginn(6,0);
        r.t_css_zeile_header(['class="h1"','class="h2"'],['Hinweis','Hinweistext']);

        lab_auffaellig:=false;
        while not q_2.eof do
        begin
          try
           if varisnull(q_2['name']) then 	s1:='' else s1:=q_2.findfield('name').asstring;
           if varisnull(q_2['tbs']) then 	s2:='' else s2:=q_2.findfield('tbs').asstring;
            r.t_css_zeile(['class="d1"','class="d2"'],[s1,s2])
          finally
           q_2.next;
         end;
      end;
      // und jetzt noch die freien Hinweise in labor
      
        s:=format (' and labor.datum ="%s" ',[dat_delphi_to_sql(q_3.findfield('datum').asstring)]);
        query:=format(' select typ.untersuchung, labor.hinweis  from labor left join typ on (labor.i_typ=typ.nummer) where labor.i_status=4 and labor.hinweis<>"" and labor.i_mitarbeiter=%s %s and labor.storno=0 %s',[inttostr(i_mitarbeiter),s,filter ]);
        sql_new(false,q_2,query,'');
        while not q_2.eof do
        begin
          try
           if varisnull(q_2['untersuchung']) then 	s1:='' else s1:=q_2.findfield('untersuchung').asstring;
           if varisnull(q_2['hinweis']) then 	s2:='' else s2:=q_2.findfield('hinweis').asstring;
            r.t_css_zeile(['class="d1"','class="d2"'],[s1,s2])
          finally
           q_2.next;
         end;
      end;


      r.t_css_ende;
    end;//datum cheded
    q_3.Next;
    end;



  finally
    r.free;
   com.bookmark_einfuegen_datei('labor_hinweise','',f_name,false);
  end;

end;


procedure tcom.labor_einfuegen_werte;
var
filter, filterfeld, filterwert,query,s,s1,s2,s3,s4,s5,s6,s7,s8,f_name,d:string;
a,i:integer;
i_mitarbeiter:int64;
r1,r2:real;
lab_auffaellig:boolean;
datum:tdate;
r:thtml_report;
o: OleVariant;
begin
with datamodul do
try
//##
if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.8', [ pfad_temp]));
f_name:=pfad_temp+'lab_werte.html';
i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');
r:=thtml_report.create(f_name);
r.css_ini(l_labor_werte);
if filterfeld<>'' then
filter:=format(' and labor.%s=%s ',[filterfeld,filterwert])
else filter:='';
//labordatum
query:=format('select labor.datum from labor  where labor.i_mitarbeiter=%s and labor.storno=0 and labor.i_status=4 group by datum order by datum desc   ',[inttostr(i_mitarbeiter) ]);
sql_new(false,q_3,query,'');
//checkbox f�llen wenn leer
q_3.first;
if form_checkbox.CheckListBox.Items.Count=0 then
begin
  while not q_3.Eof do
  begin
    form_checkbox.CheckListBox.Items.Add(q_3.findfield('datum').AsString);
    q_3.Next;
  end;
  for i:=0 to form_checkbox.checklistbox.Items.Count-1 do form_checkbox.checklistbox.checked[i]:=true;
  form_checkbox.einstellen('Datum ausw�hlen');
  form_checkbox.ShowModal;
end;

q_3.first;
while not q_3.Eof do //f�r jedes datum
  begin
  if form_checkbox.is_checked(q_3.findfield('datum').asstring) then
  begin
      d:= q_3.findfield('datum').asstring;
      s:=format (' and labor.datum ="%s" ',[dat_delphi_to_sql(q_3.findfield('datum').asstring)]);

      //i_mitarbeiter:=getbigint( q_mitarbeiter,'nummer');
      query:='select labor.*, typ.untersuchung,status.status  from labor left join typ on(labor.i_typ=typ.nummer) '+
        format(' left join status on (labor.i_status=status.nummer) where labor.i_mitarbeiter=%s %s and labor.storno=0 %s',[inttostr(i_mitarbeiter),s,filter ]);
      sql_new(false,q_2,query,'');
      q_2.first;
      s:= format('%s Labor ',[datetostr(datum)]);


      r.t_css_beginn(6,0);
      r.t_css_zeile_header(['class="h1"','class="h2"','class="h3"','class="h4"','class="h5"','class="h6"','class="h7"'],['Labor vom '+d,'','','','','','']);
      r.t_css_zeile_header(['class="h1"','class="h2"','class="h3"','class="h4"','class="h5"','class="h6"','class="h7"'],['Parameter','Wert','Einheit','Ergebnistext','Marker','Norm','Hinweise']);

      lab_auffaellig:=false;
      while not q_2.eof do
      begin
        try
				 if varisnull(q_2['untersuchung']) then 	s1:='' else s1:=q_2.findfield('untersuchung').asstring;
				 if varisnull(q_2['wert']) then 	r1:=0 else r1:=q_2.findfield('wert').asfloat;
				 if varisnull(q_2['einheit']) then 	s2:='' else s2:=q_2.findfield('einheit').asstring;
				 if varisnull(q_2['ergebnistext']) then 	s3:='' else s3:=r.html_text(q_2.findfield('ergebnistext').asstring);
				 if varisnull(q_2['grenzwertindikator']) then 	s4:='' else s4:=q_2.findfield('grenzwertindikator').asstring;
         if s4<>'' then
         begin
            lab_auffaellig:=true;
            s6:='s. Hinweis' +s1;
         end;
				 if varisnull(q_2['norm']) then 	s5:='' else s5:=r.html_text(q_2.findfield('norm').asstring);
				 if varisnull(q_2['testbezogenerhinweis']) then 	s6:='' else s6:=r.html_text(q_2['testbezogenerhinweis']);
				 //if varisnull(q_2['status']) then 	s7:='' else s7:=(q_2['status']);
            //r.t_zeile('',[s1,floattostr(r1),s2,s3,s4,s5,s6,s7]);

				 //r.t_zeile('',[s1,floattostr(r1),s2,s3,s4,s5]);
         if s4='' then
          r.t_css_zeile(['class="d1"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[s1,floattostr(r1),s2,s3,s4,s5,s6])
         else
          r.t_css_zeile(['class="d1_bold"','class="d2"','class="d3"','class="d4"','class="d5"','class="d6"','class="d7"'],[s1,floattostr(r1),s2,s3,s4,s5,s6]);
        finally
				 q_2.next;
       end;
    end;
    r.t_css_ende;

    r.zeile('');
    if lab_auffaellig then
      r.zeile('Mindestens ein Wert liegt nicht im Normbereich.')
    else
      r.zeile('Alle Werte liegen im Normbereich.');
  end;//datum checked
  q_3.Next;
  end;
finally
 r.free;
 com.bookmark_einfuegen_datei('labor_werte','',f_name,false);


end;
end;


procedure tcom.diagnosen_einfuegen;
var
diagnosen,query,s_mitarbeiter: string;

begin
with datamodul do
begin
  s_mitarbeiter:=q_mitarbeiter.findfield('nummer').asstring;
  query:=format('select * from diagnosen where storno=0 and i_mitarbeiter=%s order by icd',[s_mitarbeiter]);
  sql_new(false,q_5,query,'');
  diagnosen:='';
  q_5.First;
  while not q_5.eof do
  begin
    diagnosen:= diagnosen+' ,'+q_5.findfield('icd_text').asstring+' ('+q_5.findfield('icd').asstring+')';
    q_5.Next;
  end;
  diagnosen:=copy(diagnosen,3,length(diagnosen));
  com.bookmark_einfuegen('diagnosen',diagnosen,false);
end;
end;


procedure tcom.stempel_einfuegen;
var
r: thtml_report;
f_name,query:string;
i_unterschrift:integer;
begin
if i_stempel>0 then i_unterschrift:=i_stempel else i_unterschrift:=akt_untersucher;
query:=format('select * from untersucher where nummer=%d',[i_unterschrift]);
datamodul.sql_new(false,datamodul.q_12,query,'');
try
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.8', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
  r:=thtml_report.create(f_name);
  if datamodul.q_12.findfield('untersucher').asstring<>'' then
	r.tag(datamodul.q_12.findfield('f_1').asstring ,datamodul.q_12.findfield('untersucher').asstring);
  if datamodul.q_12.findfield('untersucher_2').asstring<>'' then
  r.tag(datamodul.q_12.findfield('f_2').asstring ,datamodul.q_12.findfield('untersucher_2').asstring);
  if datamodul.q_12.findfield('untersucher_3').asstring<>'' then
  r.tag(datamodul.q_12.findfield('f_3').asstring ,datamodul.q_12.findfield('untersucher_3').asstring);
  if datamodul.q_12.findfield('untersucher_4').asstring<>'' then
  r.tag(datamodul.q_12.findfield('f_4').asstring ,datamodul.q_12.findfield('untersucher_4').asstring);
  if datamodul.q_12.findfield('untersucher_email').asstring<>'' then
  r.tag(datamodul.q_12.findfield('f_email').asstring ,datamodul.q_12.findfield('untersucher_email').asstring);
  if datamodul.q_12.findfield('untersucher_telefon').asstring<>'' then
  r.tag(datamodul.q_12.findfield('f_telefon').asstring ,datamodul.q_12.findfield('untersucher_telefon').asstring);
  
finally
	r.free;
end;
	  //com.bookmark_einfuegen('#stempel#','',false); //geht nur an die Stelle und ersetzt werte
	 // com.dateieinfuegen(f_name);
    com.bookmark_einfuegen_datei('stempel','',f_name,false); //geht nur an die Stelle und ersetzt werte
end;

procedure tcom.arbeitsberichteinfuegen;
var
//us: thtml_uebersicht;
f_name:string;
begin
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.9', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
	//com.bookmark_einfuegen('#arbeitsbericht#','',false); //geht nur an die Stelle und ersetzt werte
	//com.dateieinfuegen(f_name);
  com.bookmark_einfuegen_datei('arbeitsbericht','',f_name,false); //geht nur an die Stelle und ersetzt werte

end;

procedure tcom.datum_erstellt_einfuegen;
begin
  //com.bookmark_einfuegen('#datum_erstellt#',datum_dok_erstellt,false);
  com.bookmark_einfuegen('datum_erstellt',datum_dok_erstellt,false);
end;

procedure tcom.uil_einfuegen(uil:integer;von,bis: tdate);
var
us: thtml_uebersicht;
f_name:string;
begin

try
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.10', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
	us:=thtml_uebersicht.create(f_name);
  case uil of
    1: ;// us.report(form_main.dbtreeview.Items,2,'','');
    2:  us.report(form_main.dbtreeview.Items,3,'','',0);
    3:  us.report(form_main.dbtreeview.Items,4,'','',0);
    4:;
    5:  us.report(form_main.dbtreeview.Items,3,'i_status','1',0);
    6:  us.report(form_main.dbtreeview.Items,4,'i_status','1',0);
    7:  us.report(form_main.dbtreeview.Items,3,'i_status','1',1); //kurzversion
  end;
finally
	us.free;
end;
    case uil of
    1:  ;
    2:  com.bookmark_einfuegen_datei('impfungen_alle','',f_name,false); //geht nur an die Stelle und ersetzt werte
    3:  com.bookmark_einfuegen_datei('labor_alle','',f_name,false); //geht nur an die Stelle und ersetzt werte
    4:;
    5:  com.bookmark_einfuegen_datei('impfungen_geplant_lang','',f_name,false); //geht nur an die Stelle und ersetzt werte
    6:  com.bookmark_einfuegen_datei('labor_geplant','',f_name,false); //geht nur an die Stelle und ersetzt werte
    7:  com.bookmark_einfuegen_datei('impfungen_geplant','',f_name,false); //geht nur an die Stelle und ersetzt werte
  end;
	  //com.dateieinfuegen(f_name);
end;

function tcom.strausdatei(tabelle:tzquery; feldname:string): string;
var
ftype:tfieldtype;
position:integer;
begin
		  result:='##';
		  //spezialf�lle
		  if feldname='datum' then
		  begin
			result:=datetostr(date());
			exit;
		  end;
      
		  if copy(feldname,1,1)='"' then  //freitext im Arbene-serienbrief
		  begin
			result:=feldname;
			exit;
		  end;

     //////#######spezialfelder

      if feldname='werte' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='stempel' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='diagnosen' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='arbeitsbericht' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='datum_erstellt' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='impfungen_alle' then
		  begin
			result:='';
			exit;
		  end;
      if feldname='labor_alle' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='impfungen_geplant' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='impfungen_geplant_lang' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='labor_geplant' then
		  begin
			result:='';
			exit;
		  end;

      if feldname='labor_werte' then
		  begin
			result:='';
			exit;
		  end;
      if feldname='labor_hinweise' then
		  begin
			result:='';
			exit;
		  end;

		  if tabelle.FindField(feldname)=nil then exit;  //gibt�s nicht

		  ftype:=tabelle.FieldByName(feldname).DataType;
		  if tabelle[feldname]=null then result:=''
		  else
		  begin
			  case ftype of
				  ftInteger, ftlargeint,ftsmallint:begin
									if lowercase(copy(feldname, length(feldname)-1,2))='_b' then
									begin
										if tabelle[feldname]=0 then result:='nein'
										else if tabelle[feldname]=1 then result:='ja';
									end
									else
									result:=inttostr(tabelle[feldname]);
								end;
				  ftmemo:	begin
								  result:=tabelle[feldname];
								  position:=pos(chr(13)+chr(10),result);
								  while position>0 do
								  begin
									delete(result,position,2);
									insert(' ', result,position);
									position:=pos(chr(13)+chr(10),result);
								  end;
								  position:=pos(chr(13),result);
								  while position>0 do
								  begin
									delete(result,position,1);
									insert(' ', result,position);
									position:=pos(chr(13),result);
								  end;
								  position:=pos(chr(10),result);
								  while position>0 do
								  begin
									delete(result,position,1);
									insert(' ', result,position);
									position:=pos(chr(10),result);
								  end;

								end	;
				  ftdate,ftdatetime:begin
								result:=datetostr(tabelle[feldname]);
								if result='01.01.1900' then result :='';
                if result='01.01.0001' then result:='';
							end;
				  fttime:begin
								result:=timetostr(tabelle[feldname]);
								if copy(result,1,5)='00:00' then result :='';
							end;
			  else
						begin //string
						  result:=tabelle[feldname];
						  position:=pos(chr(13)+chr(10),result);
						  while position>0 do
						  begin
							delete(result,position,2);
							insert(' ', result,position);
							position:=pos(chr(13)+chr(10),result);
						  end;
						 end;
			  end;
end;
end;


function tcom.query_create(liste:tstringlist; sql_where,jointab:string):string;
var i:integer;
	  sql_select,sql_join:string;
	  joinliste:tintegerlist;
begin
if liste.count=0 then exit;
with datamodul do
try
	joinliste:=tintegerlist.Create;
  if jointab<>'' then
	begin
	 if q_sql_join.Locate('verbindung',jointab,[loCaseInsensitive,loPartialKey])
	 then joinliste.aktualisieren(q_sql_join['nummer'],1);
  end;
	sql_select:='';
	i:=0;
	while i< liste.Count do
	begin
			if q_sql_felder.locate('name1',liste[i],[loCaseInsensitive]) then
			begin
				if q_sql_felder['dbfeld1']<>'' then sql_select:=sql_select+q_sql_felder['dbfeld1'] +' , ';

				if q_sql_felder['dbfeld2']<>'' then  sql_select:=sql_select+q_sql_felder['dbfeld2'] +' , ';
				if q_sql_felder['dbfeld3']<>'' then  sql_select:=sql_select+q_sql_felder['dbfeld3'] +' , ';

				if q_sql_felder['i_join1']>0 then joinliste.aktualisieren(q_sql_felder['i_join1'],1);
				if q_sql_felder['i_join2']>0 then joinliste.aktualisieren(q_sql_felder['i_join2'],1);
			end
			else;
			inc(i);
	 end;

	 if sql_select<>'' then //mind ein feld vorhanden
	 begin
		sql_select:='select '+sql_select;
		sql_select:=copy(sql_select,1,length(sql_select)-3);//komma weg
	 end
	 else sql_select:='select mitarbeiter.nummer ';


	 sql_select:=sql_select+' from mitarbeiter ';

	 sql_join:=form_auswertung.sql_join_aus_liste(joinliste);

	 result:=sql_select+sql_join+sql_where;
finally
joinliste.free;
end;
end;



procedure tcom.export_excel(query:string);
var
zaehler,i:integer;
zeile:string;
begin
with datamodul, form_main do
try
	sql_new(false,q_1,query,'');
        excel_connect('Tagesplan');
        //excel_addworkbook('Termin�bersicht');
	//xl_connect;
	//xl_anzeigen;
	//xl_addworkbook;

	//Excel.Connect;
	//excel.Newworkbook(sheet,'');


	for i:=0 to q_1.fieldcount-1 do
	begin
	  //excel.PutStrAt(1,i+1,q_1.Fields[i].fieldname);
	  //xl_PutStrAt(1,i+1,q_1.Fields[i].fieldname);
          excel_PutStrAt(1,i+1,q_1.Fields[i].fieldname);
	end;

	zaehler:=2;
	while not q_1.eof do
	begin
		zeile:='';

		for i:=0 to q_1.fieldcount-1 do
		begin
		  excel_PutStrAt(zaehler,i+1,q_1.Fields[i].asstring);
		  //xl_PutStrAt(zaehler,i+1,q_1.Fields[i].asstring);
		end;
		inc(zaehler);
		q_1.Next;
	end;
	  //if trim(makro_excel)<>'' then  excel.RunMacro(makro_excel);
finally
	//xl_Disconnect;
        excel_disconnect;
end;
end;





function Tcom.audio_graph(q:tzquery):integer;
var
wort,sfl, sfr, sdbr, sdbl, spr, spl,weber,s,sli,sre,sdat,svalues,mname,erst_feld,letzt_feld,mw_feld:string;
db:array[1..4,1..12] of string;
i,j,iw,z,ej,lj:integer;
li, re: tstringlist;
xvalues,values:variant;

function makearray(s:string):variant;
var a:array of integer;
i,p:integer;
l:tstringlist;

begin
try
	l:=tstringlist.create;
	p:=pos(',',s);
	while p>0 do
	begin
		l.Add(trim(copy(s,1,p-1)));
		s:=copy(s,p+1,length(s));
		p:=pos(',',s);
	end;
	if trim(s)<>'' then l.add(trim(s)) ;
	i:=l.count;
	result:=vararraycreate([0,i-1],varinteger);

	for i:=0 to l.count-1 do
	begin
		if l.Strings[i]<>'' then result[i]:=strtoint(l.Strings[i]) else result[i]:='-3000';
	end;

finally
	l.free;
end;
end;

function kommaretrim(s:string):string;
begin
while copy(s,length(s),1)=',' do
begin
	s:=copy(s,1,length(s)-1);
	s:=trim(s);
end;
result:=s;
end;
/////////////////////////////////////////////////////////////
begin
with form_main, datamodul do    //keine ahnung warum
try
	mname:=form_main.ComboBox_history.Text;
	li:=tstringlist.Create;
	re:=tstringlist.create;
	xvalues:=vararrayof([250,500,1,2,3,4,6,8,12]);
	z:=1;
	q.First;
	while not q.Eof do
	begin
	  sdat:=datetostr(q['datum']);
	  wort:=q['str'];
	  iw:=strtoint(copy(wort,1,2));
	  case iw of
		 0:weber:='rechts';
		 1:weber:='medial';
		 2:weber:='links';
	  end;
	  sfr:=trim(copy(wort,3,4));
	  sfl:=trim(copy(wort,7,4));
	  sdbr:=trim(copy(wort,11,3));
	  sdbl:=trim(copy(wort,14,3));
	  spr:=trim(copy(wort,17,3));
	  spl:=trim(copy(wort,20,3));

	  for i:=1 to 4 do
	  begin
		 s:=copy(wort,23+(36*(i-1)),36);
      erst_feld:='';
      letzt_feld:='';
		 for j:=1 to 12 do //schreibe string in array
		 begin
			db[i,j]:=trim(copy(s,j*3-2,3));
			if db[i,j]='-20' then db[i,j]:='';
        if (erst_feld='') and (db[i,j]<>'') then
           begin
                erst_feld:=db[i,j];
           end;
	    end;
      for j:=12 downto 1 do //letzte Felder
		 begin
        if (letzt_feld='') and (db[i,j]<>'') then
        begin
             letzt_feld:=db[i,j];
        end;
	    end;
      if erst_feld='' then erst_feld:='0';
      if letzt_feld='' then letzt_feld:='0';
		 if db[i,1]='' then db[i,1]:=erst_feld;
      if db[i,12]='' then db[i,12]:=letzt_feld;

      j:=2;
      erst_feld:=db[i,1];
      while j<=12 do
      begin
           if db[i,j]='' then
           begin
              while db[i,j]='' do inc(j); //leere Felder �berspringen
              letzt_feld:=db[i,j];
              mw_feld:=inttostr(5*((strtoint(erst_feld)+strtoint(letzt_feld)) div 10));
              dec(j);
              while db[i,j]='' do
              begin
                   db[i,j]:=mw_feld;
                   dec(j);
              end;
           end;
           erst_feld:=db[i,j];
           inc(j);
      end;
      for j:=1 to 12 do //f�lle leere felder dazwischen
		 begin



		 end;

	  end;


	  for i:=0 to 1 do
	  begin
		 sli:='';
		 sre:='';
		 sli:=sli+db[1+i*2,2]+',';
		 sli:=sli+db[1+i*2,3]+',';
		 sli:=sli+db[1+i*2,5]+',';
		 sli:=sli+db[1+i*2,7]+',';
		 sli:=sli+db[1+i*2,8]+',';
		 sli:=sli+db[1+i*2,9]+',';
		 sli:=sli+db[1+i*2,10]+',';
		 sli:=sli+db[1+i*2,11]+',';
		 sli:=sli+db[1+i*2,12]+',';

		 sre:=sre+db[2+i*2,2]+',';
		 sre:=sre+db[2+i*2,3]+',';
		 sre:=sre+db[2+i*2,5]+',';
		 sre:=sre+db[2+i*2,7]+',';
		 sre:=sre+db[2+i*2,8]+',';
		 sre:=sre+db[2+i*2,9]+',';
		 sre:=sre+db[2+i*2,10]+',';
		 sre:=sre+db[2+i*2,11]+',';
		 sre:=sre+db[2+i*2,12]+',';


		 sli:=kommaretrim(sli);
		 sre:=kommaretrim(sre);

		 case i of
		 0: begin
			 sli:='LL '+sdat+sli;
			 sre:='LL '+sdat+sre;
			 end;
		 1: begin
			 sli:='KL '+sdat+sli;
			 sre:='KL '+sdat+sre;
			end;
		 end;
		 li.Add(sli);
		 re.Add(sre);
	  end;

	  q.Next;
	  inc(z);
	end;


	xl_connect;
	xl_addworkbook;
	xl_anzeigen;
	//xlw:=xla.Workbooks.Add(xlwbatworksheet,lcid);

	rnge:=xla.range['A1','A2'];

	chart_erzeugen('Audiogramm linkes Ohr ' +mname);


	for z:=0 to li.Count-1 do
	begin
		svalues:=copy(li.Strings[z],14,50);
		values:=makearray(svalues);
		s:=copy(li.Strings[z],1,13);
		if VarArrayHighBound(values,1)>0 then chart_add_serie(xvalues,values,s);
	end;

	chart_erzeugen('Audiogramm rechtes Ohr ' +mname);
	for z:=0 to re.Count-1 do
	begin
		svalues:=copy(re.Strings[z],14,50);
		values:=makearray(svalues);
		s:=copy(re.Strings[z],1,13);
		if VarArrayHighBound(values,1)>0 then chart_add_serie(xvalues,values,s);
	end;

finally
result:=z-1;
li.Free;
re.Free;
eobjekt:=Unassigned;
end;
end;




procedure tcom.chart_add_serie(xvalues,values:variant;namen:string);
var
col,count:integer;

begin
	//if copy(namen,1,1)='L' then col:=3 else col :=25;
  col:=1; //immer schwarz

	ch.SeriesCollection.NewSeries;
	count:=ch.seriescollection.count;
	o:=ch.seriescollection[count];
  col:=count+2;
  if col>56 then col:=1;
	o.name:=namen;
  if copy(namen,1,1)='L' then o.Border.LineStyle := xlContinuous else o.Border.LineStyle := xlDash;
	//o.Border.LineStyle := xlAutomatic;
	o.Border.ColorIndex := col;
	o.Border.Weight :=xlMedium;// xlThick;
	o.MarkerStyle := xlMarkerStyleAutomatic;
	o.MarkerBackgroundColorIndex := col;
	o.MarkerForegroundColorIndex := col;
	o.MarkerSize := 10;

	o.values:=values;
	o.xvalues:= xvalues;

end;

procedure tcom.chart_erzeugen(Titel:string);

begin

	//eobjekt.workbooks.add(xlWBatWorkSheet);

	//rnge:=com.eobjekt.Sheets[1].range['A1','A2']; //'Tabelle1'



	ch:=xla.Charts.Add(emptyparam,emptyparam,emptyparam,emptyparam,lcid);
	ch.name:=copy(titel,1,22);
	ch.SetSourceData (rnge, xlrows);
	ch.SeriesCollection[1].Values:=vararrayof([0,0,0,0,0,0,0]);
	ch.SeriesCollection[1].XValues :=vararrayof([250,500,1,2,3,4,6,8,12]);

	ch.HasTitle := True;
	ch.ChartTitle.Characters.Text := titel;
	ch.ChartType := xlLineMarkers;
	ch.Legend.Position := xlBottom;

	ch.PlotArea.interior.ColorIndex := 2;
	ch.PlotArea.interior.PatternColorIndex := 1;
	ch.PlotArea.interior.Pattern := xlSolid;

 //	ch.Location(Where:=xlLocationAsObject, Name:='Tabelle1');
	ch.Axes[xlCategory].HasMajorGridlines:= False;
	ch.Axes[xlCategory].HasMinorGridlines:= True;
	ch.Axes[xlValue].HasMajorGridlines := True;
	ch.Axes[xlValue].HasMinorGridlines := False;
	ch.Axes[xlValue].MinimumScale := -10;
	ch.Axes[xlValue].MaximumScale := 110 ;
	ch.Axes[xlValue].MinorUnit := 10    ;
	ch.Axes[xlValue].MajorUnit := 10;
	ch.Axes[xlValue].Crosses := xlCustom;
	ch.Axes[xlValue].CrossesAt := -10;
	ch.Axes[xlValue].ReversePlotOrder := True;
	ch.Axes[xlValue].ScaleType := xlLinear;

//	ch.Axes[xlValue].DisplayUnit := xlNone; funkt nicht in win2000



	ch.SeriesCollection[1].Delete;
end;

function tcom.excel_connect(name:string):boolean;
begin
     Excel := CreateOleObject('Excel.Application');
     Excel.Visible := true;
     //Excel.Workbooks.add ;       2011/3/16
     //excel.activeworkbook.title:='name'; 2011/3/16
     excel_addworkbook(name);    //statdessen s.0.
     //excel.activeworkbook.SaveAs(name+'.xls');
     //excel.ActiveWorkBook.Saved := True;
     //
     excel.visible:=true;
     //arbene_vor;
end;

function tcom.excel_addworkbook(wbname:string):boolean;
begin

     excel.Application.SheetsInNewWorkBook := 1; //Die 1 is die Anzahl der Sheets
     Excel.Workbooks.Add;
     Excel.Sheets[1].Name := wbname;
     
     Excel.Visible := true;
end;

procedure tcom.excel_PutStrAt(row,column:integer;s:string);
var r:string;
begin
try
	s:=stringreplace(s,'==',' =',[rfreplaceall]);
	s:=stringreplace(s,'>',' ',[rfreplaceall]);
    Excel.Sheets[1].Cells[row,column].Value:=s;

except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;


procedure tcom.excel_PutintAt(row,column:integer;i:integer);
var r:string;
begin
try
        Excel.Sheets[1].Cells[row,column].Value:=i;

except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;

procedure tcom.excel_PutextAt(row,column:integer;f:real);
var r:string;
begin
try
        Excel.Sheets[1].Cells[row,column].Value:=f;

except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;

function  tcom.excel_getdataAt(row,column:integer):string;
var r:string;
begin
try

  result:= string(Excel.workbooks[1].Sheets[1].Cells[row,column].Value);
except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;

procedure tcom.exel_optimale_hoehe_breite;
begin
	//Excel.workbooks[1].Sheets[1].Cells.Select;
    //Excel.workbooks[1].Sheets[1].Range['A1','Z32000'].activate;
    Excel.workbooks[1].Sheets[1].Range['A1','Z32000'].Columns.AutoFit;
    Excel.workbooks[1].Sheets[1].Range['A1','Z32000'].Rows.AutoFit;

    //Excel.workbooks[1].Sheets[1].Range['A:250'].Rows.AutoFit;;
    //Excel.workbooks[1].Sheets[1].Selection.Columns.AutoFit;
    //Excel.workbooks[1].Sheets[1].Selection.Rows.AutoFit;
end;


procedure tcom.xl_PutStrAt(y,x:integer;s:string);
var r:string;
begin
try
	s:=stringreplace(s,'==',' =',[rfreplaceall]);
	s:=stringreplace(s,'>',' ',[rfreplaceall]);
	r:=xl_zelle(y,x);
	xla.range[r,r].Value:=s;
except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;



procedure tcom.xl_PutintAt(y,x:integer;i:integer);
var r:string;
begin
try
	r:=xl_zelle(y,x);
	xla.range[r,r].Value:=i;
except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;

procedure tcom.xl_PutextAt(y,x:integer;f:real);
var r:string;
begin
try
	r:=xl_zelle(y,x);
	xla.range[r,r].Value:=f;
except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;

function  tcom.xl_getdataAt(y,x:integer):string;
var r:string;
begin
try
r:=xl_zelle(y,x);
result:=string(xla.range[r,r].value);
except
	showmessage('Fenster in Excel ge�ffnet oder Excel nicht vorhanden.');
end;
end;


procedure tcom.word_sichtbar;
begin
wobjekt.visible:=1;
end;

procedure tcom.word_unsichtbar;
begin
wobjekt.visible:=0;
end;

procedure tcom.excel_vordergrund;
var
excelhandle:thandle;
begin
try
    //Excel := GetActiveOleObject('Excel.Application');
    ExcelHandle := Excel.Hwnd; {* Handle wird ermittelt erst ab excel 2007 *}
    Excel.WindowState := SW_SHOWNORMAL; {* Fenster wird aus der Taskleiste geholt *}
    SetForegroundWindow(ExcelHandle); {* Fenster wird in den Vordergrund geholt *}
except
//alte excel version
end;
end;

end.
