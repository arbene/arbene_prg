unit a_rechnung_automatisch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask;

type
  TForm_rechnung_automatisch = class(TForm)
    ComboBox_monat: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn_Probelauf: TBitBtn;
    BitBtn2: TBitBtn;
    SpeedButton_besonderheiten: TSpeedButton;
    MaskEdit_dat: TMaskEdit;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Memo: TMemo;
    Panel_b: TPanel;
    BitBtn_esc: TBitBtn;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_ProbelaufClick(Sender: TObject);
    procedure SpeedButton_besonderheitenClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_rechnung_automatisch: TForm_rechnung_automatisch;

implementation

uses a_main, a_data, a_kalender, a_rechnung;

{$R *.dfm}

procedure TForm_rechnung_automatisch.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
maskedit_dat.text:=datetostr(date());
end;

procedure TForm_rechnung_automatisch.BitBtn_ProbelaufClick(
  Sender: TObject);
  var
  query,monatsfeld,monat:string;
begin
   memo.Lines.clear;
   //monatsfeld:='m_'+inttostr(combobox_monat.ItemIndex+1);
   monat:=inttostr(combobox_monat.ItemIndex+1);
   //query:=format('select * from firma left join rechnung_regeln on (rechnung_regeln.i_firma=firma.nummer) where firma.nummer in (%s) and rechnung_regeln.monat=%s and firma.storno=0 and rechnung_regeln.storno=0 ',[firmen_filter,monat]);
   query:=format('select * from rechnung_regeln left join firma on (rechnung_regeln.i_firma=firma.nummer) where firma.nummer in (%s) and ((%s-rechnung_regeln.monat)%s rechnung_regeln.periode)=0   and firma.storno=0 and rechnung_regeln.storno=0 ',[firmen_filter,monat,'%']);
   datamodul.sql(false,datamodul.q_12,query,'firma');
   datamodul.q_12.First;
   while not datamodul.q_12.eof do
   begin
    memo.Lines.Add(datamodul.q_12.findfield('firma').AsString);
    datamodul.q_12.Next;
   end;

end;

procedure TForm_rechnung_automatisch.SpeedButton_besonderheitenClick(
  Sender: TObject);
begin
     form_kalender.showmodal;
	maskedit_dat.text :=datetostr(int(form_kalender.auswahlkalender.date));
end;

procedure TForm_rechnung_automatisch.BitBtn2Click(Sender: TObject);
var
query,monatsfeld,monat:string;
m,m_a:integer;
begin
   memo.Lines.clear;
   //monatsfeld:='m_'+inttostr(combobox_monat.ItemIndex+1);
   m:=combobox_monat.ItemIndex+1;
   //query:=format('select * from firma where nummer in (%s) and %s=1  and parent=0',[firmen_filter,monatsfeld]);    //keine Kinderfirmen

   monat:=inttostr(combobox_monat.ItemIndex+1);
   //query:=format('select firma.firma, rechnung_regeln.*  from firma left join rechnung_regeln on (rechnung_regeln.i_firma=firma.nummer) where firma.nummer in (%s) and rechnung_regeln.monat=%s and firma.storno=0 and rechnung_regeln.storno=0 and firma.parent=0 ',[firmen_filter,monat]);
   //query:=format('select * from firma left join rechnung_regeln on (rechnung_regeln.i_firma=firma.nummer) where firma.nummer in (%s) and ((%s-rechnung_regeln.monat)%s rechnung_regeln.periode)=0 and firma.storno=0 and rechnung_regeln.storno=0 ',[firmen_filter,monat,'%']);
   query:=format('select * from rechnung_regeln left join firma on (rechnung_regeln.i_firma=firma.nummer) where firma.nummer in (%s) and ((%s-rechnung_regeln.monat)%s rechnung_regeln.periode)=0 and firma.storno=0 and rechnung_regeln.storno=0 ',[firmen_filter,monat,'%']);
   datamodul.sql(false,datamodul.q_12,query,'firma');
   datamodul.q_12.First;
   while not datamodul.q_12.eof do
   begin
    form_rechnung:=tform_rechnung.create(self);
    form_rechnung.ini_rechnungslauf(datamodul.q_12.findfield('nummer').asinteger);   //regel
    form_rechnung.b_f_re:=true;
    form_rechnung.MaskEdit_redat.Text:=maskedit_dat.Text;
    form_rechnung.ComboBox_zeit.Visible:=false;
    m_a:=m-datamodul.q_12.findfield('periode').AsInteger;
    if m_a<1 then m_a:=1;
    form_rechnung.zeiteinstellen_monat(m_a,m);
    form_rechnung.CheckBox_wiedervorlage.checked:=true;
    datamodul.q_rechnung.First;
    if form_rechnung.ShowModal=mrok then memo.Lines.Add('Rechnungserzeugung f�r '+datamodul.q_12.findfield('firma').AsString) else
                                        memo.Lines.Add('Keine Rechnungserzeugung f�r '+datamodul.q_12.findfield('firma').AsString);
    datamodul.q_12.Next;
   end;

end;

end.
