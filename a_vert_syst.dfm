�
 TFORM_VERT_SYST 0!  TPF0TForm_vert_systForm_vert_systLeftpTopWidth�HeightHelpContext9BorderIconsbiSystemMenu CaptionSatelliten SystemeColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight 	TNotebookNotebookLeft TopWidth�Height�AlignalClientTabOrder  TPage Left Top CaptionHaupt TLabelLabel1Left
Top� Width� HeightCaption$Name des Satelliten (mind 5 Zeichen)  TLabelLabel2LeftTopWidthGHeightCaptionNummern-Kreis  TLabelLabel3LeftTop8WidthaHeightCaption   Satelliten - Schlüssel  TLabelLabel7LeftTopWidth� HeightCaptionvorhandene Satelliten-Systeme  TLabelLabel10Left� TopVWidthHeightCaption<   Achtung, jeder Schlüssel darf nur ein mal verwendet werden.  TLabelLabel11Left� TopoWidth)HeightCaption@   Erzeugen Sie bei jeder Neuinstallation einen neuen Schlüssel!!!  TLabelLabel12LeftTopKWidth.HeightCaptionLabel12Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  
TDBgridEXT
DBgridEXT1LeftTop Width� Height� 
DataSourceDataModul.ds_1DefaultDrawing	OptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameNameWidth� Visible	    TBitBtn
BitBtn_newLeft� Top-WidthHeightCaption1   Neuen Schlüssel für  Satelliten-System erzeugenTabOrderOnClickBitBtn_newClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUP UUUUUUWwUUUUUUP�UUUUUU��UUUUUP �UUUUUWwWUUUUUP��UUUUU��WUUUUP ;UUUUWwUuUUUU ��UUUUUwUWUUUUU;UU��_UuUW  �UUWwuwuWUUp��;UUwUWuUuUU����UUUUUUWUUU���UUUU�UUUU��UUUWUu�UUw���UUUu�uUW�UUP�3��UUUW_UUW�UUU��UUUUu��wUUUUW  uUUUUWwwuUUU	NumGlyphs  TDBEditDBEdit2LeftTopWidth� HeightColorclSilver	DataFieldnummern_kreis
DataSourceDataModul.ds_1ReadOnly	TabOrderOnChangeDBEdit2Change  TBitBtn	BitBtn_beLeft� Top� Width� HeightCaptionOKDefault	TabOrderVisibleOnClickBitBtn_beClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TDBEditDBEdit3LeftTopHWidthyHeightColorclSilver	DataField
schluessel
DataSourceDataModul.ds_1ReadOnly	TabOrder  TBitBtn	BitBtn_abLeft�Top� Width� HeightCaptionAbbruchTabOrderVisibleOnClickBitBtn_abClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs  TEditEdit_newLeft
Top� Width� HeightEnabled	MaxLength
TabOrder  	TGroupBox	GroupBox1Left Top�Width�HeightGAlignalBottomCaptionNummern-Kreis des Haupt-SystemsTabOrder 	TSpinEditSpinEdit_mainLeft(TopWidth1HeightMaxValue	MinValue TabOrder Value   TBitBtnBitBtn3LeftxTopWidth� HeightCaption(   Nummernkreis des Hauptsystems verändernTabOrderOnClickBitBtn3Click   TBitBtnBitBtn_writeLeftTop-Width� HeightCaption   Schlüsseldatei schreibenTabOrderOnClickBitBtn_writeClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333 pw 3333wwww3333 �� 3333w�3w3333 �� 3333w��w3333    ?���wwww        wwwwwwww������333337������?����� �̜�w7swwww����9�3?����w�  �𙙓ww77ww�������?���www �  9�3w7ww7w����9�3?��s7w���3y�3w7�?ww3�����3��swws3   33333www33333	NumGlyphs   TPage Left Top CaptionSatellit TLabelLabel4LeftTop0Width.HeightCaption>   Bitte hier den, vom Haupt-System erzeugten Schlüssel eingeben  TLabelLabel5LeftTopWidthMHeightCaptionNummern - Kreis  TLabelLabel6LeftTop� WidthAHeightCaptionSystem Name  TLabelLabel8LeftToppWidth�HeightCaption9Achtung, wenn Sie  ein Satelliten -System erzeugt haben ,Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TLabelLabel9LeftTop� Width*HeightCaption+   können Sie dies nicht rückgängig machen!Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TEditEdit_keyLeftTopPWidth�HeightTabOrder   TBitBtnBitBtn_sat_iniLeftTop� Width� HeightCaption Satelliten-System initialisierenDefault	TabOrderOnClickBitBtn_sat_iniClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TDBEditDBEdit4LeftTop� Width� HeightColor	clBtnFace	DataFieldname
DataSourceDataModul.ds_2ReadOnly	TabOrder  TDBEditDBEdit5LeftTop(WidthyHeightColor	clBtnFace	DataFieldnummern_kreis
DataSourceDataModul.ds_2ReadOnly	TabOrder  TBitBtnBitBtn_openLeft�TopLWidth� HeightCaption   Schlüsseldatei öffnenTabOrderOnClickBitBtn_openClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333�������0 www 37wwwwww?  ��� 37��7�  �� 37�7�  �� 37���  ��� 37www3       ?�����       wwwww ���� 3333 ���� 3333 ���� 3333 ���� 3333 ���� 3333 ����3333w ���� s����s0	���� 37wwwwww3	NumGlyphs   TPage Left Top Caption
system_typ TRadioGroupRadioGroup_wasLeft Top Width�Height� AlignalTopCaption#Bitte Legen Sie den System-Typ festItems.StringsDas System ist das Haupt-System&Das System ist ein Satelliten - System TabOrder OnClickRadioGroup_wasClick  TBitBtnBitBtn4Left� Top� Width� HeightCaptionAbbruchModalResultTabOrder
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs    TPanelPanel2Left Top�Width�HeightAlignalBottomTabOrder  TToolBarToolBar1Left Top Width�HeightAutoSize	CaptionToolBar1ImagesForm_main.ImageList_mainShowCaptions	TabOrder TToolButtonToolButton1Left TopWidthCaptionToolButton1
ImageIndexStyletbsSeparator  TToolButtonToolButton_closeLeftTopHintBeendenAutoSize	Caption	Verlassen
ImageIndexParentShowHintShowHint	OnClickToolButton_closeClick    