unit a_vorlagen;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_main,a_data,
	StdCtrls, Buttons, OleCtnrs, olecontainerext, Mask, DBCtrls, Grids,
  DBGrids, ExtCtrls, ComCtrls, ToolWin,db, TreeView_ext, dbcombo_number,
  FileCtrl,Variants, ExtDlgs, Menus;

type
  TForm_vorlagen = class(TForm)   //help 200..299
    ToolBar_main: TToolBar;
    ToolButton21: TToolButton;
    ToolButton_neu: TToolButton;
    ToolButton9: TToolButton;
    ToolButton_sichern: TToolButton;
	  ToolButton7: TToolButton;
	  ToolButton_delete: TToolButton;
	  ToolButton43: TToolButton;
    Panel_bottom: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn2: TBitBtn;
    ToolButton_edit: TToolButton;
	  ToolButton1: TToolButton;
	  ToolButton2: TToolButton;
    TabControl1: TTabControl;
    Notebook1: TNotebook;
    GroupBox_typ: TGroupBox;
    ComboBox_typ: TComboBox;
    RadioGroup_allgemein: TRadioGroup;
    TreeView: TTreeView_ext;
    Panel_re: TPanel;
    ScrollBox1: TScrollBox;
    Arbeitsmedizin_Vorlagen: TOLeContainerext;
    Panel_li: TPanel;
    dokutitel: TEdit;
    Label2: TLabel;
    Timer1: TTimer;
    OpenDialog: TOpenDialog;
    BitBtn_datwahl: TBitBtn;
    Edit_datname: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label_laenge: TLabel;
    Notebook_beschreibung: TNotebook;
    GroupBox_def: TGroupBox;
    Label1: TLabel;
    DBEdit_firma_text_titel: TDBEdit;
    DBCheckBox_amb: TDBCheckBox;
    Label_kapitel: TLabel;
    DBCombo_num_kapitel: TDBCombo_num;
    GroupBox_wahl: TGroupBox;
    Label6: TLabel;
    Edit_datum: TEdit;
    SpeedButton_dat: TSpeedButton;
    Label5: TLabel;
    Label7: TLabel;
    ComboBox_thema_wahl: TComboBox;
    Label8: TLabel;
    ComboBox_unterschrift_wahl: TComboBox;
    Edit_titel_wahl: TEdit;
    GroupBox_tbs_such: TGroupBox;
    SpeedButton_text_such: TSpeedButton;
    CheckBox_alpha: TCheckBox;
    Edit_text_such: TEdit;
    CheckBox_speichern: TCheckBox;
    PopupMenu_ole: TPopupMenu;
    InfozumDokument1: TMenuItem;
    Timer_open: TTimer;
	  procedure ToolButton_neuClick(Sender: TObject);
	  procedure ToolButton_sichernClick(Sender: TObject);
	  procedure RadioGroup_allgemeinClick(Sender: TObject);
	  procedure ToolButton_deleteClick(Sender: TObject);
	  procedure Arbeitsmedizin_VorlagenLButtonDown(Sender: TObject);
	  procedure FormShow(Sender: TObject);
	  procedure ToolButton_editClick(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure ToolButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid_vorlagenDblClick(Sender: TObject);
    procedure ComboBox_typChange(Sender: TObject);
    procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure TreeViewChanging(Sender: TObject; Node: TTreeNode;var AllowChange: Boolean);
    procedure TreeViewDblClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ComboBox_typClick(Sender: TObject);
    procedure ComboBox_typSelect(Sender: TObject);
    procedure SpeedButton_datClick(Sender: TObject);
    procedure BitBtn_datwahlClick(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
    procedure dokutitelChange(Sender: TObject);
    procedure CheckBox_alphaClick(Sender: TObject);
    procedure Edit_text_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton_text_suchClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure InfozumDokument1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure OpenDialogSelectionChange(Sender: TObject);
    procedure OpenDialogShow(Sender: TObject);

	private
	  { Private-Deklarationen }
       procedure knoepfe_edit_del ;
       procedure ds_sichern;
	public
	  { Public-Deklarationen }
		  i_filter_p,fi:integer;
      f:boolean;
      pfad:string;
      filterindex:integer;
      r_titel:string;
      r_i_kapitel:integer;
      r_i_arzt:integer;
      r_datum:tdate;
		  procedure filtern;

  end;

var
  Form_vorlagen: TForm_vorlagen;

implementation

uses a_vorlagenwahl, a_line_edit, a_dokutitel, a_kalender;

{$R *.DFM}

procedure TForm_vorlagen.ToolButton_neuClick(Sender: TObject);
var
i_firma,bSani,i_filter,i_kapitel:integer;
nr:int64;
rf:integer;
pa,query,s:string;
begin
  ds_sichern;
	if radiogroup_allgemein.itemindex=0 then i_firma:=0 else i_firma:=datamodul.q_Firma['nummer'];

    {form_dokutitel:=tform_dokutitel.create(self);
    form_main.kapitel_lesen(form_dokutitel.combobox.Items,1);
    form_dokutitel.edit.text:='';
		//form_dokutitel.combobox.text:=kapitel;
		form_dokutitel.combobox.ItemIndex:=0;
		form_dokutitel.helpcontext:=2500;
		if form_dokutitel.showmodal<>mrok then exit; }


		form_line_edit:=tform_line_edit.create(self);
		form_line_edit.helpcontext:=2500;
		form_line_edit.CheckBox.Caption:='Sichtbar f�r Ambulanz';
        if (show_ambulanz and (combobox_typ.ItemIndex=1)) then form_line_edit.CheckBox.Visible:=true else form_line_edit.CheckBox.Visible:=false;
		form_line_edit.Caption:='Titel';
		form_line_edit.showmodal;
		if form_line_edit.Edit.text='' then
        begin
        	showmessage('Bitte Dokumentennamen angeben');
        	exit;
        end;
		if form_line_edit.CheckBox.Checked then bsani:=1 else bsani:=0;

        //query:=format('select nummer from kapitel  )

		//bsani:=0;
    { case prg_typ of
			  1,2:
           begin
           s:=combobox_typ.text;
           if trim(s)=  'Dokumente Firma'  then i_filter:=0;
            if trim(s)=  'Dokumente Proband'  then i_filter:=1;
            if trim(s)=  'Bescheinigung Eignungsuntersuchung Firma'  then i_filter:=2;
            if trim(s)=  'Bescheinigung Eignungsuntersuchung Proband'  then i_filter:=3;
            if trim(s)=  'Einladung zu Untersuchungsterminen'  then i_filter:=4;
            if trim(s)=  'Labormitteilung an den Probanden'  then i_filter:=5;
            if trim(s)=  '�bersicht Untersuchunen, Impfungen, Ambulanz'  then i_filter:=6;
            if trim(s)=  'Unfallanzeige'  then i_filter:=7;
            if trim(s)=  'Unfallanzeige - Kurzmeldung'  then i_filter:=8;
            if trim(s)=  'Personalien'  then i_filter:=9;
            if trim(s)=  'Rechnung Firma'  then i_filter:=10;
            if trim(s)=  'Rechnung Proband'  then i_filter:=11;
            if trim(s)=  'Dokument in Abteilung/Gef�hrdungsbeurteilung'  then i_filter:=12;
            if trim(s)=  'Bescheinigung Vorsorge Firma'  then i_filter:=13;
            if trim(s)=  'Bescheinigung Vorsorge Proband'  then i_filter:=14;


           	//i_filter:=combobox_typ.itemindex;
           end;
          3:case combobox_typ.itemindex of
			         0: i_filter:=1;
			         1: i_filter:=10;
            end;
          4:i_filter:=1;
     end; }

     i_filter:=fi;
     rf:=datamodul.q_vorlagen.findfield('reihenfolge').asinteger;
     pa:=datamodul.q_vorlagen.findfield('parent').asstring;
     if pa='' then pa:='0';

	 neuer_datensatz(datamodul.q_vorlagen,['nummer','i_firma','i_filter','name','ole','b_ambulanz','reihenfolge','parent']
     ,[null,i_firma,i_filter,form_line_edit.Edit.text,'',bsani,rf,pa]) ;
     //neuer_datensatz(datamodul.q_vorlagen,['nummer','i_firma','i_filter','name','kapitel','ole','b_ambulanz','reihenfolge','parent']
	 //	,[null,i_firma,i_filter,form_dokutitel.Edit.text,form_dokutitel.combobox.text,'',bsani,rf,pa]) ;
     datamodul.q_vorlagen.post;
     nr:= getbigint(datamodul.q_vorlagen,'nummer');
     
     form_line_edit.Release;
	   //	form_dokutitel.release;
     datamodul.q_vorlagen.tag:=1;
     datamodul.q_vorlagen.disablecontrols;
     treeview.liste_lesen;
     treeview.reihenfolge_neu;
     datamodul.q_vorlagen.EnableControls;
     treeview.suche_nummer(nr);

     datamodul.q_vorlagen.tag:=0;

     form_main.entsperren(panel_re);

     datamodul.q_vorlagen.edit;

  //try
  //olecontainer.CreateObject('word.document',false)
	//except
   //if ole_text=0 then
		Arbeitsmedizin_vorlagen.CreateObject('Word.Document',false);// InsertObjectDialog;  hier abfrage auf openoffice
   //else
   //if ole_text=1 then Arbeitsmedizin_vorlagen.CreateObject('com.sun.star.ServiceManager',false);
	//end;
  {try
    pause(10);
    Arbeitsmedizin_vorlagen.DoVerb(ovshow);
   except
     pause(2000);
     Arbeitsmedizin_vorlagen.DoVerb(ovshow);
   end;}
   oleanzeigen(arbeitsmedizin_vorlagen);
	Arbeitsmedizin_vorlagen.modified:=true;

  	toolbutton_neu.enabled:=false;
    toolbutton_edit.Enabled:=false;
    toolbutton_delete.enabled:=false;
    toolbutton_sichern.enabled:=true;
    combobox_typ.Enabled:=false;
    RadioGroup_allgemein.Enabled:=false;
    knoepfe_edit_del ;
end;

procedure TForm_vorlagen.ToolButton_sichernClick(Sender: TObject);
begin

  application.ProcessMessages;
  ds_sichern;
 {direktspeichern:=true;
 datamodul.sql_memo_post(datamodul.q_vorlagen,Arbeitsmedizin_vorlagen);
 form_main.sperren(panel_li);

 //filtern;   }
 form_main.p_sperren(datamodul.q_vorlagen,datamodul.q_vorlagen,user_id,[panel_re]);
 knoepfe_edit_del ;

end;

procedure TForm_vorlagen.RadioGroup_allgemeinClick(Sender: TObject);
begin
	filtern;
end;

procedure tform_vorlagen.filtern;
var
query,s,rf:string;
ba,i_firma:integer;
begin
if f then exit;

try
f:=true;
datamodul.q_vorlagen.DisableControls;
application.processmessages;

with datamodul do
begin

	fi:=1;
	//if Arbeitsmedizin_vorlagen<>nil then sql_memo_post(q_vorlagen,form_vorlagen.Arbeitsmedizin_vorlagen);
	//q_vorlagen.tag:=1;   201002118  scroll muss immer ausgel�st werden wenn nur 1 DS niccht und wenn schon auf dem ersten steht auch nicht
	//if b.b_objekt('proband-dokumente-arzt')>-1 then ba :=0 else ba:=1;

  if checkbox_alpha.Checked then rf:= 'name'
   else rf:= 'reihenfolge';

  ba:=0;
	if  radiogroup_allgemein.itemindex=0 then i_firma:=0 else i_firma:=q_firma['nummer'];
	if groupbox_typ.Visible then
	begin
     case prg_typ of
     1,2:
     begin
    		 s:=combobox_typ.text;
           if trim(s)=   'Dokumente Firma'  then fi:=0;
            if trim(s)=  'Dokumente Proband'  then fi:=1;
            if trim(s)=  'Bescheinigung Eignungsuntersuchung Firma'  then fi:=2;
            if trim(s)=  'Bescheinigung Eignungsuntersuchung Proband'  then fi:=3;
            if trim(s)=  'Einladung zu Untersuchungsterminen'  then fi:=4;
            if trim(s)=  'Labormitteilung an den Probanden'  then fi:=5;
            if trim(s)=  '�bersicht Untersuchunen, Impfungen, Ambulanz'  then fi:=6;
            if trim(s)=  'Unfallanzeige'  then fi:=7;
            if trim(s)=  'Unfallanzeige - Kurzmeldung'  then fi:=8;
            if trim(s)=  'Personalien'  then fi:=9;
            if trim(s)=  'Rechnung Firma'  then fi:=10;
            if trim(s)=  'Rechnung Proband'  then fi:=11;
            if trim(s)=  'Dokument in Abteilung/Gef�hrdungsbeurteilung'  then fi:=12;
            if trim(s)=  'Bescheinigung Vorsorge Firma'  then fi:=13;
            if trim(s)=  'Bescheinigung Vorsorge Proband'  then fi:=14;
     end;//fi:=combobox_typ.itemindex;
     3:
			case combobox_typ.itemindex of
			0: fi:=1;
			1: fi:=10;
			end;
		4:fi:=1;
		end ;
	end
	else fi:=i_filter_p;

	
	if fi=1 then
	begin
		  //form_main.kapitel_lesen(dbcombobox_kapitel.items,1);
		  //if dbcombobox_kapitel.items.count>=0 then dbcombobox_kapitel.itemindex:=0;
		  dbcombo_num_kapitel.visible:=true;
		  label_kapitel.visible:=true;
  end
  else
  begin
		  //dbcombobox_kapitel.visible:=false;
		  //label_kapitel.visible:=false;
  end;



	query:=format('select * from vorlagen where (i_filter=%d) and (i_firma=%d)  and (b_ambulanz>=%d ) ',
						[fi,i_firma,ba]);


	sql_new(true,datamodul.q_vorlagen,query,rf);
  //if   datamodul.q_vorlagen.RecordCount=1 then  q_vorlagen.tag:=0; // sonst wird kein scroll ausgel�st

  if checkbox_alpha.Checked then treeview.liste_lesen_alphabetisch else treeview.liste_lesen;
  //treeview.liste_lesen;
  if treeview.Items.count=0 then  arbeitsmedizin_vorlagen.DestroyObject;

  if treeview.Items.count>0 then treeview.Selected:=treeview.Items[0];



//  if (combobox_typ.itemindex >=2) and ( datensatz(datamodul.q_vorlagen)) then
  TOOLBUTTon_neu.Enabled:=true;
   combobox_typ.Enabled:=true;
   RadioGroup_allgemein.Enabled:=true;
   if datensatz(datamodul.q_vorlagen) then toolbutton_edit.Enabled:=true;

   toolbutton_delete.Enabled:=false;
   toolbutton_sichern.enabled:=false;
   knoepfe_edit_del;
  q_vorlagen.tag:=0;
  q_vorlagen.Last;
end;
finally
f:=false;
datamodul.q_vorlagen.EnableControls;
datamodul.q_vorlagen.first;
end;
end;


procedure tform_vorlagen.knoepfe_edit_del ;
begin

if form_main.m_erweitertefehlermeldungen.Checked then exit;

case   fi of
	2,3,4,6,7,8,12,13,14:
   begin
       {if datensatz(datamodul.q_vorlagen) and (radiogroup_allgemein.itemindex=0) then
       begin
          TOOLBUTTon_neu.Enabled:=false;
          toolbutton_delete.Enabled:=false;
       end; }
   end;
end;
end;

procedure TForm_vorlagen.ToolButton_deleteClick(Sender: TObject);
begin
	//if MessageDlg('Soll der aktuelle Datensatz gel�scht werden?', mtConfirmation	,[mbYes, mbNo], 0)=mryes then
	begin
     dateiloeschen(datamodul.q_vorlagen, false,false);//datamodul.q_vorlagen.delete;
     form_main.olecontainer_destroy(arbeitsmedizin_vorlagen);

     form_main.sperren(panel_re);

     toolbutton_neu.enabled:=true;
     combobox_typ.Enabled:=true;
     RadioGroup_allgemein.Enabled:=true;
    if datensatz(datamodul.q_vorlagen) then toolbutton_edit.Enabled:=true;
    //toolbutton_edit.Enabled:=true;
     toolbutton_delete.enabled:=false;
     toolbutton_sichern.enabled:=false;
     filtern;
	end;
end;

procedure TForm_vorlagen.Arbeitsmedizin_VorlagenLButtonDown(Sender: TObject);
begin
   oleanzeigen(Arbeitsmedizin_vorlagen);
	//Arbeitsmedizin_vorlagen.DoVerb(ovshow);
end;

procedure TForm_vorlagen.FormShow(Sender: TObject);
begin
	if tag=1 then
   begin
		filtern;
		datamodul.q_vorlagenAfterScroll(datamodul.q_vorlagen);
   end;
  if tag=2 then
  begin
    BitBtn_datwahlClick(Sender);
  end;    
end;

procedure TForm_vorlagen.ToolButton_editClick(Sender: TObject);
var
re:boolean;
//const wtguid: TGUID = '{E2662688-B44A-4229-9E00-EC6218938682}';

begin
if kein_datensatz(datamodul.q_vorlagen) then exit;
try
	//form_main.entsperren(panel_li);
    re:=form_main.p_entsperren(datamodul.q_vorlagen,datamodul.q_vorlagen,user_id,[panel_re],false);
    toolbutton_neu.enabled:=false;
    toolbutton_edit.Enabled:=false;
  	toolbutton_delete.enabled:=true;
    toolbutton_sichern.enabled:=true;
    combobox_typ.Enabled:=false;
    //treeview.enabled:=false;
    RadioGroup_allgemein.Enabled:=false;
    oleanzeigen(Arbeitsmedizin_vorlagen);
    datamodul.q_vorlagen.Edit;
	  //Arbeitsmedizin_vorlagen.DoVerb(ovshow);
except
	showmessage('Kein MS-Word vorhanden oder Olecontainer leer');
end;
  knoepfe_edit_del ;
end;

procedure TForm_vorlagen.FormCreate(Sender: TObject);
begin
	 //grid_einstellen(dbgrid_vorlagen);
    notebook1.PageIndex:=0;
	 form_main.sperren(panel_re);
    f:=false;
	 //datamodul.q_vorlagen.RequestLive:=true;
   form_main.form_positionieren(tform(sender));
   Edit_datum.Text:=datetostr(now());
   edit_titel_wahl.color:= cl_entsperrt;
   combobox_thema_wahl.color:= cl_entsperrt;
   combobox_unterschrift_wahl.color:= cl_entsperrt;
   edit_datum.color:= cl_entsperrt;
	case prg_typ of
	2: dbcheckbox_amb.visible:=false;
	3: begin
	  combobox_typ.Items.Clear;
	  combobox_typ.Items.Add('Dokumente');
	  combobox_typ.Items.Add('Rechnung');
	  radiogroup_allgemein.items[0]:='alle Kunden-Gruppen';
	  radiogroup_allgemein.items[1]:='aktuelle Kunden-Gruppe';
	  dbcheckbox_amb.visible:=false;
	  caption:='Vorlagen ';
	 end;
	 4: begin
	  groupbox_typ.Visible:=false;
	  groupbox_typ.Height:=0;
	  combobox_typ.Items.Clear;
	  combobox_typ.Items.Add('Dokumente');
	  //combobox_typ.Items.Add('Rechnung');
	  radiogroup_allgemein.items[0]:='alle Abteilungen';
	  radiogroup_allgemein.items[1]:='aktuelle Abteilung';
	  dbcheckbox_amb.visible:=false;
	  caption:='Vorlagen ';
	  i_filter_p:=1;
	 end;
	 end;
	 combobox_typ.ItemIndex:=0;

    toolbutton_neu.enabled:=true;
    combobox_typ.Enabled:=true;
    RadioGroup_allgemein.Enabled:=true;
    if datensatz(datamodul.q_vorlagen) then toolbutton_edit.Enabled:=true;
    toolbutton_delete.enabled:=false;
    toolbutton_sichern.enabled:=false;
    form_main.combolesen( combobox_thema_wahl,'kapitel','kapitel','kapitel','');
    form_main.combolesen( combobox_unterschrift_wahl,'untersucher','untersucher','untersucher','angemeldeter User');
    try
      dbcombo_num_kapitel.lookup_laden;
   except
   end;

end;

procedure TForm_vorlagen.ToolButton1Click(Sender: TObject);
begin
try
 direktspeichern:=true;
 ds_sichern;
 form_main.p_sperren(datamodul.q_vorlagen,datamodul.q_vorlagen,user_id,[panel_re]);
 //datamodul.sql_memo_post(datamodul.q_vorlagen,Arbeitsmedizin_vorlagen);
finally
 modalresult:=mrok;
end;
end;

procedure TForm_vorlagen.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   //datamodul.q_vorlagen.RequestLive:=false;
  { form_main.combodeaktivieren(combobox_thema_wahl);
   form_main.combodeaktivieren(combobox_unterschrift_wahl );
   treeview.liste_loeschen;
   arbeitsmedizin_vorlagen.Destroyobject; //.Close; }
end;

procedure TForm_vorlagen.DBGrid_vorlagenDblClick(Sender: TObject);
begin
if not toolbar_main.Visible then modalresult:=mrok;
end;

procedure TForm_vorlagen.ComboBox_typChange(Sender: TObject);
begin
datamodul.q_vorlagen.DisableControls;
ds_sichern;
form_main.p_sperren(datamodul.q_vorlagen,datamodul.q_vorlagen,user_id,[panel_re]);
datamodul.q_vorlagen.EnableControls;
end;

procedure TForm_vorlagen.TreeViewChange(Sender: TObject; Node: TTreeNode);
var
i:integer;
begin
///////////////
if treeview.gesperrt then exit;
if (not assigned(form_vorlagen))  then exit;
treeview.gesperrt:=true;

 	form_main.olecontainer_laden(datamodul.q_vorlagen,'ole',form_vorlagen.Arbeitsmedizin_vorlagen);

////////////////
  edit_titel_wahl.text:=datamodul.q_vorlagen.findfield('name').asstring;
  i:=datamodul.q_vorlagen.findfield('i_kapitel').asinteger;
  form_main.combogehezu(combobox_thema_wahl,i);
  form_main.combogehezu(combobox_unterschrift_wahl,zugeordneter_arzt);
treeview.gesperrt:=false;
end;


procedure TForm_vorlagen.TreeViewChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
if treeview.gesperrt then exit;
  ds_sichern;   //hier nur q_vorlagen speichern
  form_main.p_sperren(datamodul.q_vorlagen,datamodul.q_vorlagen,user_id,[panel_re]);
  form_main.olecontainer_destroy(arbeitsmedizin_vorlagen);
end;

procedure tform_vorlagen.ds_sichern;
begin
 direktspeichern:=true;
 if (Notebook_beschreibung.pageindex=0) and form_vorlagen.Visible  then      //NOTL�SUNG DA DAS DIE DB-KOMONENTE NI CHT AUF DIE DB SCHREIBT
 begin
 try
  DBEdit_firma_text_titel.SetFocus;
  DBCombo_num_kapitel.SetFocus;
  DBCheckBox_amb.SetFocus;
 except
 end;
 end;
  /////////////////

  if (datamodul.q_vorlagen.State in [dsedit, dsinsert]) then
   begin
    if (not direktspeichern) then
     begin
        //if (MessageDlg('Speichern der Vorlage?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
       form_main.olecontainer_speichern(true,datamodul.q_vorlagen,'ole',form_vorlagen.Arbeitsmedizin_vorlagen);
    end
    else
    begin

      form_main.olecontainer_speichern(true,datamodul.q_vorlagen,'ole',form_vorlagen.Arbeitsmedizin_vorlagen);
    end;
     direktspeichern:=false;

    datamodul.sql_memo_post(datamodul.q_vorlagen,Arbeitsmedizin_vorlagen);
    form_main.formular_sperren;
    olecontainer_close(arbeitsmedizin_vorlagen);
  //datamodul.q_vorlagen.Post;
 end;


//////////////////



  toolbutton_neu.enabled:=true;
  combobox_typ.Enabled:=true;
  RadioGroup_allgemein.Enabled:=true;
  //treeview.enabled:=true;
  if datensatz(datamodul.q_vorlagen) then toolbutton_edit.Enabled:=true;
  toolbutton_delete.enabled:=false;
  toolbutton_sichern.enabled:=false;
end;


procedure TForm_vorlagen.TreeViewDblClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_vorlagen.TabControl1Change(Sender: TObject);
var
r:boolean;
begin
notebook1.pageindex:=tTabControl(sender).tabindex ;
if tTabControl(sender).tabindex=1 then BitBtn_datwahlClick(Sender);

end;

procedure TForm_vorlagen.ComboBox_typClick(Sender: TObject);
begin
datamodul.q_vorlagen.DisableControls;
 ds_sichern;
 form_main.p_sperren(datamodul.q_vorlagen,datamodul.q_vorlagen,user_id,[panel_re]);
 //arbeitsmedizin_vorlagen.Close;
datamodul.q_vorlagen.EnableControls;
end;

procedure TForm_vorlagen.ComboBox_typSelect(Sender: TObject);
begin
	filtern;
end;

procedure TForm_vorlagen.SpeedButton_datClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
     edit_datum.text:= datetostr(form_kalender.auswahlkalender.date);
end;

procedure TForm_vorlagen.BitBtn_datwahlClick(Sender: TObject);
var
r:boolean;
begin
application.ProcessMessages;
if pfad='' then pfad:=pfad_vorlagen;
IF form_vorlagen.Tag=2 then pfad:=pfad_vorlagen;
opendialog.InitialDir:=pfad;
 opendialog.FilterIndex :=filterindex;
 r:=opendialog.Execute;

if r=true then
begin

  edit_datname.text:= opendialog.FileName;
  pfad:=wurzelpfad( opendialog.filename);
  dokutitel.text:= dateiname(opendialog.FileName);
  filterindex:=opendialog.filterindex ;
  //if length( dokutitel.text) <=80 then
  //modalresult:=mrok;
   timer1.Enabled:=true;

end
else
begin
   dokutitel.text:= opendialog.FileName;
end;

end;

procedure TForm_vorlagen.BitBtn_okClick(Sender: TObject);
begin
if notebook1.activepage='dat' then
begin
  //if length(dokutitel.Text)>50 then showmessage('Bitte Datei')
end;
modalresult:=mrok;
end;

procedure TForm_vorlagen.dokutitelChange(Sender: TObject);
begin
if length(dokutitel.text)>80 then label_laenge.Caption:= format('Bitte k�rzen um %d Zeichen',[length(dokutitel.text)-80])
else label_laenge.Caption:=''; 
end;

procedure TForm_vorlagen.CheckBox_alphaClick(Sender: TObject);
begin
	filtern;
end;

procedure TForm_vorlagen.Edit_text_suchKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=vk_return then SpeedButton_text_suchClick(sender);
end;

procedure TForm_vorlagen.SpeedButton_text_suchClick(Sender: TObject);
begin
treeview.suche_datensatz(edit_text_such.text);
end;

procedure TForm_vorlagen.FormDestroy(Sender: TObject);
begin
  form_main.combodeaktivieren(combobox_thema_wahl);
   form_main.combodeaktivieren(combobox_unterschrift_wahl );
   treeview.liste_loeschen;
   arbeitsmedizin_vorlagen.Destroyobject; //.Close;
end;

procedure TForm_vorlagen.Timer1Timer(Sender: TObject);
begin
  timer1.Enabled:=false;
  modalresult:=mrok;
end;

procedure TForm_vorlagen.InfozumDokument1Click(Sender: TObject);
begin
oleinfo(arbeitsmedizin_vorlagen);
end;

procedure TForm_vorlagen.BitBtn1Click(Sender: TObject);
begin
  if Messagedlg('Sollen die aktuelle Vorlage zur�ckgesetzt werden?',mtConfirmation, mbOkCancel	,0)<>mrOK  then exit;
  
end;

procedure TForm_vorlagen.OpenDialogSelectionChange(Sender: TObject);
begin
timer_open.Enabled:=false;
end;

procedure TForm_vorlagen.OpenDialogShow(Sender: TObject);

begin
  timer_open.Enabled:=true;

end;

end.
