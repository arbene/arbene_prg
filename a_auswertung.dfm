�
 TFORM_AUSWERTUNG 0��  TPF0TForm_auswertungForm_auswertungLeft�Top� Width�Height�HelpContext$Caption
AuswertungColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Menu	MainMenu1OldCreateOrderScaledOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TToolBarToolBar1Left Top Width�Height!ButtonHeightButtonWidth(CaptionToolBar1ImagesForm_main.ImageList_mainTabOrder  TToolButtonToolButton_neuLeft TopHintNeuCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonToolButton2Left(TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeft7TopHint   LöschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_deleteClick  TToolButtonToolButton3Left_TopWidthCaptionToolButton3
ImageIndexStyletbsSeparator  TToolButtonToolButton1LeftnTopCaptionToolButton1
ImageIndexOnClickToolButton1Click   TPageControlPageControl_auswertungLeft Top!Width�HeightV
ActivePageTabSheet_ergebnisAlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnChangePageControl_auswertungChange
OnChangingPageControl_auswertungChanging 	TTabSheettabsheet_filternCaption'   Welche Kriterien müssen erfüllt sein?
ImageIndex 	TGroupBox	GroupBox1Left Top WidthAHeight:AlignalLeftCaption   Doppelklick zum auswählenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel8LeftTop�Width=Height\AlignalBottom
BevelOuterbvNoneTabOrder  TSpeedButtonSpeedButton5Left� Top	WidthHeight
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� ������������������������ �������������� � ������������ � � ���������  ��� � ������� ������ � ����� � ������ � ��  ��� ������ � � ����� ������ � � ����� ������ ��� ����� ���  ����� �����   �������� ��� ������������   ����������������������������������������OnClickSpeedButton5Click  TSpeedButtonSpeedButton7Left� Top	WidthHeight
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� �������������������������������������� �������������� � ������������ ��� ������������ ��� ������������ ��� ����������� ��� ����������� ��� ������� ��� ��� �������� �� � ��������  ��� �����������������������������������������������������������������OnClickSpeedButton7Click  TLabelLabel25LeftTopWidth\HeightCaption   mit "und" verknüpft  TLabelLabel29LeftTop8Width� HeightCaption$   für die die Abfrage nicht zutrifft.  TLabelLabel30LeftTopIWidth� HeightCaption)   (die Verneinung gilt ab Befunde abwärts)  TBitBtn	BitBtn_e1Left� TopWidth`HeightCaption	ErweitertTabOrder OnClickBitBtn_e1Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�333333Ds333333w��33333tDs333337w?�33334DDs33337?w?�3333DDDs3333s�w?�333tDDDs3337�3w?�334DDDDs337?33w?�33DDDDDs?����w���������wwwwww3w33�����333s33?w33<����3337�3?w333<���333373?w3333���33333s?w3333<��333337�w33333<�3333337w333333�3333333w3333333LayoutblGlyphRight	NumGlyphs  	TCheckBoxCheckBox_negativLeft	Top&Width)HeightCaption%   Das Ergebnis enthält die Probanden, TabOrderOnClickCheckBox_negativClick   	TTreeViewTreeViewLeftTopWidth=Height�Hint   Doppelklick zum auswählenAlignalClientHideSelectionIndentParentShowHintReadOnly	RightClickSelect	ShowHint	StateImagesForm_main.ImageList_bereichTabOrderOnChangeTreeViewChange
OnChangingTreeViewChanging
OnDblClickToolButton_neuClick   	TGroupBox	GroupBox2LeftTop Width�Height:AlignalClientCaptionKriterium spezifizierenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder 	TNotebookNotebookLeftTopWidth�Height)HelpContextAlignalClient	PageIndex
TabOrder  TPage Left Top Captionleer  TPage Left Top Captionstring TDBTextDBText3Left� Top� WidthAHeight	DataField	s_einheit
DataSource!DataModul.DataSource_sql_abfragen  TDBEditDBEdit_wertLeft� Top� WidthAHeight	DataFieldWert
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TDBComboBoxDBComboBox3Left8Top� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder   TPage Left Top CaptionWert TLabelLabel1LeftTop� Width1HeightCaption<"%" vor oder nach dem  Wort bedeutet: vor oder nach dem Wort  TLabelLabel2LeftTop� Width� HeightCaption-   dürfen weitere Zeichen stehen  z.B. "%wort%"  TDBEditDBEdit_stringLeft`Top� Width� Height	DataFieldString
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TDBComboBoxDBComboBox2LeftTop� Width1HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<>like TabOrder   TPage Left Top CaptionAuswahlliste TDBCheckBoxDBCheckBox1Left Top@WidthaHeightCaptionAlle 	DataFieldalle
DataSource!DataModul.DataSource_sql_abfragenTabOrder ValueChecked1ValueUnchecked0  TDBCombo_numDBCombo_num_befund_listeLeft"TophWidth/Heightalle_einfuegenkeines_einfuegen	DataFieldliste
DataSource!DataModul.DataSource_sql_abfragenzconnectionDataModul.connection_mainzfield_indexnummerzstorno
ItemHeightTabOrder   TPage Left Top CaptionZeit TLabelLabel9Left7Top9Width4HeightCaption	Datum von  TLabelLabel10Left7TopyWidth/HeightCaption	Datum bis  TSpeedButtonSpeedButton3Left� TopIWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton3Click  TSpeedButtonSpeedButton1Left� Top� WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  TDBEditDBEdit1Left7TopIWidth� Height	DataFieldDatum_Beginn
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TDBEditDBEdit2Left7Top� Width� Height	DataField
Datum_Ende
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TPage Left Top CaptionICD TLabelLabel19LeftTop� Width-HeightCaption
   Schlüssel  TLabelLabel20LeftxTop� WidthHeightCaptionText  TDBEditDBEdit_icd_schluesselTagLeftTop� WidthQHeightColor	clBtnFace	DataFieldICD_Schluessel
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder   TBitBtnBitBtn2LeftTopHWidth1HeightCaption   ICD-Schlüssel wählenTabOrderOnClickBitBtn2Click  TDBEditDBEdit5TagLeftxTop� Width� HeightColor	clBtnFace	DataFieldString
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder   TPage Left Top CaptionBesonderheiten TLabelLabel27LeftTop� WidthUHeightCaptionTrifft zu oder nicht  TDBEditDBEdit_bes_hauptkriteriumTagLeftTop� Width.HeightColor	clBtnFace	DataFieldString
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder   TBitBtnBitBtn_bes_hauptLeftTop@Width.HeightCaptionBesonderheiten -HauptkriteriumTabOrderOnClickBitBtn_bes_hauptClick  TDBComboBoxDBComboBox5LeftTop� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder   TPage Left Top CaptionBesonderheit_child TLabelLabel26LeftTop� WidthUHeightCaptionTrifft zu oder nicht  TBitBtnBitBtn3LeftTop@Width4HeightCaptionBesonderheiten -NebenkriteriumTabOrder OnClickBitBtn3Click  TDBEditDBEdit_besonder_nebenTagLeftTop� Width4HeightColor	clBtnFace	DataFieldString
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder  TDBComboBoxDBComboBox6LeftTop� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder   TPage Left Top Captionbefund_leer TDBRadioGroupDBRadioGroup3Left0Top0WidthHeightiCaption
   Auffällig	DataField
Auffaellig
DataSource!DataModul.DataSource_sql_abfragenItems.Strings   nicht ausgewähltNormalbefund
   Auffällig TabOrder Values.Strings-101    TPage Left Top Captionbefund_wert TDBTextDBText2Left� Top� WidthAHeight	DataField	s_einheit
DataSource!DataModul.DataSource_sql_abfragen  TDBEditDBEdit3Left� Top� WidthaHeight	DataFieldWert
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TDBComboBoxDBComboBox7Left8Top� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder  TDBRadioGroupDBRadioGroup2Left0Top0WidthHeightiCaption
   Auffällig	DataField
Auffaellig
DataSource!DataModul.DataSource_sql_abfragenItems.Strings   nicht ausgewähltNormalbefund
   Auffällig TabOrderValues.Strings-101    TPage Left Top Captionbefund_string TDBLookupComboBoxDBLookupComboBox_befund_listeLeft(Top� WidthHeight	DataFieldListe
DataSource!DataModul.DataSource_sql_abfragenKeyFieldNummerTabOrder   TDBRadioGroupDBRadioGroup1Left(Top8WidthHeightiCaption
   Auffällig	DataField
Auffaellig
DataSource!DataModul.DataSource_sql_abfragenItems.Strings   nicht ausgewähltNormalbefund
   Auffällig TabOrderValues.Strings-101    TPage Left Top Captionbefund_liste TLabelLabel3Left8TopHWidth8HeightCaptionDatum Start  TLabelLabel4Left8Top� Width;HeightCaption
Datum Ende  TSpeedButtonSpeedButton2Left� TopYWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton3Click  TSpeedButtonSpeedButton4Left� Top� WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  TDBEditDBEdit4Left8TopXWidth� Height	DataFieldDatum_Beginn
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TDBEditDBEdit6Left8Top� Width� Height	DataField
Datum_Ende
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TPage Left Top Captionbefund_Blutdruck TLabelLabel102LeftTopWidth-Height	AlignmenttaRightJustifyCaption	BlutdruckFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel103Left� Top� WidthHeightCaption/Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel104Left� Top� Width� HeightCaption!systolisch    /       diastolisch  TLabelLabel5Left TopWidth3HeightCaptionA   Falls Werte für Systolikum, Diastolikum und Puls angegeben sind,  TLabelLabel6Left Top Width� HeightCaption+   müssen alle drei das Kriterium  erfüllen.  TLabelLabel7Left Top@WidthHeightCaption>Ist ein Wert "0", so spielt er bei der Auswertung keine Rolle.  TLabelLabel15Left1Top� WidthHeightCaptionPuls  TDBEditDBEdit_sLeft� Top� Width1Height	DataFieldi_1
DataSource!DataModul.DataSource_sql_abfragenTabOrder  TDBEditDBEdit_dLeft� Top� Width1Height	DataFieldi_2
DataSource!DataModul.DataSource_sql_abfragenTabOrder  TDBComboBoxDBComboBox_rrLeft-Top� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder  TDBRadioGroupDBRadioGroup5Left)Top0Width� HeightiCaption
   Auffällig	DataField
Auffaellig
DataSource!DataModul.DataSource_sql_abfragenItems.Strings   nicht ausgewähltNormalbefund
   Auffällig TabOrder Values.Strings-101   TDBEditDBEdit_pulsLeft0Top� Width1Height	DataFieldi_3
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TPage Left Top CaptionImpfung TLabelLabel18Left"ToppWidth"HeightCaptionImpftyp  TLabel	ImpfstoffLeft Top� Width(HeightCaption	Impfstoff  TDBLookupComboBoxDBLookupComboBox_impfung_typTagLeft TopxWidthHeightColor	clBtnFace	DataFieldListe
DataSource!DataModul.DataSource_sql_abfragenDropDownRows EnabledKeyFieldNummer	ListFieldUntersuchung
ListSourceDataModul.ds_typReadOnly	TabOrder 	OnCloseUp#DBLookupComboBox_impfung_typCloseUp
OnDropDown$DBLookupComboBox_impfung_typDropDown  TDBLookupComboBoxDBLookupComboBox_impfung_stoffLeft Top� WidthIHeight	DataFieldWert
DataSource!DataModul.DataSource_sql_abfragenKeyFieldNummer	ListField	Impfstoff
ListSourceDataModul.ds_impfstoffTabOrderVisibleOnEnter#DBLookupComboBox_impfung_stoffEnterOnExit"DBLookupComboBox_impfung_stoffExit  TDBCheckBoxDBCheckBox_impf_allLeft TopHWidthqHeightCaptionAlle Impfungen	DataFieldalle
DataSource!DataModul.DataSource_sql_abfragenTabOrderValueChecked1ValueUnchecked0  TBitBtnBitBtn_impfTag
Left� TopFWidth� HeightCaption   Impfung auswählenTabOrderOnClickBitBtn_impfClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� U    UUUwwww�UU    �UUUwwwww�UUP���UUW���w�UP� �0�UU��w��w�P   �3 UWwww��wUP��� 3UW���w�UP� ��3UW�wW��UP����3UW�UW��UP��� 3UW���w�UP   p3UWwwwW�UUP��p3UUW����UU    UUwwwwUUwww UUu�UUwUUP���UUWu�UW�UUP;; UUUWu��w�UUUp   UUUUWwwwU	NumGlyphs  TDBEditDBEdit9Left Top� WidthIHeightColor	clBtnFace	DataFieldstring
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder   TPage Left Top Captionlabor TDBLookupComboBoxdblookupcombobox_LISTE_laborTagLeftTop�Width.HeightColor	clBtnFace	DataFieldListe
DataSource!DataModul.DataSource_sql_abfragenDropDownRows EnabledKeyFieldNummer	ListFieldUntersuchung
ListSourceDataModul.ds_typReadOnly	TabOrder Visible	OnCloseUp#dblookupcombobox_LISTE_laborCloseUp
OnDropDown$dblookupcombobox_LISTE_laborDropDown  TDBCheckBoxDBCheckBox_labor_allLeftTop<Width� HeightCaptionAlle Laboruntersuchungen	DataFieldalle
DataSource!DataModul.DataSource_sql_abfragenTabOrderValueChecked1ValueUnchecked0  TBitBtn
BitBtn_labTag
Left� Top6Width� HeightCaption   Labor auswählenTabOrderOnClickBitBtn_labClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� U    UUUwwww�UU    �UUUwwwww�UUP���UUW���w�UP� �0�UU��w��w�P   �3 UWwww��wUP��� 3UW���w�UP� ��3UW�wW��UP����3UW�UW��UP��� 3UW���w�UP   p3UWwwwW�UUP��p3UUW����UU    UUwwwwUUwww UUu�UUwUUP���UUWu�UW�UUP;; UUUWu��w�UUUp   UUUUWwwwU	NumGlyphs  	TGroupBoxGroupBox_labwertLeft$Top� WidtheHeightICaptionWertTabOrder TDBTextDBText1Left� Top WidthAHeight	DataField	s_einheit
DataSource!DataModul.DataSource_sql_abfragen  TDBComboBoxDBComboBox_relationLeftTopWidth9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder   TDBEditDBEdit_laborwertLeftHTopWidthiHeight	DataFieldWert
DataSource!DataModul.DataSource_sql_abfragenTabOrder   TDBEditDBEdit7LeftTophWidthiHeightColor	clBtnFace	DataFieldstring
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder   TPage Left Top Captionuntersuchung TBitBtnBitBtn_untwahlTag
Left� TopFWidth� HeightCaption   Vorsorge-Typ auswählenTabOrder OnClickBitBtn_untwahlClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� U    UUUwwww�UU    �UUUwwwww�UUP���UUW���w�UP� �0�UU��w��w�P   �3 UWwww��wUP��� 3UW���w�UP� ��3UW�wW��UP����3UW�UW��UP��� 3UW���w�UP   p3UWwwwW�UUP��p3UUW����UU    UUwwwwUUwww UUu�UUwUUP���UUWu�UW�UUP;; UUUWu��w�UUUp   UUUUWwwwU	NumGlyphs  TDBLookupComboBoxDBLookupComboBox_untTagLeft(Top0Width'HeightColor	clBtnFace	DataFieldliste
DataSource!DataModul.DataSource_sql_abfragenDropDownRows�KeyFieldnummer	ListFielduntersuchung
ListSourceDataModul.ds_typTabOrderVisible	OnCloseUp#dblookupcombobox_LISTE_laborCloseUp  TDBCheckBoxDBCheckBox2LeftTopKWidth� HeightCaptionAlle Vorsorgen	DataFieldalle
DataSource!DataModul.DataSource_sql_abfragenTabOrderValueChecked1ValueUnchecked0  TDBEditDBEdit8Left Top� WidthYHeightColor	clBtnFace	DataFieldstring
DataSource!DataModul.DataSource_sql_abfragenReadOnly	TabOrder   TPage Left Top CaptionBoolean TDBRadioGroupDBRadioGroup4Left8TopXWidth� Heighti	DataFieldwert
DataSource!DataModul.DataSource_sql_abfragenItems.StringsKriterium trifft zuKriterium trifft NICHT zu TabOrder Values.Strings10    TPage Left Top Captionarchiv TDBRadioGroupDBRadioGroup6Left8TopXWidth� Heighti	DataFieldwert
DataSource!DataModul.DataSource_sql_abfragenItems.Stringsaktive Probandenarchivierte Probanden TabOrder Values.Strings01    TPage Left Top Captionfirma TListBoxListBox_firmaTagLeftaTopAWidthHeight�AlignalClientColor	clBtnFace
ItemHeightTabOrder   TPanelPanel5Left Top Width�HeightAAlignalTop
BevelOuterbvNoneTabOrder TBitBtnBitBtn1LeftaTop Width� HeightCaption   Firmen auswählenTabOrder OnClickBitBtn1Click   TPanelPanel6Left Top Width�Height)AlignalBottom
BevelOuterbvNoneTabOrder  TPanelPanel7LeftqTopAWidthXHeight�AlignalRight
BevelOuterbvNoneTabOrder  TPanelPanel9Left TopAWidthaHeight�AlignalLeft
BevelOuterbvNoneTabOrder   TPage Left Top CaptionBMI TLabelLabel11Left� Top� Width� HeightCaption.   Grösse                 Gewicht                 TLabelLabel12Left@TopWidth� HeightCaption   Grösse / Gewicht / BMI / WHtR  TLabelLabel8Left;TopWidth� HeightCaption5   Falls Werte für Grösse und Gewicht  angegeben sind,  TLabelLabel13Left;Top Width� HeightCaption,   müssen beide das Kriterium (><=) erfüllen.  TLabelLabel14Left;Top@WidthHeightCaption>Ist ein Wert "0", so spielt er bei der Auswertung keine Rolle.  TLabelLabel28Left=Top`Width� HeightCaption1   BMI und WHtR können nicht als Kriterium angeben   TDBEditDBEdit_groesseLeft� Top� Width1Height	DataFieldf_1
DataSource!DataModul.DataSource_sql_abfragenTabOrderOnChangeDBEdit_groesseChange  TDBEditDBEdit_gewichtLeft� Top� Width1Height	DataFieldf_2
DataSource!DataModul.DataSource_sql_abfragenTabOrderOnChangeDBEdit_groesseChange  TDBComboBoxDBComboBox1Left=Top� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder  TDBRadioGroupDBRadioGroup_bmiLeft9Top(Width� HeightiCaption
   Auffällig	DataField
Auffaellig
DataSource!DataModul.DataSource_sql_abfragenItems.Strings   nicht ausgewähltNormalbefund
   Auffällig TabOrder Values.Strings-101   TDBEdit
DBEdit_bmiLeft.Top� Width1Height	DataFieldf_3
DataSource!DataModul.DataSource_sql_abfragenTabOrderVisible   TPage Left Top CaptionLufu TLabelLabel16Left� Top� Width� HeightCaption4FEV1                  FVC                  FEV1/FVC   TLabelLabel17Left@TopWidthJHeightCaptionLungenfunktion  TLabelLabel21Left;TopWidthHeightCaption7   Falls Werte für FEV1, FVC und FEV1/FVC angegeben sind,  TLabelLabel22Left;Top Width� HeightCaption0   müssen alle drei das Kriterium (><=) erfüllen.  TLabelLabel23Left;Top@WidthHeightCaption>Ist ein Wert "0", so spielt er bei der Auswertung keine Rolle.  TDBEdit
DBEdit_fevLeft� Top� Width1Height	DataFieldf_1
DataSource!DataModul.DataSource_sql_abfragenTabOrderOnChangeDBEdit_groesseChange  TDBEdit
DBEdit_fvcLeft� Top� Width1Height	DataFieldf_2
DataSource!DataModul.DataSource_sql_abfragenTabOrderOnChangeDBEdit_groesseChange  TDBComboBoxDBComboBox4Left=Top� Width9HeightStylecsDropDownList	DataFieldRelation
DataSource!DataModul.DataSource_sql_abfragen
ItemHeightItems.Strings=<> TabOrder  TDBRadioGroupDBRadioGroup7Left9Top(Width� HeightiCaption
   Auffällig	DataField
Auffaellig
DataSource!DataModul.DataSource_sql_abfragenItems.Strings   nicht ausgewähltNormalbefund
   Auffällig TabOrder Values.Strings-101   TDBEditDBEdit_fev_fvcLeft.Top� Width1Height	DataFieldf_3
DataSource!DataModul.DataSource_sql_abfragenTabOrder     	TGroupBoxGroupBox_erweitertLeftATop Width� Height:AlignalLeftFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible 
TDBgridEXT
DBgridEXT2LeftTopWidth� Height�AlignalClient
DataSource!DataModul.DataSource_sql_abfragenDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamenameTitle.CaptionNameWidth� Visible	    TPanelPanel4LeftTop�Width� Height\AlignalBottom
BevelOuterbvNoneTabOrder TLabelLabel24LeftTopWidth_HeightCaption   mit "oder" verknüpft     	TTabSheetTabSheet_anzeigeCaption&Welche Felder sollen angezeigt werden?
ImageIndex TPanelPanel_anzeige_liLeft Top Width�Height:AlignalClient
BevelOuterbvNoneCaptionPanel_anzeige_liTabOrder  	TGroupBox	GroupBox4LeftTop Width�Height:AlignalClientCaptionHFelder die angezeigt werden bzw. nach denen sortiert wird (re Maustaste)Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TTreeView_extTreeLeftTop WidthnHeight�DragModedmAutomaticIndentReadOnly	TabOrderbereich_feldnamebereichbereich_wertbereich_wert2 
mysqlqueryDataModul.q_typ_tree
mysql_felduntersuchung  TTreeView_extTree_anzeigeLeftTopWidth�Height)Hint   Doppelklick zum auswählenAlignalClientDragModedmAutomaticIndentParentShowHint	PopupMenu	PopupMenuReadOnly	RightClickSelect	ShowHint	StateImagesForm_main.ImageList_bereichTabOrder 
OnDblClickTree_anzeigeDblClickbereich_wert bereich_wert2 
mysqlqueryDataModul.q_a1
mysql_feldfeldname   TPanelPanel3Left Top WidthHeight:AlignalLeft
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TSpeedButtonSpeedButton6Left� Top� Width!Height
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� ������������������������ �������������� � ������������ � � ���������  ��� � ������� ������ � ����� � ������ � ��  ��� ������ � � ����� ������ � � ����� ������ ��� ����� ���  ����� �����   �������� ��� ������������   ����������������������������������������OnClickSpeedButton6Click  TSpeedButtonSpeedButton8Left� Top� Width!Height
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� �������������������������������������� �������������� � ������������ ��� ������������ ��� ������������ ��� ����������� ��� ����������� ��� ������� ��� ��� �������� �� � ��������  ��� �����������������������������������������������������������������OnClickSpeedButton8Click  TRadioGroupRadioGroup_anzeigeartLeft Top WidthHeight� AlignalTopCaptionArt der AbfrageFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	ItemIndex Items.Strings   alle Datensätze anzeigen   Anzahl gleicher Datensätze)   Anzahl gleicher Datensätze (aufsteigend)(   Anzahl gleicher Datensätze (absteigend) 
ParentFontTabOrder      	TTabSheetTabSheet_ergebnisCaptionErgebnis
ImageIndex 
TDBgridEXT
DBgridEXT1Left Top Width�HeightAlignalClient
DataSourceDataModul.ds_a2DefaultDrawing	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit 
ParentFontReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT1DblClickkeine_farbeZeitdiff_L_B darf_ziehen  TPanelPanel1Left TopWidth�Height"AlignalBottomFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel_exportLeft�TopWidth<HeightCaption                      TLabelLabel_anzahlLeftTopWidth&HeightCaptionAnzahl:   TBitBtnBitBtn_exportLeftfTopWidth� HeightCaptionExportieren nach ExcelTabOrder OnClickBitBtn_exportClick
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� �(((((((��������(������(�����((�(򂂂�(��((/(/�(��(��(/((/�(����(��/(((��(�򂂂�(��((�(/�(����(��������((((((((��������     
TPopupMenu	PopupMenuImagesForm_main.ImageList_bereichLeft�Top 	TMenuItem	anzeigen1Captionanzeigen
ImageIndexOnClickanzeigen1Click  	TMenuItemsortierenaufsteigen1Captionsortieren aufsteigen
ImageIndexOnClicksortierenaufsteigen1Click  	TMenuItemsortierenabsteigend1Captionsortieren absteigend
ImageIndexOnClicksortierenabsteigend1Click  	TMenuItemdemarkieren1CaptiondemarkierenOnClickdemarkieren1Click   	TMainMenu	MainMenu1Left  	TMenuItemDatei1CaptionDatei 	TMenuItemm_AbfrageladenCaptionAbfrage ladenOnClickm_AbfrageladenClick  	TMenuItemm_AbfragespeichernCaptionAbfrage speichernOnClickm_AbfragespeichernClick  	TMenuItemm_AbfragenbearbeitenCaptionAbfragen bearbeitenOnClickm_AbfragenbearbeitenClick  	TMenuItemAbfragezurcksetzen1Caption   Abfrage zurücksetzenOnClickAbfragezurcksetzen1Click  	TMenuItemN1Caption-  	TMenuItemAuswertungverlassen1CaptionAuswertung verlassenOnClickAuswertungverlassen1Click   	TMenuItemBearbeiten1CaptionBearbeiten  	TMenuItem	FilterneuCaption
Filter neuOnClickFilterneuClick  	TMenuItemFilterloeschenCaption   Filter löschenOnClickFilterloeschenClick   	TMenuItemeinfacheAbfragen1CaptionAbfragenOnClickeinfacheAbfragen1Click  	TMenuItemm_toolsCaptionTools 	TMenuItemm_update_fehlgeschlagenCaption:Fehlgeschlagen Anweisungen bei der DatenbankaktualisierungOnClickm_update_fehlgeschlagenClick  	TMenuItem"m_ProtokollDatenbankaktualisierungCaption!Protokoll DatenbankaktualisierungOnClick'm_ProtokollDatenbankaktualisierungClick  	TMenuItemm_AnmeldeprotokollCaptionAnmeldeprotokollOnClickm_AnmeldeprotokollClick    
TPopupMenuPopupMenu_ergebnisLeft�Top 	TMenuItem1GehezuMitarbeiterNameVornameGeburtsdatumauswhlen1CaptionGehe zum Mitarbeiter OnClickDBgridEXT1DblClick    