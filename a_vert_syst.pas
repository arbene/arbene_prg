unit a_vert_syst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, DBCtrls, Buttons, Grids, DBGrids, dbgridEXT,a_data,db,a_main,
  Spin, ComCtrls, ToolWin;

type
  TForm_vert_syst = class(TForm)
    Notebook: TNotebook;
    Panel2: TPanel;
    DBgridEXT1: TDBgridEXT;
    BitBtn_new: TBitBtn;
    DBEdit2: TDBEdit;
    Label1: TLabel;
	  Label2: TLabel;
    Label3: TLabel;
    BitBtn_be: TBitBtn;
    DBEdit3: TDBEdit;
    Edit_key: TEdit;
    Label4: TLabel;
    BitBtn_sat_ini: TBitBtn;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    BitBtn_ab: TBitBtn;
    Edit_new: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    RadioGroup_was: TRadioGroup;
    GroupBox1: TGroupBox;
    SpinEdit_main: TSpinEdit;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    ToolBar1: TToolBar;
    ToolButton_close: TToolButton;
    BitBtn_open: TBitBtn;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    ToolButton1: TToolButton;
    BitBtn_write: TBitBtn;

    procedure BitBtn_newClick(Sender: TObject);
	  procedure BitBtn_abClick(Sender: TObject);
    procedure BitBtn_beClick(Sender: TObject);
    procedure BitBtn_sat_iniClick(Sender: TObject);
    procedure RadioGroup_wasClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure ToolButton_closeClick(Sender: TObject);
    procedure BitBtn_openClick(Sender: TObject);
    procedure BitBtn_writeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit2Change(Sender: TObject);
  private
    { Private-Deklarationen }

  public
    { Public-Deklarationen }
  end;

var
  Form_vert_syst: TForm_vert_syst;

implementation

{$R *.DFM}



procedure TForm_vert_syst.BitBtn_newClick(Sender: TObject);
begin
bitbtn_be.Visible:=true;
bitbtn_ab.Visible:=true;
edit_new.Enabled:=true;
edit_new.visible:=true;
label1.visible:=true;
end;

procedure TForm_vert_syst.BitBtn_abClick(Sender: TObject);
begin
bitbtn_ab.Visible:=false;
bitbtn_be.Visible:=false;
edit_new.Enabled:=false;
	 edit_new.visible:=false;
label1.visible:=false;
end;

procedure TForm_vert_syst.BitBtn_beClick(Sender: TObject);
var
nk:integer;
query,sname:string;
begin
with datamodul do
try

	 sname:=edit_new.text;

	 if length(trim(sname))<5 then
	 begin
		 showmessage('Name zu kurz');
		 exit;
	 end;
	 query:=format('select nummer from satelliten where upper(name)=upper("%s")',[sname]);
	 if mysql_d.Feldinhalt(query,0)<>'' then
		begin
			showmessage('Name schon vorhanden');
			exit;
		end;
   neuer_datensatz(q_1,['name'],[sname]);
	 //datamodul.q_1.Append;
	 //q_1['name']:=sname;

	 query:='select max(nummern_kreis) from satelliten';
	 try
		nk:=strtoint(mysql_d.Feldinhalt(query,0));
	 except
		nk:=110;
	 end;
   if nk<110 then nk:=110;
	 if nk>9000 then
	 begin
		showmessage('Sie k�nnen nicht mehr als 9000 Nummernkreise anlegen.');
		exit;
	 end;

	 q_1.FindField('nummern_kreis').asinteger:=nk+1;

	 query:='0000000'+inttostr( q_1.FindField('nummern_kreis').asinteger);
	 query:=copy(query,length(query)-4,5);
	 query:=query+sname+'                   ';
	 query:=copy(query,1,16);
	 randomize;
	 //query:=inttostr(random(9))+'arb'+query+'med'+inttostr(random(9));
	 query:='arb'+query+datetostr(date)+timetostr(time);
	 //q_1['schluessel']:=form_main.CipherManager.encodeString(query);
   q_1.FindField('schluessel').asstring:=form_main.string_verschluesseln(query);
   //q_1['schluessel']:=form_main.string_verschluesseln(query);
	 if q_1.state in [dsedit, dsinsert] then datamodul.q_1.post;

	 // 5arb000006ssssssssssmed8
finally
	 bitbtn_ab.Visible:=false;
	 bitbtn_be.Visible:=false;
	 edit_new.Enabled:=false;
	 edit_new.visible:=false;
label1.visible:=false;
end;
end;

procedure TForm_vert_syst.BitBtn_sat_iniClick(Sender: TObject);
var
sname,query,tabelle:string;
nummer:integer;
begin
if MessageDlg('Soll dieses System als Satelliten-System eingerichtet werden?'
		, mtConfirmation	,[mbYes, mbNo], 0)<>mryes then
	begin
		showmessage('Erzeugen Sie eine Neuinstallation und initialisieren Sie danach das Satelliten System neu.');
		exit;
	end;

with datamodul do
begin
	 query:=format('select nummer from satelliten where schluessel like "%s" ',['%'+edit_key.text+'%']);
	 if mysql_d.Feldinhalt(query,0)<>'' then
		begin
			showmessage('Die Datenbank des Satelliten darf nicht gleich der des Haupt-Systems sein.');
			exit;
		end;
	 //query:=form_main.CipherManager.decodeString(edit_key.text);
   query:=form_main.string_entschluesseln(edit_key.text);
	 if (copy(query,1,3)<>'arb')  then
	 begin
		showmessage('Schl�ssel nicht korrekt');
		exit;
	 end;
	 if MessageDlg('Sind sie sicher, dass Sie dieses System als Satelliten-System einrichten wollen, und dass der Schl�ssel bisher nicht verwendet wurde?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;

	 nummer:=strtoint(copy(query,4,5));
	 sname:=copy(query,9,10);
	q_2.edit;
	q_2['nummern_kreis']:=nummer;
	q_2['name']:=sname;
	q_2['haupt']:=0;
	q_2['s1']:=edit_key.text;
	q_2.post;



  //alle tabellen, die vom Hauptsystem importiert werden werden gel�scht.
	if MessageDlg('alle Daten dieses Systems werden gel�scht '+chr(13)+'Nach Abgleich mit dem Haupt-System sind dessen Daten verf�gbar.', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
  query:='select * from export_import where main=0 or main=1 ';
  sql(false,q_5,query,'');
  q_5.first;
  while not q_5.Eof do
  begin
     //schluessel, berechtigung_firma, berechtigung_objekt, berechtigung_vorlage   amb_weiter hat als ts 000000 wird daher nicht syncronisiert 
     tabelle:= lowercase(q_5['tab_name']);
     if pos(  '#'+tabelle+'#', ' #amb_weiter#, #schluessel#, #berechtigung_firma#, #berechtigung_objekt#, #berechtigung_vorlage#') =0 then
     begin
           query:='delete from '+q_5['tab_name'];
            mysql_d.sql_exe(query);
     end;
     q_5.Next;
  end;
  //nkr

  form_main.aktualisiere_nummern_kreis(nummer,'ini');

	showmessage('Die Berechtigungen und Lizenzdaten werden duch den ersten Datenabgleich aktualisiert.');

end;
	modalresult:=mrok;
end;



procedure TForm_vert_syst.RadioGroup_wasClick(Sender: TObject);
begin

if radiogroup_was.ItemIndex= 1 then
begin
	if (Messagedlg('Sollen das System zum Satelliten-System werden?',mtConfirmation, mbOkCancel	,0)=mrOK ) then notebook.pageindex:=1;
end;
if radiogroup_was.ItemIndex= 0 then
	if (Messagedlg('Sollen das System zum Haupt-System werden?',mtConfirmation, mbOkCancel	,0)=mrOK ) then notebook.pageindex:=0;
end;

procedure TForm_vert_syst.BitBtn1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_vert_syst.FormCreate(Sender: TObject);
begin

  form_main.form_positionieren(tform(sender));

  edit_new.visible:=false;
  label1.visible:=false;

end;



procedure TForm_vert_syst.BitBtn3Click(Sender: TObject);
begin
try
 inttostr(spinedit_main.Value);
except
	exit;
end;
	 if MessageDlg('Sind sie sicher, dass Sie den Nummernkreis ver�ndern wollen? Normalerweise ist dies nicht sinnvoll.', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then exit;
try

   form_main.aktualisiere_nummern_kreis(spinedit_main.Value,'ini');
finally
	 datamodul.q_2.edit;
   datamodul.q_2['nummern_kreis']:=spinedit_main.Value;
   datamodul.q_2.post;
   showmessage('Der Nummernkreis wurde ge�ndert');
end;
end;


procedure TForm_vert_syst.ToolButton_closeClick(Sender: TObject);
begin
modalresult:=mrok;
end;


procedure TForm_vert_syst.BitBtn_openClick(Sender: TObject);
var datname,s:string;
	  dat:textfile;
      fdate:tdatetime;
begin
	form_main.OpenDialog.filter:='txt |*.txt';
	if not form_main.OpenDialog.Execute then exit;
	datname:=form_main.OpenDialog.FileName;
	if not fileexists(datname) then exit;
    fdate:=FileDateToDateTime ( fileage(datname));
    if fdate+2<now() then
    begin
    	showmessage('Die Schl�sseldatei muss nach 2 Tagen wieder neu erzeugt werden');
        exit;
    end;
	assignfile(dat,datname );
    reset(dat);
	readln(dat,s);
	edit_key.text:=s;
	closefile(dat);
end;

procedure TForm_vert_syst.BitBtn_writeClick(Sender: TObject);
var datname,s:string;
	  dat:textfile;
begin
  if edit_new.Visible then
  begin
    showmessage('Bitte die Neuanlage mit OK best�tigen');
    exit;
  end;

	 form_main.saveDialog.DefaultExt:='.txt';
   form_main.Savedialog.Filter:='Text-Datei|*.txt';
	if not form_main.saveDialog.Execute then exit;
	datname:=form_main.saveDialog.FileName;
	//if not fileexists(datname) then exit;
	s:=dbedit3.text;
	assignfile(dat,datname );
	 rewrite(dat);
	 writeln(dat,s);
	 closefile(dat);

end;

procedure TForm_vert_syst.FormShow(Sender: TObject);
begin
spinedit_main.Value:=datamodul.q_2['nummern_kreis'];
end;


procedure TForm_vert_syst.DBEdit2Change(Sender: TObject);
var
num,num_max,query,tab:string;
nk:integer;
rv:boolean;
begin
nk:=datamodul.q_1.findfield('nummern_kreis').asinteger;
num:=inttostr(1000000000000*nk+1);
num_max:=inttostr(1000000000000*(nk+1)-100);
query:='select tab_name from export_import where main=0';
datamodul.sql(false,datamodul.q_b1, query,'');
datamodul.q_b1.First;
rv:=false;
while not datamodul.q_b1.Eof do
begin
  tab:=datamodul.q_b1.findfield('tab_name').asstring;
  query:=format('select nummer from %s where (nummer>=%s) and (nummer<%s) ',[tab,num,num_max]);
  if mysql_d.sql_exe_rowsvorhanden(query) then rv:=true;
  datamodul.q_b1.Next;
end;

if rv then
begin
  label12.caption:='Achtung, Nummernkreis schon in Verwendung';
  bitbtn_write.Enabled :=false;
  dbedit3.visible:=false;
end
else
begin
  label12.caption:='';
  bitbtn_write.Enabled :=true;
  dbedit3.visible:=true;
end;
end;

end.
