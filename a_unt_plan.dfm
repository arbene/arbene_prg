�
 TFORM_UNT_PLAN 0�  TPF0TForm_unt_planForm_unt_planLeft�Top� HelpContext�BorderIcons BorderStylebsDialogCaptionFristenClientHeight�ClientWidthPColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top�WidthPHeight4AlignalBottom
BevelOuterbvNoneTabOrder  TBitBtnBitBtn1Left� TopWidth� HeightCaptionOKDefault	ModalResultTabOrder 
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn2Left�TopWidth� HeightTabOrderKindbkAbort  TPanelPanel2Left Top Width)Height4AlignalLeft
BevelOuterbvNoneTabOrder   TPanelPanel_rightLeft2TopWidthHeight�AlignalRight
BevelOuterbvNoneTabOrder 	TGroupBox	GroupBox1LeftTop WidthHeight�AlignalClientCaption   Frist auswählenTabOrder  TPanelPanel6LeftTop�Width� Height!AlignalBottom
BevelOuterbvNoneTabOrder  TSpeedButtonSpeedButton1Left� TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  TButtonButton_fristLeftiTopWidth@HeightCaption   Datum ändernTabOrder VisibleOnClickButton_fristClick   TDBGridDBGrid1LeftTopWidth� HeighttAlignalClient
DataSourceDataModul.ds_7OptionsdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBGrid1CellClick
OnDblClickButton_fristClickColumnsExpanded	FieldNameFristWidth� Visible	    TPanelPanel7Left� TopWidthHeighttAlignalRight
BevelOuterbvNoneTabOrder   TPanelPanel5Left Top WidthHeight�AlignalLeft
BevelOuterbvNoneTabOrder   TPanelPanel3Left TopWidth)Height�AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel4Left Top WidthPHeightAlignalTop
BevelOuterbvNoneTabOrder  TPanelPanel_li_miLeft)TopWidth	Height�AlignalClient
BevelOuterbvNoneTabOrder 	TGroupBox	GroupBox2Left Top Width	Height� AlignalTopTabOrder  TStringGrid
StringGridLeftTopWidthHeight� AlignalClientColCountCtl3D		FixedCols RowCount	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRowMovinggoRowSelect ParentCtl3D	PopupMenuPopupMenu_stringgrid
ScrollBars
ssVerticalTabOrder 
OnRowMovedStringGridRowMovedOnSelectCellStringGridSelectCell   	TGroupBoxGroupBox_infoLeft TopWidth	Height� AlignalBottomCaption	Infos zu:TabOrder TDBMemoDBMemo_infoLeftTop)WidthHeightpAlignalClient	DataFieldmemo
DataSourceDataModul.ds_typReadOnly	
ScrollBars
ssVerticalTabOrder   TDBMemoDBMemo_untersuchungLeftTopWidthHeightAlignalTop	DataFielduntersuchung
DataSourceDataModul.ds_typReadOnly	TabOrder   TRadioGroupRadioGroup_abfolgeLeftTop� WidthHeight)Caption   Nachuntersuchungsfrist für Columns	ItemIndex Items.StringsErstuntersuchungNachuntersuchung TabOrderOnClickRadioGroup_abfolgeClick   
TPopupMenuPopupMenu_stringgridLeft�Top( 	TMenuItemm_ausgewhlteZeilelschenCaption   ausgewählte Zeile löschenOnClickm_ausgewhlteZeilelschenClick    