object Form_memo: TForm_memo
  Left = 671
  Top = 250
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Bitte Suchbegriffe f'#252'r die T'#228'tigkeiten eingeben'
  ClientHeight = 302
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 257
    Width = 660
    Height = 45
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel_b: TPanel
      Left = 0
      Top = 0
      Width = 660
      Height = 45
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn_ok: TBitBtn
        Left = 123
        Top = 8
        Width = 146
        Height = 25
        Caption = 'OK'
        ModalResult = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object BitBtn_abb: TBitBtn
        Left = 317
        Top = 8
        Width = 147
        Height = 25
        Caption = 'abbrechen'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Kind = bkAbort
      end
    end
  end
  object Memo: TMemo
    Left = 40
    Top = 33
    Width = 580
    Height = 224
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 660
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Panel3: TPanel
    Left = 0
    Top = 33
    Width = 40
    Height = 224
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
  end
  object Panel4: TPanel
    Left = 620
    Top = 33
    Width = 40
    Height = 224
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 4
  end
end
