unit a_soundex;

interface
uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, ExtCtrls,dbtables,ZDataset,Variants;

type tsoundex = class (tobject)
private
	match , zusatz, ersatz, regellist,wertigkeit:tstringlist;
	sonderzeichen,satzzeichen :set of char;
	buchstaben, regelzeichen,zahlen:set of char;
	procedure regeln( var s_in :string; var sz:integer);
	function finde_regel(s_in:string;sz:integer):integer;
	procedure verdichten(var s_in:string);
	procedure ersetze_umlaute(var s_in:string);
  function strdel(text,suchen: string): string;
  function strrepl(text,suchen,ersatz: string): string;
  function textnormieren(text: string) : string;

public
	constructor create(rule:integer;table_rules:tzquery);
	destructor free;
  function soundex(s_in: string) :string;

end;


implementation



constructor tsoundex.create(rule:integer;table_rules:tzquery);
var
	s_match, s_zusatz, s_ersatz, s_regel,s_wertigkeit, suchbegriff,t:string;
  i,j:integer;
	b:char;

begin
  buchstaben:=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','�','�','�','�'];
	regelzeichen:=['-','<','^','$'];
	zahlen:=['1','2','3','4','5','6','7','8','9','0'];
	sonderzeichen:=['�','�','�','�'];
	satzzeichen:=['�','`',chr(39),'"','.'];
	match:=tstringlist.Create;
	zusatz:=tstringlist.Create;
	ersatz:=tstringlist.Create;
	regellist:=tstringlist.Create;
	wertigkeit:=tstringlist.create;
		table_rules.first;
	 	while not table_rules.eof do
		begin
			if table_rules['such']<>null then suchbegriff:=table_rules['such'] else suchbegriff:='';
			if suchbegriff='#' then suchbegriff:=' ';
			if rule=1 then if table_rules['ersatz1']<>null then s_ersatz:=table_rules['ersatz1'] else s_ersatz:=''
			else if table_rules['ersatz2']<>null then s_ersatz:=table_rules['ersatz2'] else s_ersatz:='';
        if s_ersatz='#' then s_ersatz:=' ';
        //if s_ersatz='NULL' then s_ersatz:='';
			if (s_ersatz<>'NULL')  then
			begin
				//erste Buchstaben
				s_match:='';
				s_zusatz:='';
				s_regel:='';
				s_wertigkeit:='5';
				i:=1;
				b:=suchbegriff[i];
				while ((b in  buchstaben) or (b in satzzeichen)) and (i<=length(suchbegriff))do
				begin
					s_match:=s_match+b;
              if s_match='o' then
              begin
                t:=s_match;
              end;
					inc(i);
              if i<=length(suchbegriff) then  b:=suchbegriff[i];
				end;
				//exitstieren zusatzbuchstaben?
				if b='(' then
				begin
					j:=pos(')',suchbegriff);
					s_zusatz:=copy(suchbegriff,i+1,j-i-1);
					i:=j+1;
              if i<=length(suchbegriff) then b:=suchbegriff[i];
				end;

			  //suche regeln und wertigkeit
				while (((b in  regelzeichen) or (b in zahlen)) and (i<=length(suchbegriff))) do
           begin
           	if b in regelzeichen then s_regel:=s_regel+b
              else s_wertigkeit:=b;
              inc(i);
              if i<=length(suchbegriff) then b:=suchbegriff[i];
           end;
           if s_match<>'' then
				begin
              match.Add(s_match);
              zusatz.Add(s_zusatz);
              ersatz.Add(s_ersatz);
              wertigkeit.Add(s_wertigkeit);
              regellist.add(s_regel);
           end;
        end;
			table_rules.Next;
		end;
end;

destructor tsoundex.free;
begin
	match.Free;
	zusatz.free;
	ersatz.free;
	regellist.free;
  wertigkeit.Free;
end;


function tsoundex.soundex(s_in: string) :string;
var
 sz:integer;
begin
     s_in :=textnormieren(s_in);
     if s_in <>'' then
     begin
        sz:=1;
        while sz<=length(s_in) do
        begin
           regeln(s_in, sz);
        end;
			verdichten(s_in); // doppelbuchstaben..
			ersetze_umlaute(s_in);
		end;

     result:=s_in;
end;


procedure tsoundex.regeln(var s_in :string;  var sz:integer);
var
	regel,fregel,rlaenge,frlaenge,
	position,szneu:integer;
	ersetzen:boolean;
	regelwort:string;
begin
  ersetzen:=true;
	//iminus:=0;
	//szgleich:=false;
	//fortsetzung:=true;
	//pzmax:=match.Count-1;
	//b:=s_in[sz];
	regel:=finde_regel(s_in,sz);
	//regel:=finde_regel(copy(s_in,sz,length(s_in)),sz);

	if regel>-1 then
	begin
	  rlaenge:=length(match[regel]);
	  if zusatz[regel]<>'' then inc(rlaenge);//zusatzregel ++
	  if ( pos('-',regellist[regel])>0){or(pos('<',regellist[regel])>0)} then	fregel:=-1
		else  fregel:=finde_regel(s_in,sz+rlaenge-1);     //-1
	  //fregel:=finde_regel(copy(s_in,sz+rlaenge,length(s_in)),sz);

	  if fregel>-1 then
			if wertigkeit[regel]<=wertigkeit[fregel] then
					begin
						frlaenge:=length(match[fregel]);
						if zusatz[fregel]<>'' then inc(frlaenge);//zusatzregel ++
						if frlaenge>1 then
						begin
							sz:=sz+rlaenge-1; //-1
							regel:=fregel;
					  end;

					  {	regel:=fregel;
						rlaenge:=length(match[regel]);
						if zusatz[regel]<>'' then inc(rlaenge);//zusatzregel ++
						sz:=sz+rlaenge;}
					end;
	  //regel ausf�hren
	  regelwort:=regellist[regel];
	  position:= pos('-',regelwort); //soviele Zeichen werden von hinten her nicht ersetzt
	  while position >0 do begin
		dec(rlaenge);
		regelwort:=copy(regelwort,position+1,length(regelwort));
		position:= pos('-',regelwort);
    end;

    szneu:=sz+length(ersatz[regel]);
    if  pos('<',regelwort)> 0 then
    begin
      szneu:=sz;
    end;
    if  pos('^',regelwort)> 0 then
    begin
      if sz>1 then ersetzen :=false;
    end;
     if  pos('^^',regelwort)> 0 then
    begin
      if sz>1 then szneu:=1;
    end;
     if  pos('$',regelwort)> 0 then
    begin
      if not (sz+rlaenge=length(s_in)+1) then ersetzen:=false;  //vorher -1
    end;

    if ersetzen then
    begin

       delete(s_in,sz,rlaenge);
       insert( ersatz[regel],s_in,sz);
       sz:=szneu
    end
    else inc(sz);

	end
	else inc(sz);

end;


function tsoundex.finde_regel(s_in:string;sz:integer):integer;
var
	gefunden,weiter:boolean;
	index, imax,smin, smax,mmin,mmax,nindex:integer;
	b,bz:char;
	wort:string;
begin
	result:=-1;
  if s_in='' then exit;
	imax:=match.Count;
	smax:=imax-1;
	mmax:=smax;
  smin:=0;
  mmin:=0;
	gefunden:=false;
  b:=s_in[sz];
  if not (b in buchstaben) then
  begin
     result:=-1;
  	exit;
  end;
  while (not (match[smin][1] in buchstaben)) do inc(smin);  //erst bei a anfangen
  if b in sonderzeichen then
  begin
     index:=0;
     while (match[index][1] in sonderzeichen) do
		begin
        if (match[index][1]=b) then break;
        inc(index);
     end;
	end
  else
  begin
	  //i:=1;
	  index:=mmin+(smax-smin) div 2;
    while  (not ((match[index][1]=b) or (smax=smin))  ) do
    begin

        if b<match[index][1] then
        begin
           smax:=index;
           smin:=mmin;
           mmax:=smax;
           index:=smax-(smax-smin) div 2;
        end
        else
        begin
           smin:=index;
           smax:=mmax;
				mmin:=smin;
          index:=smin+(smax-smin) div 2;
        end;
	  end;

  end;

	if match[index][1]=b then    //hier noch verbesserungsf�hig      erstes vorkommen des buchstabens
  begin
		while (index>=0)and (match[index][1]=b) do dec(index);
		inc(index);
	end;

	while (index< imax)  do
	begin
		wort:=match[index];
		if pos(wort, copy(s_in,sz,length(s_in)))=1 then
		begin
		//Wortanfang oder wortendenpr�fung
		weiter:=true;
		if  (pos('^',regellist[index])>0) then if sz=1 then weiter:=true else weiter:=false;
		if (pos('$',regellist[index])>0) then if length(wort)+sz=length(s_in)+1 then weiter:=true else weiter :=false;
		if weiter then
		begin
        //fortstzung
        if zusatz[index]<>'' then
			begin
				nindex:=sz+length(match[index]);
				if nindex<=length(s_in) then bz:=s_in[nindex] else bz:=char(0); //n�chster buchstabec
				if pos(bz,zusatz[index])>0  then  //es gibt ne fortsetzungsregel
				begin
             gefunden:=true;
	           break;
           end;
        end
        else
        begin
           gefunden:=true;
           break;
        end;

     end;
		end;

		if b<>wort[1] then break;
		inc(index);
	end;

	if gefunden=true then  result:=index else result:=-1;
end;

procedure tsoundex.verdichten(var s_in:string);
var i:integer;
begin
	i:=1;
	while i<=length(s_in)-1 do
	begin
		if s_in[i]=s_in[i+1] then
			begin
				delete(s_in,i+1,1);
			end
			else inc(i);
	end;
end;

procedure tsoundex.ersetze_umlaute(var s_in:string);
begin
 s_in:=strrepl(s_in, '�','ae');
 s_in:=strrepl(s_in, '�','oe');
 s_in:=strrepl(s_in, '�','ue');
end;



function tsoundex.textnormieren(text: string) : string;
 var
	 satzzeichen,regelzeichen, steuerzeichen: string;
	 i:integer;
 begin
		steuerzeichen:='';
	  //for i:=1 to 31 do steuerzeichen:=steuerzeichen+chr(i);
		 satzzeichen:='()[]{}?/=+*"<>^#|�~'+chr(39)+chr(132)+chr(147);//+steuerzeichen;//+chr(9)+chr(13)+chr(10)
		 regelzeichen:='-<^$';
		 text:=ansilowercase(text);
		 for i:=1 to length(regelzeichen) do text:=strrepl(text, regelzeichen[i],'');
		 for i:= 1 to length(satzzeichen) do   text:=strdel(text,satzzeichen[i]);

		 text:=trim(text);
		 result:=text;
 end;

function tsoundex.strrepl(text,suchen,ersatz: string): string;
var
position: integer;
begin
		position:=pos(suchen,text);
		 while position >0 do begin
		  delete(text,position,1);
		  insert(ersatz,text,position);
		  position:=pos(suchen,text);
		 end;
		result:=text;
end;


function tsoundex.strdel(text,suchen: string): string;
var
position: integer;
begin
		position:=pos(suchen,text);
		 while position >0 do begin
		  delete(text,position,1);
		  position:=pos(suchen,text);
		 end;
		result:=text;
end;


end.

