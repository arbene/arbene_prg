unit a_treewahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, TreeView_ext, StdCtrls, Buttons, ExtCtrls,a_data, Menus, variants;

type
  TForm_treewahl = class(TForm)
    Panel_fz: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    TreeView: TTreeView_ext;
    PopupMenu: TPopupMenu;
    Neu: TMenuItem;
    Verndern1: TMenuItem;
    Lschen1: TMenuItem;
    Neuuntergeordnet1: TMenuItem;
    N1: TMenuItem;
    kopieren1: TMenuItem;
    einfuegen: TMenuItem;
    Panel2: TPanel;
    Label_suchen: TLabel;
    SpeedButton1: TSpeedButton;
    Edit_such: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure NeuClick(Sender: TObject);
    procedure Verndern1Click(Sender: TObject);
    procedure Lschen1Click(Sender: TObject);
    procedure TreeViewChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure Neuuntergeordnet1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
    procedure TreeViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TreeViewEdit_tabelle(sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure kopieren1Click(Sender: TObject);
    procedure einfuegenClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit_suchEnter(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
     procedure schliessen;
     procedure Neuer_satz(modus:integer);
    { Private-Deklarationen }
  public
	  { Public-Deklarationen }
	  multiselect:boolean;
  end;

var
  Form_treewahl: TForm_treewahl;

implementation

uses a_line_edit, a_main;

{$R *.DFM}

procedure TForm_treewahl.FormCreate(Sender: TObject);
begin
	height:=form_main.Height-50;
	form_main.form_positionieren(tform(sender));

end;

procedure TForm_treewahl.NeuClick(Sender: TObject);
begin
 neuer_satz(1);
 {form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 if form_line_edit.showmodal<>mrok then exit;
 treeview.neu(form_line_edit.Edit.text,1);
 form_line_edit.Release}

end;

procedure TForm_treewahl.Verndern1Click(Sender: TObject);
begin
form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 form_line_edit.Edit.text:=treeview.mysqlquery[treeview.mysql_feld];
 if form_line_edit.showmodal=mrok then
 begin
  treeview.mysqlquery.edit;
	treeview.mysqlquery[treeview.mysql_feld]:=form_line_edit.Edit.text;
  treeview.tabelle_feld_abgleich;
 end;
 form_line_edit.Release;
end;

procedure TForm_treewahl.Lschen1Click(Sender: TObject);
begin
 treeview.loeschen;
end;

procedure TForm_treewahl.TreeViewChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
treeview.tabelle_post;
end;

procedure TForm_treewahl.Neuuntergeordnet1Click(Sender: TObject);
begin
     neuer_satz(2);
end;

procedure TForm_treewahl.Neuer_satz(modus:integer);
var reihenfolge:integer;
parentnum, i_nummer,nr:int64;
nodedata,parentdata:pnodepointer;
  knoten:ttreenode;
begin
try
  form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 if form_line_edit.showmodal<>mrok then exit;

 treeview.change_allowed:=false;
 case modus of
	  1:
		begin
			knoten:= treeview.Items.insert( treeview.Selected, '');
		end;
		2: begin
			 knoten:= treeview.Items.Addchildfirst( treeview.Selected, '');
			  treeview.Selected.Expand(false);
			end;

	  end;  //case
		treeview.Selected:=knoten;
     try   //neu bei 1 oder 2
		if knoten.parent<>nil then
		 begin
			parentdata:=knoten.Parent.data;
			parentnum:=parentdata^.nummer;
			end
		 else
		 parentnum:=0  ;
	  except
		parentnum:=0;
	  end;

    reihenfolge:=treeview.Selected.AbsoluteIndex;
 //if datamodul.q_1['parent']<>null then parentnum:=getbigint(datamodul.q_1,'parent') else parentnum:=0;
  neuer_datensatz(datamodul.q_1,['nummer','parent','s_text','bereich'],[null,inttostr(parentnum),form_line_edit.Edit.text,treeview.bereich_wert]);

    datamodul.q_1.Post;
    new(nodedata);
    nodedata^.nummer:=getbigint(datamodul.q_1,'nummer');
    knoten.Data:=nodedata;
	  //nodedata.nummer:=getbigint(datamodul.q_1,'nummer');
    nr:=getbigint(datamodul.q_1,'nummer');
    treeview.reihenfolge_neu;
    treeview.liste_lesen;

    treeview.suche_nummer(nr);
	  //knoten.Data:=nodedata;
	  //knoten.SelectedIndex:=1;
	  //knoten.Text:=datamodul.q_1['s_text'];
    //treeview.reihenfolge_neu;


finally
 form_line_edit.Release;
 treeview.change_allowed:=true;
end;
end;

procedure TForm_treewahl.FormShow(Sender: TObject);
begin
treeview.SetFocus;
end;

procedure TForm_treewahl.TreeViewDblClick(Sender: TObject);
begin
if treeview.items.count=0 then exit;
if not multiselect then schliessen;
if treeview.selected.stateindex=-1 then treeview.markieren(true,3)
else treeview.markieren(true,-1);
end;

procedure TForm_treewahl.schliessen;
begin
	 if multiselect then modalresult:=mrok
	 else
	 if treeview.akt_nummer>=0 then modalresult:=mrok
	 else showmessage('Bitte anderes Feld ausw�hlen');
end;

procedure TForm_treewahl.TreeViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
inherited;
case key of
			8:  ;// backspace
			13: schliessen; //enter
			46: ;//del
			//vk_down: treeview.hoch;
			//vk_up: treeview.runter;//cursor
			
end;
end;

procedure TForm_treewahl.TreeViewEdit_tabelle(sender: TObject);
begin
 form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 if form_line_edit.showmodal<>mrok then exit;
 treeview.mysqlquery.edit;
 treeview.mysqlquery[treeview.mysql_feld]:=form_line_edit.Edit.text;
 treeview.mysqlquery.post;
 form_line_edit.Release
end;

procedure TForm_treewahl.BitBtn1Click(Sender: TObject);
begin
schliessen;
end;

procedure TForm_treewahl.kopieren1Click(Sender: TObject);
begin
	treeview.copy;
	einfuegen.Enabled:=true;
end;

procedure TForm_treewahl.einfuegenClick(Sender: TObject);
begin
	einfuegen.Enabled:=false;
	treeview.insert;
end;

procedure TForm_treewahl.FormDestroy(Sender: TObject);
begin
treeview.liste_loeschen;
end;

procedure TForm_treewahl.Edit_suchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key<>vk_return then exit;
     treeview.suche_datensatz(edit_such.text);
end;

procedure TForm_treewahl.Edit_suchEnter(Sender: TObject);
begin
//if treeview.items.count=0 then exit;
//treeview.Selected:=treeview.Items[0];
//treeview.change(treeview.selected);
end;

procedure TForm_treewahl.SpeedButton1Click(Sender: TObject);
begin
treeview.suche_datensatz(edit_such.text);
end;

end.
