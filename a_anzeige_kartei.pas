unit a_anzeige_kartei;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ExtCtrls, CheckLst;

type
  TForm_anzeige_kartei = class(TForm)
    CheckListBox: TCheckListBox;
    Panel_b: TPanel;
    Label_such: TLabel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_anzeige_kartei: TForm_anzeige_kartei;

implementation

uses a_main;

{$R *.dfm}

procedure TForm_anzeige_kartei.SpeedButton1Click(Sender: TObject);
var
i:integer;
begin
   for i:=0 to checklistbox.Items.Count-1 do
   begin
     if CheckListBox.ItemEnabled[i] then checklistbox.Checked[i]:=true;
   end;
end;

procedure TForm_anzeige_kartei.SpeedButton2Click(Sender: TObject);

var
i:integer;
begin
   for i:=0 to checklistbox.Items.Count-1 do
   begin
      checklistbox.Checked[i]:=false;
   end;
end;


procedure TForm_anzeige_kartei.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

end.
