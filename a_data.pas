unit a_data;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	Db,  olecontainerext,stdctrls,
	comctrls, olectnrs, ZConnection, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, ZSqlMetadata, {ZSqlProcessorm,}
  ZSqlProcessor, Grids,variants, ZSqlUpdate, ZAbstractConnection,
  ZSqlMonitor;



type
  TDataModul = class(TDataModule)
    connection_main: TZConnection;
    q_mitarbeiter: tzquery;
    q_icd_main: tzquery;
    q_diagnosen: tzquery;
    q_firma_sollist: tzquery;
    q_termin: tzquery;
    q_firma_ansprech: tzquery;
    q_firma: tzquery;
    ds_icd_main: TDataSource;
	  ds_diagnosen: TDataSource;
    ds_firma_sollist: TDataSource;
    ds_termin: TDataSource;
    ds_firma_ansprech: TDataSource;
    ds_firma: TDataSource;
    ds_mitarbeiter: TDataSource;
    q_besonderheiten: tzquery;
    q_besonderheitenListe_tree: tzquery;
    q_besonderheitenListe: tzquery;
    q_termin1: tzquery;
    q_typ: tzquery;
	  ds_besonderheitenListe: TDataSource;
    ds_besonderheitenListe_tree: TDataSource;
	  ds_besonderheiten: TDataSource;
    ds_typ: TDataSource;
    ds_untersuchung: TDataSource;
    DataSource16: TDataSource;
    q_termin6: tzquery;
    ds_termin6: TDataSource;
    q_termin5: tzquery;
	  ds_termin5: TDataSource;
    q_termin4: tzquery;
	  ds_termin4: TDataSource;
    q_termin3: tzquery;
	  ds_termin3: TDataSource;
    q_termin2: tzquery;
    ds_termin2: TDataSource;
    q_sql_baum: tzquery;
	  ds_labor: TDataSource;
    q_akt_untersuchung: tzquery;
	  ds_akt_untersuchung: TDataSource;
    q_bereich: tzquery;
    ds_bereich: TDataSource;
    q_berlist: tzquery;
    ds_berlist: TDataSource;
    q_impfstoff: tzquery;
	  ds_impfstoff: TDataSource;
    ds_impfung: TDataSource;
	  ds_termin1: TDataSource;
	  ds_vorlagen: TDataSource;
    ds_nachuntersuchung: TDataSource;
    ds_termin0: TDataSource;
    ds_termin_monat: TDataSource;
    ds_termin7: TDataSource;
    q_termin7: tzquery;
    q_termin_monat: tzquery;
    q_termin0: tzquery;
    q_nachuntersuchung: tzquery;
    q_vorlagen: tzquery;
    q_1: tzquery;
    ds_1: TDataSource;
    q_2: tzquery;
    ds_2: TDataSource;
    q_3: tzquery;
	  ds_3: TDataSource;
    q_sql_abfragen: tzquery;
    q_unt_filter: tzquery;
	  ds_unt_filter: TDataSource;
    q_sql_join: tzquery;
    q_sql_felder: tzquery;
    q_schluessel: tzquery;
    DataSource_sql_abfragen: TDataSource;
	  ds_schluessel: TDataSource;
    q_mitarbeiter_such: tzquery;
    ds_mitarbeiter_such: TDataSource;
    q_texte: tzquery;
    ds_texte: TDataSource;
	  DataSource_sql_felder: TDataSource;
    q_4: tzquery;
    q_typ_tree: tzquery;
    Ds_typ_tree: TDataSource;
    q_arbeitsunfall: tzquery;
    ds_arbeitsunfall: TDataSource;
    q_ambulanz: tzquery;
    ds_ambulanz: TDataSource;
    q_a1: tzquery;
    ds_a2: TDataSource;
    q_a2: tzquery;
    q_a3: tzquery;
    ds_4: TDataSource;
    ds_serienfeldinhalt: TDataSource;
    q_serienfeldinhalte: tzquery;
    ds_sereinfeld: TDataSource;
    q_serienfelder: tzquery;
    ds_taetigkeiten: TDataSource;
    q_taetigkeiten: tzquery;
    q_befunde: tzquery;
    q_dokumente: tzquery;
    ds_dokumente: TDataSource;
    ds_befunde: TDataSource;
    q_email: tzquery;
    ds_email: TDataSource;
    q_5: tzquery;
    ds_5: TDataSource;
    ds_amb_unfall: TDataSource;
    q_amb_unfall: tzquery;
    q_6: tzquery;
    ds_6: TDataSource;
    q_rechnung: tzquery;
    ds_rechnung: TDataSource;
    q_repositionen: tzquery;
    ds_repositionen: TDataSource;
    q_untersuchung: tzquery;
    q_impfung: tzquery;
    q_labor: tzquery;
    q_art: tzquery;
    ds_art: TDataSource;
    q_akt_untersuchung_storno: tzquery;
    ds_akt_untersuchung_storno: TDataSource;
    q_artikel: tzquery;
    ds_artikel: TDataSource;
    q_7: tzquery;
    ds_7: TDataSource;
    q_8: tzquery;
    ds_8: TDataSource;
    q_9: tzquery;
    ds_9: TDataSource;
    q_10: tzquery;
    ds_10: TDataSource;
    ZSQLMetadata1: TZSQLMetadata;
    q_11: TZQuery;
    sql_processor: TZSQLProcessor;
    q_anamnese: TZQuery;
    q_kontakte: TZQuery;
    ds_kontakte: TDataSource;
    Connection_grant: TZConnection;
    SQL_Processor_grant: TZSQLProcessor;
    q_abteilung: TZQuery;
    ds_abteilung: TDataSource;
    Connection_hs: TZConnection;
    q_firma_filter: TZQuery;
    Ds_firma_filter: TDataSource;
    q_firma_filter_vorlage: TZQuery;
    ds_firma_filter_vorlage: TDataSource;
    q_firma_zeiten_soll: TZQuery;
    ds_firma_zeiten_soll: TDataSource;
    ZUpdateSQL1: TZUpdateSQL;
    q_12: TZQuery;
    ds_11: TDataSource;
    ds_12: TDataSource;
    q_13: TZQuery;
    ds_13: TDataSource;
    q_14: TZQuery;
    ds_14: TDataSource;
    q_15: TZQuery;
    ds_15: TDataSource;
    q_16: TZQuery;
    ds_16: TDataSource;
    q_17: TZQuery;
    ds_17: TDataSource;
    q_18: TZQuery;
    ds_18: TDataSource;
    q_19: TZQuery;
    ds_19: TDataSource;
    q_20: TZQuery;
    ds_20: TDataSource;
    q_history: TZQuery;
    ds_history: TDataSource;
    ds_anamnese: TDataSource;
    q_ap3: TZQuery;
    ds_ap3: TDataSource;
    q_ap4: TZQuery;
    ds_ap4: TDataSource;
    q_ap5: TZQuery;
    ds_ap5: TDataSource;
    q_ap6: TZQuery;
    ds_ap6: TDataSource;
    q_ap10: TZQuery;
    ds_ap10: TDataSource;
    q_ap11: TZQuery;
    ds_ap11: TDataSource;
    q_ap12: TZQuery;
    ds_ap12: TDataSource;
    q_ap13: TZQuery;
    ds_ap13: TDataSource;
    q_main: TZQuery;
    ZSQLMonitor: TZSQLMonitor;
    q_b1: TZQuery;
    ds_b1: TDataSource;
    ds_main: TDataSource;
    q_hinweis: TZQuery;
    ds_hinweis: TDataSource;
    q_rechnung_regeln: TZQuery;
    ds_rechnung_regeln: TDataSource;
    procedure q_firmaBeforePost(DataSet: TDataSet);
	  procedure q_vorlagenAfterScroll(DataSet: TDataSet);
	  procedure q_termin0AfterScroll(DataSet: TDataSet);
	  procedure pre_post(table: tdataset);
	  procedure unt_pre_post(table: tdataset);
	  procedure q_mitarbeiterBeforePost(DataSet: TDataSet);
	  procedure q_texteAfterScroll(DataSet: TDataSet);
	  procedure q_texteBeforePost(DataSet: TDataSet);
	  procedure q_vorlagenBeforePost(DataSet: TDataSet);
	  procedure q_vorlagenBeforeScroll(DataSet: TDataSet);
	  procedure q_texteBeforeScroll(DataSet: TDataSet);
	  procedure ds_laborDataChange(Sender: TObject; Field: TField);
	  procedure q_AfterPost(DataSet: TDataSet);
	  procedure BeforeScroll(DataSet: TDataSet);
	  procedure termin_BeforePost(DataSet: TDataSet);
	  procedure termin_beforeScroll(DataSet: TDataSet);
    procedure q_schluesselBeforeScroll(DataSet: TDataSet);
    procedure q_firmaAfterScroll(DataSet: TDataSet);
    procedure q_ambulanzAfterScroll(DataSet: TDataSet);
    procedure q_ambulanzBeforeScroll(DataSet: TDataSet);
	  procedure q_laborBeforePost(DataSet: TDataSet);
    procedure q_ambulanzBeforePost(DataSet: TDataSet);
    procedure q_mitarbeiterAfterScroll(DataSet: TDataSet);
    procedure q_serienfelderAfterScroll(DataSet: TDataSet);
    procedure q_sql_abfragenAfterScroll(DataSet: TDataSet);
    procedure ZMySqlMonitor1MonitorEvent(Sql, Result: String);
    procedure q_rechnungAfterScroll(DataSet: TDataSet);
    procedure q_schluesselAfterScroll(DataSet: TDataSet);
    procedure ds_texteDataChange(Sender: TObject; Field: TField);
    procedure q_vorlagenAfterPost(DataSet: TDataSet);
    procedure q_untersuchungAfterScroll(DataSet: TDataSet);
    procedure DataModulCreate(Sender: TObject);
     procedure reptable;
    procedure q_firma_sollistAfterScroll(DataSet: TDataSet);
    procedure q_sql_abfragenBeforeScroll(DataSet: TDataSet);
    procedure q_untersuchungAfterPost(DataSet: TDataSet);
    procedure q_dokumenteBeforePost(DataSet: TDataSet);
    procedure q_dokumenteAfterScroll(DataSet: TDataSet);
    procedure q_anamneseAfterScroll(DataSet: TDataSet);
    procedure q_akt_untersuchungAfterScroll(DataSet: TDataSet);
    procedure ds_firma_sollistStateChange(Sender: TObject);
    procedure q_firma_ansprechAfterScroll(DataSet: TDataSet);
    procedure q_firma_zeiten_sollAfterScroll(DataSet: TDataSet);
    procedure ZSQLMonitorLogTrace(Sender: TObject; Event: TZLoggingEvent);
    procedure q_firma_sollistBeforeScroll(DataSet: TDataSet);
    procedure q_rechnung_regelnAfterScroll(DataSet: TDataSet);
    procedure q_rechnung_regelnBeforeScroll(DataSet: TDataSet);
    procedure ds_firma_sollistDataChange(Sender: TObject; Field: TField);
    procedure ds_besonderheitenDataChange(Sender: TObject; Field: TField);
	private
    { Private-Deklarationen }
    procedure status_bearbeiten(DataSet: TDataSet);
	public
	  { Public-Deklarationen }
	  //function  getbigint(tabelle: tdataset;feldname:string):int64;
	  //procedure setbigint(tabelle: tdataset;feldname:string;wert:int64);
    //procedure not_null_abfragen(table: tdataset);
     procedure not_null_abfragen(table: tzquery);
	  procedure sql(storno_check:boolean;table: tzquery; query,index:string);
	  procedure sql_new(storno_check:boolean;table: tzquery; query,index:string);
	  procedure sql_index(table: tzquery; index:string);
	  procedure sql_param(table: tzquery; query:string;count:integer;parameter,werte:array of string);
	  procedure sql_relation(tabelle,feld: string;master, detail: tzquery;order:string);
	  procedure sql_relation_desc(tabelle,feld: string;master, detail: tzquery;order:string);
	  procedure cancel_loeschen(dataset:tzquery);
	  function finddatasource(name:string):tdatasource;
	  function findtable(tname:string):tzquery;
	  procedure sql_open;
	  procedure sql_close;
	  function sql_post(table:tzquery):boolean;
	  procedure sql_memo_post(table:tzquery; ole: tolecontainerext);
	  function sql_beinhaltet(master: tzquery;tabelle, Feld:string):boolean;


	  procedure  gridladen(grid: tstringgrid;dataset: tdataset);
	  procedure  gridschreiben(grid: tstringgrid;dataset: tdataset );
	  procedure  sql_leer(tabelle: string;detail: tzquery);
	  function query_check(query:string):string;
    procedure listbox_lesen(listbox:tlistbox;q_steuer:tzquery; feld_steuer:string;tab_ausgabe, feld_ausgabe:string);
    procedure listbox_leeren(listbox: tlistbox);
    procedure listbox_locate(sender:tobject;listbox:tlistbox;q_steuer:tzquery);
	end;
	  function  getbigint(tabelle: tdataset;feldname:string):int64;
    function  getbigint_str(tabelle: tdataset;feldname:string):string;
	  procedure setbigint(tabelle: tdataset;feldname:string;wert:int64);
	  function mysql_strtofloat(s:string):real;
    procedure sql_normieren(var text:string);
    function dat_delphi_to_sql(datum: string): string;
	  function dat_sql_to_delphi(datum: string): string; //sql_datum zu delphi
    function dat_sql_to_delphi1(datum: string): string; //sql_datum zu delphi
 	  function sql_datetostr(datum:tdate):string;
    function dattime_to_sql(datum,time:string):string;
    function table_length(tab_name:string):int64;

    function sql_string_float(s:string):string;
    function floattosqlstring(x:real):string;

    procedure refresh_all;

var
  DataModul: TDataModul;

implementation

uses a_main, a_vorlagen, a_besonderheiten, a_nachuntersuchung, a_termine,
  a_start, a_berechtigung, a_unt_wahl, a_kalender, a_auswertung;

{$R *.DFM}

procedure tdatamodul.sql_index(table: tzquery; index:string);
begin
  table.Close;
  table.Open;
end;

procedure tdatamodul.sql_leer(tabelle: string;detail: tzquery);
var
sql_string:string;
begin
	 sql_string:='select * from '+tabelle+' where nummer = -1';
	 sql(true,detail,sql_string,'');
end;





procedure tdatamodul.sql_relation(tabelle,feld: string;master, detail: tzquery;order:string);
var
sql_string:string;
nummer:int64;
begin
nummer:=getbigint(master,'nummer');
if nummer>0 then
begin
	 sql_string:='select * from '+tabelle+' where '+feld+' = '+inttostr(nummer);
	 sql_new(true,detail,sql_string,order);
end
else
begin
	 sql_string:='select * from '+tabelle+' where '+feld+' = -1';
	 sql_new(true,detail,sql_string,order);
end;
end;


procedure tdatamodul.sql_relation_desc(tabelle,feld: string;master, detail: tzquery;order:string);
var
sql_string:string;
nummer:int64;
begin
nummer:=getbigint(master,'nummer');
if nummer>0 then
begin
	 sql_string:='select * from '+tabelle+' where '+feld+' = '+inttostr(nummer);
	 if order<>'' then sql_string:=sql_string+' order by '+order+' desc';
	 sql_new(true,detail,sql_string,'');
end
else
begin
	 sql_string:='select * from '+tabelle+' where '+feld+' = -1';
	 sql_new(true,detail,sql_string,'');
end;
end;


procedure tdatamodul.sql(storno_check:boolean;table: tzquery; query,index:string);
var
  ctext:string;
begin
  if trim(query)='' then exit;
  try
  //table.DisableControls;


	if storno_check then query:=query_check(query);
	ctext:=query;
	if trim(index)<>'' then ctext:=ctext+' order by '+index;
	if trim(table.Sql.text)<>trim(ctext) then
	begin
    sql_normieren(ctext);
		table.close;
		table.filtered:=false;
		table.filter:='';
     sql_normieren(ctext);
		table.sql.text:=ctext;
		table.Open;
		//table.first;
	end;
	finally
  //table.EnableControls;
  //table.first;
  application.ProcessMessages;
  end;
end;


procedure tdatamodul.sql_new(storno_check:boolean;table: tzquery; query,index:string);
var
	ctext:string;
begin
	if trim(query)='' then exit;
	try
  //table.DisableControls;
	if storno_check then query:=query_check(query);
	ctext:=query;
	if trim(index)<>'' then ctext:=ctext+' order by '+index;
   sql_normieren(ctext);
	 table.close;
   table.ReadOnly:=false;
	 table.filtered:=false;
	 table.filter:='';
   sql_normieren(ctext);
	 table.sql.text:=ctext;
	 table.Open;
	 //table.first;   20100514
	finally
    //table.EnableControls;
    application.ProcessMessages;
    //table.first;
	end;
end;

procedure tdatamodul.sql_param(table: tzquery; query:string;count:integer;parameter,werte:array of string);
var
i:integer;
params_str:string;
begin
	table.close;
	params_str:='Values:( ';
  sql_normieren(query);
	table.sql.text:=query;
	//table.params.Count:=count;
	for i:=0 to count-1 do
	begin
		params_str:=params_str+':'+parameter[i]+', ';
		table.params[i].asstring:=werte[i];
	end;
	table.sql.add(params_str);
	params_str:=params_str+')';
	//table.ExecSql;
	table.open;
end;

procedure tdatamodul.cancel_loeschen(dataset:tzquery);
var
i, anfang, ende:integer;
loeschen:boolean;
begin
	dataset.cancel;
	if (dataset.tag<>null) and (dataset.tag>0) then
	begin
		anfang:=  trunc(dataset.tag/100);
		ende:=trunc(100*frac(dataset.tag/100));
	end
	else
	begin
		anfang:=1;
		ende:=dataset.Fields.count-1;
	end;
	loeschen:=true;
	if ende >dataset.Fields.count-1 then ende :=dataset.Fields.count-1;
	for i:=anfang to ende do
		if (lowercase(dataset.Fields[i].fieldname)<>'datum') and (dataset.Fields[i].asstring<>'') then loeschen:= false;

	direktspeichern:=true;
		if loeschen then dateiloeschen(dataset, true,false) else dataset.cancel;
	abort;
end;

function tdatamodul.finddatasource(name:string):tdatasource;
var
 i:integer;
begin
	result:=nil;
	for I := datamodul.ComponentCount - 1 downto 0 do
	begin
	  if (Components[i] is tdatasource) and (lowercase(Components[i].name)=lowercase(name)) then
	  begin
		result:=tdatasource(Components[i]);
		exit;
	  end;
	end; ;

end;


function tdatamodul.findtable(tname:string):tzquery;
var
 i:integer;
begin
	result:=nil;
  if tname='q_firma' then result:=q_firma;
  if tname='q_mitarbeiter_stammdaten' then result:=q_mitarbeiter;
  if tname='q_untersuchung' then result:=q_untersuchung;
  if result<>nil then	exit;
  for I := datamodul.ComponentCount - 1 downto 0 do
	begin
	  if (Components[i] is tzquery) and (lowercase(Components[i].name)=lowercase(tname)) then
	  begin
		result:=tzquery(Components[i]);
		exit;
	  end;
	end; ;
end;


procedure tdatamodul.sql_open;
var
 i:integer;
begin
	datamodul.connection_main.hostname:=db_host;
	datamodul.connection_main.database:=db_datenbank;
	datamodul.connection_main.Connect;
  //datamodul.tr_main.Host:=db_host;
	for I := datamodul.ComponentCount - 1 downto 0 do
	begin
  try
	  if (Components[i] is tzquery) and (trim(tzquery(Components[i]).sql.text)<>'') then
    begin
      if assigned(form_start) then form_start.info('�ffne '+tzquery(Components[i]).name);
    	 tzquery(Components[i]).open;
    end;
	 except
		showmessage(tzquery(Components[i]).sql.text +' konnte nicht ausgef�hrt werden, bitte DatenbankTools.exe ausf�hren --> Tabellen reparieren ');
   end;
	end; ;
end;

procedure tdatamodul.sql_close;
var
 i:integer;
begin
	for I := datamodul.ComponentCount - 1 downto 0 do
	begin
	  if (Components[i] is tzquery) and  (tzquery(Components[i]).active) then
      begin
      try
        pause(2);
         tzquery(Components[i]).Close;
         while  tzquery(Components[i]).active do;
       except
        //showmessage( tzquery(Components[i]).name);
       end;
         //tzquery(Components[i]).active:=false;
      end;
	end;
  pause(2);
  connection_grant.Disconnect;
  connection_hs.Disconnect;
	datamodul.connection_main.Disconnect;
  //showmessage ('fini');

end;

procedure TDataModul.q_firmaBeforePost(DataSet: TDataSet);
begin
dateipost(dataset);
//form_main.sperren(form_main.TabSheet_FirmenAdresse);
end;

procedure TDataModul.q_vorlagenAfterScroll(DataSet: TDataSet);
begin
{try       //20101019
  //if q_vorlagen.tag=1 then exit;
  //q_vorlagen.tag:=1;
  try
  if q_vorlagen.ControlsDisabled then exit;
  //q_vorlagen.DisableControls;

	if assigned(form_vorlagen) and (form_vorlagen.radiogroup_allgemein<>nil)and (not q_vorlagen.ControlsDisabled) then form_main.olecontainer_laden(q_vorlagen,'ole',form_vorlagen.Arbeitsmedizin_vorlagen);
  finally
  //q_vorlagen.EnableControls;
  end;
  //q_vorlagen.tag:=0;
except
end; }
end;

function TDataModul.sql_post(table:tzquery):boolean;
begin
	if table.State in [dsedit,dsinsert] then
	begin
    not_null_abfragen(table);    //12-7-5
		table.post;
    //table.ApplyUpdates;
    result:=true;
	end
	else result:=false;
end;

procedure TDataModul.sql_memo_post(table:tzquery; ole: tolecontainerext);
begin
  if ((ole.modified) or (table.State in [dsedit, dsinsert])) and (not mysql_d.ds_geloescht) then        //vorher or
  begin
    //table.Edit;
    if datensatz(table) then
    begin
       if not (table.State in [dsedit, dsinsert]) then table.edit;
       table.post;
       //table.ApplyUpdates;
       ole.modified:=false;
    end;   
  end;
end;


procedure TDataModul.q_termin0AfterScroll(DataSet: TDataSet);
begin
	if (form_termine<>nil) and (q_termin0['i_art']<>null) then form_termine.details(q_termin0['i_art']);
end;


procedure tdatamodul.pre_post(table: tdataset);
var
	 i_untersucher:integer;
	 query,kuerzel,nr,f:string;
begin
if  (table.state in [dsedit,dsinsert]) then
begin
	//form_main.formular_sperren;       120501



	if not tzquery(table).findfield('storno').isnull then
	begin
	  if (tzquery(table).findfield('storno').asinteger=1) then
		begin
		  showmessage('Gel�schte Datens�tze k�nnen nicht ver�ndert gespeichert werden.');
		  direktspeichern:=true;
		  tzquery(table).cancelUpdates;
		  abort;
	  end;
	end;

  // richedit speichern
  if tzquery(table).name='q_firma' then
	begin
    richeditspeichern(table,'memo',form_main.RichEdit_firma);
    richeditspeichern(table,'memo_revorgabe',form_main.RichEdit_revorgabe);

  end;
  if tzquery(table).name='q_firma_ansprech' then
	begin
    richeditspeichern(table,'memo',form_main.RichEdit_firma_ansprech);
  end;

   if tzquery(table).name='q_firma_zeiten_soll' then
	begin
    richeditspeichern(table,'memo',form_main.RichEdit_zeiten_soll);
  end;
   if tzquery(table).name='q_mitarbeiter' then
	begin
    richeditspeichern(table,'memo',form_main.RichEdit_proband);
  end;


  ///////////////////////////
	{direktspeichern:=(not speichern_fragen) or direktspeichern;

	if (not direktspeichern)  then
	if not (MessageDlg('Speichern des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
	 begin
		direktspeichern:=true;
		tzquery(table).cancelUpdates;
		abort;
	end; }
	if table.findfield('i_untersucher')<>nil then
	begin
		  i_untersucher:=table.FieldByName('i_untersucher').asinteger;
		  if i_untersucher>0 then akt_untersucher:=i_untersucher;
	end;
	if tzquery(table).name='q_firma' then
	begin
	 kuerzel:=tzquery(table).findfield('kuerzel').asstring;
	 nr:=tzquery(table).findfield('nummer').asstring;
	 if kuerzel='' then exit;

	 query:=format('select nummer from firma where kuerzel="%s" and nummer<>%s and storno=0',[kuerzel,nr]);
	 f:=mysql_d.Feldinhalt(query,0);
	 if f<>'' then
    begin
	 	showmessage('Das Feld "K�rzel" '+kuerzel +' wurde bei einer anderen Firma schon benutzt - es muss eindeutig sein!');
      tzquery(table).findfield('kuerzel').asstring:=tzquery(table).findfield('kuerzel').asstring+'1';
    end;
	end;

end;
direktspeichern:=false;
status_bearbeiten(table);
end;

//procedure tdatamodul.not_null_abfragen(table: tdataset);
procedure tdatamodul.not_null_abfragen(table: tzquery);
var
i:integer;
begin
for i:=0 to table.Fields.Count -1 do
begin
     if table.fields[i].required and table.fields[i].isnull then
     case table.fields[i].datatype of
       ftsmallint,ftinteger,ftfloat,ftlargeint,ftword:table.fields[i].asstring:='0';
       ftdate,ftdatetime:table.fields[i].asstring:='01.01.2000';
       fttime:table.fields[i].asstring:='00:00:00';
       ftstring,ftFixedChar,ftWideString	:table.fields[i].asstring:='';
       ftboolean:table.fields[i].asstring:='0';
       ftmemo,ftFmtMemo	:table.fields[i].asstring:='';
       ftblob:table.fields[i].asstring:='';
       ftUnknown :table.fields[i].asstring:='';
     else
     	 table.Fields[i].AsString:='';
     end;
end;
end;



procedure tdatamodul.unt_pre_post(table: tdataset);
begin
if  (table.state in [dsedit,dsinsert]) then
begin
  form_main.formular_sperren;
  {direktspeichern:=(not speichern_fragen) or direktspeichern;
	if (not direktspeichern) then
	if not (MessageDlg('Speichern des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
	 begin
		direktspeichern:=true;
		tzquery(table).cancelUpdates;
		abort;
	end; }
end;
direktspeichern:=false;
end;

procedure TDataModul.q_mitarbeiterBeforePost(DataSet: TDataSet);
begin
	if q_mitarbeiter.state in [dsedit,dsinsert] then
	begin
		q_mitarbeiter.FindField('s_name').asstring:=soundex.soundex( q_mitarbeiter.findfield('name').asstring);
     //form_main.brief_adressenschreiben;
	end;
	pre_post(dataset);

end;


procedure TDataModul.q_dokumenteAfterScroll(DataSet: TDataSet);
var
k:twincontrol;
begin
try


	if (q_dokumente.tag=0)  and assigned(form_main) and (not ( dataset.State=dsinactive)) then
	begin
     if form_main.arbeitsmedizin_proband.State <>osempty then
	    begin
        form_main.arbeitsmedizin_proband.DestroyObject;
	    end;

    if (form_main.dbTreeView.change_allowed) and ( not q_dokumente.ControlsDisabled) then
    begin
         try
           k:=Form_main.ActiveControl;
           form_main.dbTreeView.change_allowed:=false;
           //q_dokumente.DisableControls;
           form_main.olecontainer_laden(q_dokumente,'ole',form_main.arbeitsmedizin_Proband);
         finally
           //q_dokumente.EnableControls;
           form_main.dbTreeView.change_allowed:=true;
           if k<>nil then k.SetFocus;
         end;
    end;
  end;
except
	showmessage('Anzeige-Anwendung vermutlich nicht korrekt installiert');
end;

end;

procedure TDataModul.q_texteAfterScroll(DataSet: TDataSet);
var
k:twincontrol;
begin
try


	if (q_texte.tag=0)  and assigned(form_main) and (not ( dataset.State=dsinactive)) then
	begin
     //while q_texte.ControlsDisabled  do pause(100);
     //if not  q_texte.ControlsDisabled  then
     //if (q_texte.tag=0)  then
      if form_main.arbeitsmedizin_Firma.State <>osempty then
	    begin
        form_main.arbeitsmedizin_Firma.DestroyObject;
	    end;

      try
        k:=Form_main.ActiveControl;
        //q_texte.DisableControls;
        form_main.DBgridEXT_texte.Enabled:=false; //wird nicht disabled

		    form_main.olecontainer_laden(q_texte,'ole',form_main.arbeitsmedizin_Firma);
      finally
        //q_texte.EnableControls;
         form_main.DBgridEXT_texte.Enabled:=true;
        if (k<>nil) and k.enabled then k.SetFocus;
      end;
  end;
except
	showmessage('Anzeige-Anwendung vermutlich nicht korrekt installiert');
end;
end;


function TDataModul.sql_beinhaltet(master: tzquery;tabelle, Feld:string):boolean;
var
	s_nummer:string;
	query:string;
begin
	 result:=false;
	 s_nummer:=getbigint_str(master,'nummer');
	 query:=format('select %0:s from %1:s  where %0:s =%2:s and storno=0 ',[feld,tabelle,s_nummer]);
	 result:= mysql_d.sql_exe_rowsvorhanden(query);
end;


procedure TDataModul.q_dokumenteBeforePost(DataSet: TDataSet);
var
	t:ttime;
begin

if form_main.arbeitsmedizin_Proband.state in [osempty] then exit;
if form_main.arbeitsmedizin_Proband.state in [osopen] then form_main.arbeitsmedizin_Proband.UpdateObject;
if (dataset.State in [dsedit, dsinsert]) and    //or  2015-11-18
		(form_main.arbeitsmedizin_Proband.modified )  then
begin
	//form_main.formular_sperren;
	if tzquery(dataset).findfield('storno')<>nil then
	begin
	  if tzquery(dataset)['storno'] then
		begin
		  showmessage('Gel�schte Datens�tze k�nnen nicht ver�ndert gespeichert werden.');
		  direktspeichern:=true;
		  tzquery(dataset).cancelUpdates;
		  abort;
	  end;
	end;

  {direktspeichern:=(not speichern_fragen) or direktspeichern;
	if (not direktspeichern)then
	if not (MessageDlg('Speichern des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
	 begin
		tzquery(dataset).cancelUpdates;
		abort;
	end;}

    if not q_dokumente.ControlsDisabled  then
    try
      //q_dokumente.DisableControls;
      form_main.olecontainer_speichern(true,q_dokumente,'ole',form_main.arbeitsmedizin_Proband);
    finally
      //q_dokumente.EnableControls;
    end;

 	direktspeichern:=false;
end;

end;

procedure TDataModul.q_texteBeforePost(DataSet: TDataSet);
var
	t:ttime;
begin

if form_main.arbeitsmedizin_firma.state in [osempty] then exit;
if form_main.arbeitsmedizin_firma.state in [osopen] then form_main.arbeitsmedizin_Firma.UpdateObject;
if (dataset.State in [dsedit, dsinsert]) and    //or  2015-11-18
		(form_main.arbeitsmedizin_Firma.modified)  then
begin
	//form_main.formular_sperren;
	if tzquery(dataset).findfield('storno')<>nil then
	begin
	  if tzquery(dataset)['storno'] then
		begin
		  showmessage('Gel�schte Datens�tze k�nnen nicht ver�ndert gespeichert werden.');
		  direktspeichern:=true;
		  tzquery(dataset).cancelUpdates;
		  abort;
	  end;
	end;

    if not q_texte.ControlsDisabled  then
    try
      //q_texte.DisableControls;
      form_main.olecontainer_speichern(true,q_texte,'ole',form_main.arbeitsmedizin_Firma);
    finally
      //q_texte.EnableControls;
    end;

  	direktspeichern:=false;
end;
end;

procedure TDataModul.q_vorlagenBeforePost(DataSet: TDataSet);
begin
{try
if q_vorlagen.tag=1 then abort; //laden
if (not assigned(form_vorlagen))  then exit;
if not form_vorlagen.Arbeitsmedizin_vorlagen.Modified then exit;

if (dataset.State in [dsedit, dsinsert]) then
   begin
     form_main.formular_sperren;
		if (not direktspeichern) then
     begin
	    	//if (MessageDlg('Speichern der Vorlage?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
           form_main.olecontainer_speichern(true,q_vorlagen,'ole',form_vorlagen.Arbeitsmedizin_vorlagen);
		end
		else form_main.olecontainer_speichern(true,q_vorlagen,'ole',form_vorlagen.Arbeitsmedizin_vorlagen);
     direktspeichern:=false;
   end;
except
end; }
end;

procedure TDataModul.q_vorlagenBeforeScroll(DataSet: TDataSet);
begin
{
2011/2/23  wierder zur�ck
if q_vorlagen.tag=1 then exit;
try
	if assigned(form_vorlagen) and(form_vorlagen.radiogroup_allgemein<>nil) then
  begin
	 sql_memo_post(q_vorlagen,form_vorlagen.Arbeitsmedizin_vorlagen);
     form_main.sperren(form_vorlagen.panel_li);
  end;
except
end;}
end;

procedure TDataModul.q_texteBeforeScroll(DataSet: TDataSet);
var
t:ttime;
begin
    //nur bei qtexte
    //while q_texte.tag=1  do pause(100);
    if not mysql_d.ds_geloescht then
    try
      if form_main.arbeitsmedizin_firma.state in [osopen] then form_main.arbeitsmedizin_Firma.UpdateObject;
      if form_main.arbeitsmedizin_firma.modified then q_texte.Edit;        //dauert evtl 1 s bis modified
      sql_memo_post(q_texte,form_main.arbeitsmedizin_Firma);
      form_main.formular_sperren;
    except
      showmessage('Fremd-Anwendung antwortet nicht');
    end;
    //BeforeScroll(DataSet);
end;

procedure TDataModul.ds_laborDataChange(Sender: TObject; Field: TField);
begin
try
  if not assigned(form_main) then exit;
	begin
		form_main.dbedit_labor_wert.visible:=true;
		form_main.DBText_einheit.visible:=true;
		form_main.Label_labor_wert.visible:=true;
	end;
except
end;
end;


function sql_datetostr(datum:tdate):string;
var
s_datum:string;
begin
s_datum:=datetostr(datum);

result:='"'+dat_delphi_to_sql(s_datum)+'"';
end;

function dat_delphi_to_sql(datum: string): string; //delphi-datum zu sql_datum
begin
//12.10.2015 -->2015-10-12
	result:=copy(datum,7,4)+'-'+copy(datum,4,2)+'-'+copy(datum,1,2);
end;

function dattime_to_sql(datum,time: string): string; //delphi-datum zu sql_datum
begin

	result:='"'+copy(datum,7,4)+'-'+copy(datum,4,2)+'-'+copy(datum,1,2) +' '+time+'"';

end;



function dat_sql_to_delphi(datum: string): string; //sql_datum zu delphi
begin
//2015-10-12 --> 22.10.2015
    result:=copy(datum,9,2)+'.'+copy(datum,6,2)+'.'+copy(datum,1,4);
end;


function dat_sql_to_delphi1(datum: string): string; //sql_datum zu delphi
begin
    result:=copy(datum,1,10);
end;


function sql_string_float(s:string):string;     //komma durch punkt ersetzen
begin
     result:=stringreplace(s,',','.',[rfReplaceAll]);
end;

function floattosqlstring(x:real):string;
begin
   result:=floattostr(x);
   result:=sql_string_float(result);
end;

procedure refresh_all;
var i:integer;
k:tcomponent;
begin
for I := datamodul.ComponentCount - 1 downto 0 do
	begin
	  k := tcomponent(datamodul.Components[I]);
      if k.ClassType=tzquery then
      begin
      try
        if tzquery(k).active then tzquery(k).refresh;
      except
      end;
      end;
    end;
end;


procedure TDataModul.reptable;
var i:integer;
query,tab_name,pfad:string;

begin
try
    sql_new(false,datamodul.q_1,'select tab_name from export_import ','');
    datamodul.q_1.first;
    while not datamodul.q_1.eof do
    begin

      	tab_name:=datamodul.q_1['tab_name'];

			query:='repair table ' + tab_name +' EXTENDED';

			application.processmessages;
		  //query:='repair table mitarbeiter'  ;
			SQL_Processor.script.Clear;
			SQL_Processor.Script.Add(query);
			SQL_Processor.Execute;


         query:='OPTIMIZE TABLE ' + tab_name ;
			application.processmessages;
		   //query:='repair table mitarbeiter'  ;
			SQL_Processor.script.Clear;
			SQL_Processor.Script.Add(query);
			SQL_Processor.Execute;
         datamodul.q_1.next;
    end;

except
      showmessage('Fehler beim Reparieren, Bitte melden Sie dieses Problem');
end;
end;

procedure TDataModul.q_AfterPost(DataSet: TDataSet);
begin
if arbene_geladen then
begin
	form_main.arbeitsmedizin_Firma.Modified:=false;
	form_main.arbeitsmedizin_Proband.Modified:=false;
  form_main.firmaanzeigen;
	//dataset.Refresh;  warum??
end;
end;

procedure TDataModul.BeforeScroll(DataSet: TDataSet);
begin
	if  (dataset.state in [dsbrowse]) and (not dataset.ControlsDisabled) then
  begin

   if (dataset.name='q_firma') then
    begin
       if form_main.RichEdit_firma.Modified then
       begin
         dataset.Edit;
         dataset.Post;
       end;
       if form_main.RichEdit_revorgabe.Modified then
       begin
         dataset.Edit;
         dataset.Post;
       end;
       

    end;
     if (dataset.name='q_firma_ansprech') then
    begin
       if form_main.RichEdit_firma_ansprech.Modified then
       begin
         dataset.Edit;
         dataset.Post;
       end;
    end;
    if (dataset.name='q_firma_zeiten_soll') then
    begin
       if form_main.RichEdit_zeiten_soll.Modified then
       begin
         dataset.Edit;
         dataset.Post;
       end;
    end;
     if (dataset.name='q_mitarbeiter') then
    begin
       if form_main.RichEdit_proband.Modified then
       begin
         dataset.Edit;
         dataset.Post;
       end;
    end;
    form_main.formular_sperren;
  end;
end;

procedure TDataModul.termin_BeforePost(DataSet: TDataSet);
begin
if  (dataset.state in [dsedit,dsinsert]) then
begin
  try
    //if form_termine<>nil then form_termine.termin_info_erzeugen(dataset);
  except
  end;
end;

end;

procedure TDataModul.termin_BeforeScroll(DataSet: TDataSet);
begin
 if  (dataset.state in [dsbrowse]) and (form_termine<>nil ) then form_termine.formular_sperren;
end;

procedure TDataModul.q_schluesselBeforeScroll(DataSet: TDataSet);
begin
try
	if assigned(form_berechtigung) and (form_berechtigung.edit_p_alt<>nil) then
	begin
		form_berechtigung.alles_speichern;
		form_berechtigung.pagecontrol.ActivePage:=form_berechtigung.tabsheet_passwort;
		form_berechtigung.PageControlchange(form_berechtigung.PageControl);
	end;
except
end;
end;



procedure TDataModul.q_firmaAfterScroll(DataSet: TDataSet);
var
query,s_firma:string;
begin
try
	if assigned(form_main) then
  begin
          s_firma:=datamodul.q_firma.findfield('nummer').asstring;
          if s_firma<>'' then
          begin
              query:=format('select * from rechnung_regeln where i_firma=%s and storno=0 order by name',[s_firma]);
            datamodul.sql(false,q_rechnung_regeln,query,'');
            if datensatz(q_rechnung_regeln) then form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=true else  form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=false;
          end
          else
            form_main.GroupBox_rechnung_regeln_einstellungen.Visible:=false;

          if q_firma.findfield('parent').asinteger=0 then form_main.GroupBox_rechnung_automatisch.Visible:=true else  form_main.GroupBox_rechnung_automatisch.Visible:=false;// abh�ngige Firma
          richeditladen(dataset,'memo',form_main.RichEdit_firma);
          richeditladen(dataset,'memo_revorgabe',form_main.RichEdit_revorgabe);
          form_main.firmaanzeigen;
  end;
except
end;
end;


procedure TDataModul.q_ambulanzAfterScroll(DataSet: TDataSet);
begin
 if form_main.dbTreeView.change_allowed then
	begin
	 gridladen(form_main.grid_ambulanz,dataset );
	 form_main.arbeitsunfall_show;
	end;
end;

procedure TDataModul.q_ambulanzBeforeScroll(DataSet: TDataSet);
begin

	if  (q_arbeitsunfall.state in [dsedit, dsinsert]) then q_arbeitsunfall.Post;
	//if  (dataset.state in [dsbrowse]) then form_main.formular_sperren;
end;

procedure TDataModul.q_laborBeforePost(DataSet: TDataSet);
begin
	if not tzquery(dataset).findfield('storno').isnull then
	begin
	  if (tzquery(dataset).findfield('storno').asinteger=1) then
		begin
		  showmessage('Gel�schte Datens�tze k�nnen nicht ver�ndert gespeichert werden.');
		  direktspeichern:=true;
		  tzquery(dataset).cancelUpdates;
		  abort;
	  end;
	end;
  if dataset.findfield('i_untersucher').asinteger>0 then akt_untersucher:=dataset.FieldByName('i_untersucher').asinteger;
	status_bearbeiten(dataset);
end;

procedure TDataModul.status_bearbeiten(DataSet: TDataSet);
var
s_nummer:string;
query:string;
abgeschlossen:boolean;
begin
if not ((dataset=q_labor) or (dataset=q_untersuchung) or (dataset=q_impfung))  then exit;

//

if status_change_at_post then  //automatisch den status auf abgesclossen (4) setzen
begin
	if (dataset =q_untersuchung) and  (dataset['i_beurteilung']>0) and(dataset['i_uart']>=1) and ( dataset['i_status']<4)  then  //muss beurteilt sein
			begin
			  dataset['i_status']:=4;
        if dataset['i_untersucher']<=0 then dataset['i_untersucher']:=akt_untersucher;
			  dataset['farbe']:=status_farbe(dataset['i_status']);
			  status_neu:=true;
        if  untersuchung_planen_at_post and (dataset.tag=0)then
          begin
            dataset.Tag:=7;
          end;
			end;

  if dataset=q_untersuchung then
  begin

	   abgeschlossen:=(q_untersuchung['i_beurteilung']<>null) and
    (q_untersuchung['i_untersucher']<>null) and
    (q_untersuchung['i_typ']<>null);

		if (not abgeschlossen) and (q_untersuchung['i_status']=4) then
		BEGIN
			  showmessage('Es fehlen noch "Typ", "Untersucher" oder "Beurteilung".');
			  q_untersuchung.edit;
			  q_untersuchung['i_status']:=3;
		end;
		q_untersuchung['farbe']:=status_farbe(q_untersuchung['i_status']);



  end;

	if dataset = q_impfung then
	    if (dataset['dosis']<>'') and 	(dataset['i_stoff']<>null) and
		 (dataset['i_untersucher']<>null) and (dataset['i_typ']<>null) and (dataset['charge']<>'')
		 then
		 begin
		  dataset['i_status']:=4;
      if dataset['i_untersucher']<=0 then dataset['i_untersucher']:=akt_untersucher;
		  dataset['farbe']:=status_farbe(dataset['i_status']);
		  status_neu:=true;
		 end;

  if dataset=q_impfung then
    begin
    abgeschlossen:=(q_impfung['i_stoff']<>null) and (q_impfung['i_untersucher']<>null) and (q_impfung['i_typ']<>null);

		if (not abgeschlossen) and (q_impfung['i_status']=4) then
		BEGIN
			  showmessage('Es fehlen noch "Typ", "Impfstoff", "Dosis" oder "Untersucher".');
			  q_impfung.edit;
			  q_impfung['i_status']:=3;
		end;
			  q_impfung['farbe']:=status_farbe(q_impfung['i_status']);
    end;


   if dataset=q_labor then
    if (dataset['wert']>0) or ((dataset['ergebnistext']<>null) and (length(dataset['ergebnistext'])>0)) then
    begin
      dataset['i_status']:=4;
      if dataset['i_untersucher']<=0 then dataset['i_untersucher']:=akt_untersucher;
      dataset['farbe']:=status_farbe(dataset['i_status']);
      status_neu:=true;
    end;

  if dataset=q_labor then
  begin
      abgeschlossen:=(q_labor['wert']<>null) and
      (q_labor['i_untersucher']<>null) and  (q_labor['i_typ']<>null);

      if (not abgeschlossen) and (q_labor['i_status']=4) then
      BEGIN
          showmessage('Es fehlen noch "Typ", "Wert" oder "Untersucher".');
          q_labor.edit;
          q_labor['i_status']:=4;
      end;
       q_labor['farbe']:=status_farbe(q_labor['i_status']);

  end;

end;
//termin l�schen wenn nicth auf geplant steht
  if (dataset['i_status']<>2 )   then
  begin
    s_nummer:=getbigint_str(tdataset(dataset),'i_termin');
    if s_nummer<>'0' then
    begin
      setbigint(tdataset( dataset),'i_termin',0);
      query:=format('delete from termine where nummer=%s',[s_nummer]);
      {t:=}mysql_d.sql_exe(query);
      if form_termine<>nil then form_termine.kalender_akutalisieren;
    end;
  end;
end;



procedure  tdatamodul.gridladen(grid: tstringgrid;dataset: tdataset );
var x,y: integer;
	  zelle:string;
begin
	if kein_datensatz(dataset) then
	begin
	 for y:= 0 to grid.rowcount-1 do
	  begin
	  for x:=0 to grid.colcount-1 do
		begin
			grid.cells[x,y]:='';
		end;
	  end;
	 exit
	end;
	grid.tag:=0;//dataset['nummer'];
	for y:= 0 to grid.rowcount-1 do
	  begin
	  for x:=0 to grid.colcount-1 do
		begin
			zelle:=format('s%d%d',[x,y]);
			grid.cells[x,y]:=dataset.findfield(zelle).asstring;
		end;
	  end;
end;


procedure  tdatamodul.gridschreiben(grid: tstringgrid;dataset: tdataset);
var x,y: integer;
	  zelle:string;
begin
	if grid.tag=0 then exit;
	dataset.edit;
	for y:=0 to grid.rowcount-1 do
	begin
		for x:=0 to grid.colcount-1 do
		begin
			zelle:=format('s%d%d',[x,y]);
			dataset[zelle]:=grid.cells[x,y];
		end;
	 end;
  grid.tag:=0;
end;



procedure TDataModul.q_ambulanzBeforePost(DataSet: TDataSet);
begin
if  (dataset.state in [dsedit,dsinsert]) then
begin
  form_main.formular_sperren;
  {direktspeichern:=(not speichern_fragen) or direktspeichern;
	if (not direktspeichern) then
	if not (MessageDlg('Speichern des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
	 begin
		tzquery(dataset).cancelUpdates;
		abort;
	end;}
end;
end;

procedure TDataModul.q_mitarbeiterAfterScroll(DataSet: TDataSet);
begin
if assigned(form_main) then
  begin
      richeditladen(dataset,'memo',form_main.RichEdit_proband);
    //form_main.formular_sperren;
     form_main.taetigkeitanzeigen;
     form_main.abteilunganzeigen;
   end;
end;

procedure TDataModul.q_serienfelderAfterScroll(DataSet: TDataSet);
begin
if q_serienfeldinhalte=nil then exit;
	sql_relation( 'serienfeldinhalte','i_serienfeld',q_serienfelder, q_serienfeldinhalte,'reihenfolge');
end;

function getbigint(tabelle: tdataset;feldname:string):int64;
var
r:string;
begin
try
if tabelle.findfield(feldname)<>nil then
  r:= tabelle.FindField(feldname).AsString;
except
end;
if r='' then result:=0 else result:=strtoint64(r);
end;

function getbigint_str(tabelle: tdataset;feldname:string):string;
begin
result:='';
try
if tabelle.findfield(feldname)<>nil then
   result:=tabelle.FindField(feldname).AsString;
except
end;
end;

procedure setbigint(tabelle: tdataset;feldname:string;wert:int64);
begin
try
if tabelle.findfield(feldname)<>nil then
   tabelle.FindField(feldname).AsString := inttostr(wert);
except
end;
end;



procedure TDataModul.q_sql_abfragenAfterScroll(DataSet: TDataSet);
begin
try
   if assigned(form_auswertung)  then form_auswertung.notebook_refresh;
except
end;
end;

procedure TDataModul.ZMySqlMonitor1MonitorEvent(Sql, Result: String);
var str:string;
begin
     str:=sql+'#'+result;
end;

function tdatamodul.query_check(query:string):string;
var
position:integer;
query_low,s:string;
begin
  query_low:=lowercase(query);
  if trim(storno)='' then
    begin
       result :=query_low;
       exit;
    end;
  position:=pos('where',query_low);

  if position>0 then
  begin
    if storno_show then s:='' else s:=format(' and %s ',[storno]);
    position:=pos('order',query_low);
    if position >0 then 	insert(s,query,position-1)
    else query:=query+s;
  end
  else
  begin //kein where
    if storno_show then s:='' else s:=format(' where %s ',[storno]);
    position:=pos('order',query_low);
    if position>0 then //order vorhanden
    begin
       insert(s,query,position-1);
    end
    else
    begin
       query:=query+s;
    end;
  end;
	result:=query;
end;


procedure TDataModul.q_rechnungAfterScroll(DataSet: TDataSet);
var query,s1:string;
begin
if q_rechnung.tag<>0 then exit;

if datensatz(dataset) then s1:=q_rechnung.findfield('nummer').asstring else s1:='-1';
	query:=format('select *,format(netto,2) as netto_format, format(brutto,2) as brutto_format, format(faktor,2) as ff  from re_positionen where i_rechnung="%s" order by i_mitarbeiter,db,datum, re_text ',[s1]); //order by datum,i_mitarbeiter,rf
	sql_new(true,q_repositionen,query,'');
end;

function mysql_strtofloat(s:string):real;
begin
  s:=stringreplace(s,',',decimalseparator,[rfreplaceall]);
  s:=stringreplace(s,'.',decimalseparator,[rfreplaceall]);
  result:=strtofloat(s);
end;

procedure sql_normieren(var text:string);
begin
     text:=stringreplace(text,'"',char(39),[rfReplaceAll]);
end;

procedure TDataModul.q_schluesselAfterScroll(DataSet: TDataSet);
begin
  try
    if assigned(form_berechtigung) and (form_berechtigung.edit_p_alt<>nil) then
    begin
       form_berechtigung.edit_p_user.text:= datamodul.q_schluessel.findfield('name').asstring;
       form_berechtigung.alles_speichern;
    end;
  except
  end;
end;


procedure tdatamodul.listbox_lesen(listbox:tlistbox;q_steuer:tzquery; feld_steuer:string;tab_ausgabe, feld_ausgabe:string);
var
nr_steuer,query,s:string;
p_str:pstrpointer;
begin
  listbox_leeren(listbox);
  q_steuer.first;
  while not q_steuer.eof do
  begin
     nr_steuer:=q_steuer.findfield(feld_steuer).asstring;
     query:=format('select %s from %s where nummer=%s',[feld_ausgabe,tab_ausgabe,nr_steuer ]);
     s:=mysql_d.Feldinhalt(query,0);
     new(p_str);
     p_str^.s:=q_steuer.findfield('nummer').asstring;
     listbox.Items.AddObject(s,tobject(p_str));
     q_steuer.next;
  end;
  listbox.ItemIndex:=0;
  listbox.OnClick(tobject(listbox));
end;


procedure tdatamodul.listbox_leeren(listbox:tlistbox);
var
i:integer;
begin
  for i:= 0 to listbox.Items.count-1 do
       dispose(pstrpointer(listbox.Items.Objects[i]));
  listbox.items.Clear;
end;

procedure tdatamodul.listbox_locate(sender:tobject;listbox:tlistbox;q_steuer:tzquery);
var
p_str:pstrpointer;
i:integer;
s:string;
begin
  if tlistbox(sender).items.count=0 then exit;
  i:=tlistbox(sender).itemindex;
  p_str:=pstrpointer(tlistbox(sender).items.objects[i]);
  s:=p_str^.s;
  q_steuer.Locate('nummer',strtoint64(s),[]);
end;


procedure TDataModul.ds_texteDataChange(Sender: TObject; Field: TField);
begin

end;
//20100729 q_firmasollist beforescroll gel�scht
procedure TDataModul.q_vorlagenAfterPost(DataSet: TDataSet);
begin
//form_main.arbeitsmedizin_Proband.Modified:=false;    ##2015/07/18
end;

procedure TDataModul.q_untersuchungAfterScroll(DataSet: TDataSet);
begin
if not assigned(form_main) then exit;
if not arbene_geladen then exit;
if (q_untersuchung.findfield('i_paw').asinteger<>1) and (not form_main.zust_arbmed_besch_vorhanden(datamodul.q_untersuchung.findfield('i_weitergabe').asinteger,datamodul.q_untersuchung.findfield('i_typ').asinteger,datamodul.q_untersuchung.findfield('datum').AsDateTime ))    then form_main.image_paw.Visible:=true else  form_main.image_paw.Visible:=false;
end;

procedure TDataModul.DataModulCreate(Sender: TObject);
begin
	if connection_main.Connected=true then showmessage('Achtung, connected');
end;

procedure TDataModul.q_firma_sollistAfterScroll(DataSet: TDataSet);
begin
 if form_main.BitBtn_zeit_km= nil then exit;
 if datamodul.q_firma_sollist.findfield('km_gefahren').asfloat=0
   then
   begin
     form_main.km_verbergen;
     form_main.BitBtn_zeit_km.Caption:='km eintragen';

   end
   else
   begin
     	form_main.km_anzeigen;
   	  form_main.BitBtn_zeit_km.Caption:='zur�cksetzen';
   end;
end;

procedure TDataModul.q_sql_abfragenBeforeScroll(DataSet: TDataSet);
begin
try
if not assigned(form_main) then exit;
if not arbene_geladen then exit;

if dataset.State in  [dsedit,dsinsert ] then dataset.Post;
except
end;
end;

procedure TDataModul.q_untersuchungAfterPost(DataSet: TDataSet);
begin
if dataset.tag=7 then
try
  //dataset.Refresh;
  application.ProcessMessages;
  form_main.timer_untplan.Enabled:=true;
  //form_main.unt_planen(datamodul.q_untersuchung);
finally
  dataset.tag:=0;
end;
end;


procedure TDataModul.q_anamneseAfterScroll(DataSet: TDataSet);
begin
if  modus_history and  (b.b_string_silent('proband-anamnese-arzt')>=0)  then form_main.anamnese_laden(0);
end;

procedure TDataModul.q_akt_untersuchungAfterScroll(DataSet: TDataSet);
begin
if modus_history then form_main.bereich_change(false);
end;

procedure TDataModul.ds_firma_sollistStateChange(Sender: TObject);
begin
  //if q_firma_sollist.State in [dsbrowse] then q_firma_sollist.Refresh;  //  20170501 whyyyyy
end;



procedure TDataModul.q_firma_ansprechAfterScroll(DataSet: TDataSet);
begin
try
	if assigned(form_main) then
  begin
    richeditladen(dataset,'memo',form_main.RichEdit_firma_ansprech);
  end;
except
end;
end;

procedure TDataModul.q_firma_zeiten_sollAfterScroll(DataSet: TDataSet);
begin
  try
	if assigned(form_main) then
  begin
          richeditladen(dataset,'memo',form_main.RichEdit_zeiten_soll);
  end;
except
end;
end;


function table_length(tab_name:string):int64;
var
query:string;
begin
 query:=format('select count(*) from %s ',[tab_name]);
 result:=strtoint(mysql_d.Feldinhalt(query,0));
end;


procedure TDataModul.ZsqlMonitorLogTrace(Sender: TObject; Event: TZLoggingEvent);
begin
  {if trim (Event.Error) <> '' then
    memMontor.Lines.Add(DateTimeToStr(Event.Timestamp) + ': ' +
      Event.Message + #13#10 + '   Error: ' + Event.Error)
  else
    memMontor.Lines.Add(DateTimeToStr (Event.Timestamp) + ': ' + Event.Message);}

  if modus_debug then 
   if trim (Event.Error) <> '' then showmessage(DateTimeToStr(Event.Timestamp) + ': ' +
      Event.Message + #13#10 + '   Error: ' + Event.Error);
end; // sqlMonitorLogTrace



procedure TDataModul.q_firma_sollistBeforeScroll(DataSet: TDataSet);
begin
	if  (dataset.state in [dsbrowse]) and (not dataset.ControlsDisabled) then
  begin
    form_main.formular_sperren;
  end;
end;

procedure TDataModul.q_rechnung_regelnAfterScroll(DataSet: TDataSet);
begin
if q_firma.state in [dsedit] then q_rechnung_regeln.edit;
end;

procedure TDataModul.q_rechnung_regelnBeforeScroll(DataSet: TDataSet);
begin
  if q_rechnung_regeln.State in [dsedit, dsinsert] then q_rechnung_regeln.Post;
end;

procedure TDataModul.ds_firma_sollistDataChange(Sender: TObject;
  Field: TField);
begin
  //if not assigned(field) then exit;
  //if field.Fieldname='i_zeitkonto' then  form_main.zeitkontoChange;

end;

procedure TDataModul.ds_besonderheitenDataChange(Sender: TObject;
  Field: TField);
begin
  //if q_besonderheiten.FindField('datum_bis')=field then Form_main.DBEdit_bes_erledigt_datChange(form_main.DBEdit_bes_erledigt_dat);
  //form_main.DBEdit_bes_erledigt_dat.Field.OnChange:=ds_firma_sollistDataChange;
end;

end.
