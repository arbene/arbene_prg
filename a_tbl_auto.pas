unit a_tbl_auto;

interface

uses
  ComObj, arbene_TLB;

type
  Tapp = class(TAutoObject, Iapp)
  protected
    procedure tb; safecall;
  end;

implementation

uses ComServ,forms,a_main;

procedure Tapp.tb;
begin

 application.BringToFront;
end;

initialization
  TAutoObjectFactory.Create(ComServer, Tapp, Class_app, ciMultiInstance);
end.
