�
 TFORM_NAMSUCH 0+  TPF0TForm_namsuchForm_namsuchLeft�Top� Width�Height�HelpContextBorderIconsbiSystemMenu CaptionProband suchenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder
OnActivateFormActivateOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left TopTWidth�Height+AlignalBottomTabOrder  TBitBtnBitBtn1Left Top	Width� HeightTabOrder KindbkOK  TBitBtnBitBtn2LeftTopWidth� HeightTabOrderKindbkCancel   
TDBgridEXT
DBgridEXT1Left Top� Width�Height�AlignalClient
DataSourceDataModul.ds_mitarbeiter_suchDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExitdgMultiSelect ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT1DblClick	OnKeyDownDBgridEXT1KeyDown
OnKeyPressDBgridEXT1KeyPresskeine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamenameTitle.CaptionNameWidth� Visible	 Expanded	FieldNamevornameTitle.CaptionVornameWidth� Visible	 Expanded	FieldNamegeb_datTitle.CaptionGeburtsdatumWidthMVisible	 Expanded	FieldNamefirmaTitle.CaptionFirmaWidth� Visible	 ButtonStylecbsNoneDropDownRowsExpanded	FieldNamearchivFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionArchivWidth$Visible	    
TStatusBar	StatusBarLeft TopWidth�HeightPanelsWidth2  Visible  TPanel	Panel_topLeft Top Width�Height� HelpContextP-AlignalTopTabOrder TLabelLabel1LeftTopWidthvHeightCaptionNachname, VornameFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TEditEdit1LeftTop(Width)HeightTabOrder OnChangeEdit1Change	OnKeyDownEdit1KeyDown  TBitBtnBitBtn4Left�Top4Width� HeightCaptionListe filtern / aktualisierenTabOrderOnClickBitBtn4Click  	TCheckBoxCheckBox_email_fLeft� TopWWidth� HeightCaptionhat email Adresse FirmaTabOrderVisibleOnClickCheckBox_email_fClick  	TCheckBoxCheckBox_email_pLeftTopVWidth� HeightCaptionhat email Adresse privatTabOrderVisibleOnClickCheckBox_email_fClick  	TGroupBox	GroupBox1LeftXTopWidthYHeightaCaptionFilterTabOrder TLabelLabel2LeftTop(Width*HeightCaptionVorname  TLabelLabel3LeftTop@WidthBHeightCaptionGeburtsdatum  TLabelLabel4LeftTopWidthHeightCaptionName  TEditEdit_vornameLefthTop#Width� HeightTabOrder  	TMaskEditMaskEdit_datumLefthTop=WidthIHeightEditMask!99/99/0000;1;_	MaxLength
TabOrderText
  .  .      TEdit	Edit_nameLefthTopWidth� HeightTabOrder    TButtonButton_zurueckLeft�TopTWidth� HeightCaption   Filter zurücksetzenTabOrderOnClickButton_zurueckClick  TRadioGroupRadioGroup_fLeft�TopWidth� Height!Caption
SortierungColumns	ItemIndex Items.Strings
phonetischalphabetisch TabOrderOnClickRadioGroup_fClick    