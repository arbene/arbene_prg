unit s_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, HCMngr, DCPcrypt2, DCPsha1, DCPblockciphers,
	DCPtwofish, Buttons, ComCtrls,MimeMess ,SMTPsend,POP3send ,registry,math;
      
type
  TForm_main = class(TForm)
    cipher: TDCP_twofish;
    DCP_sha11: TDCP_sha1;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    Edit_ps: TEdit;
    Edit_s: TEdit;
    CheckBox_ambulanz: TCheckBox;
    CheckBox_impfung: TCheckBox;
    CheckBox_sat: TCheckBox;
    CheckBox_ap: TCheckBox;
	  BitBtn1: TBitBtn;
    StatusBar: TStatusBar;
    Memo: TMemo;
    Label2: TLabel;
    Edit_name: TEdit;
    Label_name: TLabel;
    RadioButton_300: TRadioButton;
    RadioButton_600: TRadioButton;
    RadioButton_1200: TRadioButton;
    RadioButton_alle: TRadioButton;
    TabSheet3: TTabSheet;
    Edit1: TEdit;
    Label3: TLabel;
    BitBtn2: TBitBtn;
    Edit2: TEdit;
    procedure FormCreate(Sender: TObject);
	  procedure RadioGroupClick(Sender: TObject);
	  procedure Button1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private-Deklarationen }
	  key:string;
	  mime:TMIMEMess;
	  pop:tpop3send;
	  smtp:tsmtpsend;
	  email_userein, email_name,email_pop,email_smtp, email_passwortein,sendto:string;
	 // function mime_add_adressen:boolean;
	  function send:boolean;
	  function test_connect:boolean;
	  procedure form_positionieren(formular: tform);
	  
  public
    { Public-Deklarationen }
	end;

var
	Form_main: TForm_main;

implementation


{$R *.DFM}

procedure TForm_main.FormCreate(Sender: TObject);
var s1,s2:string;
begin
s1:='13tdatenbank kann nicht geoeffnet werden bz7eqianahuaor9ia9dieser vorgang ist verbotensddh98239al93 bitte abbrechenassie sind hierzu nicht berechtigtdj1af';
	 s2:=copy(s1,21,20)+copy(s1,1,20)+copy(s1,41,20)+copy(s1,101,20)+copy(s1,61,20)+copy(s1,81,20)+copy(s1,121,8);
	 key:=s2;
   //key muss immer gleich bleiben
mime:=TMIMEMess.create;
pop:=tpop3send.create;
//einstellungen_lesen;
email_userein:='schluessel@arbene.de';
email_name:='schluessel@arbene.de';
email_pop:='post.strato.de';
email_smtp:='post.strato.de';
email_passwortein:='12abka57';
sendto:='klawitter@arbene.de';
form_positionieren(tform(sender));
end;

procedure tform_main.form_positionieren(formular: tform);
var hoehe:integer;
begin
	hoehe:=screen.Height-30;
	formular.height:=min(formular.height, height);
	formular.height:=min(formular.height, hoehe);
	formular.Width:=min(formular.Width,Width);
	formular.Width:=min(formular.Width,screen.Width);
	formular.Top:=top+25+(height div 2)-(formular.Height div 2);
	if formular.top+formular.Height>hoehe then
	formular.top:=hoehe-formular.Height;
	formular.top:=max(formular.top,0);
	formular.Left:=left + (width div 2) - (formular.width div 2);
	if formular.left+formular.Width>screen.Width then
		formular.left:=screen.width-formular.width;
	formular.left:=max( formular.left,0);
end;

procedure TForm_main.RadioGroupClick(Sender: TObject);


var
s1,s2,k1,k2,z,v,k:string;
bin,i:integer;

function keinesonderzeichen(s:string):boolean;
var sz,z:string;
i:integer;
begin
result:=true;
sz:='+-/\*=';
for i:=1 to length(sz)do
begin
	 z:=copy(sz,i,1);
	result:=(result and (pos(z,s)=0))
end;
end;

function janein(wf:boolean):string;
begin
if wf=true then result:=' ja' else result:=' nein';
end;

begin
	k1:=trim(edit_ps.text);
	if length(k1)<>7 then
	begin
	 showmessage('falsche Schlüsslelänge');
	 exit;
	end;

	if length(trim(edit_name.text))=0 then
	begin
	 showmessage('Name muss eingegeben werden');
	 exit;
	end;

	memo.Clear;
	memo.Lines.Add('Name: '+edit_name.text);
	memo.Lines.Add('Programmschlüssel: '+edit_ps.text);
	//memo.Lines.Add('Produktiv' +janein(checkbox_prg.checked));
	memo.Lines.Add('Satellit '+janein(checkbox_sat.checked));
	memo.Lines.Add('Mehrplatz'+janein(checkbox_ap.checked ));
	memo.Lines.Add('Ambulanz '+janein(checkbox_ambulanz.checked));
	memo.Lines.Add('Impfung  '+janein(checkbox_impfung.checked));
	if  radiobutton_300.checked then memo.Lines.Add('Probandenzahl: 300')  ;
	if  radiobutton_600.checked then memo.Lines.Add('Probandenzahl: 600');
	if  radiobutton_1200.checked then memo.Lines.Add('Probandenzahl: 1200');
	if  radiobutton_alle.checked then memo.Lines.Add('Probandenzahl: unbegrenzt');

	if  radiobutton_300.checked then bin:=128;
	if  radiobutton_600.checked then bin:=bin or 64;
	if  radiobutton_1200.checked then bin:=bin or 32;
	if  radiobutton_alle.checked then bin:=bin or 16;
	if checkbox_ambulanz.checked then bin:=bin or 8;
	if checkbox_impfung.checked then bin :=bin or 4;
	if checkbox_sat.checked then bin :=bin or 2;
	if checkbox_ap.checked then bin :=bin or 1;

	{s2:=inttostr(bin);

	while length(s2)<3 do s2:='0'+s2;}
	s2:=inttohex(bin,4);

	v:=inttostr(random(100000))+inttostr(random(100000))+inttostr(random(100000));
	v:=copy(v,1,3);//##
	i:=1;
	repeat
	begin
	  inc(i);
	  k:=v+k1+s2+inttostr(random(100000))+'kl'+inttostr(random(100000))+'qw'+inttostr(random(100000))+'23573485823759';
	  k:=copy(k,1,15);
	  Cipher.InitStr     (Key,TDCP_sha1);
	  s1:=cipher.EncryptString(k);
	  cipher.Burn;
	end;
	until (keinesonderzeichen(s1) or (i>1000));

	//s1:=copy(s1,1,18);
	insert(' ',s1,6);
	insert(' ',s1,12);
	insert(' ',s1,18);

	//email(s1);
	memo.Lines.Add('Freischaltschlüssel: '+s1);
	if send then edit_s.text:=s1 else edit_s.text:='' ;


end;




procedure TForm_main.Button1Click(Sender: TObject);
begin
 //email('a');
end;

function TForm_main.send:boolean;
procedure keineverbindung;
begin
	showmessage('Keine Verbindung zum Mail-Server');
	//if sichtbar then form_email.addmail(0,memo_to.lines,editsubject.text,memo.lines,comboboxattachedfiles.Items);
	statusbar.Panels[0].text:='Keine Verbindung POP3-Server; Schlüssel kann nicht erzeugt werden ';
	screen.cursor:=crDefault;
	result:=false;
end;


var
i:integer;

begin
	 
	 
	 result:=false;


	 //form_email.popini;
	 screen.cursor := crHourGlass;

	 //if  not mime_add_adressen then exit;


	 statusbar.Panels[0].text:='Verbindung POP3-Server';
	 application.ProcessMessages;
	 result:=test_connect;
	 if not result then
	 begin
		keineverbindung;

		application.ProcessMessages;
		exit;
	 end;
	 statusbar.Panels[0].text:='Mail vorbereiten';
	 application.ProcessMessages;
	 mime.Clear;
	 mime.PartList.clear;
	 mime.Header.Subject := 'Arbene Schluessel';
	 mime.Header.From:= email_name;//'info@arbene.de';
	 mime.Header.ToList.Add(sendto);// :=sendto;
	 mime.Header.Date:=now();
	 //mime.Header.CCList.Add('');

	 mime.AddPartText(tstringlist(memo.lines));

	 //for i:=0 to ComboBoxAttachedFiles.Items.Count-1 do  mime.AddPartBinary(ComboBoxAttachedFiles.Items[i]);
	 mime.EncodeMessage;
	 mime.FinalizeHeaders;

	 statusbar.Panels[0].text:='Versenden';
	 application.ProcessMessages;

	 if SendToRaw(email_name, sendto, email_smtp, mime.Lines, '',''){email_name, email_passwortein)} then //email_useraus
	 begin
		  //	form_email.Sb.Panels[0].Text := '';
			result:=true;

	 end
		else keineverbindung;

	 screen.cursor := crDefault;
	 statusbar.Panels[0].text:='Versenden abgeschlossen';
	 end;



function tform_main.test_connect:boolean;
var s:string;
begin
	 result:=true;
	 pop.Password:=email_passwortein;
	 pop.Username:= email_userein;
	 pop.POP3Host:=email_pop;
	 pop.Timeout:=5000;
	 result:= pop.Login;
	 pop.Logout;
end;



procedure TForm_main.BitBtn2Click(Sender: TObject);
var i:integer;
s:string;
function keinesonderzeichen(s:string):boolean;
var sz,z:string;
i:integer;
begin
result:=true;
sz:='+-/\*=';
for i:=1 to length(sz)do
begin
	 z:=copy(sz,i,1);
	result:=(result and (pos(z,s)=0))
end;
end;
begin
   i:=1;
	repeat
	begin
	  inc(i);
	  Cipher.InitStr (Key,TDCP_sha1);
     s:=inttostr(random(9))+edit1.text+inttostr(random(9));

	  edit2.text:=cipher.EncryptString(s);
     //cipher.Burn;
	end;
	until (keinesonderzeichen(edit2.text) or (i>1000));

end;

end.
