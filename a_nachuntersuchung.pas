unit a_nachuntersuchung;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Grids, DBGrids, dbgridEXT,a_str_edit,a_data,
  Mask, ComCtrls, ToolWin,db, dbcombo_number,Variants, TreeView_ext;

type
  TForm_untersuchungstyp = class(TForm)
    PageControl: TPageControl;
    TabSheet_nachunt: TTabSheet;
    TabSheet_unt: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    ToolBar1: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton4: TToolButton;
    ToolButton_delete: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_save: TToolButton;
    ToolButton_cancel: TToolButton;
    ToolButton9: TToolButton;
    Label_wu: TLabel;
    ToolButton10: TToolButton;
    ToolButton_exit: TToolButton;
    TabSheet_impfstoff: TTabSheet;
	  DBgridEXT1: TDBgridEXT;
    DBEdit_impfstoff: TDBEdit;
    Label5: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit6: TDBEdit;
    TabSheet_zug_befunde: TTabSheet;
    GroupBox_befunde: TGroupBox;
    TabSheet_zug_labor: TTabSheet;
    GroupBox_lab: TGroupBox;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    Panel4: TPanel;
    DBRadioGroup1: TDBRadioGroup;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBRadioGroup2: TDBRadioGroup;
    GroupBox3: TGroupBox;
    DBMemo1: TDBMemo;
	  GroupBox4: TGroupBox;
    DBMemo2: TDBMemo;
    Panel8: TPanel;
    Notebook: TNotebook;
	  Label8: TLabel;
	  Label9: TLabel;
	  Label17: TLabel;
	  DBEdit2: TDBEdit;
	  GroupBox_info: TGroupBox;
	  DBMemo: TDBMemo;
	  GroupBox5: TGroupBox;
	  Label15: TLabel;
	  DBEdit_goae: TDBEdit;
	  DBEdit_euro: TDBEdit;
	  RadioButton1: TRadioButton;
	  RadioButton2: TRadioButton;
    DBCombo_num_fristen: TDBCombo_num;
    DBCheckBox7: TDBCheckBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    ListBox_befunde: TListBox;
    Panel6: TPanel;
    DBCheckBox_abr: TDBCheckBox;
    ListBox_labor: TListBox;
    GroupBox6: TGroupBox;
    ListBox_nachuntersuchung: TListBox;
    DBCheckBox_ust: TDBCheckBox;
    Label_n_paw: TLabel;
    DBCombo_num_nach_paw: TDBCombo_num;
    Panel_left: TPanel;
    TreeView: TTreeView_ext;
    Panel9: TPanel;
    Label7: TLabel;
    Label13: TLabel;
    DBEdit_unt_dauer: TDBEdit;
    Label246: TLabel;
    DBCombo_num_unt_art: TDBCombo_num;
    Label1: TLabel;
    DBCombo_num_grundlage: TDBCombo_num;
    DBMemo_anlass: TDBMemo;
    DBMemo_impfung: TDBMemo;
    DBMemo3: TDBMemo;
    Panel7: TPanel;
    DBCheckBox8: TDBCheckBox;
    Label3: TLabel;
    DBRadioGroup_ust_impfstoff: TDBRadioGroup;
    DBCombo_num_frist_erst: TDBCombo_num;
    Label_eu: TLabel;
	  
	  procedure ToolButton_newClick(Sender: TObject);
	  procedure ToolButton_editClick(Sender: TObject);
	  procedure ToolButton_saveClick(Sender: TObject);
	  procedure ToolButton_cancelClick(Sender: TObject);
	  procedure ToolButton_deleteClick(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure ToolButton_exitClick(Sender: TObject);
	  procedure PageControlChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure SpeedButton1Click(Sender: TObject);
	  procedure SpeedButton2Click(Sender: TObject);
	  procedure FormHide(Sender: TObject);
	  procedure FormShow(Sender: TObject);
	  procedure PageControlChange(Sender: TObject);
	  procedure DBLookupComboBox_untersuchungExit(Sender: TObject);
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
	  procedure DBEdit_punkteChange(Sender: TObject);
	  procedure RadioButton1Click(Sender: TObject);
    procedure ListBox_befundeClick(Sender: TObject);
    procedure ListBox_laborClick(Sender: TObject);
    procedure ListBox_nachuntersuchungClick(Sender: TObject);
    procedure TreeViewChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure formularsperren;
    procedure formularentsperren;
    procedure DBEdit_unt_dauerExit(Sender: TObject);
    procedure DBEdit_unt_dauerKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure DBRadioGroup_ust_impfstoffChange(Sender: TObject);
	private
	  { Private-Deklarationen}
	  procedure befunde_auswahl;
	  procedure labor_auswahl;
    procedure nachunt_auswahl;
    procedure kartei_aktualisieren;
	public
		bereich:integer;
		procedure unt_speichern(direkt:boolean);
		procedure alles_sperren;
		procedure AfterScroll(DataSet: TDataSet);

	  { Public-Deklarationen}
	end;


var
  Form_untersuchungstyp: TForm_untersuchungstyp;

implementation

uses a_main, a_unt_wahl, a_u_wahl;

{$R *.DFM}

procedure TForm_untersuchungstyp.ToolButton_newClick(Sender: TObject);
var reihenfolge,nr:integer;
	  stext,p,rf:string;
     knoten: ttreenode;
begin
if pagecontrol.ActivePage=tabsheet_unt then
begin
{if datensatz(datamodul.q_2) then
begin
	datamodul.q_2.last;
	reihenfolge:=datamodul.q_2['reihenfolge']+1;
end
else reihenfolge:=1; }
     p:= datamodul.q_2.findfield('parent').asstring;
     rf:=datamodul.q_2.findfield('reihenfolge').asstring;
     if p='' then p:='0';
     if rf='' then rf:='0';

	neuer_datensatz(datamodul.q_2,['nummer','untersuchung','bereich','reihenfolge','parent'],[null,'',bereich,rf,p]) ;
	datamodul.q_2.post;
   nr:=datamodul.q_2.findfield('nummer').asinteger;
	//typ_filtern;
	form_main.entsperren(tabsheet_unt);

   treeview.liste_lesen;
	treeview.reihenfolge_neu;
   knoten:=treeview.suche_nummer(nr);
   form_main.entsperren(tabsheet_unt);
	notebook.SetFocus;
end;

if pagecontrol.ActivePage=tabsheet_nachunt then
begin
  nachunt_auswahl;

  PageControl.OnChange(tobject(pagecontrol));
  listbox_nachuntersuchung.itemindex:=listbox_nachuntersuchung.Items.count-1;
  listbox_nachuntersuchung.OnClick(listbox_nachuntersuchung);
	form_main.entsperren(tabsheet_nachunt);

end;

if pagecontrol.ActivePage=tabsheet_impfstoff then
begin
	neuer_datensatz(datamodul.q_impfstoff,['nummer','i_typ'],[null,datamodul.q_2['nummer']]);
	form_main.entsperren(tabsheet_impfstoff);
	DBEdit_impfstoff.SetFocus;
end;

if pagecontrol.ActivePage=tabsheet_zug_befunde then
begin

  befunde_auswahl;
  PageControl.OnChange(tobject(pagecontrol));
  listbox_befunde.itemindex:=listbox_befunde.Items.count-1;
  listbox_befunde.OnClick(listbox_befunde);
  form_main.entsperren(tabsheet_zug_befunde);
end;

if pagecontrol.ActivePage=tabsheet_zug_labor then
begin
  labor_auswahl;
  PageControl.OnChange(tobject(pagecontrol));
  listbox_labor.itemindex:=listbox_labor.Items.count-1;
  listbox_labor.OnClick(listbox_labor);
  form_main.entsperren(tabsheet_zug_labor);

end;
 toolbutton_delete.enabled:=true;
 toolbutton_save.enabled:=true;
//toolbutton_delete.enabled:=false;
//toolbutton_save.enabled:=false;
end;

procedure TForm_untersuchungstyp.nachunt_auswahl;
var
nr_s:string;
bereich:integer;
knoten: ttreenode;
daten:pnodepointer;
s_nachunt,ausw:string;
begin
     try
       form_u_wahl:=tform_u_wahl.Create(self);
       form_u_wahl.auswahl_einstellen(0,1);
       if (form_u_wahl.showmodal<>mrok)  then exit;

       bereich:= form_u_wahl.RadioGroup.ItemIndex+1;
       ausw:= form_u_wahl.RadioGroup.Items[ form_u_wahl.RadioGroup.ItemIndex];
       if ausw='Vorsorgeuntersuchung' then bereich:=1;
       if ausw='Impfung' then bereich:=2;
       if ausw='Labor' then bereich:=3;
     finally
            form_u_wahl.release;
     end;

     try
       form_untwahl:=tform_untwahl.Create(self);
       form_untwahl.ini(bereich,date);
       form_untwahl.multiselect:=false;
       form_untwahl.notebook.pageindex:=bereich-1;   //passt gerade
       if form_untwahl.showmodal<>mrok then exit;

       knoten :=form_untwahl.Tree.Selected;
       daten:=knoten.Data;
       s_nachunt:=inttostr(daten^.nummer);
     finally
       form_untwahl.release;
     end;
     nr_s:=datamodul.q_2.findfield('nummer').asstring;
     neuer_datensatz(datamodul.q_nachuntersuchung,['nummer','i_typ','i_nachuntersuchung','uil' ],[null,nr_s,s_nachunt,bereich]) ;
     datamodul.q_nachuntersuchung.post;
end;


procedure TForm_untersuchungstyp.ToolButton_editClick(Sender: TObject);
begin
if pagecontrol.ActivePage=tabsheet_unt then
begin
	  if kein_datensatz(datamodul.q_typ) then exit;

	  form_main.entsperren(tabsheet_unt);
    notebook.SetFocus;
end;

if pagecontrol.ActivePage=tabsheet_nachunt then
begin
	if kein_datensatz(datamodul.q_nachuntersuchung) then exit;
	form_main.entsperren(tabsheet_nachunt);
end;

if pagecontrol.ActivePage=tabsheet_impfstoff then
begin
	if kein_datensatz(datamodul.q_impfstoff) then exit;
	form_main.entsperren(tabsheet_impfstoff);
	  DBEdit_impfstoff.SetFocus;
end;
if pagecontrol.ActivePage=tabsheet_zug_befunde then
	begin
	  if kein_datensatz(datamodul.q_3) then exit;
		form_main.entsperren(tabsheet_zug_befunde);
	end;
if pagecontrol.ActivePage=tabsheet_zug_labor then
	begin
	  if kein_datensatz(datamodul.q_5) then exit;
		form_main.entsperren(tabsheet_zug_labor);
	end;
 toolbutton_delete.enabled:=true;
 toolbutton_save.enabled:=true;
end;

procedure TForm_untersuchungstyp.ToolButton_saveClick(Sender: TObject);
begin
	unt_speichern(true);
   kartei_aktualisieren;
end;

procedure TForm_untersuchungstyp.ToolButton_cancelClick(Sender: TObject);
begin
with datamodul do
begin
 if (q_2.state=dsedit){ or (q_typ.state=dsinsert) }then
 begin
	form_main.sperren(tabsheet_unt);
	//cancel_loeschen(q_typ) ;
 end;
 if (q_nachuntersuchung.state=dsedit){ or (q_nachuntersuchung.state=dsinsert) } then
 begin
	form_main.sperren(tabsheet_nachunt);
	//cancel_loeschen(q_nachuntersuchung) ;

 end;
 if (q_impfstoff.state=dsedit){ or (q_impfstoff.state=dsinsert) }then
 begin
	 form_main.sperren(tabsheet_impfstoff);
	//cancel_loeschen(q_impfstoff) ;
 end;
  kartei_aktualisieren;
 alles_sperren;
end;
end;

procedure TForm_untersuchungstyp.ToolButton_deleteClick(Sender: TObject);
begin
	//if Messagedlg('Soll der aktuelle Datensatz gel�scht werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
	if pagecontrol.ActivePage=tabsheet_unt then
	begin
	  dateiloeschen(datamodul.q_2, false,false);
	end;

	if pagecontrol.ActivePage=tabsheet_nachunt then
	begin
	  dateiloeschen( datamodul.q_nachuntersuchung,true,true);
    pagecontrol.OnChange(pagecontrol);
	end;
	if pagecontrol.ActivePage=tabsheet_impfstoff then
	begin
	  dateiloeschen(datamodul.q_impfstoff,false,false);
	 //datamodul.q_impfstoff.delete;
	end;
	if pagecontrol.ActivePage=tabsheet_zug_befunde then
	begin
	  dateiloeschen(datamodul.q_3,false,false);
    //datamodul.q_3.refresh;
    pagecontrol.OnChange(pagecontrol);
	end;
	if pagecontrol.ActivePage=tabsheet_zug_labor then
	begin
	  dateiloeschen(datamodul.q_5,false,false);
    pagecontrol.OnChange(pagecontrol);
	  //datamodul.q_3.refresh;
	end;
   kartei_aktualisieren;
	alles_sperren;
end;

procedure TForm_untersuchungstyp.FormCreate(Sender: TObject);
var query:string;
begin
bereich:=1;
TabSheet_unt.Caption:='Bescheinigungen';
TabSheet_impfstoff.tabVisible:=false;
if not show_labor then tabsheet_zug_labor.tabvisible:=false;
caption:='Bearbeiten der Bescheinigung';
Notebook.PageIndex:=0;


if prg_typ=3 then
begin
	radiobutton1.Visible:=false;
	dbedit_goae.Visible:=false;
	radiobutton2.Checked:=true;
	TabSheet_zug_befunde.TabVisible:=false;
	TabSheet_nachunt.TabVisible:=false;
	TabSheet_zug_labor.TabVisible:=false;
	TabSheet_impfstoff.TabVisible:=false;
	caption:='Behandlungen';
	tabsheet_unt.Caption:='';

end;

application.ProcessMessages;
pagecontrol.ActivePage:=tabsheet_unt;
grid_suchen(sender);
dbcombo_num_fristen.lookup_laden;
DBCombo_num_frist_erst.lookup_laden;
datamodul.q_2.AfterScroll:=AfterScroll;
alles_sperren;
end;





procedure TForm_untersuchungstyp.ToolButton_exitClick(Sender: TObject);
begin
unt_speichern(false);
modalresult:=mrok;
end;


procedure TForm_untersuchungstyp.unt_speichern(direkt:boolean);
var
query,unt:string;
begin
direktspeichern:=direkt;
with datamodul do
begin
if pagecontrol.ActivePage=tabsheet_unt then
begin
//try
	sql_post(q_2);
	q_typ.Refresh;
	form_main.sperren(tabsheet_unt);
//except
//  showmessage('Zeit f�r die Untersuchungsdauer ist nicht korrekt');
//end;
end;
if pagecontrol.ActivePage=tabsheet_zug_befunde then
begin
	sql_post(q_3);
  pagecontrol.OnChange(pagecontrol);
	form_main.sperren(tabsheet_zug_befunde);
end;

if pagecontrol.ActivePage=tabsheet_zug_labor then
begin
	sql_post(q_5);
  pagecontrol.OnChange(pagecontrol);
	form_main.sperren(tabsheet_zug_labor);
end;

if pagecontrol.ActivePage=tabsheet_nachunt then
begin
  if q_nachuntersuchung.State in [dsedit, dsinsert] then
  begin
    query:='select untersuchung from typ where nummer='+q_nachuntersuchung.findfield('i_typ').asstring;
    unt:=mysql_d.Feldinhalt(query,0);
    //q_nachuntersuchung.edit;
    q_nachuntersuchung.FindField('untersuchung').asstring:=unt;
  end;
	sql_post(q_nachuntersuchung);
  pagecontrol.OnChange(pagecontrol);
	form_main.sperren(tabsheet_nachunt);
end;

if pagecontrol.ActivePage=tabsheet_impfstoff then
begin
	sql_post(q_impfstoff);
	form_main.sperren(tabsheet_impfstoff);
end;

toolbutton_delete.enabled:=false;
toolbutton_save.enabled:=false;
end;
end;


procedure TForm_untersuchungstyp.alles_sperren;
begin
datamodul.q_2.Filtered:=false;
form_main.sperren(pagecontrol.ActivePage);
toolbutton_delete.enabled:=false;
toolbutton_save.enabled:=false;
end;

procedure TForm_untersuchungstyp.PageControlChanging(Sender: TObject;
	var AllowChange: Boolean);
begin
	unt_speichern(false);
end;

procedure TForm_untersuchungstyp.SpeedButton1Click(Sender: TObject);
begin
form_main.tab_reihenfolge(datamodul.q_2,0);
end;

procedure TForm_untersuchungstyp.SpeedButton2Click(Sender: TObject);
begin
form_main.tab_reihenfolge(datamodul.q_2,1);
end;



procedure TForm_untersuchungstyp.FormHide(Sender: TObject);
begin
datamodul.q_typ.refresh;
end;

procedure TForm_untersuchungstyp.FormShow(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
treeview.Selected:=treeview.Items[0];
end;

procedure TForm_untersuchungstyp.PageControlChange(Sender: TObject);
	var
  query:string;
  
  nr_str:string;
begin
with datamodul do
begin
nr_str:=getbigint_str(q_2,'nummer');

if pagecontrol.ActivePage=tabsheet_nachunt then
begin
   query:='select * from nachuntersuchung where storno=0 and i_typ= '+q_2.findfield('nummer').asstring;
	 //sql_relation( ( 'nachuntersuchung','i_typ',q_2,q_nachuntersuchung,'');
   sql_new(false,q_nachuntersuchung,query,'');
	 //q_nachuntersuchung.refresh;
   datamodul.listbox_lesen(listbox_nachuntersuchung,datamodul.q_nachuntersuchung,'i_nachuntersuchung','typ','untersuchung');
end;
if pagecontrol.ActivePage=tabsheet_impfstoff then
begin
	 sql_relation('impfstoff','i_typ',datamodul.q_2,datamodul.q_impfstoff,'');
	 q_1.refresh;
end;
if pagecontrol.ActivePage=tabsheet_zug_befunde then
begin
   //query:='select unt_befunde.nummer,bereich.reihenfolge,bereich.goae, bereich.name, bereich.euro,unt_befunde.storno,unt_befunde.prioritaet, unt_befunde.abrechnen, unt_befunde.eu,unt_befunde.nu,unt_befunde.ngu,unt_befunde.memo '
	 //			+' from unt_befunde left join bereich on(unt_befunde.i_befunde=bereich.nummer ) where unt_befunde.storno=0 and unt_befunde.i_typ= '+inttostr(q_2['nummer']);
	query:=format('select * from unt_befunde where unt_befunde.storno=0 and unt_befunde.i_typ=%s order by ts ',[q_2.findfield('nummer').asstring]);
	sql_new(false,q_3,query,'');
  datamodul.listbox_lesen(listbox_befunde,datamodul.q_3,'i_befunde','bereich','name');

end;
if pagecontrol.ActivePage=tabsheet_zug_labor then
begin
  query:='select * from unt_labor where unt_labor.storno=0 and unt_labor.ii_typ= '+q_2.findfield('nummer').asstring;
	//query:='select typ.goae, typ.untersuchung, typ.euro,typ.reihenfolge, unt_labor.nummer, unt_labor.i_typ,unt_labor.ii_typ,unt_labor.storno,unt_labor.prioritaet,unt_labor.abrechnen, unt_labor.eu,unt_labor.nu,unt_labor.ngu, unt_labor.memo '
	//		+' from unt_labor left join typ on(unt_labor.i_typ=typ.nummer ) where unt_labor.storno=0 and unt_labor.ii_typ= '+inttostr(q_2['nummer']);
	sql_new(false,q_5,query,'');
  datamodul.listbox_lesen(listbox_labor,datamodul.q_5,'i_typ','typ','untersuchung');

end;
end;
alles_sperren;
end;

procedure TForm_untersuchungstyp.DBLookupComboBox_untersuchungExit(
	Sender: TObject);
begin
	 datamodul.q_1.filtered:=false;
end;

procedure tform_untersuchungstyp.befunde_auswahl;
var
		 i:integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
         s_befund,s_typ:string;

begin
with datamodul do
try

// befunde
	form_untwahl:=tform_untwahl.Create(self);
	form_untwahl.tag:=1;
	form_untwahl.notebook.pageindex:=4;
	form_untwahl.TabSheet_2.TabVisible:=false;
	form_untwahl.notebook.enabled:=false;
	form_untwahl.multiselect:=false;
	form_untwahl.parent_markieren:=false;
	form_untwahl.ToolBar.Visible:=false;
	form_untwahl.Panel_botttom.Visible:=true;
	form_untwahl.caption:='Untersuchungs-Befunde';
	//  form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.tree.mysqlquery:=datamodul.q_bereich;
	form_untwahl.tree.mysql_feld:='name';
	form_untwahl.tree.liste_lesen;



	if form_untwahl.showmodal<>mrok then exit;

	if form_untwahl.Tree.Selected=nil then exit;

	s_typ:=q_2.findfield('nummer').asstring;

  knoten :=form_untwahl.Tree.Selected;
  daten:=knoten.Data;
  s_befund:=inttostr(daten^.nummer);
  neuer_datensatz(q_3,['i_typ','i_befunde','prioritaet'],[s_typ,s_befund,'1' ]);
  q_3.post;

finally
	form_untwahl.release;
end;
end;


procedure tform_untersuchungstyp.labor_auswahl;
var
		 i:integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
      s_lab_typ,s_typ:string;

begin
with datamodul do
try

	//datamodul. sql_new(true,datamodul.q_typ_tree,format('select * from typ where bereich = %d',[3]), 'reihenfolge');
	form_untwahl:=tform_untwahl.Create(self);
	
   form_untwahl.ini(3,date);
   form_untwahl.multiselect:=false;
   form_untwahl.notebook.pageindex:=2;

   if form_untwahl.showmodal<>mrok then exit;
	 if form_untwahl.Tree.Selected=nil then exit;

	 s_typ:=q_2.findfield('nummer').asstring;


  knoten :=form_untwahl.Tree.Selected;
  daten:=knoten.Data;
  s_lab_typ:=inttostr(daten^.nummer);
  neuer_datensatz(q_5,['ii_typ','i_typ','prioritaet','abrechnen','eu','nu','ngu'],[s_typ,s_lab_typ,1,1,1,1,0 ]);
  q_5.post;

finally
	form_untwahl.release;
end;
end;

procedure TForm_untersuchungstyp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	ToolButton_exitClick(Sender);
	datamodul.q_2.AfterScroll:=nil;
  datamodul.listbox_leeren(listbox_befunde);
end;

procedure TForm_untersuchungstyp.DBEdit_punkteChange(Sender: TObject);
var betrag:real;
	  s_betrag:string;
begin
try
if  tdbedit(sender).text='' then exit;
 betrag:=strtoint(tdbedit(sender).text)*g_punktwert;


 if betrag=0 then exit;
	s_betrag:=floattostrf(betrag ,fffixed,15,2);
	dbedit_euro.Text:=s_betrag;

finally
end;

end;

procedure TForm_untersuchungstyp.RadioButton1Click(Sender: TObject);
begin
datamodul.q_2.edit;
if radiobutton1.Checked then
	begin
	  dbedit_goae.Enabled:=true;
	  dbedit_euro.Enabled:=false;

	  datamodul.q_2['euro']:=0;

	  //dbedit_euro.text:='0';

	end
	else
	begin
	  dbedit_goae.Enabled:=false;
	  dbedit_euro.Enabled:=true;
	  datamodul.q_2['goae']:='';
	  //dbedit_goae.text:='';

	end;
end;

procedure TForm_untersuchungstyp.AfterScroll(DataSet: TDataSet);
begin
	if prg_typ<>3 then
	begin
	  if dbedit_euro.text<>'0' then radiobutton2.Checked:=true else radiobutton1.Checked:=true ;
	  RadioButton1Click(nil);
	end;
end;

procedure TForm_untersuchungstyp.ListBox_befundeClick(Sender: TObject);
begin
     datamodul.listbox_locate(sender,listbox_befunde,datamodul.q_3);
end;

procedure TForm_untersuchungstyp.ListBox_laborClick(Sender: TObject);
begin
    datamodul.listbox_locate(sender,listbox_labor,datamodul.q_5);
end;

procedure TForm_untersuchungstyp.ListBox_nachuntersuchungClick(
  Sender: TObject);
  var
  uil:integer;
begin
   datamodul.listbox_locate(sender,listbox_nachuntersuchung,datamodul.q_nachuntersuchung);
   uil:=datamodul.q_nachuntersuchung.findfield('uil').asinteger;
    //alles verbergen
    DBCombo_num_frist_erst.Visible:=false;
    DBCombo_num_nach_paw.visible:=false;
    DBCombo_num_fristen.visible:=false;
    label_eu.visible:=false;
    label_wu.Visible:=false;
    Label_n_paw.visible:=false;

    case uil of
    0: begin
        label_eu.Caption:='Nachuntersuchung mit NEU anlegen. ';
        label_eu.visible:=true;
       end;
    1: begin
        DBCombo_num_frist_erst.Visible:=true;
         DBCombo_num_nach_paw.visible:=true;
        DBCombo_num_fristen.visible:=true;
        label_eu.visible:=true;
        label_wu.Visible:=true;
        Label_n_paw.visible:=true;
        label_wu.Caption:='Frist alle weiteren Untersuchungen';
        label_eu.Caption:='Frist f�r die erste Nach-Untersuchung';
      end;
    2:
      begin
        DBCombo_num_fristen.visible:=true;
        label_wu.Visible:=true;
        label_wu.Caption:='Frist f�r die Auffrischimpfung';
      end;
    3: ;
    end;


end;

procedure TForm_untersuchungstyp.TreeViewChanging(Sender: TObject;
  Node: TTreeNode; var AllowChange: Boolean);
begin
  treeview.tabelle_post;
 formularsperren;
end;

procedure TForm_untersuchungstyp.TreeViewChange(Sender: TObject;
  Node: TTreeNode);
begin
//
end;
procedure tform_untersuchungstyp.formularsperren;
begin
form_main.sperren(notebook);
toolbutton_new.Enabled:=true;
toolbutton_edit.Enabled:=true;
toolbutton_save.Enabled:=false;
toolbutton_delete.Enabled:=false;
end;

procedure tform_untersuchungstyp.formularentsperren;
begin
form_main.entsperren(notebook);
toolbutton_new.Enabled:=false;
toolbutton_edit.Enabled:=false;
toolbutton_save.Enabled:=true;
toolbutton_delete.Enabled:=true;
end;

Procedure tform_untersuchungstyp.kartei_aktualisieren;
var
	i_nummer:int64;
  rubrik:integer;
  daten:pnodepointer;
 knoten: ttreenode;
  mess:string;

begin
 		daten:=treeview.Selected.Data;
      i_nummer:=daten^.nummer;
      treeview.liste_lesen;
      treeview.change_allowed:=false;
      treeview.change_allowed:=true;
      //


      try
      if treeview.items.count=0 then exit;
      knoten:=treeview.items[0];
      while knoten<> nil do
      begin
         daten:=knoten.data;
         if (daten<>nil) and (daten^.nummer=i_nummer)  then
         begin
           treeview.selected:=knoten;
           exit;
         end;
         knoten:=knoten.GetNext;
      end;
      except
      end;


      //
      //treeview.Expand;
end;

procedure TForm_untersuchungstyp.DBEdit_unt_dauerExit(Sender: TObject);
var
t:ttime;
begin
tdbedit(sender).Text:=timetostr( StrTotimeDef(tdbedit(sender).Text, strtotime('00:00') )) ;

end;

procedure TForm_untersuchungstyp.DBEdit_unt_dauerKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
p:integer;
t:string;
begin
tdbedit(sender).text:=lowercase(tdbedit(sender).text);
p:=pos(lowercase(char(key)),tdbedit(sender).text);
if ( key in [0..47,58..95,106..189,191..255])  then
begin
 tdbedit(sender).Text:=copy(tdbedit(sender).Text,0,p-1)+ copy(tdbedit(sender).Text,p+1,20);
end;

if (pos(':',(tdbedit(sender).Text))=1) then tdbedit(sender).Text :='0'+tdbedit(sender).Text;
if (pos(':',(tdbedit(sender).Text))>3) then tdbedit(sender).Text :='';

//if key in [48..57, 96..105,190] then
begin
  t:=trim(tdbedit(sender).Text);
  if length(t)>5
  then tdbedit(sender).Text:=copy(tdbedit(sender).Text,0,5);//+ copy(tdbedit(sender).Text,p+1,20);
end;

if (pos(':',(tdbedit(sender).Text))>0) and (  pos(':',(tdbedit(sender).Text)) +  pos(':',stringwenden(tdbedit(sender).Text))<>length(tdbedit(sender).Text)+1) then
begin
   tdbedit(sender).Text:='';
end;

end;

procedure TForm_untersuchungstyp.FormDestroy(Sender: TObject);
begin
 treeview.liste_loeschen;
end;

procedure TForm_untersuchungstyp.DBRadioGroup_ust_impfstoffChange(
  Sender: TObject);
begin
//dbedit6.SetFocus;

end;

end.
