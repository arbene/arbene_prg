object Form_rechnungsgestaltung: TForm_rechnungsgestaltung
  Left = 803
  Top = 437
  Width = 785
  Height = 286
  BorderIcons = [biSystemMenu]
  Caption = 'Rechnungsgestaltung'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 209
    Width = 769
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BitBtn_ok: TBitBtn
      Left = 126
      Top = 8
      Width = 146
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtn_abb: TBitBtn
      Left = 312
      Top = 8
      Width = 145
      Height = 25
      Caption = 'abbrechen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Kind = bkAbort
    end
  end
  object CheckBox_anonymisieren: TCheckBox
    Left = 28
    Top = 63
    Width = 273
    Height = 14
    Caption = 'Rechnung anonymisieren'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 94
    Width = 769
    Height = 115
    Align = alBottom
    Caption = 
      'Die folgenden Ma'#223'nahmen werden verborgen und die Summe wird  pro' +
      'bandenbezogen zusammengefasst.'
    TabOrder = 3
    object CheckBox_goae: TCheckBox
      Left = 33
      Top = 33
      Width = 442
      Height = 13
      Caption = 'Untersuchungsschritt / Ma'#223'nahmen'
      TabOrder = 0
    end
    object CheckBox_impfstoff: TCheckBox
      Left = 33
      Top = 59
      Width = 78
      Height = 13
      Caption = 'Impfstoff'
      TabOrder = 1
    end
    object CheckBox_labor: TCheckBox
      Left = 33
      Top = 85
      Width = 79
      Height = 13
      Caption = 'Labor'
      TabOrder = 2
    end
  end
  object CheckBox_stornorechnung: TCheckBox
    Left = 29
    Top = 24
    Width = 473
    Height = 17
    Caption = 
      'Stornorechnung erstellen (wenn nicht markiert wird die Originalr' +
      'echnung gedruckt)'
    TabOrder = 0
    Visible = False
  end
end
