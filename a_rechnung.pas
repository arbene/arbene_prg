unit a_rechnung;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ExtCtrls, Mask, Buttons, Spin,db,math, variants,listen, CheckLst,
  DBCtrls,zdataset,StrUtils;

type
	TForm_rechnung = class(TForm)
	  Panel_b: TPanel;
	  BitBtn_ok: TBitBtn;
	  BitBtn_esc: TBitBtn;
	  GroupBox_datum: TGroupBox;
	  Label1: TLabel;
	  Label2: TLabel;
	  MaskEdit_bis: TMaskEdit;
	  MaskEdit_von: TMaskEdit;
	  GroupBox_leistungen: TGroupBox;
	  GroupBox_renu: TGroupBox;
	  Edit_renu: TEdit;
	  CheckBox_pauschal: TCheckBox;
	  CheckBox_stunden: TCheckBox;
    GroupBox_proband: TGroupBox;
	  CheckBox_betrzuordnen: TCheckBox;
    MaskEdit_redat: TMaskEdit;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    CheckBox_unt: TCheckBox;
    CheckBox_impfung: TCheckBox;
    CheckBox_labor: TCheckBox;
    GroupBox_uf: TGroupBox;
    Panel1: TPanel;
    CheckListBox_firmen: TCheckListBox;
    BitBtn_f_tauschen: TBitBtn;
    ComboBox_zeit: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    CheckBox_fahrstrecke: TCheckBox;
    SpeedButton11: TSpeedButton;
    GroupBox2: TGroupBox;
    ComboBox_ust: TComboBox;
    Label7: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    DBEdit_renu: TDBEdit;
    GroupBox3: TGroupBox;
    CheckBox_artikel: TCheckBox;
    CheckBox_bef: TCheckBox;
    CheckBox_offen_auch: TCheckBox;
    CheckBox_k_ust_sticht: TCheckBox;
    GroupBox4: TGroupBox;
    Label_firma: TLabel;
    CheckBox_wiedervorlage: TCheckBox;
	  procedure BitBtn_okClick(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
    procedure BitBtn_f_tauschenClick(Sender: TObject);
    procedure ComboBox_zeitChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);

	private
	  { Private-Deklarationen }
	  renu,svon,sbis:string;
	  {i_firma,} i_renu:integer;
     liste:tlist;
     firmen:string;
	  function rechnung_erstellen(i_firma,parent,hr:integer):int64; 
	  function faktor_kontrolle(faktor:real;goae_typ:integer):real;
     function get_faktor(q:tzquery;goae_typ:integer):real;
	  procedure verbote_raus(i_firma:integer);
	  procedure proband_einfuegen(i_firma,hr:integer);
     procedure rechnung_all;
     procedure re_link(i_firma:integer;renr_parent, renr_child:int64);
     function re_uebersicht(i_firma:integer;parent:int64;list:tchecklistbox):string;
     procedure zeiteinstellen;
	public
	  abschlag:integer;
	  b_f_re:boolean;
    firma_nummer:integer;
 	  function summieren(i_firma:integer;re_parent:int64;list:tlist):int64;
    procedure ini(f_nummer:integer);
     procedure ini_rechnungslauf(regel_nummer:integer);
    procedure zeiteinstellen_monat(m1,m2:integer);
	  { Public-Deklarationen }

	end;

var
	Form_rechnung: TForm_rechnung;

implementation

uses a_main, a_data, a_kalender, a_memo;

{$R *.DFM}
{
db=1, dbnr=0 name
db=1 dbnr=1 Verweis
1 zeitpauschale
2stunden
3 vorsorgeunt
4 einzelbefunde
5 impfung
6 labor
7 impfstoff
8 artikel
9 manuell eingegeben (kann alles sein)


}


procedure TForm_rechnung.BitBtn_okClick(Sender: TObject);
begin
// datamodul.q_repositionen.DisableControls;
 screen.Cursor:=crhourglass;
 form_memo:=tform_memo.create(self);
 form_memo.Caption:='Rechnung wird erstellt';
 form_memo.Panel_b.Visible:=false;
 form_rechnung.visible:=false;
 form_memo.Show;
 rechnung_all;
 form_rechnung.visible:=true;
 screen.Cursor:=crdefault;
 form_memo.free;
 //datamodul.q_repositionen.EnableControls;
	modalresult:=mrok;
end;


procedure tform_rechnung.rechnung_all;    //haupt Rechnungserstellung
var

   i,f:integer;
   renr_parent, renr_child:int64;
   query:string;
begin
try
renu:= edit_renu.text+datamodul.q_main.findfield('renu').asstring;
query:=format('select nummer from rechnung where renu="%s" and storno=0',[renu]);
if mysql_d.feldinhalt(query,0)<>'' then
begin
	showmessage('die Rechnungsnummer wurde schon verwendet');
	exit;
end;
query:=format('select nummer from re_positionen where renu="%s" and storno=0',[renu]);
if mysql_d.feldinhalt(query,0)<>'' then
begin
	showmessage('die Rechnungsnummer wurde schon verwendet');
	exit;
end;

//neue rechnungsnummer erh�hen
i_renu:=datamodul.q_main['renu'];
datamodul.q_main.edit;
datamodul.q_main['renu']:=i_renu+1;  //privat in rechnung
datamodul.q_main.post;


form_memo.Memo.Lines.Add('Hauptrechnung wird erstellt');
//rechnung_erstellen(datamodul.q_firma['nummer'],0,1); //hauptrechnung
rechnung_erstellen(firma_nummer,0,1); //hauptrechnung
form_memo.Memo.Lines.Add('Pr�fen auf unzul�ssige GO�-Kombinationen');
verbote_raus(datamodul.q_firma['nummer']);

if not checkbox_betrzuordnen.Checked then
begin
  form_memo.Memo.Lines.Add('Namen der Probanden werden eingef�gt');
  //proband_einfuegen(datamodul.q_firma['nummer'],1);
  proband_einfuegen(firma_nummer,1);
end;
form_memo.Memo.Lines.Add('Summe wird erstellt');
//renr_parent:=summieren(datamodul.q_firma['nummer'],0,liste);  //nummer der hauptrechnung

renr_parent:=summieren(firma_nummer,0,liste);  //nummer der hauptrechnung

for i:=0 to  checklistbox_firmen.Items.Count-1 do
begin
   form_memo.Memo.Lines.Add('abh�ngige Rechnungen');
   if checklistbox_firmen.Checked[i] then
   begin
      f:=integer(checklistbox_firmen.Items.Objects[i]);

      rechnung_erstellen(f,firma_nummer,0);  //re_positionen erstellen   (f,0,0)
      verbote_raus(f);
      if not checkbox_betrzuordnen.Checked then proband_einfuegen(f,0);
      renr_child:=summieren(f,renr_parent,liste); //erstellt die Rechnung  , verweis auf �bergeordnete Rechnung
      if renr_child>0 then
      re_link(f,renr_parent, renr_child);  //repositionen bei der �bergeordneten Rechnung
    end;  
end;

finally
form_main.rechnung_aktualisieren(renr_parent);
datamodul.q_rechnung.refresh;
datamodul.q_rechnung.Locate('nummer',renr_parent,[]);
form_memo.Memo.Lines.Add('Rechnung wurde erstellt');
end;

end;

procedure tform_rechnung.re_link(i_firma:integer;renr_parent, renr_child:int64);
var
rete,s_betrag,query,f:string;
drechnung:tdate;
cent:integer;
felder:array [0..1] of string;

begin
   query:=format('select eurobetrag,brutto from rechnung where nummer=%d',[renr_child]);
   mysql_d.Feldarray(query,[0,1],felder);
   cent:=round(strtofloat(felder[0])*100);
   s_betrag:=felder[1];
   query:=format('select firma from firma where nummer=%d',[i_firma]);
   f:=mysql_d.Feldinhalt(query,0);
   //rete:='Bereich: '+f;
   rete:=untergeordnete_firma_bez+' '+f;
    drechnung:=strtodate(maskedit_redat.Edittext);
	neuer_datensatz(datamodul.q_repositionen,['nummer','i_firma','renu','datum','db','dbnr', 're_text','faktor','cent','brutto','i_rechnung','i_link_rechnung','farbe'],
												  [null,i_firma,renu,drechnung,0,1,rete,1,cent,s_betrag,renr_parent,renr_child,2]);

   datamodul.q_repositionen.Post;
end;


//####################################################################
function tform_rechnung.rechnung_erstellen(i_firma,parent,hr:integer):int64;


         function indieser_rechnung_vorhanden(db:integer;i_string:string;dbnr,i_mitarbeiter:int64;datum:tdate;ust:integer):boolean;

         begin
         with datamodul do
         begin
             //selbe befund noch nicht abgerechnet   db=3
             if dbnr>-1 then
             begin
             result:=q_repositionen.Locate('i_mitarbeiter,datum,dbnr,db', //statt dbnr i_string dann alles
                        //vararrayof([inttostr(i_mitarbeiter),datum,inttostr(dbnr),db]),[loCaseInsensitive])
                   vararrayof([(i_mitarbeiter),datum,(dbnr),db]),[loCaseInsensitive])
             end
             else
             result:=q_repositionen.Locate('i_mitarbeiter,datum,db',
                        //vararrayof([inttostr(i_mitarbeiter),datum,db]),[loCaseInsensitive]);
                   vararrayof([(i_mitarbeiter),datum,db]),[loCaseInsensitive]);


           if  result then   // k�mmt vor
           begin
              if checkbox_k_ust_sticht.Checked then
              begin
                q_repositionen.edit;
                q_repositionen.findfield('ust').asinteger:=min( q_repositionen.findfield('ust').asinteger, ust);  // keine ust gewinnt
                q_repositionen.Post;
              end
              else
              begin
                q_repositionen.edit;
                q_repositionen.findfield('ust').asinteger:=max( q_repositionen.findfield('ust').asinteger, ust);   //ust gewinnt
                q_repositionen.Post;
              end;
           end;

           end; //datamodul
         end;


         function inanderer_rechnung_vorhanden(db:integer;i_string:string;dbnr,i_mitarbeiter:int64;datum:tdate):boolean;
         var
          query:string;                                                                  //dauerloesch=0 normal
         begin                                                                           //dauerloesch=1 taucht nie mehr auf
         with datamodul do                                                               //dauerloesch=2 repo  zur neuvergabe freigeben (ohne strono)
         begin                                                                            // rechnung_storno=1 -->s.o.
             //selbe befund noch nicht abgerechnet   db=3
             if dbnr>-1 then
             begin
                query:= format('select nummer from re_positionen where i_mitarbeiter=%s and datum=%s and db=%d and dbnr=%s and i_rechnung>0 and (((storno=0) and (dauerloesch=0)) or ((storno=1) and (dauerloesch=1)) )',[inttostr(i_mitarbeiter),sql_datetostr(datum),db,inttostr(dbnr)]);
                result:=mysql_d.sql_exe_rowsvorhanden(query);
             end
             else
             begin
               query:= format('select nummer from re_positionen where i_mitarbeiter=%s and datum=%s and db=%d  and (((storno=0) and (dauerloesch=0)) or ((storno=1) and (dauerloesch=1)) )',[inttostr(i_mitarbeiter),sql_datetostr(datum),db]);
               result:=mysql_d.sql_exe_rowsvorhanden(query);
             end;
          end;
         end;



         function in_anderer_rechnung_firma_vorhanden(i_firma,db:integer;retext:string;sdatum:string):boolean;
         var
          query:string;
         begin
         with datamodul do
         begin
             //selbe befund noch nicht abgerechnet   db=3
               query:= format('select nummer from re_positionen where i_firma=%s and datum="%s" and db=%d and re_text="%s" and (((storno=0) and (dauerloesch=0)) or ((storno=1) and (dauerloesch=1)) )',[inttostr(i_firma),dat_delphi_to_sql(sdatum),db,retext]);
               result:=mysql_d.sql_exe_rowsvorhanden(query);
         end;
         end;


         function goae_ausschluss(nr,goae_nn:string;i_mitarbeiter:int64;datum:tdate):boolean;
         var
         position:integer;
         ss,query:string;
         begin
            //f�r go�e ausschluss  in dieser und in anderen Rechnungen
            result:=false;
            if goae_nn='' then exit;
            query:=format('select nummer from re_positionen where nummer<>%s and i_mitarbeiter=%s and datum="%s" and goae in (%s) ',
                           [nr,inttostr(i_mitarbeiter),dat_delphi_to_sql(datetostr(datum)),goae_nn] );
            result:= mysql_d.sql_exe_rowsvorhanden(query);
         end;
///
////////////////////////////////////main

var
	  betrag,brutto,betrag_faktor,summe,betrag_o_ust,betrag_ust,km_betrag_n, km_betrag_b:int64;
      s_betrag,s_brutto,s_betrag_faktor,s_summe,status_abrechnen:string;
      ust_pausch_anteil,ust,rf,i_ust:integer;
	  query,rete,sbeginn,sende,sstunden,sminuten,sdatum,s1,s2,s3,goae,goae_nn,ma_filter,ust_filter1,ust_filter2,smemo,i_string,s_u,sh,sm,sortierung:string;
	  db,i1,monate,punkte,goae_typ,i_vorgabe:integer;
	  dbnr,datensatznr,i_mitarbeiter:int64;
	  pvon,pbis,diff,datum,dvon,drechnung:tdate;
      drek:tdatetime;
	  year1, month1,day1,year2, month2,day2,j,m,d:word;
	  faktor,euro,f1,t:real;
begin
with datamodul do
try
	 ds_repositionen.Enabled:=false;
	 //renu:=trim(edit_renu.text);
	 svon:=dat_delphi_to_sql(maskedit_von.Edittext);
	 sbis:=dat_delphi_to_sql(maskedit_bis.Edittext);
	 pvon:=strtodate(maskedit_von.Edittext);
	 pbis:=strtodate(maskedit_bis.Edittext);
   drechnung:=strtodate(maskedit_redat.Edittext);
	 dvon:= pvon;
   if checkbox_offen_auch.Checked then status_abrechnen:='3,4' else status_abrechnen:='4';
     //dbis:= pbis;
	 //i_firma:=q_firma['nummer'];
	 i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');
	 if b_f_re then
		ma_filter:='mitarbeiter.i_firma='+inttostr(i_firma)
	 else
		ma_filter:='mitarbeiter.nummer='+inttostr(i_mitarbeiter);

	 rf:=0;
    
   if combobox_ust.ItemIndex=-1 then combobox_ust.ItemIndex:=0;

   if combobox_ust.ItemIndex=0 then ust_filter1:='.ust>=0 ';
   if combobox_ust.ItemIndex=1 then ust_filter1:='.ust=1 ';
   if combobox_ust.ItemIndex=2 then ust_filter1:='.ust=0 ';

   query:=format('select * from firma where nummer=%d',[i_firma]);
   sql_new(true, q_4,query,''); //firma

	// pauschalabrechnung
	if checkbox_pauschal.Checked and (q_4['pauschal_zeit']<>0) then
	begin
		{//alle bisherigen Zeit_pauschalen wegen �berschneidungen
		query:=format('select pvon,pbis from re_positionen where i_firma=%d and db=1 and (pbis>"%s" or pvon<"%s") and storno=0 order by pvon  ',[i_firma,svon,sbis]);
		sql_new(false,q_2,query,''); // alle positionen f�r die Firma
		q_2.first;
		while not q_2.eof do
		begin
      //
		 if pvon<q_2['pbis'] then pvon:=q_2['pbis']+1;
		 if pbis<q_2['pvon'] then pbis:=q_2['pvon']-1;
		 q_2.next;
		end;}

        form_memo.Memo.Lines.Add('     Pauschalrechnung');

      decodedate(pvon, year1, month1,day1);
      decodedate(pbis, year2, month2,day2);
      m:=0;
      drek:=pvon;
      while drek<=pbis do
      begin
           drek:=incmonth(drek,1);
           inc(m);
      end;

      drek:=incmonth(drek,-1);
      if m>0 then
      	if (pbis-drek)<16 then dec(m);     //mindestens 16 Tage dann wird monat gezogen

      monate:=m;
		//if day1>2 then monate:=-1;
		//if day2<27 then monate:=monate-1;
      //if day2-day1<0 then monate:=-1 else monate:=0;
      //monate:=monate+(year2-year1)*12+month2-month1;


          euro:=q_4['euro_pauschal'];
          betrag:=round(euro*cent_mult * monate /q_4['pauschal_zeit']);
         //ust:=q_firma['ust_pauschale']*mws;
         ust:=mws;
         ust_pausch_anteil:=q_4.findfield('ust_pausch_anteil').asinteger;

         betrag_ust:=round(betrag*ust_pausch_anteil/100);
         betrag_o_ust:=betrag-betrag_ust;


         //zuerst UST - anteil
         if (betrag_ust<>0) and (combobox_ust.ItemIndex<>2) then
         begin
          rete:=format('Pauschale: %s - %s (%s Monate) Anteil m. USt',[datetostr(pvon), datetostr(pbis),inttostr(monate)]);
         	brutto:= round(betrag_ust+betrag_ust*ust/100);

         	s_betrag:=inttostr(betrag_ust);

		 		neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','pvon','pbis','renu','db','dbnr', 're_text','cent','netto','faktor','goae_nn','brutto','ust','haupt_re'],
												  [null,rf,i_firma,0,pvon,pvon,pbis,renu,1,0,rete,s_betrag,floattostrf( betrag_ust/cent_mult,ffcurrency,15,2),1,'',floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
		 		inc(rf);
         end;

         if (betrag_o_ust<>0) and (combobox_ust.ItemIndex<>1) then
         begin
            rete:=format('Pauschale: %s - %s (%s Monate) Anteil o. USt',[datetostr(pvon), datetostr(pbis),inttostr(monate)]);
            ust:=0;
         	brutto:= betrag_o_ust;

         	s_betrag:=inttostr(betrag_o_ust);

		 		neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','pvon','pbis','renu','db','dbnr', 're_text','cent','netto','faktor','goae_nn','brutto','ust','haupt_re'],
												  [null,rf,i_firma,0,pvon,pvon,pbis,renu,1,0,rete,s_betrag,floattostrf( betrag_o_ust/cent_mult,ffcurrency,15,2),1,'',floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
		 		inc(rf);
         end;

	end;

	if b_f_re then
		query:=format('select i_mitarbeiter, db,dbnr,i_string,datum from re_positionen where i_firma=%d',[i_firma])
	 else query:=format('select i_mitarbeiter, db,dbnr,i_string,datum from re_positionen where i_mitarbeiter=%s',[inttostr(i_mitarbeiter)])	;
	sql_new(true,q_2,query,''); // alle positionen f�r die Firma ben�tigt in "in anderer rechnung vorhanden"

	// stundenweise
	//if checkbox_stunden.Checked then    sp�ter abfragen
	begin
	  query:=format('select * from firma_sollist where i_firma=%d and datum>="%s" and datum<="%s" and i_sollist=0 and storno=0 order by datum , ts',[i_firma,svon, sbis]);
	  sql_new(false,q_1,query,'');
	  //su_stunden:=0;
	  q_1.first;
	  while not q_1.Eof do
	  begin
		 if q_1['datum']<>null then sdatum:=datetostr(q_1['datum']) else sdatum:='';
		 if q_1['zeit_s']<>null then sbeginn:=timetostr(q_1['zeit_s']) else sbeginn:='';
		 if q_1['zeit_e']<>null then sende:=timetostr(q_1['zeit_e']) else sende:='';
		 if q_1['stunden']<>null then sstunden:=inttostr(q_1['stunden']) else sstunden:='';
		 if q_1['minuten']<>null then sminuten:=inttostr(q_1['minuten']) else sminuten:='';
      if q_1['memo']<>null then smemo:=q_1['minuten'] else smemo:='';
		 dbnr:=getbigint(q_1,'nummer');
		 //if not q_2.Locate('db,dbnr',vararrayof(['1',inttostr(dbnr)]),[loCaseInsensitive]) then
     if not q_2.Locate('db,dbnr',vararrayof(['1',(dbnr)]),[loCaseInsensitive]) then
		 begin
			//rete:=format('Zeit: %s %s - %s (%s St. %s Min.)',[sdatum,sbeginn,sende,sstunden,sminuten,smemo  ]);

				betrag:=round((q_4['euro_stunde']*cent_mult)*(q_1['stunden']+(q_1['minuten'] / 60)));
            //ust:=q_firma['ust_stunden']*mws;
            ust:=mws;
            ust_pausch_anteil:=q_4.findfield('ust_stunden_anteil').asinteger;

            i_vorgabe:=q_1.FindField('i_ust').AsInteger;
        		case i_vorgabe of    //umsatzsteuer in der zeiterfassung �berschreibt die vorgabe
               0:begin
                  betrag_ust:=round(betrag*ust_pausch_anteil/100);
                  betrag_o_ust:=betrag-betrag_ust;
                end;
               1:begin
                  betrag_ust:=betrag;
                  betrag_o_ust:=0;
                 end;
               2:begin
                  betrag_ust:=0;
                  betrag_o_ust:=betrag;
                 end;
             end;



          if checkbox_fahrstrecke.Checked then
          begin
            km_betrag_n:=q_4.findfield('km_cent').asinteger*q_1.findfield('km_gefahren').asinteger;
            if  q_4.findfield('ust_km').asinteger=1 then
            begin
            	km_betrag_b:=km_betrag_n+round(km_betrag_n*ust/100);
               ust:=mws;
            end
            else
            begin
            	km_betrag_b:=km_betrag_n;
               ust:=0;
            end;

            rete:='Fahrstrecke: '+q_1.findfield('km_gefahren').asstring +' km ('+sdatum+')';

            if (km_betrag_n>0) and ( (combobox_ust.ItemIndex=0) or((combobox_ust.ItemIndex=2) and (ust=0)) or ((combobox_ust.ItemIndex=1) and (ust>0) )) then
            begin

              if not in_anderer_rechnung_firma_vorhanden(i_firma,2,rete,sdatum) then
               neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','cent','netto','faktor','goae_nn','brutto','ust','haupt_re'],
                                 [null,rf,i_firma,0,sdatum,renu,2,inttostr(dbnr),rete,km_betrag_n,floattostrf( km_betrag_n/cent_mult,ffcurrency,15,2),1,'',floattostrf( km_betrag_b/cent_mult,ffcurrency,15,2),ust,hr]);
            end;
          end;




       if checkbox_stunden.Checked and (betrag_ust<>0) and (combobox_ust.ItemIndex<>2) then   //Rechnung mit USt
         begin
            ust:=mws;
            t:=q_1.findfield('stunden').asinteger*60+q_1.findfield('minuten').asinteger;

            if i_vorgabe= 0 then t:=t*ust_pausch_anteil/100; //korrektur nur bei vorgabe

            sstunden:=inttostr(trunc(t/60));
            sminuten:=inttostr(trunc(60*frac(t/60)));
            rete:=format('Zeit: %s %s - %s  %s St. %s Min. Anteil m. USt.',[sdatum,sbeginn,sende,sstunden,sminuten  ]);
         	brutto:= round(betrag_ust+betrag_ust*ust/100);

         	s_betrag:=inttostr(betrag_ust);

            //brutto:=betrag+betrag*ust div 100;
            //s_betrag:=inttostr(betrag);
         if not in_anderer_rechnung_firma_vorhanden(i_firma,2,rete,sdatum) then
			      neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','cent','netto','faktor','goae_nn','brutto','ust','haupt_re'],
										[null,rf,i_firma,0,sdatum,renu,2,inttostr(dbnr),rete,s_betrag,floattostrf( betrag_ust/cent_mult,ffcurrency,15,2),1,'',floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
			inc(rf);
			//setbigint(q_repositionen,'dbnr',dbnr);
         end;


       if checkbox_stunden.Checked and (betrag_o_ust<>0) and (combobox_ust.ItemIndex<>1)  then    //position ohne ust
         begin
            t:=q_1.findfield('stunden').asinteger*60+q_1.findfield('minuten').asinteger;

            if i_vorgabe= 0 then t:=t*(100-ust_pausch_anteil)/100;  //korrektur nur bei vorgabe

            sstunden:=inttostr(trunc(t/60));
            sminuten:=inttostr(trunc(60*frac(t/60)));
            rete:=format('Zeit: %s %s - %s  %s St. %s Min. Anteil o. USt',[sdatum,sbeginn,sende,sstunden,sminuten  ]);
            ust:=0;
         	brutto:= round(betrag_o_ust+(betrag_o_ust*ust/100));

         	s_betrag:=inttostr(betrag_o_ust);

            //brutto:=betrag+betrag*ust div 100;
            //s_betrag:=inttostr(betrag);
            if not in_anderer_rechnung_firma_vorhanden(i_firma,2,rete,sdatum) then
			        neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','cent','netto','faktor','goae_nn','brutto','ust','haupt_re'],
										[null,rf,i_firma,0,sdatum,renu,2,inttostr(dbnr),rete,s_betrag,floattostrf( betrag_o_ust/cent_mult,ffcurrency,15,2),1,'',floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
			inc(rf);
			//setbigint(q_repositionen,'dbnr',dbnr);
         end;

		 end;
		 q_1.next;
	  end;
	 end;
	  //vorsorge  (besteht aus mehreren Bausteinen (befunde , labor) GO�)

	 if checkbox_unt.Checked then
	 begin
     form_memo.Memo.Lines.Add('     Untersuchungen');
     if checkbox_k_ust_sticht.checked then
      sortierung:='datum, ust'
     else
      sortierung:= 'datum, ust desc' ;
     ust_filter2:='untersuchung'+ust_filter1;
	  query:='select mitarbeiter.name as mn, mitarbeiter.vorname as mv, mitarbeiter.geb_dat as md, untersuchung.nummer, untersuchung.euro,untersuchung.i_mitarbeiter, untersuchung.datum ,typ.untersuchung,typ.nummer as i_typ, untersuchung.ust  from mitarbeiter '+'left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join untersuchung  on (untersuchung.i_mitarbeiter = mitarbeiter.nummer)';
    query:=query+' left outer join typ on (untersuchung.i_typ=typ.nummer)';
	  query:=query+format('  where ( untersuchung.datum >= "%s" ) and ( untersuchung.datum <= "%s") and (untersuchung.i_status in (%s) ) and (untersuchung.storno=0 )and (untersuchung.abrechnen=1)  and (%s ) and %s order by %s',[svon,sbis,status_abrechnen,ma_filter,ust_filter2,sortierung]);
    // nach datum , ust sortiert dann kommt zuert ohne ust und dann mit ust da ohne ust ust schl�gt
	  sql_new(false,q_1,query,'');
	  q_1.first;
	  while not q_1.Eof do
	  begin
		 sdatum:=q_1['datum'];
		 dbnr:=getbigint(q_1,'nummer');

		 i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');
		 datum:=q_1['datum'];


		 begin
			if q_1['mn']=null then s1:='' else s1:=q_1['mn'];
			if q_1['mv']=null then s2:='' else s2:=q_1['mv'];
			if q_1['md']=null then s3:='' else s3:=datetostr(q_1['md']);

      faktor:=q_4.findfield('fa_bef').asfloat ;
			rete:=q_1['untersuchung'];
			betrag:=round(q_1['euro']*cent_mult);
         s_betrag:=inttostr(betrag);
         ust:=q_1['ust']*mws;
         //brutto:=betrag+betrag*ust div 100;
         //brutto:=round((betrag+ (betrag*ust/100))*faktor);
         brutto:=round(round(betrag*faktor)*(100+ust)/100);
			//vorsorgeuntersuchung   immer auff�hren auch wenn betrag 0
         if betrag=0 then
         begin
         	faktor:=0;
          ust:=0;
         end;
			if not inanderer_rechnung_vorhanden(3,i_string,dbnr,i_mitarbeiter,datum) then
			if  not indieser_rechnung_vorhanden(3,i_string,dbnr,i_mitarbeiter,datum,ust) then
			begin
			  neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','faktor','cent','netto','brutto','ust','haupt_re'],
									  [null,rf,i_firma,inttostr(i_mitarbeiter),datum,renu,3,inttostr(dbnr),rete,faktor, s_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
			  inc(rf);
			end;
         //dazugeh�rende Befunde mehrwertsteuer dann dabei
         if betrag=0 then
         begin
            ust:=q_1['ust']*mws;
            query:='select unt_befunde.nummer,bereich.nummer as bernu, bereich.name, unt_befunde.storno,bereich.euro as beuro, goae.goae_nummer as goae_nr, goae.goae_text, goae.punkte, goae.goae_nn, goae.goae_typ from unt_befunde '
                  +' left join bereich on(unt_befunde.i_befunde=bereich.nummer ) left join goae on (goae.goae_nummer=bereich.goae)  where unt_befunde.storno=0 and  (bereich.euro<>0 or bereich.goae<>"" ) and unt_befunde.abrechnen=1 and unt_befunde.i_typ= '+inttostr(q_1['i_typ']);
            sql_new(false,q_3,query,'');//befund-bausteine

            q_3.first;
            while not q_3.eof do
            begin
               rete:=q_3['name'];
               if q_3['goae_nr']=null then goae:='' else goae:=q_3['goae_nr'];
               if q_3['goae_nn']=null then goae_nn:='' else goae_nn:=q_3['goae_nn'];
               if q_3['goae_typ']=null then goae_typ:=0 else goae_typ:=q_3['goae_typ'];
               if q_3['punkte']=null then punkte:=0 else punkte:=q_3['punkte'];
               faktor:=get_faktor(datamodul.q_4,goae_typ);

               dbnr:=getbigint(q_3,'bernu');
               datensatznr:=getbigint(q_3,'nummer');
               i_string:=inttostr(dbnr)+'/'+inttostr(datensatznr);
               euro:=q_3['beuro'];
               if euro=0 then
                  betrag:=round(punkte*cent_mult*g_punktwert)     //evtl trunc
               else
                  begin  //Abrechnen nach EUro
                     betrag:=round( euro*cent_mult);
                     goae:='';
                     faktor:=q_4.findfield('fa_bef').asfloat;  //faktor f�r eurobetr�ge
               end;
                    
                //brutto:=round((betrag+ (betrag*ust/100))*faktor);
                brutto:=round(round(betrag*faktor)*(100+ust)/100);
                s_betrag:=inttostr(betrag);
               //einzel-befunde, die zur standarduntersuchung geh�ren

               if not inanderer_rechnung_vorhanden(4,i_string,dbnr,i_mitarbeiter,datum) then    //statt i_str dbnr
                     if  not indieser_rechnung_vorhanden(4,i_string,dbnr,i_mitarbeiter,datum,ust) then  //statt i_str dbnr
                     neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr','i_string','goae','goae_nn','faktor', 're_text','cent','netto','brutto','ust','haupt_re'],
                                                [null,rf,i_firma,inttostr(i_mitarbeiter), datum,renu,4,inttostr(dbnr),i_string,goae,goae_nn,faktor,rete,s_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
                  inc(rf);
               q_3.next;
            end;
            //labor, das zur standarduntersuchung geh�rt

               //mehrwertsteuer entsprechend der Untersuchung
            query:='select unt_labor.nummer,typ.nummer as labnu,typ.euro as teuro, goae.goae_nummer as goae_nr,goae.goae_nn,goae.goae_typ,goae.punkte, typ.untersuchung ,unt_labor.storno from unt_labor '+
                   ' left join typ on(unt_labor.i_typ=typ.nummer ) left join goae on (goae.goae_nummer=typ.goae) where unt_labor.storno=0 and (typ.euro>0 or typ.goae<>"" ) and unt_labor.abrechnen=1 and unt_labor.ii_typ='+inttostr(q_1['i_typ']);
            sql_new(false,q_3,query,'');

            q_3.first;
            while not q_3.eof do
            begin
               rete:=q_3['untersuchung'];
               if q_3['goae_nr']=null then goae:='' else goae:=q_3['goae_nr'];
               if q_3['goae_nn']=null then goae_nn:='' else goae_nn:=q_3['goae_nn'];
               if q_3['goae_typ']=null then goae_typ:=0 else goae_typ:=q_3['goae_typ'];
               if q_3['punkte']=null then punkte:=0 else punkte:=q_3['punkte'];
               faktor:=get_faktor(datamodul.q_4,goae_typ);
               dbnr:=getbigint(q_3,'labnu');
               euro:=q_3['teuro'];

               if euro=0 then betrag:=round(punkte*cent_mult*g_punktwert)
               else
               begin  //euro
                   betrag:=round(euro*cent_mult);
                   goae:='';
                   faktor:=q_4.findfield('fa_bef').asfloat;  //faktor f�r eurobetr�ge
               end;
                  
               //brutto:=round((betrag+ (betrag*ust/100))*faktor);
               brutto:=round(round(betrag*faktor)*(100+ust)/100);
               s_betrag:=inttostr(betrag);
               //labor
               if not inanderer_rechnung_vorhanden(6,i_string,dbnr,i_mitarbeiter,datum) then
               if  not indieser_rechnung_vorhanden(6,i_string,dbnr,i_mitarbeiter,datum,ust) then
                    begin
                     neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr','goae','goae_nn','faktor', 're_text','cent','netto','brutto','ust','haupt_re'],
                                                [null,rf,i_firma,inttostr(i_mitarbeiter), datum,renu,6,inttostr(dbnr),goae,goae_nn,faktor,rete,s_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
                    inc(rf);
                    end;
               q_3.next;
            end;
        end; //if betrag=0

		 end;
		 q_1.next;
	  end;
	end;
	//einzelposten

	  //befunde
	 if checkbox_bef.checked then
	 begin
       form_memo.Memo.Lines.Add('     Einzelposten (Befunde)');

     ust_filter2:='akt_untersuchung'+ust_filter1;
	  query:='select mitarbeiter.nummer as i_mitarbeiter, befunde.nummer, befunde.datum,bereich.nummer as bernu, bereich.name, goae.goae_nummer as goae_nr, goae.goae_nn,goae.goae_typ,goae.punkte,akt_untersuchung.faktor, '+ 'akt_untersuchung.euro, akt_untersuchung.abrechnen ' +
				 ', akt_untersuchung.ust as ust from mitarbeiter left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join befunde on (befunde.i_mitarbeiter = mitarbeiter.nummer) ';
	  query:=query+' left outer join akt_untersuchung on (akt_untersuchung.i_untersuchung = befunde.nummer) left outer join bereich on (bereich.nummer = akt_untersuchung.i_bereich) left outer join goae on (goae.goae_nummer = akt_untersuchung.goae) ';
	  if checkbox_bef.checked then
		  query:=query+format(' where  ( ( befunde.datum >= "%s" ) and ( befunde.datum <= "%s") and (befunde.storno=0 )  ) and (akt_untersuchung.euro<>0 or akt_untersuchung.goae<>"") and (akt_untersuchung.abrechnen=1) and (%s) and %s',[svon,sbis,ma_filter,ust_filter2])
	  else //jetzt muss ext_leistung angekreuzt sein
		  query:=query+format(' where ( ( befunde.datum >= "%s" ) and ( befunde.datum <= "%s") and (befunde.storno=0 )) and bereich.ext_leistung=1 and (akt_untersuchung.euro<>0 or akt_untersuchung.goae<>"") and (akt_untersuchung.abrechnen=1) and (%s) and %s ',[svon,sbis,ma_filter,ust_filter2]);

	  sql_new(false,q_1,query,'datum');
	  //su_befunde:=0;
	  q_1.first;
	  while not q_1.Eof do
	  begin
		 datum:=(q_1['datum']);
		 dbnr:=getbigint(q_1,'bernu');
		 rete:=q_1.findfield('name').asstring;
       ust:= q_1.findfield('ust').asinteger*mws;

		 if q_1['goae_nr']=null then goae:='' else goae:=q_1['goae_nr'];
		 if q_1['goae_nn']=null then goae_nn:='' else goae_nn:=q_1['goae_nn'];
		 if q_1['goae_typ']=null then goae_typ:=0 else goae_typ:=q_1['goae_typ'];
		 if q_1['punkte']=null then punkte:=0 else punkte:=q_1['punkte'];
     faktor:=get_faktor(datamodul.q_4,goae_typ);
		 euro:=q_1.findfield('euro').asfloat;
		 i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');
		 if euro=0 then
         	 betrag:=round(punkte*cent_mult*g_punktwert)
     else
     begin
          betrag:=round(euro*cent_mult);
          goae:='';
          faktor:=q_4.findfield('fa_bef').asfloat;    //faktor f�r eurobetr�ge
     end;

      brutto:=round(round(betrag*faktor)*(100+ust)/100);
     s_betrag:=inttostr(betrag);
			//wenn der einezelbefund noch nicht abgerechnet ist (vorsorge)
		 if  not inanderer_rechnung_vorhanden(4,i_string,dbnr,i_mitarbeiter,datum) then
				if  not indieser_rechnung_vorhanden(4,i_string,dbnr,i_mitarbeiter,datum,ust) then
				  begin
					  if (q_1.FindField('abrechnen').asinteger=1) and (betrag<>0) then
					  neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr','goae','goae_nn','faktor', 're_text','cent','netto','brutto','ust','haupt_re'],
																				[null,rf,i_firma,inttostr(i_mitarbeiter), datum,renu,4,inttostr(dbnr),goae,goae_nn,faktor,rete,s_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);

				  end;

			  inc(rf);
		  q_1.next;
	  end;
	 end;


	  //impfung
	if checkbox_impfung.Checked then
	begin
    form_memo.Memo.Lines.Add('     Impfungen');

     ust_filter2:='impfung'+ust_filter1;
	  query:='select mitarbeiter.nummer as i_mitarbeiter, typ.nummer, impfung.datum, typ.untersuchung ,impfstoff.impfstoff, goae.goae_nummer as goae_nr, goae.goae_typ,goae.goae_nn, goae.punkte , impfung.faktor, '+'impfung.euro ,impfung.abrechnen, impfstoff.euro as is_euro '+
			',impfstoff.i_user as ust_istoff ,impfung.ust as ust from mitarbeiter  left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join impfung on (impfung.i_mitarbeiter = mitarbeiter.nummer)';
	  query:=query+' left outer join typ on (impfung.i_typ = typ.nummer) left outer join impfstoff on (impfung.i_stoff =impfstoff.nummer) left outer join goae on (impfung.goae=goae.goae_nummer) ';
	  query:=query+format(' where  ( ( impfung.datum >= "%s" ) and ( impfung.datum <= "%s") and (impfung.storno=0 )) and (impfung.i_status in (%s) ) and(impfung.euro<>0 or impfung.goae<>"") and (impfung.abrechnen=1) and (%s) and %s',[svon,sbis,status_abrechnen,ma_filter,ust_filter2]);
	  sql_new(false,q_1,query,'datum');
	  //su_impfung:=0;
	  q_1.first;
	  while not q_1.Eof do
	  begin
		 datum:=q_1['datum'];


		 dbnr:=getbigint(q_1,'nummer');
		 i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');

     ust:=q_1['ust']*mws;
		 begin
			rete:='Impfung: '+q_1['untersuchung'];
			if q_1['goae_nr']=null then goae:='' else goae:=q_1['goae_nr'];
			if q_1['goae_nn']=null then goae_nn:='' else goae_nn:=q_1['goae_nn'];
			if q_1['goae_typ']=null then goae_typ:=0 else goae_typ:=q_1['goae_typ'];
			if q_1['punkte']=null then punkte:=0 else punkte:=q_1['punkte'];
      faktor:=get_faktor(datamodul.q_4,goae_typ);

			//faktor:=q_4.findfield('fa_impf').asfloat;     15/06/25

			euro:=q_1.findfield('euro').asfloat;
			if euro=0 then  betrag:=round(punkte*cent_mult*g_punktwert)
      else
      begin
					betrag:=round(euro*cent_mult);
          goae:='';
          faktor:=q_4.findfield('fa_bef').asfloat; //faktor f�r eurobetr�ge
      end;
            	
      brutto:=round(round(betrag*faktor)*(100+ust)/100);
      s_betrag:=inttostr(betrag);
			//impfung
			if not inanderer_rechnung_vorhanden(5,i_string,dbnr,i_mitarbeiter,datum) then
			if not indieser_rechnung_vorhanden(5,i_string,dbnr,i_mitarbeiter,datum,ust) then  // -1 statt dbnr# impfung darf 1mal am Tag abgerechnet werden
			if q_1['abrechnen']=1 then
			begin
			  dbnr:=getbigint(q_1,'nummer');
			  neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','goae','goae_nn','faktor','cent','netto','brutto','ust','haupt_re'],
													  [null,rf,i_firma,inttostr(i_mitarbeiter),datum,renu,5,inttostr(dbnr),rete,goae,goae_nn,faktor,s_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
			  inc(rf);
			end;

			//impfstoff

			if q_1['impfstoff']=null then s1:='' else s1:= q_1['impfstoff'];
			if q_1['is_euro']=null then f1:=0 else f1:= q_1['is_euro'];
			rete:='Impfstoff: '+s1;
      //ust:=q_1['ust']*mws;  //#######################hier muss die ust aus ust_impfstoff stehen typ{ TODO 1 : Impfung UST f�r Impfstoff einstellbar machen } ->impfung ->rechnung
       i_ust:=q_1.findfield('ust_istoff').asinteger;
       case i_ust of
       0:;  //wie oben
       1:ust:=0;
       2:ust:=mws;
       end;

			betrag:=round(f1*cent_mult);
      //betrag:=betrag*trunc(faktor*100000); kein Faktor beim impfstoff
      brutto:=round(betrag+betrag*ust / 100);
			goae:='';
			goae_nn:='';
			faktor:=1;   //faktor 1 f�r impfstoff
			datum:=q_1['datum'];
			//impfstoff
      s_betrag:=inttostr(betrag);
			if  not inanderer_rechnung_vorhanden(7,i_string,dbnr,i_mitarbeiter,datum) then
       begin
         //impfstoff
         neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','goae','goae_nn','faktor','cent','netto','brutto','ust','haupt_re'],
                             [null,rf,i_firma,inttostr(i_mitarbeiter),datum,renu,7,inttostr(dbnr),rete,goae,goae_nn,faktor,S_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
         inc(rf);
         setbigint(q_repositionen,'dbnr',dbnr);
       end;
		 end;
		 q_1.next;
	  end;
	end;


	  //labor
	if checkbox_labor.Checked then
	begin
      form_memo.Memo.Lines.Add('     Labor');

     ust_filter2:='labor'+ust_filter1;
	  query:='select mitarbeiter.nummer as i_mitarbeiter, typ.nummer, labor.datum , typ.untersuchung,goae.goae_nummer as goae_nr, goae.punkte, goae.goae_nn,goae.goae_typ ,labor.faktor, labor.euro ,labor.ust as ust'+
				' from mitarbeiter  left outer join firma on (mitarbeiter.i_firma = firma.nummer) left outer join labor on (labor.i_mitarbeiter = mitarbeiter.nummer) left outer join goae on (goae.goae_nummer=labor.goae) ';
	  query:=query+format(' left outer join typ on (labor.i_typ = typ.nummer)  where ((labor.datum >= "%s" ) and ( labor.datum <= "%s") and (labor.storno=0 )) and (labor.i_status in (%s) ) and (labor.euro>0 or labor.goae<>"") and (%s) and %s',[svon,sbis,status_abrechnen,ma_filter,ust_filter2]);
      query:=query+ ' and (labor.abrechnen=1)  ';

	  sql_new(false,q_1,query,'datum');
	  //su_labor:=0;
	  q_1.first;
	  while not q_1.Eof do
	  begin
		 datum:=q_1['datum'];
		 dbnr:=getbigint(q_1,'nummer');
		 i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');
         ust:= q_1['ust']*mws;
		 if  not inanderer_rechnung_vorhanden(6,i_string,dbnr,i_mitarbeiter,datum) then
			 if not indieser_rechnung_vorhanden(6,i_string,dbnr,i_mitarbeiter,datum,ust) then //# labor darf nur ein mal am Tag abgerechnet werden
		 begin
			rete:=q_1['untersuchung'];
			if q_1['goae_nr']=null then goae:='' else goae:=q_1['goae_nr'];
			if q_1['goae_nn']=null then goae_nn:='' else goae_nn:=q_1['goae_nn'];
			if q_1['goae_typ']=null then goae_typ:=0 else goae_typ:=q_1['goae_typ'];
			if q_1['punkte']=null then punkte:=0 else punkte:=q_1['punkte'];
			faktor:=q_4.findfield('fa_lab').asfloat;
			euro:=q_1.findfield('euro').AsFloat;
			if euro=0 then 	betrag:=round(punkte*cent_mult*g_punktwert)
      else
      begin
					betrag:=round(euro*cent_mult);
					goae:='';
          faktor:=q_4.findfield('fa_bef').asfloat;    //faktor f�r eurobetr�ge
			end;

      brutto:=round(round(betrag*faktor)*(100+ust)/100);
      s_betrag:=inttostr(betrag);
			dbnr:=getbigint(q_1,'nummer');
      //labor
			neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','goae','goae_nn','faktor','cent','netto','punkte','brutto','ust','haupt_re'],
													[null,rf,i_firma,inttostr(i_mitarbeiter),datum,renu,6,inttostr(dbnr),rete,goae,goae_nn,faktor,s_betrag,floattostrf( betrag/cent_mult,ffcurrency,15,2),punkte,floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
			inc(rf);
			setbigint(q_repositionen,'dbnr',dbnr);
		 end;
		 q_1.next;
	  end;
	end;

	if checkbox_artikel.Checked then     //nicht gepflegt
	begin
      form_memo.Memo.Lines.Add('     Artikel');

	  query:='select mitarbeiter.nummer as i_mitarbeiter,artikel.*  from mitarbeiter left join artikel on (artikel.i_mitarbeiter = mitarbeiter.nummer)  ';
	  query:=query+format(' where ((artikel.datum >= "%s" ) and ( artikel.datum <= "%s") and (artikel.storno=0 )) and (%s)',[svon,sbis,ma_filter]);
	  sql_new(false,q_1,query,'datum');
	  //su_labor:=0;
	  q_1.first;
	  while not q_1.Eof do
	  begin
		 datum:=q_1['datum'];
		 dbnr:=getbigint(q_1,'nummer');
		 i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');
		 rete:=q_1['art_name'];
       ust:=q_1['ust']*mws;
		 if  not inanderer_rechnung_vorhanden(8,i_string,dbnr,i_mitarbeiter,datum) then
     if not indieser_rechnung_vorhanden(8,i_string,dbnr,i_mitarbeiter,datum,ust) then //
		 begin
			faktor:=1;//q_4['fa_lab']; immer 1
			if q_1['netto_m_rabatt']<>null then euro:=q_1['netto_m_rabatt'] else euro:=0;;
			betrag:=round(euro*cent_mult);

      brutto:=round(betrag*(100+ust)/100);
      s_betrag:=inttostr(betrag);
			dbnr:=getbigint(q_1,'nummer');

			neuer_datensatz(q_repositionen,['nummer','rf','i_firma','i_mitarbeiter','datum','renu','db','dbnr', 're_text','faktor','cent','netto','brutto','ust','haupt_re'],
													[null,rf,i_firma,inttostr(i_mitarbeiter),datum,renu,8,inttostr(dbnr),rete,faktor,s_betrag,floattostrf( euro,ffcurrency,15,2),floattostrf( brutto/cent_mult,ffcurrency,15,2),ust,hr]);
			inc(rf);
			setbigint(q_repositionen,'dbnr',dbnr);
		 end;
		 q_1.next;
	  end;
	end;

	if q_repositionen.State in [dsedit, dsinsert] then q_repositionen.post;

finally
ds_repositionen.Enabled:=true;
end;
end;



procedure TForm_rechnung.FormCreate(Sender: TObject);
var i,j:integer;
f,query:string;
begin
with datamodul
do begin
	form_main.form_positionieren(tform(sender));
	//sql_new(false,datamodul.q_5,'select * from haupt_tabelle','');
  firma_nummer:=datamodul.q_firma.findfield('nummer').asinteger;
   datamodul.q_main.Refresh;
	ini(firma_nummer);
end;
end;


procedure tForm_rechnung.ini(f_nummer:integer);
var i,j:integer;
f,query:string;
begin
  firma_nummer:=f_nummer;
  edit_renu.Text:=datamodul.q_main.findfield('renu_kopf').asstring;
  query:=format('select * from firma where nummer =%d',[f_nummer]);
  datamodul.sql_new(false,datamodul.q_13,query,'');

  label_firma.Caption:=datamodul.q_13.findfield('firma').AsString;

	checkbox_pauschal.Checked:=(datamodul.q_13['pauschal_zeit']<>0);
	checkbox_stunden.Checked:=(datamodul.q_13['euro_stunde']<>0);
	checkbox_bef.checked:=(datamodul.q_13['ar_bef']=1);
	checkbox_unt.checked:=(datamodul.q_13['ar_unt']=1);
	checkbox_impfung.checked:=(datamodul.q_13['ar_impf']=1);
	checkbox_labor.checked:=(datamodul.q_13['ar_lab']=1);
   checkbox_fahrstrecke.Checked:=(datamodul.q_13['km_cent']>0) and checkbox_stunden.Checked;
	//checkbox_ext.checked:=(datamodul.q_firma['ar_ext']=1);
   // checkbox_artikel.checked:=(datamodul.q_firma['ar_artikel']=1);
   // checkbox_artikel.Visible:=form_main.m_artikel.Visible;
	//if not b_f_re then checkbox_betrzuordnen.Checked:=false;
   liste:=untergeordnete_firmen(f_nummer);
   checklistbox_firmen.Clear;
   for i:=0 to liste.Count-1 do
   begin
      j:=integer(liste[i]);
      f:=firmenname(j);
      //firmen:=firmen+
      checklistbox_firmen.AddItem( f,pointer(j));

   end;
   BitBtn_f_tauschenClick(self);
   //memo_firmen.Text:=firmen;
   maskedit_redat.text:=datetostr(date);
   zeiteinstellen;
   liste.Free;

end;

procedure tForm_rechnung.ini_rechnungslauf(regel_nummer:integer);
var i,j,f_nummer:integer;
f,query:string;
begin

  edit_renu.Text:=datamodul.q_main.findfield('renu_kopf').asstring;
  query:=format('select firma.firma, rechnung_regeln.* from rechnung_regeln left join firma on (rechnung_regeln.i_firma=firma.nummer) where rechnung_regeln.nummer =%d',[regel_nummer]);
  datamodul.sql_new(false,datamodul.q_13,query,'');


  f_nummer:=datamodul.q_13.findfield('i_firma').Asinteger;
  firma_nummer:=f_nummer;
  label_firma.Caption:=datamodul.q_13.findfield('firma').AsString;

	checkbox_pauschal.Checked:=(datamodul.q_13['b_pauschal']=1);
	checkbox_stunden.Checked:=(datamodul.q_13['b_stundenweise']=1);
	checkbox_bef.checked:=(datamodul.q_13['b_befunde']=1);
	checkbox_unt.checked:=(datamodul.q_13['b_vorsorge']=1);
	checkbox_impfung.checked:=(datamodul.q_13['b_impfung']=1);
	checkbox_labor.checked:=(datamodul.q_13['b_labor']=1);
  checkbox_fahrstrecke.Checked:=(datamodul.q_13['b_fahrstrecke']=1) ;
  checkbox_artikel.checked:=(datamodul.q_13['b_artikel']=1);
  CheckBox_betrzuordnen.Checked:=(datamodul.q_13['b_anonym']=1);
  combobox_ust.ItemIndex:=datamodul.q_13.findfield('ust').asinteger-1;
	//if not b_f_re then checkbox_betrzuordnen.Checked:=false;
   liste:=untergeordnete_firmen(f_nummer);
   checklistbox_firmen.Clear;
   for i:=0 to liste.Count-1 do
   begin
      j:=integer(liste[i]);
      f:=firmenname(j);
      //firmen:=firmen+
      checklistbox_firmen.AddItem( f,pointer(j));

   end;
   BitBtn_f_tauschenClick(self);
   //memo_firmen.Text:=firmen;
   maskedit_redat.text:=datetostr(date);
   zeiteinstellen;
   liste.Free;

end;

function tform_rechnung.faktor_kontrolle(faktor:real;goae_typ:integer):real;
begin
	case goae_typ of
    0: result:=faktor;
    1: result:=min(faktor,1.8);
    2: result:=min(faktor,1.15);
    3: result:=1;
	end;
end;


function tform_rechnung.get_faktor(q:tzquery;goae_typ:integer):real;
begin
	case goae_typ of
    0: result:=q.findfield('fa_unt').asfloat;
    1: result:=q.findfield('fa_impf').asfloat;
    2: result:=q.findfield('fa_lab').asfloat;
    3: result:=1;
	end;
end;


procedure tform_rechnung.verbote_raus(i_firma:integer);

function goae_ausschluss(nr,goae,goae_nn:string;i_mitarbeiter:int64;datum:tdate):boolean;
var
position:integer;
ss,query,goaek:string;
re:boolean;
begin
	//f�r go�e ausschluss in dieser und in anderen Rechnungen
	//Suche in re_positionen , da mit dem billigen begonnen wird werden diese zu Gunsten der teuren zuerst gel�scht aus q1 s.u
  //ohne MWS schl�gt MWS also sortieren billig, mit MWS,
	result:=false;
	if goae_nn<>'' then
    begin
	query:=format('select nummer from re_positionen where nummer<>%s and i_mitarbeiter=%s and datum="%s" and goae in (%s) ',
						[nr,inttostr(i_mitarbeiter),dat_delphi_to_sql(datetostr(datum)),goae_nn] );
    result:= mysql_d.sql_exe_rowsvorhanden(query);

    {query:=format('delete from re_positionen where nummer<>%s and i_mitarbeiter=%s and datum="%s" and goae in (%s) ',
						[nr,inttostr(i_mitarbeiter),dat_delphi_to_sql(datetostr(datum)),goae_nn] ); }
    // nicht l�schen, da dann nochmals abgerechnet werden kann
    if result then
    begin
    	query:=format('update re_positionen set cent="0", netto="0", brutto="0" where nummer<>%s and i_mitarbeiter=%s and datum="%s" and goae in (%s) ',
						[nr,inttostr(i_mitarbeiter),dat_delphi_to_sql(datetostr(datum)),goae_nn] );
    	mysql_d.sql_exe(query);
    end;
    end;
    if goae<>'' then
    begin
    goae:=trim(goae);
    goaek:='",'+goae+',"';
   query:=format('select nummer from re_positionen where nummer<>%s and i_mitarbeiter=%s and datum="%s" and (instr(replace(concat(",",goae_nn,",")," ",""),%s)>0)  ',
   						[nr,inttostr(i_mitarbeiter),dat_delphi_to_sql(datetostr(datum)),goaek] );
   re:=  mysql_d.sql_exe_rowsvorhanden(query);
    if re then
    begin

   		query:=format('update re_positionen set cent="0", netto="0", brutto="0", re_text= left(concat(re_text,": nicht neben Ziffer ","%s"),250  ) where nummer<>%s and i_mitarbeiter=%s and datum="%s" and storno=0 and instr(replace(concat(",",goae_nn,",")," ",""),%s)>0 ',
						[goae,nr,inttostr(i_mitarbeiter),dat_delphi_to_sql(datetostr(datum)),goaek] );
    	mysql_d.sql_exe(query);
      result:=true;
    end;
    end;

end;


var
query,goae, goae_nn,s1:string;
i_mitarbeiter:int64;
datum:tdate;
dbnr:string;
nr,sortierung:string;
begin
//go� verbote raus schmeissen
with datamodul do
begin
  // nicht ust sticht ust
  if checkbox_k_ust_sticht.Checked then sortierung:=' cent desc, ust ' else sortierung := ' cent desc, ust desc ';
	query:=format('select * from re_positionen where renu="%s" and i_firma=%d and storno=0 order by %s ',[renu,i_firma,sortierung]); //nummer
	sql_new(false,q_1,query,'');

	q_1.first;
	while (not q_1.eof) do
	begin
		goae_nn:=goae_nn_normieren(q_1['goae_nn']);
		goae:=q_1['goae'];
        dbnr:=q_1.findfield('nummer').asstring;
        query:=format('select nummer from re_positionen where nummer=%s and cent>0 ', [dbnr] );
		//if (goae<>'') and (goae_nn<>'') then
        //if  q_1.findfield('cent').asinteger>0 then
        if mysql_d.sql_exe_rowsvorhanden(query)then
		begin
		  i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');
		  datum:=q_1['datum'];
		  nr:=getbigint_str(q_1,'nummer');
		  {if goae_ausschluss(s1,goae_nn,i_mitarbeiter,datum) //
			then q_1.delete
			else q_1.next;}
          goae_ausschluss(nr,goae,goae_nn,i_mitarbeiter,datum);
          {if goae_ausschluss(nr,goae_nn,i_mitarbeiter,datum)   then
          begin
          	q_1.Refresh;
          	q_1.Locate('nummer',nr,[loCaseInsensitive]);

          end;}
		end;

		q_1.next;
	end;

end;
end;



function tform_rechnung.summieren(i_firma:integer;re_parent:int64;list:tlist) :int64;        // siehe auch rechnung_aktualisieren
var
query:string;
ust,summe,summe_ust,su_artikel,su_pauschal,su_stunden,su_befunde,su_labor,su_impfung,su_impfstoff,su_vorsorge:integer;
su_ust_artikel,su_ust_pauschal,su_ust_stunden,su_ust_befunde,su_ust_labor,su_ust_impfung,su_ust_impfstoff,su_ust_vorsorge:integer;
netto, brutto,b1:int64;
s1,parameter:string;
db,farbe:integer;
i_mitarbeiter,cent,re_nr:int64;
faktor:real;
eurobrutto, euronetto:currency;
drechnung,w_dat:tdate;
begin
with datamodul do
begin
   parameter:=re_uebersicht(i_firma,re_parent,checklistbox_firmen);
   drechnung:=strtodate(maskedit_redat.Edittext);
	i_mitarbeiter:=getbigint(q_mitarbeiter,'nummer');

	query:=format('select * from re_positionen where renu="%s"  and i_firma=%d and storno=0 order by nummer ',[renu,i_firma]); //cent
    //query:=format('select * from re_positionen where renu="%s" order  by  db,length(goae),goae',[renu]) ; //evtl bessere Sortierung
	sql_new(false,q_1,query,'');
	q_1.first;
  // if kein_datensatz(q_1) and (list.count=0) then exit;
  if kein_datensatz(q_1) and (re_parent>0) then exit; // alle leeren untergeordneten rechnungen

   netto:=0;
   brutto:=0;
	while not q_1.eof do
	begin
      
      //wenn db=8 dann verweis (zuvor 9)
      db:=q_1.findfield('db').asinteger;
      cent:= getbigint(q_1,'cent');
      faktor:=q_1.findfield('faktor').asfloat  ;
      ust:=q_1.findfield('ust').asinteger;
      b1:=round(q_1.findfield('brutto').asfloat*100);
      if db=8 then //verweise     //9
      begin
        netto:=round(netto+cent*faktor); //hier w�re noch besser round(cent * faktor)
         brutto:=brutto+b1;
      end
      else
      begin
         netto:=round(netto+cent*faktor);
         brutto:=brutto+round(round(cent*faktor)*(100+ust)/100);
      end;
	  q_1.next;
	end;



    if  b_f_re then
    begin
       i_mitarbeiter:=0 ;
       farbe:=0;
    end
    else
    begin
      farbe:=6;//lime
    end;

     // else  i_firma:=-1;


    if re_parent >0 then farbe:=2;
    //if i_mitarbeiter>0 then farbe:=7;  //?????
    
    euronetto:=centtobetrag(netto);

    Eurobrutto:=centtobetrag(brutto);

    if CheckBox_wiedervorlage.Checked then w_dat:=drechnung else w_dat:=strtodate('01.01.1900');
   re_nr:=neuer_datensatz(q_rechnung,['nummer','i_firma','i_mitarbeiter','renu','datum','von','bis','eurobetrag','brutto','mws','dat_verschickt','dat_bezahlt','i_parent','m_parameter','farbe','w_dat','w_user'],
   	[null,i_firma,inttostr(i_mitarbeiter),renu,drechnung,svon,sbis, euronetto,eurobrutto,mws,null_datum,null_datum,inttostr(re_parent),parameter,farbe,w_dat,akt_untersucher]);

	if q_rechnung.State in [dsedit, dsinsert] then q_rechnung.post;

   s1:=inttostr(re_nr); //rechnungs_nummer
   query:=format('update re_positionen set i_rechnung=%s where renu="%s" and i_firma=%d',[s1,renu,i_firma] );
   mysql_d.sql_exe(query);
	q_repositionen.Refresh;
   result:=re_nr;
end;
end;


function tform_rechnung.re_uebersicht(i_firma:integer;parent:int64;list:tchecklistbox):string;
var i,j:integer;
s1,query:string;
begin
j:=0;

   result:='Erstellt durch: '+user_name+#10+'Erstellt am: '+datetostr(date())+#10;
   result:=result+'Rechnungsperiode Beginn: '+maskedit_von.Text+#10;
   result:=result+'Rechnungsperiode   Ende: '+maskedit_bis.Text+#10;
   if combobox_ust.ItemIndex=0 then result :=result+  'Leistungen mit und ohne Umsatsteuer'+#10;
   if combobox_ust.ItemIndex=1 then result :=result+  'Leistungen mit Umsatsteuer'+#10;
   if combobox_ust.ItemIndex=2 then result :=result+  'Leistungen ohne Umsatsteuer'+#10;
   if checkbox_k_ust_sticht.Checked then  result :=result+  'ArbmedVV (keine USt) sticht Eignung (mit USt)' else
                                          result :=result+  'Eignung (mit USt) sticht ArbmedVV (keine USt)';
   result:=result + #10;
   if checkbox_pauschal.Checked then result :=result+' Pauschalrechnung'+#10;
   if checkbox_stunden.Checked then result :=result+' Stundenweise Abrechnung'+#10;
   if checkbox_unt.Checked then result :=result+' Vorsorge-Untersuchungen'+#10;
   if checkbox_Impfung.Checked then result :=result+' Impfungen'+#10;
   if checkbox_bef.Checked then result :=result+' Befunde'+#10;
   if checkbox_labor.Checked then result :=result+' Labor'+#10;
   if checkbox_artikel.Checked then result :=result+' Artikel'+#10;
   if not checkbox_betrzuordnen.Checked then result :=result+' Betr�ge den Probanden zuordnen'+#10;
   result:=result + #10;

   if parent>0 then
   begin
      query:=format('select i_firma from rechnung where nummer=%d',[parent]);
      s1:= mysql_d.Feldinhalt(query,0);
      if s1<>'' then j:=strtoint(s1) ;
      //result:= 'Verweis auf Firma:'+firmenname(j)+#10;
      result:= 'Teilrechnung von Firma: '+firmenname(j)+#10;
   end
   else
   begin
      for i:=0 to list.Count-1 do
      begin
         if list.Checked[i] then
         result:=result+'Enthalten: '+list.items[i]+#10;
      end;
   end;

end;


procedure tform_rechnung.proband_einfuegen(i_firma,hr:integer);
var query,re_text:string;
	 i:integer;
	 i_m,i_mitarbeiter:int64;
	 felder :array [0..2] of string;
	 b:tbookmark;
begin
with datamodul do
begin
	i:=1;
	i_m:=0;
	//
  query:=format('select * from re_positionen where renu="%s" and i_firma=%d and storno=0 order by i_mitarbeiter,db,datum, re_text ',[renu,i_firma]); //cent  ##050307
  //query:=format('select * from re_positionen where renu="%s" order by i_mitarbeiter,datum,db, nummer ',[renu]); //cent
	sql_new(false,q_1,query,'');
	q_1.first;
	while not q_1.eof do
	begin
		if (q_1['db']>1) and  (q_1['db']<7) then
		begin
         i_mitarbeiter:=getbigint(q_1,'i_mitarbeiter');
		  if i_m<>i_mitarbeiter then
		  try
				i_m:=i_mitarbeiter;
				b:=q_1.getBookmark;
				query:=format('select name, vorname, geb_dat from mitarbeiter where nummer="%s"',[inttostr(i_mitarbeiter)]);
				mysql_d.Feldarray(query,[0,1,2],felder);
				re_text:=felder[0]+', '+felder[1]+', '+ dat_sql_to_delphi1(felder[2]);
				neuer_datensatz(q_1,['nummer','rf','i_firma','i_mitarbeiter','renu','datum', 're_text','cent','seuro','faktor','goae_nn','db','haupt_re','farbe'],
								 [null,i,i_firma,inttostr(i_mitarbeiter),renu,date, re_text, 0,'',0,'',0,hr,7]);
           q_1.post;
				inc(i);
				q_1.GotoBookmark(b);
		  finally
				q_1.FreeBookmark(b);
		  end;
		end;
		if q_1['db']<>8 then
		begin
		  q_1.edit;
		  q_1['rf']:=i;
		  inc(i);
		end;
		q_1.next;
	end;

 q_repositionen.Refresh;
end;
end;

procedure TForm_rechnung.BitBtn_f_tauschenClick(Sender: TObject);
 var i:integer;
begin
for i:=0 to  checklistbox_firmen.Items.Count- 1 do
begin
 checklistbox_firmen.Checked[i]:=not (checklistbox_firmen.Checked[i]);
end; 
end;

procedure TForm_rechnung.ComboBox_zeitChange(Sender: TObject);
begin
zeiteinstellen;
end;

procedure TForm_rechnung.zeiteinstellen;
var dat:tdate;
	y,m,d:word;
	yl,y2,a,e:string;
begin
dat:=strtodate(maskedit_redat.Text);
decodedate(dat,y,m,d); //date)
y2:=inttostr(y);
yl:=inttostr(y-1);
if ComboBox_zeit.ItemIndex=-1 then ComboBox_zeit.ItemIndex:=0;
case ComboBox_zeit.ItemIndex of
0:begin    //akt Tag
   a:=format('%d.%d.%d',[d,m,y]);
   e:=format('%d.%d.%d',[d,m,y]);
	end;
1:begin    //akt Monat
   a:=format('%d.%d.%d',[1,m,y]);
   d:=tageimmonat(m,y);
   e:=format('%d.%d.%d',[d,m,y]);

  end;
2:begin    //akt Quartal
    m:=quartalbeginn(m);
    a:=format('%d.%d.%d',[1,m,y]);
    m:=m+2;
    d:=tageimmonat(m,y);
    e:=format('%d.%d.%d',[d,m,y]);
  end;
3:begin    //akt Halbjahr
	 m:=halbjahrbeginn(m);
    a:=format('%d.%d.%d',[1,m,y]);
    m:=m+5;
    d:=tageimmonat(m,y);
    e:=format('%d.%d.%d',[d,m,y]);
  end;
4:begin    //akt Jahr
	a:=format('%d.%d.%d',[1,1,y]);
   e:=format('%d.%d.%d',[31,12,y]);
  end;

5:begin   //vortag
   d:=d-1;
   if d=0 then
      begin
         m:=m-1;
         if m=0 then
            begin
            	d:=31;
               m:=12;
               y:=y-1;
            end;
          d:=tageimmonat(m,y);
      end;
	a:=format('%d.%d.%d',[d,m,y]);
   e:=format('%d.%d.%d',[d,m,y]);
  end;
6:begin   //vormonat
    m:=m-1;
    if m=0 then
      begin
         m:=12;
         y:=y-1;
      end;
     a:=format('%d.%d.%d',[1,m,y]);
     d:=tageimmonat(m,y);
   	e:=format('%d.%d.%d',[d,m,y]);
  end;
7:begin     //vorquartal
     m:=quartalbeginn(m);
     m:=m-1;
     if m=0 then
     begin
       m:=12;
       y:=y-1;
     end;
     m:=quartalbeginn(m);

    a:=format('%d.%d.%d',[1,m,y]);
    m:=m+2;
    d:=tageimmonat(m,y);
    e:=format('%d.%d.%d',[d,m,y]);

  end;
8:begin      //vorhalbjar
	 m:=halbjahrbeginn(m);
    m:=m-1;
     if m=0 then
     begin
       m:=12;
       y:=y-1;
     end;
     m:=halbjahrbeginn(m);
    a:=format('%d.%d.%d',[1,m,y]);
    m:=m+5;
    d:=tageimmonat(m,y);
    e:=format('%d.%d.%d',[d,m,y]);
  end;
9:begin      //vorjahr
   y:=y-1;
	a:=format('%d.%d.%d',[1,1,y]);
   e:=format('%d.%d.%d',[31,12,y]);
  end;
10:begin     //n�chster Tag
	d:=d+1;
   if d> tageimmonat(m,y) then
      begin
         m:=m+1;
         if m=13 then
            begin
            	d:=1;
               m:=1;
               y:=y+1;
            end;
         d:=1;
      end;
	a:=format('%d.%d.%d',[d,m,y]);
   e:=format('%d.%d.%d',[d,m,y])
  end;
11:begin   //n�chster Monat
   m:=m+1;
    if m=13 then
      begin
         m:=1;
         y:=y+1;
      end;
     a:=format('%d.%d.%d',[1,m,y]);
     d:=tageimmonat(m,y);
     e:=format('%d.%d.%d',[d,m,y])
	end;
12:begin   //n�chstes Quartal
     m:=quartalbeginn(m)+3;     //erster monat im neuen Q

     if m=13 then
     begin
       m:=1;
       y:=y+1;
     end;
     m:=quartalbeginn(m);

    a:=format('%d.%d.%d',[1,m,y]);
    m:=m+2;
    d:=tageimmonat(m,y);
    e:=format('%d.%d.%d',[d,m,y]);
	end;
13:begin   //n�chstes halbjahr
    m:=halbjahrbeginn(m)+6;

     if m=13 then
     begin
       m:=1;
       y:=y+1;
     end;
     m:=halbjahrbeginn(m);
    a:=format('%d.%d.%d',[1,m,y]);
    m:=m+5;
    d:=tageimmonat(m,y);
    e:=format('%d.%d.%d',[d,m,y]);
	end;
14:begin   //n�chstes Jar
     y:=y+1;
	a:=format('%d.%d.%d',[1,1,y]);
   e:=format('%d.%d.%d',[31,12,y]);
	end;

end;


MaskEdit_von.Text:=datetostr(strtodate(a));
MaskEdit_bis.Text:=datetostr(strtodate(e));

end;


procedure TForm_rechnung.zeiteinstellen_monat(m1,m2:integer);
var dat:tdate;
	y,m,d:word;
	yl,y2,a,e:string;
begin
dat:=strtodate(maskedit_redat.Text);
decodedate(dat,y,m,d); //date)
y2:=inttostr(y);
yl:=inttostr(y-1);
a:=format('%d.%d.%d',[1,m1,y]);
d:=tageimmonat(m2,y);
e:=format('%d.%d.%d',[d,m2,y]);



MaskEdit_von.Text:=datetostr(strtodate(a));
MaskEdit_bis.Text:=datetostr(strtodate(e));

end;

procedure TForm_rechnung.FormDestroy(Sender: TObject);
begin
checklistbox_firmen.Items.Clear;
end;

procedure TForm_rechnung.SpeedButton11Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 begin
		maskedit_redat.Text:= datetostr(form_kalender.auswahlkalender.date);


	 end;
end;

end.
