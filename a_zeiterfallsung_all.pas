unit a_zeiterfallsung_all;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, dbgridEXT, Buttons, dbcombo_number, Mask,
  DBCtrls, ExtCtrls, Spin,ComCtrls;

type
  TForm_zeiterfassung_all = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label41: TLabel;
    Label36: TLabel;
    SpeedButton6: TSpeedButton;
    Label170: TLabel;
    Label184: TLabel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    DBgridEXT_firma: TDBgridEXT;
    Memo_taetigkeit: TMemo;
    Memo2: TMemo;
    ComboBox_kriterium: TComboBox;
    ComboBox_zeitkonto: TComboBox;
    MaskEdit_date: TMaskEdit;
    GroupBox5: TGroupBox;
    SpeedButton_z1: TSpeedButton;
    SpeedButton_z2: TSpeedButton;
    Label9: TLabel;
    Label15: TLabel;
    MaskEdit_z1: TMaskEdit;
    MaskEdit_z2: TMaskEdit;
    Label2: TLabel;
    ComboBox_abr_zeitkonto: TComboBox;
    GroupBox6: TGroupBox;
    Panel3: TPanel;
    BitBtn_leistungswahl: TBitBtn;
    Memo_aufgabenfeld: TMemo;
    ComboBox_erbringer: TComboBox;
    Panel4: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Button_fwahl: TButton;
    Label42: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    Label7: TLabel;
    Label8: TLabel;
    SpinEdit3: TSpinEdit;
    Panel2: TPanel;
    BitBtn_taetigkeit: TBitBtn;
    Button2: TButton;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button_fwahlClick(Sender: TObject);
    procedure BitBtn_leistungswahlClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton_z1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn_taetigkeitClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure leistungsauswahl(parameter:string);
  public
    { Public-Deklarationen }
    ff:string;
  end;

var
  Form_zeiterfassung_all: TForm_zeiterfassung_all;

implementation

uses a_f_wahl, a_data, a_unt_wahl, a_main, a_kalender, a_suchliste;

{$R *.DFM}

procedure TForm_zeiterfassung_all.FormCreate(Sender: TObject);
var 
	yy,m,d:word;
	y,query:string;
begin
form_main.combolesen(combobox_kriterium,'abr_anlass_krit','name','name','');
combobox_kriterium.ItemIndex:=0;
form_main.combolesen(combobox_zeitkonto,'abr_zeitkonto','name','name','');
combobox_zeitkonto.ItemIndex:=0;
form_main.combolesen(combobox_abr_zeitkonto,'abr_zeitkonto','name','name','');
combobox_abr_zeitkonto.ItemIndex:=0;
form_main.combolesen(combobox_erbringer,'untersucher','untersucher','untersucher','');
maskedit_date.Text:=datetostr(now());


decodedate(date,yy,m,d);
y:=inttostr(yy);
MaskEdit_z1.Text:='01.01.'+y;
MaskEdit_z2.Text:='31.12.'+y;

query:='update firma set einsatzstunden=0, einsatzstunden2 =0';
mysql_d.sql_exe(query);
Button_fwahlClick(Sender);

//Button1Click(sender);

end;

procedure TForm_zeiterfassung_all.Button_fwahlClick(Sender: TObject);
var
query:string;
begin
try
    dbgridext_firma.Visible:=false;
	form_f_wahl:=tform_f_wahl.create(self);
	if form_f_wahl.ShowModal=mrok then
	begin
	  ff:=form_f_wahl.firmen_nummern;
	  query:=format('select nummer, firma,einsatzstunden,einsatzstunden2 from firma where nummer in (%s) ',[ff]);
	  datamodul.sql_new(true,datamodul.q_5,query,'firma');
	end
	else
	begin
		datamodul.sql_new(true,datamodul.q_5,'select firma from firma where nummer=-1','');
	end;
    dbgridext_firma.Visible:=true;
	//firmen_nummern:= form_f_wahl.firmen_nummern;
finally
	form_f_wahl.Release;

end;


end;

procedure TForm_zeiterfassung_all.BitBtn_leistungswahlClick(
  Sender: TObject);
begin
 leistungsauswahl('aufgabe');
end;



procedure TForm_zeiterfassung_all.SpeedButton6Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	begin
   maskedit_date.Text:=datetostr(form_kalender.auswahlkalender.date);
  end;
end;

procedure TForm_zeiterfassung_all.SpeedButton_z1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	begin
   if sender=speedbutton_z1 then
   MaskEdit_z1.Text:=datetostr(form_kalender.auswahlkalender.date)
   else
   MaskEdit_z2.Text:=datetostr(form_kalender.auswahlkalender.date);
  end;
end;

procedure TForm_zeiterfassung_all.Button2Click(Sender: TObject);
var
s_firma,s_konto,query:string;
i_konto,s:integer;
t,h,sum,t2:real;
d1,d2:tdate;
prozent:boolean;
begin
sum:=0;
try
s:=spinedit1.value+spinedit2.value+spinedit3.Value;
except
	spinedit1.value:=0;
   spinedit2.value:=0;
   spinedit3.value:=0;
end;
if (spinedit1.value+spinedit2.value>0) and (spinedit3.Value>0) then
begin
	showmessage ('Es darf nur Zeit oder Prozentsatz ausgew�hlt sein');
   exit;
end;

if spinedit1.Value+spinedit2.Value+spinedit3.Value =0 then
begin
	showmessage('Bitte Zeit oder Prozentsatz eintragen');
    exit;
end;


if spinedit3.Value>0 then prozent:=true else prozent:=false;
try
  d1:=strtodate(form_zeiterfassung_all.maskedit_z1.Text);
  d2:=strtodate(form_zeiterfassung_all.maskedit_z2.Text);
except
  showmessage('Bitte Datum �berpr�fen');
  exit;
end;
s_konto:=ComboBox_abr_zeitkonto.Text ;
query:=format('select nummer from abr_zeitkonto where name="%s"',[s_konto]);

s_konto:=mysql_d.Feldinhalt(query,0);
if s_konto<>'' then i_konto:=strtoint(s_konto) else i_konto:=-1;


datamodul.q_5.first;        //auf welche Zeit verteilen?
   while not datamodul.q_5.eof do
       begin
         s_firma:=datamodul.q_5.findfield('nummer').asstring;
         t:=form_main.sollzeiten(s_firma,d1,d2,-1,i_konto);
         sum:=sum+t;
         datamodul.q_5.edit;
         datamodul.q_5.findfield('einsatzstunden').asfloat:=t;
         datamodul.q_5.post;
         datamodul.q_5.next;
       end;

if prozent then
begin
   
   h:=spinedit3.value/100;
	datamodul.q_5.first;
   while not datamodul.q_5.eof do
   begin
     datamodul.q_5.edit;
     datamodul.q_5.findfield('einsatzstunden2').asfloat:=datamodul.q_5.findfield('einsatzstunden').asfloat*h;
     datamodul.q_5.post;
     datamodul.q_5.next;
   end;
end
else
begin
   if  sum>0 then
   begin
     h:=(spinedit1.Value+spinedit2.Value/60)/sum;

     datamodul.q_5.first;
     while not datamodul.q_5.eof do
     begin
        t2:= datamodul.q_5.findfield('einsatzstunden').asfloat*h;
        t2:=round(t2*100) / 100;  //2 stellen hinter komma
       datamodul.q_5.edit;
       datamodul.q_5.findfield('einsatzstunden2').asfloat:=t2;
       datamodul.q_5.post;
       datamodul.q_5.next;
     end;
   end;  
end;
datamodul.q_5.refresh;
datamodul.q_5.first;
bitbtn1.Enabled:=true;
end;

procedure TForm_zeiterfassung_all.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   {form_main.combodeaktivieren(combobox_kriterium);
   form_main.combodeaktivieren(combobox_zeitkonto);
   form_main.combodeaktivieren(combobox_abr_zeitkonto);
   form_main.combodeaktivieren(combobox_erbringer); }
end;

procedure TForm_zeiterfassung_all.leistungsauswahl(parameter:string);
var ber,i:integer;
nr:int64;
s,w,query:string;
first:boolean;
sel:ttreenode;
daten:pnodepointer;
sl : TStringList;
ndx : Integer;

begin

try


       query:='select nummer, left(name,250) as name_f  from aufwand order by name';
       datamodul.sql_new(true, datamodul.q_7 ,query,'');
       query:='select * from abrechnung_anlass where storno=0';
		   datamodul.sql_new(false,datamodul.q_3,query, 'reihenfolge');
       form_suchliste:=tform_suchliste.create(self);
       form_suchliste.CheckBox_parent.Visible:=false;
       form_suchliste.DBgrid.columns[0].fieldname:='name_f';
       form_suchliste.DBgrid.DataSource:=datamodul.ds_7;
       form_suchliste.s_name:='name_f';
       form_suchliste.tree.mysqlquery:=datamodul.q_3;
      form_suchliste.tree.mysql_feld:='name';
      form_suchliste.tree.liste_lesen;
      if parameter='aufgabe' then  form_suchliste.CheckBox_aufgabenfeld.checked:=true;
      if parameter='taetigkeit' then  form_suchliste.CheckBox_taetigkeit.checked:=true;
      form_suchliste.startparameter:=parameter;
      
       if form_suchliste.ShowModal=mrok then
       begin

           if form_suchliste.CheckBox_aufgabenfeld.Checked then
           begin
              first:=true;
              s:=memo_aufgabenfeld.text;

              sel:=form_suchliste.Tree.Selected;
              for i:=0 to form_suchliste.Tree.Items.Count-1 do
                 begin
                     if (( form_suchliste.Tree.Items[i].StateIndex=3) or (form_suchliste.Tree.Items[i]=sel)) then
                        begin
                           daten:=form_suchliste.Tree.Items[i].data;
                           nr:=daten^.nummer;


                           datamodul.q_3.Locate('nummer',nr,[]);

                          
                          if length(s)>0 then s:=s+#13#10;
                          s:=s+datamodul.q_3.findfield('name').AsString;

                        end;
                 end;
              end;
              memo_aufgabenfeld.Text:=s;

              if form_suchliste.CheckBox_taetigkeit.Checked then
              begin

                 s:=memo_taetigkeit.text;

                 if  form_suchliste.dbgrid.SelectedRows.Count>0 then
                  begin
                      for I := 0 to form_suchliste.dbgrid.SelectedRows.Count - 1 do
                        begin
                             datamodul.q_7.GotoBookmark(Pointer(form_suchliste.DBGrid.SelectedRows.Items[I]));
                             if length(s)>0 then s:=s+#13#10;
                             s:=s+datamodul.q_7.findfield('name_f').AsString;

                        end;
                  end
                  else
                  begin
                    if length(s)>0 then s:=s+#13#10;
                    s:=datamodul.q_7.findfield('name_f').AsString;
                  end;
                 memo_taetigkeit.Text:=s;
              end;
   		end;


finally
   form_suchliste.Release;
	application.processmessages;
end;
end;

procedure TForm_zeiterfassung_all.BitBtn_taetigkeitClick(Sender: TObject);
begin
 leistungsauswahl('taetigkeit');
end;

procedure TForm_zeiterfassung_all.BitBtn1Click(Sender: TObject);
begin
 if trim(memo_aufgabenfeld.text)<>'' then modalresult:=mrok
 else
 showmessage('Bitte Aufgabenfeld ausf�llen');
end;

procedure TForm_zeiterfassung_all.FormDestroy(Sender: TObject);
begin
 form_main.combodeaktivieren(combobox_kriterium );
 form_main.combodeaktivieren(combobox_zeitkonto);
 form_main.combodeaktivieren( combobox_abr_zeitkonto);
 form_main.combodeaktivieren(combobox_erbringer);
end;

end.
