unit a_memo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TForm_memo = class(TForm)
    Panel1: TPanel;
    Memo: TMemo;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_memo: TForm_memo;

implementation

uses a_main;

{$R *.dfm}

procedure TForm_memo.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));

end;

procedure TForm_memo.FormShow(Sender: TObject);
begin
	memo.SetFocus;
end;

end.
