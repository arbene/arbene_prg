�
 TFORM_ICD 0�  TPF0	TForm_icdForm_icdLeft� TopnBorderIcons BorderStylebsDialogCaptionICD 10ClientHeight:ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight 
TDBgridEXT
DBgridEXT1Left Top)Width�Height�AlignalClient
DataSourceDataModul.ds_icd_mainDefaultDrawing	OptionsdgTitles
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelection ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT1DblClick	OnKeyDownDBgridEXT1KeyDownkeine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameicd_schluesselTitle.CaptionICD-Schl�sselWidth[Visible	 Expanded	FieldNameicd_textTitle.CaptionICD-TextWidth Visible	    TPanelPanel1Left TopWidth�Height5AlignalBottomTabOrder TBitBtn	BitBtn_okLefttTopWidth|HeightCaptionOKTabOrder OnClickBitBtn_okClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn_abbortLeft\TopWidth|HeightCaptionAbbruchTabOrderOnClickBitBtn_abbortClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs   TPanelPanel2Left Top Width�Height)AlignalTopTabOrder TBitBtnBitBtn_ebzurLeftTopWidthaHeightCaptionEbene zur�ckTabOrder OnClickBitBtn_ebzurClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333�3333337D333333�s333337DG33333�w�33337DDC3333�s733337DDD3333�s3s3337DDDG333�s37�337DDDDC33�s337337DDDDD337�������������s7wwwwws3�����337s�33s�333�����337s�37�3333����3337s�7?33333���33337s�s�33333���33337s��333333��333337w�3333333�3333337s	NumGlyphs  TBitBtnBitBtn_ebweiLeftxTopWidthaHeightCaptionEbene weiterTabOrderOnClickBitBtn_ebweiClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�333333Ds333333w��33333tDs333337w?�33334DDs33337?w?�3333DDDs3333s�w?�333tDDDs3337�3w?�334DDDDs337?33w?�33DDDDDs?����w���������wwwwww3w33�����333s33?w33<����3337�3?w333<���333373?w3333���33333s?w3333<��333337�w33333<�3333337w333333�3333333w3333333LayoutblGlyphRight	NumGlyphs  TBitBtnBitBtn_suwortLeft�TopWidthIHeightCaptionsuchenTabOrderOnClickBitBtn_suwortClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333333333333?�3333330 333333��333333 33333?��333330 333333��333333 333?�?��338 ;�333?����333�� �333�38��330�w�3338�3�8�33��w��33838�8?33��x33�3���33w���33�8��3�33��x33��8�3�33�ww��338�838�330��3338?�3�3333��3333�?��33338 333333��3333	NumGlyphs  TEdit	Edit_suchLeft Top
Width� HeightTabOrder
OnKeyPressEdit_suchKeyPress   	TGroupBox	GroupBox1Left Top�Width�Height=AlignalBottomCaptionZusatznuummernTabOrder TLabelLabel1Left0TopWidth>HeightCaptionSternnummer  TLabelLabel2Left,TopWidthEHeightCaptionZusatznummer  TDBEditDBEdit_sternLeft� TopWidthyHeight	DataFieldsternnummer
DataSourceDataModul.ds_icd_mainReadOnly	TabOrder   TDBEditDBEdit2Left�TopWidthyHeight	DataFieldzusatznummer
DataSourceDataModul.ds_icd_mainReadOnly	TabOrder    