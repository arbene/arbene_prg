unit a_weiter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls;

type
  TForm_weiter = class(TForm)
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    Timer1: TTimer;
    Memo: TMemo;
    ProgressBar: TProgressBar;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_weiter: TForm_weiter;

implementation

uses a_main;

{$R *.dfm}

procedure TForm_weiter.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

procedure TForm_weiter.Timer1Timer(Sender: TObject);
begin
progressbar.Position:=progressbar.Position+1;
if progressbar.Position=60 then modalresult:=mrok;
end;

end.
