unit a_richedit_menue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, StdCtrls, ToolWin,editor;

type
  Tform_richedit_menue = class(TForm)
    ToolBar_anamnese: TToolBar;
    Openbutton: TToolButton;
    savebutton: TToolButton;
    ToolButton14: TToolButton;
    Undobutton: TToolButton;
    Copybutton: TToolButton;
    Pastebutton: TToolButton;
    ToolButton3: TToolButton;
    FontName: TComboBox;
    ToolButton11: TToolButton;
    FontSize: TEdit;
    UpDown1: TUpDown;
    ToolButton5: TToolButton;
    BoldButton: TToolButton;
    ItalicButton: TToolButton;
    UnderlineButton: TToolButton;
    ToolButton_color: TToolButton;
    ToolButton16: TToolButton;
    LeftAlign: TToolButton;
    CenterAlign: TToolButton;
    RightAlign: TToolButton;
    ToolButton8: TToolButton;
    BulletsButton: TToolButton;
    ToolButton2: TToolButton;
    ImageList_editor: TImageList;
    RichEdit_start: TRichEdit;
    ToolButton1: TToolButton;
    ToolButton_font_plus: TToolButton;
    ToolButton_font_minus: TToolButton;
    procedure BoldButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ItalicButtonClick(Sender: TObject);
    procedure UnderlineButtonClick(Sender: TObject);
    procedure ToolButton_colorClick(Sender: TObject);
    procedure LeftAlignClick(Sender: TObject);
    procedure CenterAlignClick(Sender: TObject);
    procedure RightAlignClick(Sender: TObject);
    procedure BulletsButtonClick(Sender: TObject);
    procedure UndobuttonClick(Sender: TObject);
    procedure CopybuttonClick(Sender: TObject);
    procedure PastebuttonClick(Sender: TObject);
    procedure RichEdit_startSelectionChange(Sender: TObject);
    procedure FontNameChange(Sender: TObject);
    procedure FontSizeChange(Sender: TObject);
    procedure ToolButton_font_plusClick(Sender: TObject);
    procedure ToolButton_font_minusClick(Sender: TObject);



  private
    { Private-Deklarationen }
    rtfedit:trtfedit;
  public
    { Public-Deklarationen }
    procedure ini(re:trichedit);
  end;

var
  form_richedit_menue: Tform_richedit_menue;

implementation

uses a_main;

{$R *.dfm}

procedure Tform_richedit_menue.ini(re:trichedit);
begin
if (re=nil) or (not assigned(re)) then
begin
  showmessage('what');
  exit;
end;

try
  rtfedit.akt_editor :=re;
  rtfedit.akt_editor.OnSelectionChange:=richedit_start.OnSelectionChange;
  rtfedit.akt_editor.SelStart:=0;
except
end;  
end;

procedure Tform_richedit_menue.BoldButtonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
{if ttoolbutton(sender).Down then
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style + [fsBold]
	else
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsBold]; }

 if fsbold in rtfedit.akt_editor.SelAttributes.Style then rtfedit.akt_editor.SelAttributes.Style:= rtfedit.akt_editor.SelAttributes.Style - [fsBold]
  else rtfedit.akt_editor.SelAttributes.Style:=rtfedit.akt_editor.SelAttributes.Style + [fsBold];
end;

procedure Tform_richedit_menue.FormCreate(Sender: TObject);
begin
rtfedit:=trtfedit.create(richedit_start,toolbar_anamnese);

end;

procedure Tform_richedit_menue.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 rtfedit.Free;
end;

procedure Tform_richedit_menue.ItalicButtonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;

	{if ttoolbutton(sender).Down then
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style + [fsItalic]
	else
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsItalic]; }

    if fsItalic in  rtfedit.akt_editor.SelAttributes.Style then  rtfedit.akt_editor.SelAttributes.Style:=  rtfedit.akt_editor.SelAttributes.Style - [fsItalic]
    else  rtfedit.akt_editor.SelAttributes.Style:= rtfedit.akt_editor.SelAttributes.Style + [fsItalic];
end;

procedure Tform_richedit_menue.UnderlineButtonClick(Sender: TObject);
begin
 if rtfedit.FUpdating then Exit;

	{if ttoolbutton(sender).Down then
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style + [fsUnderline]
	else
	  rtfedit.CurrText.Style := rtfedit.CurrText.Style - [fsUnderline]; }

 if fsunderline in  rtfedit.akt_editor.SelAttributes.Style then  rtfedit.akt_editor.SelAttributes.Style:=  rtfedit.akt_editor.SelAttributes.Style - [fsunderline]
  else  rtfedit.akt_editor.SelAttributes.Style:= rtfedit.akt_editor.SelAttributes.Style + [fsunderline];
end;

procedure Tform_richedit_menue.ToolButton_colorClick(Sender: TObject);
begin
  if assigned(rtfedit) then
  begin

    if rtfedit.FUpdating then Exit;

    if not form_main.colordialog.Execute then exit;


    rtfedit.CurrText.Name := FontName.Items[FontName.ItemIndex];
    rtfedit.CurrText.Size := StrToInt(FontSize.Text);
    rtfedit.CurrText.Color:=form_main.colordialog.Color;
  end;  
//except
end;


procedure Tform_richedit_menue.LeftAlignClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;

	rtfedit.akt_editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure Tform_richedit_menue.CenterAlignClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
	rtfedit.akt_editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure Tform_richedit_menue.RightAlignClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
	rtfedit.akt_editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure Tform_richedit_menue.BulletsButtonClick(Sender: TObject);
begin
	if rtfedit.FUpdating then Exit;
	rtfedit.akt_editor.Paragraph.Numbering := TNumberingStyle(ttoolbutton(sender).Down);
end;

procedure Tform_richedit_menue.UndobuttonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.EditUndo(self);
end;

procedure Tform_richedit_menue.CopybuttonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.Editcopy(self);
end;

procedure Tform_richedit_menue.PastebuttonClick(Sender: TObject);
begin
if rtfedit.FUpdating then Exit;
rtfedit.Editpaste(self);
end;

procedure Tform_richedit_menue.RichEdit_startSelectionChange(
  Sender: TObject);
begin
rtfedit.SelectionChange(Self);
end;

procedure Tform_richedit_menue.FontNameChange(Sender: TObject);
begin
if assigned(rtfedit) then
try
  //try //wird aufgerufen bevor rtfedit creiert.
  if rtfedit.FUpdating then Exit;
  if rtfedit.akt_editor.SelText='' then exit;
  rtfedit.CurrText.Name := fontname.Items[FontName.ItemIndex];
  rtfedit.CurrText.Size := StrToInt(fontsize.Text);
  //rtfedit.CurrText.Color:=form_main.colordialog.Color;
except
end;
end;

procedure Tform_richedit_menue.FontSizeChange(Sender: TObject);
begin
if assigned(rtfedit) and assigned(rtfedit.akt_editor) then begin
//wird aufgerufen bevor rtfedit creiert.
if rtfedit.FUpdating then Exit;
if rtfedit.akt_editor.SelText='' then exit;
{rtfedit.akt_editor.Font.Name :=fontname.Items[FontName.ItemIndex];
rtfedit.akt_editor.Font.Size:=StrToInt(fontsize.Text);
rtfedit.akt_editor.Font.Color:=form_main.colordialog.Color; }
  try
    rtfedit.CurrText.Name := fontname.Items[FontName.ItemIndex];
		rtfedit.CurrText.Size := StrToInt(fontsize.Text);
		//rtfedit.CurrText.Color:=form_main.colordialog.Color;
  except
	FontSize.Text:='10';
  end; 
end;
end;

procedure Tform_richedit_menue.ToolButton_font_plusClick(Sender: TObject);
begin
  rtfedit.akt_editor.SelAttributes.Height:= rtfedit.akt_editor.SelAttributes.Height+2;
end;

procedure Tform_richedit_menue.ToolButton_font_minusClick(Sender: TObject);
begin
   if rtfedit.akt_editor.SelAttributes.Height>5 then
    rtfedit.akt_editor.SelAttributes.Height:= rtfedit.akt_editor.SelAttributes.Height-2;
end;

end.
