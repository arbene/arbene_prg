unit a_icd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, dbgridEXT, a_data, Mask,
  DBCtrls;

type
  TForm_icd = class(TForm)
    DBgridEXT1: TDBgridEXT;
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn_ebzur: TBitBtn;
    BitBtn_ebwei: TBitBtn;
    BitBtn_suwort: TBitBtn;
    BitBtn_ok: TBitBtn;
    BitBtn_abbort: TBitBtn;
    Edit_such: TEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit_stern: TDBEdit;
    DBEdit2: TDBEdit;
    procedure BitBtn_ebweiClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn_ebzurClick(Sender: TObject);
    procedure DBgridEXT1DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn_suwortClick(Sender: TObject);
    procedure DBgridEXT1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn_okClick(Sender: TObject);
    procedure BitBtn_abbortClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit_suchKeyPress(Sender: TObject; var Key: Char);
   
  private
    { Private-Deklarationen }
    filterarray: array [1..4] of string;
    procedure grundeinstellung;
  public
    { Public-Deklarationen }
  end;

var
  Form_icd: TForm_icd;

implementation

uses a_main;
{$R *.DFM}



procedure TForm_icd.BitBtn_ebweiClick(Sender: TObject);
var
	bb,be,code,filter, filter_beginn, filter_ende,levelneu,s: string;
  ebene,i_beginn, i_ende: integer;
begin
try
	filter:='';
  s:=chr(39);
	code:=datamodul.q_icd_main['icd_schluessel'];
  ebene:=datamodul.q_icd_main['icd_ebene'];
  filterarray[ebene]:=datamodul.q_icd_main.sql.text;
  i_beginn:=strtoint(copy(code,2,2));
  bb:=copy(code,1,1);
  if (ebene=1) or (ebene=2) then
  	begin
        i_ende:=strtoint(copy(code,6,2));
        be:=copy(code,5,1);
        if i_beginn<=9 then
        	filter_beginn:=bb+'0'+inttostr(i_beginn)
        else  filter_beginn:=bb+inttostr(i_beginn);
        if i_ende <=9 then
        	filter_ende:=be+'0'+inttostr(i_ende)
        else filter_ende:=be+inttostr(i_ende);
        levelneu:=inttostr(ebene+1);
        Filter :='select icd_schluessel , icd_text,sternnummer,zusatznummer, icd_ebene, nummer from icd_main where ( icd_schluessel>=' +s +filter_beginn+s+ ' and  icd_schluessel<= '+s+ filter_ende+s+' and icd_ebene='+levelneu+' )';
     end;
   if (ebene =3)   then
   begin
    if i_beginn<=9 then
    	filter_beginn:=bb+'0'+inttostr(i_beginn)
    else filter_beginn:=bb+inttostr(i_beginn);
    if i_beginn<=8 then
    	filter_ende:=bb+'0'+inttostr(i_beginn+1)
    else filter_ende:=bb+inttostr(i_beginn+1);
   Filter :=
   'select icd_schluessel , icd_text, sternnummer,zusatznummer,icd_ebene ,nummer from icd_main where ( icd_schluessel>=' +s +filter_beginn+ s+' and  icd_schluessel<= '+s+ filter_ende+s+' and icd_ebene=4 )';
   end;

   if filter<>'' then
   begin
	   datamodul.q_icd_main.Close;
     sql_normieren(filter);
  	datamodul.q_icd_main.sql.text:=filter;
		datamodul.q_icd_main.open;
   end;
   if kein_datensatz(datamodul.q_icd_main) then
   begin
     datamodul.q_icd_main.Close;
     sql_normieren(filterarray[3]);
  	datamodul.q_icd_main.sql.text:=filterarray[3];
		datamodul.q_icd_main.open;
   end;
   ebene:= datamodul.q_icd_main['icd_ebene'];

   if ebene= 4 then  bitbtn_ebwei.Enabled:=false;
   bitbtn_ebzur.Enabled:=true;
except
end;
end;

procedure TForm_icd.FormActivate(Sender: TObject);

begin
grundeinstellung;
end;

procedure TForm_icd.BitBtn_ebzurClick(Sender: TObject);
var
	ebene: integer;
begin
try
   ebene:=datamodul.q_icd_main['icd_ebene'];
   if (ebene=1) or (filterarray[ebene-1]='') then
   begin
     grundeinstellung;
     exit;
   end;
   datamodul.q_icd_main.Close;
   sql_normieren(filterarray[ebene-1]);
   datamodul.q_icd_main.sql.text:=filterarray[ebene-1];
   datamodul.q_icd_main.open;
   ebene:= datamodul.q_icd_main['icd_ebene'];
   if ebene= 1 then  bitbtn_ebzur.Enabled:=false;
   bitbtn_ebwei.Enabled:=true;
except
end;
end;

procedure TForm_icd.DBgridEXT1DblClick(Sender: TObject);

begin
	 if datamodul.q_icd_main['icd_ebene'] =4 then modalresult:=mrok
	 else if BitBtn_ebwei.Enabled then BitBtn_ebweiClick(self);
end;


procedure TForm_icd.Button1Click(Sender: TObject);
var j: integer;
text:string;
a1,a2: array[1..7] of string;
begin
{a1[1]:=chr(132);
a2[1]:='�';
a1[2]:=chr(148);
a2[2]:='�';
a1[3]:=chr(129);
a2[3]:='�';
a1[4]:=chr(153);
a2[4]:='�';
a1[5]:=chr(154);
a2[5]:='�';
a1[6]:=chr(142);
a2[6]:='�';
a1[7]:='�';
a2[7]:='�';

datamodul.q_rules.First;
while not datamodul.q_rules.eof do
begin
if datamodul.q_rules['such']<>null then
text:=datamodul.q_rules['such']
else text:='';
for j:=1 to 7 do
begin
	text:=stringreplace(text,a1[j],a2[j],[rfReplaceAll]);
end;

datamodul.q_rules.edit;
datamodul.q_rules['such']:=text;
datamodul.q_rules.post;
datamodul.q_rules.next;
end;

datamodul.q_rules.First;
while not datamodul.q_rules.eof do
begin
if datamodul.q_rules['ersatz1']<>null then
text:=datamodul.q_rules['ersatz1']
else text:='';
for j:=1 to 7 do
begin
	text:=stringreplace(text,a1[j],a2[j],[rfReplaceAll]);
end;

datamodul.q_rules.edit;
datamodul.q_rules['ersatz1']:=text;
datamodul.q_rules.post;
datamodul.q_rules.next;
end;

datamodul.q_rules.First;
while not datamodul.q_rules.eof do
begin
if datamodul.q_rules['ersatz2']<>null then
text:=datamodul.q_rules['ersatz2']
else text:='';

for j:=1 to 7 do
begin
	text:=stringreplace(text,a1[j],a2[j],[rfReplaceAll]);
end;

datamodul.q_rules.edit;
datamodul.q_rules['ersatz2']:=text;
datamodul.q_rules.post;
datamodul.q_rules.next;
end;}



end;

procedure TForm_icd.BitBtn_suwortClick(Sender: TObject);
var
query:string;
begin
query:='select icd_schluessel , icd_text,sternnummer,zusatznummer, icd_ebene,icd_main.nummer from icd_main where icd_text like "%'+edit_such.text+'%"';
datamodul.q_icd_main.Close;
sql_normieren(query);
 datamodul.q_icd_main.sql.text:=query;
 datamodul.q_icd_main.open;
bitbtn_ebzur.Enabled:=true;

end;

procedure TForm_icd.DBgridEXT1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case key of
	VK_RETURN: BitBtn_ebweiClick(self);
  vk_back:  BitBtn_ebzurClick(self) ;
end;
end;

procedure TForm_icd.BitBtn_okClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_icd.BitBtn_abbortClick(Sender: TObject);
begin
modalresult:=mrabort;
end;



procedure TForm_icd.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));

grid_suchen(sender);

end;

procedure TForm_icd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	//datamodul.sql(datamodul.q_icd_main,'select * from icd_main','');
end;


procedure TForm_icd.grundeinstellung;
var
filter: string;
begin
   bitbtn_ebzur.Enabled:=false;
   bitbtn_ebwei.Enabled:=true;
   filter:='select icd_schluessel , icd_text,sternnummer,zusatznummer,icd_ebene,nummer from icd_main where icd_ebene =1';
   datamodul.q_icd_main.Close;
   datamodul.q_icd_main.sql.clear;
   datamodul.q_icd_main.sql.add(filter);
   datamodul.q_icd_main.open;
end;


procedure TForm_icd.Edit_suchKeyPress(Sender: TObject; var Key: Char);
begin
	if key=chr(13) then begin
  	BitBtn_suwortClick(sender);
     key:=char(0);
     end;
end;

end.
