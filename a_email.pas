unit a_email;

interface

uses
		Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, DBCtrls, Grids, DBGrids, dbgridEXT, ComCtrls, ExtCtrls, ToolWin,
	a_main, Mask,a_data,db, Spin, variants ;

type
	TForm_email = class(TForm)
	  ToolBar1: TToolBar;
	  ToolButton_neu: TToolButton;
	  ToolButton2: TToolButton;
	  ToolButton_delete: TToolButton;
	  ToolButton3: TToolButton;
	  ToolButton1: TToolButton;
	  Panel1: TPanel;
	  RadioGroup_sort: TRadioGroup;
	  Panel2: TPanel;
	  StatusBar: TStatusBar;
    DBgridEXT: TDBgridEXT;
    ToolButton_cancel: TToolButton;
	  ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_send: TToolButton;
	  ToolButton8: TToolButton;
    PageControl: TPageControl;
    TabSheet_ausgang: TTabSheet;
    TabSheet_sent: TTabSheet;
    RadioGroup_user: TRadioGroup;
    RadioGroup_time: TRadioGroup;
	  procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
	  procedure ToolButton_deleteClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure RadioGroup_sentClick(Sender: TObject);
	  procedure ToolButton_sendClick(Sender: TObject);
	  procedure SakSMTPEncodeStart(Sender: TObject; FileName: String;
		 BytesCount: Integer);
	  procedure FormCreate(Sender: TObject);
	  procedure ToolButton_cancelClick(Sender: TObject);
	  procedure DBgridEXTDblClick(Sender: TObject);
	  procedure ToolButton_neuClick(Sender: TObject);
	private
	  { Private-Deklarationen }
	  zeit_popini:ttime;
	  procedure liste_filtern;
	  function send_silent:boolean;

	public
	  { Public-Deklarationen }
	  pro1, pro2: tprogressbar;
	  sb:tstatusbar;

	  procedure popini;
	  //function send:boolean;

	 
	  procedure addmail(zeit:tdatetime;adressat:tstrings;betreff:string;mtext, anhang:tstrings);

	end;

var
	Form_email: TForm_email;

implementation

uses a_smtp;

{$R *.DFM}




procedure tform_email.SakSMTPEncodeStart(Sender: TObject; FileName: String;
	BytesCount: Integer);
begin
	pro1.Position := 0;
	Sb.Panels[0].Text := 'Encoding '+FileName+' of '+intToStr( BytesCount)+' bytes';
end;


procedure tform_email.popini;
begin

end;


procedure TForm_email.FormShow(Sender: TObject);
begin
//
end;


procedure tform_email.addmail(zeit:tdatetime;adressat:tstrings;betreff:string;mtext, anhang:tstrings);
var d:tdate;
begin
with datamodul do
begin
	d:=strtodate('01'+dateSeparator+'01'+dateSeparator+'1000');
	//if zeit=0 then Sb.Panels[0].Text := 'Mail-Ausgang';
	if zeit<>0 then neuer_datensatz(q_email,['nummer','datum','betreff','benutzer'],[null,zeit,betreff,user_id])
	else neuer_datensatz(q_email,['nummer','datum','betreff','benutzer'],[null,d,betreff,user_id]);

	form_main.strings_speichern(q_email,'mtext',mtext);
	form_main.strings_speichern(q_email,'adressat_liste',adressat);
	if adressat.count> 0 then q_email['adressat']:=adressat[0];
	if anhang.Count>0 then
	begin
		  form_main.datei_speichern(q_email,'anhang',anhang[0]);
		  q_email['anhang_dat']:=anhang[0]
	end;
	q_email.post;
	q_email.refresh;
end;
end;


procedure TForm_email.ToolButton5Click(Sender: TObject);
var
dat:string;
begin
with datamodul do
try
	 form_smtp:=tform_smtp.create(self);
   if q_email['adressat_liste']<>null then form_main.strings_laden(q_email,'adressat_liste',form_smtp.Memo_to.Lines);
   if q_email['mtext']<>null then form_main.strings_laden(q_email,'mtext',form_smtp.Memo.Lines);
	 if q_email['anhang_dat']<>null then dat:= q_email['anhang_dat'] else dat:='';
   if not fileexists(dat) then dat:='';
	 if dat<>'' then
	 begin
		 form_smtp.ComboBoxAttachedFiles.items.add(dat);
		 form_smtp.ComboBoxAttachedFiles.itemIndex := 0;
		 form_main.datei_laden(q_email,'anhang',dat);
	 end;
	 if q_email['betreff']<>null then form_smtp.EditSubject.Text:=q_email['betreff'];

	 form_smtp.ShowModal;
	 if fileexists( dat) then deletefile(dat);
finally
   form_smtp.Release;
end;
end;



procedure TForm_email.ToolButton_deleteClick(Sender: TObject);
var
i:integer;
nummern,query:string;
begin
if  (MessageDlg('Löschen der markierten Datensätze?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes ) then exit;
nummern:='0';
for i:= dbgridext.SelectedRows.count-1 downto 0 do
	begin
	  datamodul.q_email.GotoBookmark(tbookmark(dbgridext.SelectedRows.items[i]));
	  nummern:=nummern+ ','+getbigint_str(datamodul.q_email,'nummer');

	  //dateiloeschen( datamodul.q_email,true,false); //  datamodul.q_email.Delete;
	 end;
query:= format('delete from email where nummer in (%s)',[nummern]);
mysql_d.sql_exe(query);
datamodul.q_email.Refresh;

end;

procedure TForm_email.ToolButton1Click(Sender: TObject);
begin
ToolButton_cancelClick(Sender);
modalresult:=mrok;
end;

procedure tform_email.liste_filtern;
var query, whereuser,zeit:string;
begin
try
Screen.Cursor := crHourglass;
	if radiogroup_user.ItemIndex=0 then
		whereuser:='where benutzer="'+inttostr(user_id)+'"'
	else
		whereuser:='where benutzer>-1 ';

	case radiogroup_time.ItemIndex of
	0: zeit:= ' and datum> '+ sql_datetostr(date-7);
	1: zeit:= ' and datum> '+ sql_datetostr(date-31);
	2: zeit:= ' ';
	end;

	query:= 'select * from email '+whereuser ;
	//query:= 'select nummer,adressat_liste, datum, adressat, betreff, benutzer,anhang_dat, ts, storno from email '+whereuser ;

	if pagecontrol.ActivePage=tabsheet_ausgang then
		begin
		query:=query+' and datum = "1000-01-01" ';
		dbgridext.Columns.Items[0].width:=0;
		toolbutton_send.Enabled:=true;
		end
	else
		begin
			query:=query+zeit+' and datum <> "1000-01-01" ';
			dbgridext.Columns.Items[0].width:=120;
			toolbutton_send.Enabled:=false;
		end;
	application.ProcessMessages;

	if radiogroup_sort.ItemIndex=0 then
		query := query+ ' order by datum desc'
	else	query := query+ ' order by adressat';
	application.ProcessMessages;
	datamodul.sql(true,datamodul.q_email,query,'');
	application.ProcessMessages;
	grid_einstellen(dbgridext);
	application.ProcessMessages;
finally
 Screen.Cursor := crdefault;
end;
end;

procedure TForm_email.RadioGroup_sentClick(Sender: TObject);
begin
	 liste_filtern;
end;

procedure TForm_email.ToolButton_sendClick(Sender: TObject);

begin
	if pagecontrol.ActivePage<>tabsheet_ausgang then exit;
	if (MessageDlg('Sollen alle ausgewählten mails verschickt werden?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes ) then exit;

	send_silent;

end;

function tform_email.send_silent:boolean;
var
dat:string;
i:integer;
begin
try
	with datamodul do
	begin
	  //q_email.first;
	  sb:=statusbar;
	  //groupbox_send.Visible:=true;
	  form_smtp:=tform_smtp.create(self);

	  if not form_smtp.test_connect then
	  begin
		  showmessage('Keine Verbindung zum Mail-Server, evt. Passwort falsch');
		  exit;
	  end;

	  for i:=0 to dbgridext.selectedrows.Count-1 do
	  begin
       try
		  q_email.GotoBookmark(tbookmark(dbgridext.SelectedRows.items[i]));

	  //while not (q_email.Eof)  do
	  //begin
		  if q_email['adressat_liste']<>null then form_main.strings_laden(q_email,'adressat_liste',form_smtp.Memo_to.Lines);
		  if q_email['mtext']<>null then form_main.strings_laden(q_email,'mtext',form_smtp.Memo.Lines);
		  if q_email['anhang_dat']<>null then dat:= q_email['anhang_dat'] else dat:='';
		  if dat<>'' then
		  begin
			  form_smtp.ComboBoxAttachedFiles.items.add(dat);
			  form_smtp.ComboBoxAttachedFiles.itemIndex := 0;
			  form_main.datei_laden(q_email,'anhang',dat);
		  end;
		  if q_email['betreff']<>null then form_smtp.EditSubject.Text:=q_email['betreff'];

		  result:=form_smtp.send(false);

		  if fileexists( dat) then deletefile(dat);
		  //q_email.refresh;

		  if not result then exit;
       except
       // bei feherhaftem Bookmark
       end;
		end;
	  end;
finally
   datamodul.q_email.refresh;
	 form_smtp.Release;
	 //groupbox_send.Visible:=false;
end;
end;



procedure TForm_email.FormCreate(Sender: TObject);
begin
if prg_typ=2 then radiogroup_user.visible:=false;
zeit_popini:=time();
form_main.form_positionieren(tform(sender));
liste_filtern;
end;

procedure TForm_email.ToolButton_cancelClick(Sender: TObject);
begin
{form_email.SakSMTP.Canceled := true;
groupbox_send.Visible:=false;}
end;

procedure TForm_email.DBgridEXTDblClick(Sender: TObject);
begin
  ToolButton_sendClick(Sender);
end;

procedure TForm_email.ToolButton_neuClick(Sender: TObject);
begin
try
	form_smtp:=tform_smtp.create(self);
	form_smtp.ShowModal;
finally
	form_smtp.Release;
end;
end;

end.
