unit a_import;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, dbgridEXT, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db,
  DBTables, ImgList, ToolWin, CheckLst,zdataset, DBCtrls,math,a_report,shellapi,editor,Variants;

type
  TForm_importieren = class(TForm)
    PageControl: TPageControl;
    TabSheet_filter: TTabSheet;
    OpenDialog: TOpenDialog;
    Label_zaehler: TLabel;
    TabSheet_fehler: TTabSheet;
    RichEdit: TRichEdit;
    ToolBar1: TToolBar;
    ToolButton_close: TToolButton;
    Panel_links: TPanel;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    CheckListBox: TCheckListBox;
    Panel2: TPanel;
    BitBtn_html_export: TBitBtn;
    RadioGroup_ueberein: TRadioGroup;
    BitBtn1: TBitBtn;
    Label_ex: TLabel;
    Panel3: TPanel;
    RadioGroup_datum: TRadioGroup;
    GroupBox5: TGroupBox;
    Panel4: TPanel;
    BitBtn_import: TBitBtn;
	  BitBtn_abort: TBitBtn;
    ComboBox_was: TComboBox;
	  Panel5: TPanel;
	  GroupBox_email: TGroupBox;
	  Edit_email: TEdit;
    Label3: TLabel;
    GroupBox_familienstand: TGroupBox;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    Edit_fg: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Edit_fw: TEdit;
    Label8: TLabel;
    Edit_fh: TEdit;
    Label9: TLabel;
    Edit_fl: TEdit;
    ProgressBar: TProgressBar;
    Label_anzahl: TLabel;
    GroupBox_geschlecht: TGroupBox;
	  Label1: TLabel;
    Label2: TLabel;
    Edit_m: TEdit;
    Edit_w: TEdit;
    Label10: TLabel;
    GroupBox_ueberschreiben: TGroupBox;
    CheckBox_ueberschreiben: TCheckBox;
    CheckBox_archiv: TCheckBox;
    CheckBox_testdurchlauf: TCheckBox;
    Label11: TLabel;
    CheckBox_archiv_rest: TCheckBox;
    Label_archiv_all: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure  excel_ueberpruefen;
	  procedure ToolButton_closeClick(Sender: TObject);
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
	  procedure BitBtn_html_exportClick(Sender: TObject);
	  procedure BitBtn_abortClick(Sender: TObject);
	  procedure BitBtn_importClick(Sender: TObject);
	  procedure RadioGroup_wasClick(Sender: TObject);
	  procedure BitBtn1Click(Sender: TObject);
	  function  felderueberpruefen:boolean;
	  function  i_bediener(bediener:string):string;
	  function  i_impfstoff(impfstoff:string):string;
	  function  i_beurteilung(beurteilung:string):string;
    function i_zeitkonto(zeitkonto:string):string;
    function i_abrechnungskriterium(konto:string):string;
    function i_firmenkuerzel(kuerzel:string):string;
	private
	  { Private-Deklarationen }
	  F: TextFile;
	  abbruch:boolean;
	  tabelle: string;
	  tabelle_update:tzquery;
    temp_tab_p, temp_tab_n:string;
	  function datum_konvertieren(datum:string):string;
	  procedure import_excel;
	  procedure excel_oeffnen;
    procedure csv_oeffnen;
    procedure felder_checken;
	  procedure feldereinlesen(felder:tstringlist);
    function  memoadd(memo:tstrings;text:string):tstrings;
    procedure  filter_rest;
    procedure archivier_rest;
	public
	  { Public-Deklarationen }
	  mod_csv:boolean;
    procedure satztolist(var liste: tstringlist;s: string);
    procedure csv_einlesen;
    //constructor Create(AOwner: TComponent); override;

	end;
var
  Form_importieren: TForm_importieren;

implementation

uses a_main,  a_data, a_archiv_rest, a_csv_import, a_richedit_menue;

{$R *.DFM}




procedure TForm_importieren.import_excel;
var i,j,position,z,anzahl:integer;
feld,zeile,sname:string;
felder: tstringlist;
query:string;

function sonderzeichenraus(f:string):string;
var sz:string;
i,p:integer;
begin
  f:=trim(f);//Leerzeichen weg
  sz:='"~�^|�`'+chr(39);
  for i:=1 to 8 do
  f:=stringreplace(f,copy(sz,i,1),'',[rfreplaceall]);

  result:=f;

end;

//################################
begin //1
abbruch:=false;
if checklistbox.Items.Count<=0 then
begin
	 showmessage('Daten aus Excel haben falsches Format oder noch nicht verbunden');
	 exit;
end;
i:=0;


 case combobox_was.itemindex of
	0,5,6,7: i:=checklistbox.Items.IndexOf('kuerzel');
 end;
 if (i<0) and (radiogroup_ueberein.ItemIndex<>3) then //hier evtl auch ohne firma durchwinken
  begin
   case combobox_was.itemindex of
    0,5,6,7: showmessage('Die Spalte "kuerzel"  (Firmen-K�rzel) fehlt in der Excel-Tabelle');
   end;
  exit;
 end;


try
  query:=format('delete from %s',[temp_tab_p]);
  mysql_d.sql_exe(query);
  datamodul.q_5.Refresh;
  query:=format('delete from %s',[temp_tab_n]);
  mysql_d.sql_exe(query);
  datamodul.q_6.Refresh;
	felder:=tstringlist.Create;

	richedit.Lines.Add('######### Beginn Import#######');
  if checkbox_testdurchlauf.checked then richedit.Lines.Add('#####Testdurchlauf#####');
	 //Excel.Connect;
	 //com.xl_connect;
	 j:=2;
	 zeile:='-';

	 while (zeile<>'') do  //Anzahl
	 begin  //2
    if mod_csv then
    //zeile:=form_csv_import.StringGrid.Cells[j-1,0]
     zeile:=form_csv_import.StringGrid.Cells[0,j-1]
    else
	  zeile:=trim(com.excel_GetDataAt(j,1));
	  zeile:=copy(zeile,1,1);
	  inc(j);
	  label_anzahl.Caption:=inttostr(j);
	 end;
	 anzahl:=j-1;
	 progressbar.Max:=anzahl;

	 pagecontrol.ActivePage:=tabsheet_fehler;
	 j:=2;
	 zeile:='-';
	 while (zeile<>'') and (not abbruch) do  //Haupt-schleife
	 begin  //2

		label_zaehler.Caption:=inttostr( j);
		application.ProcessMessages;
		felder.clear;
		zeile:='';
		z:=0;
		for i:=1 to checklistbox.Items.Count do
		begin
			 //feld:=Excel.GetDataAtFileSheet(j,i,sname);
    if mod_csv then
      //feld:=form_csv_import.StringGrid.Cells[j-1,i-1]
      feld:=form_csv_import.StringGrid.Cells[i-1,j-1]
    else
			feld:=com.excel_GetDataAt(j,i);

			feld:=sonderzeichenraus(feld);
			felder.Add(feld); //g1,g2,g3.....
			zeile:=zeile+feld;
		end;
    //jetzt einlesen
		if zeile<>'' then feldereinlesen(felder);
		if z=0 then
			begin
				inc(j); //mit 2 geht�s los
				progressbar.Position:=j;
				application.ProcessMessages;
			end;
	 end; //2
     
finally
	 felder.Free;
   if checkbox_testdurchlauf.checked then richedit.Lines.Add('#####Ende Testdurchlauf#####');

   	richedit.Lines.Add('######### Ende Import#######');

	 //com.xl_disconnect
end;
end;  //1


procedure tform_importieren.csv_einlesen;
begin

 mod_csv:=true;


  edit_m.Text:=csv_m;
  edit_w.Text:=csv_w;
  edit_fl.Text:=csv_fl;
  edit_fh.Text:=csv_fh;
  edit_fg.Text:=csv_fg;
  edit_fw.Text:=csv_fw;
  radiogroup_datum.ItemIndex:=csv_datum;
  radiogroup_ueberein.ItemIndex:=csv_ueberein;
 checkbox_ueberschreiben.Checked:=csv_ueberschreiben;
 checkbox_archiv.Checked:=csv_archiv;
 checkbox_archiv_rest.checked:=csv_archiv_rest;

 csv_oeffnen;  //in liste eintragen
 import_excel;  //lesen aus grid
end;

procedure tform_importieren.feldereinlesen(felder:tstringlist);
var p,i,i_firma,i_branche,i_typ:integer;
s_datum,s_untersuchung,feldname,feldinhalt,s,sm,ft,typ_nummer,si,s1,s2,s3,s4:string;
feld:TField;
typ:TFieldType;
datum:string;
dat:tdate;
neu:boolean;
pfirma, pname, pvorname,pgebdat,pp_nummer,query,s_kuerzel: string;
 i_mitarbeiter,nnummer,fi_nr, ma_nr:int64;
 z:integer;
 liste:tstringlist;
 text:string;
 ps:integer;


procedure mitarbeiter_positivliste(i_mitarbeiter:int64);
begin
  if not datamodul.q_5.Locate('p_nummer',i_mitarbeiter,[]) then
  begin
    //hinzuf�gen  steht in q_1
    datamodul.q_5.last;
    datamodul.q_5.append;
    datamodul.q_5.FindField('f_nummer').AsString:=datamodul.q_1.FindField('i_firma').AsString;
    datamodul.q_5.FindField('p_nummer').AsString:=datamodul.q_1.FindField('nummer').AsString;
    datamodul.q_5.FindField('p_name').AsString:=datamodul.q_1.FindField('name').AsString;
    datamodul.q_5.FindField('p_vorname').AsString:=datamodul.q_1.FindField('vorname').AsString;
    datamodul.q_5.FindField('p_geb_dat').AsString:=datamodul.q_1.FindField('geb_dat').AsString;
    datamodul.q_5.FindField('f_nummer').AsString:=datamodul.q_1.FindField('i_firma').AsString;
    datamodul.q_5.Post;
  end;
end;
    //######################################################
begin
with datamodul do
begin
		if (combobox_was.itemindex in [0,1,2,3,4,5,7]  )  then  //das Feld firma bzw kuerzel muss beim Einlesen der Firma nicht drin sein!
		begin
			i:=checklistbox.Items.IndexOf('firma');
      if i<0 then  i:=checklistbox.Items.IndexOf('kuerzel');

			if (i=-1)  then exit; //firma immer notwendig
			case combobox_was.ItemIndex of
			  0:; //proband
			  1: sql_new(true,q_typ,'select * from typ where bereich=1','');
			  2: sql_new(true,q_typ,'select * from typ where bereich=2','');
			  3: sql_new(true,q_typ,'select * from typ where bereich=3','');
              4: ;//text  q_anamnese
			  5:;//abteilung
			  6:;//firma
              7:; //zeiten
			end;

			// hier zuordnung zu den Feldnamen
			i:=checklistbox.Items.IndexOf('name');
			if i>-1 then pname:=felder[i]  else pname:=''   ;
			i:=checklistbox.Items.IndexOf('vorname');
			if i>-1 then pvorname:=felder[i] else pvorname:='';
			i:=checklistbox.Items.IndexOf('geb_dat');
			if i>-1 then pgebdat:=datum_konvertieren(felder[i]) else pgebdat:='';
			i:=checklistbox.Items.IndexOf('p_nummer');
			if i>-1 then pp_nummer:=felder[i] else pp_nummer:='';

			i:=checklistbox.Items.IndexOf('firma');
      if i<0 then i:=checklistbox.Items.IndexOf('kuerzel');

			if q_3.locate('kuerzel',trim(copy(felder[i],1,32)) ,[loCaseInsensitive	]) then    //firma vorhanden
				begin
					i_firma:=datamodul.q_3.findfield('nummer').asinteger;
          //hier zur Firmenliste hinzuf�gen
          //fliste.add(inttostr(i_firma));
					i_branche:=datamodul.q_3.findfield('i_branche').asinteger;
					pfirma:=inttostr(i_firma);

					//if radiogroup_ueberein.ItemIndex=0 then
					case radiogroup_ueberein.ItemIndex of
						0: query:=format('select * from mitarbeiter where (i_firma="%s") and (name="%s") and ( vorname="%s") and (geb_dat="%s" )',[pfirma, pname, pvorname,dat_delphi_to_sql(pgebdat)]);
						1: query:=format('select * from mitarbeiter where (i_firma="%s") and (name="%s") and ( vorname="%s")',[pfirma, pname, pvorname]);
						2: query:=format('select * from mitarbeiter where (i_firma="%s") and trim(leading "0" from  p_nummer)=trim(leading "0" from "%s")',
												[pfirma, pp_nummer]);
            3:query:=format('select * from mitarbeiter where  (name="%s") and ( vorname="%s") and (geb_dat="%s" )',[ pname, pvorname,dat_delphi_to_sql(pgebdat)]);
					end;
          sql_new(true,q_1,query,'');
				end

		    else
		        begin
			     richedit.lines.Add(format(' %s nicht vorhanden  | %s, %s nicht eingelesen',[felder[i],pname,pvorname]));
			     exit;  //firma nicht gefunden
           end;
        end;

                  

      if combobox_was.ItemIndex=0 then tabelle_update:=datamodul.q_1
      else  tabelle_update:=datamodul.q_2;

      i_mitarbeiter:=getbigint(q_1,'nummer');

      //mitarbeiter_positivliste(i_mitarbeiter);

     // hier zur Probandenliste hinzuf�gen
     //pliste.add(inttostr(i_mitarbeiter));

		  case combobox_was.ItemIndex of //abh�ngige Tabellen
				  0:query:='';
				  1,2,3:
          begin
					 p:=checklistbox.Items.IndexOf('datum');
					 if p>-1 then s_datum:=datum_konvertieren(felder[ p]) else s_datum:='01.01.2000';
					 s_datum:=dat_delphi_to_sql(s_datum);
					 p:=checklistbox.Items.IndexOf('typ');
					 if p>-1 then s_untersuchung:=trim(felder[ p]) else
             begin
              s_untersuchung:='';
                richedit.lines.Add(format(  'Die Spalte typ existiert nicht, bitte in Excel verbessern ',[]));
                exit;
              end;
					 {if (s_untersuchung='') or ( not q_typ.Locate('untersuchung',s_untersuchung,[locaseinsensitive,loPartialKey])) then
					 begin
						richedit.lines.Add(format(  '"%s" existiert nicht, bitte neu anlegen oder in Excel verbessern ',[s_untersuchung]));
						exit;
					 end; }

					 s_untersuchung:=trim(s_untersuchung)+'%';//trim(copy(s_untersuchung,1,4))+'%';

					 //bestimme i_typ
					 query:=format('select nummer from typ where untersuchung like "%s" and storno=0 ',[s_untersuchung ]);
					 typ_nummer:=mysql_d.Feldinhalt(query,0);
           if typ_nummer='' then
           begin
            richedit.lines.Add(format(  '"%s" existiert nicht, bitte neu anlegen oder in Excel verbessern ',[s_untersuchung]));
            exit;
           end;

             case combobox_was.ItemIndex of
              1: query:=format('select * from untersuchung where (i_mitarbeiter=%d) and (datum="%s") and (i_typ="%s" ) and storno=0 ',[i_mitarbeiter,s_datum,typ_nummer ]);
              2: query:=format('select * from impfung  where (i_mitarbeiter=%d) and (datum="%s") and (i_typ = "%s" ) and storno=0 ',[i_mitarbeiter,s_datum,typ_nummer ]);
              3: query:=format('select * from labor where (i_mitarbeiter=%d) and (datum="%s") and (i_typ="%s" ) and storno=0 ',[i_mitarbeiter,s_datum,typ_nummer ]);
             end;
           end;
           4: begin
                 query:=format('select * from anamnese where (nummer=%d)  and storno=0 ',[i_mitarbeiter ]);
               end;
				  5:begin //7

						 p:=checklistbox.Items.IndexOf('abteilung_kuerzel');
						 if p>-1 then s_kuerzel:=trim(felder[p]) else s_kuerzel:='';

						 query:=format('select * from abteilung where kuerzel ="%s" and i_firma=%s',[s_kuerzel,pfirma]);
					 end;
				  6: begin
						 i:=checklistbox.Items.IndexOf('kuerzel');
             if i=-1 then
             begin
              showmessage('Das notwendige Feld "kuerzel" existiert nicht, (muss gef�llt sein)') ;
                exit;
                BitBtn_abortClick(self);
             end;
						 s_kuerzel:=trim(felder[i]);
             s_kuerzel:=copy(s_kuerzel,1,32);
             s_kuerzel:=s_kuerzel;
						 query:=format('select * from firma where kuerzel="%s" and storno=0 ',[s_kuerzel]);
						end;
           7: begin
                 i:=checklistbox.Items.IndexOf('kriterium');
                 s1:=trim(felder[i]);
                 s1:=i_abrechnungskriterium(s1);

                 i:=checklistbox.Items.IndexOf('konto');
                 s2:=trim(felder[i]);
                 s2:=i_zeitkonto(s2);


                 i:=checklistbox.Items.IndexOf('von');
                 s3:=trim(felder[i]);
                 s3:=dat_delphi_to_sql(s3);
                 i:=checklistbox.Items.IndexOf('bis');
                 s4:=trim(felder[i]);
                 s4:=dat_delphi_to_sql(s4);
                 query:=format('select * from firma_zeiten_soll where i_firma=%d and von="%s" and bis="%s" and i_zeitkonto=%s and i_abrechnungskriterium=%s and storno=0 ',[i_firma,s3,s4,s2,s1]);
              end;

				end;

				if query<>'' then sql_new(false,q_2,query,'');

				neu:=false;
        //neuer datensatz
        //if not checkbox_testdurchlauf.checked then
        case combobox_was.ItemIndex of //
				  0: if kein_datensatz(q_1) then
						 begin
							if (radiogroup_ueberein.ItemIndex=2)
									and ((checklistbox.Items.IndexOf('name')<0) or (checklistbox.Items.IndexOf('vorname')<0)or (checklistbox.Items.IndexOf('geb_dat')<0)) then exit;//personalnummer reicht nicht zum neuanlegen
              if (radiogroup_ueberein.ItemIndex=3)
									and ((checklistbox.Items.IndexOf('name')<0) or (checklistbox.Items.IndexOf('vorname')<0) or (checklistbox.Items.IndexOf('geb_dat')<0) or (checklistbox.Items.IndexOf('kuerzel')<0 ))then exit;//personalnummer reicht nicht zum neuanlegen
             if not checkbox_testdurchlauf.checked then
             begin
							nnummer:=neuer_datensatz (tabelle_update,['nummer','i_firma','ende'],[null,i_firma,'2999/12/31']);
							tabelle_update.post;
             end;
							query:=format('select * from mitarbeiter where nummer= %d',[nnummer]);
							sql_new(true,q_1,query,'');
							neu:=true;
						 end;
				  1: if kein_datensatz(q_2) then
						begin
							if datensatz(q_1) then
							begin
               if not checkbox_testdurchlauf.checked then
                begin
                  nnummer:=neuer_datensatz(tabelle_update,['nummer','i_mitarbeiter'],[null,inttostr(i_mitarbeiter)]);
                  tabelle_update.post;
                end;
								query:=format('select * from untersuchung where (nummer=%d)',[nnummer]);
								sql(true,q_2,query,'');
								neu:=true;
							end
							else
							begin
								richedit.lines.Add(format(  '%s , %s ,%s existiert nicht, bitte zuerst MA importieren ',[pname,pvorname,pp_nummer]));
								exit;
							end;
						end;
				  2: if kein_datensatz(q_2) then
						begin
							if datensatz(q_1) then
							begin
                if not checkbox_testdurchlauf.checked then
               begin
								nnummer:=neuer_datensatz(tabelle_update,['nummer','i_mitarbeiter'],[null,inttostr(i_mitarbeiter)]);
								tabelle_update.post;
               end;
								query:=format('select * from impfung where (nummer=%d)',[nnummer]);
								sql(true,q_2,query,'');
								neu:=true;
							end
							else
							begin
								richedit.lines.Add(format(  '%s , %s, %s existiert nicht, bitte zuerst MA importieren ',[pname,pvorname,pp_nummer]));
								exit;
							end;
						end;
				  3: if kein_datensatz(q_2) then
						begin
							if datensatz(q_1) then
							begin
                if not checkbox_testdurchlauf.checked then
                begin
                  nnummer:=neuer_datensatz(tabelle_update,['nummer','i_mitarbeiter'],[null,inttostr(i_mitarbeiter)]);
                  tabelle_update.post;
                end;
								query:=format('select * from labor where (nummer=%d)',[nnummer]);
								sql(true,q_2,query,'');
								neu:=true;
							end
							else
							begin
								richedit.lines.Add(format(  '%s , %s, %s existiert nicht, bitte zuerst MA importieren ',[pname,pvorname,pp_nummer]));
								exit;
							end;
						end;
          4: begin //7
              if not datensatz(q_1) then //mitarbeiter vorhanden
							begin
								richedit.lines.Add(format(  '%s , %s, %s existiert nicht, bitte zuerst MA importieren ',[pname,pvorname,pp_nummer]));
								//showmessage('der einzulesende Mitarbeiter existiert nicht, bitte zuerst Mitarbeiter importieren.');
								exit;
							end;
              if kein_datensatz(q_2) then //anamnese nicht vorhanden neu anlegen
                try
                  if not checkbox_testdurchlauf.checked then
                  begin
                    neuer_datensatz(tabelle_update,['nummer'],[inttostr(i_mitarbeiter)]);
                    tabelle_update.post;
                  end;
                  liste:=tstringlist.Create;
                  liste.Add('{\rtf1 }');
                  form_main.strings_speichern(tabelle_update,'memo_blob',liste);
                  neu:=true;
                finally
                  liste.free;
                end;

              end;


				  5: begin //7
						if kein_datensatz(q_2) then //abteilung nicht vorhanden neu anlegen
							begin
                if not checkbox_testdurchlauf.checked then
                begin
                  neuer_datensatz(tabelle_update,['nummer','i_firma'],[null,i_firma]);
                  tabelle_update.post;
                end;
							  neu:=true;
							end;

						 if datensatz(q_1) then //mitarbeiter vorhanden
							begin
                if not checkbox_testdurchlauf.checked then
                begin
                  tabelle_update.edit;
                  setbigint(tabelle_update,'i_mitarbeiter',i_mitarbeiter);
                  tabelle_update['kuerzel']:=s_kuerzel;
                  tabelle_update['name']:=q_1['name'];
                  tabelle_update['vorname']:=q_1['vorname'];
                  tabelle_update.post;

                  s:=format('%s.%s%s',[ trim(q_1['vorname']),trim(q_1['name']),trim(edit_email.text)]);
                  p:=checklistbox.Items.IndexOf('abteilung');
                  if p>-1 then s_kuerzel:=felder[p] else s_kuerzel:='';
                  q_1.edit;
                    if trim(edit_email.text)<>'' then   q_1['email_f']:=s;
                    q_1['abteilung']:=s_kuerzel;
                    direktspeichern:=true;
                  q_1.post;
                 end;
							end
							else
							begin
                if not checkbox_testdurchlauf.checked then
                begin
                  tabelle_update.edit;
                  tabelle_update['kuerzel']:=s_kuerzel;
                  tabelle_update.post;
                end;
								richedit.lines.Add(format(  'Abteilungsvorgesetzter (Mitarbeiter) %s , %s, %s existiert nicht ',[pname,pvorname,pp_nummer]));
								//showmessage('der einzulesende Mitarbeiter existiert nicht, bitte zuerst Mitarbeiter importieren.');
								
							end;
						  end;
				     6: begin //firma
						if kein_datensatz(q_2) then //firma nicht vorhanden neu anlegen
							begin
                if not checkbox_testdurchlauf.checked then
                begin
                  fi_nr:=neuer_datensatz(tabelle_update,['nummer'],[null]);
                  ma_nr:=neuer_datensatz(datamodul.q_mitarbeiter,['nummer','i_firma','name','archiv','ende','storno'],[null,inttostr(fi_nr),'dummy','-','2999/12/31','0']);

                  direktspeichern:=true;
                  datamodul.q_mitarbeiter.post;
                end;
                query:=format('update mitarbeiter set storno=1 where nummer=%s',[inttostr(ma_nr)]);
              mysql_d.sql_exe(query);


							    neu:=true;
							end;

					  end;
            7: begin //firma_zeitne
						if kein_datensatz(q_2) then //firma nicht vorhanden neu anlegen
							begin
                if not checkbox_testdurchlauf.checked then
                begin
                  nnummer:=neuer_datensatz(tabelle_update,['nummer','i_firma'],[null,i_firma]);
                  tabelle_update.post;
                end;
                direktspeichern:=true;
				 				neu:=true;
							end;

					  end;
				end;


				if  (neu or (checkbox_ueberschreiben.Checked)) or (not checkbox_testdurchlauf.Checked) then
				begin //4 main
            	if not checkbox_testdurchlauf.checked then tabelle_update.Edit;
					if combobox_was.itemindex<=6 then
					begin
						case combobox_was.itemindex of
						  0:sm:='';
						  1:sm:=format(' Vorsorge "%s" ',[s_untersuchung]);
						  2:sm:=format(' Impfung "%s"',[s_untersuchung]);
						  3:sm:=format(' Labor "%s"',[s_untersuchung]);
						  5:sm:=format(' Abteilung "%s"',[s_kuerzel]);
						  6:sm:=format(' Firmenk�rzel "%s"',[s_kuerzel]);
						end;

						//if neu then richedit.lines.Add('NEU    |'+pname+' | '+ pvorname+' | '+pp_nummer+sm)
						//else  richedit.lines.Add('UPDATE |'+pname+' | '+ pvorname+' | '+pp_nummer+sm);
            if neu then richedit.lines.Add('NEU    |')
						else  richedit.lines.Add('UPDATE |');
					end
					else
						//if neu then richedit.lines.Add('NEU    |'+felder[0])
             //     else  richedit.lines.Add('UPDATE |'+felder[0]);
             if neu then richedit.lines.Add('NEU    |')
             else  richedit.lines.Add('UPDATE |');

					if not checkbox_testdurchlauf.checked then
          begin
            tabelle_update.edit;
					  if combobox_was.ItemIndex=0 then if checkbox_archiv.checked then tabelle_update['archiv']:='-';
           end;

       //################
        for i:=0 to checklistbox.Items.Count-1 do
        begin  //4
        try
         feldname:= lowercase( checklistbox.Items[i]);
         if (feldname<>'nummer') and (feldname<>'firma')  and checklistbox.Checked[i] then
         begin  //5
          if (feldname='kuerzel') and (combobox_was.ItemIndex<>6) then  //damit �berall k�rzel gilt
						begin
							typ:=ftstring;
							feldname:='firma';
						end;

          //lesen  ##########################
          feldinhalt:= felder[i];
					feld:=tabelle_update.FindField(feldname);

          //########################

            if feld<>nil then typ:=feld.DataType else typ:=ftUnknown	;

					 if (feldname='firmenname') and (combobox_was.ItemIndex=6) then
						begin
							typ:=ftstring;
							feldname:='firma';
						end;

           if (feldname='firma') and (combobox_was.ItemIndex=0) and (radiogroup_ueberein.ItemIndex=3) then  //proband neuer firma zuordnen
          begin
          //kuerzel wurde zu firma ge�ndert
            typ:=ftinteger;
            feldname:='i_firma';
            feld:=tabelle_update.FindField(feldname);
            feldinhalt:=i_firmenkuerzel(feldinhalt);
          end;


            if feldname='einsatzstunden' then
						begin
							typ:=ftstring;
							feldname:='einsatzstunden';
						end;
            if feldname='von' then
						begin
							typ:=ftdate;
							feldname:='von';
						end;
            if feldname='bis' then
						begin
							typ:=ftdate;
							feldname:='bis';
						end;
            if feldname='konto' then
						begin
							typ:=ftstring;
							feldname:='i_zeitkonto';
              feldinhalt:=i_zeitkonto(feldinhalt);
						end;
            if feldname='kriterium' then
						begin
                  typ:=ftstring;
                  feldname:='i_abrechnungskriterium';
                  feldinhalt:=i_abrechnungskriterium(feldinhalt);
                  end;
            if feldname='konto1' then
						begin
							typ:=ftstring;
							feldname:='zeitkonto';

						end;
            if feldname='kriterium1' then
						begin
							typ:=ftstring;
							feldname:='abrechnungskriterium';

                  end;
            if feldname='stunden' then
						begin
							typ:=ftstring;
							feldname:='einsatzstunden';
						end;


						if feldname='anmerkung' then
						begin
							typ:=ftstring;
                     //typ:=ftmemo;
							feldname:='memo';
							//feldinhalt:=i_impfstoff(feldinhalt );
						end;

						if feldname='art' then
						begin
							typ:=ftinteger;
							feldname:='i_uart';
							ft:=feldinhalt;
							feldinhalt:='1';
							if ft='2'then feldinhalt:='2';
							if ft='3'then feldinhalt:='3';
						end;



						if feldname='arzt' then
						begin
							typ:=ftinteger;
							feldname:='i_untersucher';
							ft:=i_bediener(feldinhalt );
							if ft='-1' then  richedit.lines.Add(format('Arzt / Mitarbeiter  " %s" nicht vorhanden',[feldinhalt]));
							feldinhalt:=ft;
						end;

						if feldname='beurteilung' then
						begin
							typ:=ftinteger;
							feldname:='i_beurteilung';
							ft:=i_beurteilung(feldinhalt);
							if ft='-1' then  richedit.lines.Add(format('Beurteilung " %s" nicht vorhanden',[feldinhalt]));
							feldinhalt:=ft;
						end;

						if feldname='charge' then
						begin
							typ:=ftstring;
							feldname:='charge';
							//feldinhalt:=i_impfstoff(feldinhalt );
						end;

						if feldname='dosis' then
						begin
							typ:=ftstring;
							feldname:='dosis';
							//feldinhalt:=i_impfstoff(feldinhalt );
						end;

            if feldname='ende' then    //datum
            begin
              typ:=ftstring;
              feldname:='ende';
              if trim(feldinhalt)='' then feldinhalt:= '31.12.2999';
              feldinhalt:= datum_konvertieren(feldinhalt);
              try
                 if strtodate(feldinhalt)>strtodate('31.12.2099') then feldinhalt:= '31.12.2999';
                 if strtodate(feldinhalt)<strtodate('31.12.1900') then feldinhalt:= '31.12.2999';
              except
                feldinhalt:='31.12.2999';
              end;

            end;


            if feldname='beginn' then    //datum
            begin
              typ:=ftstring;
              feldname:='beginn';
              if trim(feldinhalt)='' then feldinhalt:= '01.01.2000';
              feldinhalt:= datum_konvertieren(feldinhalt);
              try
                 if strtodate(feldinhalt)>strtodate('31.12.2099') then feldinhalt:= '01.01.2000';
                if strtodate(feldinhalt)<strtodate('31.12.1900') then feldinhalt:= '01.01.2000';
              except
                feldinhalt:='01.01.2000';
              end;

            end;

						if feldname='ergebnistext' then
						begin
							typ:=ftstring;
							feldname:='ergebnistext';
							//feldinhalt:=i_impfstoff(feldinhalt );
						end;

						if feldname='familienstand' then
						begin
							typ:=ftinteger;
							feldname:='i_famstand';

							if lowercase(feldinhalt)=edit_fl.text then feldinhalt:='1' else
							if lowercase(feldinhalt)=edit_fh.text then feldinhalt:='2' else
							if lowercase(feldinhalt)=edit_fg.text then feldinhalt:='3' else
							if lowercase(feldinhalt)=edit_fw.text then feldinhalt:='4' else
							feldinhalt:='0';
						end;

						if feldname='geschlecht' then
						begin
							typ:=ftstring;
							if lowercase(feldinhalt)=edit_m.text then feldinhalt:='2'
							else
							if lowercase(feldinhalt)=edit_w.text then feldinhalt:='1' else feldinhalt:='0';
						end;

						if feldname='impfabfolge' then
						begin
							typ:=ftstring;
							feldname:='i_impfabfolge';
							ft:=feldinhalt;
							feldinhalt:='1';
							if ft='2'then feldinhalt:='2';
							if ft='3'then feldinhalt:='3';
							if ft='4'then feldinhalt:='4';
							if ft='5'then feldinhalt:='5';
							if ft='6'then feldinhalt:='6';
							if ft='7'then feldinhalt:='7';
						end;


						if feldname='impfstoff' then
						begin
							typ:=ftinteger;
							feldname:='i_stoff';
							ft:=i_impfstoff(feldinhalt );
							if ft='-1' then  richedit.lines.Add(format('Impfstoff " %s" nicht vorhanden',[feldinhalt]));
							feldinhalt:=ft;
						end;

						if feldname='nation' then
						begin
							typ:=ftinteger;
							feldname:='i_nation';
							ft:=inttostr(form_main.nationalitaet(feldinhalt ));
							if ft='-1' then  richedit.lines.Add(format('Nation " %s" nicht vorhanden',[feldinhalt]));
							feldinhalt:=ft;
						end;

            if feldname='pflicht_angebot_wunsch' then
						begin
							typ:=ftinteger;
							feldname:='i_paw';
							ft:=feldinhalt;
							feldinhalt:='1';
							if ft='2'then feldinhalt:='2';
							if ft='3'then feldinhalt:='3';
						end;

						if feldname='schicht' then
						begin
							feldname:='i_schichtmodell';
							feldinhalt:=trim(feldinhalt);
							typ:=ftinteger;
							query:=format('select nummer from schicht where schicht_kuerzel="%s"  ',[feldinhalt]);
							ft:=mysql_d.Feldinhalt(query,0);
							if ft='' then
							begin
								richedit.lines.Add(format('Schichtmodell " %s" nicht vorhanden',[feldinhalt]));
								ft:='0';
							end;
							feldinhalt:=ft;
						end;

						if feldname='status' then
						begin
							typ:=ftinteger;
							feldname:='i_status';
							ft:=feldinhalt;
							feldinhalt:='1';
							if ft='2'then feldinhalt:='2';
							if ft='3'then feldinhalt:='3';
							if ft='4'then feldinhalt:='4';
						end;


						if feldname='taetigkeit' then
						begin
							typ:=ftLargeInt;
							feldname:='i_taetigkeit';
							feldinhalt:=inttostr(form_main.i_taetigkeit(i_branche,feldinhalt));
						end;

						if feldname='abteilung' then     // nur mitarbeiter
						begin
							typ:=ftLargeInt;
							feldname:='i_abteilung';
							feldinhalt:=inttostr(form_main.i_abteilung(i_firma,feldinhalt,combobox_was.ItemIndex));
						end;

                  
            if feldname='abteilung_lang' then
						begin
							typ:=ftstring;
							feldname:='langtext';

						end;

						if feldname='testbezogenerhinweis' then
						begin
							typ:=ftstring;
							feldname:='testbezogenerhinweis';
						end;
            if feldname='grenzwertindikator' then
						begin
							typ:=ftstring;
							feldname:='grenzwertindikator';
						end;

            if feldname='einheit' then
						begin
							typ:=ftstring;
							feldname:='einheit';
						end;

            if feldname='norm' then
						begin
							typ:=ftstring;
							feldname:='norm';
						end;
            if feldname='goae' then
						begin
							typ:=ftstring;
							feldname:='goae';
						end;

						if feldname='typ' then
						begin
							feldname:='i_typ';
							feldinhalt:=trim(feldinhalt); //trim(copy(feldinhalt,1,4));
							typ:=ftinteger;
							if typ_nummer<>'' then feldinhalt:=typ_nummer
							else
                    		begin
							  richedit.lines.Add(format('Untersuchung/Impfung/Labor " %s" nicht vorhanden',[feldinhalt]));
							  feldinhalt:='-1';
							end;

							{if q_typ.Locate('untersuchung',feldinhalt,[locaseinsensitive,loPartialKey]) then feldinhalt:=inttostr(q_typ['nummer'])
							  else
							begin
							  richedit.lines.Add(format('Untersuchung/Impfung/Labor " %s" nicht vorhanden',[feldinhalt]));
							  feldinhalt:='-1';
							end;}

						end;

						if feldname='wert' then
						begin
							typ:=ftstring;
							feldname:='wert';
							//feldinhalt:=i_impfstoff(feldinhalt );

						end;


            if feldname='anamnese' then
						begin
							typ:=ftblob;
							feldname:='memo_blob';
							//feldinhalt:=i_impfstoff(feldinhalt );

						end;

           //EINLESEN
            //richedit.Enabled:=false;
						case typ of  //6
							ftString  :
                    begin
                      si:=form_main.umlaut_convert(feldinhalt);
                      if not checkbox_testdurchlauf.checked then if si<>'' then tabelle_update.FindField(feldname).asstring:=si;
                      richedit.text:=richedit.Text+'|'+si;
                    end;
							ftSmallint, ftinteger :
                    begin
                      if feldinhalt<>'' then
                      if not checkbox_testdurchlauf.checked then tabelle_update.FindField(feldname).asstring:=feldinhalt;
                      richedit.text:=richedit.Text+'|'+feldinhalt;
                    end;
              ftLargeInt:
                    begin
                      if not checkbox_testdurchlauf.checked then setbigint(tabelle_update,feldname,strtoint64(feldinhalt));
                      richedit.text:=richedit.Text+'|'+feldinhalt;
                     end;
							ftDate,ftdatetime:
								  begin
									  datum:=datum_konvertieren(feldinhalt);
									  dat:=strtodate(datum);
									  if not checkbox_testdurchlauf.checked then tabelle_update[feldname]:=dat;
                    richedit.text:=richedit.Text+'|'+datum;
									end;
              ftblob: begin
                           try
                           liste:=tstringlist.Create;


                           form_main.strings_laden(tabelle_update,feldname,liste);
                           if liste.count>0 then
                           begin
                            text:=liste[liste.count-1];
                              text:=stringwenden(text);
                              ps:=pos('}',text);
                              if ps>0 then text:=copy(text,ps+1,length(text));
                              text:=stringwenden(text);

                              liste[liste.count-1]:=text;
                           end;
                           feldinhalt:=stringreplace(feldinhalt,#$A,'\par ',[rfReplaceAll]);
                           liste.Add(feldinhalt);

                          liste.add('\par }');
                           if not checkbox_testdurchlauf.checked then form_main.strings_speichern(tabelle_update,feldname,liste);
                           finally
                            liste.free;
                           end;
                           //if (liste.count>0) then richedit.text:=richedit.Text+'|'+liste[1]+'...';
                        end;
                      ftmemo: begin   //eigentlich nicht mehr n�tig, sollte als string behandelt werden  08.01.17
                           try
                           text:= tabelle_update.FindField(feldname).asstring;
                           //liste:=tstringlist.Create;


                           //form_main.strings_laden(tabelle_update,feldname,liste);
                           if text<>'' then
                           begin

                              text:=stringwenden(text);
                              ps:=pos('}',text);
                              if ps>0 then text:=copy(text,ps+1,length(text));
                              text:=stringwenden(text);


                           end
                           else text:='{\rtf1\'; //bisher leer

                           feldinhalt:=stringreplace(feldinhalt,#$A,'\par ',[rfReplaceAll]);
                           text:=text+ feldinhalt;

                           text:=text+'\par }';

                           finally
                           if not checkbox_testdurchlauf.checked then tabelle_update.FindField(feldname).asstring:=text;
                           richedit.text:=richedit.Text+'|'+copy(text,1,20)+'...';

                           end;
                    end;
						end; //6
            //richedit.Enabled:=true;
            richedit.HideSelection:=false;
            RichEdit.SelStart:=length(RichEdit.text);
            application.ProcessMessages;
                        //end;//5a
						end; //5
						except //fehler beim einlesen eines feldes
							on E: Exception do
								 try
									 richedit.lines.Add (E.Message);
								finally
								end;
						end;
            end; //4      ende schleife

            if not checkbox_testdurchlauf.checked then
            begin
              if combobox_was.ItemIndex=0 then tabelle_update['s_name']:=soundex.soundex( q_1['name']);
              direktspeichern:=true;
              tabelle_update.post;
            end;
            if combobox_was.ItemIndex=0 then   //probanden die eingelesen werden
            begin
              i_mitarbeiter:=getbigint(q_1,'nummer');
              if i_mitarbeiter<>0 then mitarbeiter_positivliste(i_mitarbeiter);
            end;
					end;//4 main
                    //form_main.abteilungskuerzel_eintragen;
	end;
end;


procedure TForm_importieren.FormCreate(Sender: TObject);
var
i:integer;
query:string;
begin
  
  //if modus='csv' then mod_csv:=true else mod_csv:=false;
	form_main.form_positionieren(tform(sender));
  combobox_was.ItemIndex:=0;
	RadioGroup_wasClick(Sender);
	datamodul.sql_new(true,datamodul.q_3,'select * from firma', '');
  datamodul.sql_new(true,datamodul.q_4,'select * from status', '');
	RadioGroup_wasClick(sender);
  //temp tabelle  firmennummer, firmenname, probandnr,proband vname, proband_nname, proband_gebdat,
	//tabelle:='mitarbeiter';
  temp_tab_p:=format('temp_tab_%d_imp_p',[user_id]);
  query:=format('drop table if exists %s',[temp_tab_p]);
  mysql_d.sql_exe(query);
  query:= format('create table  %s (nummer int(11) not null auto_increment  primary key, f_nummer int(11), f_name char(50),p_nummer bigint(19) , p_name char(50), p_vorname char(50),p_geb_dat date, KEY p_nummer (p_nummer),KEY f_nummer (f_nummer))',[temp_tab_p]);
  mysql_d.sql_exe(query);
  query:=format('select * from %s',[temp_tab_p]);
  datamodul.sql( false,datamodul.q_5,query,'');

  temp_tab_n:=format('temp_tab_%d_imp_n',[user_id]);
  query:=format('drop table if exists %s',[temp_tab_n]);
  mysql_d.sql_exe(query);
  query:= format('create table  %s (nummer int(11) not null auto_increment  primary key, f_nummer int(11), f_name char(50),p_nummer bigint(19), p_name char(50), p_vorname char(50),p_geb_dat date,KEY p_nummer (p_nummer),KEY f_nummer (f_nummer))',[temp_tab_n]);
  mysql_d.sql_exe(query);
  query:=format('select * from %s order by f_name, p_name,p_vorname, p_geb_dat',[temp_tab_n]);
  datamodul.sql( false,datamodul.q_6,query,'');
  

end;

procedure tform_importieren.excel_ueberpruefen;
var
i:integer;
begin
 if not mod_csv then
  try
      //l�uft excel?
      i:=excel.Workbooks.Count;
      i:=1+1;
  except
         com.excel_connect('Import');
         com.excel_vordergrund;
 end;
end;


function tform_importieren.datum_konvertieren(datum:string):string;    //delphi-format
var
d:string;
dat:tdate;
begin
if datum<>'' then
begin
	 case radiogroup_datum.ItemIndex of
     	0: begin //01.12.1999
						if length(datum)=9 then datum:='0'+ datum;
				  end;
          1:begin //01/12/1999
						if length(datum)=9 then datum:='0'+ datum;
						delete(datum,3,1);
						delete(datum,5,1);
						insert('.',datum,3);
					  insert('.',datum,6);
              end;
				2: begin   //01121999
					  if length(datum)=7 then datum:='0'+ datum;
					  insert('.',datum,3);
					  insert('.',datum,6);
				  end;
				3: begin  //19991201
					  d:=copy(datum,1,4);
					  d:=copy(datum,5,2)+'.'+d;
					  d:=copy(datum,7,2)+'.'+d;
					  datum:=d;
					end;
        4: begin
             dat:=strtoint(datum);
             datum:=datetostr(dat);
           end;
			  end;
	 result:=datum;
end
else result:='01.01.1900';
end;


procedure TForm_importieren.ToolButton_closeClick(Sender: TObject);
begin
modalresult:=mrok;
end;


procedure TForm_importieren.satztolist(var liste: tstringlist;s: string);
var
position:integer;
ts:string;
begin
	  liste.clear;
    position:=pos(chr(9),s);
    while position > 0 do
    begin
     ts:=trim(copy(s,1,position-1));
     s:=copy(s,position+1,length(s));
		liste.Add(ts);
	   position:=pos(chr(9),s);
    end;
    if trim(s)<>'' then  liste.Add(s);
end;

procedure TForm_importieren.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
query:string;
begin
  form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
 form_richedit_menue.Parent:=nil;
 
	datamodul.sql_new(true,datamodul.q_typ,'select * from typ ',' reihenfolge ');
	datamodul.q_mitarbeiter.refresh;
	datamodul.q_mitarbeiter_such.refresh;
	datamodul.q_untersuchung.Refresh;
  query:=format('drop table if exists %s',[temp_tab_p]);
  mysql_d.sql_exe(query);
  query:=format('drop table if exists %s',[temp_tab_n]);
  mysql_d.sql_exe(query);
  datamodul.q_5.SQL.Text:='';
  datamodul.q_6.sql.text:='';

end;

procedure TForm_importieren.BitBtn_html_exportClick(Sender: TObject);
var
f_name, html_name:string;
re:thtml_report;
i:integer;
begin
try
  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.11', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
	re:=thtml_report.create(f_name);
  re.tag('h2','�bersicht Import');
  re.zeile('');
  for i:=0 to richedit.Lines.Count-1 do
  begin
    re.zeile(richedit.Lines[i]);
  end;
finally
	re.free;
end;
	html_name:='file:\\'+f_name;
	shellexecute_se(0,'open',pchar(f_name),'','',sw_shownormal);

end;

procedure TForm_importieren.BitBtn_abortClick(Sender: TObject);
begin
progressbar.Position:=0;
abbruch:=true;
end;

procedure TForm_importieren.BitBtn_importClick(Sender: TObject);
begin
   if not ist_hauptsystem then
   begin
     if  combobox_was.itemindex =7 then
     begin
      showmessage ('Import ist nur auf dem Hauptsystem m�glich');
      exit;
     end;
   end;
	  excel_oeffnen;

	if felderueberpruefen=false then exit;



	 bitbtn_abort.Visible:=true;
	 import_excel;
	 bitbtn_abort.Visible:=false;

   if checkbox_archiv_rest.Checked then  //alles was nicht importiert wurde wird archiviert
   begin
     try
     form_archiv_rest:=tform_archiv_rest.create(self);
     if checkbox_testdurchlauf.Checked then form_archiv_rest.label1.Caption:='Nur Testdurchlauf, die Probanden werden nicht archiviert.';

     filter_rest;
     if (form_archiv_rest.ShowModal=mrok) and (not checkbox_testdurchlauf.Checked) then archivier_rest;

     finally
     form_archiv_rest.Release;
     end;
   end;


   case  combobox_was.itemindex of
   1: datamodul.q_untersuchung.Refresh;
   2: datamodul.q_impfung.Refresh;
   3:datamodul.q_labor.Refresh;
   4: datamodul.q_anamnese.Refresh;
   5:datamodul.q_abteilung.Refresh;
   6: showmessage('F�r die importierten Firmen m�ssen noch die Berechtigungen erteilt werden  ->Einstellungen->Berechtigungen ->Ber. Firmen' );
   end;
   refresh_all;
   form_main.berechtigung_firma;
end;



procedure TForm_importieren.RadioGroup_wasClick(Sender: TObject);
var query:string;
begin
radiogroup_ueberein.Visible:=true;
radiogroup_datum.Visible:=true;
checkbox_archiv.Visible:=true;
CheckBox_archiv_rest.visible:=true;
Label_archiv_all.visible:=true;
groupbox_email.Visible:=true;
groupbox_geschlecht.visible:=true;
GroupBox_familienstand.visible:=true;
bitbtn_import.Enabled:=false;

case combobox_was.ItemIndex of      //nicth h�bsch aber es tut
	0: begin
			tabelle:='mitarbeiter';
			groupbox_email.Visible:=false;
		end;
	1: begin
			tabelle:='untersuchung';
			checkbox_archiv.Visible:=false;
      CheckBox_archiv_rest.visible:=false;
      Label_archiv_all.visible:=false;
			groupbox_email.Visible:=false;
			groupbox_geschlecht.visible:=false;
			GroupBox_familienstand.visible:=false;

		end;
	2: begin
			tabelle:='impfung';
			checkbox_archiv.Visible:=false;
      CheckBox_archiv_rest.visible:=false;
      Label_archiv_all.visible:=false;
			groupbox_email.Visible:=false;
			groupbox_geschlecht.visible:=false;
			GroupBox_familienstand.visible:=false;
		end;
	3: begin
			tabelle:='labor';
			checkbox_archiv.Visible:=false;
      CheckBox_archiv_rest.visible:=false;
      Label_archiv_all.visible:=false;
			groupbox_email.Visible:=false;
			groupbox_geschlecht.visible:=false;
			GroupBox_familienstand.visible:=false;
		end;
    4: begin //freitext
      tabelle:='anamnese';
      checkbox_archiv.Visible:=false;
      CheckBox_archiv_rest.visible:=false;
      Label_archiv_all.visible:=false;
			groupbox_email.Visible:=false;
			groupbox_geschlecht.visible:=false;
			GroupBox_familienstand.visible:=false;
       end;

	5:begin //7
		tabelle:='abteilung';
		 //radiogroup_ueberein.Visible:=false;
		 radiogroup_datum.Visible:=false;
		 checkbox_archiv.Visible:=false;
     CheckBox_archiv_rest.visible:=false;
     Label_archiv_all.visible:=false;
		 groupbox_geschlecht.visible:=false;
		 GroupBox_familienstand.visible:=false;
	  end;
	 6:begin //7
		tabelle:='firma';
		 radiogroup_ueberein.Visible:=false;
		 radiogroup_datum.Visible:=false;
		 checkbox_archiv.Visible:=false;
     CheckBox_archiv_rest.visible:=false;
     Label_archiv_all.visible:=false;
		 groupbox_geschlecht.visible:=false;
		 GroupBox_familienstand.visible:=false;
         groupbox_email.Visible:=false;
	  end;
     7: begin
       tabelle:='firma_zeiten_soll';
       radiogroup_ueberein.Visible:=false;
			 radiogroup_datum.Visible:=false;
		 	checkbox_archiv.Visible:=false;
      CheckBox_archiv_rest.visible:=false;
      Label_archiv_all.visible:=false;
			 groupbox_geschlecht.visible:=false;
		 	GroupBox_familienstand.visible:=false;
        	 groupbox_email.Visible:=false;
        end;
end;
query:=format('select * from %s where nummer=0',[tabelle]);
datamodul.sql(true,datamodul.q_2,query,'');
end;


procedure tform_importieren.csv_oeffnen;
var
feld,sname:string;
i: integer;
begin
try
   if not assigned(form_csv_import) then exit;
	 checklistbox.items.clear;

	 i:=0;
	 feld:=' ';
	 while {(feld <>'') and (feld<>'*** Excel - No Reply *') and} (i<form_csv_import.stringgrid.ColCount)  do
	 begin
		feld:=form_csv_import.stringgrid.Cells[i,0];//Excel.GetDataAtFileSheet(1,i,sname);
		//feld:=copy(feld,1,length(feld)-2);
		feld:=trim(feld);
		if feld<>'' then checklistbox.items.Add(lowercase(feld));

		inc(i);
	 end;

   if checklistbox.items.count=0 then
     begin
     	showmessage('csv-Datei ist leer');

     end;
    if not felderueberpruefen then showmessage('CSV-Felder nicht ausreichend');
finally
	//Excel. Disconnect;
	//com.excel_disconnect;
end;

// hier felder checken
if checklistbox.items.count>0 then felder_checken;

end;

procedure tform_importieren.excel_oeffnen;
var
feld,sname:string;
i: integer;
begin
try
	 checklistbox.items.clear;
	 //Excel.Connect;
	 //com.xl_connect;
     try
        //l�uft excel?
        i:=excel.Workbooks.Count;


     except
           //com.excel_connect('Import Daten einf�gen');
           showmessage('Excel reagiert nicht - In Excel alle Dialoge / Markierungen schlie�en und dann neu verbinden');
           excel.visible:=true;
           bitbtn_import.Enabled:=false;
           exit;
     end;
     bitbtn_import.Enabled:=true;
	 //if com.xla.Workbooks.Count=0 then
     	if excel.Workbooks.Count=0 then
	 begin
		showmessage('Bitte in der Excel Anwendung Tabelle neu anlegen und die Importtabelle in diese Excel Mappe kopieren und erneut verbinden.');
		//excel.workbooks.add;
        excel.visible:=true;
		exit;
	 end;

	 //sname:=com.xla.workbooks[1].name;// Excel.CurrentSheet;
   sname:=excel.workbooks[1].name;// Excel.CurrentSheet;
	 label_ex.Caption:=sname;
	 i:=1;
	 feld:=' ';
	 while (feld <>'') {and (feld<>'*** Excel - No Reply *')} and (i<100)  do
	 begin
		feld:=com.excel_GetDataAt(1,i);//Excel.GetDataAtFileSheet(1,i,sname);
		//feld:=copy(feld,1,length(feld)-2);
		feld:=trim(feld);
		if feld<>'' then checklistbox.items.Add(lowercase(feld));

		inc(i);
	 end;
     if checklistbox.items.count=0 then
     begin
     	showmessage('Bitte die notwendigen Daten in die Excel-Tabelle  '+sname+ '  kopieren und erneut verbinden.');
        excel.visible:=true;
        bitbtn_import.Enabled:=false;
     end;
finally
	//Excel. Disconnect;
	//com.excel_disconnect;
end;

// hier felder checken
if checklistbox.items.count>0 then felder_checken;

end;

procedure TForm_importieren.felder_checken;
var
feld,sname:string;
i:integer;
begin
	//if checklistbox.Items.IndexOf('firma')>=0 then
	//	 checklistbox.state[checklistbox.Items.IndexOf('firma')]:=cbchecked;
   if combobox_was.ItemIndex in [1,2,3] then
	 begin
		if checklistbox.Items.IndexOf('typ')>-1 then
		 checklistbox.state[checklistbox.Items.IndexOf('typ')]:=cbchecked;
		 if checklistbox.Items.IndexOf('datum')>-1 then
		 checklistbox.state[checklistbox.Items.IndexOf('datum')]:=cbchecked;
		 if checklistbox.Items.IndexOf('arzt')>-1 then
		 checklistbox.state[checklistbox.Items.IndexOf('arzt')]:=cbchecked;
		 if checklistbox.Items.IndexOf('status')>-1 then
		 checklistbox.state[checklistbox.Items.IndexOf('status')]:=cbchecked;
		 if checklistbox.Items.IndexOf('anmerkung')>-1 then
		 checklistbox.state[checklistbox.Items.IndexOf('anmerkung')]:=cbchecked;
	 end;

   if combobox_was.ItemIndex in [0,1,2,3,4,5,7] then
	 begin
		if checklistbox.Items.IndexOf('firma')>-1 then   //k�rzel
		 checklistbox.state[checklistbox.Items.IndexOf('firma')]:=cbchecked;
	 end;

   if combobox_was.ItemIndex in [0,1,2,3,4,5,6,7] then
	 begin
		if checklistbox.Items.IndexOf('kuerzel')>-1 then   //k�rzel
		 checklistbox.state[checklistbox.Items.IndexOf('kuerzel')]:=cbchecked;
	 end;
   if combobox_was.ItemIndex in [0,1,2,3,4,5] then
	 begin

          if checklistbox.Items.IndexOf('name')>-1 then
			 checklistbox.state[checklistbox.Items.IndexOf('name')]:=cbchecked;
              if checklistbox.Items.IndexOf('vorname')>-1 then
			 checklistbox.state[checklistbox.Items.IndexOf('vorname')]:=cbchecked;
              if checklistbox.Items.IndexOf('geb_dat')>-1 then
			 checklistbox.state[checklistbox.Items.IndexOf('geb_dat')]:=cbchecked;
             if checklistbox.Items.IndexOf('p_nummer')>-1 then
			 checklistbox.state[checklistbox.Items.IndexOf('p_nummer')]:=cbchecked;
	 end;

	case  combobox_was.ItemIndex of
	0: begin //proband
		 for i:= 0 to checklistbox.Items.Count-1 do
		  begin
			 if (datamodul.q_2.FindField( checklistbox.Items[i])<>nil)
				 or (checklistbox.Items[i]='nation')
				 or (checklistbox.Items[i]='taetigkeit')
				 or (checklistbox.Items[i]='schicht')
				 or (checklistbox.Items[i]='familienstand')

				 then  checklistbox.Checked[i]:=true;
		  end;
		end;
	1: begin //untersuchung
		  if checklistbox.Items.IndexOf('art')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('art')]:=cbchecked;
		  if checklistbox.Items.IndexOf('beurteilung')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('beurteilung')]:=cbchecked;
      if checklistbox.Items.IndexOf('pflicht_angebot_wunsch')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('pflicht_angebot_wunsch')]:=cbchecked;
		end;
	2: begin //impfung
		 if checklistbox.Items.IndexOf('impfstoff')>-1 then
			  checklistbox.state[checklistbox.Items.IndexOf('impfstoff')]:=cbchecked;
		  if checklistbox.Items.IndexOf('dosis')>-1 then
			  checklistbox.state[checklistbox.Items.IndexOf('dosis')]:=cbchecked;
		  if checklistbox.Items.IndexOf('charge')>-1 then
			  checklistbox.state[checklistbox.Items.IndexOf('charge')]:=cbchecked;
		  if checklistbox.Items.IndexOf('impfabfolge')>-1 then
			  checklistbox.state[checklistbox.Items.IndexOf('impfabfolge')]:=cbchecked;
		end;
	3: begin //labor
		  if checklistbox.Items.IndexOf('wert')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('wert')]:=cbchecked;
		  if checklistbox.Items.IndexOf('ergebnistext')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('ergebnistext')]:=cbchecked;
			if checklistbox.Items.IndexOf('testbezogenerhinweis')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('testbezogenerhinweis')]:=cbchecked;
      if checklistbox.Items.IndexOf('grenzwertindikator')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('grenzwertindikator')]:=cbchecked;
      if checklistbox.Items.IndexOf('einheit')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('einheit')]:=cbchecked;
      if checklistbox.Items.IndexOf('norm')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('norm')]:=cbchecked;
      if checklistbox.Items.IndexOf('goae')>-1 then
			checklistbox.state[checklistbox.Items.IndexOf('goae')]:=cbchecked;
		end;
    4:  begin //Freitext
    if checklistbox.Items.IndexOf('anamnese')>-1 then
			 checklistbox.state[checklistbox.Items.IndexOf('anamnese')]:=cbchecked;

		end;
	5: begin //abteilung
		if checklistbox.Items.IndexOf('abteilung_kuerzel')>-1 then
   checklistbox.state[checklistbox.Items.IndexOf('abteilung_kuerzel')]:=cbchecked;
   if checklistbox.Items.IndexOf('abteilung_lang')>-1 then
   checklistbox.state[checklistbox.Items.IndexOf('abteilung_lang')]:=cbchecked;
   if checklistbox.Items.IndexOf('anmerkung')>-1 then
   checklistbox.state[checklistbox.Items.IndexOf('anmerkung')]:=cbchecked;

		end;
	6: begin  //firma
		 for i:= 0 to checklistbox.Items.Count-1 do begin
			 if (datamodul.q_2.FindField( checklistbox.Items[i])<>nil)

				 {or (checklistbox.Items[i]='kuerzel')
				 {or (checklistbox.Items[i]='strasse')
				 or (checklistbox.Items[i]='ort')
				 or (checklistbox.Items[i]='postfach')
				 or (checklistbox.Items[i]='plz')
				 or (checklistbox.Items[i]='telefon')
				 or (checklistbox.Items[i]='fax')
				 or (checklistbox.Items[i]='email')}
				 then  checklistbox.Checked[i]:=true;// else checklistbox.Checked[i]:=false;

              if checklistbox.Items[i]='firmenname' then  checklistbox.Checked[i]:=true;   //geht auch mit Frima aber dann schwer mit K�rzel auseinanderzuhalten
              {if checklistbox.Items[i]='ez_m1' then  checklistbox.Checked[i]:=true;
              if checklistbox.Items[i]='ez_m2' then  checklistbox.Checked[i]:=true;
              if checklistbox.Items[i]='ez_m3' then  checklistbox.Checked[i]:=true;
              if checklistbox.Items[i]='ez_t1' then  checklistbox.Checked[i]:=true;
              if checklistbox.Items[i]='ez_t2' then  checklistbox.Checked[i]:=true;
              if checklistbox.Items[i]='ez_t3' then  checklistbox.Checked[i]:=true; }
              if checklistbox.Items[i]='anmerkung' then  checklistbox.Checked[i]:=true;   // noch nicht getetestet aber m�sste tun 20120127
			 end;
		end;
    7: begin  //Zeiten
    for i:= 0 to checklistbox.Items.Count-1 do begin
   if (datamodul.q_2.FindField( checklistbox.Items[i])<>nil)

				 {or (checklistbox.Items[i]='kuerzel')
				 {or (checklistbox.Items[i]='strasse')
				 or (checklistbox.Items[i]='ort')
				 or (checklistbox.Items[i]='postfach')
				 or (checklistbox.Items[i]='plz')
				 or (checklistbox.Items[i]='telefon')
				 or (checklistbox.Items[i]='fax')
				 or (checklistbox.Items[i]='email')}
		 then  checklistbox.Checked[i]:=true;// else checklistbox.Checked[i]:=false;

              //if checklistbox.Items[i]='firmenname' then  checklistbox.Checked[i]:=true;   //geht auch mit Frima aber dann schwer mit K�rzel auseinanderzuhalten
        if checklistbox.Items[i]='von' then  checklistbox.Checked[i]:=true;
        if checklistbox.Items[i]='bis' then  checklistbox.Checked[i]:=true;
        if checklistbox.Items[i]='stunden' then  checklistbox.Checked[i]:=true;
        if checklistbox.Items[i]='konto' then  checklistbox.Checked[i]:=true;
        if checklistbox.Items[i]='kriterium' then  checklistbox.Checked[i]:=true;
        if checklistbox.Items[i]='konto1' then  checklistbox.Checked[i]:=true;
        if checklistbox.Items[i]='kriterium1' then  checklistbox.Checked[i]:=true;

		end;
		end;
	end;

end;


procedure TForm_importieren.BitBtn1Click(Sender: TObject);
begin
excel_oeffnen;
end;




function TForm_importieren.felderueberpruefen:boolean;
var mess,cr:string;
begin
result:=true;
cr:=chr(13)+chr(10);
if checklistbox.Items.count=0 then mess:='Keine Verbindung mit Excel oder keine Daten vorhanden'+cr;



if combobox_was.ItemIndex in [1,2,3] then
begin
	if checklistbox.Items.IndexOf('typ')<0 then mess:=mess+'Typ (Untersuchung, Impfung oder Labor) muss angegeben sein'+cr;
	if checklistbox.Items.IndexOf('datum')<0 then mess:=mess+'"datum" f�r Untersuchung, Impfung oder Labor muss angegeben sein'+cr;
end;

if combobox_was.ItemIndex in [0,1,2,3,4] then
        begin
           case radiogroup_ueberein.itemindex  of
            0:
           	begin
              if (checklistbox.Items.IndexOf('kuerzel')<0) then mess:=mess+'Die Spalte "Kuerzel" (Firmenk�rzel) fehlt'+cr;
              if checklistbox.Items.IndexOf('name')<0 then mess:=mess+'Die Spalte "Name" fehlt'+cr;
              if checklistbox.Items.IndexOf('vorname')<0 then mess:=mess+'Die Spalte "Vorname" fehlt'+cr;
              if checklistbox.Items.IndexOf('geb_dat')<0 then mess:=mess+'Die Spalte "Geb_dat" fehlt'+cr;
           	end;
           1:
          	 begin
                if (checklistbox.Items.IndexOf('kuerzel')<0) then mess:=mess+'Die Spalte "Kuerzel" (Firmenk�rzel) fehlt'+cr;
                if checklistbox.Items.IndexOf('name')<0 then mess:=mess+'Die Spalte "Name" fehlt'+cr;
                if checklistbox.Items.IndexOf('vorname')<0 then mess:=mess+'Die Spalte "Vorname" fehlt'+cr;

          	 end;
           2:begin
                if (checklistbox.Items.IndexOf('kuerzel')<0) then mess:=mess+'Die Spalte "kuerzel" (Firmenk�rzel) fehlt'+cr;
                if checklistbox.Items.IndexOf('p_nummer')<0 then mess:=mess+'Die Spalte "p_nummer" (Peronalnummer) fehlt'+cr;
         	  end;
           3:begin
                if (checklistbox.Items.IndexOf('kuerzel')<0) then mess:=mess+'Die Spalte "Kuerzel" (Firmenk�rzel) fehlt'+cr;
               if checklistbox.Items.IndexOf('name')<0 then mess:=mess+'Die Spalte "Name" fehlt'+cr;
                if checklistbox.Items.IndexOf('vorname')<0 then mess:=mess+'Die Spalte "Vorname" fehlt'+cr;
                if checklistbox.Items.IndexOf('geb_dat')<0 then mess:=mess+'Die Spalte "Geb_dat" fehlt'+cr;
            end;
          
		end;
 end;


	if combobox_was.ItemIndex in [5] then //,6
  begin

      if (checklistbox.Items.IndexOf('kuerzel')<0)  then mess:=mess+'Die Spalte "Kuerzel" (Firmen-K�rzel) fehlt.'+cr;
  end;

  if combobox_was.ItemIndex in [4] then
     begin
      if checklistbox.Items.IndexOf('anamnese')<0 then mess:=mess+'Die Spalte "Anamnese" (Freitext) fehlt'+cr;
     end;

	if combobox_was.ItemIndex in [5] then
  begin
    if checklistbox.Items.IndexOf('abteilung_kuerzel')<0 then mess:=mess+'Die Spalte "Abteilung_Kuerzel" (K�rzel f�r die Abteilung) muss angegeben sein'+cr;
  end;

   if combobox_was.ItemIndex in [7] then
   begin
          if checklistbox.Items.IndexOf('stunden')<0 then mess:=mess+'"Stunden" Die Zeitvorgabe muss angegeben sein'+cr;
          if checklistbox.Items.IndexOf('konto')<0 then mess:=mess+'"Konto" Das Zeitkonot muss angegeben sein'+cr;
          if checklistbox.Items.IndexOf('kriterium')<0 then mess:=mess+'"Kriterium" Das Abrechnunskriterium muss angegeben sein'+cr;
          if checklistbox.Items.IndexOf('von')<0 then mess:=mess+'"von" Der Start der Periode muss angegeben sein'+cr;
          if checklistbox.Items.IndexOf('bis')<0 then mess:=mess+'"bis" Das Ende der Periode muss angegeben sein'+cr;
   end;


	 if mess<>'' then
	 begin
		showmessage(mess);
		result:=false;
	 end;

end;

function tform_importieren.i_bediener(bediener:string):string;
var
	query:string;
begin
if bediener<>'' then bediener:=bediener+'%';
query:=format('select nummer from untersucher where untersucher like "%s" ',[bediener]);
result:=mysql_d.Feldinhalt(query,0);
if result='' then result:='-1';
end;

function tform_importieren.i_firmenkuerzel(kuerzel:string):string;
var
	query:string;
begin
query:=format('select nummer from firma where kuerzel= "%s" ',[kuerzel]);
result:=mysql_d.Feldinhalt(query,0);
if result='' then result:='-1';
end;


function tform_importieren.i_impfstoff(impfstoff:string):string;
var
	query:string;
begin
if Impfstoff<>'' then impfstoff:=trim(impfstoff)+'%';
query:=format('select nummer from impfstoff where impfstoff like "%s" ',[impfstoff]);
result:=mysql_d.Feldinhalt(query,0);
if result='' then result:='-1';
end;

function tform_importieren.i_beurteilung(beurteilung:string):string;
var
	query:string;
begin
if beurteilung<>'' then beurteilung:=trim(beurteilung)+'%';
query:=format('select nummer from beurteilung where text like "%s" ',[beurteilung]);
result:=mysql_d.Feldinhalt(query,0);
if result='' then result:='-1';
end;

function tform_importieren.i_abrechnungskriterium(konto:string):string;
var
	query:string;
begin
   if konto<>'' then konto:=trim(konto)+'%';
  query:=format('select nummer from abr_anlass_krit where name like "%s" and storno=0 ',[konto ]);
  result:=mysql_d.Feldinhalt(query,0);
  if result='' then result:='-1';
end;

function tform_importieren.i_zeitkonto(zeitkonto:string):string;
var
	query:string;
begin
if zeitkonto<>'' then zeitkonto:=trim(zeitkonto)+'%';
query:=format('select nummer from abr_zeitkonto where name like "%s" and storno=0 ',[zeitkonto ]);
result:=mysql_d.Feldinhalt(query,0);

if result='' then result:='-1';
end;

function tform_importieren.memoadd(memo:tstrings;text:string):tstrings;
begin
//text:=text+'hallo';
memo.add(text);
end;


procedure  tform_importieren.filter_rest;
var
query,firmen,f_name:string;
nummer:int64;
begin
with datamodul do begin
  query:=format('select * from %s group by f_nummer',[temp_tab_p]);
  datamodul.sql(false,datamodul.q_7,query,'');
  q_7.First;
  while not q_7.Eof do
  begin
    firmen:=firmen+','+q_7.findfield('f_nummer').asstring;
    q_7.Next;
  end;
  firmen:=copy(firmen,2,length(firmen));
  query:=format('select * from mitarbeiter where i_firma in (%s)',[firmen]);
  datamodul.sql(false,datamodul.q_7,query,'');
  q_7.First;
  while not q_7.Eof do
  begin
    nummer:=getbigint( q_7,'nummer');//strtoint(q_7.findfield('nummer').asstring);
    if not q_5.Locate('p_nummer',nummer,[]) then
    begin
       q_firma.Locate('nummer',q_7.findfield('i_firma').AsString,[]);

      //query:= format('create table  %s (nummer int(11) not null auto_increment  primary key, f_nummer int(11), f_name char(50),p_nummer bigint(19), p_name char(50), p_vorname char(50),p_geb_dat date,KEY p_nummer (p_nummer),KEY f_nummer (f_nummer))',[temp_tab_n]);
        q_6.AppendRecord([nil,q_7.findfield('i_firma').asstring,q_firma.FindField('firma').asstring,q_7.findfield('nummer').asstring,q_7.findfield('name').asstring ,q_7.findfield('vorname').asstring,q_7.findfield('geb_dat').asstring]);
    end;
    q_7.Next;
  end;
  q_6.Refresh;

end;
end;

procedure tform_importieren.archivier_rest;
var
s_nummer,query:string;
begin
richedit.lines.Add('######### Beginn Archivierung #######');
  with datamodul do
  begin
    q_6.first;
    while not q_6.Eof do
    begin
      s_nummer:=q_6.findfield('p_nummer').asstring;
      query:=format('update mitarbeiter set archiv="X" where nummer=%s',[s_nummer]);
      mysql_d.sql_exe(query);
      richedit.lines.Add(format('Archiv| %s, %s, %s ',[q_6.findfield('p_name').asstring,q_6.findfield('p_vorname').asstring,q_6.findfield('p_geb_dat').asstring]));
      q_6.next;
    end;
  end;
  richedit.lines.Add('######### Ende Archivierung #######');
end;


end.
