unit a_serienbrieffelder;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	Grids, DBGrids, dbgridEXT, ExtCtrls, StdCtrls, Buttons,db, variants;

type
  Tform_serienfelder = class(TForm)
    Panel1: TPanel;
	  Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    DBgridEXT1: TDBgridEXT;
	  DBgridEXT2: TDBgridEXT;
	  SpeedButton2: TSpeedButton;
	  SpeedButton1: TSpeedButton;
	  SpeedButton3: TSpeedButton;
	  SpeedButton4: TSpeedButton;
	  BitBtn1: TBitBtn;
    BitBtn_change: TBitBtn;
    BitBtn_delete1: TBitBtn;
    BitBtn_delete2: TBitBtn;
    BitBtn_change2: TBitBtn;
    BitBtn_neu2: TBitBtn;
	  procedure BitBtn_change2Click(Sender: TObject);
	  procedure SpeedButton1Click(Sender: TObject);
	  procedure SpeedButton2Click(Sender: TObject);
	  procedure SpeedButton4Click(Sender: TObject);
	  procedure SpeedButton3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn_changeClick(Sender: TObject);
    procedure BitBtn_delete1Click(Sender: TObject);
    procedure BitBtn_neu2Click(Sender: TObject);
    procedure BitBtn_delete2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
	private
	  { Private-Deklarationen }
	  procedure rf_neu;
	public
	  { Public-Deklarationen }
  end;

var
  form_serienfelder: Tform_serienfelder;

implementation

uses a_data, a_listwahl, a_main, a_line_edit;

{$R *.DFM}

procedure Tform_serienfelder.BitBtn_change2Click(Sender: TObject);
begin
	with datamodul do
	begin
		if (MessageDlg('Auswahl aus Datenbankfeldern (sonst Freitext)?', mtConfirmation	,[mbYes, mbNo], 0)=mryes ) then
		begin
			try
			  sql_new(false,q_1,'select name1 from sql_felder where name1<>""','reihenfolge');
			  form_liste:=tform_liste.create(self);
          form_liste.DBgridEXT.Columns[0].fieldname:='name1';
          form_liste.DBgridEXT.Columns[0].width:=form_liste.DBgridEXT.Width-50;
          form_liste.height:= form_main.Height-170;
			  if form_liste.showmodal<>mrok then exit;
			  q_serienfeldinhalte.edit;
			  q_serienfeldinhalte['feldinhalt']:=q_1['name1'];
			finally
			  form_liste.Release;
			end;
		end
		else
		begin
		  try
			  form_line_edit:=tform_line_edit.create(self); ;
			 form_line_edit.CheckBox.visible:=false;
			 form_line_edit.edit.text:=datamodul.q_serienfeldinhalte['feldinhalt'];
			 if form_line_edit.showmodal=mrok then
			 begin
			  datamodul.q_serienfeldinhalte.edit;
			  datamodul.q_serienfeldinhalte['feldinhalt']:='"'+trim(form_line_edit.edit.text)+'"';
			  datamodul.q_serienfeldinhalte.post;
			 end;
		  finally
			 form_line_edit.release;
		  end;
		end;
	end;
end;

procedure Tform_serienfelder.SpeedButton1Click(Sender: TObject);
begin
form_main.tab_reihenfolge(datamodul.q_serienfelder,0);
end;

procedure Tform_serienfelder.SpeedButton2Click(Sender: TObject);
begin
form_main.tab_reihenfolge(datamodul.q_serienfelder,0);
end;

procedure Tform_serienfelder.SpeedButton4Click(Sender: TObject);
begin
form_main.tab_reihenfolge(datamodul.q_serienfeldinhalte,0);
end;

procedure Tform_serienfelder.SpeedButton3Click(Sender: TObject);
begin
form_main.tab_reihenfolge(datamodul.q_serienfeldinhalte ,1);
end;

procedure tform_serienfelder.rf_neu;
var
i:integer;
bm:tbookmark;
begin
with datamodul do
begin
	bm:=q_serienfeldinhalte.GetBookmark;
	q_serienfeldinhalte.First;
	i:=0;
	while not q_serienfeldinhalte.eof do
	begin
	  q_serienfeldinhalte.Edit;
	  q_serienfeldinhalte['reihenfolge']:=i;
	  inc(i);
	  q_serienfeldinhalte.next;
	end;
	q_serienfeldinhalte.GotoBookmark(bm);
	q_serienfeldinhalte.FreeBookmark(bm);
end;
end;

procedure Tform_serienfelder.BitBtn1Click(Sender: TObject);
var nummer:integer;
begin
	datamodul.q_serienfelder.Last;
	if datamodul.q_serienfelder['reihenfolge']<>null then
		nummer:=datamodul.q_serienfelder['reihenfolge']+1
		else nummer:=0;
	neuer_datensatz(datamodul.q_serienfelder,['nummer','feldname','reihenfolge'],[null,'',nummer]) ;
	datamodul.q_serienfelder.post;
	BitBtn_changeClick(Sender);
end;

procedure Tform_serienfelder.BitBtn_changeClick(Sender: TObject);
begin
 form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 form_line_edit.edit.text:=datamodul.q_serienfelder['feldname'];
 if form_line_edit.showmodal=mrok then
 begin
	datamodul.q_serienfelder.edit;
	datamodul.q_serienfelder['feldname']:=trim(form_line_edit.edit.text);
	datamodul.q_serienfelder.post;
 end;
 form_line_edit.Release;
end;

procedure Tform_serienfelder.BitBtn_delete1Click(Sender: TObject);
begin
	if (MessageDlg('L�schen des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes ) then exit;
	if kein_datensatz(datamodul.q_serienfeldinhalte) then dateiloeschen(datamodul.q_serienfelder,false,false); //datamodul.q_serienfelder.Delete;
end;

procedure Tform_serienfelder.BitBtn_neu2Click(Sender: TObject);
begin
with datamodul do
begin
	neuer_datensatz(q_serienfeldinhalte,['nummer','i_serienfeld','feldinhalt','reihenfolge']
		,[null,q_serienfelder['nummer'],'',99999999]) ;
	q_serienfeldinhalte.post;
	rf_neu;
	BitBtn_change2Click(Sender);
end;
end;

procedure Tform_serienfelder.BitBtn_delete2Click(Sender: TObject);
begin
if (datensatz(datamodul.q_serienfeldinhalte)) then dateiloeschen(datamodul.q_serienfeldinhalte,false,false);// datamodul.q_serienfeldinhalte.delete; 
end;

procedure Tform_serienfelder.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
datamodul.q_serienfelder.Refresh;
end;

end.
