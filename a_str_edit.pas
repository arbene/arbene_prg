unit a_str_edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TForm_str_edit = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit: TEdit;
    procedure FormActivate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_str_edit: TForm_str_edit;

implementation

{$R *.DFM}

procedure TForm_str_edit.FormActivate(Sender: TObject);
begin
edit.SetFocus;
end;

end.
