unit a_icdwortliste;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_data,
  Grids, DBGrids, dbgridEXT, ExtCtrls, StdCtrls, Buttons,db;

type
  TForm_icdwortlist = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    DBgridEXT1: TDBgridEXT;
    procedure DBgridEXT1DblClick(Sender: TObject);
    procedure DBgridEXT1KeyPress(Sender: TObject; var Key: Char);
    procedure DBgridEXT1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
   
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_icdwortlist: TForm_icdwortlist;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_icdwortlist.DBgridEXT1DblClick(Sender: TObject);
begin
 modalresult:=mrok;
end;

procedure TForm_icdwortlist.DBgridEXT1KeyPress(Sender: TObject;
  var Key: Char);
var ordkey: integer;
begin
ordkey:=ord(key);
case
ordkey of 48..256 :
  begin
	form_icdwortlist.caption:='ICD-Wortliste '+DBgridEXT1.eingabetext;
  datamodul.q_icd_wort.first;
	datamodul.q_icd_wort.locate('wort',DBgridEXT1.eingabetext,[loCaseInsensitive,loPartialKey	]);
	dbgridext1.SetFocus;
  end;
end;
end;

procedure TForm_icdwortlist.DBgridEXT1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	form_icdwortlist.caption:='ICD-Wortliste  '+DBgridEXT1.eingabetext;
end;

procedure TForm_icdwortlist.FormCreate(Sender: TObject);
begin

grid_suchen(sender);
end;

procedure TForm_icdwortlist.FormShow(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
dbgridext1.SetFocus;
end;



end.
