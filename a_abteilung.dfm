�
 TFORM_ABTEILUNG 0e  TPF0TForm_abteilungForm_abteilungLeft`TopdBorderIconsbiSystemMenu BorderStylebsToolWindowCaptionArbeitsbereich - InfoClientHeightQClientWidth9Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight 	TGroupBox	GroupBox1Left TopWidth9Height8AlignalBottomCaptionAbteilungsinfoTabOrder  TLabelLabel1LeftTopWidth(HeightCaptionFirmaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel2Left#Top9WidthLHeightCaptionAbteilung K�rzel  TLabelLabel3Left� Top9WidthpHeightCaptionAbteilung Beschreibung  TLabelLabel4LeftTopiWidthmHeightCaptionAbteilungsleiterFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel5LeftTopyWidthHeightCaptionName  TLabelLabel6Left TopyWidth*HeightCaptionVorname  TLabelLabel7LeftTop� Width$HeightCaptionTelefon  TLabelLabel8Left Top� WidthHeightCaptionemail  TLabelLabel9LeftTop� WidthKHeightCaptionMitarbeiterFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel10LeftTop� WidthHeightCaptionName  TLabelLabel11Left Top� Width*HeightCaptionVorname  TLabelLabel12LeftTop Width$HeightCaptionTelefon  TLabelLabel13LeftTop WidthHeightCaptionemail  TEdit
Edit_firmaLeft"Top!Width�HeightReadOnly	TabOrder   TEditEdit_telefonLeftTop� Width� HeightReadOnly	TabOrder  TEditEdit_vornameLeft Top� Width� HeightReadOnly	TabOrder  TEdit	Edit_nameLeftTop� Width� HeightReadOnly	TabOrder  TEditEdit_beschreibungLeft� TopIWidth	HeightReadOnly	TabOrder  TEditEdit_kuerzelLeft!TopIWidth� HeightReadOnly	TabOrder  TEdit
Edit_emailLeft Top� Width� HeightReadOnly	TabOrder  TEditEdit_ma_telefonLeftTopWidth� HeightReadOnly	TabOrder  TEditEdit_ma_vornameLeftTop� Width� HeightReadOnly	TabOrder  TEditEdit_ma_nameLeftTop� Width� HeightReadOnly	TabOrder	  TEditEdit_ma_emailLeftTopWidth� HeightReadOnly	TabOrder
   	TGroupBox	GroupBox2Left TopWidth9Height� AlignalClientCaptionArbeitsbereicheTabOrder 
TDBgridEXTDBgridEXT_apLeft TopWidth�Height� AlignalClient
DataSourceDataModul.ds_2DefaultDrawing	Options	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameArbeitsbereichTitle.CaptionBereichWidth�Visible	    TPanelPanel1LeftTopWidthHeight� AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel2LeftTopWidthHeight� AlignalRight
BevelOuterbvNoneTabOrder   TToolBarToolBarLeft Top Width9HeightCaptionToolBarImagesForm_main.ImageList_mainTabOrder TToolButtonToolButton_newLeft TopHintNeue Berechtigung vergebenCaptionToolButton_new
ImageIndex ParentShowHintShowHint	OnClickToolButton_newClick  TToolButtonToolButton2LeftTopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_delLeftTopHintBerechtigung l�schenCaptionToolButton_del
ImageIndexParentShowHintShowHint	OnClickToolButton_delClick  TToolButtonToolButton7Left6TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolButton_closeLeftCTopHintBeendenCaptionToolButton_close
ImageIndexParentShowHintShowHint	OnClickToolButton_closeClick  TBitBtnBitBtn1LeftZTopWidthKHeightCaptionBitBtn1TabOrder OnClickBitBtn1Click    