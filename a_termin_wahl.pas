unit a_termin_wahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Spin, ComCtrls, Mask;

type
  TForm_twahl = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox_zeitraum: TGroupBox;
    RadioButton_gesamt: TRadioButton;
    RadioButton_vergangen: TRadioButton;
    RadioButton_zukunft: TRadioButton;
    SpinEdit_von: TSpinEdit;
    RadioButton_auswahl: TRadioButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpinEdit_bis: TSpinEdit;
    RadioGroup_typ: TRadioGroup;
    RadioButton_auswahl_dat: TRadioButton;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label5: TLabel;
    Label6: TLabel;
    GroupBox_archiv: TGroupBox;
    CheckBox_archiv: TCheckBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpinEdit_vonChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_twahl: TForm_twahl;

implementation

uses a_main,  a_data, a_kalender;

{$R *.DFM}

procedure TForm_twahl.FormActivate(Sender: TObject);
begin
//form_main.combolesen(ComboBox_firma, datamodul.q_firma,'firma');

end;

procedure TForm_twahl.FormCreate(Sender: TObject);
begin
if prg_typ=3 then
begin
	radiogroup_typ.Visible:=false;
	//radiogroup_firma.Visible:=false;
	groupbox_archiv.Visible:=false;
end;

form_main.form_positionieren(tform(sender));

//form_main.form_positionieren(tform(sender));

end;

procedure TForm_twahl.SpeedButton1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
   begin
	 if sender=speedbutton1 then maskedit1.text:=datetostr(int(form_kalender.auswahlkalender.date));
 	 if sender=speedbutton2 then maskedit2.text:=datetostr(int(form_kalender.auswahlkalender.date));
   radiobutton_auswahl_dat.checked:=true;
   end;
end;

procedure TForm_twahl.SpinEdit_vonChange(Sender: TObject);
begin
radiobutton_auswahl.checked:=true;
end;

end.
