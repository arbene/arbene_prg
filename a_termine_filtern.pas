unit a_termine_filtern;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls;

type
  TForm_termine_filtern = class(TForm)
    GroupBox2: TGroupBox;
    ComboBox_untersucher: TComboBox;
    Panel1: TPanel;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RadioGroup_firma: TRadioGroup;
    PageControl_ansicht: TPageControl;
    TabSheet_planen: TTabSheet;
    TabSheet_Zeiterfassung: TTabSheet;
    ComboBox_terminart: TComboBox;
    ComboBox_konto: TComboBox;
    Label1: TLabel;
    Label3: TLabel;
    procedure ComboBox_untersucherChange(Sender: TObject);
    procedure ComboBox_terminartChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PageControl_ansichtChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_termine_filtern: TForm_termine_filtern;

implementation

uses a_termine, a_main,  a_data;

{$R *.DFM}

procedure TForm_termine_filtern.ComboBox_untersucherChange(
  Sender: TObject);
begin
	//checkbox_alleunt.Checked:=false;
end;

procedure TForm_termine_filtern.ComboBox_terminartChange(Sender: TObject);
begin
   //checkbox_allearten.Checked:=false;
end;

procedure TForm_termine_filtern.FormShow(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
  PageControl_ansichtChange(sender);

end;

procedure TForm_termine_filtern.FormDestroy(Sender: TObject);
begin
	form_main.combodeaktivieren(ComboBox_untersucher);
	form_main.combodeaktivieren(combobox_terminart);
end;

procedure TForm_termine_filtern.PageControl_ansichtChange(Sender: TObject);
begin
if pagecontrol_ansicht.ActivePage= TabSheet_Zeiterfassung then
  begin
    caption:= 'Zeiterfassung';
    if (b.b_string_silent('Auswertung-Gesamt')<0) then combobox_untersucher.Enabled:=false else combobox_untersucher.Enabled:=true ;
    if (b.b_string_silent('Auswertung-Gesamt')<0) then form_main.combogehezu(ComboBox_untersucher,akt_untersucher);
  end
else
  begin
    caption:='Termine filtern';
    combobox_untersucher.Enabled:=true ;
  end;
  g_termin_filtern_tab:=pagecontrol_ansicht.ActivePageIndex;
end;

end.
