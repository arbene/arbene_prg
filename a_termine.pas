unit a_termine;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, ExtCtrls, ComCtrls,db,DBTables,listen, Buttons, Spin,
  DBGrids, dbgridEXT, ToolWin, DBCtrls, Mask, Panel_zeitplan, Menus, zdataset,
  dbcombo_number, DBEdit_time,Variants;

type
  TForm_termine = class(TForm)
    ToolBar1: TToolBar;
    ToolButton_neu: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton4: TToolButton;
	  ToolButton_speichern: TToolButton;
    ToolButton9: TToolButton;
    ToolButton_cancel: TToolButton;
    ToolButton6: TToolButton;
	  ToolButton_loeschen: TToolButton;
    ToolButton_exit: TToolButton;
    PageControl_zeit: TPageControl;
	  Splitter1: TSplitter;
    TabSheet_tag: TTabSheet;
    TabSheet_woche: TTabSheet;
    Panel_top_main: TPanel;
    DBgridEXT_tag: TDBgridEXT;
    GroupBox_mo: TGroupBox;
    GroupBox_do: TGroupBox;
    GroupBox_di: TGroupBox;
	  GroupBox_sa: TGroupBox;
    GroupBox_mi: TGroupBox;
    GroupBox_fr: TGroupBox;
	  GroupBox_so: TGroupBox;
    PageControl_rechts: TPageControl;
    TabSheet_detail: TTabSheet;
    TabSheet_terminieren: TTabSheet;
    Panel2: TPanel;
    BitBtn_untersuchung_select: TBitBtn;
	  DBgridEXT_unt_filter: TDBgridEXT;
    Panel7: TPanel;
    Label9: TLabel;
    GroupBox_zeitp: TGroupBox;
    Label2: TLabel;
	  Label3: TLabel;
    DBEdit_datum_s: TDBEdit;
    GroupBox_anm: TGroupBox;
    GroupBox_term_dat: TGroupBox;
	  DBEdit_vorname: TDBEdit;
    DBEdit_nachname: TDBEdit;
    Label_typ: TLabel;
    Label_t_art: TLabel;
    Label_vorname: TLabel;
    Label_nachname: TLabel;
    DBgridEXT_mo: TDBgridEXT;
    DBgridEXT_do: TDBgridEXT;
    DBgridEXT_di: TDBgridEXT;
    DBgridEXT_fr: TDBgridEXT;
    DBgridEXT_mi: TDBgridEXT;
    DBgridEXT_sa: TDBgridEXT;
	  DBgridEXT_so: TDBgridEXT;
    Panel3: TPanel;
    ToolButton_filtern: TToolButton;
	  Panel1: TPanel;
	  Panel11: TPanel;
    SpeedButton_datum_runter: TSpeedButton;
    Panel10: TPanel;
    SpeedButton_datum_rauf: TSpeedButton;
    Edit_datum: TEdit;
    ToolButton7: TToolButton;
    ToolButton13: TToolButton;
    ToolButton_kalender: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    TabSheet_monat: TTabSheet;
    Panel_zeitplan1: TPanel_zeitplan;
    Label1: TLabel;
    TabSheet_liste: TTabSheet;
    DBgridEXT_liste: TDBgridEXT;
    ToolButton1: TToolButton;
    MainMenu_termin: TMainMenu;
	  Datei1: TMenuItem;
    m_Neu: TMenuItem;
    Verndern1: TMenuItem;
    Speichern1: TMenuItem;
    Rckgngig1: TMenuItem;
    Lschen1: TMenuItem;
    All_del: TMenuItem;
    N1: TMenuItem;
    Verlassen1: TMenuItem;
    Bearbeiteb1: TMenuItem;
    Filtern1: TMenuItem;
    druck: TMenuItem;
    ToolButton_schicht: TToolButton;
    ToolButton5: TToolButton;
    Label4: TLabel;
    DBText2: TDBText;
    N2: TMenuItem;
    Email: TMenuItem;
    Tagesplan1: TMenuItem;
    ToolButton10: TToolButton;
    DBMemo_detail: TDBMemo;
    Terminieren1: TMenuItem;
    StatusBar: TStatusBar;
    DBCombo_num_art: TDBCombo_num;
    SpeedButton_terminaendern: TSpeedButton;
    DBEdit_zeit_s: TDBEdit_time;
    DBEdit_zeit_e: TDBEdit_time;
    Label5: TLabel;
    DBEdit_firma: TDBEdit;
    PopupMenu1: TPopupMenu;
    m_schicht: TMenuItem;
    DBMemo_anlass: TDBMemo;
    DBMemo_selekt_anlass: TDBMemo;
    TabSheet_zeit: TTabSheet;
    Panel_fsi_oben: TPanel;
    Label41: TLabel;
    Label36: TLabel;
    Label20: TLabel;
    Stunden: TLabel;
    Minuten: TLabel;
    SpeedButton6: TSpeedButton;
    Label195: TLabel;
    Label196: TLabel;
    Label252: TLabel;
    Label253: TLabel;
    DBEdit_z_date: TDBEdit;
    DBEdit_z_Stunden: TDBEdit;
    DBEdit_z_minuten: TDBEdit;
    DBCombo_num_z_erbringer: TDBCombo_num;
    DBComboBox_z_zeitansatz: TDBComboBox;
    DBCheckBox_z_zusatz: TDBCheckBox;
    DBEdit_z_beginn: TDBEdit_time;
    DBEdit_z_ende: TDBEdit_time;
    Panel_fsi_2: TPanel;
    Label170: TLabel;
    Label184: TLabel;
    DBCombo_num_z_abrechnungskriterium: TDBCombo_num;
    DBCombo_num_z_zeitkonto: TDBCombo_num;
    GroupBox_fsi_3: TGroupBox;
    DBMemo_z_aufgabe: TDBMemo;
    GroupBox_fsi_4: TGroupBox;
    DBMemo_z_aufwand: TDBMemo;
    GroupBox_fsi_5: TGroupBox;
    DBMemo_z_anmerkung: TDBMemo;
    Edit_firma: TEdit;
	  procedure SpinButton1DownClick(Sender: TObject);
    procedure SpinButton1UpClick(Sender: TObject);
    procedure BitBtn_untersuchung_selectClick(Sender: TObject);
    procedure ToolButton_neuClick(Sender: TObject);
	  procedure ToolButton_speichernClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton_cancelClick(Sender: TObject);
	  procedure SpeedButton_datum_raufClick(Sender: TObject);
    procedure SpeedButton_datum_runterClick(Sender: TObject);
    procedure CheckBox_alleuntClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure DBgridEXT_Enter(Sender: TObject);
    procedure PageControl_zeitChange(Sender: TObject);
	  procedure ToolButton_loeschenClick(Sender: TObject);
	  procedure ToolButton_exitClick(Sender: TObject);
	  procedure DBgridEXT_DragOver(Sender, Source: TObject; X, Y: Integer;
		 State: TDragState; var Accept: Boolean);
	  procedure DBgridEXT_DragDrop(Sender, Source: TObject; X,
		 Y: Integer);
	  procedure DBgridEXT_unt_filterLButtonDown(Sender: TObject);
	  procedure DBgridEXT_DblClick(Sender: TObject);
	  procedure ToolButton_filternClick(Sender: TObject);
	  procedure ToolButton_kalenderClick(Sender: TObject);
	  procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	  procedure PageControl_zeitChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure DBgridEXT_CellClick(Column: TColumn);
	  procedure DBgridEXT_miCellClick(Column: TColumn);
	  procedure DBgridEXT_moLbuttongehalten(Sender: TObject);
	  procedure Panel_zeitplan1Click(Sender: TObject);
	  procedure Panel_zeitplan1MouseMove(Sender: TObject; Shift: TShiftState;
		 X, Y: Integer);
	  procedure Panel_zeitplan1MouseDown(Sender: TObject;
		 Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
	  procedure Panel_zeitplan1MouseUp(Sender: TObject; Button: TMouseButton;
		 Shift: TShiftState; X, Y: Integer);
	  procedure Panel_zeitplan1DblClick(Sender: TObject);
	  procedure Edit_datumChange(Sender: TObject);
	  procedure verndern1Click(Sender: TObject);
	  procedure verndern2Click(Sender: TObject);
	  procedure speichern1Click(Sender: TObject);
	  procedure abbrechen1Click(Sender: TObject);
	  procedure lschen1Click(Sender: TObject);
	  procedure Terminbersichtverlassen1Click(Sender: TObject);
	  procedure DBgridEXT_listeCellClick(Column: TColumn);
	  procedure formular_sperren;
	  function formular_entsperren:boolean;
	  procedure DBgridEXT_listeLButtonDown(Sender: TObject);
	  procedure termin_info_erzeugen(tabelle:tdataset);
	  procedure All_delClick(Sender: TObject);
	  procedure FormResize(Sender: TObject);
	  procedure druckClick(Sender: TObject);
	  procedure PageControl_rechtsChange(Sender: TObject);
	  procedure BitBtn_terminaendernClick(Sender: TObject);
	  procedure ToolButton_schichtClick(Sender: TObject);
	  procedure EmailClick(Sender: TObject);
	  procedure Tagesplan1Click(Sender: TObject);
	  procedure DBgridEXT_unt_filterDblClick(Sender: TObject);
	  procedure FormShow(Sender: TObject);
	  procedure Terminieren1Click(Sender: TObject);
    procedure SpeedButton_terminaendernClick(Sender: TObject);
    procedure DBEdit_zeit_sEnter(Sender: TObject);
    procedure DBEdit_zeit_sExit(Sender: TObject);
	private
	  { Private-Deklarationen}
	  q_tage:array [0..9] of tzquery;
	  source_tage:array [0..9] of TDataSource;
	  groupbox_tage: array [1..7]  of tgroupbox;
	  wochendaten:array [0..7] of tdatetime;
	  wochentage:array [1..7] of string;
	  zaehler,form_terminieren_drag_radiogroup_index:integer;

	  startx, starty:integer;
	  zugrichtung:integer;
	  y_maus:integer;
    p_untersucher,p_firma,p_art:string;
      dt:ttime;
    modus:integer;
	procedure unt_status_aendern(nummer:int64; uil:integer);
	procedure suche_untersuchung(quit:boolean);
	procedure statuslesen;
	procedure unt_filtern;

	procedure wochezeichnen;
	procedure wochenkalender;
	function wochentag(datum:tdatetime): integer;
	function kalenderwoche(datum:tdatetime):integer;
	procedure details_tabelle(day:integer);
	//procedure unt_filter_append(table: tzzmysqlquery;art:integer);
	procedure datumanzeigen;
	procedure shape_speichern(shape:tshape_termin);
 procedure zeichne_monat;
	procedure tag_excel;
  procedure uil_terminieren(uil_nummer: int64;i_unt_art:integer;datum:tdate;termin_nummer:int64;verschickt:boolean);
  procedure gehezudatum(datum:tdate);

 public
	  { Public-Deklarationen}
	  fette_tage: array of integer;
	  hauptdatum:tdatetime;
	  akt_tabelle:integer;    //aktuelle Tabelle
	  dbgrid_tage: array [0..7] of tdbgridext;
	  neu_lesen:boolean;
	  procedure details(art:integer);
	  procedure infoerzeugen(day:integer);
	  function gehezutermin(nummer:int64):boolean;
	  function terminiere_uil(nummer:int64;rubrik:integer):boolean;
	  procedure kalenderfiltern;
	 procedure termine_speichern(direkt:boolean);
	 procedure termine_laden;
	 procedure Jahresfeiertage(datum:tdate);
	 procedure monatsfeiertage(datum:tdate);
	 procedure kalender_akutalisieren;
   procedure all_refresh;
end;

var
	Form_termine: TForm_termine;

implementation

uses a_data, a_main, a_termin_wahl, a_termine_drag, a_kalender,
	a_termine_filtern, a_namsuch, a_schicht, a_u_wahl;

{$R *.DFM}




procedure TForm_termine.unt_filtern;
var
data: pnodepointer;
datum_von, datum_bis: tdate;
ii_firma: integer;
filter: string;
tabelle: array of string;
typen: array of string;
checkboxen: array of tcheckbox;

begin

zaehler:=0;
setlength(tabelle,4);
tabelle[1]:='select * from untersuchung';
tabelle[2]:='select * from impfung';
tabelle[3]:='select * from labor';
setlength(typen,4);
typen[1]:='Bescheinigung';
typen[2]:='Impfung';
typen[3]:='Labor';
setlength(checkboxen,4);


if  form_twahl.RadioButton_gesamt.checked then
	begin
  	datum_von:=now-36500;
     datum_bis:=now+36500;//100 Jahre
  end;
if  form_twahl.RadioButton_vergangen.checked then
	begin
		datum_von:=now-36500;
     datum_bis:=now;
  end;
if  form_twahl.RadioButton_zukunft.checked then
	begin
		datum_von:=now;
     datum_bis:=now+36500;//100 Jahre
	end;
if  form_twahl.RadioButton_auswahl_dat.checked then
	begin
		datum_von:=strtodate(form_twahl.maskedit1.edittext) ;
		datum_bis:=strtodate(form_twahl.maskedit2.edittext);
  end;


if  form_twahl.RadioButton_auswahl.checked then
	begin
		datum_von:=now - form_twahl.spinedit_von.Value ;
		datum_bis:=now+ form_twahl.spinedit_bis.Value;
  end;



filter:=' where datum >= '+sql_datetostr(datum_von) + ' and datum <= '+ sql_datetostr(datum_bis)+' and i_status=1 ' ;


// if  form_twahl.radiogroup_firma.ItemIndex=1 then
		filter:=filter +' and i_firma in ('+firmen_filter+')';// ='+inttostr(ii_firma);

 if  not form_twahl.checkbox_archiv.Checked then
		filter:=filter +' and archiv<>"X"';



case form_twahl.RadioGroup_typ.itemindex of
	0: filter:='select firma.firma,firma.kuerzel ,untersuchung.* , name, vorname, i_firma,untersuchung,schicht_s_dat, i_schichtmodell , typ.dauer '+'from untersuchung left join mitarbeiter on (untersuchung.i_mitarbeiter =mitarbeiter.nummer) left join typ on (typ.nummer=untersuchung.i_typ) left join firma on (firma.nummer=mitarbeiter.i_firma)' +filter+ ' and untersuchung.storno=0';
	1: filter:='select firma.firma,firma.kuerzel,impfung.*, name, vorname, i_firma,untersuchung ,schicht_s_dat, i_schichtmodell,typ.dauer '+'from impfung left join mitarbeiter on(impfung.i_mitarbeiter =mitarbeiter.nummer) left join typ on (typ.nummer=impfung.i_typ) left join firma on (firma.nummer=mitarbeiter.i_firma) '+filter + ' and impfung.storno=0';
	2: filter:='select firma.firma,firma.kuerzel,labor.* , name, vorname, i_firma,untersuchung ,schicht_s_dat, i_schichtmodell,typ.dauer '+' from labor left join mitarbeiter on(labor.i_mitarbeiter =mitarbeiter.nummer) left join typ on (typ.nummer=labor.i_typ) left join firma on (firma.nummer=mitarbeiter.i_firma)'+filter + ' and labor.storno=0';
end;

filter :=filter +' order by datum,name,vorname';

datamodul.sql_new(false,datamodul.q_unt_filter,filter,'');

end;

procedure TForm_termine.SpinButton1DownClick(Sender: TObject);
begin
hauptdatum:=hauptdatum-365;
end;

procedure TForm_termine.SpinButton1UpClick(Sender: TObject);
begin
	hauptdatum:=hauptdatum+365 ;
end;


procedure tform_termine.statuslesen;
begin
end;

procedure TForm_termine.BitBtn_untersuchung_selectClick(Sender: TObject);
begin
	if form_twahl.Showmodal<>mrok then exit;
	unt_filtern;

end;

procedure TForm_termine.ToolButton_neuClick(Sender: TObject);
var
i,bereich:integer;
zeit_plan:ttime;
nr_termin,nr_unt,i_mitarbeiter:int64;
tab,query:string;
begin
pagecontrol_rechts.ActivePage:=tabsheet_detail ;
if pagecontrol_zeit.ActivePage=tabsheet_tag then dbgridext_tag.SetFocus;
i:=0;
while (i<8) and (not dbgrid_tage[i].focused) do inc(i);
if i=8 then
begin
showmessage('Bitte w�hlen Sie einen Tag aus');
exit; // keine Tabelle fokusiert
end;

 if form_namsuch.showmodal<> mrok then exit;
 i_mitarbeiter:=getbigint(datamodul.q_mitarbeiter_such,'nummer');
 try
   form_u_wahl:=tform_u_wahl.Create(self);
   form_u_wahl.auswahl_einstellen(0,1);
   if (form_u_wahl.showmodal<>mrok)  then exit;
   bereich:= form_u_wahl.RadioGroup.ItemIndex+1;
 finally
   form_u_wahl.release;
 end;

 nr_unt:=form_main.untersuchung_hinzufuegen(i_mitarbeiter, bereich,2,hauptdatum,false);

 q_tage[i].last;
	 try
		if q_tage[i]['zeit_e']<>null then zeit_plan:=q_tage[i]['zeit_e']
		else
		  if q_tage[i]['zeit_s']<>null then zeit_plan:=q_tage[i]['zeit_s']
		  else  zeit_plan:=strtotime(termine_startzeit);
	 except
    if termine_startzeit='' then termine_startzeit:='8:00';
		zeit_plan :=strtotime(termine_startzeit);
	 end;


  nr_termin:=neuer_datensatz(q_tage[i],['nummer','datum_s','zeit_s','i_untersuchung','i_art','memo'],[null,hauptdatum,zeit_plan,inttostr(nr_unt),bereich,'-']);
  if nr_termin=-1 then exit;
  form_termine.termin_info_erzeugen(q_tage[i]);
	q_tage[i].post;
  case bereich of
  1: tab:='untersuchung';
  2: tab:='impfung';
  3: tab:='labor';
  end;
  query:=format('update %s set i_termin=%s where nummer=%s',[tab,inttostr(nr_termin),inttostr(nr_unt)]);
  mysql_d.sql_exe(query);

 	case bereich of
  1:datamodul.q_untersuchung.refresh;
  2:datamodul.q_impfung.refresh;
  3:datamodul.q_labor.refresh;
  end;
  //form_termine.termin_info_erzeugen(q_tage[i]);


  kalender_akutalisieren;
  gehezutermin(nr_termin);
	formular_entsperren;

	//form_main.entsperren(tabsheet_detail);

end;

procedure TForm_termine.ToolButton_speichernClick(Sender: TObject);
begin
    termine_speichern(true);
    if pagecontrol_zeit.ActivePage=tabsheet_monat then zeichne_monat;
end;

procedure tform_termine.termine_speichern(direkt:boolean);
begin
with datamodul do
begin

 //form_main.sperren(tabsheet_detail);
 try
 if  (q_tage[akt_tabelle].state in [dsedit,dsinsert]) then
 begin
    termin_info_erzeugen(q_tage[akt_tabelle]);
    q_tage[akt_tabelle].post;
 end;
 except
 end;
 formular_sperren;
end;

end;



procedure tform_termine.termine_laden;
var
i: integer;
begin
with datamodul do
begin
  i:=0;
	while (not dbgrid_tage[i].focused) and (i<7) do inc(i);
end;
end;

procedure TForm_termine.ToolButton_editClick(Sender: TObject);
begin
pagecontrol_rechts.ActivePage:=tabsheet_detail ;
formular_entsperren;
//form_main.entsperren(tabsheet_detail);

end;

procedure TForm_termine.ToolButton_cancelClick(Sender: TObject);
begin
pagecontrol_rechts.ActivePage:=tabsheet_detail ;

q_tage[akt_tabelle].cancel ;
 formular_sperren;

end;

procedure TForm_termine.SpeedButton_datum_raufClick(Sender: TObject);
var
day, month, year:word;
begin
if pagecontrol_zeit.ActivePage=tabsheet_tag then
	hauptdatum:=hauptdatum+1;
if  pagecontrol_zeit.ActivePage=tabsheet_woche then
   hauptdatum:=hauptdatum+7;
if (pagecontrol_zeit.ActivePage=tabsheet_monat) or (pagecontrol_zeit.ActivePage=tabsheet_liste)  then
	begin
  decodedate(hauptdatum, year, month, day);
   if (month+1=2) and (day>28) then day:=28;
   if month+1=13 then hauptdatum:=encodedate(year+1, 1,day)
   else try
			   hauptdatum:=encodedate(year,month+1,day);
        except
	        hauptdatum:=encodedate(year,month+1,day-1);
        end;
   end;
	wochenkalender;
	kalenderfiltern;
  datumanzeigen;
  
if pagecontrol_zeit.ActivePage=tabsheet_monat then
   zeichne_monat;
end;

procedure TForm_termine.SpeedButton_datum_runterClick(Sender: TObject);
var
day, month, year:word;
begin
if pagecontrol_zeit.ActivePage=tabsheet_tag then
	hauptdatum:=hauptdatum-1;

if  pagecontrol_zeit.ActivePage=tabsheet_woche then
	 hauptdatum:=hauptdatum-7;
if (pagecontrol_zeit.ActivePage=tabsheet_monat) or (pagecontrol_zeit.ActivePage=tabsheet_liste) then
	begin
      decodedate(hauptdatum, year, month, day);
      if (month-1=2) and (day>28) then day:=28;
      if month-1=0 then hauptdatum:=encodedate(year-1, 12,day)
      else
      try
         hauptdatum:=encodedate(year,month-1,day);
      except
         hauptdatum:=encodedate(year,month-1,day-1);
      end;
	end;

	wochenkalender;
	kalenderfiltern;
	Datumanzeigen;
if pagecontrol_zeit.ActivePage=tabsheet_monat then
	zeichne_monat;

end;


procedure tform_termine.kalenderfiltern;
var
	data: pnodepointer;
	nummer,i_konto:integer;
	sql_txt,q_konto:string;
	i:integer;
	monatsbeginn, monatsende:tdate;
	year, month,day:word;
  nr64:int64;
begin
with form_termine_filtern do
begin
	if (ComboBox_untersucher.Itemindex<0) or
		{(radiogroup_firma.ItemIndex=0) or}
		(ComboBox_terminart.Itemindex<0)   then exit;

  if form_termine_filtern.PageControl_ansicht.ActivePage=form_termine_filtern.TabSheet_Zeiterfassung then
  begin
    modus:=2;
    MainMenu_termin.Items.Enabled:=false;
    form_termine.caption:='Zeiterfassung';
    panel_zeitplan1.zeit_min:=1;
    panel_zeitplan1.zeit_max:=12;
    pagecontrol_zeit.ActivePage:=tabsheet_monat;
    TabSheet_tag.Tabvisible:=false;
    tabsheet_woche.tabVisible:=false;
    tabsheet_liste.tabVisible:=false;

    pagecontrol_rechts.ActivePage:=TabSheet_zeit;
    TabSheet_zeit.TabVisible:=true;
    TabSheet_terminieren.TabVisible:=false;
    TabSheet_detail.TabVisible:=false;
    toolbutton_neu.Enabled:=false;
    toolbutton_edit.Enabled:=false;
    toolbutton_schicht.Enabled:=false;
  end
  else //Planen
  begin
    modus:=1;
      MainMenu_termin.Items.Enabled:=true;
    form_termine.caption:='Termin�bersicht';
     //pagecontrol_zeit.ActivePage:=tabsheet_monat;
     panel_zeitplan1.zeit_min:=7;
    panel_zeitplan1.zeit_max:=19;
     TabSheet_tag.tabVisible:=true;
     tabsheet_woche.tabVisible:=true;
     tabsheet_liste.tabVisible:=true;

     pagecontrol_rechts.ActivePage:=TabSheet_detail;
     TabSheet_zeit.TabVisible:=false;
    TabSheet_terminieren.TabVisible:=true;
    TabSheet_detail.TabVisible:=true;
    toolbutton_neu.Enabled:=true;
    toolbutton_edit.Enabled:=true;
    toolbutton_schicht.Enabled:=true;
  end;


   details_tabelle(-1);

	{data:=pnodepointer(ComboBox_untersucher.Items.Objects[ComboBox_untersucher.Itemindex]);
	nummer:=data^.nummer;
	if checkbox_alleunt.Checked then p_untersucher:=''
		else p_untersucher:=' and i_untersucher='+inttostr(nummer); }

	if radiogroup_firma.ItemIndex=0 then p_firma:=''
		else p_firma:=' and i_firma in ('+firmen_filter+')';

  nr64:= form_main.combowert(form_termine_filtern.ComboBox_untersucher);
   if nr64>0 then  p_untersucher:=' and i_untersucher='+inttostr(nr64) else p_untersucher:='';


	decodedate(hauptdatum, year, month,day);
	monatsbeginn:=encodedate(year, month,1);
	if month=12 then monatsende:=encodedate(year+1,1,1)
		else monatsende:=encodedate(year, month+1,1);

  if modus=2 then
  begin


   i_konto:=form_main.combowert(form_termine_filtern.ComboBox_konto);
   if i_konto>0 then q_konto:= ' and i_zeitkonto='+inttostr(i_konto) else q_konto:='';

    sql_txt:=format('select * from firma_sollist where datum>=%s and datum<=%s %s %s %s order by datum',[sql_datetostr(monatsbeginn),sql_datetostr(monatsende),p_untersucher,p_firma,q_konto]);
    
  end
  else //Planen
  begin

    nummer:=form_main.combowert(form_termine_filtern.ComboBox_terminart);
    if nummer>0 then  p_art:=' and i_art='+inttostr(nummer) else  p_art:='';

    for i:=0 to 7 do   //wochentage
    begin
        sql_txt:='select * from termine where datum_s= '
          +sql_datetostr(wochendaten[i])+p_untersucher+p_firma+p_art + ' order by "zeit_s"' ;

        datamodul.sql_new(false,q_tage[i],sql_txt,'');

    end;


     sql_txt:='select * from termine where datum_s>= '+sql_datetostr(monatsbeginn)+
	    ' and datum_s< '+sql_datetostr(monatsende)+p_untersucher+p_firma+p_art + ' order by "datum_s", "zeit_s"' ;
  end;

	datamodul.sql_new(false,datamodul.q_termin_monat,sql_txt,'');

  if pagecontrol_zeit.ActivePage=tabsheet_tag then  details_tabelle(0);
	 if pagecontrol_zeit.ActivePage=tabsheet_woche then details_tabelle(wochentag(hauptdatum));
	 if pagecontrol_zeit.ActivePage=tabsheet_monat then  details_tabelle(9);

end;
end;

procedure TForm_termine.CheckBox_alleuntClick(Sender: TObject);
begin
kalenderfiltern;
end;

procedure tform_termine.wochezeichnen;
var
breite, hoehe: integer;
i:integer;
begin
application.ProcessMessages;
try
	 breite:=tabsheet_woche.Width;
	 hoehe:=tabsheet_woche.Height;
	 for i:=1 to 3
	 do begin
		 groupbox_tage[i].top:=(i-1)*(hoehe div 3);
		 groupbox_tage[i].left:=0;
		 groupbox_tage[i].width :=(breite div 2) ;
		 groupbox_tage[i].height:=(hoehe div 3);

		 groupbox_tage[i+3].top:=(i-1)*hoehe div 3;
		 groupbox_tage[i+3].left:=breite div 2;
		 groupbox_tage[i+3].width :=(breite div 2) ;
		 groupbox_tage[i+3].height:=(hoehe div 3);
	 end;

	 groupbox_tage[6].height:=(hoehe div 6);
	 groupbox_tage[7].top:=2*(hoehe div 3)+hoehe div 6 ;
	 groupbox_tage[7].left:=breite div 2;
	 groupbox_tage[7].width :=(breite div 2) ;
	 groupbox_tage[7].height:=(hoehe div 6);

	 for i:=1 to 7 do
	 begin
		dbgrid_tage[i].columns[0].width:=35;
		dbgrid_tage[i].columns[1].width:=16;
		dbgrid_tage[i].columns[2].width:=35;
		dbgrid_tage[i].columns[3].width:=(breite div 2)-110; //24
	 end;
	dbgrid_tage[0].columns[0].width:=35;
	dbgrid_tage[0].columns[1].width:=35;
	dbgrid_tage[0].columns[2].width:=16;
	dbgrid_tage[0].columns[3].width:=40;
	dbgrid_tage[0].columns[4].width:=(tabsheet_tag.Width )-150; //24
except
end;
end;


procedure TForm_termine.FormCreate(Sender: TObject);
var
i:integer;
query:string;
begin
if prg_typ=3 then
begin
	dbcombo_num_art.Visible:=false;
	label_t_art.visible:=false;
	tabsheet_terminieren.Caption:='geplante Behandlung terminieren';
	bitbtn_untersuchung_select.Caption:='Behandlung selektieren';
	dbgridext_tag.columns[3].title.caption:='K-Gruppe';
end;

if prg_typ=2 then
begin
     toolbutton_schicht.Visible:=false;
end;


  dbgridext_tag.columns[0].fieldname:='zeit_s';
  dbgridext_tag.columns[1].fieldname:='zeit_e';
  dbgridext_mo.columns[0].fieldname:='zeit_s';
  dbgridext_di.columns[0].fieldname:='zeit_s';
  dbgridext_mi.columns[0].fieldname:='zeit_s';
  dbgridext_do.columns[0].fieldname:='zeit_s';
  dbgridext_fr.columns[0].fieldname:='zeit_s';
  dbgridext_sa.columns[0].fieldname:='zeit_s';
  dbgridext_so.columns[0].fieldname:='zeit_s';

  width:=form_main.width;
  height:=form_main.Height;
  neu_lesen:=true;
  form_main.form_positionieren(tform(sender));

  grid_einstellen(dbgridext_unt_filter);
  query:=format('select * from firma where nummer in (%s)',[firmen_berechtigung]);
  datamodul.sql(true,datamodul.q_2, query,'firma');
  panel_zeitplan1.fmonats_feiertage:=monats_feiertage;
  dbgridext_unt_filter.bmp_Verzeichnis:=bildverzeichnis;
  //form_main.form_positionieren(tform(sender));

  form_main.combolesen(form_termine_filtern.ComboBox_untersucher, 'untersucher','untersucher','untersucher','alle');
  form_main.combogehezu( form_termine_filtern.ComboBox_untersucher,akt_untersucher);
  form_main.combolesen(form_termine_filtern.combobox_terminart,'art','name','','alle');
  form_main.combolesen(form_termine_filtern.ComboBox_konto,'abr_zeitkonto','name','','alle');
  dbcombo_num_art.lookup_laden;

  pagecontrol_rechts.ActivePage:=tabsheet_detail;
  pagecontrol_zeit.ActivePage:=tabsheet_tag;
  form_terminieren_drag_radiogroup_index:=0;


  q_tage[0]:=datamodul.q_termin0;
  q_tage[1]:=datamodul.q_termin1;
  q_tage[2]:=datamodul.q_termin2;
  q_tage[3]:=datamodul.q_termin3;
  q_tage[4]:=datamodul.q_termin4;
  q_tage[5]:=datamodul.q_termin5;
  q_tage[6]:=datamodul.q_termin6;
  q_tage[7]:=datamodul.q_termin7;
  q_tage[8]:=datamodul.q_termin;
  q_tage[9]:=datamodul.q_termin_monat;

  source_tage[0]:=datamodul.ds_termin0;
  source_tage[1]:=datamodul.ds_termin1;
  source_tage[2]:=datamodul.ds_termin2;
  source_tage[3]:=datamodul.ds_termin3;
  source_tage[4]:=datamodul.ds_termin4;
  source_tage[5]:=datamodul.ds_termin5;
  source_tage[6]:=datamodul.ds_termin6;
  source_tage[7]:=datamodul.ds_termin7;
  source_tage[8]:=datamodul.ds_termin;
  source_tage[9]:=datamodul.ds_termin_monat;

  wochentage[1]:='Montag';
  wochentage[2]:='Dienstag';
  wochentage[3]:='Mittwoch';
  wochentage[4]:='Donnerstag';
  wochentage[5]:='Freitag';
  wochentage[6]:='Samstag';
  wochentage[7]:='Sonntag';


  dbgrid_tage[0]:=dbgridext_tag;
  dbgrid_tage[1]:=dbgridext_mo;
  dbgrid_tage[2]:=dbgridext_di;
  dbgrid_tage[3]:=dbgridext_mi;
  dbgrid_tage[4]:=dbgridext_do;
  dbgrid_tage[5]:=dbgridext_fr;
  dbgrid_tage[6]:=dbgridext_sa;
  dbgrid_tage[7]:=dbgridext_so;

  groupbox_tage[1]:=groupbox_mo;
  groupbox_tage[2]:=groupbox_di;
  groupbox_tage[3]:=groupbox_mi;
  groupbox_tage[4]:=groupbox_do;
  groupbox_tage[5]:=groupbox_fr;
  groupbox_tage[6]:=groupbox_sa;
  groupbox_tage[7]:=groupbox_so;

  setlength(panel_zeitplan1.arfarbe,10);
  panel_zeitplan1.arfarbe[0]:=clwhite;
  panel_zeitplan1.arfarbe[1]:=clred;
  panel_zeitplan1.arfarbe[2]:=clgreen;
  panel_zeitplan1.arfarbe[3]:=claqua;
  panel_zeitplan1.arfarbe[4]:=clyellow;
  panel_zeitplan1.arfarbe[5]:=clfuchsia;
  panel_zeitplan1.arfarbe[6]:=cllime;


  wochezeichnen;

  form_main.sperren(tabsheet_detail);
  formular_sperren;

  hauptdatum:=date();
  for i:=0 to 7 do  dbgrid_tage[i].bmp_Verzeichnis:=bildverzeichnis;
  dbgridext_liste.bmp_Verzeichnis:=bildverzeichnis;
  dbgridext_unt_filter.bmp_Verzeichnis:=bildverzeichnis;
  statuslesen;
  datumanzeigen;
  PageControl_zeitChange(Sender);

  //PageControl_zeitChange(Sender);

  DBCombo_num_z_erbringer.lookup_laden;
  DBCombo_num_z_abrechnungskriterium.lookup_laden;
  DBCombo_num_z_zeitkonto.lookup_laden;
end;

procedure TForm_termine.Splitter1Moved(Sender: TObject);
begin
wochezeichnen;
end;

procedure tform_termine.wochenkalender; //erstellt f�r die einzelnen kalender das Datum
var
	  datum,datum_neu:tdatetime;
	  i:integer;
begin
	datum:=hauptdatum;
	datum_neu:=datum;
	wochendaten[0]:=datum;
	wochendaten[wochentag(datum_neu)]:=datum_neu;
	datum_neu:=datum_neu-1;
	while (wochentag(datum_neu) >=1) and (wochentag(datum_neu) <7) do
	 begin
		wochendaten[wochentag(datum_neu)]:=datum_neu;
		datum_neu:=datum_neu-1;
	end;
	datum_neu:=datum+1;
	while (wochentag(datum_neu) >1) and (wochentag(datum_neu)<=7) do
	 begin
		wochendaten[wochentag(datum_neu)]:=datum_neu;
		datum_neu:=datum_neu+1;
	end;
	for i:=1 to 7 do
	begin
		groupbox_tage[i].caption:=wochentage[i]+ ', '+datetostr(wochendaten[i]);
	end;

end;

function tform_termine.wochentag(datum:tdatetime): integer;
begin
result:=dayofweek(datum)-1;
if result=0 then result:=7;

end;

{function tform_termine.kalenderwoche(datum:tdatetime):integer;
var
year, month, day:word;
kw,i:integer;
erstmon:tdate;
begin
	decodedate(datum,year, month, day);
	I:=1;
	erstmon:=encodedate(year,1,i);
	while dayofweek(erstmon)<>2 do
	begin
		inc(i);
		erstmon:=encodedate(year,1,i);
	end;
	if datum<erstmon then result:=1
	else result:=(trunc(datum-erstmon) div 7) +2;
end;}

function tform_termine.kalenderwoche(datum:tdatetime):integer;
var
  AYear,dummy :word;
  First       :TDateTime;
begin
  DecodeDate(Datum+((8-DayOfWeek(Datum)) mod 7)-3, AYear, dummy, dummy);
  First :=EncodeDate(AYear, 1, 1);
  Result:=(trunc(Datum-First-3+(DayOfWeek(First)+1) mod 7) div 7)+1;
end;

procedure TForm_termine.DBgridEXT_Enter(Sender: TObject);
var i:integer;
begin
	 i:=0;
	 while (not dbgrid_tage[i].focused) and (i<=9) do inc(i);

	 hauptdatum:=wochendaten[i];
	 wochendaten[0]:=wochendaten[i];
	 details_tabelle(i);
	 datumanzeigen;
end;

procedure TForm_termine.PageControl_zeitChange(Sender: TObject);
procedure neu_true;
begin
 toolbutton_neu.enabled:=true;
 m_neu.Enabled:=true;
end;

procedure neu_false;
begin
 toolbutton_neu.enabled:=false;
 m_neu.Enabled:=false;
end;

begin
wochenkalender;
kalenderfiltern;
all_del.Enabled:=false;
druck.enabled:=false;
email.enabled:=false;

if pagecontrol_zeit.ActivePage=tabsheet_woche then
begin
	neu_true;
	tabsheet_terminieren.tabVisible:=true;
	akt_tabelle:=wochentag(hauptdatum);
	druck.enabled:=true;
	email.enabled:=true;
  Tagesplan1.visible:=false;
	if form_termine.visible then dbgrid_tage[akt_tabelle].setfocus;
end;
if pagecontrol_zeit.ActivePage=tabsheet_tag then
begin
	neu_true;
	tabsheet_terminieren.tabVisible:=true;
	akt_tabelle:=0;
	druck.enabled:=true;
	email.enabled:=true;
  Tagesplan1.visible:=true;
	if (form_termine<>nil) and (form_termine.visible) then dbgridext_tag.SetFocus;
end;
if pagecontrol_zeit.ActivePage=tabsheet_monat then
	begin
		neu_false;
		pagecontrol_rechts.ActivePage:=tabsheet_detail;
		tabsheet_terminieren.tabVisible:=false;
    Tagesplan1.visible:=false;
		application.ProcessMessages;
		akt_tabelle:=9;
		zeichne_monat;
	end;
if pagecontrol_zeit.ActivePage=tabsheet_liste then
	begin
		neu_false;
		pagecontrol_rechts.ActivePage:=tabsheet_detail;
		tabsheet_terminieren.tabVisible:=false;
    Tagesplan1.visible:=false;
		datamodul.q_termin0.Filtered:=false;
		all_del.Enabled:=true;
		druck.enabled:=true;
		email.enabled:=true;
		grid_einstellen(dbgridext_liste);

	end;
end;


procedure tform_termine.zeichne_monat;
var
art:integer;
zeitstart,zeitende:ttime;
 year, month, day,day_alt:word;
firma:string;
begin

panel_zeitplan1.shape_aktuell:=nil;
panel_zeitplan1.alle_termine_loeschen;
panel_zeitplan1.datum:=hauptdatum;
//Jahresfeiertage(hauptdatum);
//monatsfeiertage(hauptdatum);


with datamodul do
begin
		q_termin_monat.First;
		while not q_termin_monat.eof do
		begin
     if modus=2 then
      begin
        firma:=firmenname(datamodul.q_termin_monat.findfield('i_firma').asinteger );
        decodedate(q_termin_monat['datum'],year, month, day);
        if day <>day_alt then zeitstart:=strtotime('01:00');
        zeitende:=zeitstart+strtotime(format('%d:%d',[q_termin_monat.findfield('stunden').asinteger,q_termin_monat.findfield('minuten').asinteger]));
        panel_zeitplan1.termin_zeichnern(day,zeitstart,zeitende, firma,getbigint(q_termin_monat,'nummer'),1 );
        day_alt:=day;
        zeitstart:=zeitende;
      end
      else // planung
      begin
			  if q_termin_monat['zeit_s']<>null then
			  begin
				  decodedate(q_termin_monat['datum_s'],year, month, day);
				  if q_termin_monat['zeit_e']=null then zeitende:=q_termin_monat['zeit_s']+0.02 else
				      zeitende:=q_termin_monat['zeit_e'];
				  if q_termin_monat['firma']=null then firma:='' else firma:=q_termin_monat['firma'];
				  if q_termin_monat['i_art']=null then art:=0 else art :=q_termin_monat['i_art'];
					panel_zeitplan1.termin_zeichnern(day,q_termin_monat['zeit_s'],zeitende, firma,getbigint(q_termin_monat,'nummer'),art );
			  end;
       end;

			  q_termin_monat.next;
		end;
		q_termin_monat.first;
end;
end;


procedure tform_termine.details(art:integer);
begin
if art<=3 then
	begin
		DBmemo_anlass.Visible:=true;
		DBEdit_vorname.Visible:=true;
		DBEdit_nachname.Visible:=true;
		Label_typ.Visible:=true;
		Label_vorname.Visible:=true;
		Label_nachname.Visible:=true;
	end
	else
	begin
		DBmemo_anlass.Visible:=false;
     DBEdit_vorname.Visible:=false;
		DBEdit_nachname.Visible:=false;
     Label_typ.Visible:=false;
     Label_vorname.Visible:=false;
     Label_nachname.Visible:=false;
    
  end;
end;

procedure tform_termine.details_tabelle(day:integer);
begin
if day=-1 then
begin
dbedit_zeit_s.DataSource:=nil;
    dbedit_zeit_e.DataSource:=nil;
    dbedit_datum_s.DataSource:=nil;
    dbedit_vorname.DataSource:=nil;
    dbedit_nachname.DataSource:=nil;
    dbcombo_num_art.DataSource:=nil;
    dbmemo_anlass.DataSource:=nil;
    dbmemo_detail.DataSource:=nil;

     dbedit_z_date.DataSource:=nil;
    dbedit_z_beginn.DataSource:=nil;
    dbedit_z_ende.DataSource:=nil;
    dbedit_z_stunden.DataSource:=nil;
    dbedit_z_minuten.DataSource:=nil;
    DBCombo_num_z_erbringer.DataSource:=nil;
    DBCombo_num_z_zeitkonto.DataSource:=nil;
    DBCombo_num_z_abrechnungskriterium.DataSource:=nil;
    DBComboBox_z_zeitansatz.DataSource:=nil;
    DBCheckBox_z_zusatz.DataSource:=nil;
    DBMemo_z_aufgabe.DataSource:=nil;
    DBMemo_z_aufwand.DataSource:=nil;
    DBMemo_z_anmerkung.DataSource:=nil;
    exit;
end;

 if modus=2 then
  begin
    dbedit_zeit_s.DataSource:=nil;
    dbedit_zeit_e.DataSource:=nil;
    dbedit_datum_s.DataSource:=nil;
    dbedit_vorname.DataSource:=nil;
    dbedit_nachname.DataSource:=nil;
    dbcombo_num_art.DataSource:=nil;
    dbmemo_anlass.DataSource:=nil;
    dbmemo_detail.DataSource:=nil;

    dbedit_z_date.DataSource:=datamodul.ds_termin_monat;
    dbedit_z_beginn.DataSource:=datamodul.ds_termin_monat;
    dbedit_z_ende.DataSource:=datamodul.ds_termin_monat;
    dbedit_z_stunden.DataSource:=datamodul.ds_termin_monat;
    dbedit_z_minuten.DataSource:=datamodul.ds_termin_monat;
    DBCombo_num_z_erbringer.DataSource:=datamodul.ds_termin_monat;
    DBCombo_num_z_zeitkonto.DataSource:=datamodul.ds_termin_monat;
    DBCombo_num_z_abrechnungskriterium.DataSource:=datamodul.ds_termin_monat;
    DBComboBox_z_zeitansatz.DataSource:=datamodul.ds_termin_monat;
    DBCheckBox_z_zusatz.DataSource:=datamodul.ds_termin_monat;
    DBMemo_z_aufgabe.DataSource:=datamodul.ds_termin_monat;
    DBMemo_z_aufwand.DataSource:=datamodul.ds_termin_monat;
    DBMemo_z_anmerkung.DataSource:=datamodul.ds_termin_monat;


  end
  else
  begin
    dbedit_z_date.DataSource:=nil;
    dbedit_z_beginn.DataSource:=nil;
    dbedit_z_ende.DataSource:=nil;
    dbedit_z_stunden.DataSource:=nil;
    dbedit_z_minuten.DataSource:=nil;
    DBCombo_num_z_erbringer.DataSource:=nil;
    DBCombo_num_z_zeitkonto.DataSource:=nil;
    DBCombo_num_z_abrechnungskriterium.DataSource:=nil;
    DBComboBox_z_zeitansatz.DataSource:=nil;
    DBCheckBox_z_zusatz.DataSource:=nil;
    DBMemo_z_aufgabe.DataSource:=nil;
    DBMemo_z_aufwand.DataSource:=nil;
    DBMemo_z_anmerkung.DataSource:=nil;

    dbedit_zeit_s.DataSource:=source_tage[day];
    dbedit_zeit_s.dataField:='zeit_s';
    dbedit_zeit_e.DataSource:=source_tage[day];
    dbedit_zeit_e.dataField:='zeit_e';
    dbedit_datum_s.DataSource:=source_tage[day];
    dbedit_datum_s.datafield:='datum_s';
    //dbedit_datum_e.DataSource:=source_tage[day];
    //dbedit_datum_e.datafield:='datum_e';
    dbedit_vorname.DataSource:=source_tage[day];
    dbedit_vorname.datafield:='vorname';
    dbedit_nachname.DataSource:=source_tage[day];
    dbedit_nachname.datafield:='name';
    dbcombo_num_art.DataSource:=source_tage[day];
    dbcombo_num_art.datafield:='i_art';
    dbmemo_anlass.DataSource:=source_tage[day];
    dbmemo_anlass.datafield:='info';
    dbmemo_detail.DataSource:=source_tage[day];
    dbmemo_detail.DataField:='memo';

    akt_tabelle:=day;
    if q_tage[day]['i_art']<>null then details(q_tage[day]['i_art']);
  end;
  //dbmemo.DataSource:=source_tage[day];
  //dbmemo.datafield:='anmerkung';

end;
procedure TForm_termine.ToolButton_loeschenClick(Sender: TObject);
var i:integer;
begin
pagecontrol_rechts.ActivePage:=tabsheet_detail ;
if pagecontrol_zeit.ActivePage=tabsheet_liste then
   begin
		if  (MessageDlg('L�schen der markierten Datens�tze?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes ) then exit;
		for i:= 0 to dbgridext_liste.SelectedRows.count-1 do
     begin
		  try
		  datamodul.q_termin_monat.GotoBookmark(tbookmark(dbgridext_liste.SelectedRows.items[i]));
		  if getbigint(datamodul.q_termin_monat,'i_untersuchung')>0 then
				 unt_status_aendern(getbigint(datamodul.q_termin_monat,'i_untersuchung'), datamodul.q_termin_monat['i_art']);
		  //dateiloeschen(datamodul.q_termin_monat,true,false);//

       //datamodul.q_termin_monat.Delete;
       //dbgridext_liste.Refresh;
		  //kalender_akutalisieren;
       except
       end;
     end;
     dbgridext_liste.SelectedRows.Delete;
     kalender_akutalisieren;
   end
else
begin
	if  (MessageDlg('L�schen des Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes ) then exit;

	if getbigint(q_tage[akt_tabelle],'i_untersuchung')>0 then
				 unt_status_aendern(getbigint(q_tage[akt_tabelle],'i_untersuchung'), q_tage[akt_tabelle]['i_art']);
	 dateiloeschen(q_tage[akt_tabelle],true,false);// q_tage[akt_tabelle].delete;
	 kalender_akutalisieren;
	 zeichne_monat;
end;
 //form_main.sperren(tabsheet_detail);
 formular_sperren;

end;

procedure TForm_termine.ToolButton_exitClick(Sender: TObject);
begin
termine_speichern(false);
modalresult:=mrok;
end;


procedure TForm_termine.DBgridEXT_DragOver(Sender, Source: TObject; X,
	Y: Integer; State: TDragState; var Accept: Boolean);
begin
	 if tcontrol(sender) <>tcontrol(source) then  Accept :=true else
	 accept:=false;

end;

procedure TForm_termine.DBgridEXT_DragDrop(Sender, Source: TObject; X,
	Y: Integer);
var
i,j,i_firma,i_unt_art:integer;
	data: pnodepointer;
 new_nummer,i_untersuchung, i_mitarbeiter:int64;
	zeit_plan,t1,diff:ttime;
	s_nummer,sql_text,mem:string;
  verschickt:boolean;
  bm:tbookmark;
begin
	 with datamodul do
	 begin
	 if not (source is tdbgridext) then  exit;// ein Knoten wurde auf den bereich gezogen
	// if datamodul.q_unt_filter['nummer']=null then exit; //kein DS zum ziehen
	 j:=0;//
	 while (not (tdbgridext(source)=dbgrid_tage[j])) and (j<7) do inc(j);
	 if tdbgridext(source)<>dbgrid_tage[j] then j:=0; //wenn 0 dann Ziehen aus unt-filtern


	 i:=0;
	 while (not (tdbgridext(sender)=dbgrid_tage[i])) and (i<7) do inc(i);
	 //while (not dbgrid_tage[i].focused) nase von anne and (i<7) do inc(i);


	 dbgrid_tage[i].setfocus;
	 if j=0 then
		if  datamodul.q_unt_filter['dauer']<>null
		then
		  diff:=datamodul.q_unt_filter['dauer']//*0.00069444444444
		else diff:=0
	 else diff:=q_tage[j]['zeit_e']-q_tage[j]['zeit_s'];
   //q_tage[i].first;
	 if q_tage[i]['zeit_e']<>null then t1:=q_tage[i]['zeit_e']
	 else t1:=strtotime(termine_startzeit);//strtotime('7:0');    //startzeit

	 q_tage[i].next;

	 while not q_tage[i].eof do
	 begin
		 if q_tage[i]['zeit_s']-t1>=diff then
			begin
				q_tage[i].prior;
				break;
			end;
		 t1:=q_tage[i]['zeit_e'];
		 q_tage[i].next;
	 end;

	 //q_tage[i].last;
	 try
		if q_tage[i]['zeit_e']<>null then zeit_plan:=q_tage[i]['zeit_e']
		else
		  if q_tage[i]['zeit_s']<>null then zeit_plan:=q_tage[i]['zeit_s']
		  else  zeit_plan:=strtotime(termine_startzeit);
	 except
		zeit_plan:=strtotime(termine_startzeit);
	 end;

	 hauptdatum:=wochendaten[i];
	 wochendaten[0]:=wochendaten[i];
	 details_tabelle(i);

	 if j=0 then  //terminieren
	 try
		form_terminieren_drag:=tform_terminieren_drag.Create(self);
     form_terminieren_drag.radiogroup.itemindex:=form_terminieren_drag_radiogroup_index;
		if datamodul.q_unt_filter['vorname']<>null then
			form_terminieren_drag.Label_Name.Caption:=datamodul.q_unt_filter['vorname']
     else
			form_terminieren_drag.Label_Name.Caption:='';
		if datamodul.q_unt_filter['name']<>null then form_terminieren_drag.Label_Name.Caption:=
			datamodul.q_unt_filter['name'];
     if datamodul.q_unt_filter['untersuchung']<>null then form_terminieren_drag.Label_typ.Caption:=
			datamodul.q_unt_filter['untersuchung'];

		form_terminieren_drag.maskedit1.edittext:=timetostr(zeit_plan);

		Form_terminieren_drag.Label_termin.caption:={Form_terminieren_drag.Label_termin.caption+', '+ }formatdatetime('dddd",  "ddddd',hauptdatum);



		if Form_terminieren_drag.showmodal<>mrok then   //Formular mit OK best�tigt
			begin
			form_terminieren_drag.release;
			exit;
			end;
		//zeit festgelegt

		//i_mitarbeiter:=getbigint(q_unt_filter,'i_mitarbeiter');
		//i_firma:=getbigint(q_unt_filter,'i_firma');
		i_untersuchung:=getbigint(q_unt_filter,'nummer');
		i_unt_art:=1; //untersuchung impfung oder labor
		sql_text:=lowercase(q_unt_filter.sql.text);
		if pos('from impfung', sql_text)>0 then i_unt_art:=2;
		if pos('from labor',sql_text)>0 then i_unt_art:=3;
    mem:=form_terminieren_drag.memo.text+'-'; //+' ' notwendig da sonst edit des Termins ohne �nderung zum Fehler f�rht ???????
		new_nummer:=neuer_datensatz(q_tage[i],
			['nummer','i_untersuchung','datum_s','datum_s','zeit_s','zeit_e', 'i_art','i_typ','memo']
			  ,[null,inttostr(i_untersuchung), hauptdatum ,hauptdatum,strtotime(form_terminieren_drag.maskedit1.edittext),
				strtotime(form_terminieren_drag.maskedit2.edittext),i_unt_art ,integer(q_unt_filter['i_typ']),mem]);

			//q_tage[i].edit;

			termin_info_erzeugen(q_tage[i]);
			q_tage[i].post;
 			//s_nummer:= getbigint_str(q_unt_filter,'nummer');
      bm:=q_tage[i].GetBookmark;
      q_tage[i].Refresh;
      q_tage[i].GotoBookmark(bm);
      q_tage[i].FreeBookmark(bm);
      i_untersuchung:= getbigint(q_unt_filter,'nummer');

      if form_terminieren_drag.RadioGroup.itemindex in [0,1] then verschickt:=true;

      uil_terminieren(i_untersuchung,i_unt_art,hauptdatum,new_nummer,verschickt);


		if form_terminieren_drag.RadioGroup.itemindex =0 then  //email
			form_main.einladung_drucken(getbigint(q_tage[i],'nummer'),1);
		if form_terminieren_drag.RadioGroup.itemindex =1 then  //drucken
			form_main.einladung_drucken(getbigint(q_tage[i],'nummer'),0);

     //application.BringToFront;
     arbene_vor;
    try //hier evtl bookamrkerror????
		q_unt_filter.refresh; //beide refr. notwendig
    except
    end;
		//q_tage[i].refresh;
	 finally
		form_terminieren_drag_radiogroup_index:=form_terminieren_drag.radiogroup.itemindex;
		form_terminieren_drag.Release ;
	 end

	 else  //ziehen aus tagen zum datumswechsel

	 begin
		q_tage[j].edit;
		q_tage[j]['datum_s']:=hauptdatum;
		q_tage[j]['zeit_e']:=t1+diff;
		q_tage[j]['zeit_s']:=t1;
		direktspeichern:=true;
     form_termine.termin_info_erzeugen(q_tage[i]);
     q_tage[j].post;
     i_untersuchung:=getbigint(q_tage[j],'i_untersuchung');
     i_unt_art:=q_tage[j]['i_art'];
     new_nummer:=getbigint(q_tage[j],'nummer');
     uil_terminieren(i_untersuchung,i_unt_art,hauptdatum,new_nummer,false);
     q_tage[i].refresh;
     //q_tage[i].locate('nummer',inttostr(new_nummer),[]);
     q_tage[i].locate('nummer',(new_nummer),[]);
     q_tage[j].refresh;


     //kalenderfiltern;
   end;
   end;
end;


procedure tform_termine.uil_terminieren(uil_nummer: int64;i_unt_art:integer;datum:tdate;termin_nummer:int64;verschickt:boolean);
var query, tab,m_dat, m_dat_einladung:string;
begin
     m_dat:=sql_datetostr(datum);
     m_dat_einladung:=sql_datetostr(date());
     case i_unt_art of
			1: tab:='untersuchung';
			2: tab:='impfung';
			3: tab:='labor';
		end;
     if verschickt then
     query:=format('update %s set i_status=2, i_termin=%s, datum=%s, datum_einladung=%s where nummer=%s  ',[tab,inttostr(termin_nummer),m_dat,m_dat_einladung , inttostr(uil_nummer)])
     else
     query:=format('update %s set i_status=2, i_termin=%s, datum=%s where nummer=%s  ',[tab,inttostr(termin_nummer),m_dat, inttostr(uil_nummer)]);

     mysql_d.sql_exe(query);

		case i_unt_art of
			1: datamodul.q_untersuchung.Refresh;
			2: datamodul.q_impfung.Refresh;
			3: datamodul.q_labor.Refresh;
		end;

end;

procedure tform_termine.infoerzeugen(day:integer);
var
 typ, nachName, vorname, firma,query: string;
begin
with datamodul do
try
		if kein_datensatz( q_tage[day]) then exit;
		if not (q_tage[day].state in [dsedit, dsinsert ]) then exit;
		query:=format('select * from firma where nummer in (%s)',[firmen_berechtigung]);
		sql(true, q_2,query,'nummer');
		q_2.first;
		//if q_2.locate('nummer',getbigint_str(q_tage[day],'i_firma'),[]) then firma :=q_2['kuerzel'] else firma:='';
    if q_2.locate('nummer',getbigint(q_tage[day],'i_firma'),[]) then firma :=q_2['kuerzel'] else firma:='';
		q_typ.first;
		if q_typ.locate('nummer',q_tage[day]['i_typ'],[]) then typ :=q_typ['untersuchung'] else typ:='';
		if q_tage[day]['name']<>null then nachname:= q_tage[day]['name'] else nachname :='';
		if q_tage[day]['vorname']<>null then vorname:= trim(copy(q_tage[day]['vorname'],1,1))+', ' else vorname :='';
		q_tage[day].edit ;
		q_tage[day]['firma']:=firma;
		q_tage[day]['info']:=(vorname+nachname+' / '+typ+' / '+q_tage[day]['memo']);
		if q_tage[day]['i_art']=4 then q_tage[day]['farbe']:=2 else q_tage[day]['farbe']:=null ;
		q_art.first;
		q_art.locate('nummer',q_tage[day]['i_art'],[]);
		q_tage[day]['bmp_nr']:=datamodul.q_art['bmp_nr'];
		//direktspeichern:=true;
		//####q_tage[day].applyupdates ;

finally
end;

end;

procedure tform_termine.termin_info_erzeugen(tabelle:tdataset);
var
typ, nachName, vorname,firma,query,tab,s_mitarbeiter,mem: string;
i_art:integer;
s_untersuchung:string;
felder1 :array [0..1] of string;
felder2 :array [0..2] of string;
begin
with datamodul do
try
		if not (tabelle.state in [dsedit, dsinsert ]) then exit;
     if kein_datensatz( tabelle) then exit;

     i_art:=tabelle.findfield('i_art').asinteger;
     s_untersuchung:=tabelle.findfield('i_untersuchung').asstring;

     case i_art of
     1: tab:='untersuchung';
     2: tab:='impfung';
     3: tab:='labor';
     end;

    query:=format('select i_mitarbeiter,i_typ from %s where nummer=%s ',[tab,s_untersuchung]);
    mysql_d.Feldarray(query,[0,1],felder1);

    query:=format('select name, vorname, i_firma from mitarbeiter where nummer=%s',[felder1[0]]);
    mysql_d.Feldarray(query,[0,1,2],felder2);

    query:=format('select firma from firma where nummer=%s',[felder2[2]]);
    firma:=mysql_d.Feldinhalt(query,0);

    query:=format('select untersuchung from typ where nummer=%s',[felder1[1]]);
    typ:=copy(mysql_d.Feldinhalt(query,0),1,100);

    mem:=copy( tabelle.findfield('memo').asstring,1,100);
    //tabelle.edit;
    tabelle.findfield('i_mitarbeiter').asstring:= felder1[0];
    tabelle.findfield('i_typ').asstring:=felder1[1];
    tabelle.findfield('name').asstring:=felder2[0];
    tabelle.findfield('vorname').asstring:=felder2[1];
    tabelle.findfield('i_firma').asstring:=felder2[2];
    tabelle.findfield('firma').asstring:=firma;
    tabelle.findfield('info').asstring:=copy(felder2[0]+' '+felder2[1]+' / '+typ+' / '+mem,1,200);
    //tabelle.Post;
    //tabelle.Edit;

except
end;

end;




procedure TForm_termine.DBgridEXT_unt_filterLButtonDown(Sender: TObject);
begin
	DBgridEXT_unt_filter.BeginDrag(false,1);

end;



procedure TForm_termine.DBgridEXT_DblClick(Sender: TObject);
begin
	suche_untersuchung(true);
end;

procedure TForm_termine.suche_untersuchung(quit:boolean);
var
i,  art: integer;
stammdaten,untersuchung:int64;
begin
i:=akt_tabelle;
//while (not dbgrid_tage[i].focused) and (i<7) do inc(i);
if q_tage[i]['i_art']<>null then art:=q_tage[i]['i_art'] else art:=0;
stammdaten:=getbigint(q_tage[i],'i_mitarbeiter');
untersuchung:=getbigint(q_tage[i],'i_untersuchung');
//firma:=q_tage[i]['i_firma'];

if form_main.gehezuuntersuchung(art,stammdaten,untersuchung{, firma, q_tage[i]} ) and quit then modalresult:=mrok;
end;

procedure tform_termine.gehezudatum(datum:tdate);
begin
  hauptdatum:=datum;
	 wochenkalender;
	 kalenderfiltern;
   datumanzeigen;
   if pagecontrol_zeit.ActivePage=tabsheet_monat then
   zeichne_monat;
end;


function tform_termine.gehezutermin(nummer:int64):boolean;
var
feld: string;

begin
with datamodul do
begin
	feld:=mysql_d.Feldinhalt('select datum_s from termine where nummer='+inttostr(nummer),0);
	if feld='' then exit;
	hauptdatum:=strtodate( dat_sql_to_delphi1(feld));
	//q_termin.first;
	//result:= q_termin.locate('nummer',nummer,[]);
		pagecontrol_zeit.ActivePage:=tabsheet_tag;
		wochenkalender;
		kalenderfiltern;
		datumanzeigen;
		//q_termin0.locate('nummer',inttostr(nummer),[]);
    q_termin0.locate('nummer',(nummer),[]);
		result:=true;
	end;
end;


function tform_termine.terminiere_uil(nummer:int64;rubrik:integer):boolean;
var
filter:string;
begin
pagecontrol_rechts.activepage:=tabsheet_terminieren;
case rubrik of
	1: filter:='select firma.firma,firma.kuerzel ,untersuchung.* , name, vorname, i_firma,untersuchung,schicht_s_dat, i_schichtmodell , typ.dauer '+'from untersuchung left join mitarbeiter on (untersuchung.i_mitarbeiter =mitarbeiter.nummer) left join typ on (typ.nummer=untersuchung.i_typ) left join firma on (firma.nummer=mitarbeiter.i_firma) where i_status=1 and untersuchung.nummer='+inttostr(nummer) +' and untersuchung.storno=0';
	2: filter:='select firma.firma,firma.kuerzel,impfung.*, name, vorname, i_firma,untersuchung ,schicht_s_dat, i_schichtmodell,typ.dauer '+'from impfung left join mitarbeiter on(impfung.i_mitarbeiter =mitarbeiter.nummer) left join typ on (typ.nummer=impfung.i_typ) left join firma on (firma.nummer=mitarbeiter.i_firma) where i_status=1 and impfung.nummer='+inttostr(nummer)+' and impfung.storno=0';
	3: filter:='select firma.firma,firma.kuerzel,labor.* , name, vorname, i_firma,untersuchung ,schicht_s_dat, i_schichtmodell,typ.dauer '+' from labor left join mitarbeiter on(labor.i_mitarbeiter =mitarbeiter.nummer) left join typ on (typ.nummer=labor.i_typ) left join firma on (firma.nummer=mitarbeiter.i_firma) where i_status=1 and labor.nummer='+inttostr(nummer)+' and labor.storno=0';
end;
datamodul.sql_new(false,datamodul.q_unt_filter,filter,'');
if datensatz(datamodul.q_unt_filter) then result:=true else result:=false;
end;



procedure tform_termine.datumanzeigen;
var
year, month, day:word;
kw,i:integer;
erstmon:tdate;
begin
	decodedate(hauptdatum,year, month, day);
	I:=1;
	erstmon:=encodedate(year,1,i);
	while dayofweek(erstmon)<>2 do
	begin
		inc(i);
		erstmon:=encodedate(year,1,i);
	end;
	kw:=kalenderwoche(hauptdatum);
	tabsheet_monat.Caption:='Monats�bersicht  '+formatdatetime('mmmm yyyy',hauptdatum);
	tabsheet_woche.Caption:='KW - '+inttostr(kw);
	tabsheet_tag.caption:=formatdatetime('dddd',hauptdatum);
	edit_datum.text:=formatdatetime('dddd",  "ddddd',hauptdatum);
end;

procedure TForm_termine.ToolButton_filternClick(Sender: TObject);
begin

if form_termine_filtern.showmodal <>mrok then exit;
kalenderfiltern;
if pagecontrol_zeit.ActivePage=tabsheet_monat then  zeichne_monat;


end;

procedure TForm_termine.ToolButton_kalenderClick(Sender: TObject);
begin
form_kalender.Auswahlkalender.Date:=int(hauptdatum);
if form_kalender.showmodal =mrok then gehezudatum(int(form_kalender.auswahlkalender.date));

end;


procedure TForm_termine.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
termine_speichern(false);
canclose:=true;
end;

procedure TForm_termine.PageControl_zeitChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	termine_speichern(false);
end;

procedure TForm_termine.DBgridEXT_CellClick(Column: TColumn);
var i:integer;
begin
//i:=0;
//while (not dbgrid_tage[i].focused) and (i<7) do inc(i);
i:=akt_tabelle;
hauptdatum:=wochendaten[i];
wochendaten[0]:=wochendaten[i];
details_tabelle(i);
datumanzeigen;


dbgrid_tage[i].BeginDrag(false,1);

end;

procedure TForm_termine.DBgridEXT_miCellClick(Column: TColumn);
begin
termine_laden;
end;

procedure TForm_termine.DBgridEXT_moLbuttongehalten(Sender: TObject);
begin
dbgrid_tage[akt_tabelle].BeginDrag(false,1);
end;

procedure TForm_termine.Panel_zeitplan1Click(Sender: TObject);
var nummer:integer;
begin
  //if modus=2 then exit;
  if not (sender is tshape_termin) then exit;
	nummer:=tshape_termin(sender).nummer;
	if nummer>0 then
	begin
  	datamodul.q_termin_monat.first;
		//datamodul.q_termin_monat.locate('nummer',inttostr(nummer),[]);
    datamodul.q_termin_monat.locate('nummer',(nummer),[]);
	end;

end;

procedure TForm_termine.Panel_zeitplan1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
panel:tshape_termin;
nummer: int64;
begin

  //if modus=2 then exit;
	if not (sender is tshape_termin) then
	begin
	  Screen.Cursor := crdefault;
	  exit;
	end;
	 panel:=tshape_termin(sender);
	if (y<-20) or (y>panel.Height+10) or (x<-20) or (x>panel.Width+20) then  //au�erhalb
		begin
			startx:=x;
			starty:=y;
			screen.Cursor:=crdefault;
			zugrichtung:=-1;
			exit;
		end;


    case modus of
    1: begin
        if not (ssLeft in Shift) then exit;
        if zugrichtung=0 then
        begin
            panel.left:=panel.left+x-startx;
            panel.top:=panel.top+y-starty;
        end;

        if zugrichtung=2 then  panel.width:=x;

        if zugrichtung=1 then
          begin
          panel.left:=panel.left+x-startx;
          panel.width:=panel.Width-(x-startx);
        end;
       end;
      2: begin
          {nummer:=tshape_termin(sender).nummer;
          if nummer>0 then
          begin
            datamodul.q_termin_monat.first;
            datamodul.q_termin_monat.locate('nummer',(nummer),[]);
	        end;}
        end;
      end;
end;

procedure TForm_termine.Panel_zeitplan1MouseDown(Sender: TObject;
	Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
panel:tshape_termin;
nummer:int64;
begin
if modus=2 then
begin
  nummer:=tshape_termin(sender).nummer;
  if nummer>0 then
  begin
    datamodul.q_termin_monat.first;
    datamodul.q_termin_monat.locate('nummer',(nummer),[]);
    edit_firma.text:=firmenname(datamodul.q_termin_monat.findfield('i_firma').asinteger );
  end;
  exit;
end;
y_maus:=y;
if not (sender is tshape_termin) then exit;

if button in [mbright] then
begin //
 tshape_termin(sender).SendToBack;
 //Panel_zeitplan1Click(Sender);
 exit;
end;

if panel_zeitplan1.shape_aktuell<>tshape_termin(sender) then
	begin
		if assigned(panel_zeitplan1.shape_aktuell) then
			begin
			 panel_zeitplan1.shape_aktuell.brush.color:=tshape_termin(sender).farbe;
			 panel_zeitplan1.shape_aktuell.brush.style:= bsbdiagonal;
			end;
		panel_zeitplan1.shape_aktuell:=tshape_termin(sender);
		panel_zeitplan1.shape_aktuell.brush.color:=clblue;
		panel_zeitplan1.shape_aktuell.brush.style:= bssolid;
		nummer:=tshape_termin(sender).nummer;
		if nummer>0 then
		begin
     	datamodul.q_termin_monat.first;
			//datamodul.q_termin_monat.locate('nummer',inttostr(nummer),[]);
      datamodul.q_termin_monat.locate('nummer',(nummer),[]);
        if datamodul.q_termin_monat['datum_s']<>null then
	        begin
        	hauptdatum:=datamodul.q_termin_monat['datum_s'];
				//wochenkalender;
				//kalenderfiltern;
				Datumanzeigen;
          end;
			if datamodul.q_termin_monat['i_art']<>null then  details(datamodul.q_termin_monat['i_art']);
		end;
	end;

   if not (sender is tshape_termin) then exit;
   panel:=tshape_termin(sender);
   if panel.nummer<=0 then exit;

   if (y<0) or (y>panel.Height) or (x<0) or (x>panel.Width) then exit;
   if (x>2) and (x<panel.width-2) then    //8->2
   begin
     zugrichtung:=0;
     Screen.Cursor := crsize;
   end;
   if (x<2)then  //8
   begin
    zugrichtung:=1;
    Screen.Cursor :=crhsplit;
   end;
   if (x>panel.width-3) then  //8
   begin
     zugrichtung:=2;
      Screen.Cursor :=crhsplit;
   end;
   startx:=x;
   starty:=y;
end;


procedure tform_termine.shape_speichern(shape:tshape_termin);
var
start, ende:tdatetime;
datum:tdatetime;
i_tag:integer;
year, month, day:word;
begin
with datamodul do
begin
	start:=panel_zeitplan1.zeit_start(shape);
	ende:=panel_zeitplan1.zeit_ende(shape);
	i_tag:=panel_zeitplan1.tage(shape.top);
	datum:=q_termin_monat['datum_s'];
	decodedate(datum, year,month, day);
	datum:=encodedate(year, month, i_tag);

	q_termin_monat.edit;
	q_termin_monat['zeit_s']:=start;
	q_termin_monat['zeit_e']:=ende;
	q_termin_monat['datum_s']:=datum;
	direktspeichern:=true;
	q_termin_monat.post;
end;

end;


procedure TForm_termine.Panel_zeitplan1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var shape:tshape_termin;
begin
 if Modus=2 then exit;
	zugrichtung:=-1;
	screen.Cursor:=crdefault;

	if not (sender is tshape_termin) then exit;
	shape:=tshape_termin(sender);
	if shape.nummer>0 then
	begin
		panel_zeitplan1.shape_einpassen(shape);
		shape_speichern(shape);
	end;
end;

procedure TForm_termine.Panel_zeitplan1DblClick(Sender: TObject);
var tag:integer;
year, month, day:word;
nummer:int64;
begin
   case modus of
   1: begin
       tag:=panel_zeitplan1.tage(y_maus);
       decodedate(hauptdatum, year, month, day);
       hauptdatum:=encodedate(year, month, tag);
       wochenkalender;
       kalenderfiltern;
       Datumanzeigen;
       pagecontrol_zeit.activepage:=tabsheet_tag;
     end;
   2: begin
          nummer:=tshape_termin(sender).nummer;
          form_main.gehezu_firma_sollist(datamodul.q_termin_monat.findfield('i_firma').asinteger ,nummer) ;
          modalresult:=mrok;
          //gehezutermin(nummer);
      end;
   end;
end;

procedure TForm_termine.Edit_datumChange(Sender: TObject);
begin
  Jahresfeiertage(hauptdatum);
	monatsfeiertage(hauptdatum);
end;

procedure tform_termine.Monatsfeiertage(datum:tdate);
var i:integer;
	  year, month, day, akt_monat:word;
	listenwert: pstringintegerliste;
	tagname:string;
begin
	monats_feiertage.clear;

	//j:=1;
   decodedate(datum,year, month, day);
   akt_monat:=month;
   datum:=encodedate(year, month,1);
   tag:=dayofweek(datum);
  while tag <>1 do //sonntag
  begin
    datum:=datum +1;
    tag:=dayofweek(datum);
  end;

  decodedate(datum,year, month, day);
  while day<=31 do
  begin
  	monats_feiertage.int_aktualisieren('',day);
     day:=day+7
  end;
  //�brige Feiertage
  for i:=0 to jahres_feiertage.Count-1  do
  begin
    listenwert:=jahres_feiertage[i];
    tagname:=listenwert^.str1;
    datum:=(listenwert^.int1);
    decodedate(datum, year, month, day);
    if akt_monat=month then
    	monats_feiertage.int_aktualisieren(tagname,day);
  end;

  // array schreiben


  setlength(fette_tage,monats_feiertage.Count);
	for i:=0 to monats_feiertage.count-1 do
  begin
     listenwert:= monats_feiertage[i];
     fette_tage[I]:=listenwert^.int1;
  end;

end;

procedure tform_termine.Jahresfeiertage(datum:tdate);
var
	year, month, day:word;
	m, n,a,b,c,d,e,Tag, mon:integer;
  ostern , dat:tdate;
begin
jahres_feiertage.clear;
decodedate(datum , year, month, day);
//Ostersonntag
	m:=23;
  n:=3;
  if year>1799 then n:=4;
  if year>1899 then begin inc(n); m:=24; end;
  if year>2099 then inc(n);

  a:=year mod 19;
  b:=year mod 4;
  c:=year mod 7;
	d:=((a*19)+m)mod 30;
	e:=((b*2)+(c*4)+(d*6)+n) mod 7;
  tag:=d+e+22;
  if tag > 31 then
  begin
  	tag:=tag-31;
     if (tag=26) or((tag=25) and (d=28) and (a>10))then tag:=tag-7;
     mon:=4;
  end
  else
  	mon:=3;

  dat:=encodedate(year, mon, tag);
  ostern:=dat;
  jahres_feiertage.int_aktualisieren('Ostersonntag', trunc(dat));
	dat:=ostern-2;
  jahres_feiertage.int_aktualisieren('Karfreitag', trunc(dat));
  dat:=ostern+1;
  jahres_feiertage.int_aktualisieren('Ostermontag', trunc(dat));
  dat:=ostern+49;
  jahres_feiertage.int_aktualisieren('Pfingstsonntag', trunc(dat));
	dat:=ostern+50;
  jahres_feiertage.int_aktualisieren('Pfingsttmontag', trunc(dat));
  dat:=ostern+39;
  jahres_feiertage.int_aktualisieren('Chr. Himmelfahrt', trunc(dat));
  dat:=ostern+60;
  jahres_feiertage.int_aktualisieren('Frohnleichnam', trunc(dat));
  dat:=encodedate(year, 11, 15); //Bu� u Bettag 3. Mittwoch im November   also fr�hestens der 15.
  repeat
  	if dayofweek(dat)<>4 then dat:=dat+1;
  until dayofweek(dat)=4;
  jahres_feiertage.int_aktualisieren('Bu� und Bettag', trunc(dat));

  jahres_feiertage.int_aktualisieren('Neujahr', trunc(encodedate(year, 1,1)));
  jahres_feiertage.int_aktualisieren('hl drei K�nige', trunc(encodedate(year, 1,6)));
	jahres_feiertage.int_aktualisieren('Tag der Arbeit',trunc(encodedate(year,5,1 )));
	jahres_feiertage.int_aktualisieren('Mari� Himmelfahrt',trunc(encodedate(year,8, 15 )));
  jahres_feiertage.int_aktualisieren('Tag dt. Einheit',trunc(encodedate(year,10,3 )));
  jahres_feiertage.int_aktualisieren('Allerheiligen',trunc(encodedate(year, 11,1)));
  jahres_feiertage.int_aktualisieren('1. Weihnachtsfeiertag',trunc(encodedate(year,12,25 )));
	jahres_feiertage.int_aktualisieren('2. Weihnachtsfeiertag',trunc(encodedate(year,12,26 )));


end;
procedure tform_termine.kalender_akutalisieren;
var i:integer;
begin
	for i:=0 to 9 do q_tage[i].refresh;
end;

procedure TForm_termine.verndern1Click(Sender: TObject);
begin
	ToolButton_neuClick(Sender);
end;

procedure TForm_termine.verndern2Click(Sender: TObject);
begin
ToolButton_editClick(Sender);
end;

procedure TForm_termine.speichern1Click(Sender: TObject);
begin
ToolButton_speichernClick(Sender);
end;

procedure TForm_termine.abbrechen1Click(Sender: TObject);
begin
ToolButton_cancelClick(Sender);
end;

procedure TForm_termine.lschen1Click(Sender: TObject);
begin
ToolButton_loeschenClick(sender);
end;

procedure TForm_termine.Terminbersichtverlassen1Click(Sender: TObject);
begin
ToolButton_exitClick(Sender);
end;

procedure TForm_termine.DBgridEXT_listeCellClick(Column: TColumn);
begin
 	hauptdatum:=datamodul.q_termin_monat['datum_s'];
  datumanzeigen;
end;

procedure tform_termine.formular_sperren;
begin
		form_main.p_sperren(q_tage[akt_tabelle],q_tage[akt_tabelle],user_id,[tabsheet_detail]);
		toolbutton_cancel.Enabled:=false;
		toolbutton_speichern.Enabled:=false;
		toolbutton_loeschen.Enabled:=false;

end;


function tform_termine.formular_entsperren:boolean;
begin
 if kein_datensatz(q_tage[akt_tabelle]) then exit;
 
 result:=form_main.p_entsperren(q_tage[akt_tabelle],q_tage[akt_tabelle],user_id,[groupbox_zeitp,groupbox_anm],true);

 if result then
 begin
	toolbutton_cancel.Enabled:=true;
	toolbutton_speichern.Enabled:=true;
	toolbutton_loeschen.Enabled:=true;
 end;
end;


procedure TForm_termine.DBgridEXT_listeLButtonDown(Sender: TObject);
begin
try
		hauptdatum:=datamodul.q_termin_monat['datum_s'];
	  details_tabelle(9);
		datumanzeigen;
except
end;    
end;

procedure TForm_termine.All_delClick(Sender: TObject);
var year, year1, month,month1, day,day1 :word;
begin
  decodedate(hauptdatum,year,month,day);
  decodedate(date(),year1,month1,day1);
	if (year>=year1) and(month>=month1) then
  begin
  	showmessage('Termine m�ssen in der Vergangenheit liegen');
	  exit;
  end;
	if  (MessageDlg('L�schen aller Datens�tze in der Liste?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes ) then exit;
	with datamodul do
	begin
		q_termin_monat.First;
		while not q_termin_monat.Eof do  dateiloeschen(datamodul.q_termin_monat,false,false);// q_termin_monat.Delete;
	end;

end;

procedure TForm_termine.FormResize(Sender: TObject);
begin
if visible then wochezeichnen;
grid_einstellen(dbgridext_unt_filter);
grid_einstellen(dbgridext_liste);
end;


procedure TForm_termine.druckClick(Sender: TObject);
var i:integer;
i64:int64;
begin
if pagecontrol_zeit.ActivePage=tabsheet_liste then
begin
for i:= 0 to dbgridext_liste.SelectedRows.count-1 do
     begin
       datamodul.q_termin_monat.GotoBookmark(tbookmark(dbgridext_liste.SelectedRows.items[i]));
       //suche_untersuchung(false);
  	  form_main.einladung_drucken(getbigint(datamodul.q_termin_monat,'nummer'),0);
     end;
exit;
end;
i64:=getbigint(q_tage[akt_tabelle],'nummer');
if i64>0 then form_main.einladung_drucken(i64,0);
end;

procedure TForm_termine.PageControl_rechtsChange(Sender: TObject);
begin
if tpagecontrol(sender).activepage=tabsheet_terminieren then
begin
	tabsheet_monat.TabVisible:=false;
	tabsheet_liste.TabVisible:=false;
	unt_filtern;
	statusbar.Panels[0].text:='Ziehen Sie den Mitarbeiter auf den gew�nschten Termin.';
  ToolButton_schicht.enabled:=true;
end
else
begin
	tabsheet_monat.TabVisible:=true;
	tabsheet_liste.TabVisible:=true;
	statusbar.Panels[0].text:='';
  ToolButton_schicht.enabled:=false;
end;

end;

procedure TForm_termine.BitBtn_terminaendernClick(Sender: TObject);
begin
	suche_untersuchung(true);
  form_main.formular_entsperren(true,false);
end;

procedure tform_termine.all_refresh;
var i:integer;
begin
	for i:=0 to 7 do q_tage[i].refresh;
  q_tage[9].refresh;
end;

procedure tform_termine.unt_status_aendern(nummer:int64; uil:integer);     //nur wenn status einbestellt zur�cksetzen auf geplant
var query:string;
farbe:integer;
sdat:string;
begin
  sdat:=sql_datetostr(date());
  datamodul.sql(false,datamodul.q_4,'select * from status', 'nummer');
  datamodul.q_4.first;
  farbe:=datamodul.q_4['farbe'];
  case uil of
     1: query:=format('update untersuchung set farbe= %d where nummer= %d ',[farbe,nummer]); //and i_status=2
     2: query:=format('update impfung set farbe= %d where nummer= %d ',[farbe,nummer]);
     3: query:=format('update labor set farbe= %d where nummer= %d ',[farbe,nummer]);
  end;
 	mysql_d.sql_exe( query);

	case uil of
     1: query:=format('update untersuchung set i_status= 1 where nummer= %d  ',[nummer]);
     2: query:=format('update impfung set i_status= 1 where nummer= %d ',[nummer]);
     3: query:=format('update labor set i_status= 1 where nummer= %d ',[nummer]);
  end;
 	mysql_d.sql_exe( query);

 	case uil of
     1: query:=format('update untersuchung set datum= %s where nummer= %d  ',[sdat,nummer]);
     2: query:=format('update impfung set datum= %s where nummer= %d ',[sdat,nummer]);
     3: query:=format('update labor set datum= %s where nummer= %d ',[sdat,nummer]);
  end;
 	mysql_d.sql_exe( query);

  case uil of
     1: query:=format('update untersuchung set i_termin=0 where nummer= %d  ',[nummer]);
     2: query:=format('update impfung set i_termin= 0 where nummer= %d ',[nummer]);
     3: query:=format('update labor set i_termin= 0 where nummer= %d ',[nummer]);
  end;
 	mysql_d.sql_exe( query);


  case uil of
     1: datamodul.q_untersuchung.refresh;
     2: datamodul.q_impfung.refresh;
     3: datamodul.q_labor.refresh;
  end;
end;


procedure TForm_termine.ToolButton_schichtClick(Sender: TObject);
begin
if kein_datensatz(datamodul.q_unt_filter) then exit;
if  (datamodul.q_unt_filter['i_schichtmodell']=null) or (datamodul.q_unt_filter['schicht_s_dat']=null) then
begin
	showmessage('Bitte Untersuchungen selektieren');
  exit;
end;
datamodul.sql(false,datamodul.q_1,'select * from schicht','');
 if not datamodul.q_1.Locate('nummer',datamodul.q_unt_filter['i_schichtmodell'],[]) then
 begin
	showmessage('Kein Schichtmodell zugeordnet');
  exit;
 end;
 try
	 form_schicht:=tform_schicht.Create(self);
	 form_schicht.ini(datamodul.q_unt_filter['name'],datamodul.q_unt_filter['vorname'],date,datamodul.q_unt_filter['schicht_s_dat'], datamodul.q_1['schicht']);
	 form_schicht.zeichne;

	 if form_schicht.showmodal=mryes then gehezudatum(form_schicht.sel_date) ;
 finally
 form_schicht.Release;
 end;
 //terminieren

end;

procedure TForm_termine.EmailClick(Sender: TObject);
var i:integer;
i64:int64;
begin
	 if pagecontrol_zeit.ActivePage=tabsheet_liste then
   begin
      for i:= 0 to dbgridext_liste.SelectedRows.count-1 do
           begin
             datamodul.q_termin_monat.GotoBookmark(tbookmark(dbgridext_liste.SelectedRows.items[i]));
				  //suche_untersuchung(false);
				  form_main.einladung_drucken(getbigint(datamodul.q_termin_monat,'nummer'),1);
           end;
        exit;
   end
   else
   begin
   i64:=getbigint(q_tage[akt_tabelle],'nummer');
	 if i64>0 then
		begin
		 form_main.einladung_drucken(i64,1);
		end;
   end;
end;

procedure TForm_termine.Tagesplan1Click(Sender: TObject);
begin
tag_excel;
end;

procedure TForm_termine.tag_excel;
var
query, sql_where:string;
position:integer;
feldlist:tstringlist;
begin
try
feldlist:=tstringlist.create;
feldlist.Add('name');
feldlist.Add('vorname');
feldlist.Add('geburtsdatum');
feldlist.Add('t_datum');
feldlist.Add('t_typ');
feldlist.Add('t_zeit');
feldlist.Add('f_name');
feldlist.Add('Telefon_Firma');

query:=q_tage[0].sql.text;
position:=pos('where',query);
sql_where:=copy(query,position,length(query));

query:=com.query_create(feldlist,sql_where,'termine');

com.export_excel(query);

finally
feldlist.free;
end;
end;


procedure TForm_termine.DBgridEXT_unt_filterDblClick(Sender: TObject);
var
 art: integer;
stammdaten,untersuchung:int64;
begin
with datamodul do
begin
   stammdaten:=getbigint(q_unt_filter,'i_mitarbeiter');
   untersuchung:=getbigint(q_unt_filter,'nummer');
   art:=form_twahl.radiogroup_typ.itemindex+1;   
   if form_main.gehezuuntersuchung(art,stammdaten,untersuchung{, firma, q_tage[i] })  then modalresult:=mrok;
end;
end;


procedure TForm_termine.FormShow(Sender: TObject);
begin
neu_lesen:=true;
form_termine_filtern.PageControl_ansicht.ActivePageIndex:=g_termin_filtern_tab;
formular_sperren;
if neu_lesen then kalenderfiltern;
if pagecontrol_zeit.ActivePage=tabsheet_monat then  zeichne_monat;
end;

procedure TForm_termine.Terminieren1Click(Sender: TObject);
begin
pagecontrol_rechts.ActivePage:=tabsheet_terminieren;
end;

procedure TForm_termine.SpeedButton_terminaendernClick(Sender: TObject);
var
datum:tdate;
t1,diff:ttime;
query:string;
new_nummer,i_untersuchung:int64;
i_unt_art:integer;
begin
 if form_kalender.showmodal =mrok then
	 with datamodul do begin
     datum:= int(form_kalender.auswahlkalender.date);
     try
     diff:=q_tage[akt_tabelle].findfield('zeit_e').AsDateTime-q_tage[akt_tabelle].findfield('zeit_s').AsDateTime;
     except
     diff:=0;
     end;
     query:='select * from termine where datum_s= '
			+sql_datetostr(datum)+p_untersucher+p_firma+p_art + 'order by "zeit_s"' ;

		datamodul.sql_new(false,q_3,query,'');
     if q_3['zeit_e']<>null then t1:=q_3['zeit_e']
	   else t1:=strtotime(termine_startzeit);

	   q_3.next;

	   while not q_3.eof do
	   begin
		if q_3['zeit_s']-t1>=diff then
			begin
				q_3.prior;
				break;
			end;
		 t1:=q_3['zeit_e'];
		 q_3.next;
	 end;

    q_tage[akt_tabelle].edit;
		q_tage[akt_tabelle]['datum_s']:=datum;
		q_tage[akt_tabelle]['zeit_e']:=t1+diff;
		q_tage[akt_tabelle]['zeit_s']:=t1;


     i_untersuchung:=getbigint(q_tage[akt_tabelle],'i_untersuchung');
     i_unt_art:=q_tage[akt_tabelle]['i_art'];
     new_nummer:=getbigint(q_tage[akt_tabelle],'nummer');

     uil_terminieren(i_untersuchung,i_unt_art,datum,new_nummer,false);

     direktspeichern:=true;
     form_termine.termin_info_erzeugen(q_tage[akt_tabelle]);
     q_tage[akt_tabelle].post;
     q_tage[akt_tabelle].refresh;

     gehezutermin(new_nummer);
	 end;
end;

procedure TForm_termine.DBEdit_zeit_sEnter(Sender: TObject);
begin

if not (tdbedit_time(sender).datasource.State in [dsedit, dsinsert]) then exit;
//dt:=strtotime(dbedit_zeit_e.text)-strtotime(dbedit_zeit_s.text);
dt:= tdbedit_time(sender).datasource.dataset.findfield('zeit_e').asdatetime-tdbedit_time(sender).datasource.dataset.findfield('zeit_s').asdatetime;

end;

procedure TForm_termine.DBEdit_zeit_sExit(Sender: TObject);
begin
if not (tdbedit_time(sender).datasource.State in [dsedit, dsinsert]) then exit;

tdbedit_time(sender).datasource.dataset.findfield('zeit_e').asdatetime:=tdbedit_time(sender).datasource.dataset.findfield('zeit_s').asdatetime+dt;

end;

end.
