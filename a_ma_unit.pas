unit a_ma_unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls,Variants;

type
  TForm_ma_unit = class(TForm)
    Panel1: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    GroupBox1: TGroupBox;
    Edit_name1: TEdit;
    Edit_vorname1: TEdit;
    Edit_geb_dat1: TEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit_firma1: TEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit_name2: TEdit;
    Edit_vorname2: TEdit;
    Edit_geb_dat2: TEdit;
    BitBtn2: TBitBtn;
    Edit_firma2: TEdit;
    RichEdit2: TRichEdit;
    RichEdit1: TRichEdit;
    Label2: TLabel;
    Label10: TLabel;
    SpeedButton_tausch: TSpeedButton;
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton_tauschClick(Sender: TObject);
    procedure weg_anzeigen;
    procedure bleib_anzeigen;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen }
    nummer_bleib, nummer_weg:int64;
  public
    { Public-Deklarationen }
  end;

var
  Form_ma_unit: TForm_ma_unit;

implementation

uses a_data, a_namsuch, a_main, a_richedit_menue;

{$R *.DFM}

procedure TForm_ma_unit.BitBtn3Click(Sender: TObject);
var
str1,str2:string;
position,i:integer;
nr: int64;
begin
if nummer_weg*nummer_bleib=0 then exit;

if nummer_weg=nummer_bleib then
begin
	showmessage('die ausgewählten Probanden sind gleich');
	exit;
end;

if not( (edit_geb_dat1.text=edit_geb_dat2.text)) then
begin
	showmessage('Die Geburtsdaten müssen übereinstimmen.');
	exit;
end;
if Messagedlg('Sollen die Probanden zusammengeführt werden?',mtConfirmation, mbOkCancel	,0)<> mrOK  then exit;

with datamodul do
begin

		 sql(true,q_1,'select * from diagnosen where i_mitarbeiter='+inttostr(nummer_weg)  ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_diagnosen.refresh;


		 sql(true,q_1,'select * from besonderheiten where i_mitarbeiter='+inttostr(nummer_weg),'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		q_besonderheiten.refresh;

		 sql(true,q_1,'select * from befunde where i_mitarbeiter='+inttostr(nummer_weg)  ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_befunde.refresh;

		 sql(true,q_1,'select * from untersuchung where i_mitarbeiter='+inttostr(nummer_weg)  ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_untersuchung.refresh;

		 sql(true,q_1,'select * from impfung where i_mitarbeiter='+inttostr(nummer_weg)  ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_impfung.refresh;

		 sql(true,q_1,'select * from labor where i_mitarbeiter='+inttostr(nummer_weg) ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_labor.refresh;

		 sql(true,q_1,'select * from dokumente where i_mitarbeiter='+inttostr(nummer_weg) ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_dokumente.refresh;

		 sql(true,q_1,'select * from termine where i_mitarbeiter='+inttostr(nummer_weg) ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_termin.refresh;

		 sql(true,q_1,'select * from ambulanz where i_mitarbeiter='+inttostr(nummer_weg) ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_ambulanz.refresh;

		 sql(true,q_1,'select * from rechnung where i_firma=-1 and i_mitarbeiter='+inttostr(nummer_weg) ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_rechnung.refresh;

		 sql(true,q_1,'select * from artikel where i_mitarbeiter='+inttostr(nummer_weg) ,'');
		 q_1.first;
		 while not q_1.eof do
		 begin
			q_1.edit;
			setbigint(q_1,'i_mitarbeiter',nummer_bleib);
			q_1.next;
		 end;
		 q_artikel.refresh;




     //anamnese
     richedit1.Clear;
     richedit2.Clear;
     sql(true,q_1,'select * from anamnese where nummer='+inttostr(nummer_weg)  ,'');
     if datensatz(q_1) then
     begin
        //form_main.strings_laden(datamodul.q_1,'memo',richedit1.Lines);
        richeditladen(datamodul.q_1,'memo',richedit1);
        q_1.edit;
        q_1['storno']:=1;
        q_1.post;
     end;
   //  q_1.delete;

		 sql(true,q_1,'select * from anamnese where nummer='+inttostr(nummer_bleib)  ,'');
     if not datensatz(q_1) then
      begin
        nr:=getbigint(datamodul.q_mitarbeiter,'nummer');
        if nr=0 then exit;
        neuer_datensatz(datamodul.q_anamnese,['nummer'],[inttostr(nr)]);
       end;
     //form_main.strings_laden(datamodul.q_1,'memo',richedit2.Lines );
     richeditladen(datamodul.q_1,'memo',richedit2);


		 //richedit1.Text:=richedit1.text+#13#10+richedit2.text;  //keine Formatierungen

     
    richedit2.SelectAll;
    if (richedit2.SelLength > 0) then
    begin
      richedit2.CopyToClipboard;
      richedit1.SelStart:=length(richedit1.Text);
      richedit1.PasteFromClipboard;
    end;

		 q_1.edit;
       //form_main.strings_speichern(datamodul.q_1,'memo',richedit1.Lines);
       richeditspeichern( datamodul.q_1,'memo',richedit1);
		 q_1.post;

		 q_anamnese.refresh;
   //ende amnamnese

     richedit1.Clear;
     richedit2.Clear;
     
		 sql(true,q_1,'select * from mitarbeiter where nummer='+inttostr(nummer_weg)  ,'');
     richeditladen(datamodul.q_1,'memo',richedit1);
		 //richedit1.text:=q_1['memo'];
		 q_1.edit;
		 q_1['storno']:=1;
		 q_1.post;
		 //  q_1.delete;

		 sql(true,q_1,'select * from mitarbeiter where nummer='+inttostr(nummer_bleib)  ,'');
		 //richedit2.text:=q_1['memo'];
     richeditladen(datamodul.q_1,'memo',richedit2);


    richedit2.SelectAll;
    if (richedit2.SelLength > 0) then
  begin
    richedit2.CopyToClipboard;
    richedit1.SelStart:=length(richedit1.Text);
    richedit1.PasteFromClipboard;
  end;

    //richedit1.Text:=richedit1.text+#10+richedit2.text;    //hiermit werden keine formatierungen übernommen


		 q_1.edit;
		 //q_1['memo']:=richedit1.text;
     richeditspeichern( datamodul.q_1,'memo',richedit1);
		 q_1.post;





		 q_mitarbeiter_such.refresh;
		 q_mitarbeiter.refresh;

	end;
   modalresult:=mrok;
end;

procedure TForm_ma_unit.BitBtn1Click(Sender: TObject);
begin
with datamodul do
begin
	if form_namsuch.showmodal= mrok then
  begin
  	nummer_bleib:=getbigint(q_mitarbeiter_such,'nummer');
    bleib_anzeigen;


  end;
end;
end;

procedure TForm_ma_unit.bleib_anzeigen;
begin
with datamodul do
begin
    if nummer_bleib=0 then exit;

    sql(true,q_1,'select * from mitarbeiter where nummer ='+inttostr(nummer_bleib),'');
     //q_1.Locate('nummer', inttostr(nummer_bleib),[]);
     q_1.Locate('nummer', (nummer_bleib),[]);
     if q_1['name']<>null then edit_name1.text:=q_1['name'];
     if q_1['vorname']<>null then edit_vorname1.text:=q_1['vorname'];
     if q_1['geb_dat']<>null then edit_geb_dat1.text:=datetostr(q_1['geb_dat']);
     sql(true, q_2,'select * from firma where nummer = '+inttostr(q_1['i_firma']),'');
     if q_2['firma']<>null then edit_firma1.text:=q_2['firma'];
end;
end;

procedure TForm_ma_unit.BitBtn2Click(Sender: TObject);
begin
with datamodul do
begin
	if form_namsuch.showmodal= mrok then
  begin
  	nummer_weg:=getbigint(q_mitarbeiter_such,'nummer');
    weg_anzeigen;
  end;
end;
end;

procedure tform_ma_unit.weg_anzeigen;
begin
with datamodul do
begin
   if nummer_weg=0 then exit;
   
    sql(true,q_1,'select * from mitarbeiter where nummer ='+inttostr(nummer_weg),'');
     if q_1['name']<>null then edit_name2.text:=q_1['name'];
     if q_1['vorname']<>null then edit_vorname2.text:=q_1['vorname'];
     if q_1['geb_dat']<>null then edit_geb_dat2.text:=datetostr(q_1['geb_dat']);
     sql(true, q_2,'select * from firma where nummer = '+inttostr(q_1['i_firma']),'');
     if q_2['firma']<>null then edit_firma2.text:=q_2['firma'];
end;     
end;

procedure TForm_ma_unit.FormCreate(Sender: TObject);
begin
	 if prg_typ=3 then
	 begin
		caption:='Kunden zusammenführen';
		groupbox1.Caption:='Kunde der beibehalten wird';
		groupbox2.caption:='Kunde der gelöscht wird (die Befunde, etc. finden sich annschließend bei obigem Kunden)';
	 end;
   nummer_bleib:=0;
   nummer_weg:=0;
   form_main.form_positionieren(tform(sender));
   datamodul.q_mitarbeiter_such.refresh;

end;

procedure TForm_ma_unit.SpeedButton_tauschClick(Sender: TObject);
var
i:int64;
begin
  i:=nummer_bleib;
  nummer_bleib:=nummer_weg;
  nummer_weg:=i;

  bleib_anzeigen;
  weg_anzeigen;


end;

procedure TForm_ma_unit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
form_richedit_menue.ini(form_richedit_menue.RichEdit_start);
 form_richedit_menue.Parent:=nil;
end;

end.


