unit a_berechtigung;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_data,
	StdCtrls, Grids, DBGrids, dbgridEXT, ExtCtrls, ComCtrls, ToolWin, Buttons,
	DBCtrls, CheckLst, listen,db, dbcombo_number,Zdataset,variants, Mask,Math;

type tberechtigungen = class( tobject)
	private
	  firmen: tintegerlist;
	  objekte:tstringintegerlist;
    b_tabellen:tstringlist;
    b_objekte:tstringlist;
	public
    destructor  free; 
	  constructor create(id:integer);

	  procedure b_objekt_tab(var tab:ttabsheet;objekt:string);
	  function b_objekt(bobjekt:string):integer;
	  procedure berechtigungen_lesen;
    function b_p_schreib(pagec:tpagecontrol):boolean;
    function b_string(bobjekt:string):integer;
    function b_string_silent(bobjekt:string):integer;
    function b_firma(i_firma:integer):boolean;
	  function b_alter_ok(table: tzquery):boolean;
    function b_storno_ok(table: tzquery):boolean;
	  function b_archiv_ok(table: tzquery):boolean;
    function b_darf_loeschen(bobjekt:string):boolean;
    function b_hat_tab_ber(tabelle:string; grenzwert:integer):boolean;
end;

type
	TForm_berechtigung = class(TForm)
    ToolBar: TToolBar;
	  ToolButton_new: TToolButton;
	  ToolButton2: TToolButton;
    ToolButton_del: TToolButton;
    Toolbutton_Verlassen: TToolButton;
	  ToolButton7: TToolButton;
    PageControl: TPageControl;
	  TabSheet_passwort: TTabSheet;
    Notebook: TNotebook;
    Label2: TLabel;
    Label3: TLabel;
	  Edit_new2: TEdit;
	  Edit_new3: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Edit_edit2: TEdit;
	  Edit_edit3: TEdit;
	  Label7: TLabel;
    Label8: TLabel;
    TabSheet_b_firma: TTabSheet;
    Panel_left: TPanel;
	  DBgridEXT_name: TDBgridEXT;
    CheckListBox_firmen: TCheckListBox;
    TabSheet_b_aktionen: TTabSheet;
    Panel2: TPanel;
    Panel_ab: TPanel;
    Panel7: TPanel;
    DBgridEXT_b: TDBgridEXT;
    DBRadioGroup_aktion: TDBRadioGroup;
    SpeedButton_entfernen: TSpeedButton;
    SpeedButton_hinzufuegen: TSpeedButton;
	  Panel6: TPanel;
    Panel_mb: TPanel;
	  DBgridEXT_v: TDBgridEXT;
    Label13: TLabel;
    Label14: TLabel;
    Edit_new1: TEdit;
    Label1: TLabel;
	  Label_p_alt: TLabel;
    Edit_p_alt: TEdit;
    Label4: TLabel;
    Edit_p_user: TEdit;
    Label9: TLabel;
    DBCombo_num_untersucher: TDBCombo_num;
    ToolButton_save: TToolButton;
    ToolButton5: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton3: TToolButton;
    Label12: TLabel;
    Label16: TLabel;
    Panel1: TPanel;
    Panel4: TPanel;
    Label15: TLabel;
    Panel9: TPanel;
    Panel10: TPanel;
    BitBtn_check: TBitBtn;
    BitBtn_decheck: TBitBtn;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    Memo1: TMemo;
    Panel8: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Label10: TLabel;
    Panel13: TPanel;
    Label11: TLabel;
    DBEdit_change1: TDBEdit;
    DBEdit_change: TDBEdit;
    procedure ToolButton_newClick(Sender: TObject);
    procedure passwort_speichern;
    procedure BitBtn_edit_abbClick(Sender: TObject);
    //procedure neuer_datensatz;
    procedure Toolbutton_VerlassenClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure SpeedButton_entfernenClick(Sender: TObject);
	  procedure SpeedButton_hinzufuegenClick(Sender: TObject);
	  procedure PageControlChange(Sender: TObject);
	  procedure PageControlChanging(Sender: TObject;
		 var AllowChange: Boolean);
	  procedure BitBtn_checkClick(Sender: TObject);
	  procedure BitBtn_decheckClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure DBgridEXT_bEnter(Sender: TObject);
    procedure DBgridEXT_vEnter(Sender: TObject);
    procedure DBEdit_changeChange(Sender: TObject);
    procedure DBEdit_change1Change(Sender: TObject);
    procedure DBgridEXT_vDblClick(Sender: TObject);
    procedure DBgridEXT_bDblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
	private
	  { Private-Deklarationen }
	  procedure edit_sperren;
	  procedure new_sperren;
	  procedure firma_speichern;
    procedure berechtigungen_neu;

    procedure formularsperren;
    procedure loeschen_firma_storno;

	public
	  { Public-Deklarationen }
	  schl_nummer:integer;
	  procedure alles_speichern;
     procedure formularentsperren;
	end;

var
	Form_berechtigung: TForm_berechtigung;

implementation
uses a_main, a_line_edit;
{$R *.DFM}

procedure TForm_berechtigung.formularsperren;
begin
  form_main.sperren(notebook);
  toolbutton_save.Enabled:=false;
  toolbutton_del.Enabled:=false;
end;

procedure TForm_berechtigung.formularentsperren;
begin
  form_main.entsperren(notebook);
  toolbutton_save.Enabled:=true;
  toolbutton_del.Enabled:=true;
end;

procedure TForm_berechtigung.ToolButton_newClick(Sender: TObject);
var

bm: tbookmark;
begin
   try
   form_line_edit:=tform_line_edit.create(self);
	 form_line_edit.Label_2.Caption:='Name eingeben (ist auch das anf�ngliche Passwort)';
    form_line_edit.edit.text:='';
	form_line_edit.CheckBox.Visible:=false;

	  if form_line_edit.showmodal=mrok then
	  begin
   	  neuer_datensatz(datamodul.q_schluessel,['nummer','name','passwort'],
  	     [null,form_line_edit.edit.text ,form_main.string_verschluesseln(form_line_edit.edit.text) ]);
        datamodul.q_schluessel.post;
        bm:=datamodul.q_schluessel.getbookmark;

        { DBgridEXT_name.Enabled:=false;
   		pagecontrol.activepage:=TabSheet_passwort;
	 	notebook.PageIndex:=1
	 	edit_new1.text:='';
	 	edit_new2.text:='';
		 edit_new3.text:='';
   		tabsheet_b_firma.tabVisible:=false;
   		tabsheet_b_aktionen.tabVisible:=false;
   		toolbutton_new.Enabled:=false;
   		toolbutton_del.Enabled:=false; }
        datamodul.q_schluessel.first;
        datamodul.q_schluessel.gotobookmark(bm);
        datamodul.q_schluessel.FreeBookmark(bm);
        datamodul.q_schluessel.edit;
        formularentsperren;
      end;
   finally
   	form_line_edit.Release;
   end;
end;

procedure TForm_berechtigung.passwort_speichern;
begin
if edit_p_alt.visible then
	begin
	//if datamodul.q_schluessel['passwort']<>form_main.CipherManager.encodeString(edit_p_alt.text)
  	if form_main.string_entschluesseln(datamodul.q_schluessel['passwort'])<>(edit_p_alt.text)
	then
	begin
	  showmessage('altes Passwort nicht korrekt');
	  exit;
	end;

end;

if edit_edit2.text<>'' then //passwort eingegeben
begin
   if edit_edit2.text<>edit_edit3.text  then
	begin
		showmessage('neues Passwort nicht identisch geschrieben');
		exit;
	end;
	if length(edit_edit2.text)<5 then
	begin
		showmessage('Passwort mu� mindestens 5 Zeichen lang sein.');
		exit;
	end;
    datamodul.q_schluessel.edit;
	//datamodul.q_schluessel['passwort']:=form_main.CipherManager.encodeString(edit_edit2.text);
  	datamodul.q_schluessel.findfield('passwort').asstring:=form_main.string_verschluesseln(edit_edit2.text);
  	datamodul.q_schluessel.Post;
  	showmessage('User-Name und Passwort wurden ge�ndert')
end;

  if edit_p_user.Text='' then
  begin
       showmessage('User-name darf nicht leer sein');
       exit;
  end;

	datamodul.q_schluessel.edit;
	datamodul.q_schluessel.findfield('name').asstring:=edit_p_user.Text;
	datamodul.q_schluessel.Post;
  	edit_sperren;
	if edit_p_alt.visible then modalresult:=mrok;

end;

procedure TForm_berechtigung.BitBtn_edit_abbClick(Sender: TObject);
begin
	edit_sperren;
	if edit_p_alt.visible then modalresult:=mrok;
end;

{procedure TForm_berechtigung.neuer_datensatz;
begin
with datamodul do
begin
  	q_schluessel.first;

  if edit_new2.text<>edit_new3.text then
	begin
  	showmessage('Passwort nicht identisch');
     //form_main.sperren(notebook.Components[0]);
     exit;
  end;
	if length(edit_new2.text)<5 then
	begin
		showmessage('Passwort mu� mindestens 5 Zeichen lang sein.');
		exit;
	end;
  neuer_datensatz(q_schluessel,['nummer','name','passwort'],
  	//[null,edit_new1.text,form_main.ciphermanager.encodestring(edit_new2.text) ]);
     [null,edit_new1.text,form_main.string_verschluesseln(edit_new2.text) ]);
  if q_schluessel.state in [dsinsert, dsedit ] then q_schluessel.post;
	schl_nummer:=q_schluessel['nummer'];
	new_sperren;
	berechtigungen_neu;
end;
end;}

procedure TForm_berechtigung.Toolbutton_VerlassenClick(Sender: TObject);
begin
  ToolButton_saveClick(Sender);
  alles_speichern;
  b.berechtigungen_lesen;
  loeschen_firma_storno;
  datamodul.q_firma.Refresh;
  //datamodul.q_firma_lookup.Refresh;
  modalresult:=mrok;
end;

procedure TForm_berechtigung.ToolButton_delClick(Sender: TObject);
var query:string;
begin
     if datamodul.q_schluessel['nummer']= user_id then
     begin
     	showmessage('Sie d�rfen sich nicht selber l�schen');
        exit;
     end;

     if datamodul.q_schluessel['nummer']= 1 then
     begin
     	showmessage('Dieser Datensatz darf nicht gel�scht werden');
        exit;
     end;
     if (MessageDlg('L�schen des aktuellen Datensatzes?', mtConfirmation	,[mbYes, mbNo], 0)=mryes )
     then
     begin
          //query:='delete from berechtigung_objekt where benutzer='+inttostr(datamodul.q_schluessel['nummer']);
		  mysql_d.sql_exe(query);
		  dateiloeschen(datamodul.q_schluessel,true,false);//datamodul.q_schluessel.delete;
          formularsperren;
     end;
end;

procedure TForm_berechtigung.FormCreate(Sender: TObject);
begin
	 case prg_typ of
	 3: begin
			tabsheet_b_firma.Caption:='Berechtigungen Kunden-Gruppe';
		 end;
	 4: begin
			tabsheet_b_firma.Caption:='Berechtigungen Abteilungen';
		 end;
	 end;

	 pagecontrol.ActivePage:=tabsheet_passwort;
	 grid_einstellen(dbgridext_name);
	 grid_einstellen(dbgridext_v);
	 grid_einstellen(dbgridext_b);
	 DBCombo_num_untersucher.lookup_laden;
	 edit_sperren;
	 new_sperren;
	 form_main.form_positionieren(tform(sender));
     notebook.pageindex:=1;


	 PageControlChange(sender);
     edit_p_user.text:= datamodul.q_schluessel.findfield('name').asstring;
   
    tag:=1;
    formularsperren;
end;



procedure TForm_berechtigung.edit_sperren;
begin
   edit_edit2.text:='';
   edit_edit3.text:='';
   if datamodul.q_schluessel.state in [dsedit, dsinsert] then datamodul.q_schluessel.post;
end;
procedure TForm_berechtigung.new_sperren;
begin
  notebook.pageindex:=1;
	edit_NEW1.text:='';
	edit_new2.text:='';
	edit_new3.text:='';
  if datamodul.q_schluessel.state in [dsedit, dsinsert] then datamodul.q_schluessel.post;

end;


procedure TForm_berechtigung.SpeedButton_entfernenClick(Sender: TObject);
var query:string;
begin
  if datamodul.q_1.FindField('objekt').asstring='Firma-Stammdaten' then exit;
	query:=format('update berechtigung_vorlage set sichtbar =1 where objekt=%S and immer=0',[hk+string(datamodul.q_1['objekt'])+hk]);
	if mysql_d.sql_exe(query) then    //wenn immer =1 dann nicht
  begin
		if (datensatz(datamodul.q_1)) then dateiloeschen(datamodul.q_1,true,true);
		datamodul.q_2.refresh;
	end;
    if datensatz(datamodul.q_1) then DBRadioGroup_aktion.Enabled:=true else DBRadioGroup_aktion.enabled:=false;
end;

procedure TForm_berechtigung.SpeedButton_hinzufuegenClick(Sender: TObject);
var query,s1,s2:string;
	  rf,i1:integer;
begin
  DBRadioGroup_aktion.Enabled:=false;
	query:=format('update berechtigung_vorlage set sichtbar =0 where objekt = "%s"',[datamodul.q_2.findfield('objekt').asstring]);
	mysql_d.sql_exe(query);
	if datamodul.q_2['nummer']=null then exit;
	rf:=datamodul.q_2['reihenfolge'];
	datamodul.q_2.Locate('reihenfolge',rf,[]);

  if datamodul.q_1.Locate('benutzer,objekt', vararrayof([schl_nummer,datamodul.q_2['objekt'] ]),[]) then
  begin
    datamodul.q_1.edit;
    datamodul.q_1.FindField('storno').asinteger:=0;
  end
  else
  begin
    s1:=datamodul.q_2.findfield('objekt').asstring;
    i1:=datamodul.q_2.findfield('reihenfolge').asinteger;
    DBRadioGroup_aktion.ItemIndex:=2;
    neuer_datensatz( datamodul.q_1,['nummer','benutzer','objekt','berechtigung','reihenfolge'],[null,schl_nummer,s1,2,i1]);

  end;
  //datamodul.q_1.AppendRecord([nil,schl_nummer,datamodul.q_2['objekt'],0,datamodul.q_2['reihenfolge']]);

  datamodul.sql_post(datamodul.q_1);
  
	datamodul.q_2.refresh;
	datamodul.q_1.refresh;
  if datensatz(datamodul.q_1) then DBRadioGroup_aktion.Enabled:=true else DBRadioGroup_aktion.enabled:=false;
end;

procedure TForm_berechtigung.PageControlChange(Sender: TObject);
var query:string;
 daten:pnodepointer;
 i,nummer:integer;
begin
if pagecontrol.ActivePage=tabsheet_passwort then
begin
     edit_edit2.text:='';
     edit_edit2.text:='';
end;

if user_id<> 1 then
if ((pagecontrol.ActivePage=tabsheet_b_aktionen) or (pagecontrol.ActivePage=tabsheet_b_firma)) and (b.b_string_silent('Berechtigungen')=-1) then
		begin
			pagecontrol.ActivePage:=tabsheet_passwort;
        showmessage('Nur mit "Master-Berechtigung" erlaubt');
        exit;
		end;

with datamodul do
begin
schl_nummer:=q_schluessel.findfield('nummer').asinteger;

toolbutton_new.Enabled:=true;
toolbutton_edit.Enabled:=true;

if pagecontrol.ActivePage=tabsheet_b_aktionen then
begin
    toolbutton_new.Enabled:=false;
    toolbutton_edit.Enabled:=false;
		query:='update berechtigung_vorlage set sichtbar =1 where sichtbar=0';
		mysql_d.sql_exe(query);
		sql_new(true,q_1,format('select * from berechtigung_objekt where benutzer=%d and storno=0',[schl_nummer]),'reihenfolge');
		q_1.First;
		while not q_1.eof do
		begin
			query:=format('update berechtigung_vorlage set sichtbar =0 where objekt=%S',[hk+string(datamodul.q_1['objekt'])+hk]);
			mysql_d.sql_exe(query);
			q_1.Next;
		end;

		sql_new(true,q_2,'select * from berechtigung_vorlage where sichtbar=1','reihenfolge');

		dbgridext_b.DataSource:=ds_1;
    dbedit_change.DataSource:=ds_1;
		dbradiogroup_aktion.DataSource:=ds_1;
		dbgridext_v.DataSource:=ds_2;
    dbedit_change1.DataSource:=ds_2;

end;

if pagecontrol.ActivePage=tabsheet_b_firma then
begin
  toolbutton_new.Enabled:=false;
  toolbutton_edit.Enabled:=false;
  sql_new(true,q_3,'select nummer, firma from firma','firma');
	form_main.listeeinlesen(checklistbox_firmen.Items,q_3,'firma');
  for i:=0 to checklistbox_firmen.Items.Count-1 do
  begin
  	daten:=pnodepointer(checklistbox_firmen.Items.Objects[i]);
     nummer:=daten^.nummer;
     if mysql_d.Feldinhalt(format('select nummer from berechtigung_firma where benutzer=%d and i_firma=%d and storno=0',[schl_nummer,nummer]),0)<>'' then
     checklistbox_firmen.Checked[i]:=true else checklistbox_firmen.Checked[i]:=false;
  end;
end;

if pagecontrol.ActivePage=TabSheet_b_aktionen then
begin
  if datensatz(datamodul.q_1) then DBRadioGroup_aktion.Enabled:=true else DBRadioGroup_aktion.enabled:=false;
end;


end;
end;

procedure TForm_berechtigung.firma_speichern;
var query:string;
 daten:pnodepointer;
 i,nummer:integer;
 vorhanden:boolean;
begin
screen.cursor:=crhourglass;
for i:=0 to checklistbox_firmen.Items.Count-1 do
  begin
  	daten:=pnodepointer(checklistbox_firmen.Items.Objects[i]);
     nummer:=daten^.nummer;
     // erst mal l�schen
     //query:=format('delete from berechtigung_firma where benutzer=%d and i_firma=%d ',[schl_nummer,nummer]);
     //mysql_d.sql_exe(query);
     //vorhanden
     query:=format('select nummer from berechtigung_firma where benutzer=%d and i_firma=%d ',[schl_nummer,nummer]);
     vorhanden:= mysql_d.sql_exe_rowsvorhanden(query);

     //query:=format('select nummer from berechtigung_firma where user=%d and i_firma=%d',[schl_nummer,nummer]);
     if checklistbox_firmen.Checked[i] then
     begin
        if not vorhanden then query:=format('insert berechtigung_firma set benutzer=%d, i_firma=%d',[schl_nummer,nummer]);
        if  vorhanden then query:=format('update berechtigung_firma set storno =0 where benutzer=%d and i_firma=%d',[schl_nummer,nummer]);
     	//query:=format('insert into berechtigung_firma ( benutzer, i_firma ) values ( %d,%d) ',[schl_nummer,nummer]);
     end
    else
    begin
       if vorhanden then query:=format('update berechtigung_firma set storno =1 where benutzer=%d and i_firma=%d',[schl_nummer,nummer]);
    end;
     mysql_d.sql_exe(query);
  end;
screen.cursor:= crdefault;

end;

procedure TForm_berechtigung.loeschen_firma_storno;
var query:string;
 s_firma:string;
begin
//l�schen wenn firma storniert
   screen.Cursor:=crhourglass;
  application.ProcessMessages;

  dbradiogroup_aktion.values.text:='';
  dbradiogroup_aktion.items.text:='';
  dbradiogroup_aktion.DataField:='';
  dbradiogroup_aktion.Datasource:=nil;
  dbgridext_v.DataSource:=nil;
  dbedit_change.DataSource:=nil;
  dbedit_change1.DataSource:=nil;

  dbgridext_b.DataSource:=nil;
  dbgridext_b.Enabled:=false;

  {query:='select * from berechtigung_firma';
  //datamodul.q_1.First;
  datamodul.sql(false, datamodul.q_1,query,'');
  while not datamodul.q_1.Eof do
  begin
    s_firma:=datamodul.q_1.findfield('i_firma').asstring;
    query:=format('select nummer from firma where nummer=%s and storno =0',[s_firma]);
    if not mysql_d.sql_exe_rowsvorhanden(query) then
    begin
    	datamodul.q_1.edit;
      datamodul.q_1['storno']:=1;
      datamodul.q_1.Post;
    end;
  	 datamodul.q_1.Next;
  end; }

  query:='select berechtigung_firma.nummer from berechtigung_firma left join firma on (firma.nummer=berechtigung_firma.i_firma) where firma.storno=1 and berechtigung_firma.storno=0';
  datamodul.sql(false, datamodul.q_1,query,'');
  datamodul.q_1.First;
  while not datamodul.q_1.Eof do
  begin
     query:=format('update berechtigung_firma set storno=1  where nummer=%s',[datamodul.q_1.findfield('nummer').asstring]);
     mysql_d.sql_exe(query);
     datamodul.q_1.next;
  end;


  //   dbradiogroup_aktion.Datasource:=datamodul.ds_1;
 screen.Cursor:=crdefault;
end;

procedure TForm_berechtigung.PageControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	alles_speichern;
end;


procedure tform_berechtigung.alles_speichern;
begin

if (notebook.PageIndex=1) then
begin
if (edit_edit2.text<>'') and (edit_edit2.text<>'') then
	begin
     passwort_speichern;
	end
   else
   begin
      if  datamodul.q_schluessel.State in [dsedit, dsinsert] then datamodul.q_schluessel.post;
   end;
end;

if notebook.PageIndex=0 then
begin
	//BitBtn_new_okClick(self);
    tabsheet_b_firma.tabVisible:=true;
    tabsheet_b_aktionen.tabVisible:=true;
    datamodul.q_schluessel.First;
end
else
//BitBtn_edit_okClick(self);

if pagecontrol.activepage=tabsheet_passwort then
begin
end;

if pagecontrol.ActivePage=tabsheet_b_firma then firma_speichern;

if datamodul.q_1.state in [dsedit, dsinsert] then datamodul.q_1.post;


DBgridEXT_name.Enabled:=true;

toolbutton_edit.Enabled:=true;
//toolbutton_del.Enabled:=true;
formularsperren;



end;


procedure TForm_berechtigung.BitBtn_checkClick(Sender: TObject);
var i:integer;
begin
	for i:=0 to checklistbox_firmen.Items.Count-1 do checklistbox_firmen.checked[i]:=true;
end;

procedure TForm_berechtigung.BitBtn_decheckClick(Sender: TObject);
var i:integer;
begin
	for i:=0 to checklistbox_firmen.Items.Count-1 do checklistbox_firmen.checked[i]:=false;
end;


constructor tberechtigungen.create(id:integer);
begin
  firmen:=tintegerlist.create;
  objekte:=tstringintegerlist.create;
  b_tabellen:=tstringlist.Create;
  b_objekte:=tstringlist.Create;

  b_tabellen.Add('texte');
  b_objekte.Add('firma-dokumente');
  b_tabellen.Add('dokumente');
  b_objekte.Add('proband-dokumente-arzt|proband-dokumente-sani');
  b_tabellen.Add('untersuchung');
  b_objekte.Add('proband-bescheinigung|proband-bescheinigung-ag');
  b_tabellen.Add('befunde');
  b_objekte.Add('proband-befunde');
   b_tabellen.Add('akt_untersuchung');
  b_objekte.Add('proband-befunde');
  b_tabellen.Add('impfung');
  b_objekte.Add('proband-impfung');
   b_tabellen.Add('labor');
  b_objekte.Add('proband-labor');
   b_tabellen.Add('besonderheiten');
  b_objekte.Add('proband-besonderheiten');
   b_tabellen.Add('rechnung');
  b_objekte.Add('proband-rechnung|firma-rechnung');




  berechtigungen_lesen;

end;

destructor tberechtigungen.free;
begin
	firmen.free;
	objekte.free;
  b_tabellen.free;
  b_objekte.free;
end;

procedure tberechtigungen.b_objekt_tab(var tab:ttabsheet;objekt:string);
begin
	tab.tag:=b_objekt(objekt);
end;

function tberechtigungen.b_objekt(bobjekt:string):integer;
begin
	bobjekt:=lowercase(bobjekt);
	result:=objekte.suchen_int(bobjekt);
end;

function tberechtigungen.b_p_schreib(pagec:tpagecontrol):boolean;
begin
	if pagec.activepage.tag>=1 then result:=true
  else
	begin
  showmessage('Sie haben keine Berechtigung zum Schreiben');
	result:=false;
	end;
end;

function tberechtigungen.b_storno_ok(table: tzquery):boolean;
begin
	result:=true;
  //if not (table.state in [dsedit, dsinsert]) then exit;

  if table.findfield('storno').asstring ='1'then
		begin
		  showmessage('Der Datensatz ist gel�scht.'+chr(13)+'�nderungen sind nicht erlaubt.');
			RESULT:=false;
        exit;
		end;
end;



function tberechtigungen.b_alter_ok(table: tzquery):boolean;
begin
	result:=true;

  if not jahr_sperre then exit;

	if (table.findfield('datum')<>nil) then
	begin
	  if (table.findfield('i_status')<>nil) then
	  begin
		 if (table['i_status']=4) and ((date()-table['datum'])>365) then
		 begin
			  showmessage('Nach einem Jahr darf ein Datensatz nicht mehr ge�ndert werden.');
			  RESULT:=false;
			  exit;
		 end;
	  end
	  else
		 if ((date()-table['datum'])>365) then
		 begin
			  showmessage('Nach einem Jahr darf ein Datensatz nicht mehr ge�ndert werden.');
			  RESULT:=false;
			  exit;
		 end;
	end;


end;


function tberechtigungen.b_archiv_ok(table: tzquery):boolean;
begin
result:=true;

if table.findfield('archiv')=nil then // alle ausser q_mitarbeiter sonst nicht mehr r�ckg�ngig
	begin
		if datamodul.q_mitarbeiter['archiv']='X' then
		begin
		  showmessage('Der Datensatz "Mitarbeiter" ist archiviert.'+chr(13)+'�nderungen sind nicht erlaubt.');
			RESULT:=false;
		end;
   end;

end;

function tberechtigungen.b_darf_loeschen(bobjekt:string):boolean;
var
ber:integer;
begin
  result:=false;
	bobjekt:=lowercase(bobjekt);
	ber:=objekte.suchen_int(bobjekt);
  if ber=2 then result:=true else showmessage('Sie haben keine Berechtigung f�r "'+bobjekt+'"');
end;

function tberechtigungen.b_hat_tab_ber(tabelle:string; grenzwert:integer):boolean;
var
i,e:integer;
s,s1:string;
begin
// f�r alle true ausser den in b_tabellen vermerkten
result:=true;
i:=b_tabellen.IndexOf(tabelle);
if i>=0 then
  s:=b_objekte.Strings[i]
else exit ;
//b aufl�sen
e:=-1;

repeat
  i:=pos('|',s);
  if i>0 then
    s1:=copy(s,1,i-1)
  else
    s1:=s;
  s:=copy(s,i+1,length(s));

  e:=max(e,b_string_silent(s1));

until i=0;

if e<grenzwert then result:=false;

end;


function tberechtigungen.b_string(bobjekt:string):integer;
begin
	bobjekt:=lowercase(bobjekt);
	result:=objekte.suchen_int(bobjekt);
  if result=-1 then showmessage('Sie haben keine Berechtigung f�r "'+bobjekt+'"');
end;

function tberechtigungen.b_string_silent(bobjekt:string):integer;
begin
try
	bobjekt:=lowercase(bobjekt);
  bobjekt:=trim(bobjekt);
	result:=objekte.suchen_int(bobjekt);
except
	result:=-1;
end;
end;


procedure tberechtigungen.berechtigungen_lesen;
var
  query,objekt,e:string;
  berechtigung:integer;
  f:boolean;
begin
screen.Cursor:=crhourglass;
application.ProcessMessages;
objekte.clear;
with datamodul do
begin
//wenn berechtigung_objekt bzw berechtigung_firma leer dann tausche mit *_old
  f:=false;
  query:='select count(*) from berechtigung_firma';
  e:=mysql_d.Feldinhalt(query,0);
  if e='0' then
  begin
    query:='show tables like "berechtigung_firma_old"';
    e:=mysql_d.Feldinhalt(query,0);
    if e<>'' then
    begin
      query:='insert into berechtigung_firma select * from berechtigung_firma_old' ;
      mysql_d.sql_exe(query);
      f:=true;
    end
    else
      showmessage('Die Tabelle Berechtigung_firma ist leer ->erneut abgleichen');
   end;


  query:='select count(*) from berechtigung_objekt';
  e:=mysql_d.Feldinhalt(query,0);
  if e='0' then
  begin
    query:='show tables like "berechtigung_objekt_old"';
    e:=mysql_d.Feldinhalt(query,0);
    if e<>'' then
    begin
      query:='insert into berechtigung_objekt select * from berechtigung_objekt_old' ;
      mysql_d.sql_exe(query);
      f:=true;
    end
    else
      showmessage('Die Tabelle Berechtigung_objekt ist leer ->erneut abgleichen');
   end;

   if f then  showmessage('Der letzten Abgleich war unvollst�ndig');

	query:=format('select * from berechtigung_firma where benutzer=%d',[user_id]);
	sql_new(true,q_b1,query,'');
	q_b1.first;
	while not q_b1.eof do
	begin
		firmen.hinzufuegen(q_b1.findfield('i_firma').asinteger,0);
		q_b1.next;
	end;
	query:=format('select * from berechtigung_objekt where benutzer=%d and storno=0',[user_id]);
	sql_new(true,q_b1,query,'');
	q_b1.first;
	while not q_b1.eof do
	begin
		objekt:=lowercase(q_b1['objekt']);
     berechtigung:= q_b1['berechtigung'];
     //if objekt='vorlagen'then if system_modus=0 then berechtigung:=0;
		objekte.hinzufuegen(objekt,berechtigung); ;
		q_b1.next;
	end;
	end;
 screen.Cursor:=crdefault;
end;

function tberechtigungen.b_firma(i_firma:integer):boolean;
var
	  query:string;
begin
   query:=format('select nummer from berechtigung_firma where benutzer=%d and i_firma=%d',[user_id,i_firma]);
   result:= mysql_d.Feldinhalt(query,0)<>'';
end;


procedure tform_berechtigung.berechtigungen_neu;
var 
query:string;
begin
with datamodul do
begin
  sql_new(true,q_1,format('select * from berechtigung_objekt where benutzer=%d and storno=0',[schl_nummer]),'reihenfolge');

  sql_new(true,q_2,'select * from berechtigung_vorlage where immer=1 ','reihenfolge'); //diese Berechtigung soll jeder haben
  q_2.first;
  while not q_2.eof do
  begin
        query:=format('update berechtigung_vorlage set sichtbar =0 where objekt = %S',[hk+q_2['objekt']+hk]);
        mysql_d.sql_exe(query);
        //q_1.AppendRecord([nil,schl_nummer,q_2['objekt'],0,q_2['reihenfolge']]);
        neuer_datensatz(q_1,['nummer','benutzer','objekt','berechtigung','reihenfolge'],[null,schl_nummer,q_2.findfield('objekt').asstring,0,q_2.findfield('reihenfolge').asinteger]);
        //datamodul.q_2.refresh;
        q_2.next;
  end;
end;
end;

procedure TForm_berechtigung.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 datamodul.q_schluessel.Filtered:=false;
end;

procedure TForm_berechtigung.ToolButton_saveClick(Sender: TObject);
begin
	alles_speichern;
    formularsperren;
end;

procedure TForm_berechtigung.ToolButton4Click(Sender: TObject);
begin
new_sperren;
edit_sperren;
DBgridEXT_name.Enabled:=true;
tabsheet_b_firma.tabVisible:=true;
tabsheet_b_aktionen.tabVisible:=true;
end;

procedure TForm_berechtigung.ToolButton_editClick(Sender: TObject);
begin
	formularentsperren;
end;

procedure TForm_berechtigung.DBgridEXT_bEnter(Sender: TObject);
begin
//dbgridext_v.SelectedIndex:=-1;
dbgridext_v.keine_farbe:=true;
dbgridext_b.keine_farbe:=false;
dbgridext_b.Refresh;
dbgridext_v.Refresh;
DBEdit_changeChange(self);
end;

procedure TForm_berechtigung.DBgridEXT_vEnter(Sender: TObject);
begin
dbgridext_b.keine_farbe:=true; //.SelectedField:=nil;// .SelectedIndex:=-1;
dbgridext_v.keine_farbe:=false;
dbgridext_b.Refresh;
dbgridext_v.Refresh;
DBEdit_change1Change(self);

end;

procedure TForm_berechtigung.DBEdit_changeChange(Sender: TObject);
var
objekt,info,query:string;
begin
if dbgridext_b.keine_farbe=true then exit;
if datamodul.q_1.findfield('objekt')=nil then exit;
objekt:=datamodul.q_1.findfield('objekt').AsString;
query:=format('select memo from berechtigung_vorlage where objekt="%s"',[objekt]);
info:=mysql_d.Feldinhalt(query,0);
memo1.Text:=info;
end;

procedure TForm_berechtigung.DBEdit_change1Change(Sender: TObject);
var
objekt,info,query:string;
begin
if dbgridext_v.keine_farbe=true then exit;
objekt:=datamodul.q_2.findfield('objekt').AsString;
query:=format('select memo from berechtigung_vorlage where objekt="%s"',[objekt]);
info:=mysql_d.Feldinhalt(query,0);
memo1.Text:=info;


//memo1.Text:=datamodul.q_2.findfield('objekt').AsString;

end;

procedure TForm_berechtigung.DBgridEXT_vDblClick(Sender: TObject);
begin
SpeedButton_hinzufuegenClick(Sender);
end;

procedure TForm_berechtigung.DBgridEXT_bDblClick(Sender: TObject);
begin
SpeedButton_entfernenClick(Sender);
end;

procedure TForm_berechtigung.FormDestroy(Sender: TObject);
begin
 form_main.listedeaktivieren(checklistbox_firmen.Items );
end;

end.



