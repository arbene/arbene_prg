unit a_labornummern;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls;

type
  TForm_labornummer = class(TForm)
    edit_vorspann: TEdit;
    UpDown1: TUpDown;
    Edit_nummer: TEdit;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_labornummer: TForm_labornummer;

implementation

uses a_main;

{$R *.dfm}

procedure TForm_labornummer.UpDown1Click(Sender: TObject; Button: TUDBtnType);
var
n:integer;
begin
try
  n:=strtoint(edit_nummer.text);
  if button =btnext then inc(n) else dec(n);
  if n<0 then n:=1;
  edit_nummer.text:=inttostr(n);
except
end;
end;

procedure TForm_labornummer.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

end.
