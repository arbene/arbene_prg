unit CUIAPI;

interface

const

// event
  SEV_CUIINIT = 'EV_CUIINIT';
  {
    This broadcast message notifies other plug-ins that CustomUI plug-in has been loaded
  }
  SEV_CUIINITSEQUENCE = 'EV_CUIINITSEQUENCE';
  {
    This broadcasr message is sent when a major sequence is started. The first parameter of
    InParams block has integer type and indicates, what kind of a sequence 
    has been initialized:
      0 - Installation
      1 - Maintenance
      2 - Uninstallation
  }
  SEV_CUIBEFORECREATEWINDOW = 'EV_CUIBEFORECREATEWINDOW';
  {
    This broadcast message indicates that a window is about to be created.
    The first parameter of InParams block contains the window's ID
  }
  SEV_CUIAFTERCREATEWINDOW = 'EV_CUIAFTERCREATEWINDOW';
  {
    This broadcast message indicates that a window is about to be created.
    The first parameter of InParams block contains the window's ID  
  }
  SEV_CUIBEFORECLOSEWINDOW = 'EV_CUIBEFORECLOSEWINDOW';
  {
    This broadcast message indicates that a window has been created.
    The first parameter in the InParams block has string type and contains the ID of
    the dialog window that has just been created.
  }
  SEV_CUICANCLOSEWINDOW = 'EV_CUICANCLOSEWINDOW';
  {
    This event is sent when a user tries to close the window by pressing
    "close" button in the system menu or pressing Alt+F4 combination. This event
    is NOT sent when the user presses "Cancel" or "Next" buttons.
    The first parameter in the InParams block has a string type and contains the ID of
    the dialog window that is about to be closed.
    Returning nonzero will cancel closing the window.
  }
  SEV_CUIBEFOREPROGRESS = 'EV_CUIBEFOREPROGRESS';
  {
    This event is sent before a progress window is created. 
    The returned value is not analyzed.
  }
  SEV_CUIAFTERPROGRESS = 'EV_CUIAFTERPROGRESS';
  {
    The EV_CUIAFTERPROGRESS event is sent after a progress window is destroyed.
    The returned value is not analyzed.
  }

// actions
  SCM_CUIISACTIVE = 'CM_CUIISACTIVE';
  {
    Find out the current state of the CustomUI plug-in.
    
    Returns the current plug-in state:
    0 - the plug-in is not loaded
    1 - the plug-in is loaded, but its configuration is still unread
    2 - the plug-in is ready.
  }
  SCM_CUISHOWWINDOW = 'CM_CUISHOWWINDOW';
  {
    Shows a window.
    The first parameter in the InParams block should have string type and should contain
    the ID of the window that must be shown. The second parameter should have boolean
    type and specifies if the window should be modal.
    If the window is not modal, CustomUI plug-in returns 1 if the operation was completed
    successfully. Otherwise the plug-in returns the modal result of the window as integer.
  }
  SCM_CUICLOSEWINDOW = 'CM_CUICLOSEWINDOW';
  {
    Closes a window.
    The first parameter in the InParams block should have string type and should contain
    the ID of the window that must be closed.
    Returns 1 if the operation was completed successfully.
  }
  SCM_CUIDOACTION = 'CM_CUIDOACTION';
  {

    Enables to perform an action as if it was used in Scripting actions tool. 
    You can use all project variables, functions, etc.
    The first parameter in the InParams block should have string type and should contain
    the action to be performed, i.e. SetProp("MyPlugDlg.Label2.Caption", "NewCaption")
    Note: The command DOES NOT return any values, that were calculated during the 
    evaluation of the action. Use variables to get the results.
  }
  SCM_CUIGETPROPERTY = 'CM_CUIGETPROPERTY';
  {
    Gets the property of a control or a window.
    The first parameter in the InParams block must have string type and contain 
    the property definition, i.e. MyPlugDlg.Progress.Position
    Returns the property value in the first parameter of the OutParams block.
  }
  SCM_CUISETPROPERTY = 'CM_CUISETPROPERTY';
  {
    Sets the property of a control or a window.
    The first parameter in the InParams block must have string type and contain the property
    definition, i.e. "MyPlugDlg.Progress.Position". The second parameter should contain
    new value of the property.
  }
  SCM_CUITRYABORT = 'CM_CUITRYABORT';
  {
    The CM_CUITRYABORT causes the CustomUI plug-in to ask the user if the installation
    should be aborted. The CustomUI plug-in aborts the installation, if confirmed by
    the user.
    If the user has confirmed aborting the installation, the CustomUI plug-in returns 1.
  }
  SCM_CUIENABLEMAINWINDOW = 'CM_CUIENABLEMAINWINDOW';
  {
    Turns the main window on/off.
    The first parameter in the InParams block has boolean type and defines if the main
    window is enabled (true - enabled, false - disabled).
  }
  SCM_CUIGETHANDLE = 'CM_CUIGETHANDLE';
  {
    Returns the handle of the control (or a window).
    The InParams block must have two string parameters: Window ID and Control ID.
    Returns the handle of the control. If the control ID wasn't specified, the handle
    of the window is returned.
  }
  SCM_CUIBROWSEFORFOLDER = 'CM_CUIBROWSEFORFOLDER';
  {
    Shows a browse for folder dialog. If the dialog is not specified in the 
    installation project, the standard window is shown. 
    The first parameter in the InParams block should have string type and should contain
    the default path.
    The path, selected by user is returned in the first parameter of the OutParams block.
    An empty string is returned if the user pressed Cancel button.
  }

implementation

end.
