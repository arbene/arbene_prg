unit a_vonbis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TForm_datumvonbis = class(TForm)
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    MaskEdit_von: TMaskEdit;
    MaskEdit_bis: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton_von: TSpeedButton;
    SpeedButton_bis: TSpeedButton;
    Label3: TLabel;
    procedure SpeedButton_vonClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_datumvonbis: TForm_datumvonbis;

implementation

uses a_kalender, a_main;

{$R *.DFM}

procedure TForm_datumvonbis.SpeedButton_vonClick(
  Sender: TObject);
begin
 if form_kalender.showmodal =mrok then
 begin
	if sender =speedbutton_von then  maskedit_von.Text:=form_kalender.MaskEdit1.text
	else maskedit_bis.Text:=form_kalender.MaskEdit1.text;
 end;
end;

procedure TForm_datumvonbis.BitBtn1Click(Sender: TObject);
begin
try
 strtodate(form_datumvonbis.MaskEdit_von.text);
 strtodate(form_datumvonbis.MaskEdit_bis.text);
 modalresult:=mrok;
except
	showmessage('Datum nicht korrekt');
end;

end;

procedure TForm_datumvonbis.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

end.
