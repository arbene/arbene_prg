object Form_vorsorge_abgleich: TForm_vorsorge_abgleich
  Left = 480
  Top = 162
  Width = 1200
  Height = 822
  Caption = 'Bescheinigungen planen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 326
    Width = 1184
    Height = 5
    Cursor = crVSplit
    Align = alTop
    Color = clMedGray
    ParentColor = False
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 1184
    Height = 124
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 25
      Width = 102
      Height = 13
      Caption = 'Beginn des Zeitraums'
    end
    object Label2: TLabel
      Left = 238
      Top = 25
      Width = 94
      Height = 13
      Caption = 'Ende des Zeitraums'
    end
    object Label3: TLabel
      Left = 20
      Top = 57
      Width = 155
      Height = 13
      Caption = 'Plandatum f'#252'r die Bescheinigung'
    end
    object Label4: TLabel
      Left = 436
      Top = 25
      Width = 209
      Height = 13
      Caption = 'in dem nach Bescheinigungen gesucht wird.'
    end
    object MaskEdit_ende: TMaskEdit
      Left = 343
      Top = 22
      Width = 75
      Height = 21
      EditMask = '!90/90/0000;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  .  .    '
    end
    object MaskEdit_beginn: TMaskEdit
      Left = 129
      Top = 22
      Width = 77
      Height = 21
      EditMask = '!90/90/0000;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '01.01.2000'
    end
    object MaskEdit_plan: TMaskEdit
      Left = 216
      Top = 54
      Width = 77
      Height = 21
      EditMask = '!90/90/0000;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '01.01.2000'
    end
    object BitBtn1: TBitBtn
      Left = 13
      Top = 85
      Width = 222
      Height = 20
      Caption = 'Probelauf durchf'#252'hren'
      TabOrder = 3
      OnClick = BitBtn1Click
    end
    object BitBtn_neu: TBitBtn
      Left = 293
      Top = 85
      Width = 254
      Height = 20
      Caption = 'fehlende Bescheinigunen exportieren'
      Enabled = False
      TabOrder = 4
      OnClick = BitBtn_neuClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00F82828282828
        2828828282828282828228FFFFFFFFFFFF2882FFFFFFF828288228F2828282F2
        822882F828282F282F8228F28282F2828F2882FF282F28282F8228FFF2F2828F
        FF2882FF2F282828FF8228F2F28282828F2882F82828F8282F8228F2828FFF82
        8F2882FFFFFFFFFFFF8228282828282828288282828282828282}
    end
    object BitBtn_hinfaellig: TBitBtn
      Left = 611
      Top = 85
      Width = 248
      Height = 21
      Caption = 'hinf'#228'llige Bescheinigungen exportieren'
      Enabled = False
      TabOrder = 5
      OnClick = BitBtn_hinfaelligClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00F82828282828
        2828828282828282828228FFFFFFFFFFFF2882FFFFFFF828288228F2828282F2
        822882F828282F282F8228F28282F2828F2882FF282F28282F8228FFF2F2828F
        FF2882FF2F282828FF8228F2F28282828F2882F82828F8282F8228F2828FFF82
        8F2882FFFFFFFFFFFF8228282828282828288282828282828282}
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 124
    Width = 1184
    Height = 202
    Align = alTop
    Caption = 
      'Aufgrund der Gef'#228'hrdungen am Arbeitsplatz fehlen diese Bescheini' +
      'gungen und sollten dem Probanden hinzugef'#252'gt werden.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 2
      Top = 15
      Width = 1180
      Height = 185
      Align = alClient
      DataSource = DataModul.ds_14
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -9
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Expanded = False
          FieldName = 'abteilung'
          Title.Caption = 'Abteilung'
          Width = 146
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ap'
          Title.Caption = 'Arbeitsplatz'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Name'
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vorname'
          Width = 133
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'geb_dat'
          Title.Caption = 'Geburtsdatum'
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bescheinigung'
          Width = 361
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Plan_Datum'
          Title.Caption = 'geplant'
          Width = 107
          Visible = True
        end>
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 331
    Width = 1184
    Height = 336
    Align = alClient
    Caption = 
      'Diese Bescheinigungen sind geplant, sollten aber storniert werde' +
      'n, da die Gef'#228'hrdung nicht mehr besteht.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object DBGrid2: TDBGrid
      Left = 2
      Top = 15
      Width = 1180
      Height = 319
      Align = alClient
      DataSource = DataModul.ds_15
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -9
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Expanded = False
          FieldName = 'Abteilung'
          Width = 145
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ap'
          Title.Caption = 'Arbeitsplatz'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Name'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vorname'
          Width = 133
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'geb_dat'
          Title.Caption = 'Geburtsdatum'
          Width = 93
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bescheinigung'
          Width = 351
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Plan_Datum'
          Title.Caption = 'geplant'
          Width = 101
          Visible = True
        end>
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 667
    Width = 1184
    Height = 116
    Align = alBottom
    TabOrder = 3
    object BitBtn_ok: TBitBtn
      Left = 19
      Top = 67
      Width = 267
      Height = 25
      Caption = 'obige Bescheinigungen eintragen bzw. stornieren'
      Default = True
      TabOrder = 0
      OnClick = BitBtn_okClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtn_esc: TBitBtn
      Left = 374
      Top = 67
      Width = 118
      Height = 25
      Caption = 'Abbruch'
      ModalResult = 3
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object CheckBox_eintragen: TCheckBox
      Left = 15
      Top = 11
      Width = 694
      Height = 14
      Caption = 'Fehlende Bescheinigungen werden eingetragen'
      TabOrder = 2
    end
    object CheckBox_storno: TCheckBox
      Left = 15
      Top = 34
      Width = 616
      Height = 17
      Caption = 
        'geplante Bescheinigungen des Probanden, die nicht der Gef'#228'hrdung' +
        'slage entsprechen,  werden storniert'
      TabOrder = 3
    end
  end
end
