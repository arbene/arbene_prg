object Form_dbpfadwahl: TForm_dbpfadwahl
  Left = 751
  Top = 206
  Width = 463
  Height = 559
  Caption = 'Pfadauswahl'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 88
    Top = 6
    Width = 234
    Height = 16
    Caption = 'Verzeichnis mit Doppelklick ausw'#228'hlen!'
  end
  object DriveComboBox: TDriveComboBox
    Left = 69
    Top = 313
    Width = 279
    Height = 22
    DirList = DirectoryListBox
    TabOrder = 0
  end
  object DirectoryListBox: TDirectoryListBox
    Left = 68
    Top = 27
    Width = 277
    Height = 282
    ItemHeight = 16
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 455
    Width = 447
    Height = 66
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object BitBtn_ok: TBitBtn
      Left = 41
      Top = 18
      Width = 147
      Height = 31
      Caption = 'OK'
      Default = True
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtn_abb: TBitBtn
      Left = 241
      Top = 18
      Width = 148
      Height = 31
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Kind = bkAbort
    end
  end
  object Edit_uv: TEdit
    Left = 69
    Top = 360
    Width = 277
    Height = 24
    TabOrder = 3
  end
  object Button_anlegen: TButton
    Left = 69
    Top = 396
    Width = 269
    Height = 30
    Caption = 'Unterverzeichnis anlegen'
    TabOrder = 4
    OnClick = Button_anlegenClick
  end
end
