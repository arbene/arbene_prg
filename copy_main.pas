unit copy_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables, Mask, DBCtrls, Grids, DBGrids, dbgridEXT;

type
  TForm1 = class(TForm)
    Table2: TTable;
    Button1: TButton;
    Table1: TTable;
    OpenDialog: TOpenDialog;
    DBgridEXT1: TDBgridEXT;
    DataSource1: TDataSource;
    DBEdit1: TDBEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
	feldname:string;
  i,j:integer;
begin
if not opendialog.Execute then exit;

table1.tablename:=opendialog.FileName;
table2.TableType:=ttDBase;
table2.tablename:='befund_db.dbf';
table1.FieldDefs.Update;
table1.IndexDefs.Update;
table2.FieldDefs.Assign(table1.fielddefs);
table2.IndexDefs.Assign(table1.indexdefs);
table2.createtable;
table1.Open;
table2.open;


table1.First;
while not table1.Eof do
begin
     table2.Append;
  	for i:=0 to table1.fieldcount-1 do
		begin  //4
  	feldname:= lowercase( table1.Fields[i].fieldname);
     if  table1[feldname]<>null then
     	table2[feldname]:=table1[feldname];
     end;
	 	table1.Next;
end;

end;


end.
