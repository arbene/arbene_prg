unit a_abteilung;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, dbgridEXT, ComCtrls, ToolWin;

type
  TForm_abteilung = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Edit_firma: TEdit;
    Edit_telefon: TEdit;
    Edit_vorname: TEdit;
    Edit_name: TEdit;
    Edit_beschreibung: TEdit;
    Edit_kuerzel: TEdit;
    Edit_email: TEdit;
    Edit_ma_telefon: TEdit;
    Edit_ma_vorname: TEdit;
    Edit_ma_name: TEdit;
    Edit_ma_email: TEdit;
    GroupBox2: TGroupBox;
    DBgridEXT_ap: TDBgridEXT;
    ToolBar: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_close: TToolButton;
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton_newClick(Sender: TObject);
    procedure ToolButton_closeClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_abteilung: TForm_abteilung;

implementation

uses a_main, a_data,  a_arbeitsplatz, a_tabellen;

{$R *.DFM}

procedure TForm_abteilung.FormCreate(Sender: TObject);
var
	s_vorges,abteilung,query:string;
	felder:array[0..10] of string;
  s_firma,s_proband:string;
begin
with datamodul do
begin
   s_proband:=getbigint_str(datamodul.q_mitarbeiter,'nummer');

   query:=format('select  a_bereich.nummer, a_bereich.name as Arbeitsbereich from a_bereich_proband left join a_bereich on (a_bereich_proband.i_a_bereich= a_bereich.nummer)  where i_proband=%s',[s_proband]);
   sql_new(false,q_2,query,'');



   query:='select name, vorname,telefon_f, email_f,abteilung, i_firma  from mitarbeiter where nummer='+s_proband;
   mysql_d.Feldarray(query,[0,1,2,3,4,5],felder);
   edit_ma_name.text:=felder[0];
   edit_ma_vorname.text:=felder[1];
   edit_ma_telefon.text:=felder[2];
   edit_ma_email.text:=felder[3];
	 abteilung:=felder[4];
	 edit_kuerzel.text:=abteilung;
	 s_firma:=felder[5];
	 if s_firma='' then s_firma:='0';
   if abteilung<>'' then
   begin
      query:=format('select i_mitarbeiter,langtext from abteilung where kuerzel="%s" and i_firma=%s',[abteilung, s_firma]);
      mysql_d.Feldarray(query,[0,1],felder);
      s_vorges:=felder[0];
      if s_vorges='' then s_vorges:='-1';
		 edit_beschreibung.text:=felder[1];
		 query:='select name, vorname,telefon_f, email_f from mitarbeiter where nummer='+s_vorges;
      mysql_d.Feldarray(query,[0,1,2,3,4],felder);
      edit_name.text:=felder[0];
      edit_vorname.text:=felder[1];
      edit_telefon.text:=felder[2];
		 edit_email.text:=felder[3];
   end;
	 query:=format('select firma from firma where nummer=%s',[s_firma]);
	 edit_firma.text:=mysql_d.feldinhalt(query,0);

   form_main.form_positionieren(tform(sender));
end;
end;
procedure TForm_abteilung.ToolButton_newClick(Sender: TObject);
var
query,s_proband,s_ap:string;
i_firma:integer;
nummer:int64;
begin
   try
   i_firma:=datamodul.q_firma['nummer'];
   query:=format('select * from a_platz where i_firma=%d',[i_firma]);
   datamodul.sql(false,datamodul.q_1,query,'');
   form_tabellen:=tform_tabellen.create(self);
   form_tabellen.Notebook.PageIndex:=13;
   form_tabellen.DBEdit_ap_name.datafield:='name';
   form_tabellen.DBGrid_tabellen.Columns[1].fieldname:='Name';
	 form_tabellen.DBGrid_tabellen.Columns[1].width:=form_tabellen.DBGrid_tabellen.width-50;
	 form_tabellen.DBGrid_tabellen.Columns[0].width:=0;
   if form_tabellen.ShowModal=mrok then
   begin
     nummer:=mysql_d.neue_nummer('a_bereich_proband');
     s_proband:=getbigint_str( datamodul.q_mitarbeiter,'nummer');
     s_ap:=getbigint_str(datamodul.q_1,'nummer');
     query:=format('insert a_bereich_proband set nummer=%s, i_proband=%s, i_a_bereich=%s',[inttostr(nummer),s_proband,s_ap]);
     mysql_d.sql_exe(query);
     datamodul.q_2.Refresh;
   end;
   finally
   form_tabellen.release;
   end;
end;

procedure TForm_abteilung.ToolButton_closeClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_abteilung.ToolButton_delClick(Sender: TObject);
begin
dateiloeschen(datamodul.q_2,false,false);
end;

procedure TForm_abteilung.BitBtn1Click(Sender: TObject);
begin
try
  datamodul.q_2.Close;
  datamodul.q_3.Close;
  datamodul.q_4.Close;
  datamodul.q_5.Close;
  datamodul.q_6.Close;
  form_ap:=tform_ap.create(self);
  
  form_ap.showmodal;
finally
  form_ap.release;
end;
end;

end.
