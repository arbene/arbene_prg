�
 TFORM_TERMINE 05b  TPF0TForm_termineForm_termineLeft�Top� Width]Height�HelpContext&Caption   TerminübersichtColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MenuMainMenu_terminOldCreateOrderPosition	poDefaultOnCloseQueryFormCloseQueryOnCreate
FormCreateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight 	TSplitter	Splitter1Left�Top,Height)AlignalRightOnMovedSplitter1Moved  TToolBarToolBar1Left Top WidthMHeightAutoSize	ButtonHeightButtonWidth9CaptionToolBar1ImagesForm_main.ImageList_mainTabOrder  TToolButtonToolButton15Left TopWidthCaptionToolButton15
ImageIndexStyletbsSeparator  TToolButtonToolButton_neuLeftTopHintNeuer TerminCaptionToolButton_neu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonToolButton2Left>TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeftFTopHint   Termin verändernCaptionToolButton_edit
ImageIndexParentShowHintShowHint	OnClickToolButton_editClick  TToolButtonToolButton4LeftTopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton_speichernLeft� TopHint	SpeichernCaptionToolButton_speichern
ImageIndexParentShowHintShowHint	OnClickToolButton_speichernClick  TToolButtonToolButton6Left� TopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_cancelLeft� TopHint   Änderung verwerfenCaptionToolButton_cancel
ImageIndexParentShowHintShowHint	OnClickToolButton_cancelClick  TToolButtonToolButton9LeftTopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_loeschenLeft	TopHint   Termin löschenCaptionToolButton_loeschen
ImageIndexParentShowHintShowHint	OnClickToolButton_loeschenClick  TToolButtonToolButton13LeftBTopWidthCaptionToolButton13
ImageIndexStyletbsSeparator  TToolButtonToolButton_schichtLeftJTopHintSchichtplanCaptionToolButton_schicht
ImageIndexParentShowHintShowHint	OnClickToolButton_schichtClick  TToolButtonToolButton7Left�TopWidth<CaptionToolButton7
ImageIndexStyletbsSeparator  TPanelPanel1Left�TopWidth� HeightTabOrder  TPanelPanel11LeftTopWidth(HeightAlignalLeft
BevelOuterbvNoneTabOrder  TSpeedButtonSpeedButton_datum_runterLeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333�3333337D333333�s333337DG33333�w�33337DDC3333�s733337DDD3333�s3s3337DDDG333�s37�337DDDDC33�s337337DDDDD337�������������s7wwwwws3�����337s�33s�333�����337s�37�3333����3337s�7?33333���33337s�s�33333���33337s��333333��333337w�3333333�3333337s	NumGlyphsParentShowHintShowHintOnClickSpeedButton_datum_runterClick   TPanelPanel10Left� TopWidth HeightAlignalRight	AlignmenttaRightJustify
BevelOuterbvNoneTabOrder TSpeedButtonSpeedButton_datum_raufLeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�333333Ds333333w��33333tDs333337w?�33334DDs33337?w?�3333DDDs3333s�w?�333tDDDs3337�3w?�334DDDDs337?33w?�33DDDDDs?����w���������wwwwww3w33�����333s33?w33<����3337�3?w333<���333373?w3333���33333s?w3333<��333337�w33333<�3333337w333333�3333333w3333333LayoutblGlyphRight	NumGlyphsOnClickSpeedButton_datum_raufClick   TEdit
Edit_datumLeft#TopWidth� HeightReadOnly	TabOrderText
Edit_datumOnChangeEdit_datumChange   TToolButtonToolButton14Left�TopWidthCaptionToolButton14
ImageIndexStyletbsSeparator  TToolButtonToolButton_kalenderLeft�TopHintMonatskalenderCaption
ToolButton
ImageIndex	ParentShowHintShowHint	OnClickToolButton_kalenderClick  TToolButtonToolButton1Left�TopWidth1CaptionToolButton1
ImageIndexStyletbsSeparator  TToolButtonToolButton_filternLeft!TopHintKalender filternCaptionToolButton_filtern
ImageIndexParentShowHintShowHint	OnClickToolButton_filternClick  TToolButtonToolButton10LeftZTopWidth1CaptionToolButton10
ImageIndexStyletbsSeparator  TToolButtonToolButton_exitLeft�TopHintKalender verlassenCaptionToolButton_exit
ImageIndexParentShowHintShowHint	OnClickToolButton_exitClick  TToolButtonToolButton5Left�TopWidthCaptionToolButton5
ImageIndexStyletbsSeparator   TPageControlPageControl_zeitLeft Top,Width�Height)
ActivePageTabSheet_monatAlignalClientTabOrderOnChangePageControl_zeitChange
OnChangingPageControl_zeitChanging 	TTabSheetTabSheet_tagCaptionTag 
TDBgridEXTDBgridEXT_tagLeft Top Width�HeightAlignalClient
DataSourceDataModul.ds_termin0DefaultDrawing	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OptionsdgTitles
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit 
ParentFontTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldName
ber_zeit_sTitle.CaptionBeginnWidth<Visible	 Expanded	FieldName
ber_zeit_eTitle.CaptionEndeWidth<Visible	 Expanded	FieldNamebmp_nrTitle.Caption WidthVisible	 Expanded	FieldNamefirmaTitle.CaptionFirmaWidth>Visible	 Expanded	FieldNameinfoTitle.CaptionInfoWidthpVisible	     	TTabSheetTabSheet_wocheCaptionWoche
ImageIndex 	TGroupBoxGroupBox_moLeft Top Width� Height� CaptionGroupBox_moTabOrder  
TDBgridEXTDBgridEXT_moLeftTopWidth� Height� AlignalClientBorderStylebsNone
DataSourceDataModul.ds_termin1DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sWidthVisible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoWidth� Visible	     	TGroupBoxGroupBox_doLeft� Top Width� Height� CaptionGroupBox_doTabOrder 
TDBgridEXTDBgridEXT_doLeftTopWidth� Height� AlignalClientBorderStylebsNone
DataSourceDataModul.ds_termin4DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sWidth;Visible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoWidth� Visible	     	TGroupBoxGroupBox_diLeft Top� Width� Height� CaptionGroupBox_diTabOrder 
TDBgridEXTDBgridEXT_diLeftTopWidth� Height� AlignalClientBorderStylebsNoneCtl3D	
DataSourceDataModul.ds_termin2DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ParentCtl3DReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sVisible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoWidth� Visible	     	TGroupBoxGroupBox_saLeft� Top@Width� HeightICaptionGroupBox_saTabOrder 
TDBgridEXTDBgridEXT_saLeftTopWidth� Height8AlignalClientBorderStylebsNoneCtl3D	
DataSourceDataModul.ds_termin6DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ParentCtl3DReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sVisible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoVisible	     	TGroupBoxGroupBox_miLeft Top@Width� Height� CaptionGroupBox_miTabOrder 
TDBgridEXTDBgridEXT_miLeftTopWidth� Height� AlignalClientBorderStylebsNoneCtl3D	
DataSourceDataModul.ds_termin3DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ParentCtl3DReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sVisible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoVisible	     	TGroupBoxGroupBox_frLeft� Top� Width� Height� CaptionGroupBox_frTabOrder 
TDBgridEXTDBgridEXT_frLeftTopWidth� Height� AlignalClientBorderStylebsNoneCtl3D	
DataSourceDataModul.ds_termin5DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ParentCtl3DReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sVisible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoVisible	     	TGroupBoxGroupBox_soLeft� Top�Width� HeightICaptionGroupBox_soTabOrder 
TDBgridEXTDBgridEXT_soLeftTopWidth� Height8AlignalClientBorderStylebsNoneCtl3D	
DataSourceDataModul.ds_termin7DefaultDrawing	Options
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ParentCtl3DReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_miCellClick
OnDblClickDBgridEXT_DblClick
OnDragDropDBgridEXT_DragDrop
OnDragOverDBgridEXT_DragOverkeine_farbeZeitdiff_L_B2OnLButtonDownDBgridEXT_EnterOnLbuttongehaltenDBgridEXT_moLbuttongehaltendarf_ziehen	ColumnsExpanded	FieldNamezeit_sVisible	 Expanded	FieldNamebmp_nrWidthVisible	 Expanded	FieldNamefirmaVisible	 Expanded	FieldNameinfoVisible	      	TTabSheetTabSheet_monatCaption   Monatsübersicht
ImageIndex TPanel_zeitplanPanel_zeitplan1Left Top Width�HeightAlignalClientColorclWindowTabOrder OnClickPanel_zeitplan1Click
OnDblClickPanel_zeitplan1DblClickOnMouseDownPanel_zeitplan1MouseDownOnMouseMovePanel_zeitplan1MouseMove	OnMouseUpPanel_zeitplan1MouseUpdatum      A�@zeit_minzeit_max	zeit_step   	TTabSheetTabSheet_listeCaptionMonats-Liste
ImageIndex 
TDBgridEXTDBgridEXT_listeLeft Top Width�HeightAlignalClient
DataSourceDataModul.ds_termin_monatDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgRowSelectdgCancelOnExitdgMultiSelect ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBgridEXT_listeCellClick
OnDblClickDBgridEXT_DblClickkeine_farbeZeitdiff_L_B OnLButtonDownDBgridEXT_listeLButtonDowndarf_ziehenColumnsExpanded	FieldNamedatum_sTitle.CaptionDatumVisible	 Expanded	FieldNamebmp_nrTitle.Caption WidthVisible	 Expanded	FieldNamefirmaTitle.CaptionFirmaVisible	 Expanded	FieldNameinfoTitle.CaptionInfoWidth8Visible	      TPanelPanel_top_mainLeft TopWidthMHeightAlignalTop
BevelOuterbvNoneTabOrder TPanelPanel3Left Top WidthHHeightAlignalLeft
BevelOuterbvNoneTabOrder    TPageControlPageControl_rechtsLeft�Top,Width]Height)
ActivePageTabSheet_detailAlignalRightHotTrack	TabOrderOnChangePageControl_rechtsChange 	TTabSheetTabSheet_detailCaptionTermin-Details 	TGroupBoxGroupBox_zeitpLeft Top WidthUHeightYAlignalTopCaption	ZeitpunktTabOrder  TLabelLabel2LeftTop6Width$HeightCaptionBeginn:  TLabelLabel3Left� Top6WidthHeightCaptionEnde:  TLabelLabel1LeftTopWidth"HeightCaptionDatum:  TSpeedButtonSpeedButton_terminaendernLeft}TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_terminaendernClick  TDBEditDBEdit_datum_sTagLeft/TopWidthIHeightColorclSilver	DataFieldDatum_s
DataSourceDataModul.ds_termin0ReadOnly	TabOrder   TDBEdit_timeDBEdit_zeit_sLeft/Top2WidthAHeight	DataFieldzeit_s
DataSourceDataModul.ds_termin0TabOrderOnEnterDBEdit_zeit_sEnterOnExitDBEdit_zeit_sExit  TDBEdit_timeDBEdit_zeit_eLeft� Top2WidthAHeight	DataFieldzeit_e
DataSourceDataModul.ds_termin0TabOrder   	TGroupBoxGroupBox_anmLeft Top1WidthUHeight� AlignalTopCaption	AnmerkungTabOrder TDBMemoDBMemo_detailLeftTopWidthQHeightpAlignalClientTabOrder    	TGroupBoxGroupBox_term_datLeft TopYWidthUHeight� AlignalTopCaptionTermindatenTabOrder TLabel	Label_typLeftTop8WidthHeightCaptionAnlass  TLabelLabel_t_artLeftTopWidth5HeightCaption	   Maßnahme  TLabelLabel_vornameLeftTop� Width*HeightCaptionVorname  TLabelLabel_nachnameLeftTop� Width4HeightCaptionNachname  TLabelLabel5LeftToplWidthHeightCaptionFirma  TDBEditDBEdit_vornameLeft?Top� Width� Height	DataFieldVorname
DataSourceDataModul.ds_termin0TabOrder  TDBEditDBEdit_nachnameLeft?Top� Width� Height	DataFieldName
DataSourceDataModul.ds_termin0TabOrder  TDBCombo_numDBCombo_num_artLeft?TopWidth� Heightalle_einfuegenkeines_einfuegen	DataFieldi_art
DataSourceDataModul.ds_termin0zconnectionDataModul.connection_main	ztab_nameartzfield_textnamezfield_indexnummerzordernummerzstorno
ItemHeightTabOrder   TDBEditDBEdit_firmaLeft?TopiWidth� Height	DataFieldfirma
DataSourceDataModul.ds_termin0TabOrder  TDBMemoDBMemo_anlassLeft@Top6Width� Height	DataFieldinfo
DataSourceDataModul.ds_termin0ReadOnly	TabOrder    	TTabSheetTabSheet_terminierenCaption#geplante Untersuchungen terminieren
ImageIndex TPanelPanel2Left Top WidthUHeight)AlignalTop
BevelOuterbvNoneCaptionPanel2TabOrder  TBitBtnBitBtn_untersuchung_selectLeft(TopWidth� HeightCaptionUntersuchungen selektierenTabOrder OnClickBitBtn_untersuchung_selectClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3������33wwwwww3� ����33w333�3�����33s�37�3���� 337�3ws3����3377s33�� ��333w333�� ��333w333�� ��33?w�33�� �33s�ws33�����3377333�� ��333w333�����3337�33��� ��3333w�33��� ��3333w333������33�����3������33wwwwwws	NumGlyphs   
TDBgridEXTDBgridEXT_unt_filterLeft Top)WidthUHeight�Hint7   Ziehen Sie den Mitarbeiter auf den gewünschten Termin.AlignalClient
DataSourceDataModul.ds_unt_filterDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ParentShowHintReadOnly	ShowHint	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT_unt_filterDblClickkeine_farbeZeitdiff_L_B OnLButtonDownDBgridEXT_unt_filterLButtonDowndarf_ziehenColumnsExpanded	FieldNamedatumTitle.CaptionDatumVisible	 Expanded	FieldNamenameTitle.CaptionNameWidthSVisible	 Expanded	FieldNamevornameTitle.CaptionVornameWidthEVisible	 Expanded	FieldNamekuerzelTitle.CaptionFirmaWidth>Visible	    TPanelPanel7Left Top�WidthUHeight<AlignalBottom
BevelOuterbvNoneTabOrder TLabelLabel9LeftTopWidth*HeightCaptionAnlass:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4Left
TopWidth#HeightCaptionFirma:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText2Left7Top	Width� Height	DataFieldfirma
DataSourceDataModul.ds_unt_filterFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBMemoDBMemo_selekt_anlassLeft7TopWidth� Height	DataFielduntersuchung
DataSourceDataModul.ds_unt_filterTabOrder     	TTabSheetTabSheet_zeitCaptionZeiterfassung
ImageIndex TPanelPanel_fsi_obenLeft Top WidthUHeight� AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel41LeftTop6Width)HeightCaptionDatum:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel36LeftTophWidth-HeightCaption
Erbringer:  TLabelLabel20Left� TopJWidth	HeightCaption :   TLabelStundenLeft� Top6Width0HeightCaptionStundenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelMinutenLeft� Top6Width.HeightCaptionMinutenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TSpeedButtonSpeedButton6LeftUTopEWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphs  TLabelLabel195Left� TopeWidth1HeightCaption
Zeitansatz  TLabelLabel196Left� TopwWidthHeightCaption%  TLabelLabel252LeftvTop6Width$HeightCaption Beginn  TLabelLabel253Left� Top6WidthHeightCaptionEnde  TDBEditDBEdit_z_dateTagLeftTopFWidth@Height	DataFieldDatumReadOnly	TabOrder   TDBEditDBEdit_z_StundenLeft� TopFWidth$Height	DataFieldStundenReadOnly	TabOrder  TDBEditDBEdit_z_minutenLeft� TopFWidth$Height	DataFieldMinutenReadOnly	TabOrder  TDBCombo_numDBCombo_num_z_erbringerLeftTopwWidth� Heightalle_einfuegenkeines_einfuegen	DataFieldi_untersucherzconnectionDataModul.connection_main	ztab_nameuntersucherzfield_textuntersucherzfield_indexnummerzorderuntersucherzstorno	
ItemHeightReadOnly	TabOrder  TDBComboBoxDBComboBox_z_zeitansatzLeft� ToptWidthFHeightHint   <- und -> zum verändernAutoComplete	DataField
zeitansatzDropDownCount
ItemHeightItems.Strings1501007566605033250 ParentShowHintReadOnly	ShowHint	TabOrder  TDBCheckBoxDBCheckBox_z_zusatzLeftTopvWidth[HeightCaptionZusatzleistung	DataFieldueberstundenReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBEdit_timeDBEdit_z_beginnLeftuTopFWidth$Height	DataFieldzeit_sReadOnly	TabOrder  TDBEdit_timeDBEdit_z_endeLeft� TopFWidth%Height	DataFieldzeit_eReadOnly	TabOrder  TEdit
Edit_firmaLeftTopWidth)HeightReadOnly	TabOrder   TPanelPanel_fsi_2Left Top� WidthUHeight3AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel170Left� Top Width+HeightCaption
Kriterium:  TLabelLabel184LeftTop WidthHeightCaptionKonto:  TDBCombo_num"DBCombo_num_z_abrechnungskriteriumLeft� TopWidth� Heightalle_einfuegenkeines_einfuegen	DataFieldi_kriteriumzconnectionDataModul.connection_main	ztab_nameabr_anlass_kritzfield_textnamezfield_indexnummerzorderreihenfolgezstorno	
ItemHeightReadOnly	TabOrder  TDBCombo_numDBCombo_num_z_zeitkontoLeftTopWidth� Heightalle_einfuegenkeines_einfuegen	DataFieldi_zeitkontozconnectionDataModul.connection_main	ztab_nameabr_zeitkontozfield_textnamezfield_indexnummerzorderreihenfolgezstorno	
ItemHeightReadOnly	TabOrder    	TGroupBoxGroupBox_fsi_3Left Top� WidthUHeight� AlignalTopCaptionAufgabenfeldTabOrder TDBMemoDBMemo_z_aufgabeLeftTopWidthQHeight|AlignalClient	DataFieldanlassReadOnly	
ScrollBars
ssVerticalTabOrder    	TGroupBoxGroupBox_fsi_4Left TopaWidthUHeight� AlignalTopCaption   Tätigkeit / AufwandTabOrder TDBMemoDBMemo_z_aufwandLeftTopWidthQHeightoAlignalClient	DataFieldmemoReadOnly	
ScrollBars
ssVerticalTabOrder    	TGroupBoxGroupBox_fsi_5Left Top�WidthUHeight,AlignalClientCaptionAnmerkung privatTabOrder TDBMemoDBMemo_z_anmerkungLeftTopWidthQHeightAlignalClient	DataFieldmemo2ReadOnly	
ScrollBars
ssVerticalTabOrder      
TStatusBar	StatusBarLeft TopUWidthMHeightPanelsWidth2    	TMainMenuMainMenu_terminImagesForm_main.ImageList_main	OwnerDraw	Left0Top" 	TMenuItemDatei1CaptionDatei 	TMenuItemm_NeuCaptionNeu
ImageIndex ShortCutN@OnClickToolButton_neuClick  	TMenuItem	Verndern1Caption   Ändern
ImageIndexShortCut�@OnClickToolButton_editClick  	TMenuItem
Speichern1Caption
Speichern 
ImageIndexShortCutS@OnClickToolButton_speichernClick  	TMenuItem	Rckgngig1Caption	Abbrechen
ImageIndexShortCutA@OnClickToolButton_cancelClick  	TMenuItemLschen1Caption	   Löschen 
ImageIndexShortCutL@OnClickToolButton_loeschenClick  	TMenuItemAll_delCaption   Alles LöschenEnabledOnClickAll_delClick  	TMenuItemN2Caption-  	TMenuItem
Tagesplan1CaptionTagesplan  nach Excel ausgebenOnClickTagesplan1Click  	TMenuItemdruckCaptionEinladung  druckenOnClick
druckClick  	TMenuItemEmailCaptionEinladung als Email versenden
ImageIndexOnClick
EmailClick  	TMenuItemN1Caption-  	TMenuItem
Verlassen1Caption	Verlassen
ImageIndexShortCutV@OnClickToolButton_exitClick   	TMenuItemBearbeiteb1Caption
Bearbeiten 	TMenuItemFiltern1CaptionTermine filtern
ImageIndexOnClickToolButton_filternClick  	TMenuItemTerminieren1CaptionTerminierenOnClickTerminieren1Click    
TPopupMenu
PopupMenu1LeftXTop  	TMenuItem	m_schichtCaptionSchichtplan aufrufenOnClickToolButton_schichtClick    