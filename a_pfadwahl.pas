unit a_pfadwahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl,math;

type
  TForm_pfadwahl = class(TForm)
    DriveComboBox: TDriveComboBox;
    DirectoryListBox: TDirectoryListBox;
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    Edit_uv: TEdit;
    Button_anlegen: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button_anlegenClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_pfadwahl: TForm_pfadwahl;

implementation

uses a_main, abackup_main;

//uses ;

{$R *.DFM}

procedure TForm_pfadwahl.FormCreate(Sender: TObject);
var hoehe:integer;
begin

	hoehe:=screen.Height-30;
	height:=min( height, height);
	height:=min( height, hoehe);
	Width:=min( Width,Width);
	Width:=min( Width,screen.Width);
	 top:=top+25+(height div 2)-( Height div 2);
	if  top+ Height>hoehe then
	 top:=hoehe- Height;
	 top:=max( top,0);
	 Left:=left + (width div 2) - ( width div 2);
	if  left+ Width>screen.Width then
		 left:=screen.width- width;
	 left:=max(  left,0);

  form_main.form_positionieren(tform(sender));
end;

procedure TForm_pfadwahl.Button_anlegenClick(Sender: TObject);
var
dir:string;
begin
	dir:=getcurrentdir;
	if setcurrentdir(directorylistbox.Directory) then createdir(edit_uv.text);
	setcurrentdir(dir);
  directorylistbox.update;
end;

end.
