unit a_arztbrief_drucken;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls,a_com;

type
  TForm_arztbrief_drucken = class(TForm)
    Panel1: TPanel;
    BitBtn_cancel: TBitBtn;
    BitBtn_ok: TBitBtn;
    CheckListBox: TCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure liste_ini;
    procedure drucken(n:integer);
  public
    { Public-Deklarationen }
  end;

var
  Form_arztbrief_drucken: TForm_arztbrief_drucken;

implementation

uses a_main, a_data;

{$R *.DFM}

procedure TForm_arztbrief_drucken.FormCreate(Sender: TObject);
begin
		 form_main.form_positionieren(tform(sender));
      liste_ini;
end;


procedure TForm_arztbrief_drucken.liste_ini;
VAR
i:integer;
i_ma:int64;
k:pkontaktpointer;
query,s:string;
begin
with datamodul do begin
  i_ma:=getbigint( datamodul.q_mitarbeiter,'nummer');
  query:=format('select kontakte.* from kontakte where i_mitarbeiter=%s %s order by datum desc',[inttostr(i_ma),and_storno]);
  sql(false,datamodul.q_1,query,'');
  while not datamodul.q_1.Eof do
  begin
       new(k);
       k.nummer:=getbigint(q_1,'nummer');
       k.name:=q_1.findfield('name').asstring;
       k.vorname:=q_1.findfield('vorname').asstring;
       k.zhd:=q_1.findfield('zhd').asstring;
       k.strasse:=q_1.findfield('strasse').asstring;
       k.plz:=q_1.findfield('plz').asstring;
       k.ort:=q_1.findfield('ort').asstring;
       k.email :=q_1.findfield('email').asstring;
       s:=format('%s , %s , %s , %s',[k.name,k.vorname,k.zhd,k.ort]);
       checklistbox.Items.AddObject(s,tobject(k));
       checklistbox.checked[checklistbox.Items.Count-1]:=true;
       q_1.next;
  end;

  {form_main.strings_laden(datamodul.q_mitarbeiter,'memo',checklistbox.items);
  for i:=0 to checklistbox.Items.Count -1 do
   checklistbox.checked[i]:=true;}
 end;
end;


procedure TForm_arztbrief_drucken.BitBtn_okClick(Sender: TObject);
var i:integer;
begin
for i:=0 to checklistbox.Items.Count -1 do
  begin
       if checklistbox.checked[i] then drucken(i);
  end;
modalresult:=mrok;
end;


procedure  TForm_arztbrief_drucken.drucken(n:integer);
var
adressat, nachrichtlich,s,bm:string;
i:integer;
k:pkontaktpointer;

procedure string_to_liste(z:string;var liste:tstringlist);
var
p:integer;
begin
p:=pos(';',z);
   while p >0 do
   begin
     liste.add(copy(z,1,p-1));
     z:=copy( z,p+1,length(z));
     p:=pos(';',z);
   end;
end;
//##############################
begin
  //adr_list:=tstringlist.Create;
  form_main.olecontainer_laden( datamodul.q_dokumente,'ole',form_main.arbeitsmedizin);
  olecontainer_anzeigen(form_main.arbeitsmedizin);
  //string_to_liste(checklistbox.Items[n],adr_list);
  k:=pkontaktpointer(checklistbox.items.Objects[n]);
  com.bookmark_einfuegen('adr_name',k.name,true);
  com.bookmark_einfuegen('adr_vorname',k.vorname,true);
  com.bookmark_einfuegen('adr_zhd',k.zhd,true);
  com.bookmark_einfuegen('adr_strasse',k.strasse,true);
  com.bookmark_einfuegen('adr_plz',k.plz,true);
  com.bookmark_einfuegen('adr_ort',k.ort,true);


  for i:=0 to checklistbox.Items.Count-1 do
  begin
      if checklistbox.Checked[i] and (i<>n) then
      begin
        k:=pkontaktpointer(checklistbox.items.Objects[i]);
        s:=format('%s %s %s, %s, %s %s',[k.name,k.vorname,k.zhd,k.strasse, k.plz, k.ort]);
        nachrichtlich:=nachrichtlich+s+#13;
      end;
  end;
  com.bookmark_einfuegen('nachrichtlich',nachrichtlich,true);

  com.doku_drucken;

  end;

procedure TForm_arztbrief_drucken.FormDestroy(Sender: TObject);
var i:integer;
begin
 for i:= 0 to checklistbox.Items.count-1 do
       dispose(pkontaktpointer(checklistbox.Items.Objects[i]));
 checklistbox.items.Clear;

end;

end.
