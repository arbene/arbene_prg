unit a_k_liste;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, dbtreeview_kaskade, ToolWin, TreeView_ext;

type
  Tform_k_liste = class(TForm)
    ToolBar: TToolBar;
    ToolButton_neu: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton2: TToolButton;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton_quit: TToolButton;
    TreeView_ext: TTreeView_ext;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  form_k_liste: Tform_k_liste;

implementation

{$R *.DFM}

procedure Tform_k_liste.FormCreate(Sender: TObject);
begin
  treeview_ext.liste_lesen;
end;

procedure Tform_k_liste.FormDestroy(Sender: TObject);
begin
     treeview_ext.liste_loeschen ;
end;

end.
