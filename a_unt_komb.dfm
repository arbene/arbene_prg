�
 TFORM_UNT_KOMB 0�  TPF0TForm_Unt_kombForm_Unt_kombLeft� Top� Width�Height�BorderIcons CaptionUntersuchungs-KombinationenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanel
Panel_mainLeft TopWidth� HeightgAlignalLeft
BevelOuterbvNoneCaption
Panel_mainTabOrder  	TGroupBoxGroupBox_mainLeft TopiWidth� Height� AlignalClientCaptionUntersuchungs-KombinationTabOrder  
TDBgridEXTDBgridEXT_mainLeftTopWidth� Height� AlignalClient
DataSourceDataModul.Ds_unt_komb_mainDefaultDrawing	OptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnEnterDBgridEXT_mainEnterOnExitDBgridEXT_mainExitZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameNameWidth� Visible	     	TNotebookNotebookLeft Top Width� HeightiAlignalTopTabOrder TPage Left Top Caption
Einrichten TRadioGroup
RadioGroupLeft Top Width� HeightaAlignalTop	ItemIndex Items.StringsUntersuchungImpfungLabor TabOrder OnClickRadioGroupClick   TPage Left Top Captionneu 	TGroupBoxGroupBox_planenLeft Top Width� HeightaAlignalTopTabOrder  TSpeedButtonSpeedButton1Left� TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  TLabelDatumLeftTopWidthHeightCaptionDatum  TLabeluntersucherLeftTop?Width:HeightCaptionUntersucher  	TMaskEditMaskEditLeftHTopWidthAHeightEditMask!90/90/0000;1;_	MaxLength
TabOrder Text
  .  .      	TComboBoxComboBox_untersucherLeftHTop=Width� Height
ItemHeight TabOrderTextComboBox_untersucher      TToolBarToolBarLeft Top Width�HeightCaptionToolBarImagesForm_main.ImageList_mainTabOrder TToolButtonToolButton_neuLeft TopCaptionToolButton_neu
ImageIndex OnClickToolButton_neuClick  TToolButtonToolButton2LeftTopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeftTopCaptionToolButton_edit
ImageIndexOnClickToolButton_editClick  TToolButtonToolButton4Left6TopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeft>TopCaptionToolButton_save
ImageIndexOnClickToolButton_saveClick  TToolButtonToolButton9LeftUTopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_delLeft]TopCaptionToolButton_del
ImageIndexOnClickToolButton_delClick  TToolButtonToolButton6LefttTopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_quitLeft� TopCaptionToolButton_quit
ImageIndexOnClickToolButton_quitClick   TPanelPanel_okLeft Top�Width�Height+AlignalBottom
BevelOuterbvNoneTabOrder TBitBtnBitBtn1LeftXTopWidthiHeightTabOrder KindbkOK  TBitBtnBitBtn2Left TopWidthyHeightTabOrderKindbkCancel   	TGroupBoxGroupBox_childLeft� TopWidth� HeightgAlignalClientCaptionUntersuchungen TabOrder 
TDBgridEXTDBgridEXT_childLeftTopWidth� HeightVAlignalClient
DataSourceDataModul.Ds_unt_komb_childDefaultDrawing	Options	dgEditingdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnEnterDBgridEXT_childEnterOnExitDBgridEXT_childExitZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameTypWidth� Visible	      