unit a_unterschungs_info;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls, Grids, Aligrid, DBGrids, dbgridEXT,a_report,shellapi;

type
  TForm_u_info = class(TForm)
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    Panel1: TPanel;
    DBgridEXT_untersuchung: TDBgridEXT;
    DBgridEXT_labor: TDBgridEXT;
    BitBtn1_ausgabe: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1_ausgabeClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_u_info: TForm_u_info;

implementation

uses a_main, a_data;

{$R *.DFM}

procedure TForm_u_info.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));

end;





procedure TForm_u_info.BitBtn1_ausgabeClick(Sender: TObject);
var
f_name, html_name,query:string;
r:thtml_report;
stunden_r, minuten_r,t:real;
m,s1,s2,s3,s4,s5,s6,s7:string;
begin


with datamodul do
try

  if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.13', [ pfad_temp]));
	f_name:=pfad_temp+'infotext.html';
	//speichern;
    m:= q_mitarbeiter.findfield('name').asstring + ', '+q_mitarbeiter.findfield('vorname').asstring +  ', '+ q_mitarbeiter.findfield('geb_dat').asstring;


	r:=thtml_report.create(f_name);
    r.stag('font size= medium');
    r.zeile(format('  Laufzettel f�r  %s ',[m]));
    r.absatz;
    q_3.First;
    m:='Untersuchungen: ';
    while not q_3.eof do
    begin
    m:=m+q_3.findfield('untersuchung').asstring+', ';
    q_3.Next;
    end;
    r.zeile(m);
    r.stag('font size= small');
    r.absatz;
    r.absatz;

    r.t_kopfzeile('border width="90%" frame=box align=center','',['Untersuchungsschritt','Erstuntersuchung','Nachuntersuchung','notwendig','Erledigt'],['60%','10%','10%','10%','10%']);
    q_1.first ;
    while not q_1.eof do
    begin
      s1:=q_1.findfield('Untersuchungsschritt').asstring;
      s2:=q_1.findfield('eu').asstring;
      if s2='1' then s2:='X' else s2:='';
      s3:=q_1.findfield('nu').asstring;
      if s3='1' then s3:='X' else s3:='';
      s4:= q_1.findfield('prioritaet').asstring;
      if s4='1' then s4:='X' else s4:='';

      r.t_zeile('',[s1,s2,s3,s4,'']);
      q_1.Next;
    end;


    r.stag('/table');


    r.absatz;

    r.absatz;
    r.absatz;

    r.t_kopfzeile('border width="90%" frame=box align=center','',['Laborparameter','Erstuntersuchung','Nachuntersuchung','Notwendig','Erledigt'],['60%','10%','10%','10%','10%']);
    q_2.first ;
    while not q_2.eof do
    begin
      s1:=q_2.findfield('Laborparameter').asstring;
      s2:=q_2.findfield('eu').asstring;
      if s2='1' then s2:='X' else s2:='';
      s3:=q_2.findfield('nu').asstring;
      if s3='1' then s3:='X' else s3:='';
      s4:= q_2.findfield('prioritaet').asstring;
      if s4='1' then s4:='X' else s4:='';

      r.t_zeile('',[s1,s2,s3,s4,'']);
      q_2.Next;
    end;


    r.stag('/table');





finally
	r.free;
end;
	html_name:='file:\\'+f_name;
	shellexecute_se(0,'open',pchar(f_name),'','',sw_shownormal);
  
end;

end.
