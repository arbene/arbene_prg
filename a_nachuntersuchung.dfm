�
 TFORM_UNTERSUCHUNGSTYP 0P1  TPF0TForm_untersuchungstypForm_untersuchungstypLeft�Top� BorderStylebsDialogCaptionVorsorgeClientHeight7ClientWidth1Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TPageControlPageControlLeft TopIWidth1Height�
ActivePageTabSheet_nachuntAlignalClientHotTrack	TabOrder OnChangePageControlChange
OnChangingPageControlChanging 	TTabSheetTabSheet_untCaptionVorsorge
ImageIndex TPanelPanel8Left�Top Width�Height�AlignalRight
BevelOuterbvNoneTabOrder  	TNotebookNotebookLeft TopWidth�Height� AlignalTop	PageIndexTabOrder  TPage Left Top CaptionUntersuchung TPanelPanel9Left Top Width�Height� AlignalClient
BevelOuterbvNoneTabOrder  TLabelLabel7LeftTop� WidthcHeightCaptionUntersuchungsdauer  TLabelLabel13Left`Top� WidthHeightCaptionhh:mm  TLabelLabel246LeftTopwWidthHeightCaptionArt  TLabelLabel1LeftTopMWidthiHeightCaptiongesetzliche Grundlage  TLabelLabel3LeftTopWidthHeightCaptionAnlass  TDBCombo_numDBCombo_num_unt_artLeftTop� WidthrHeightalle_einfuegenkeines_einfuegen	DataFieldi_paw
DataSourceDataModul.ds_2zconnectionDataModul.connection_main	ztab_nameuntersuchungsart2zfield_textnamezfield_indexnummerzordernummerzstorno
ItemHeightTabOrder  TDBCombo_numDBCombo_num_grundlageLeftTop^WidthrHeightalle_einfuegenkeines_einfuegen	DataField
i_arbmedvv
DataSourceDataModul.ds_2zconnectionDataModul.connection_main	ztab_namegesetzliche_grundlagezfield_textnamezfield_index
i_arbmedvvzordernummerzstorno
ItemHeightItems.StringsArbmedVVEignung TabOrder  TDBMemoDBMemo_anlassLeftTopWidthqHeight1	DataFielduntersuchung
DataSourceDataModul.ds_2
ScrollBars
ssVerticalTabOrder   TDBEditDBEdit_unt_dauerLeftTop� WidthIHeight	DataFieldDauer
DataSourceDataModul.ds_2TabOrderOnKeyUpDBEdit_unt_dauerKeyUp    TPage Left Top CaptionImpfung TLabelLabel8LeftTopWidth&HeightCaptionImpfung  TLabelLabel9LeftToppWidthHeightCaptionDauer  TLabelLabel17LeftlTop� WidthHeightCaptionhh:mm  TDBEditDBEdit2LeftTop� WidthXHeight	DataFieldDauer
DataSourceDataModul.ds_2TabOrder OnExitDBEdit_unt_dauerExitOnKeyUpDBEdit_unt_dauerKeyUp  TDBMemoDBMemo_impfungLeftTopWidthtHeight1	DataFielduntersuchung
DataSourceDataModul.ds_2TabOrder    	TGroupBoxGroupBox_infoLeft TopsWidth�Height[AlignalTopCaptionInfoTabOrder TDBMemoDBMemoLeftTopWidth�HeightJAlignalClient	DataFieldmemo
DataSourceDataModul.ds_2
ScrollBars
ssVerticalTabOrder    	TGroupBox	GroupBox5Left Top� Width�Height� AlignalTopCaption
AbrechnungTabOrder TLabelLabel15LeftoTopYWidthHeightCaption   �   TLabelLabel10Left� Top!Width� HeightCaption*   Wenn nach GOÄ abgerechnet wird und keine   TLabelLabel11Left� Top9Width� HeightCaption/   Ziffer eingegeben ist, werden die zugehörigen   TLabelLabel12Left� TopNWidthjHeightCaptionBefunde abgerechnet.  TDBEditDBEdit_goaeLeft+Top(Width=Height	DataFieldgoae
DataSourceDataModul.ds_2TabOrder   TDBEditDBEdit_euroLeft+TopUWidth=Height	DataFieldeuro
DataSourceDataModul.ds_2TabOrder  TRadioButtonRadioButton1LeftTopWidthqHeightCaption   GOÄChecked	TabOrderTabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2LeftTopBWidthqHeightCaptionPreisTabOrderOnClickRadioButton1Click  TDBCheckBoxDBCheckBox_ustLeftTopwWidthcHeightCaptionUSt berechnen	DataFieldust
DataSourceDataModul.ds_2TabOrderValueChecked1ValueUnchecked0   TPanelPanel7Left Top Width�HeightAlignalTop
BevelOuterbvNoneTabOrder TDBCheckBoxDBCheckBox8LeftTopWidthqHeightCaptionJ   Nur Überschrift (kann nicht als Vorsorge od. Impfung  ausgewählt werden)	DataFieldueberschrift
DataSourceDataModul.ds_2TabOrder ValueChecked1ValueUnchecked0    TPanel
Panel_leftLeft Top Width�Height�AlignalClient
BevelOuterbvNoneDragModedmAutomaticTabOrder TTreeView_extTreeViewLeft Top Width�Height�AlignalClientDragModedmAutomaticHideSelectionHotTrack	IndentTabOrder OnChangeTreeViewChange
OnChangingTreeViewChangingbereich_wert bereich_wert2     	TTabSheetTabSheet_zug_befundeCaption   zugehörige Befunde
ImageIndex 	TGroupBoxGroupBox_befundeLeft Top Width�Height�AlignalClientCaptionBefundeTabOrder  TListBoxListBox_befundeLeftTopWidth�Height�AlignalClient
ItemHeightTabOrder OnClickListBox_befundeClick   TPanelPanel1Left�Top Width/Height�AlignalRight
BevelOuterbvNoneCaptionPanel1TabOrder 	TGroupBox	GroupBox1Left TopYWidth/HeightiAlignalTopCaption   Gültig fürTabOrder  TDBCheckBoxDBCheckBox1LeftTopWidth� HeightCaptionErstuntersuchung	DataFieldeu
DataSourceDataModul.ds_3TabOrder ValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox2LeftTop4Width� HeightCaptionNachuntersuchung	DataFieldnu
DataSourceDataModul.ds_3TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox3LeftTopPWidth� HeightCaptionNachgehende Untersuchung	DataFieldngu
DataSourceDataModul.ds_3TabOrderValueChecked1ValueUnchecked0   TPanelPanel4Left Top Width/HeightYAlignalTop
BevelOuterbvNoneTabOrder TDBCheckBoxDBCheckBox7LeftTop+WidthHeightCaption	Abrechnen	DataField	abrechnen
DataSourceDataModul.ds_3TabOrder ValueChecked1ValueUnchecked0   TDBRadioGroupDBRadioGroup1Left Top� Width/HeightWAlignalTop	DataField
prioritaet
DataSourceDataModul.ds_3Items.Strings	Notwendig
Fakultativ TabOrderValues.Strings10   	TGroupBox	GroupBox3Left TopWidth/HeightiAlignalTopCaption	AnmerkungTabOrder TDBMemoDBMemo1LeftTopWidth+HeightXAlignalClient	DataFieldmemo
DataSourceDataModul.ds_3TabOrder      	TTabSheetTabSheet_zug_laborCaption   zugehöriges Labor
ImageIndex 	TGroupBoxGroupBox_labLeft Top Width�Height�AlignalClientCaptionLaborTabOrder  TListBoxListBox_laborLeftTopWidth�Height�AlignalClient
ItemHeightTabOrder OnClickListBox_laborClick   TPanelPanel5Left�Top Width/Height�AlignalRight
BevelOuterbvNoneCaptionPanel1TabOrder 	TGroupBox	GroupBox2Left TopqWidth/HeightiAlignalTopCaption   Gültig fürTabOrder  TDBCheckBoxDBCheckBox4LeftTopWidth� HeightCaptionErstuntersuchung	DataFieldeu
DataSourceDataModul.ds_5TabOrder ValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox5LeftTop4Width� HeightCaptionNachuntersuchung	DataFieldnu
DataSourceDataModul.ds_5TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox6LeftTopPWidth� HeightCaptionNachgehende Untersuchung	DataFieldngu
DataSourceDataModul.ds_5TabOrderValueChecked1ValueUnchecked0   TDBRadioGroupDBRadioGroup2Left Top� Width/HeightWAlignalTop	DataField
prioritaet
DataSourceDataModul.ds_5Items.Strings	Notwendig
Fakultativ TabOrderValues.Strings10   	TGroupBox	GroupBox4Left Top1Width/HeightiAlignalTopCaption	AnmerkungTabOrder TDBMemoDBMemo2LeftTopWidth+HeightXAlignalClient	DataFieldmemo
DataSourceDataModul.ds_5TabOrder    TPanelPanel6Left Top Width/HeightqAlignalTop
BevelOuterbvNoneTabOrder TDBCheckBoxDBCheckBox_abrLeftTop@WidthHeightCaption	Abrechnen	DataField	abrechnen
DataSourceDataModul.ds_5TabOrder ValueChecked1ValueUnchecked0     	TTabSheetTabSheet_impfstoffCaption	Impfstoff
ImageIndex TLabelLabel5Left1ToptWidth(HeightCaption	Impfstoff  TLabelLabel20Left7Top� Width!HeightCaptionKosten  TLabelLabel21Left�Top� WidthHeightCaptionEuro  
TDBgridEXT
DBgridEXT1Left Top Width!Height�AlignalLeft
DataSourceDataModul.ds_impfstoffDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldName	impfstoffTitle.Caption	ImpfstoffWidth<Visible	    TDBEditDBEdit_impfstoffLefthToppWidth!Height	DataField	Impfstoff
DataSourceDataModul.ds_impfstoffTabOrder  TDBEditDBEdit6LefthTop� WidthIHeight	DataFieldeuro
DataSourceDataModul.ds_impfstoffTabOrder  TDBRadioGroupDBRadioGroup_ust_impfstoffLeft?Top� Width� HeightUCaptionUmsatzsteuer 	DataFieldi_user
DataSourceDataModul.ds_impfstoffItems.StringsVorgabekeine UST ausweisenimmer UST ausweisen TabOrderValues.Strings012 OnChange DBRadioGroup_ust_impfstoffChange   	TTabSheetTabSheet_nachuntCaptionNachuntersuchung TPanelPanel2LeftzTop Width�Height�AlignalRightTabOrder  TLabelLabel_wuLeft TopaWidth� HeightCaption"Frist alle weiteren Untersuchungen  TLabelLabel_n_pawLeft Top� WidthTHeightCaptionUntersuchungsart  TLabelLabel_euLeft Top/WidthiHeightCaptionFrist Erstuntersuchung  TDBCombo_numDBCombo_num_fristenLeft!ToprWidth� Heightalle_einfuegenkeines_einfuegen	DataFieldfrist
DataSourceDataModul.ds_nachuntersuchungzconnectionDataModul.connection_main	ztab_namefristenzfield_textfristzfield_indexi_fristzorderi_fristzstornoDropDownCount
ItemHeightTabOrder  TDBCombo_numDBCombo_num_nach_pawLeftTop� Width� Heightalle_einfuegenkeines_einfuegen	DataFieldi_paw
DataSourceDataModul.ds_nachuntersuchungzconnectionDataModul.connection_main	ztab_nameuntersuchungsart2zfield_textnamezfield_indexnummerzordernummerzstorno
ItemHeightTabOrder  TDBCombo_numDBCombo_num_frist_erstLeft!TopBWidth� Heightalle_einfuegenkeines_einfuegen	DataField
vorgezogen
DataSourceDataModul.ds_nachuntersuchungzconnectionDataModul.connection_main	ztab_namefristenzfield_textfristzfield_indexi_fristzorderi_fristzstornoDropDownCount
ItemHeightTabOrder    	TGroupBox	GroupBox6Left Top WidthzHeight�AlignalClientCaptionNachuntersuchungTabOrder TListBoxListBox_nachuntersuchungLeftTopWidthvHeight�AlignalClient
ItemHeightTabOrder OnClickListBox_nachuntersuchungClick     TPanelPanel3Left Top(Width1Height!AlignalTop
BevelOuterbvNoneTabOrder TDBMemoDBMemo3TagLeftTopWidth�HeightColor	clBtnFace	DataFielduntersuchung
DataSourceDataModul.ds_2ReadOnly	TabOrder    TToolBarToolBar1Left Top Width1Height(AutoSize	ButtonHeight$ButtonWidth5CaptionToolBar1ImagesForm_main.ImageList_mainShowCaptions	TabOrder TToolButtonToolButton_newLeft TopCaptionNeu
ImageIndex OnClickToolButton_newClick  TToolButtonToolButton2Left5TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeft=TopCaption   Ändern
ImageIndexOnClickToolButton_editClick  TToolButtonToolButton4LeftrTopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeftzTopCaptionSichern
ImageIndexOnClickToolButton_saveClick  TToolButtonToolButton9Left� TopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeft� TopCaption   Löschen
ImageIndexOnClickToolButton_deleteClick  TToolButtonToolButton10Left� TopWidthCaptionToolButton10
ImageIndexStyletbsSeparator  TToolButtonToolButton_cancelLeft� TopCaptionAbbruch
ImageIndexVisibleOnClickToolButton_cancelClick  TToolButtonToolButton6Left)TopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_exitLeft1TopCaption	Verlassen
ImageIndexOnClickToolButton_exitClick    