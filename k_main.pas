unit k_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, zQuery, ZMySqlQuery, ZTransact, ZMySqlTr, ZConnect, ZMySqlCon,
  StdCtrls, Buttons,registry,math;

type
  TForm_main = class(TForm)
    ds_1: TDataSource;
    OpenDialog: TOpenDialog;
    Edit_file_1: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
	  Edit_file_2: TEdit;
    Label2: TLabel;
    BitBtn_konvert: TBitBtn;
    Label_fortschritt: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn_konvertClick(Sender: TObject);
    procedure ini;
  private
    { Private-Deklarationen }
   db_host,db_login,db_port,db_datenbank,db_passwort, pfad_temp:string;
   sl_schicht, sl_schicht_kuerzel:tstringlist;
   a_fehler, a_beginn: array [0..1023] of integer;
   infile, outfile:textfile;
   tab:char;
   function db_connect() : boolean;
   procedure schicht_lesen;
   procedure konvertieren;
	 function getschicht(stunde:integer):char;
	 function str_differenz(str1,str2:string):integer;
   procedure schicht_einpassen(vorlage,ist:string;var beginn, fehler:integer);
	 procedure suche_beste_schicht(ist:string;var s_kuerzel:string;var beginn:integer);
   function getfirma(f:string):string;
   public
    { Public-Deklarationen }
  end;

var
	Form_main: TForm_main;

implementation

{$R *.DFM}

procedure tform_main.ini;
var
	f:textfile;
	dat,s:string;
begin

	dat:=extractfiledir(application.exename)+'\arbene.ini';
	if fileexists(dat) then
	try
	  AssignFile(F,dat );   {globales }
	  Reset(F);
	  Readln(F, S);
	  db_host:=s;
	finally
	  CloseFile(F);
	end;
	db_passwort:='nix'+'reinkommen';
  db_datenbank:='Arbene';
  db_login:='Arbene';
  db_port:='3306';
  db_connect();
end;

procedure TForm_main.FormCreate(Sender: TObject);
var
	reg: Tregistry;
begin
try
	reg := Tregistry.create;
	reg.RootKey:=hkey_current_user;
	reg.openkey('software\Klawitter\arbene\pfade',false);
	pfad_temp:=reg.readstring('pfad_temp');
 	reg.CloseKey;
finally
  if pfad_temp='' then pfad_temp:='c:\';
	reg.CloseKey;
	reg.free;
  tab:=chr(9);
end;
ini;
sl_schicht:=tstringlist.Create;
sl_schicht_kuerzel:=tstringlist.Create;
schicht_lesen;

end;

function tform_main.db_connect() : boolean;
begin
try
  result:=true;
	db_main.database:=db_datenbank;
	db_main.Host:=db_host;
	db_main.Login:=db_login;
	db_main.Port:=db_port;
  db_main.Password:=db_passwort;

  db_main.Connect;
  tr_main.Connect;

except
	result:=false;
end;
end;

procedure TForm_main.FormClose(Sender: TObject; var Action: TCloseAction);
begin
sl_schicht.free;
sl_schicht_kuerzel.free;
db_main.Disconnect;
tr_main.disConnect;

end;

procedure tform_main.schicht_lesen;
var query,schicht:string;
i:integer;
begin
query:='select * from schicht';
q_1.Close;
q_1.Sql.text:=query;
q_1.Open;
q_1.first;
while not q_1.eof do
begin
  if q_1['schicht_kuerzel']<>null then sl_schicht_kuerzel.add( q_1['schicht_kuerzel']);
  if q_1['schicht']<>null then
  begin
     schicht:= '';
     for i:=0 to 40 div length( q_1['schicht']) do schicht:=schicht+ q_1['schicht'];
     schicht:=copy(schicht,length(schicht)-31,32)+schicht;
  	sl_schicht.add(schicht);
  end;
	q_1.Next;
end;


end;

procedure TForm_main.BitBtn1Click(Sender: TObject);
begin
  if opendialog.execute then
  begin
  	edit_file_1.text:=form_main.opendialog.FileName;
     edit_file_2.text:=edit_file_1.text+'_K';


  end;
end;

procedure TForm_main.BitBtn_konvertClick(Sender: TObject);
begin
konvertieren;
end;

procedure TForm_main.konvertieren;
var
zeile_in,zeile_out,firma, pnummer,firma_nummer,schicht,schicht_k,s_kuerzel,beginn_dat,monat,jahr:string;
beginn,tag,zaehler:integer;
stunden,minuten,i:integer;
smonat:array[1..31] of char;
begin
try
   if FileExists(edit_file_2.text) then deletefile(edit_file_2.text);
   assignfile(infile,edit_file_1.text);
   reset(infile);
   assignfile(outfile,edit_file_2.text);
   rewrite(outfile);
   zeile_out:='firma,p_nummer,schicht,schicht_S_dat';
   writeln(outfile,zeile_out);
   zaehler:=0;

   readln(infile,zeile_in); //erste BAZT
   while (pos('BAZT',zeile_in)<1) and (not eof(infile) )do readln(infile,zeile_in);
   while not eof(infile) do
   begin
      firma_nummer:=copy(zeile_in,1,7);
      firma:=copy(zeile_in,1,2);
      pnummer:=copy(zeile_in,3,5);
      schicht_k:=copy(zeile_in,25,3);
      monat:=copy(zeile_in,10,2);
      jahr:=copy(zeile_in,12,2);
      for i:=1 to 31 do smonat[i]:='-';

      //readln(infile,zeile_in); //zweite BAZT nach Gruppenwechsel
      //while (pos('BAZT',zeile_in)<1) and (not eof(infile) )do readln(infile,zeile_in);

      while copy(zeile_in,1,7)=firma_nummer do //solange selber ma
      begin
          tag:=strtoint(copy(zeile_in,8,2));
          stunden:=strtoint(copy(zeile_in,22,2));
          minuten:=strtoint(copy(zeile_in,24,2));
          smonat[tag]:=getschicht(stunden);
         readln(infile,zeile_in); //n�chste zeile BAZT
         while (pos('BAZT',zeile_in)<1) and (not eof(infile) )do readln(infile,zeile_in);
      end;

     suche_beste_schicht(string(smonat),s_kuerzel,beginn);

     //zeile_out:=firma_nummer+' '+s_kuerzel+' '+inttostr(beginn)+' '+ string(smonat) ;
     firma:=getfirma(firma);
     beginn_dat:=format('0%d.%s.20%s',[beginn,monat,jahr]);
     beginn_dat:=copy(beginn_dat,length(beginn_dat)-9,10);
     zeile_out:=format('%s,%s,%s,%s ',[firma,pnummer,s_kuerzel,beginn_dat]);
     writeln(outfile,zeile_out);

     label_fortschritt.Caption:= inttostr(zaehler);
     inc(zaehler);
     application.ProcessMessages;
   end;
finally
 closefile(infile);
 closefile(outfile);
end;
end;



function TForm_main.getschicht(stunde:integer):char;
begin
   case stunde of
      0..4:  result:='N';
      5..11:  result:='F';
      12..19: result:='S';
      20..24:result:='N';
   else
	   result:='-';
   end;
end;


function tform_main.str_differenz(str1,str2:string):integer;
var
i,lmin:integer;
begin
   result:=0;
   lmin:=min(length(str1),length(str2));
   for i:=1 to lmin do if str1[i]<>str2[i]then inc(result);
end;

procedure tform_main.schicht_einpassen(vorlage,ist:string;var beginn, fehler:integer);
var i,l:integer;
	  a_fehler: array[1..31] of integer;
begin
l:=length(ist);
for i:= 1 to l do
begin
	a_fehler[i]:=str_differenz(copy(vorlage,34-i,l),ist);
end;
beginn:=1;
fehler:=a_fehler[1];
for i:=2 to l do
    if a_fehler[i]<fehler then
    begin
    	beginn:=i;
     fehler:=a_fehler[i];
    end;
end;


procedure tform_main.suche_beste_schicht(ist:string;var s_kuerzel:string;var beginn:integer);
var i, fehler, min_fehler, min_fehler_beginn,s_index:integer;
begin
min_fehler:=60;
for i:=0 to sl_schicht.Count-1 do
begin
   schicht_einpassen(sl_schicht[i],ist,beginn,fehler);
   a_fehler[i]:=fehler;
   a_beginn[i]:=beginn;
   if fehler<min_fehler then
   	begin
     	min_fehler:=fehler;
        min_fehler_beginn:=beginn;
        s_index:=i;
     end;
end;

s_kuerzel:=sl_schicht_kuerzel[s_index];
beginn:=min_fehler_beginn;

end;


function tform_main.getfirma(f:string):string;
begin
   f:=uppercase(f);
   if f='L8' then result:='00A';
   if f='WS' then result:='00S';
   if f='W5' then result:='00K';
end;

end.
