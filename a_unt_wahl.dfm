�
 TFORM_UNTWAHL 0p  TPF0TForm_untwahlForm_untwahlLeftKTop� WidthzHeight�CaptionLaborColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel_botttomLeft Top�WidthjHeight'AlignalBottomTabOrder  TPanelPanel1LeftTopWidthhHeight%AlignalClient
BevelOuterbvNoneTabOrder  TBitBtn	BitBtn_okLeft'TopWidth� HeightCaptionOKParentShowHintShowHint	TabOrder OnClickBitBtn_okClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtn
BitBtn_abbLefttTopWidth� HeightCaption	AbbrechenModalResultParentShowHintShowHint	TabOrderOnClickBitBtn_abbClick
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs    TPanel
Panel_mainLeft Top(WidthjHeightgAlignalClientCaption
Panel_mainTabOrder 	TNotebookNotebookTagLeft8TopWidth1HeighteAlignalRight	PageIndexTabOrder  TPage Left Top CaptionUntersuchung TLabelLabel4LeftTop"WidthxHeightCaptionVorsorge / Untersuchung  TLabelLabel7LeftToprWidthcHeightCaptionUntersuchungsdauer  TLabelLabel13Left� Top� WidthHeightCaptionhh:mm  TDBEditDBEdit_unt_dauerLeftTop� Width`Height	DataFieldDauer
DataSourceDataModul.Ds_typ_treeTabOrder   TDBMemoDBMemo_untersuchungLeftTop4Width�Height.	DataFieldUntersuchung
DataSourceDataModul.Ds_typ_treeTabOrder   TPage Left Top CaptionImpfung TLabelLabel8LeftTopWidth&HeightCaptionImpfung  TLabelLabel9LeftTopeWidthHeightCaptionDauer  TLabelLabel33Left� TopxWidthHeightCaptionhh:mm  TDBEditDBEdit2LeftTopvWidth`Height	DataFieldDauer
DataSourceDataModul.Ds_typ_treeTabOrder   TDBMemoDBMemo_impfungLeftTop.Width�Height'	DataFieldUntersuchung
DataSourceDataModul.Ds_typ_treeTabOrder   TPage Left Top CaptionLabor TLabelLabel10LeftTop+WidthQHeightCaptionLabor-Parameter   TLabelLabel11LeftTop� WidthcHeightCaptionUntersuchungsdauer  TLabelLabel_einheitLeftTopoWidth HeightCaptionEinheit  TLabelLabel2LeftTop� WidthKHeightCaptionNormwert Mann  TLabelLabel16LeftTopoWidthDHeightCaption   Kürzel für LDT  TLabelLabel17LeftTop� WidthEHeightCaptionNormwert Frau  TDBEditDBEdit_labeinheitLeftTop� WidthiHeight	DataFieldEinheit
DataSourceDataModul.Ds_typ_treeTabOrder  TDBEditDBEdit4LeftTop	WidthaHeight	DataFieldDauer
DataSourceDataModul.Ds_typ_treeTabOrder  TDBCheckBoxDBCheckBox_groupLeftTop
Width� HeightCaption   Gruppen-Überschrift	DataFieldb_group
DataSourceDataModul.Ds_typ_treeTabOrderValueChecked1ValueUnchecked0  TDBEditDBEdit_kuerzelLeftTop� WidthyHeight	DataFieldkuerzel
DataSourceDataModul.Ds_typ_treeTabOrder   TDBMemo	DBMemo_nmLeftTop� Width� Height9	DataFieldnorm_m
DataSourceDataModul.Ds_typ_tree
ScrollBars
ssVerticalTabOrder  TDBMemo	DBMemo_nfLeft
Top� Width� Height9	DataFieldnorm_w
DataSourceDataModul.Ds_typ_tree
ScrollBars
ssVerticalTabOrder  	TGroupBox	GroupBox2Left Top�Width1Height� AlignalBottomCaption
AbrechnungTabOrder TLabelLabel27LeftsTop`WidthHeightCaption   �   TLabelLabel21Left� TopWidth� HeightCaption,   Nach dem Ändern von GOÄ oder Preis müssen  TLabelLabel22Left� Top.Width� HeightCaption*bestehende Labor-Befunde angepasst werden!  TDBEditDBEdit_euro_labLeft-Top[WidthAHeight	DataFieldeuro
DataSourceDataModul.Ds_typ_treeTabOrder   TDBEditDBEdit_goae_labLeft,Top&WidthAHeight	DataFieldgoae
DataSourceDataModul.Ds_typ_treeTabOrder  TRadioButtonRadioButtonl1LeftTopWidthWHeightCaption   GOÄChecked	TabOrderTabStop	OnClickRadioButtonl1Click  TRadioButtonRadioButtonl2LeftTopJWidth\HeightCaptionPreisTabOrderOnClickRadioButtonl1Click  TButtonbutton_laborLeft� TopVWidth� HeightCaption$   GOÄ bei bestehendem Labor eintragenTabOrderOnClickbutton_laborClick  TDBCheckBoxDBCheckBox1Left Top� WidthaHeightCaptionUSt berechnen	DataFieldust
DataSourceDataModul.Ds_typ_treeTabOrderValueChecked1ValueUnchecked0   TDBMemoDBMemo_laborLeftTop<Width�Height-	DataFielduntersuchung
DataSourceDataModul.Ds_typ_treeTabOrder  TBitBtnBitBtn_laborhinweiseTagLeftTop-Width� HeightCaption   Laborhinweise auswählenTabOrderOnClickBitBtn_laborhinweiseClick  	TGroupBoxGroupBox_labor_hinweisLeftTopHWidth�Height� Caption   ausgewählte LaborhinweiseTabOrder	 TDBGridDBGrid_labor_hinweiseLeftTopWidth�HeightpAlignalClient
DataSourceDataModul.ds_hinweisOptions	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNamenameReadOnly	Title.CaptionHinweisWidth� Visible	 Expanded	FieldNametbs_sTitle.CaptionTextWidthiVisible	 Expanded	FieldNamei1Title.Captionmin_mWidth$Visible	 Expanded	FieldNamei2Title.Captionmax_mWidth&Visible	 Expanded	FieldNamei1wTitle.Captionmin_wWidth%Visible	 Expanded	FieldNamei2wTitle.Captionmax_wWidth'Visible	 Expanded	FieldNametbs_s1Title.CaptionGrenzeWidth&Visible	      TPage Left Top CaptionDatum-untersucher TSpeedButtonSpeedButton3TagLeft[Top8WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton3Click  TLabelLabel1LeftTop'WidthHeightCaptionDatum  TLabelLabel45LeftTop_Width:HeightCaptionUntersucher  TLabelLabel32LeftTop� WidthHeightCaptionInfo  	TComboBoxComboBox_untersucherTagLeftToppWidth� HeightStylecsDropDownList
ItemHeightTabOrder   	TMaskEditMaskEdit_datumTagLeftTop8Width=HeightEditMask!90.90.0000;1;_	MaxLength
TabOrderText
  .  .      TDBMemoDBMemo_memo_vorsorgeLeftTop� WidthiHeight� 	DataFieldmemo
DataSourceDataModul.Ds_typ_treeReadOnly	
ScrollBars
ssVerticalTabOrder   TPage Left Top Captionuntersuchungsbefunde TPageControlPageControlLeft Top Width1Heighte
ActivePage
TabSheet_1AlignalClientTabOrder OnChangePageControlChange
OnChangingPageControlChanging 	TTabSheet
TabSheet_1CaptionErscheinungsbild TLabelLabel3LeftTopWidthAHeightCaptionBefund-Name  TDBEditDBEdit_nameLeftTop WidthHeight	DataFieldname
DataSourceDataModul.ds_bereichTabOrder OnChangeDBEdit_nameChange  TDBRadioGroupDBRadioGroup_erscheinungsbildLeft Top� Width)Height� AlignalBottomCaptionErscheinungsbild	DataFieldmodus
DataSourceDataModul.ds_bereichItems.StringsAnmerkungenZahlenwert und AnmerkungenAuswahlliste und Anmerkungen
AudiogrammSehtestBlutdruck / Puls   Grösse / Gewicht / BauchumfangLungenfunktionWork Ability Index TabOrderValues.Strings012345678   	TGroupBox	GroupBox1Left Top�Width)Height� AlignalBottomCaption
AbrechnungTabOrder TLabelLabel26LeftgTopVWidthHeightCaption   �   TLabelLabel20Left� Top7Width� HeightCaption$bestehende Befunde angepasst werden!  TLabelLabel23Left� Top$Width� HeightCaption,   Nach dem Ändern von GOÄ oder Preis müssen  TDBEditDBEdit_euro_befLeft!TopRWidthAHeight	DataFieldeuro
DataSourceDataModul.ds_bereichTabOrder  TDBEditDBEdit_goae_befLeft Top"WidthAHeight	DataFieldgoae
DataSourceDataModul.ds_bereichTabOrder   TDBCheckBoxDBCheckBox_extLeftToppWidthpHeightCaptionexterne Leistung	DataFieldext_leistung
DataSourceDataModul.ds_bereichTabOrderValueChecked1ValueUnchecked0Visible  TRadioButtonRadioButton1LeftTopWidthqHeightCaption   GOÄChecked	TabOrderTabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2LeftTopBWidthqHeightCaptionPreisTabOrderOnClickRadioButton1Click  TButtonButton_goaeLeft� TopXWidth� HeightCaption'   GOÄ bei bestehenden Befunden eintragenTabOrderOnClickButton_goaeClick  TDBCheckBoxDBCheckBox2LeftTop� WidthpHeightCaptionUSt berechnen	DataFieldust
DataSourceDataModul.ds_bereichTabOrderValueChecked1ValueUnchecked0Visible    	TTabSheet
TabSheet_2CaptionDetail
ImageIndex TPanelPanel2Left Top Width)HeightIAlignalClient
BevelOuterbvNoneCaptionPanel2TabOrder  	TGroupBoxGroupBox_memoLeft TopWidth)Height0AlignalClientCaption   Maske für Anmerkungen TabOrder  	TRichEditRichEdit_befundeLeftTopWidth%HeightAlignalClient
ScrollBarsssBothTabOrder OnEnterRichEdit_befundeEnter   	TNotebookNotebook_befundeLeft Top Width)HeightAlignalTopCtl3D		PageIndexParentCtl3DTabOrder TPage Left Top Captionleer  TPage Left Top Captionzahl TLabelLabel5Left TopWidth5HeightCaption
Bezeichner  TLabelEinheitLeft TopOWidth HeightCaptionEinheit  TLabelLabel18Left Top� WidthqHeightCaption   Grenzwert für "Auffällig"  TDBEdit
DBEdit_bezLeft(Top/Width� Height	DataField
bezeichner
DataSourceDataModul.ds_bereichTabOrder   TDBEditDBEdit_bezdimLeft(TopgWidth� Height	DataFieldbezeichner_dim
DataSourceDataModul.ds_bereichTabOrder  TDBComboBoxDBComboBox1Left(Top� Width1Height	DataField
s_rel_auff
DataSourceDataModul.ds_bereich
ItemHeightItems.Strings<> TabOrder  TDBEditDBEdit3LefthTop� Width� Height	DataFieldr_auff
DataSourceDataModul.ds_bereichTabOrder   TPage Left Top Captionliste TSpeedButtonSpeedButton1TagLeftTTop� WidthHeight>
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphs  TSpeedButtonSpeedButton2TagLeftTTop� WidthHeightA
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphs  TPanelPanel3Left Top� Width)HeightYAlignalBottom
BevelOuterbvNoneTabOrder  TSpeedButtonSpeedButton_upLeft� Top$WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsOnClickSpeedButton_upClick  TSpeedButtonSpeedButton_downLeftTop$WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsOnClickSpeedButton_downClick  TLabelLabel19Left TopWidthsHeightCaption   Doppelklick zum Ändern  TLabelLabel24Left TopWidth[HeightCaption   Normal <-> Auffällig  TBitBtnBitBtn3LeftTop$WidthAHeightCaption   HinzufügenTabOrder OnClickBitBtn3Click  TBitBtnBitBtn_veraendernLeft\Top$WidthAHeightCaption
   VerändernTabOrderOnClickBitBtn_veraendernClick  TBitBtnBitBtn4Left� Top$WidthKHeightCaption   LöschenTabOrderOnClickBitBtn4Click  TDBCheckBoxDBCheckBox_reliLeftTopBWidth� HeightCaption   Ausprägung links und rechts	DataFieldreli
DataSourceDataModul.ds_bereichTabOrderValueChecked1ValueUnchecked0   TPanelPanel4Left Top WidthHeight� AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel5LeftTop WidthHeight� AlignalRight
BevelOuterbvNoneTabOrder  TDBGridDBGrid1LeftTop Width�Height� Hint&   Ändern Normal/Auff: Klick auf re RandAlignalClient
DataSourceDataModul.ds_berlistOptions	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit ParentShowHintShowHint	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 	OnColExitDBGrid1ColExitOnEditButtonClickDBGrid1EditButtonClickOnExitDBGrid1ExitColumnsExpanded	FieldName
listenfeldReadOnly	Title.Caption
ListenfeldWidth� Visible	 DropDownRowsExpanded	FieldNamec_auffaelligPickList.Stringsnormal
   auffällig Title.Caption   Normal/AuffälligWidthQVisible	     TPage Left Top Caption
Audiogramm Taudiogrammaudiogramm1Left Top WidthmHeightAlignalClientCaptionaudiogramm1TabOrder l1"@l2�	ql3�	ql4�	ql6�	qlsumme@Sk1�	qk2�	qk3�	qk4�	qk6�	qksumme@SSeitelinks   TPage Left Top CaptionSehtest TLabelLabel6LefthTophWidthsHeightCaption   Formular für den Sehtest   TPage Left Top CaptionBludruck TLabelLabel12LefthToppWidthdHeightCaption   Formular für Bludruck   TPage Left Top Caption   Grösse / Gewicht TLabelLabel14LefthTop`WidthSHeightCaption   Grösse / Gewicht   TPage Left Top CaptionLungenfunktion TLabelLabel15LeftTop WidthJHeightCaptionLungenfunktion   TPage Left Top CaptionWAI       TPage Left Top Caption
Leistungen 	TGroupBoxGroupBox_zeitLeft Top Width1HeighteAlignalClientCaptionVorgaben LeistungenTabOrder  	TGroupBox	GroupBox4LeftTop�Width-Height� AlignalBottomCaption   Tätigkeit / AufwandTabOrder  TDBMemoDBMemo_zeit_leistungLeftTopWidth)HeightvAlignalClient	DataFieldtbsTabOrder    	TGroupBoxGroupBox_taetigkeitenLeftTop@Width-Height� AlignalClientCaption   ausgewählte TätigkeitenTabOrder TDBGridDBGrid_taetigkeitenLeftTopWidth)HeightAlignalClient
DataSourceDataModul.ds_9OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
taetigkeitTitle.Caption	   TätgkeitWidth&Visible	     TPanelPanel8LeftTop�Width-HeightAlignalBottom
BevelOuterbvNoneTabOrder  TPanelPanel9LeftTopWidth-Height1AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel25LeftTopWidth?HeightCaptionAufgabenfeld  TLabelLabel28LeftTop� Width9HeightCaptionZeitvorgabe  TLabelLabel29LeftTop� WidthfHeightCaptionAbrechnungskriterium  TLabelLabel30LeftTop� Width-HeightCaption	Zeitkonto  TLabelLabel31Left� Top� WidthGHeightCaptionZeitansatz in %  TDBCombo_num DBCombo_num_abrechnungskriteriumLeftTop� Width� Heightalle_einfuegenkeines_einfuegen	DataFieldi_kriteriumzconnectionDataModul.connection_main	ztab_nameabr_anlass_kritzfield_textnamezfield_indexnummerzorderreihenfolgezstorno	
ItemHeightTabOrder OnEnter%DBCombo_num_abrechnungskriteriumEnterOnExit$DBCombo_num_abrechnungskriteriumExit  TDBCombo_num DBCombo_num_abrechnung_zeitkontoLeftTop� Width� Heightalle_einfuegenkeines_einfuegen		DataFieldi_zeitkontozconnectionDataModul.connection_main	ztab_nameabr_zeitkontozfield_textnamezfield_indexnummerzorderreihenfolgezstorno	
ItemHeightTabOrder  TDBEditDBEdit_zeitansatzLeft� Top� WidthJHeight	DataField
zeitansatzTabOrderOnChangeDBEdit_zeitansatzChangeOnKeyUpDBEdit_zeitansatzKeyUp  TDBMemoDBMemo_zeit_aufgabenfeldLeftTop(Width�HeightQ	DataFieldname
ScrollBars
ssVerticalTabOrder  TBitBtnBitBtn_taetigkeitenLeftTopWidthHeightCaption#   zugehörige Tätigkeiten auswählenTabOrderOnClickBitBtn_taetigkeitenClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUU����UUUP   UUUWwww�UU	��� UUUuUUUw_UP����UW�UUUu� Uw�����_      wwwwww_uPp���Ww�UU�U ��� Uw�U�wWUP�p  UW_�wuwUU �� UUwuU�wUUP�p UUW_�wuUUU ��UUUwuU�UUUP�p UUUW_�wuUUUU UUUUUUwuUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU	NumGlyphs  TDBEdit_timeDBEdit_zeitLeftTop� WidthbHeight	DataFieldzeitvorgabeTabOrder     TPage Left Top HelpContextCaptionfirmenkriterien  TPage Left Top Captionaufwand 	TGroupBox	GroupBox3Left TopQWidth1Height� AlignalTopCaption   TätigkeitsbeschreibungTabOrder  TDBMemoDBMemo_aufwandLeftTopWidth-Height� AlignalClient	DataFieldnameTabOrder    TPanelPanel7Left Top Width1HeightQAlignalTop
BevelOuterbvNoneTabOrder  TDBCheckBoxDBCheckBox_Aufwand_allLeftTopWidthyHeightCaption=Aufwandsbeschreibung wird bei allen Aufgabenfeldern angezeigt	DataFieldi_allTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox_ausblendenLeftTop0WidthqHeightCaption)   Tätigkeit nicht einfügen (Überschrift)	DataFieldi_ausblendenTabOrderValueChecked1ValueUnchecked0   TPage Left Top Captionbescheinigung  TPage Left Top Captiontextbausteine TLabelLabel34LeftTop+WidthHeightCaptionTitel  TLabelLabel35LeftTopxWidth6HeightCaptionHinweistext  TDBMemo
DBMemo_tbsLeftTop� Width�Height� 	DataFieldtbsTabOrder   TDBEditDBEdit_tbs_nameLeftTop@Width�Height	DataFieldnameTabOrder  	TGroupBoxGroupBox_tbs_auswahlLeft ToppWidth�Height� Caption)   Kriterien für die automatische ZuordnungTabOrder TLabelLabel36Left� TopNWidthHeightCaptionund  TLabelLabel37LeftTop� WidthHeightCaptionoder  TLabelLabel38LeftTop� WidthqHeightCaptionder Grenzwertinikator ist  TLabelLabel39LeftTopWidth(HeightCaptionC   Falls diese Kriterien erfüllt sind, wird der Hinweis vorgeschlagen  TLabelLabel40LeftTop4WidthYHeightCaptionDer Laborwert liegt  TLabelLabel41Left-TopMWidthjHeightCaption   bei Männern zwischen  TLabelLabel42Left� TopnWidthHeightCaptionund  TLabelLabel43Left-TopmWidthaHeightCaptionbei Frauen zwischen  	TComboBoxComboBox_tbs_giTagLeft� Top� WidthQHeight
ItemHeightTabOrderOnExitComboBox_tbs_giExitItems.Strings +-   TBitBtnBitBtn_uebernehmenTagLeftpTophWidthYHeightCaption   übernehmenTabOrder OnClickBitBtn_uebernehmenClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphs  TEditspinedit_tbs_uwTagLeft� TopHWidthIHeightTabOrderText0OnChangespinedit_tbs_uwChangeOnExitSpinEdit_tbs_uw1Exit
OnKeyPressspinedit_tbs_uwKeyPress  TEditspinedit_tbs_owTagLeft TopHWidthIHeightTabOrderText0OnChangespinedit_tbs_uwChangeOnExitSpinEdit_tbs_ow1Exit
OnKeyPressspinedit_tbs_uwKeyPress  TEditspinedit_tbs_uwwTagLeft� TophWidthIHeightTabOrderText0OnChangespinedit_tbs_uwChangeOnExitSpinEdit_tbs_uww1Exit
OnKeyPressspinedit_tbs_uwKeyPress  TEditspinedit_tbs_owwTagLeft TophWidthIHeightTabOrderText0OnChangespinedit_tbs_uwChangeOnExitSpinEdit_tbs_oww1Exit
OnKeyPressspinedit_tbs_uwKeyPress     TPanelPanel_liLeftTopWidth7HeighteAlignalClient
BevelOuterbvNoneTabOrder TPanelPanel6Left Top Width7Height AlignalTop
BevelOuterbvNoneTabOrder 
DesignSize7   TLabelLabel_suchenLeftTopWidth9Height	AlignmenttaRightJustifyCaption
Suchen:   Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontLayouttlCenter  TSpeedButtonSpeedButton4TagLeft� TopWidth(Height
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsOnClickSpeedButton4Click  TSpeedButtonSpeedButton7LeftTopWidthHeightHintBaum zuklappenAnchorsakTopakRight 
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� �������������������������������������� �������������� � ������������ ��� ������������ ��� ������������ ��� ����������� ��� ����������� ��� ������� ��� ��� �������� �� � ��������  ��� �����������������������������������������������������������������ParentShowHintShowHint	OnClickSpeedButton7Click  TSpeedButtonSpeedButton5Left<TopWidthHeightHintBaum aufklappenAnchorsakTopakRight 
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� ������������������������ �������������� � ������������ � � ���������  ��� � ������� ������ � ����� � ������ � ��  ��� ������ � � ����� ������ � � ����� ������ ��� ����� ���  ����� �����   �������� ��� ������������   ����������������������������������������ParentShowHintShowHint	OnClickSpeedButton5Click  TEdit	Edit_suchTagLeft=TopWidth� HeightTabOrder OnEnterEdit_suchEnter	OnKeyDownEdit_suchKeyDown   TTreeView_extTreeLeft Top Width7HeightEAlignalClientDragModedmAutomaticHideSelectionHotTrack	Indent	PopupMenuPopupMenu_untwahlReadOnly	StateImagesForm_main.ImageList_bereichTabOrderOnChange
TreeChange
OnChangingTreeChanging
OnDblClickTreeDblClickbereich_wert bereich_wert2 OnEdit_tabelleToolButton_editClick    TToolBarToolBarLeft Top WidthjHeight(AutoSize	ButtonHeight$ButtonWidth7CaptionToolBarImagesForm_main.ImageList_mainShowCaptions	TabOrder TToolButtonToolButton_newLeft TopHintNeu gleichgeordnetCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_newClick  TToolButtonToolButton2Left7TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeft?TopHint
   VerändernCaption   Ändern
ImageIndexParentShowHintShowHint	OnClickToolButton_editClick  TToolButtonToolButton4LeftvTopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeft~TopHint	SpeichernCaption	Speichern
ImageIndexParentShowHintShowHint	OnClickToolButton_saveClick  TToolButtonToolButton7Left� TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeft� TopHint   LöschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_deleteClick  TToolButtonToolButton6Left� TopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_closeLeftTopHint	VerlassenCaption	Verlassen
ImageIndexParentShowHintShowHint	OnClickToolButton_closeClick   
TStatusBar	StatusBarLeft Top�WidthjHeightPanelsWidth2  Visible  
TPopupMenuPopupMenu_untwahlLeftxTop 	TMenuItemm_alleitemsCaption2alle untergeordneten Items markieren / demarkierenOnClickm_alleitemsClick   
TPopupMenuPopupMenu_richeditLeftHTop 	TMenuItemdropdown_richedit_FettCaptionFett
ImageIndex  	TMenuItemdropdown_richedit_KursivCaptionKursiv
ImageIndex  	TMenuItemdropdown_richedit_UnterstrichenCaptionUnterstrichen
ImageIndex  	TMenuItemdropdown_richedit_zurcksetzenCaption   Zurücksetzen    