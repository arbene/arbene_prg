unit a_rechnungsgestaltung;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,a_main;

type
  TForm_rechnungsgestaltung = class(TForm)
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    CheckBox_anonymisieren: TCheckBox;
    GroupBox1: TGroupBox;
    CheckBox_goae: TCheckBox;
    CheckBox_impfstoff: TCheckBox;
    CheckBox_labor: TCheckBox;
    CheckBox_stornorechnung: TCheckBox;
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_rechnungsgestaltung: TForm_rechnungsgestaltung;

implementation

{$R *.dfm}

procedure TForm_rechnungsgestaltung.FormShow(Sender: TObject);
begin
  form_main.form_positionieren(tform(sender));

end;

end.
