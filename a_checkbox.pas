unit a_checkbox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, ExtCtrls;

type
  TForm_checkbox = class(TForm)
    Panel_b: TPanel;
    Panel_m: TPanel;
    CheckListBox: TCheckListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    BitBtn_esc: TBitBtn;
    BitBtn_ok: TBitBtn;
    BitBtn_decheck: TBitBtn;
    BitBtn_check: TBitBtn;
    Panel4: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_checkClick(Sender: TObject);
    procedure BitBtn_decheckClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    function is_checked(s:string):boolean;
    procedure einstellen(s:string);
  end;

var
  Form_checkbox: TForm_checkbox;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_checkbox.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

procedure TForm_checkbox.BitBtn_checkClick(Sender: TObject);
var i:integer;
begin
	for i:=0 to checklistbox.Items.Count-1 do checklistbox.checked[i]:=true;

end;

procedure TForm_checkbox.BitBtn_decheckClick(Sender: TObject);
var i:integer;
begin
	for i:=0 to checklistbox.Items.Count-1 do checklistbox.checked[i]:=false;

end;

function tform_checkbox.is_checked(s:string):boolean;
var
i:integer;
begin
  i:=checklistbox.Items.IndexOf(s);
  result:=checklistbox.Checked[i];
end;

procedure tform_checkbox.einstellen(s:string);
var
i,h:integer;
begin
  i:=checklistbox.Items.count;
  caption:=s;
  h:=i*20 +200;
  if h>600 then h:=600;
  height:=h;
end;

end.
