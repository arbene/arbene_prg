�
 TFORM_TEXTBAUSTEINE 0]  TPF0TForm_textbausteineForm_textbausteineLeft�Top� WidthHeight�HelpContext�/CaptionTextbausteineColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight 	TSplitter	Splitter1Left9Top(WidthHeight?Color	clMedGrayParentColor  TToolBarToolBar_mainLeft Top Width
Height(AutoSize	ButtonHeight$ButtonWidth5CaptionToolBarColor	clBtnFaceImagesForm_main.ImageList_mainParentColorShowCaptions	TabOrder  TToolButtonToolButton21Left TopWidthCaptionToolButton21
ImageIndexStyletbsSeparator  TToolButtonToolButton_neuLeftTopHintDatensatz neu anlegenCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonToolButton9Left9TopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeftATopCaption   Ändern
ImageIndexOnClickToolButton_editClick  TToolButtonToolButton43LeftvTopWidthCaptionToolButton43
ImageIndexStyletbsSeparator  TToolButtonToolButton_sichernLeft~TopHintDatensatz speichernCaptionSichern
ImageIndexParentShowHintShowHint	OnClickToolButton_sichernClick  TToolButtonToolButton7Left� TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeft� TopHint   Datensatz löschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_deleteClick  TToolButtonToolButton2Left� TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton1LeftTopCaption	Verlassen
ImageIndexOnClickToolButton1Click   TPanelPanel_bottomLeft TopgWidth
Height#AlignalBottomTabOrder TBitBtnBitBtn_insertLeftTopWidth� HeightCaption	   EinfügenTabOrder OnClickBitBtn_insertClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn2Left�TopWidth� HeightCaption	abbrechenTabOrderOnClickBitBtn2ClickKindbkCancel   TPanelPanel1Left Top(Width9Height?AlignalLeft
BevelOuterbvNoneCaptionPanel1TabOrder 	TGroupBoxGroupBox_kapitelLeft Top Width9Height<AlignalTopCaptionFiltern nach ThemaTabOrder  	TComboBoxComboBox_themaLeftTopWidthHeight
ItemHeightTabOrder TextalleOnChangeComboBox_themaChangeItems.Stringsalle    TTreeView_extTreeViewLeft TopqWidth9Height�AlignalClientDragModedmAutomaticFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style HideSelectionHotTrack	Indent
ParentFontReadOnly		RowSelect	StateImagesForm_main.ImageList_bereichTabOrder
OnDblClickTreeViewDblClickbereich_wert bereich_wert2 
mysqlqueryDataModul.q_6
mysql_feldnamemysqldatasourceDataModul.ds_6  	TGroupBoxGroupBox_tbs_suchLeft Top<Width9Height5AlignalTopCaptionSortieren / SuchenTabOrder TSpeedButtonSpeedButton_text_suchTagLeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsOnClickSpeedButton_text_suchClick  	TCheckBoxCheckBox_alphaLeft	TopWidthwHeightCaptionalphabetisch sortiertTabOrder OnClickComboBox_themaChange  TEditEdit_text_suchTagLeft� TopWidth� HeightTabOrder	OnKeyDownEdit_text_suchKeyDown    TPanelPanel_reLeft>Top(Width�Height?AlignalClient
BevelOuterbvNoneCaptionPanel_reTabOrder TDBMemoDBMemoLeft TopqWidth�Height�AlignalClient	DataFieldtbs
DataSourceDataModul.ds_6TabOrder WantTabs	  	TGroupBox	GroupBox1Left Top Width�Height<AlignalTopCaptionTitelTabOrder TDBEditDBEdit_titelLeft-TopWidthHHeight	DataFieldname
DataSourceDataModul.ds_6TabOrder    	TGroupBoxGroupBox_themaLeft Top<Width�Height5AlignalTopCaptionThemaTabOrderOnClickBitBtn_insertClick TDBCombo_numDBCombo_num_themaLeft-TopWidthHHeightalle_einfuegenkeines_einfuegen	DataField	i_kapitel
DataSourceDataModul.ds_6zconnectionDataModul.connection_main	ztab_namekapitelzfield_textkapitelzfield_indexnummerzorderkapitelzstornoDropDownCount
ItemHeightTabOrder      