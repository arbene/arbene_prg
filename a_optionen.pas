unit a_optionen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ToolWin, Buttons, DBCtrls, Spin, ExtCtrls, Mask,
  DBEdit_time, Grids, Aligrid;

type
  TForm_optionen = class(TForm)
    PageControl: TPageControl;
    TabSheet_db: TTabSheet;
    Edit_login: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edit_port: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Edit_host: TEdit;
    Edit_db: TEdit;
    Label6: TLabel;
    Edit_mysqladmin: TEdit;
    TabSheet_pfade: TTabSheet;
    TabSheet_internet: TTabSheet;
    Edit_email_name: TEdit;
    Edit_email_adresse: TEdit;
    Edit_email_pop: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
	  Label15: TLabel;
    Edit_email_userein: TEdit;
    Edit_email_passwortein: TEdit;
    Edit_email_smtp: TEdit;
    Edit_email_useraus: TEdit;
    BitBtn_default: TBitBtn;
    Label16: TLabel;
    Edit_temp: TEdit;
    BitBtn_temp: TBitBtn;
    Panel1: TPanel;
    Label18: TLabel;
    Label8: TLabel;
	  Edit_ldt: TEdit;
    BitBtn_ldt: TBitBtn;
    Label17: TLabel;
    TabSheet_abrechnung: TTabSheet;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    GroupBox1: TGroupBox;
    Label24: TLabel;
    Edit_vorspann: TEdit;
    SpinEdit_renu: TSpinEdit;
    Label25: TLabel;
    SpinEdit_mws: TSpinEdit;
    Edit_punktwert: TEdit;
    Label31: TLabel;
    Edit_infotexte: TEdit;
    BitBtn_infotexte: TBitBtn;
    TabSheet_abrechnung_user: TTabSheet;
    TabSheet_allgemein: TTabSheet;
    CheckBox_archiv_show: TCheckBox;
    CheckBox_jahr: TCheckBox;
    CheckBox_speichern_fragen: TCheckBox;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    Label33: TLabel;
    Label34: TLabel;
    Edit_mysql_daten: TEdit;
    BitBtn_mysql_daten: TBitBtn;
    Edit_backupverzeichnis: TEdit;
    BitBtn_backupverzeichnis: TBitBtn;
    CheckBox_bestaet_neu: TCheckBox;
    CheckBox_backup: TCheckBox;
    Label35: TLabel;
    Edit_mysql_prg: TEdit;
    BitBtn_mysql_prg: TBitBtn;
    Label36: TLabel;
    CheckBox_dbwahl: TCheckBox;
    Datenabgleich: TTabSheet;
    Panel_abgleich: TPanel;
    Label19: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Edit_abgl_mysql: TEdit;
    Edit_abgl_lokal: TEdit;
    BitBtn_lokal: TBitBtn;
    Panel2: TPanel;
    Label37: TLabel;
    Edit_abgl_iphaupt: TEdit;
    Label38: TLabel;
    Edit_abgl_dbhaupt: TEdit;
    Label39: TLabel;
    TabSheet_fehler: TTabSheet;
    Button_nkr_einlesen: TButton;
    Label41: TLabel;
    Label42: TLabel;
    Edit_vorlagen: TEdit;
    BitBtn_vorlagen: TBitBtn;
    GroupBox2: TGroupBox;
    CheckBox_voso_ab: TCheckBox;
    CheckBox_bef_ab: TCheckBox;
    CheckBox_impf_ab: TCheckBox;
    CheckBox_lab_ab: TCheckBox;
    Label30: TLabel;
    Label40: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    SpinEdit_s1: TSpinEdit;
    SpinEdit_s2: TSpinEdit;
    SpinEdit_s3: TSpinEdit;
    Label47: TLabel;
    SpinEdit_s4: TSpinEdit;
    Label48: TLabel;
    Label49: TLabel;
    Button_sperrdatei_loeschen: TButton;
    Label50: TLabel;
    CheckBox_ableich_beginn: TCheckBox;
    CheckBox_ableich_ende: TCheckBox;
    TabSheet_integrierteprogramme: TTabSheet;
    Label53: TLabel;
    Label54: TLabel;
    ComboBox_ole_text: TComboBox;
    ComboBox_ole_tab: TComboBox;
    Label51: TLabel;
    SpinEdit_s5: TSpinEdit;
    Button_soundexneu: TButton;
    TabSheet_vors_eign: TTabSheet;
    CheckBox_status: TCheckBox;
    CheckBox_arbmed_besch: TCheckBox;
    CheckBox_druck_mit_zustimmung: TCheckBox;
    CheckBox_druck_mit_zustimmung_eignung: TCheckBox;
    CheckBox_druck_mit_zustimmung_besch: TCheckBox;
    CheckBox_druck_mit_zustimmung_besch_eignung: TCheckBox;
    Label52: TLabel;
    Label55: TLabel;
    Edit_bescheinigungen: TEdit;
    BitBtn_bescheinigungen: TBitBtn;
    Edit_rechnungen: TEdit;
    BitBtn_rechnungen: TBitBtn;
    CheckBox_speicher_bescheinigungen: TCheckBox;
    CheckBox_bescheinigung_in_kartei: TCheckBox;
    CheckBox_speicher_rechnungen: TCheckBox;
    checkbox_rechnungen_in_kartei: TCheckBox;
    CheckBox_rechnung: TCheckBox;
    Label56: TLabel;
    Edit_hilfsprogramme: TEdit;
    BitBtn_hilfsprogramme: TBitBtn;
    Button1: TButton;
    Label58: TLabel;
    CheckBox_MAPI: TCheckBox;
    Label23: TLabel;
    Edit_zeitansatz: TEdit;
    GroupBox3: TGroupBox;
    Label26: TLabel;
    ComboBox_ve_datum: TComboBox;
    Label27: TLabel;
    ComboBox_ve_text: TComboBox;
    Label32: TLabel;
    ComboBox_ve_art: TComboBox;
    Label59: TLabel;
    ComboBox_ve_beurteilung: TComboBox;
    Label60: TLabel;
    MaskEdit_termine_startzeit: TMaskEdit;
    checkbox_untersuchung_planen_at_post: TCheckBox;
    Label61: TLabel;
    ComboBox_f_ve_px: TComboBox;
    Label62: TLabel;
    TabSheet_wiedervorlage: TTabSheet;
    CheckBox_wdv_besch: TCheckBox;
    CheckBox_wdv_impfu: TCheckBox;
    CheckBox_wdv_labor: TCheckBox;
    CheckBox_wdv_ambul: TCheckBox;
    CheckBox_wdv_Beson: TCheckBox;
    CheckBox_wdv_Dokfi: TCheckBox;
    CheckBox_wdv_dokpr: TCheckBox;
    CheckBox_wdv_proje: TCheckBox;
    Label63: TLabel;
    Button_repo_pruefen: TButton;
    label_ds: TLabel;
    CheckBox_anpass_berechtigung: TCheckBox;
    CheckBox_abgleich_neugewinnt: TCheckBox;
    TabSheet_csv: TTabSheet;
    Label65: TLabel;
    Edit_csv_pfad: TEdit;
    BitBtn_csv: TBitBtn;
    Label66: TLabel;
    GroupBox_csv_felder: TGroupBox;
    RadioGroup_csv_datum: TRadioGroup;
    GroupBox_familienstand: TGroupBox;
    Label57: TLabel;
    Label64: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Edit_csv_fg: TEdit;
    Edit_csv_fw: TEdit;
    Edit_csv_fh: TEdit;
    Edit_csv_fl: TEdit;
    GroupBox_geschlecht: TGroupBox;
    Label69: TLabel;
    Label70: TLabel;
    Edit_csv_m: TEdit;
    Edit_csv_w: TEdit;
    GroupBox_csv_spalten: TGroupBox;
    StringAlignGrid_csv: TStringAlignGrid;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    GroupBox_csv_optionen: TGroupBox;
    CheckBox_csv_ueberschreiben: TCheckBox;
    CheckBox_csv_archiv: TCheckBox;
    CheckBox_csv_archiv_rest: TCheckBox;
    SpinEdit_csv: TSpinEdit;
    RadioGroup_csv_ueberein: TRadioGroup;
    CheckBox_pdf_via_dat: TCheckBox;
    CheckBox_word_close: TCheckBox;
    Label74: TLabel;
    Edit_pdftk: TEdit;
    BitBtn_pdftk: TBitBtn;
    Button2: TButton;
    CheckBox_email: TCheckBox;
    CheckBox_email_abteilung: TCheckBox;
    CheckBox_zeiterfassung: TCheckBox;
    Label75: TLabel;
    SpinEdit_mws_reduziert: TSpinEdit;
    Label76: TLabel;
    CheckBox_wdv_refi: TCheckBox;
    Label77: TLabel;
    GroupBox4: TGroupBox;
    StringAlignGrid_rechnung_format: TStringAlignGrid;
    Memo1: TMemo;
    GroupBox5: TGroupBox;
    StringAlignGrid_rechnung_spalten: TStringAlignGrid;
    TabSheet_laborbrief: TTabSheet;
    GroupBox6: TGroupBox;
    StringAlignGrid_labor_werte: TStringAlignGrid;
    GroupBox7: TGroupBox;
    StringAlignGrid_labor_hinweise: TStringAlignGrid;
    CheckBox_runadmin: TCheckBox;
    Label78: TLabel;
    Label79: TLabel;
    Edit_untergeordnete_firma: TEdit;
    CheckBox_einzelplatz: TCheckBox;
    procedure ToolButton_quitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton_cancelClick(Sender: TObject);
    procedure BitBtn_defaultClick(Sender: TObject);
    procedure BitBtnClick(Sender: TObject);
    procedure Button_nkr_einlesenClick(Sender: TObject);
    procedure Button_sperrdatei_loeschenClick(Sender: TObject);
    procedure Button_soundexneuClick(Sender: TObject);
    procedure CheckBox_druck_mit_zustimmung_beschClick(Sender: TObject);
    procedure CheckBox_druck_mit_zustimmung_besch_eignungClick(
      Sender: TObject);
    procedure CheckBox_druck_mit_zustimmung_eignungClick(Sender: TObject);
    procedure CheckBox_druck_mit_zustimmungClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button_repo_pruefenClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private-Deklarationen }

  public
    { Public-Deklarationen }
  end;

var
  Form_optionen: TForm_optionen;

implementation

uses a_main, a_data, a_pfadwahl, a_vert_syst;

{$R *.DFM}

procedure TForm_optionen.ToolButton_quitClick(Sender: TObject);
begin
	modalresult:=mrok;
end;

procedure TForm_optionen.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
	try
  if connected then
	datamodul.sql(false,datamodul.q_1,'select * from haupt_tabelle','');
	except
	end;
	//if system_modus=0 then tabsheet_abrechnung.TabVisible:=false; 2015-09-06 bisher werden die einstellungen nicht �bernommen
  if prg_typ=2 then tabsheet_db.tabvisible:=false;
end;

procedure TForm_optionen.ToolButton_cancelClick(Sender: TObject);
begin
	modalresult:=mrabort;
end;

procedure TForm_optionen.BitBtn_defaultClick(Sender: TObject);
begin
edit_host.Text:='localhost';
edit_db.Text:='arbene';
edit_port.text:='3306';
edit_login.text:='arbene';

end;

procedure TForm_optionen.BitBtnClick(Sender: TObject);
var
pfad:string;
position:integer;
begin
  form_pfadwahl:=tform_pfadwahl.create(self);
  form_pfadwahl.Edit_uv.Visible:=true;
  form_pfadwahl.Button_anlegen.Visible:=true;
  if form_pfadwahl.showmodal =mrok then
  begin
       pfad:=form_pfadwahl.DirectoryListBox.Directory;
       if copy(pfad,length(pfad),1)<>'\'then pfad:=pfad+'\';

       if lowercase(pfad)='c:\' then showmessage('Der Pfad darf nicht "c:\" sein.');

       if tbitbtn(sender)=bitbtn_temp then edit_temp.Text:=pfad;
       if tbitbtn(sender)=bitbtn_ldt then edit_ldt.Text:=pfad;
       if tbitbtn(sender)=bitbtn_infotexte then edit_infotexte.Text:=pfad;
       if tbitbtn(sender)=bitbtn_mysql_daten then edit_mysql_daten.Text:=pfad;
       if tbitbtn(sender)=bitbtn_backupverzeichnis then edit_backupverzeichnis.Text:=pfad;
       if tbitbtn(sender)=bitbtn_lokal then edit_abgl_lokal.Text:=pfad;
       if tbitbtn(sender)=bitbtn_mysql_prg then edit_mysql_prg.Text:=pfad;
       if tbitbtn(sender)=bitbtn_vorlagen then edit_vorlagen.Text:=pfad;
       if tbitbtn(sender)=bitbtn_bescheinigungen then edit_bescheinigungen.Text:=pfad;
       if tbitbtn(sender)=bitbtn_rechnungen then edit_rechnungen.Text:=pfad;
       if tbitbtn(sender)=bitbtn_hilfsprogramme then edit_hilfsprogramme.Text:=pfad;
       if tbitbtn(sender)=bitbtn_csv then edit_csv_pfad.Text:=pfad;
       if tbitbtn(sender)=bitbtn_pdftk then edit_pdftk.Text:=pfad;
  end;
  form_pfadwahl.Release;
end;

procedure TForm_optionen.Button_nkr_einlesenClick(Sender: TObject);
var
i:integer;
query:string;
begin
if  b.b_string('Berechtigungen')=-1  then exit;

if Messagedlg('Sollen der Nummernkreis aktualisiert werden? '+#10+'(neue Einstellung unter Optionen werden nicht gespeichert)',mtConfirmation, mbOkCancel	,0)=mrOK then
begin
    datamodul. sql_new(true,datamodul.q_1,'select * from satelliten','');
    query:='select nummern_kreis from haupt_tabelle';
    i:=strtoint(mysql_d.Feldinhalt(query,0));
    form_main.aktualisiere_nummern_kreis(i,'');
	  
     showmessage('der Nummernkreis '+inttostr(i)+' wurde neu eingelesen');
     modalresult:=mrabort;
end;

end;

procedure TForm_optionen.Button_sperrdatei_loeschenClick(Sender: TObject);
var query:string;
begin
   if Messagedlg('Soll die Sperrdatei gel�scht werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
   query:='delete from sperren where sperrobjekt<>"user"';
   mysql_d.sql_exe(query);

end;

procedure TForm_optionen.Button_soundexneuClick(Sender: TObject);
begin
	form_main.soundexneu;

end;

procedure TForm_optionen.CheckBox_druck_mit_zustimmung_beschClick(
  Sender: TObject);
begin
if CheckBox_druck_mit_zustimmung_besch.Checked then CheckBox_druck_mit_zustimmung.Checked:=false;
end;

procedure TForm_optionen.CheckBox_druck_mit_zustimmung_besch_eignungClick(
  Sender: TObject);
begin
  if CheckBox_druck_mit_zustimmung_besch_eignung.Checked then CheckBox_druck_mit_zustimmung_eignung.Checked:=false;
end;

procedure TForm_optionen.CheckBox_druck_mit_zustimmung_eignungClick(
  Sender: TObject);
begin
   if CheckBox_druck_mit_zustimmung_eignung.Checked then CheckBox_druck_mit_zustimmung_besch_eignung.Checked:=false;
end;

procedure TForm_optionen.CheckBox_druck_mit_zustimmungClick(
  Sender: TObject);
begin
   if CheckBox_druck_mit_zustimmung.Checked then CheckBox_druck_mit_zustimmung_besch.Checked:=false;
end;

procedure TForm_optionen.Button1Click(Sender: TObject);
var
nr,query:string;
begin
with datamodul do
begin
if  b.b_string('Berechtigungen')=-1  then exit;
if Messagedlg('Sollen die Dokumente komprimiert werden? '+#10+'(Dies bezieht sich nur auf Dokumente die vor Version 3.3.0.14 erstellt wurden)',mtConfirmation, mbOkCancel	,0)<>mrOK then  exit;

  query:='select * from dokumente';
  sql_new(false,q_3,query,'');
  q_3.first;
  while not q_3.eof do
  begin
     form_main.stream_komprimieren(q_3,'ole');
     label_ds.caption:='Dokumente '+q_3.findfield('nummer').asstring;
     application.ProcessMessages;
     q_3.Next;
  end;
  query:='select * from texte';
  datamodul.sql_new(false,q_3,query,'');
  q_3.first;
  while not q_3.eof do
  begin
     form_main.stream_komprimieren(q_3,'ole');
     label_ds.caption:='Texte '+ q_3.findfield('nummer').asstring;
     application.ProcessMessages;
     q_3.Next;
  end;
  label_ds.caption:='Abgeschlossen'
end;

end;



procedure TForm_optionen.Button_repo_pruefenClick(Sender: TObject);
begin
  form_main.rechnungen_pruefen('');
  //StringAlignGrid_csv.Cols[0].CommaText
end;

procedure TForm_optionen.Button2Click(Sender: TObject);
var
html_name:string;
begin
      html_name:='https://www.pdflabs.com/tools/pdftk-server/';
      shellexecute_se(0,'open',pchar(html_name),'','',sw_shownormal); //application.Handle
end;

end.
