program DatenbankTools;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  abackup_main in 'abackup_main.pas' {aForm_main},
  zip in 'zip.pas',
  db_tools_datamodul in 'db_tools_datamodul.pas' {DataModule1: TDataModule},
  dbtools_pfadwahl in 'dbtools_pfadwahl.pas' {Form_dbpfadwahl};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Datenbank-Tools';
  Application.CreateForm(TaForm_main, aForm_main);
  Application.CreateForm(TForm_dbpfadwahl, Form_dbpfadwahl);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.Run;
end.
