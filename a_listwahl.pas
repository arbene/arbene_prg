unit a_listwahl;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, dbgridEXT, a_data, a_main,
	Menus,db,Zdataset, ComCtrls, ToolWin, variants;

type
  TForm_liste = class(TForm)
    DBgridEXT: TDBgridEXT;
    PopupMenu: TPopupMenu;
    Hinzufgen1: TMenuItem;
    Lschen1: TMenuItem;
    ndern1: TMenuItem;
    ToolBar: TToolBar;
    ToolButton_neu: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton_quit: TToolButton;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    Panel_b: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PopupMenu_aufwand: TPopupMenu;
    zusammenfhren1: TMenuItem;
    lschen2: TMenuItem;
    ndern2: TMenuItem;
    SpeedButton_down: TSpeedButton;
    SpeedButton_up: TSpeedButton;
    ToolButton4: TToolButton;
    procedure Hinzufgen1Click(Sender: TObject);
    procedure Lschen1Click(Sender: TObject);
    procedure DBgridEXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBgridEXTKeyPress(Sender: TObject; var Key: Char);
    procedure ndern1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBgridEXTDblClick(Sender: TObject);
    procedure ToolButton_quitClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure lschen2Click(Sender: TObject);
    procedure zusammenfhren1Click(Sender: TObject);
    procedure ndern2Click(Sender: TObject);
    procedure reihenfolge(richtung:integer);
    procedure SpeedButton_downClick(Sender: TObject);
    procedure SpeedButton_upClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
	  { Public-Deklarationen }
	  bereich_feld:string;
	  bereich_wert:integer;
	  s_name:string;
  end;

var
	Form_liste: TForm_liste;

implementation

uses a_line_edit;

{$R *.DFM}

procedure TForm_liste.Hinzufgen1Click(Sender: TObject);
begin
 form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 if form_line_edit.showmodal=mrok then
 begin
	//dbgridext.datasource.dataset.AppendRecord([null,form_line_edit.edit.text]);
  if dbgridext.DataSource.DataSet.FindField('reihenfolge')<>nil then
    neuer_datensatz(tzquery(dbgridext.datasource.dataset),['nummer',s_name],[null,form_line_edit.edit.text])
  else
    neuer_datensatz(tzquery(dbgridext.datasource.dataset),['nummer',s_name,'reihenfolge'],[null,form_line_edit.edit.text,0]);
	dbgridext.datasource.dataset.Edit;
	if dbgridext.datasource.dataset.FindField(bereich_feld)<>nil then dbgridext.datasource.dataset[bereich_feld]:=bereich_wert;
	dbgridext.datasource.dataset.post;
 end;
 form_line_edit.Release;
end;

procedure TForm_liste.Lschen1Click(Sender: TObject);
begin
dateiloeschen(tzquery(dbgridext.datasource.dataset),false,false); //if MessageDlg('Datensatz l�schen?', mtConfirmation	,[mbYes, mbNo], 0)=mryes then datamodul.q_1.delete;
end;

procedure TForm_liste.DBgridEXTKeyDown(Sender: TObject; var Key: Word;
	Shift: TShiftState);
begin
	form_liste.caption:=DBgridEXT.eingabetext;
end;

procedure TForm_liste.DBgridEXTKeyPress(Sender: TObject; var Key: Char);
var
ordkey:integer;
begin
ordkey:=ord(key);
case  ordkey of
13: modalresult:=mrok;
48..256 :
  begin
    form_liste.caption:=DBgridEXT.eingabetext;
    dbgridext.datasource.dataset.first;
    dbgridext.datasource.dataset.locate(s_name,DBgridEXT.eingabetext,[loCaseInsensitive,loPartialKey]);
    dbgridext.SetFocus;
	end;

end;
end;

procedure TForm_liste.ndern1Click(Sender: TObject);
begin
 form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 form_line_edit.edit.text:=dbgridext.datasource.dataset[s_name];
 if form_line_edit.showmodal=mrok then
 begin
	dbgridext.datasource.dataset.Edit;
	dbgridext.datasource.dataset[s_name]:=form_line_edit.edit.text;
	dbgridext.datasource.dataset.post;
	dbgridext.datasource.dataset.refresh;
 end;
 form_line_edit.Release;

end;

procedure TForm_liste.FormShow(Sender: TObject);
begin
	//height:= form_main.Height-170;
	form_main.form_positionieren(tform(sender));
	//op:=140;
	grid_einstellen(dbgridext);
  if dbgridext.DataSource.DataSet.FindField('reihenfolge')<>nil then
  begin
    SpeedButton_up.Visible:=true;
    SpeedButton_down.Visible:=true;
  end
  else
  begin
    SpeedButton_up.Visible:=false;
    SpeedButton_down.Visible:=false;
  end;
end;

procedure TForm_liste.DBgridEXTDblClick(Sender: TObject);
begin
	modalresult:=mrok;
end;

procedure TForm_liste.ToolButton_quitClick(Sender: TObject);
begin
modalresult:=mrabort;
end;

procedure TForm_liste.ToolButton1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_liste.lschen2Click(Sender: TObject);
var
name_bei,nr_loesch, query,nr_bei:string;
i:integer;
begin

name_bei:=dbgridext.datasource.dataset.findfield('name').asstring;
nr_loesch:=dbgridext.datasource.dataset.findfield('nummer').asstring;

if Messagedlg( copy( name_bei,1,80)+ #13#10+'wird gel�scht, - weiter?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
      query:=format('update anlass_aufwand set storno=1 where i_aufwand=%s',[nr_loesch]);
      mysql_d.sql_exe(query);

      query:=format('update aufwand set storno=1 where nummer=%s',[ nr_loesch]);
      mysql_d.sql_exe(query);

  datamodul.q_7.Refresh;

end;

procedure TForm_liste.zusammenfhren1Click(Sender: TObject);
var
name_bei,nr_loesch, query,nr_bei:string;
i:integer;
begin
//best�tigung
//storno in aufw�nde
//angleichen  anlass_aufwand
name_bei:=dbgridext.datasource.dataset.findfield('name').asstring;
nr_bei:=dbgridext.datasource.dataset.findfield('nummer').asstring;
if  dbgridext.SelectedRows.Count<1 then
begin
	showmessage('Es m�ssem mindestens eine Zeile markiert sein.');
   exit;
end;
if Messagedlg( copy( name_bei,1,80)+ #13#10+'wird beibehalten, die �brigen markierten Zeilen werden gel�scht - weiter?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;

 for I := 0 to dbgridext.SelectedRows.Count - 1 do
 begin
        datamodul.q_7.GotoBookmark(Pointer(DBGridext.SelectedRows.Items[I]));
        if datamodul.q_7.findfield('nummer').asstring<>nr_bei then
        begin
            nr_loesch:=dbgridext.datasource.dataset.findfield('nummer').asstring;
            query:=format('update anlass_aufwand set i_aufwand=%s where i_aufwand=%s',[nr_bei, nr_loesch]);
            mysql_d.sql_exe(query);

            query:=format('update aufwand set storno=1 where nummer=%s',[ nr_loesch]);
            mysql_d.sql_exe(query);

        end;
 end;

  datamodul.q_7.Refresh;
end;

procedure TForm_liste.ndern2Click(Sender: TObject);
begin
    form_line_edit:=tform_line_edit.create(self); ;
 form_line_edit.CheckBox.visible:=false;
 form_line_edit.edit.text:=dbgridext.datasource.dataset.findfield('name').asstring;
 if form_line_edit.showmodal=mrok then
 begin
	dbgridext.datasource.dataset.Edit;
	dbgridext.datasource.dataset.findfield('name').asstring:=form_line_edit.edit.text;
	dbgridext.datasource.dataset.post;
	dbgridext.datasource.dataset.refresh;
 end;
 form_line_edit.Release;
end;


procedure TForm_liste.reihenfolge(richtung:integer);
var
reihenfolge1,reihenfolge2:integer;
nummer1,i:integer;
begin
with datamodul do
begin
 
	if (richtung=0) and (dbgridext.DataSource.DataSet.Bof) then exit;
	if (richtung=1) and (dbgridext.DataSource.DataSet.eof) then exit;
  nummer1:=dbgridext.DataSource.DataSet.findfield('nummer').asinteger;
  //neu sortieren
  i:=1;
  dbgridext.DataSource.DataSet.DisableControls;
  dbgridext.DataSource.DataSet.First;
  while not dbgridext.DataSource.DataSet.Eof do
  begin
     dbgridext.DataSource.DataSet.edit;
     dbgridext.DataSource.DataSet.findfield('reihenfolge').asinteger:=i;
     dbgridext.DataSource.DataSet.post;
     inc(i);
     dbgridext.DataSource.DataSet.Next;
  end;
  dbgridext.DataSource.DataSet.locate('nummer',nummer1,[]);
  dbgridext.DataSource.DataSet.EnableControls;
  reihenfolge1:=dbgridext.DataSource.DataSet.findfield('reihenfolge').asinteger;
    if reihenfolge1=-1 then
    begin
       dbgridext.DataSource.DataSet.edit;
       dbgridext.DataSource.DataSet.findfield('reihenfolge').asinteger:=nummer1;
       dbgridext.DataSource.DataSet.post;
       dbgridext.DataSource.DataSet.refresh;
	   dbgridext.DataSource.DataSet.Locate( 'nummer', nummer1,[]);
       exit;
    end;
	if (richtung=0) then dbgridext.DataSource.DataSet.Prior else dbgridext.DataSource.DataSet.Next;

	reihenfolge2:=dbgridext.DataSource.DataSet.findfield('reihenfolge').asinteger;
  if (reihenfolge2=reihenfolge1) and not(dbgridext.DataSource.DataSet.eof) and not(dbgridext.DataSource.DataSet.bof) then reihenfolge2:=reihenfolge1+1;
	dbgridext.DataSource.DataSet.edit;
	dbgridext.DataSource.DataSet.FindField('reihenfolge').AsInteger:=reihenfolge1;
	dbgridext.DataSource.DataSet.post;
	//q_berlist.first;
	dbgridext.DataSource.DataSet.locate('nummer',nummer1,[]);
	dbgridext.DataSource.DataSet.edit;
	dbgridext.DataSource.DataSet.FindField('reihenfolge').asinteger:=reihenfolge2;
	dbgridext.DataSource.DataSet.post;
	dbgridext.DataSource.DataSet.refresh;
	dbgridext.DataSource.DataSet.Locate( 'nummer', nummer1,[]);
end;
end;

procedure TForm_liste.SpeedButton_downClick(Sender: TObject);
begin
reihenfolge(1);
end;

procedure TForm_liste.SpeedButton_upClick(Sender: TObject);
begin
reihenfolge(0);
end;

end.
