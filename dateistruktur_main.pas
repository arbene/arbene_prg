unit dateistruktur_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, ZQuery, ZMySqlQuery, ZConnect, ZMySqlCon, ZTransact,
  ZMySqlTr;

type
  TForm_main = class(TForm)
    db1: TZZMySqlDatabase;
    q1: TZZMySqlQuery;
    q2: TZZMySqlQuery;
    db2: TZZMySqlDatabase;
    Edit1: TEdit;
    Edit2: TEdit;
    b_start: TButton;
    Memo1: TMemo;
    q11: TZZMySqlQuery;
    ZZMySqlTransact1: TZZMySqlTransact;
    ZZMySqlTransact2: TZZMySqlTransact;
    procedure b_startClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    function dateistruktur1( dat: string):string;
    function dateistruktur2( dat: string):string;
  end;

var
  Form_main: TForm_main;

implementation

{$R *.DFM}


function tform_main.dateistruktur1( dat: string):string;
var
query:string;
i:integer;
begin
query:= format('select * from %s limit 0',[dat]);
result:='';
q1.close;
q1.Sql.text:=query;
q1.open;
for i:=0 to q1.fields.Count-1 do
begin
     result:=result+ lowercase(q1.fields[i].fieldname)+'|';
end;

end;

function tform_main.dateistruktur2( dat: string):string;
var
query:string;
i:integer;
begin
query:= format('select * from %s limit 0',[dat]);
result:='';
q2.close;
q2.Sql.text:=query;
q2.open;
for i:=0 to q2.fields.Count-1 do
begin
     result:=result+ lowercase(q2.fields[i].fieldname)+'|';
end;

end;

procedure TForm_main.b_startClick(Sender: TObject);
var
tn,w1,w2:string;
begin
db1.Database:=edit1.Text;
db1.Connect;
q11.Open;
db2.Database:=edit2.text;
db2.connect;


memo1.Clear;
q11.first;
while not q11.eof do
begin
     tn:=q11['tab_name'];
     w1:=dateistruktur1(tn);
     w2:=dateistruktur2(tn);
     if w1=w2 then memo1.Lines.Add('OK '+tn) else
     begin
          memo1.Lines.Add('Fehler '+tn);
          memo1.Lines.Add(w1);
          memo1.Lines.Add(w2);
     end;
     q11.next;
end;



end;

end.
 