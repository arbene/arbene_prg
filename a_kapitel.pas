unit a_kapitel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, ComCtrls, ToolWin,db, ExtCtrls;

type
  TForm_kapitel = class(TForm)
    ToolBar: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_delete: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_close: TToolButton;
    DBGrid: TDBGrid;
    ToolButton_save: TToolButton;
    ToolButton1: TToolButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton_newClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton_closeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton_deleteClick(Sender: TObject);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure ComboBoxChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_kapitel: TForm_kapitel;

implementation

uses a_main, a_data, a_line_edit;

{$R *.DFM}

procedure TForm_kapitel.FormCreate(Sender: TObject);
var
query:string;
begin
form_main.form_positionieren(tform(sender));
query:='select * from kapitel';
datamodul.sql_new(true,datamodul.q_1,query,'kapitel');


toolbutton_new.enabled:=true;
  toolbutton_edit.enabled:=true;
  toolbutton_delete.Enabled:=false;
  toolbutton_save.enabled:=false;
end;

procedure TForm_kapitel.ToolButton_newClick(Sender: TObject);
var
bereich:integer;
kapitel:string;
begin
{if combobox.ItemIndex=-1 then
begin
		showmessage('bitte Bereich ausw�hlen');
		exit;
end;}
try
form_line_edit:=tform_line_edit.create(self);
form_line_edit.checkbox.visible:=false;
if form_line_edit.Showmodal<>mrok then exit;
//bereich:=combobox.itemindex;
kapitel:=trim(copy(form_line_edit.Edit.Text,1,50));
if lowercase( kapitel)='alle' then
begin
	showmessage('"alle" darf nicht als Kapitel verwendet werden.');
	exit;
end;
neuer_datensatz(datamodul.q_1,['bereich','kapitel','storno'],[0,kapitel,0]);
toolbutton_new.enabled:=false;
  toolbutton_edit.enabled:=false;
  toolbutton_delete.Enabled:=false;
  toolbutton_save.enabled:=true;

finally
form_line_edit.release;
end;
end;



procedure TForm_kapitel.ToolButton_editClick(Sender: TObject);
var
bereich:integer;
kapitel:string;
begin
{if combobox.ItemIndex=-1 then
begin
		showmessage('bitte Bereich ausw�hlen');
		exit;
end;}
try
form_line_edit:=tform_line_edit.create(self);
form_line_edit.checkbox.visible:=false;
form_line_edit.Edit.Text:= datamodul.q_1.findfield('kapitel').asstring ;
if form_line_edit.Showmodal<>mrok then exit;
kapitel:=trim(copy(form_line_edit.Edit.Text,1,50));
datamodul.q_1.edit;
datamodul.q_1.findfield('kapitel').asstring:=kapitel;

toolbutton_new.enabled:=false;
  toolbutton_edit.enabled:=false;
  toolbutton_delete.Enabled:=true;
  toolbutton_save.enabled:=true;

finally
form_line_edit.release;
end;

end;

procedure TForm_kapitel.ToolButton_closeClick(Sender: TObject);
begin
toolbutton_saveclick(sender);
modalresult:=mrok;
end;

procedure TForm_kapitel.FormClose(Sender: TObject;
	var Action: TCloseAction);
begin
	if datamodul.q_1.State in [dsedit, dsinsert] then  datamodul.q_1.post;
end;


procedure TForm_kapitel.ToolButton_deleteClick(Sender: TObject);
begin
 if Messagedlg('Soll der aktuelle Datensatz gel�scht werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
 datamodul.q_1.Edit;
 datamodul.q_1.findfield('storno').asinteger:=1;
 datamodul.q_1.post;
 datamodul.q_1.refresh;

 toolbutton_new.enabled:=true;
  toolbutton_edit.enabled:=true;
  toolbutton_delete.Enabled:=false;
  toolbutton_save.enabled:=false;
end;


procedure TForm_kapitel.ToolButton_saveClick(Sender: TObject);
begin
   if datamodul.q_1.state in [dsedit, dsinsert] then
   datamodul.q_1.post;

  toolbutton_new.enabled:=true;
  toolbutton_edit.enabled:=true;
  toolbutton_delete.Enabled:=false;
  toolbutton_save.enabled:=false;
end;


procedure TForm_kapitel.ComboBoxChange(Sender: TObject);
var
query:string;
begin
	//toolbutton_saveclick(sender);
 //query:=format('select * from kapitel where bereich=%d',[combobox.itemindex]);
 //datamodul.sql_new(true,datamodul.q_1,query,'kapitel');
end;


end.
