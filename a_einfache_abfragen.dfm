�
 TFORM_EINFACHE_ABFRAGEN 0�  TPF0Tform_einfache_abfragenform_einfache_abfragenLeft�Top� Width�Height�CaptionAbfragenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TPanelPanel_bLeft TopfWidthxHeight%AlignalBottom
BevelOuterbvNoneTabOrder  TBitBtn	BitBtn_okLeft� TopWidthvHeightCaptionOKDefault	TabOrder OnClickBitBtn_okClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtn
BitBtn_escLeft�TopWidthvHeightCaptionAbbruchModalResultTabOrder
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs   	TGroupBoxGroupBox_firmaLeft Top WidthHeightfAlignalLeftCaption   Firma auswählenTabOrder TListBoxListBox_firmaTagLeftTopiWidthHeight�AlignalClientColor	clBtnFace
ItemHeightTabOrder   TPanelPanel2LeftTopWidthHeightZAlignalTop
BevelOuterbvNoneTabOrder TBitBtnBitBtn1Left0TopWidth� HeightCaption   Firmen auwählenTabOrder OnClickBitBtn1Click    TPanelPanel1LeftTop Width_HeightfAlignalClientCaptionPanel1TabOrder 	TGroupBoxGroupBox_datumLeftTopWidth]Height� AlignalTopCaptionOptionenTabOrder  TLabelLabel1LeftTop3Width!HeightCaptionBeginn  TLabelLabel2Left� Top3WidthHeightCaptionEnde  TSpeedButtonSpeedButton_teLeftTop.WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_tbClick  TSpeedButtonSpeedButton_tbLeft}Top/WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_tbClick  TLabelLabel_erbringerLeftTopPWidth*HeightCaption	ErbringerVisible  	TCheckBoxCheckBox_excelLeftoTopWidth� HeightCaptiondirekt in Excel anzeigenTabOrder  	TCheckBoxCheckBox_ausgeschiedenLeftTop{WidthJHeightCaption-   zusätzlich ausgeschiedene Probanden anzeigenTabOrder  	TComboBoxComboBox_zeiterfassungLeftTopWidthHeightDropDownCount
ItemHeightTabOrder TextHeuteOnChangeComboBox_zeiterfassungChangeItems.StringsHeuteaktuelles Jahr
1. Quartal
2. Quartal
3. Quartal
4. QuartalVorjahr1. Quartal Vorjahr2. Quartal Vorjahr3. Quartal Vorjahr4. Quartal Vorjahrgesamter Zeitraum   	TCheckBoxCheckBox_soll_jahrLeftTopiWidth�HeightCaptionB   Für die Sollzeit wird das Kalenderjahr (Beginn) zu Grunde gelegt.TabOrderVisible  	TCheckBoxCheckBox_archiviertLeftTop� Width� HeightCaption*   zusätzlich archivierte Probanden anzeigenTabOrder  	TMaskEditdateedit_beginnLeft8Top0WidthAHeightEditMask!90/90/0000;1;_	MaxLength
TabOrderText
  .  .      	TMaskEditdateedit_endeLeft� Top0WidthBHeightEditMask!90/90/0000;1;_	MaxLength
TabOrderText
  .  .      TDBCombo_numDBCombo_num_zeitabrLeftHTopMWidth� Heightalle_einfuegen	keines_einfuegen	DataFieldi_untersucher
DataSourceDataModul.ds_firma_sollistzconnectionDataModul.connection_main	ztab_nameuntersucherzfield_textuntersucherzfield_indexnummerzorderuntersucherzstorno	
ItemHeightTabOrderVisible   	TGroupBox	GroupBox1LeftTop� Width]Height�AlignalClientCaption   Abfrage auswählenTabOrder TListBoxListBox_abfrageLeftTopWidthYHeightBAlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder OnClickListBox_abfrageClick
OnDblClickBitBtn_okClick  	TGroupBox	GroupBox2LeftTopQWidthYHeightiAlignalBottomCaptionHinweis zur AbfrageTabOrder TMemoMemo_hinweisLeftTopWidthUHeightXAlignalClientReadOnly	
ScrollBars
ssVerticalTabOrder       