object Form_main: TForm_main
  Left = 368
  Top = 139
  Width = 703
  Height = 534
  BorderIcons = []
  Caption = ' Backup / Sicherung der bisherigen Programmdatei'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 184
    Top = 440
    Width = 297
    Height = 25
    Caption = 'Weiter'
    ModalResult = 6
    TabOrder = 0
  end
  object Memo: TMemo
    Left = 72
    Top = 96
    Width = 521
    Height = 249
    TabOrder = 1
  end
  object Timer: TTimer
    Enabled = False
    OnTimer = TimerTimer
    Left = 192
    Top = 32
  end
end
