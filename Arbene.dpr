program arbene;
{%ToDo 'Arbene.todo'}

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  dialogs,
  a_kalender in 'a_kalender.pas' {Form_kalender},
  editor in 'editor.pas',
  a_tabellen in 'a_tabellen.pas' {Form_tabellen},
  a_besonderheiten in 'a_besonderheiten.pas' {Form_besonderheiten},
  a_line_edit in 'a_line_edit.pas' {Form_line_edit},
  a_nachuntersuchung in 'a_nachuntersuchung.pas' {Form_untersuchungstyp},
  a_icd in 'a_icd.pas' {Form_icd},
  a_termin_wahl in 'a_termin_wahl.pas' {Form_twahl},
  a_termine_drag in 'a_termine_drag.pas' {Form_terminieren_drag},
  a_unt_plan in 'a_unt_plan.pas' {Form_unt_plan},
  a_termine_filtern in 'a_termine_filtern.pas' {Form_termine_filtern},
  a_auswertung in 'a_auswertung.pas' {Form_auswertung},
  a_main in 'a_main.pas' {Form_main},
  a_com in 'a_com.pas',
  a_vorlagen in 'a_vorlagen.pas' {Form_vorlagen},
  a_soundex in 'a_soundex.pas',
  a_namsuch in 'a_namsuch.pas' {Form_namsuch},
  a_start in 'a_start.pas' {Form_start},
  a_berechtigung in 'a_berechtigung.pas' {Form_berechtigung},
  a_verschieb in 'a_verschieb.pas' {Form_verschieb},
  a_bmp_wahl in 'a_bmp_wahl.pas' {form_bmp_wahl},
  a_optionen in 'a_optionen.pas' {Form_optionen},
  a_listwahl in 'a_listwahl.pas' {Form_liste},
  a_ma_unit in 'a_ma_unit.pas' {Form_ma_unit},
  mysql_direkt in 'mysql_direkt.pas',
  a_import in 'a_import.pas' {Form_importieren},
  a_unt_wahl in 'a_unt_wahl.pas' {Form_untwahl},
  a_smtp in 'a_smtp.pas' {Form_smtp},
  a_schicht in 'a_schicht.pas' {Form_schicht},
  a_abgleich in 'a_abgleich.pas' {Form_abgleich},
  a_treewahl in 'a_treewahl.pas' {Form_treewahl},
  a_serienbrieffelder in 'a_serienbrieffelder.pas' {form_serienfelder},
  a_vert_syst in 'a_vert_syst.pas' {Form_vert_syst},
  a_termine in 'a_termine.pas' {Form_termine},
  a_speichern in 'a_speichern.pas' {Form_speichern},
  a_f_wahl in 'a_f_wahl.pas' {Form_f_wahl},
  a_email in 'a_email.pas' {Form_email},
  a_nachricht in 'a_nachricht.pas' {Form_nachricht},
  a_registrieren in 'a_registrieren.pas' {Form_registrierung},
  a_ldt in 'a_ldt.pas' {Form_ldt},
  a_einfache_abfragen in 'a_einfache_abfragen.pas' {form_einfache_abfragen},
  a_rechnung in 'a_rechnung.pas' {Form_rechnung},
  a_unterschungs_info in 'a_unterschungs_info.pas' {Form_u_info},
  a_report in 'a_report.pas',
  a_arbeitsplatz in 'a_arbeitsplatz.pas' {Form_ap},
  a_ap_infowahl in 'a_ap_infowahl.pas' {Form_ap_infowahl},
  a_gefaehrdungen in 'a_gefaehrdungen.pas' {Form_gefaehrdungen},
  a_pdf_wahl in 'a_pdf_wahl.pas' {Form_pdfwahl},
  a_pfadwahl in 'a_pfadwahl.pas' {Form_pfadwahl},
  a_besch_druck in 'a_besch_druck.pas' {Form_kartei_druck},
  a_repo_neu in 'a_repo_neu.pas' {Form_repo_neu},
  a_uebersicht_arbeitsunfall in 'a_uebersicht_arbeitsunfall.pas' {form_ueb_arbunfall},
  a_data in 'a_data.pas' {DataModul: TDataModule},
  a_abgleich_ausnahme in 'a_abgleich_ausnahme.pas' {Form_imp_ausnahmen},
  a_backup in 'a_backup.pas' {Form_backup},
  a_kapitel in 'a_kapitel.pas' {Form_kapitel},
  a_dokutitel in 'a_dokutitel.pas' {Form_dokutitel},
  a_arztbrief_drucken in 'a_arztbrief_drucken.pas' {Form_arztbrief_drucken},
  a_textbausteine in 'a_textbausteine.pas' {Form_textbausteine},
  a_vonbis in 'a_vonbis.pas' {Form_datumvonbis},
  a_hdnummer in 'a_hdnummer.pas',
  arbene_TLB in 'arbene_TLB.pas',
  arbene_auto in 'arbene_auto.pas',
  a_bdt in 'a_bdt.pas' {Form_bdt},
  a_dbwahl in 'a_dbwahl.pas' {form_dbwahl},
  a_zeiterfallsung_all in 'a_zeiterfallsung_all.pas' {Form_zeiterfassung_all},
  a_checkbox in 'a_checkbox.pas' {Form_checkbox},
  a_firmen_zusammen in 'a_firmen_zusammen.pas' {form_firmen_zusammen},
  vclvixes in 'vclvixes.pas',
  a_uebernahme_jahr in 'a_uebernahme_jahr.pas' {Form_uebername_jahr},
  a_memo in 'a_memo.pas' {Form_memo},
  a_anzeige_kartei in 'a_anzeige_kartei.pas' {Form_anzeige_kartei},
  a_spenden in 'a_spenden.pas' {Form_spenden},
  a_repo_edit_m in 'a_repo_edit_m.pas' {Form_repo_edit_m},
  a_suchliste in 'a_suchliste.pas' {form_suchliste},
  a_wartefenster in 'a_wartefenster.pas' {form_wartefenster},
  a_labornummern in 'a_labornummern.pas' {Form_labornummer},
  a_rechnungsgestaltung in 'a_rechnungsgestaltung.pas' {Form_rechnungsgestaltung},
  a_archiv_rest in 'a_archiv_rest.pas' {Form_archiv_rest},
  a_vorsorgen_abgleich in 'a_vorsorgen_abgleich.pas' {Form_vorsorge_abgleich},
  NTDLLDebugNOP in 'NTDLLDebugNOP.pas',
  a_richedit_menue in 'a_richedit_menue.pas' {form_richedit_menue},
  a_csv_import in 'a_csv_import.pas' {Form_csv_import},
  a_weiter in 'a_weiter.pas' {Form_weiter},
  a_rechnung_automatisch in 'a_rechnung_automatisch.pas' {Form_rechnung_automatisch},
  a_timer in 'a_timer.pas' {Form_timer};

{$R *.RES}

{$R *.TLB}
begin

	Application.Initialize;
	Application.Title := 'Arbene';
  //Application.HelpFile := 'D:\arbmed\ARBENE.HLP';
  Application.CreateForm(TDataModul, DataModul);
  Application.CreateForm(TForm_main, Form_main);
  Application.CreateForm(TForm_rechnung_automatisch, Form_rechnung_automatisch);
  Application.CreateForm(TForm_timer, Form_timer);
  //Application.CreateForm(TForm_uebername_jahr, Form_uebername_jahr);
  if form_main.tag<>1 then
	begin
   //MemChk;
	//showmessage('0');
      form_start.info('Programm l�dt Kalender');
      application.ProcessMessages;
      Application.CreateForm(TForm_kalender, Form_kalender);
      //showmessage('kalender');
      form_start.info('Programm l�dt Termine');
      application.ProcessMessages;
      Application.CreateForm(TForm_termine_filtern, Form_termine_filtern);

      application.ProcessMessages;

      // showmessage('Form_termine_filtern');
      Application.CreateForm(TForm_twahl, Form_twahl);
      //showmessage('Form_twahl');
       application.ProcessMessages;
      //showmessage('termine');
      form_start.info('Arbene l�dt Namensuche');
       application.ProcessMessages;
      Application.CreateForm(TForm_namsuch, Form_namsuch);
      //showmessage('namsuch');
      form_start.info('Arbene l�dt Auswertung');
       application.ProcessMessages;
      Application.CreateForm(TForm_auswertung, Form_auswertung);
      //Application.CreateForm(TForm_ap, Form_ap);
      form_start.info('Auswertungen sind geladen');
       application.ProcessMessages;

      form_start.release;
      form_main.wartefenster_zeigen;

      Application.Run;
	  end
	else
    begin
      form_main.Visible:=false;
      form_main.width:=0;
      form_main.Height:=0;
      form_main.Close;
      Application.Run;
    
    end;
end.
