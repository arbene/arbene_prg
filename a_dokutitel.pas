unit a_dokutitel;

interface
                                
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TForm_dokutitel = class(TForm)
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    Edit: TEdit;
    ComboBox: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_dokutitel: TForm_dokutitel;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_dokutitel.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

end.
