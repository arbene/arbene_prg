unit a_einfache_abfragen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ExtCtrls, dbcombo_number;

type
  Tform_einfache_abfragen = class(TForm)
    Panel_b: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    GroupBox_firma: TGroupBox;
    ListBox_firma: TListBox;
    Panel1: TPanel;
    GroupBox_datum: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    ListBox_abfrage: TListBox;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    GroupBox2: TGroupBox;
    Memo_hinweis: TMemo;
    CheckBox_excel: TCheckBox;
    CheckBox_ausgeschieden: TCheckBox;
    ComboBox_zeiterfassung: TComboBox;
    CheckBox_soll_jahr: TCheckBox;
    CheckBox_archiviert: TCheckBox;
    dateedit_beginn: TMaskEdit;
    dateedit_ende: TMaskEdit;
    SpeedButton_te: TSpeedButton;
    SpeedButton_tb: TSpeedButton;
    DBCombo_num_zeitabr: TDBCombo_num;
    Label_erbringer: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ListBox_abfrageClick(Sender: TObject);
    procedure ComboBox_zeiterfassungChange(Sender: TObject);
    procedure Zeiteneinstellen;
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton_tbClick(Sender: TObject);
  private
	  { Private-Deklarationen }


	public
	  { Public-Deklarationen }
	  	  ffilter:string;
          berechtigung:integer;
  end;

var
  form_einfache_abfragen: Tform_einfache_abfragen;

implementation

uses a_main, a_f_wahl, a_data, a_kalender;

{$R *.DFM}

procedure Tform_einfache_abfragen.FormCreate(Sender: TObject);
var
query:string;
begin

  case prg_typ of
	3:begin
		Listbox_abfrage.Items.Add('Gesamtumsatz');
		Listbox_abfrage.Items.Add('Umsatz Behandlungen');
		Listbox_abfrage.Items.Add('Umsatz Artikel');
		Listbox_abfrage.Items.Add('Material gesamt');
		Listbox_abfrage.Items.Add('Material zum Bestellen');
		Listbox_abfrage.Items.Add('Geburtstagsliste');
		groupbox_firma.width:=0;

	end;
  1,2,4:
    begin    //extra abfragen f�r Abfragen durch Kunden berechtigung=1 noch notwendig
    if (b.b_string_silent('Auswertung-Gesamt')>-1)  then
		begin
         Listbox_abfrage.Items.Add('Probanden');
         if (show_impfung and show_labor) then Listbox_abfrage.Items.Add('Durchgef�hrte Untersuchungen');
         Listbox_abfrage.Items.Add('Abgeschlossene Bescheinigungen');
         Listbox_abfrage.Items.Add('Abgeschlossene Bescheinigungen (nicht gedruckt)');

         if show_impfung then Listbox_abfrage.Items.Add('Durchgef�hrte Impfungen');
         if show_labor then Listbox_abfrage.Items.Add('Durchgef�hrte Labor-Untersuchungen');
         Listbox_abfrage.Items.Add('Geplante Bescheinigungen');
         if show_impfung then Listbox_abfrage.Items.Add('Geplante Impfungen');
         if show_labor then Listbox_abfrage.Items.Add('Geplante Labor-Untersuchungen');
         Listbox_abfrage.Items.Add('BG-Statistik');
         Listbox_abfrage.Items.Add('Statistik Arbeitsunf�lle');
         if b.b_string_silent('firma-rechnung')>=0 then
         begin
           Listbox_abfrage.Items.Add('Rechnungen Firma');
           Listbox_abfrage.Items.Add('unbezahlte Rechnungen Firma');
         end;
         if b.b_string_silent('proband-rechnung')>=0 then
         begin
           Listbox_abfrage.Items.Add('Rechnungen Proband');
           Listbox_abfrage.Items.Add('unbezahlte Rechnungen Proband');
         end;
         //if prg_typ<>2 then Listbox_abfrage.Items.Add('Geburtstagsliste');
       	Listbox_abfrage.Items.Add('Zeiten-Einzelnachweis');
        Listbox_abfrage.Items.Add('Zeiten-Firma-Konten-Summe');
       	Listbox_abfrage.Items.Add('Zeitbilanz');
        Listbox_abfrage.Items.Add('Zeitbilanz erweitert');
      end
      else
      begin //wenn nicht gesamt auswertung
        if  (b.b_string_silent('Auswertung-Arbeitssicherheit')>-1) then
        begin
           //Listbox_abfrage.Items.Add('Probanden');
           Listbox_abfrage.Items.Add('Abgeschlossene Bescheinigungen AG');
           Listbox_abfrage.Items.Add('Geplante Bescheinigungen AG');
           //if show_impfung then Listbox_abfrage.Items.Add('Durchgef�hrte Impfungen');
           Listbox_abfrage.Items.Add('BG-Statistik');
           Listbox_abfrage.Items.Add('Statistik Statistik Arbeitsunf�lle');
        end;

       end;
     end;
  end;
	form_main.form_positionieren(tform(sender));
   if einfache_abfragen_beginn='' then dateedit_beginn.text:=datetostr(date) else dateedit_beginn.text:=(einfache_abfragen_beginn) ;
	dateedit_ende.text:=datetostr(date);

  ffilter:=firmen_filter;
  query:=format('select * from firma where nummer in (%s)',[ffilter]);
  datamodul.sql(true,datamodul.q_3,query,'firma');
  form_main.itemslesen(listbox_firma.items,datamodul.q_3,'firma','');
  dbcombo_num_zeitabr.lookup_laden;
  //Zeiteneinstellen;
end;



procedure Tform_einfache_abfragen.BitBtn1Click(Sender: TObject);
var query:string;
begin

form_f_wahl:=tform_f_wahl.create(self);
if form_f_wahl.ShowModal=mrok then
begin
	ffilter:=form_f_wahl.firmen_nummern;
	query:=format('select * from firma where nummer in (%s)',[ffilter]);
	datamodul.sql(true,datamodul.q_3,query,'firma');
	if ffilter<>'' then form_main.itemslesen(listbox_firma.items,datamodul.q_3,'firma','');
end;
form_f_wahl.Release;

end;

procedure Tform_einfache_abfragen.BitBtn_okClick(Sender: TObject);
begin
if (groupbox_firma.width>0) and  (listbox_firma.Items.Count<=0) then
begin
	showmessage('Bitte mindestens eine Firma ausw�hlen');
	exit;
end;
try
	//strtodate(dateedit_beginn.date);
	//strtodate(dateedit_ende.date);
except
	showmessage('Datum nicht korrekt');
	exit;
end;

if listbox_abfrage.ItemIndex<0 then
begin
	showmessage('Bitte Abfrage ausw�hlen');
	exit;
end;


modalresult:=mrok;
end;




procedure Tform_einfache_abfragen.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
   einfache_abfragen_beginn:=(dateedit_beginn.text);
   CheckBox_excel.checked:=false;
end;



procedure Tform_einfache_abfragen.ListBox_abfrageClick(Sender: TObject);
var
abfrage:string;
i:integer;
begin

  CheckBox_soll_jahr.Visible:=false;
 DBCombo_num_zeitabr.ItemIndex:=0;

  abfrage:=form_einfache_abfragen.listbox_abfrage.Items[form_einfache_abfragen.listbox_abfrage.itemindex];
  if abfrage= 'Probanden' then i:=0;
  if abfrage= 'Geplante Bescheinigungen' then  i:=1;
  if abfrage= 'Geplante Impfungen' then i:=2;
  if abfrage= 'BG-Statistik' then i:=3;
  if abfrage= 'Rechnungen Proband' then i:=4;
  if abfrage= 'Rechnungen Firma' then i:=5;
  if abfrage= 'unbezahlte Rechnungen Proband' then i:=6;
  if abfrage= 'unbezahlte Rechnungen Firma' then i:=15;
  //if abfrage= 'Geburtstagsliste' then i:=6;
  if abfrage= 'Geplante Labor-Untersuchungen' then i:=7;
  if abfrage= 'Abgeschlossene Bescheinigungen' then i:=8;
  if abfrage= 'Durchgef�hrte Impfungen' then i:=9;
  if abfrage= 'Durchgef�hrte Labor-Untersuchungen' then i:=10;
  if abfrage= 'Zeiten-Firma-Konten-Summe' then i:=11;
  if abfrage= 'Zeiten-Einzelnachweis' then i:=12;
  if abfrage= 'Zeitbilanz' then i:=13;
  if abfrage= 'Zeitbilanz erweitert' then i:=20;
  if abfrage= 'Durchgef�hrte Untersuchungen' then i:=14;
  if abfrage= 'Abgeschlossene Bescheinigungen (nicht gedruckt)' then i:=16;
  if abfrage= 'Abgeschlossene Bescheinigungen AG' then i:=17;
  if abfrage= 'Geplante Bescheinigungen AG' then i:=18;
  if abfrage= 'Statistik Arbeitsunf�lle' then  i:=19;
memo_hinweis.Text:='';

case i of
0: begin
		memo_hinweis.lines.Add('Firma, Name, Vorname, Geburtsdatum');
      memo_hinweis.lines.Add('und die Adresse des Probanden');
   end;
1: begin
     memo_hinweis.lines.Add('Firma, Name, Vorname, Geburtsdatum');
     memo_hinweis.lines.Add('Datum und Typ der geplanten Untersuchung');
   end;
2: begin
		memo_hinweis.lines.Add('Firma, Name, Vorname, Geburtsdatum');
     memo_hinweis.lines.Add('Datum und Typ der geplanten Impfung')
    end;
3: begin
	  memo_hinweis.lines.Add('Statistik �ber die Bescheinigungen ');
     //memo_hinweis.lines.Add('Vorsorgeuntersuchungen');
   end;
4: begin
	  memo_hinweis.lines.Add('�bersicht �ber die Rechnungen, die Probanden zugeordnet sind');

   end;
5: begin
	  memo_hinweis.lines.Add('�bersicht �ber die Rechnungen, die einer Firma zugeordnet sind');
   end;
6: begin
	  memo_hinweis.lines.Add('�bersicht �ber die unbezahlten Rechnungen, die Probanden zugeordnet sind');

   end;
15: begin
	  memo_hinweis.lines.Add('�bersicht �ber die unbezahlten Rechnungen, die einer Firma zugeordnet sind');

   end;
8: begin
	  memo_hinweis.lines.Add('Abgeschlossenes Bescheinigungen');
     memo_hinweis.lines.Add('sowie die dazugeh�rigen geplanten Bescheinigungen');
     memo_hinweis.lines.Add('');
   end;
7: begin
	  memo_hinweis.lines.Add('geplante Laboruntersuchungen');

   end;
9: begin
	  memo_hinweis.lines.Add('Durchgef�hrte Impfungen');
     memo_hinweis.lines.Add('');
     memo_hinweis.lines.Add('');
   end;
10: begin
	  memo_hinweis.lines.Add('Durchgef�hrte Laboruntersuchungen');
     memo_hinweis.lines.Add('');
     memo_hinweis.lines.Add('');
   end;
11: begin
	  memo_hinweis.lines.Add('Summen �ber das Zeitkonto und das Abrechnungskriterium');
     memo_hinweis.lines.Add('');
     memo_hinweis.lines.Add('');
   end;
12: begin
	  memo_hinweis.lines.Add('alle erbrachten Zeiten');
     memo_hinweis.lines.Add('');
     memo_hinweis.lines.Add('');
   end;
13: begin
	  memo_hinweis.lines.Add('Summen �ber das Zeitkonto ');
     memo_hinweis.lines.Add('bilanziert mit den Zeitvorgaben');
     memo_hinweis.lines.Add('');
     memo_hinweis.lines.Add('Die Sollzeit bezieht entsprechend der Auswahl ');
     memo_hinweis.lines.Add('auf das Kalenderjahr oder auf den gew�hlten Zeitraum.');
     CheckBox_soll_jahr.Visible:=true;
   end;
14: begin
    memo_hinweis.lines.Add('Bescheinigungen, Impfungen und Labor-Untersuchungen, ');
     memo_hinweis.lines.Add('die neu angelegt oder ver�ndert wurden.');
   end;
16: begin
    memo_hinweis.lines.Add('Abgeschlossene Bescheinigungen, ');
    memo_hinweis.lines.Add('die noch nicht als gedruckt markiert sind.');
   end;
17: begin
    memo_hinweis.lines.Add('Abgeschlossene und geplante Bescheinigungen, ');
    memo_hinweis.lines.Add('ohne Beurteilung');
   end;
18: begin
    memo_hinweis.lines.Add('geplante Bescheinigungen ');
   end;
19: begin
        memo_hinweis.lines.Add('');
    end;
20: begin
	  memo_hinweis.lines.Add('Summen �ber das Zeitkonto und die Erbringer');
     memo_hinweis.lines.Add('bilanziert mit den Zeitvorgaben');
     memo_hinweis.lines.Add('');
     memo_hinweis.lines.Add('Die Sollzeit bezieht entsprechend der Auswahl ');
     memo_hinweis.lines.Add('auf das Kalenderjahr oder auf den gew�hlten Zeitraum.');
     CheckBox_soll_jahr.Visible:=true;
   end;
end;

case i of
  11,12,13 :
    begin
      Label_erbringer.Visible:=true;
      DBCombo_num_zeitabr.Visible:=true;


    end;
 else
   begin
      Label_erbringer.Visible:=false;
      DBCombo_num_zeitabr.Visible:=false;

      
   end;
end;

case i of
  11,12,13,20 :
    begin
      CheckBox_ausgeschieden.visible:=false;
      CheckBox_archiviert.visible:=false;
    end;
 else
   begin

      CheckBox_ausgeschieden.visible:=true;
      CheckBox_archiviert.visible:=true;
   end;
end;

end;

procedure Tform_einfache_abfragen.ComboBox_zeiterfassungChange( Sender: TObject);
begin
   Zeiteneinstellen;
end;




procedure Tform_einfache_abfragen.Zeiteneinstellen;
var dat:tdate;
	yy,m,d:word;
	yl,y,a,e:string;
begin
decodedate(date,yy,m,d);
y:=inttostr(yy);
yl:=inttostr(yy-1);
if combobox_zeiterfassung.ItemIndex=-1 then combobox_zeiterfassung.ItemIndex:=0;
case combobox_zeiterfassung.ItemIndex of
0:begin
    a:=datetostr(now());
    e:=datetostr(now());
  end;
1:begin
	a:='01.01.'+y;
    e:='31.12.'+y;
  end;
2:begin
	a:='01.01.'+y ;
    e:='31.03.'+y ;
  end;
3:begin
    a:='01.04.'+y;
    e:='30.06.'+y;
  end;
4:begin
	a:='01.07.'+y;
    e:='30.09.'+y;
  end;
5:begin
	a:='01.10.'+y;
    e:='31.12.'+y;
  end;

6:begin
	a:='01.01.'+yl;
    e:='31.12.'+yl;
  end;
7:begin
	a:='01.01.'+yl ;
    e:='31.03.'+yl ;
  end;
8:begin
    a:='01.04.'+yl;
    e:='30.06.'+yl;
  end;
9:begin
	a:='01.07.'+yl;
    e:='30.09.'+yl;
  end;
10:begin
	a:='01.10.'+yl;
    e:='31.12.'+yl;
  end;
11:begin
	a:='01.01.2000';
    //e:=DateToStr(Now);
    e:='31.12.9999';
  end;
end;


dateEdit_beginn.text:=(a);
dateEdit_ende.text:=(e);

end;

procedure Tform_einfache_abfragen.FormDestroy(Sender: TObject);
begin
 dbcombo_num_zeitabr.loeschen;
 form_main.itemsdeaktivieren(listbox_firma.items);
end;

procedure Tform_einfache_abfragen.SpeedButton_tbClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
   begin
    if sender=speedbutton_tb then
	  dateedit_beginn.text:=datetostr(int(form_kalender.auswahlkalender.date));
    if sender=speedbutton_te then
	  dateedit_ende.text:=datetostr(int(form_kalender.auswahlkalender.date));
   end;
end;

end.
