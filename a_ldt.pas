unit a_ldt;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, a_data,mysql_direkt,db, Spin, ComCtrls, CheckLst, variants,ZipMstr19,
  ExtCtrls;

type
		laborbericht=^labbericht;
		labbericht  = record
		Berichtsart: integer;
		ID:string;
		labid:string;
    docid:string;
		eingang:tdate;
		ausgang:tdate;
		befundart:string;
		geschlecht:integer;
		nname:string;  
		vname:string;
		end;

type
	TForm_ldt = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    SpinEdit_vor: TSpinEdit;
    SpinEdit_nach: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    CheckListBox: TCheckListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    BitBtn_zuordnen: TBitBtn;
    CheckBox_kompress: TCheckBox;
    BitBtn_ende: TBitBtn;
    Label7: TLabel;
    Label9: TLabel;
    CheckBox_close: TCheckBox;
    ComboBox_was: TComboBox;
    Label3: TLabel;
    Label8: TLabel;
	  procedure BitBtn1Click(Sender: TObject);
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_zuordnenClick(Sender: TObject);
    procedure CheckListBoxClickCheck(Sender: TObject);
    procedure CheckListBoxDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn_endeClick(Sender: TObject);
	private
	  { Private-Deklarationen }
	  ldtdat,dir:string;
	  procedure ldteinlesen;
	  procedure ds_zuordnen(labor_ident:string;ma_nummer:int64;datum:tdate;geschlecht:integer);
	  procedure liste_leeren;
	  function suche_typ(testident,testbezeichnung,einheit,normalwerttext:string;geschlecht:integer):integer;
	  procedure labor_anlegen(ma_nummer:int64;typ_id:integer;datum:tdate;lab_id, doc_id,testident:string;ergebniswert:real;
												einheit,normalwerttext, ergebnistext,grenzwertindikator,testbezogenerhinweis:string);
	  function labor_ersetzen(test:string):boolean;
      procedure ldt_komprimieren(dat,dir:string);
      function ldt_convert_dat(s:string; v:integer):string;
	public
	  { Public-Deklarationen }
	end;

var
	Form_ldt: TForm_ldt;

implementation

uses a_main, a_namsuch, a_unt_wahl;

{$R *.DFM}

procedure tform_ldt.ldteinlesen;
var anzeige, nz,query,s,fi,anforderungsident,testident,ident,satztyp,nachname,vorname, gebdat,
		labid,docid,t,sident:string;
	  eingang,ausgang:tdate;
	  f: textfile;
	  fk,geschlecht,i,intident,code:integer;
	  lb,o:laborbericht;
	  r:real;
    felder:array [0..1] of string;
    ch,mikro:boolean;
    v: int64;
    versionds:integer;
begin
nz:=chr(13)+chr(10);
intident:=0;
versionds:=0;//undefiniert
try
	AssignFile(F,ldtdat );   { Datei ausgew�hlt }
	Reset(F);
with datamodul do
begin
	query:='delete from ldt';
	mysql_d.sql_exe(query);
	sql(true,q_1,'select * from ldt','');
	i:=1;


	while not eof(f) do
	begin
		Readln(F, S);
    if modus_debug then showmessage(S);
		fk:=strtoint(copy(s,4,4));
		fi:=copy(s,8,length(s));
		fi:=trim(form_main.umlaut_convert(fi));


		case fk of

			8000: begin //satzidentifikation      fi=8220 am dateianfang
						if (fi='8201') or (fi='8202') or (fi='8203') then
						begin
							nachname:='';
							vorname:='';
							gebdat:='';
							anforderungsident:='';
						end;
					end;
      9212: begin  //Version der Datensatzbescchreibung
              if fi='LDT1001.02' then versionds:=1;
              if fi='LDT1014.01' then versionds:=2;
            end;
			8310: begin //anforderungs-ident immer als erstes
              docid:=fi;
              anforderungsident:=fi;
              mikro:=false;
					  end;
			3101: begin //name
              nachname:=fi;
              inc(intident);
              if anforderungsident='' then anforderungsident:=nachname;
              {val(fi,v,code);
              if code<>0 then
              begin    //keine Zahl
                nachname:=fi;
                inc(intident);
                if anforderungsident='' then anforderungsident:=inttostr(intident)+' '+nachname;
              end
              else
              begin
                anforderungsident:=fi;
              end; }
           end;
			3102: begin //vorname
						vorname:=fi;
            if anforderungsident=inttostr(intident)+' '+nachname then anforderungsident:= anforderungsident+' '+vorname;
					end;
			3103: begin //geburtsdatum
            gebdat:=ldt_convert_dat(fi, versionds);
						{insert('.',fi,3);
						insert('.',fi,6);
						gebdat:=fi;}
            //if anforderungsident=nachname+vorname then anforderungsident:= nachname+vorname+gebdat;
					end;
			8311: begin //Auftragsnummer des Labors

						labid:=fi;
            mikro:=false;
					end;
			8301: begin //eingagnsdatum
						//insert('.',fi,3);
						//insert('.',fi,6);
						//lb^.eingang:=strtodate(fi);
						//o:=laborbericht(listbox.items.Objects[listbox.items.count-1]);
						//eingang:=strtodate(fi);
						//listbox.items.Objects[listbox.items.count-1]:=tobject(o);
            eingang:=strtodate(ldt_convert_dat(fi, versionds));
            mikro:=false;

					end;
			8302: begin //berichtdatum
						//insert('.',fi,3);
						//insert('.',fi,6);
						//o:=laborbericht(listbox.items.Objects[listbox.items.count-1]);
						//ausgang:=strtodate(fi);
						//listbox.items.Objects[listbox.items.count-1]:=tobject(o);
						//lb^.ausgang:=strtodate(fi);
            ausgang:=strtodate(ldt_convert_dat(fi, versionds));
            mikro:=false;
					end;

			8407: begin // geschlecht
						//lb^.geschlecht:=strtoint(fi);
						//o:=laborbericht(listbox.items.Objects[listbox.items.count-1]);
						geschlecht:=strtoint(fi);
						//listbox.items.Objects[listbox.items.count-1]:=tobject(o);
					end;
			8401: begin //befundart sowohl in 8201,8202,8203
                  //8401 die Befundarten FI=(E = Endbefund, T = Teilbefund, V = Vorl�ufiger Befund, A = Archiv-Befund, N = Nachforderung 
                mikro:=false;
                        //checken ob ident nur 1 mal
                ch:=false;
                if anforderungsident<>'' then
                begin
                    sident:='%'+anforderungsident+'%';
                    query:=format ('select count(*) from mitarbeiter where l_nummer like "%s" ',[sident]);
                    mysql_d.Feldarray(query,[0],felder);
                    if felder[0]='1' then ch :=true;
                    try
                      if strtoint(felder[0])>1 then showmessage('Die Anforderungsident '+anforderungsident+' kommt mehrfach vor.');
                    except
                    end;
                    query:=format ('select name, vorname from mitarbeiter where l_nummer like "%s" ',[sident]);
                    mysql_d.Feldarray(query,[0,1],felder);
                    if ch then
                    begin
                      nachname:=felder[0];
                      vorname:=felder[1];
                    end;
                end;

            anzeige:=anforderungsident+' '+nachname+' '+vorname;
						anzeige:=trim(anzeige);
						if anzeige<>'' then
						begin
						  new(lb);
						  lb^.ID:=anforderungsident;
						  lb^.labid:=labid;
              lb^.docid:=docid;
						  lb^.eingang:=eingang;
						  lb^.ausgang:=ausgang;
						  lb^.geschlecht:=geschlecht;
						  lb^.befundart:=fi;
						  lb^.nname:=nachname;
						  lb^.vname:=vorname;
						  checklistbox.items.AddObject(anzeige,tobject(lb));
						  checklistbox.itemindex:=checklistbox.Items.Count-1;
              checklistbox.Checked[checklistbox.itemindex]:=ch;
              if modus_debug then showmessage('liste add '+anzeige);
						  application.processmessages;
						end;
					end;
			end;


		if anforderungsident<>'' then
			case fk of
        8410: begin // test-id
						inc(i);
						neuer_datensatz(q_1,['nummer','lab_id','doc_id','anforderungsident','testident','testbezeichnung','testbezogenerhinweis'],[i,lb.labid,lb.docid, lb^.id,fi,fi,'']);  //testbezeicchnung wird bei 8411 �berschrieben
						q_1.post;
					end;
         8434: begin // Anforderung Mikrobiologischer Bericht
               if not mikro then
               begin
						inc(i);
						neuer_datensatz(q_1,['nummer','lab_id','do_cid','anforderungsident','testident','testbezogenerhinweis'],[i,lb.labid,lb.docid,lb^.id,fi,'']);
						q_1.post;
                  mikro:=true;
               end;

               begin //testbezeichnung
						q_1.edit;
                  if q_1['testbezeichnung']='' then
							q_1['testbezeichnung']:=q_1['testbezeichnung']+fi
                  else q_1['testbezeichnung']:=q_1['testbezeichnung']+': '+fi ;
						q_1.post;
				   end;

					end;


			8411:begin //testbezeichnung
						q_1.edit;
						q_1.findfield('testbezeichnung').asstring:=fi;
						q_1.post;
				  end;
			8420:begin  //ergebniswert
						q_1.edit;
						fi:=stringreplace(fi,'.',',',[rfReplaceAll]);
						r:=round(strtofloat(fi)*100);      //2 stellen hinterm kommma
						r:=r/100;
						q_1['ergebniswert']:=r;
						q_1.post;
				  end;
			8421:begin  //einheit
						q_1.edit;
						q_1['einheit']:=fi;
						q_1.post;
				  end;
			8480:begin   //normalwerte , ergebnis
            mikro:=false;
            if fi<>'' then
						begin
              			t:=trim(q_1.findfield('ergebnistext').asstring);
						  q_1.edit;
						  if t='' then q_1['ergebnistext']:=fi
						  else q_1['ergebnistext']:=q_1['ergebnistext']+nz+fi;
						  q_1.post;
						end;  
				  end;
			8470:begin  //testbezogener Hinweis
            if fi<>'' then
						begin
              t:=trim(q_1.findfield('testbezogenerhinweis').asstring);
						  q_1.edit;
						  if t='' then q_1.findfield('testbezogenerhinweis').asstring:=fi
						  else q_1.findfield('testbezogenerhinweis').asstring:=q_1.findfield('testbezogenerhinweis').asstring+nz+fi;
						  q_1.post;
						end;
				  end;

			8460:begin

						if fi<>'' then
						begin
              		t:=trim(q_1.findfield('normalwerttext').asstring);
						  q_1.edit;

						  if t='' then q_1['normalwerttext']:=fi
						  else q_1['normalwerttext']:=q_1['normalwerttext']+nz+fi;
							q_1.post;
						end;
				  end;
			8422:begin
						q_1.edit;
						q_1['grenzwertindikator']:=fi;
						q_1.post;
				  end;

		end;
	end;
// einlesen in Labor, wenn der parameter nicht existiert muss einer neu angelegt werden.
if checklistbox.Items.count>0 then checklistbox.ItemIndex:=0;


end;
finally
	CloseFile(F);
end;

end;


procedure tform_ldt.ds_zuordnen(labor_ident:string;ma_nummer:int64;datum:tdate;geschlecht:integer);
var
	typ_id,rf:integer;
	query:string;
begin
with datamodul do
begin
	query:=format('select * from ldt where  anforderungsident= "%s" order by nummer',[labor_ident]);
	sql_new(false,q_1,query,'');
	q_1.first;

	while not q_1.eof do  //typ_id in ldt datei eintragen
	begin
		 typ_id:=suche_typ(q_1.findfield('testident').asstring,q_1.findfield('testbezeichnung').asstring,q_1.findfield('einheit').asstring,q_1.findfield('normalwerttext').asstring,geschlecht);
		 q_1.edit;
		 q_1.FindField('i_typ').asinteger:=typ_id;
		 q_1.post;
		 q_1.next;
	end;

	query:=format('select * from ldt left join typ on (typ.nummer=ldt.i_typ) where  anforderungsident= "%s" order by typ.reihenfolge',[labor_ident]);
	sql(false,q_1,query,'');
	q_1.first;
	while not q_1.eof do
	begin
	  //if q_1['ergebniswert']<>0 then //gruppen�berschrift
	  begin    //wenn typ  nicht vorhanden muss er neu angelegt werden.
     if modus_debug then showmessage('labor anlegen '+q_1['testident']);
     
		 labor_anlegen( ma_nummer,q_1.findfield('i_typ').asinteger,datum,q_1.findfield('lab_id').asstring,q_1.findfield('doc_id').asstring,
                          q_1.findfield('testident').asstring,q_1.findfield('ergebniswert').asfloat,q_1.findfield('einheit').asstring,
                          q_1.findfield('normalwerttext').asstring,
													q_1.findfield('ergebnistext').asstring,q_1.findfield('grenzwertindikator').AsString,q_1.findfield('testbezogenerhinweis').asstring);

	  end;
	  q_1.next;
	end;
end;
end;

procedure TForm_ldt.BitBtn1Click(Sender: TObject);

begin
  if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt datei ausw�hlen');

	checklistbox.Items.Clear;
  bitbtn_zuordnen.SetFocus;
	form_main.opendialog.InitialDir:=pfad_ldt;
	form_main.opendialog.Filter:='LDT-Dateien |*.ldt;*.dat;*.lab';
	if form_main.opendialog.execute=false then exit ;
	ldtdat:=form_main.OpenDialog.FileName;
  dir:= form_main.OpenDialog.InitialDir;
	ldteinlesen;

end;


procedure TForm_ldt.ldt_komprimieren(dat,dir:string);
var
datei:string;
begin
  if not DirectoryExists(pfad_infotexte)then
  begin
    showmessage('Der Pfad f�r die gezippten LDT-Dateien existiert nicht - > Siehe Optionen');
    exit;
  end;

    datei:=sql_datetostr(now);
  datei:=datei+'-'+timetostr(now);
  datei:=stringreplace(datei,'.','-',[rfreplaceall]);
  datei:=stringreplace(datei,':','-',[rfreplaceall]);
  datei:=stringreplace(datei,' ','-',[rfreplaceall]);
  datei:=stringreplace(datei,'"','',[rfreplaceall]);
    {datei:=datetimetostr(now());
    datei:=stringreplace(datei,'.','_',[rfreplaceall]);
    datei:=stringreplace(datei,':','_',[rfreplaceall]); }
    datei:=pfad_infotexte+datei;

     form_main.ZipMaster.AddOptions:= form_main.ZipMaster.AddOptions-[addmove];
    form_main.ZipMaster.FSpecArgs.Add(dat);
	  form_main.ZipMaster.ZipFileName:=datei+'_LDT.zip';
    form_main.ZipMaster.RootDir:= pfad_ldt;
    form_main.ZipMaster.Add;  //hier zippen
    deletefile(dat);
end;

procedure TForm_ldt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt close1');
	liste_leeren;
	form_namsuch.StatusBar.Panels[0].text:='';
	form_namsuch.StatusBar.visible:=false;
  if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt close2');
end;

procedure tform_ldt.liste_leeren;
var
	o:laborbericht;
begin
	while checklistbox.Items.Count>0 do
	begin
	  o:=laborbericht(checklistbox.items.Objects[checklistbox.items.count-1]);
	  dispose(o);
	  checklistbox.Items.delete(checklistbox.items.count-1);
	end;
	datamodul.q_typ.refresh;
end;


procedure TForm_ldt.FormCreate(Sender: TObject);
begin
  if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt create1');
form_main.form_positionieren(tform(sender));
  if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt positioniert');
datamodul.sql(true,datamodul.q_typ_tree,format('select * from typ where bereich = %d',[3]), 'kuerzel');
  if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt create2');
end;


procedure TForm_ldt.BitBtn_zuordnenClick(Sender: TObject);
var
ma:int64;
datum:tdate;
o:laborbericht;
geschlecht:integer;
namestring,anforderungsident,query,f_name,v_name:string;
ch:boolean;
felder :array [0..4] of string;
begin
if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt zuordnen');

if checklistbox.itemindex=-1 then exit;
try
	  o:=laborbericht(checklistbox.items.Objects[checklistbox.itemindex]);
    ch:=checklistbox.checked[checklistbox.itemindex];
    anforderungsident:=o^.id;
    f_name:=o.nname ;
    v_name:=o.vname;
    datum:=o^.eingang;
    geschlecht:=o^.geschlecht;
    if (anforderungsident<>'')  then   //labnumer vorhanden  dann danach den probanden suchen
    begin
        anforderungsident:='%'+anforderungsident+'%';
    	query:=format ('select nummer,name, vorname,geb_dat from mitarbeiter where l_nummer like "%s"',[anforderungsident]);
    	mysql_d.Feldarray(query,[0,1,2,3],felder);
    	if felder[0]<>'' then
    	begin
           ma:=strtoint64(felder[0]);
           o.nname:= felder[1];
           o.vname:=felder[2];
           namestring:=felder[1]+', '+felder[2]+', '+felder[3]+' (LDT-Import)';
    	end
    end;

    if not ch then
    	begin //nummer nicht gefunden
        form_namsuch.StatusBar.visible:=true;
		    form_namsuch.StatusBar.Panels[0].text:=o.id;
        form_namsuch.edit1.text:=o.nname;
        if trim (o.vname)<>'' then form_namsuch.edit1.text:=form_namsuch.edit1.text+', '+o.vname;
        if form_namsuch.showmodal<> mrok then exit;
        if checklistbox.Items.Count<=0 then exit;
        //o:=laborbericht(checklistbox.items.Objects[checklistbox.itemindex]);

        ma:=getbigint(datamodul.q_mitarbeiter_such,'nummer');
        namestring:='';
        if datamodul.q_mitarbeiter_such['Name']<>null then  namestring:= string(datamodul.q_mitarbeiter_such.FindField('Name').asstring);
        if datamodul.q_mitarbeiter_such['vorname']<>null then  namestring:=namestring+ ', '+string(datamodul.q_mitarbeiter_such.FindField('vorname').asstring);
        if datamodul.q_mitarbeiter_such['geb_dat']<>null then
         begin
            namestring:=namestring+ ', '+datetostr(datamodul.q_mitarbeiter_such.findfield('geb_dat').AsDateTime)+' (LDT-Import)';
         end;

    end;

	form_main.history_add(namestring,ma);
  anforderungsident:=o.ID;
  //zuordnen zur Nummer  checklistbox.items[checklistbox.itemindex]
	ds_zuordnen(anforderungsident,ma,datum,geschlecht);

	 //o:=laborbericht(checklistbox.items.Objects[checklistbox.itemindex]);
finally
	 dispose(o);
	 checklistbox.Items.delete(checklistbox.itemindex);
   if checklistbox.items.count>0 then  checklistbox.itemindex:=0;
   if ((checklistbox.items.count=0) and  (checkbox_kompress.Checked)) then ldt_komprimieren(ldtdat,dir);
   if CheckBox_close.Checked and (checklistbox.items.count=0) then modalresult:=mrok;// else    showmessage('Der LDT-Import ist abgeschlossen.');
end;
end;


function tform_ldt.suche_typ(testident,testbezeichnung,einheit,normalwerttext:string;geschlecht:integer):integer;
var
	typ_nummer:integer;
	neu:boolean;
begin
neu:=false;
with datamodul do
begin
	 if not q_typ_tree.Locate( 'kuerzel',testident,[loCaseInsensitive]) then
	 begin  //typ anlegen

		if normalwerttext='' then normalwerttext:='-'; //text als sollfeld darf nicht leer sein

		if not labor_ersetzen(testbezeichnung) then
		begin
			neuer_datensatz(q_typ_tree,['nummer'],[null]);
			//q_typ_tree.Append;
			neu:=true;
		end;
		q_typ_tree.Edit;
		q_typ_tree.FindField('bereich').AsInteger:=3;
		if neu then q_typ_tree.findfield('untersuchung').AsString:=testbezeichnung ;
		q_typ_tree.FindField('kuerzel').AsString:=testident ;
		q_typ_tree.FindField('einheit').Asstring:=einheit ;
		q_typ_tree.FindField('norm_m').asstring:=normalwerttext;
		q_typ_tree.FindField('norm_w').asstring:=normalwerttext;
		if neu then  q_typ_tree.findfield('reihenfolge').asinteger:=0;
		q_typ_tree.post;
		q_typ.Refresh;
	 end;
	 result:=q_typ_tree['nummer'];
end;
end;

procedure tform_ldt.labor_anlegen(ma_nummer:int64;typ_id:integer;datum:tdate;lab_id, doc_id,testident:string;ergebniswert:real;
												einheit,normalwerttext, ergebnistext,grenzwertindikator,testbezogenerhinweis:string);
var
	query,dat_s,dat_e, dat_o:string;
	lab_nr:string;
	neu:boolean;
  modus:integer;
begin
with datamodul do
begin
	 neu:=false;
   modus:=combobox_was.ItemIndex;
	 //suche nach geplantem Labortermin
	 dat_s:=sql_datetostr(datum-spinedit_vor.Value);
	 dat_e:=sql_datetostr(datum+spinedit_nach.Value);
	 dat_o:=sql_datetostr(datum);      // an exaktem Datum


  { //tagesaktuell
	 query:=format('select nummer from labor where  i_mitarbeiter="%s" and i_typ=%d and datum=%s and i_status<>4',
				  [inttostr(ma_nummer),typ_id,dat_o]);
	 lab_nr:=mysql_d.Feldinhalt(query,0);

	 if lab_nr='' then}
   
	 begin
       // offen im Intervall
     case modus of
       0:  query:=format('select nummer from labor where  i_mitarbeiter="%s" and i_typ=%d and datum >=%s and datum <=%s ',     //alle
                  [inttostr(ma_nummer),typ_id,dat_s,dat_e]);

       1: query:=format('select nummer from labor where  i_mitarbeiter="%s" and i_typ=%d and datum >=%s and datum <=%s and i_status<>4',  //alle au�er abgeschlossen
                  [inttostr(ma_nummer),typ_id,dat_s,dat_e]);
       2: query:=format('select nummer from labor where  i_mitarbeiter="%s" and i_typ=%d and ((datum >=%s and datum <=%s and i_status<>4) or (datum =%s and i_status=4))',  //alle au�er abgeschlossen und abgeschlossen tagesaktuell
                  [inttostr(ma_nummer),typ_id,dat_s,dat_e,dat_o]);
       3:  query:=format('select nummer from labor where  i_mitarbeiter=0 and i_typ=0 and i_status=9',    //keine
                  []);
     end;

      lab_nr:=mysql_d.Feldinhalt(query,0);

      if lab_nr='' then //definitiv neu anlegen
        begin
           lab_nr:=inttostr(form_main.lab_neu(datum,typ_id,4,ma_nummer,akt_untersucher,true,true));
           neu:=true;
        end;
	 end;

	 if lab_nr='0' then
	 begin
	 	showmessage('Laborwert konnte nicht angelegt werden da Gruppen�berschrift ausgew�hlt');
	  exit;
	 end;
	 //if not neu then
	 //begin
		//query:='select labor.*,typ.untersuchung from labor left join typ on (labor.i_typ= typ.nummer) '+
		//        format(' where labor.nummer=%s ',[lab_nr]);


		query:=format('select * from labor where labor.nummer=%s ',[lab_nr]);
		sql_new(false,q_2,query,'');
	 //end;
	 //wenn nicht vorhanden

	 q_2.Edit;
	 q_2.FindField('datum').AsDateTime:=datum;
	 q_2.FindField('i_status').asinteger:=4;
	 q_2.findfield('i_untersucher').asinteger:=akt_untersucher;
	 q_2.findfield('wert').asfloat:=ergebniswert ;
	 q_2.findfield('einheit').asstring:=einheit ;
	 q_2.findfield('norm').asstring:=normalwerttext ;
	 q_2.findfield('ergebnistext').asstring:=ergebnistext ;
	 q_2.findfield('grenzwertindikator').AsString:=grenzwertindikator ;
	 q_2.findfield('testbezogenerhinweis').asstring:=testbezogenerhinweis ;
   q_2.FindField('ldt_nummer').asstring:=testident;//docid;
   q_2.FindField('lab_id').asstring:=lab_id;
   q_2.FindField('doc_id').asstring:=doc_id;
   q_2.Post;
end;
end;

function tform_ldt.labor_ersetzen(test:string): boolean;
var ber:integer;
	  bm:integer;
begin
if form_main.nicht_satellit_tab then exit;
ber:=b.b_string('Untersuchungen-Listen');
if ber<1 then exit;
bm:=0;
try
	form_untwahl:=tform_untwahl.Create(self);
	form_untwahl.HelpContext:=10830;


	form_untwahl.ToolButton_new.Visible:=false;
	form_untwahl.ToolButton_delete.Visible:=false;
	form_untwahl.ToolButton_close.Visible:=false;
	form_untwahl.BitBtn_abb.Caption:='Neu anlegen';
	form_untwahl.BitBtn_abb.hint:='Neue Labor-Untersuchung als Vorlage anlegen';
  form_untwahl.BitBtn_abb.Glyph:=nil;

	form_untwahl.BitBtn_ok.Caption:='�berschreiben';
	form_untwahl.BitBtn_ok.hint:='Ausgew�hlte Vorlage f�r die Labor-Untersuchung �berschreiben';
  form_untwahl.BitBtn_ok.Glyph:=nil;
	form_untwahl.statusbar.visible:=true;

	datamodul.sql_new(true,datamodul.q_typ_tree,format('select * from typ where bereich = %d',[3]), 'reihenfolge');

	form_untwahl.notebook.visible:=true;
	form_untwahl.notebook.pageindex:=2;
	form_untwahl.multiselect:=false;
	form_untwahl.ToolBar.Visible:=true;
	form_untwahl.Panel_botttom.Visible:=true;
	form_untwahl.caption:='Labor';
	form_untwahl.statusbar.Panels[0].text:='Bitte vorhandene Labor-Untersuchung f�r '+test+' ausw�hlen oder neu anlegen';
	form_untwahl.tree.bereich_feldname:='bereich';
	form_untwahl.Tree.bereich_wert:=3;
	form_untwahl.tree.mysqlquery:=datamodul.q_typ_tree;
	form_untwahl.tree.mysql_feld:='untersuchung';
	form_untwahl.tree.liste_lesen;
	datamodul.q_typ_tree.first;
  form_untwahl.edit_such.text:=test;
	showmessage('Labor-Untersuchung f�r '+test+' nicht vorhanden. '+chr(13)+'Bitte vorhandene Labor-Untersuchung f�r '+test+' ausw�hlen oder neu anlegen.');
  //form_untwahl.tree.suche_datensatz(test);
	if form_untwahl.showmodal =mrok then result:=true{labor ersetzen} else result:=false  ;
	bm:=datamodul.q_typ_tree['nummer'];
finally
	form_untwahl.release;
	datamodul.sql(true,datamodul.q_typ_tree,format('select * from typ where bereich = %d',[3]), 'kuerzel');
	datamodul.q_typ_tree.Locate('nummer',bm,[]);
end;


end;

procedure TForm_ldt.CheckListBoxClickCheck(Sender: TObject);
begin
if checklistbox.Checked[checklistbox.itemindex]=true then checklistbox.Checked[checklistbox.itemindex]:=false;
end;

procedure TForm_ldt.CheckListBoxDblClick(Sender: TObject);
begin
  BitBtn_zuordnenClick(Sender);
end;

function tform_ldt.ldt_convert_dat(s:string; v:integer):string;

begin
case v of
  2: begin  //JJJJMMTT
      result:=copy(s,7,2)+'.'+copy(s,5,2)+'.'+copy(s,1,4);
     end
else
  begin  //ttMMJJJJ
   insert('.',s,3);
	 insert('.',s,6);
	 result:=s;
  end;
end;
end;
procedure TForm_ldt.FormShow(Sender: TObject);
begin
if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt show');
end;

procedure TForm_ldt.BitBtn_endeClick(Sender: TObject);
begin
if form_main.m_erweitertefehlermeldungen.Checked then showmessage('form_ldt button_ende');
modalresult:=mrok;

end;

end.
