unit a_registrieren;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls;

type
  TForm_registrierung = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit_ln: TEdit;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    Edit_vorname: TEdit;
    Edit_name: TEdit;
    Edit_strasse: TEdit;
    Edit_plz: TEdit;
    Edit_ort: TEdit;
    Edit_telefon: TEdit;
    Edit_email: TEdit;
    Label5: TLabel;
	  Label6: TLabel;
	  Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit_firma: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label_1: TLabel;
    Label_2: TLabel;
    Label_3: TLabel;
    Edit4: TEdit;
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
  private
	  { Private-Deklarationen }
	  s1,s2:string;
  public
    { Public-Deklarationen }
  end;

var
  Form_registrierung: TForm_registrierung;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_registrierung.Edit1Change(Sender: TObject);
begin
	s1:=edit1.text;
	if length(edit1.text)=5 then edit2.setfocus;
end;

procedure TForm_registrierung.Edit2Change(Sender: TObject);
begin
	s2:=edit2.text;
	if length(edit2.text)=5 then edit3.setfocus;
end;

procedure TForm_registrierung.BitBtn3Click(Sender: TObject);
var
query,schluessel,prognr:string;
begin
 query:='select hs1 from haupt_tabelle where haupt=1';
 schluessel:=mysql_d.Feldinhalt(query,0);
 if schluessel<>'' then
		//prognr:=form_main.CipherManager.DecodeString(schluessel)
		prognr:=form_main.string_entschluesseln( schluessel)
 else prognr:='-1';

 try
	 form_main.Arbeitsmedizin.CreateObjectFromFile(w_pfad+'\registrierung.doc',false);    //pfad
	 form_main.Arbeitsmedizin.DoVerb(0);
	 com.wordobjekterstellen(form_main.Arbeitsmedizin);
	 com.bookmark_einfuegen('firma',edit_firma.text,true);
	 com.bookmark_einfuegen('vorname',edit_vorname.text,true);
	 com.bookmark_einfuegen('name',edit_name.text,true);
	 com.bookmark_einfuegen('strasse',edit_strasse.text,true);
	 com.bookmark_einfuegen('plz',edit_plz.text,true);
	 com.bookmark_einfuegen('ort',edit_ort.text,true);
	 com.bookmark_einfuegen('telefon',edit_telefon.text,true);
	 com.bookmark_einfuegen('email',edit_email.text,true);
	 com.bookmark_einfuegen('programmschluessel',prognr,true);
	 com.bookmark_einfuegen('datum',datetostr(date),true);
 except
	 showmessage('"MS-Word" ist nicht korrekt installiert oder die Datei "registrierung.doc" ist nicht im Programmpfad');
	 showmessage('Öffnen Sie Datei c:\programme\zeus\registrierung.doc manuell und tragen sie den Programmschlüssel '+prognr+' ein.');
 end;
end;

procedure TForm_registrierung.BitBtn1Click(Sender: TObject);
var query,s1,s2,k1,k2 :string;
begin
	 query:='select hs1 from haupt_tabelle where haupt=1';
	 s1:=mysql_d.Feldinhalt( query,0);
	 //k1:=form_main.ciphermanager.decodeString(s1);
   k1:=form_main.string_entschluesseln(s1);

	 s2:='Arb'+trim(edit1.text+edit2.text+edit3.text+edit4.text);//+'==';
	 //k2:=form_main.ciphermanager.decodeString(s2);
   k2:=form_main.string_entschluesseln(s2);
	if  (copy(k2,4,7)<>k1) then  //##3
	begin
		showmessage('Der Schlüssel ist fehlerhaft (Gross-Kleinschreibung beachten)');
		exit;
	end;
	query:=format('update haupt_tabelle set hs2="%s" where haupt=1',[s2]);
	mysql_d.sql_exe(query);

	query:=format('update haupt_tabelle set ln="%s" where haupt=1',[edit_ln.text]);
	mysql_d.sql_exe(query);

	form_main.lizenztest;

	showmessage('Sie haben '+application.Title+' erfolgreich registriert, Danke. Das Programm muss jetzt beendet werden.');
	modalresult:=mrok;
end;

procedure TForm_registrierung.FormCreate(Sender: TObject);
begin
  caption:=application.Title+' registrieren';
  if prg_typ=2 then
  begin
   label_1.visible:=false;
   label_2.visible:=false;
   label_3.visible:=false;

  end;
	form_main.form_positionieren(tform(sender));
end;

procedure TForm_registrierung.Edit3Change(Sender: TObject);
begin
	if length(edit3.text)=5 then edit4.setfocus;
end;

end.
