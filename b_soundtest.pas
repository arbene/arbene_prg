unit b_soundtest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_soundex,
	StdCtrls,  ZQuery, ZMySqlQuery, Db, ZTransact, ZMySqlTr, ZConnect,
  ZMySqlCon;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    db: TZMySqlDatabase;
    table1: TZMySqlQuery;
    ZMySqlTransact1: TZMySqlTransact;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;
    soundex:tsoundex;
implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
	edit1.text:=soundex.soundex(edit1.text);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
	db.Connect;
	table1.Open;
	soundex:=Tsoundex.create(1,table1);
end;

end.
