�
 TFORM_GEFAEHRDUNGEN 0D,  TPF0TForm_gefaehrdungenForm_gefaehrdungenLeft}Top� WidthHeight#HelpContext
*ActiveControlToolBar1Caption   Vorlagen für GefährdungenColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TSplitterSplitterLeft�Top-WidthHeight�Color	clMedGrayParentColor  TToolBarToolBar1Left Top WidthHeight-ButtonHeight$ButtonWidth;CaptionToolBarImagesForm_main.ImageList_mainShowCaptions	TabOrder  TToolButtonToolButton_newLeft TopCaptionNeu
ImageIndex OnClickToolButton_newClick  TToolButtonToolButton2Left;TopWidthCaptionToolButton3
ImageIndexStyletbsSeparator  TToolButtonToolButton_changeLeftCTopCaption   Ändern
ImageIndexOnClickToolButton_changeClick  TToolButtonToolButton4Left~TopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeft� TopCaption	Speichern
ImageIndexOnClickToolButton_saveClick  TToolButtonToolButton7Left� TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolButton_delLeft� TopHint	    löschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_delClick  TToolButtonToolButton9LeftTopWidth=CaptionToolButton5
ImageIndexStyletbsSeparator  TToolButtonToolButton_abortLeftITopCaption	Abbrechen
ImageIndexOnClickToolButton_abortClick  TToolButtonToolButton11Left�TopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton_okLeft�TopCaption
   Auswählen
ImageIndexOnClickToolButton_okClick  TFileListBoxFileListBoxLeft�TopWidth� Height$
ItemHeightMask*.pdfTabOrder Visible   TPanel
Panel_leftLeft Top-Width�Height�AlignalLeft
BevelOuterbvNoneCaption
Panel_leftTabOrder TPanel
Panel_suchLeft Top Width�HeightAlignalTop
BevelInnerbvRaised
BevelOuterbvNoneTabOrder  TLabelLabel_suchenLeftTopWidth0HeightCaptionSuchen:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontLayouttlCenter  TSpeedButtonSpeedButton1Left� TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsOnClickSpeedButton1Click  TEdit	Edit_suchLeftQTopWidth� HeightTabOrder OnEnterEdit_suchEnter	OnKeyDownEdit_suchKeyDown   Tdbtreeview_k
dbtreeviewLeft TopWidth�Height�AlignalClientDragModedmAutomaticHideSelectionIndentReadOnly		RowSelect	TabOrderToolTipsOnChangedbtreeviewChange
OnChangingdbtreeviewChanging
OnDblClickdbtreeviewDblClick
OnDragDropdbtreeviewDragDrop
OnDragOverdbtreeviewDragOvera_alla_ebene a_stornotab1DataModul.q_7tab2DataModul.q_8tab3DataModul.q_9key2i_gef1key3i_gef2anzeige_feld1nameanzeige_feld2nameanzeige_feld3memo_gef   TPanelPanel_reLeft�Top-WidthGHeight�AlignalClientCaptionPanel_reTabOrder 	TNotebookNotebookLeftTopWidthEHeight�AlignalClient	PageIndexTabOrder  TPage Left Top Captione1 TLabelLabel2LeftTopWidth� HeightCaption   GegfährdungsklasseFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4Left9Top� WidthHeightCaptionSeiteVisible  TLabelLabel7Left
Top� WidthFHeightCaptionInfo PDF-DateiVisible  TLabelLabel6LeftTop� Width6HeightCaptionInfo WWW  TDBEditDBEdit_gef1LeftTopFWidth�Height	DataFieldname
DataSourceDataModul.ds_7TabOrder   TDBEditDBEdit2Left]Top� WidthRHeight	DataField	pdf_seite
DataSourceDataModul.ds_7TabOrderVisible  TDBComboBoxDBComboBox_pdf2Left\Top� Width$Height	DataField	pdf_datei
DataSourceDataModul.ds_7
ItemHeightTabOrderVisible  TBitBtnBitBtn1TagLeft�Top� WidthMHeightCaptionAnzeigenTabOrderVisible
Glyph.Data
:  6  BM6      6   (                                  ���������������������������JB�90����������������������������������������:1�<3�������������oi�������������������������ni�90�hc�������������C;�����������������LD�KC�<3�90ﲰ�������������90�e_�������������@7�90�90�F@�������������^Y�90�C;�������������sm�90�90�������������?7�90�90�������������90�<3����������������90�90�90�]V�������������QJ�`Z�������������QJ�90�90�90�<4�������������������������������:1�90�90�90�90�������������������������uo�90�90�90�90�90�OG�������������������������A9�90�90�90�90�90�90ﾽ����������������������90�90�90�90�90�90�90�xs�������������������[S�90�90�90�90�90�90�90�>7�������������������90�90�90�90�90�90�90�90�90ﭫ����������������90�90�90�90�90ﯬ�lf�lf�lf�lf��������������yu�lf�lf�lf�lf�lf�  TDBEditDBEdit_e1_wwwLeftWTop� Width(Height	DataFieldurl
DataSourceDataModul.ds_7TabOrder  TBitBtnBitBtn_e1_wwwTagLeft�Top� WidthMHeightCaptionAnzeigenTabOrderOnClickBitBtn_e1_wwwClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUU���UUUUTLLUUUUU�ww_�UUT�D�DUUUWu�Uw_UULC4DDEUUuWUU�T��4��3UW�W�Uw�T��3DD3UWUWw�Uw_���3���EU�w�UUL�33��D�Www��U�333����www��UL�333<LEWwwwU��<�3<��u�u_w�u\��333LUW��www��T�3333�UWWwwww�UU33�<3�UUwwUuwuUUS<���UUUWu��wUUUU\L�UUUUUWwwUUU	NumGlyphs   TPage Left Top Captione2 TLabelLabel1LeftTopWidth� HeightCaption   GefährdungsgruppeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3Left(Top� WidthFHeightCaptionInfo PDF-DateiVisible  TLabelLabel5LeftRTop� WidthHeightCaptionSeiteVisible  TLabelLabel8LeftTop� Width6HeightCaptionInfo WWW  TDBEditDBEdit_gef2LeftTopFWidth�Height	DataFieldname
DataSourceDataModul.ds_8TabOrder   TDBEditDBEdit1Left|Top� WidthPHeight	DataField	pdf_seite
DataSourceDataModul.ds_8TabOrderVisible  TDBComboBoxDBComboBox_pdfLeft{Top� Width
Height	DataField	pdf_datei
DataSourceDataModul.ds_8
ItemHeightTabOrderVisible  TBitBtnBitBtn_acrobatTagLeft�Top� WidthRHeightCaptionAnzeigenTabOrderVisible
Glyph.Data
:  6  BM6      6   (                                  ���������������������������JB�90����������������������������������������:1�<3�������������oi�������������������������ni�90�hc�������������C;�����������������LD�KC�<3�90ﲰ�������������90�e_�������������@7�90�90�F@�������������^Y�90�C;�������������sm�90�90�������������?7�90�90�������������90�<3����������������90�90�90�]V�������������QJ�`Z�������������QJ�90�90�90�<4�������������������������������:1�90�90�90�90�������������������������uo�90�90�90�90�90�OG�������������������������A9�90�90�90�90�90�90ﾽ����������������������90�90�90�90�90�90�90�xs�������������������[S�90�90�90�90�90�90�90�>7�������������������90�90�90�90�90�90�90�90�90ﭫ����������������90�90�90�90�90ﯬ�lf�lf�lf�lf��������������yu�lf�lf�lf�lf�lf�  TDBEditDBEdit_e2_wwwLeftbTop� Width)Height	DataFieldurl
DataSourceDataModul.ds_8TabOrder  TBitBtnBitBtn_e2_wwwTagLeft�Top� WidthHHeightCaptionAnzeigenTabOrderOnClickBitBtn_e1_wwwClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUU���UUUUTLLUUUUU�ww_�UUT�D�DUUUWu�Uw_UULC4DDEUUuWUU�T��4��3UW�W�Uw�T��3DD3UWUWw�Uw_���3���EU�w�UUL�33��D�Www��U�333����www��UL�333<LEWwwwU��<�3<��u�u_w�u\��333LUW��www��T�3333�UWWwwww�UU33�<3�UUwwUuwuUUS<���UUUWu��wUUUU\L�UUUUUWwwUUU	NumGlyphs   TPage Left Top Captione3 TPanel	Panel_gefLeft Top WidthEHeight�AlignalClient
BevelOuterbvNoneTabOrder  TPanelPanel7Left,Top&WidthHeight� AlignalRight
BevelOuterbvNoneTabOrder   	TGroupBox	GroupBox1Left Top WidthEHeightoAlignalTopCaption   Gefährdung / BelastungTabOrder TDBMemoDBMemo_gef3LeftTopWidthAHeight^AlignalClientColorclWhite	DataFieldmemo_gef
DataSourceDataModul.ds_9
ScrollBars
ssVerticalTabOrder WantTabs	   	TGroupBox	GroupBox2Left TopoWidthEHeight|AlignalTopCaption
   MaßnahmenTabOrder TDBMemoDBMemo_massnahmeLeftTopWidthAHeightkAlignalClientColorclWhite	DataFieldmemo_massnahme
DataSourceDataModul.ds_9
ScrollBars
ssVerticalTabOrder WantTabs	   	TGroupBox	GroupBox3Left Top� WidthEHeight� AlignalTopCaptionBescheinigungenTabOrder 
TDBgridEXT
DBgridEXT1LeftTopWidthAHeight� AlignalClient
DataSourceDataModul.ds_15DefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamebescheinigungTitle.CaptionBescheinigungWidth�Visible	 Expanded	FieldName	grundlageTitle.Caption	GrundlageWidthVisible	    TPanelPanel1LeftTop� WidthAHeightAlignalBottom
BevelOuterbvNoneTabOrder TBitBtnBitBtn_hin_notwLeft� TopWidthOHeightCaption   HinzufügenTabOrder OnClickBitBtn_hin_notwClick  TBitBtnBitBtn_weg_notwLeft� TopWidthHHeightCaption   löschenTabOrderOnClickBitBtn_weg_notwClick    	TGroupBox	GroupBox4Left Top�WidthEHeightDAlignalTopCaptionfakultative BescheinigungenTabOrderVisible 
TDBgridEXT
DBgridEXT2LeftTopWidthAHeightAlignalClient
DataSourceDataModul.ds_16DefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamebescheinigungTitle.CaptionBescheinigungWidth�Visible	 Expanded	FieldName	grundlageTitle.Caption	GrundlageWidthVisible	    TPanelPanel2LeftTop'WidthAHeightAlignalBottom
BevelOuterbvNoneTabOrder TBitBtnBitBtn_hin_fakLeft� TopWidthOHeightCaption   HinzufügenTabOrder OnClickBitBtn_hin_fakClick  TBitBtnBitBtn_weg_fakLeftTopWidthIHeightCaption   löschenTabOrderOnClickBitBtn_weg_notwClick         