�
 TFORM_EMAIL 0L  TPF0TForm_email
Form_emailLeftUTop� Width�Height6HelpContext6Caption Mail Ausgang / Verschickte MailsColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TToolBarToolBar1Left Top Width�Height1ButtonHeightButtonWidth;CaptionToolBar1ShowCaptions	TabOrder  TToolButtonToolButton_neuLeft TopHintNeuCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonToolButton2Left;TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton5LeftGTopCaption   Öffnen
ImageIndexOnClickToolButton5Click  TToolButtonToolButton6Left� TopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_deleteLeft� TopHint   LöschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_deleteClick  TToolButtonToolButton8Left� TopWidthCaptionToolButton8
ImageIndexStyletbsSeparator  TToolButtonToolButton_cancelLeft� TopHint   Übertragung abbrechenCaption	AbbrechenEnabled
ImageIndexParentShowHintShowHint	VisibleOnClickToolButton_cancelClick  TToolButtonToolButton4LeftTopWidth
CaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton1LeftTopCaption	Verlassen
ImageIndexOnClickToolButton1Click  TToolButtonToolButton3LeftTTopWidthCaptionToolButton3
ImageIndexStyletbsSeparator  TToolButtonToolButton_sendLeftbTopCaptionSenden
ImageIndexVisibleOnClickToolButton_sendClick   TPanelPanel1Left Top1Width�Height8AlignalTop
BevelOuterbvNoneTabOrder TRadioGroupRadioGroup_sortLeft Top Width Height8AlignalLeftCaption
SortierungColumnsFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	ItemIndex Items.StringsDatum email-Adresse 
ParentFontTabOrder OnClickRadioGroup_sentClick  TRadioGroupRadioGroup_userLeft Top Width� Height8AlignalLeftCaptionAbsenderColumns	ItemIndex Items.Stringseigene Mails
alle Mails TabOrderOnClickRadioGroup_sentClick  TRadioGroupRadioGroup_timeLeft�Top Width�Height8AlignalClientCaption
ZeitspanneColumns	ItemIndex Items.Strings1 Woche 	4 Wochen Vergangenheit TabOrderOnClickRadioGroup_sentClick   TPanelPanel2Left TopiWidth�Height�AlignalClient
BevelOuterbvNoneCaptionPanel_listeTabOrder 
TDBgridEXT	DBgridEXTLeft TopWidth�HeightzAlignalClient
DataSourceDataModul.ds_emailDefaultDrawing	Options	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExitdgMultiSelect ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamedatumTitle.CaptionDatumWidthxVisible	 Expanded	FieldNameadressatTitle.CaptionAdressatWidth� Visible	 Expanded	FieldNamebetreffTitle.CaptionBetreffWidth�Visible	    TPageControlPageControlLeft Top Width�Height
ActivePageTabSheet_ausgangAlignalTopTabOrderOnChangeRadioGroup_sentClick 	TTabSheetTabSheet_ausgangCaptionMail-Ausgang  	TTabSheetTabSheet_sentCaptionVerschickte Mails
ImageIndex    
TStatusBar	StatusBarLeft Top�Width�HeightPanelsWidth�      