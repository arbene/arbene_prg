unit a_ap_vorlagen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, StdCtrls, DBCtrls, Mask, ExtCtrls, dbtreeview_kaskade,
  Buttons,a_data,db;

type
  TForm_ap_vorlagen = class(TForm)
    ToolBar: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton3: TToolButton;
    ToolButton_change: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_save: TToolButton;
    ToolButton7: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton5: TToolButton;
    ToolButton_abort: TToolButton;
    ToolButton4: TToolButton;
    ToolButton_ok: TToolButton;
    Panel_left: TPanel;
    Panel_such: TPanel;
    Label_suchen: TLabel;
    SpeedButton1: TSpeedButton;
    Edit_such: TEdit;
    dbtreeview: Tdbtreeview_k;
    PageControl: TPageControl;
    Notebook: TNotebook;
    Label2: TLabel;
    DBEdit_ap: TDBEdit;
    Label1: TLabel;
    DBEdit_gef2: TDBEdit;
    Label58: TLabel;
    DBMemo_gef: TDBMemo;
    procedure ToolButton_newClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton_saveClick(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure gef_neu;
    procedure ap_neu;
  public
    { Public-Deklarationen }
  end;

var
  Form_ap_vorlagen: TForm_ap_vorlagen;

implementation
uses  a_main, a_u_wahl;
{$R *.DFM}

procedure TForm_ap_vorlagen.ToolButton_newClick(Sender: TObject);
var
   f_u_wahl:tform_u_wahl;
begin
try
   ToolButton_saveClick(Sender);
   f_u_wahl:=tform_u_wahl.Create(self);
   f_u_wahl.Panel_date.visible:=false;

   f_u_wahl.RadioGroup.Items.Add('Arbeitsplatz');
   f_u_wahl.RadioGroup.Items.Add('Gefährdungsgruppe');

   f_u_wahl.RadioGroup.itemindex:=0;
   if f_u_wahl.ShowModal<>mrok then exit;

   if f_u_wahl.RadioGroup.Itemindex=0 then ap_neu else   gef_neu;


finally
f_u_wahl.release;
end;
end;
procedure TForm_ap_vorlagen.SpeedButton1Click(Sender: TObject);
begin
dbtreeview.suche_datensatz(edit_such.text);
end;

procedure TForm_ap_vorlagen.Edit_suchKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key<>vk_return then exit;
dbtreeview.suche_datensatz(edit_such.text);
end;

procedure TForm_ap_vorlagen.ap_neu;
var nr:int64;
begin
    nr:=neuer_datensatz(datamodul.q_3,['nummer'],[null]);
    dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_3,nr,'',2);
end;

procedure TForm_ap_vorlagen.gef_neu;
var nr:int64;
begin

   nr:=neuer_datensatz(datamodul.q_1,['nummer','i_ap','i_gefaehrdungsfaktor'],[null]);
   if dbtreeview.a_ebene=1 then
    dbtreeview.Selected:=dbtreeview.kind_einfuegen( dbtreeview.akt_knoten[1] ,datamodul.q_1,nr,'',2)
    else
    dbtreeview.Selected:=dbtreeview.parent_einfuegen( dbtreeview.akt_knoten[2] ,datamodul.q_1,nr,'',2);
end;

procedure TForm_ap_vorlagen.ToolButton_saveClick(Sender: TObject);
begin
case dbtreeview.a_ebene of
  1:if datamodul.q_3.state in [dsedit, dsinsert] then
      begin
           datamodul.q_3.post;
           dbtreeview.name_neu(dbtreeview.akt_knoten[dbtreeview.a_ebene]);
      end;
  2:  begin
        if datamodul.q_1.state in [dsedit, dsinsert] then
        begin
         datamodul.q_1.post;
         dbtreeview.name_neu(dbtreeview.akt_knoten[dbtreeview.a_ebene]);
      end;
      end;
  end;
end;

end.
