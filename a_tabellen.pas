unit a_tabellen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids,db, ExtCtrls, DBCtrls, ComCtrls,
  ToolWin,dbtables, Mask, dbgridEXT,a_data, dbcombo_number,zdataset,Variants;

type
	TForm_tabellen = class(TForm)
	  ToolBar: TToolBar;
	  ToolButton_neu: TToolButton;
    space1: TToolButton;
    ToolButton_edit: TToolButton;
    space2: TToolButton;
    ToolButton_save: TToolButton;
    space3: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton_quit: TToolButton;
    Notebook: TNotebook;
    BitBtn_laden: TBitBtn;
    DBImage: TDBImage;
    Label2: TLabel;
    DBEdit_untersuchung: TDBEdit;
    Label3: TLabel;
    DBEdit_frist1: TDBEdit;
    Label4: TLabel;
    DBEdit_frist2: TDBEdit;
    Label5: TLabel;
    DBEdit_status: TDBEdit;
    BitBtn1: TBitBtn;
    DBEdit_bild_beschreibung: TDBEdit;
    BitBtn_color: TBitBtn;
	  Button1: TButton;
    DBEdit_s_name: TDBEdit;
    DBEdit_s_schicht: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Panel_b: TPanel;
	  Label_such: TLabel;
    DBEdit_name: TDBEdit;
    DBEdit_strasse: TDBEdit;
    DBEdit_plz: TDBEdit;
    DBEdit_ort: TDBEdit;
    DBEdit_telefon: TDBEdit;
    DBEdit_fax: TDBEdit;
    DBEdit_email: TDBEdit;
    DBMemo_memo: TDBMemo;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit_www: TDBEdit;
    DBEdit_abfragen_name: TDBEdit;
    DBMemo_abfragen: TDBMemo;
    Label22: TLabel;
    Label23: TLabel;
	  Label24: TLabel;
    DBEdit_s_kuerzel: TDBEdit;
    BitBtn_taetig_lesen: TBitBtn;
    Label25: TLabel;
    Label26: TLabel;
    DBEdit_taetigkeit: TDBEdit;
    DBEdit_einsatzzeit: TDBEdit;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    Label9: TLabel;
    Label21: TLabel;
    Label40: TLabel;
	  DBEdit_goae_nr: TDBEdit;
    DBEdit_goae_nn: TDBEdit;
    DBEdit_goae_punkte: TDBEdit;
    DBRadioGroup_goae_kategorie: TDBRadioGroup;
    Label10: TLabel;
    DBEdit_goae_text: TDBEdit;
    Button_goae: TButton;
    DBEdit_artnr: TDBEdit;
    Label20: TLabel;
    DBEdit_ar_name: TDBEdit;
    Label39: TLabel;
    DBMemo_art: TDBMemo;
    DBCombo_lieferant: TDBCombo_num;
    DBEdit_inhalt: TDBEdit;
    DBEdit_ekp: TDBEdit;
    DBComboBox_einheit: TDBComboBox;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    DBEdit_evkp: TDBEdit;
    Label45: TLabel;
    Label46: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    DBEdit_ean: TDBEdit;
    Label53: TLabel;
    DBEdit_lager: TDBEdit;
    DBEdit_lagermind: TDBEdit;
    DBEdit_lagerbest: TDBEdit;
    DBEdit_mindbest: TDBEdit;
    DBEdit_verpackungseinheit: TDBEdit;
    DBEdit_vkp: TDBEdit;
    Label54: TLabel;
    DBEdit_postfach: TDBEdit;
    Label_kundennummer: TLabel;
    DBEdit_kundennummer: TDBEdit;
    Label47: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    GroupBox_art: TGroupBox;
    CheckBox_frei_artikel: TCheckBox;
    DBEdit_ap_name: TDBEdit;
    Arbeitsplatz: TLabel;
    BitBtn_ap: TBitBtn;
    DBEdit_gef: TDBEdit;
    DBMemo_gef: TDBMemo;
    ToolButton1: TToolButton;
    Label57: TLabel;
    Label58: TLabel;
    ToolButton2: TToolButton;
    Label99: TLabel;
    DBCombo_taet_branche: TDBCombo_num;
    button_zusammenfuehren: TButton;
    Panel1: TPanel;
    DBgrid_tabellen: TDBgridEXT;
    Panel_suchen: TPanel;
    Label207: TLabel;
    Edit_such: TEdit;
    SpeedButton_firma_such: TSpeedButton;
    Panel2: TPanel;
    Label60: TLabel;
    Label_gdtdatei: TLabel;
    DBEdit_gdtprogramm: TDBEdit;
    DBEdit_gdtname: TDBEdit;
    Label63: TLabel;
    DBRadioGroup_gdtparameter: TDBRadioGroup;
    DBEdit_gdtdatei: TDBEdit;
    DBEdit_gdtrueckantwort: TDBEdit;
    Label62: TLabel;
    DBEdit_gdtidemfaenger: TDBEdit;
    Label_idempfaenger: TLabel;
    Label_parameter: TLabel;
//     +     Label_parameter: TLabel;

    DBEdit_gdtparameter: TDBEdit;
    DBEdit_an_name: TDBEdit;
    DBEdit_an_brief: TDBEdit;
    DBEdit_an_dativ: TDBEdit;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Panel3: TPanel;
    DBCombo_num_zeitkonto: TDBCombo_num;
    Label1: TLabel;
    DBEdit_untersucher: TDBEdit;
    SpeedButton_up: TSpeedButton;
    SpeedButton_down: TSpeedButton;
    BitBtn_bwahl: TBitBtn;
    Label69: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Panel4: TPanel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    DBEdit_amb_weiter: TDBEdit;
    Label70: TLabel;
    DBEdit_abschluss_sperrdatum: TDBEdit;
    DBMemo_abschluss_kommentar: TDBMemo;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    DBEdit_abschluss_erstelldatum: TDBEdit;
    Label77: TLabel;
    SpeedButton5: TSpeedButton;
    Label78: TLabel;
    DBRadioGroup_gdt_version: TDBRadioGroup;
    DBCheckBox_gdtwarten: TDBCheckBox;
    DBEdit_untersucher2: TDBEdit;
    DBEdit_untersucher3: TDBEdit;
    DBEdit_untersucher4: TDBEdit;
    DBEdit_untersucher_email: TDBEdit;
    DBEdit_untersucher_telefon: TDBEdit;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    DBComboBox_u1: TDBComboBox;
    DBComboBox_u2: TDBComboBox;
    DBComboBox_u3: TDBComboBox;
    DBComboBox_u4: TDBComboBox;
    DBComboBox_u5: TDBComboBox;
    DBComboBox_u6: TDBComboBox;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    panel_string: TPanel;
    SpeedButton_stringhoch: TSpeedButton;
    SpeedButton_stringrunter: TSpeedButton;
    Label_string: TLabel;
    DBEdit_string: TDBEdit;
    OpenDialog: TOpenDialog;
    Panel5: TPanel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label59: TLabel;
    Label90: TLabel;
    DBEdit_avorname: TDBEdit;
    DBEdit_astrasse: TDBEdit;
    DBEdit_aplz: TDBEdit;
    DBEdit_aort: TDBEdit;
    DBEdit_atelefon: TDBEdit;
    DBEdit_afax: TDBEdit;
    DBEdit_aemail: TDBEdit;
    DBMemo_amemo: TDBMemo;
    DBEdit_awww: TDBEdit;
    DBEdit_aname: TDBEdit;
    DBEdit_atitel: TDBEdit;
    DBEdit_afach: TDBEdit;
    DBEdit_azhd: TDBEdit;
    Panel_VCARD: TPanel;
    BitBtn2: TBitBtn;
    DBMemo_p_name: TDBMemo;
    Panel_zeitkonto: TPanel;
    Label61: TLabel;
    DBEdit_zeitkonto: TDBEdit;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    BitBtn_zeitkonto_kriterien: TBitBtn;
    BitBtn3: TBitBtn;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    procedure ToolButton_neuClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
    procedure ToolButton_quitClick(Sender: TObject);
    procedure BitBtn_bwahl1Click(Sender: TObject);
	  procedure BitBtn_bmpClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn_colorClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DBgrid_tabellenKeyDown(Sender: TObject; var Key: Word;
		 Shift: TShiftState);
    procedure DBgrid_tabellenKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn_taetig_lesenClick(Sender: TObject);
    procedure BitBtn_okClick(Sender: TObject);
    procedure BitBtn_escClick(Sender: TObject);
    procedure DBgrid_tabellenDblClick(Sender: TObject);
    procedure Button_goaeClick(Sender: TObject);
    procedure BitBtn_apClick(Sender: TObject);
    procedure button_zusammenfuehrenClick(Sender: TObject);
    procedure SpeedButton_firma_suchClick(Sender: TObject);
    procedure Edit_suchKeyPress(Sender: TObject; var Key: Char);
    
    procedure dbchange(DataSet: TDataSet);
    procedure db_afterscroll(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SpeedButton_upClick(Sender: TObject);
    procedure SpeedButton_downClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBRadioGroup_gdt_versionChange(Sender: TObject);
    procedure DBEdit_zeitkontoChange(Sender: TObject);
    procedure BitBtn_zeitkonto_kriterienClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
	private
	  { Private-Deklarationen}
	  inidir: string;
	  procedure filtern(wort:string);

       procedure alles_speichern;
       procedure reihenfolge(richtung:integer);
	public
	  { Public-Deklarationen}
	  f_query,suchfeld:string;

	  tabelle: tzquery;
	end;

var
  Form_tabellen: TForm_tabellen;

implementation

uses  a_main, a_bmp_wahl, a_namsuch, a_auswertung, a_arbeitsplatz,
  a_kalender, a_listwahl;

{$R *.DFM}

procedure TForm_tabellen.ToolButton_neuClick(Sender: TObject);
var i_firma,i_branche:integer;
   nr: int64;
   d:string;
begin
	//datamodul.q_1.appendRecord([nil]);
    alles_speichern;

  if Messagedlg('Soll ein neuer Datensatz erstellt werden?',mtConfirmation, [mbOK,mbcancel],0)<>mrok then exit;
  
	i_firma:=datamodul.q_firma['nummer'];
    i_branche:=datamodul.q_firma.findfield('i_branche').asinteger;
    d:=datetostr(now());
	case notebook.PageIndex of
	4: nr:=neuer_datensatz(datamodul.q_1,['nummer'],[null]);
	5: nr:=neuer_datensatz(datamodul.q_1,['nummer','i_firma'],[null,i_firma]); //schicht
	6: nr:=neuer_datensatz(datamodul.q_1,['nummer','i_firma','kuefirma'],[null,i_firma,inttostr(i_firma)]);  //abteilung
	8: nr:=neuer_datensatz(datamodul.q_1,['nummer'],[null]);
	9: nr:=neuer_datensatz(datamodul.q_1,['nummer','i_branche'],[null,i_branche]); //taetigkeit
	11:nr:=neuer_datensatz(datamodul.q_1,['nummer'],[null]); //goae
	12:nr:=neuer_datensatz(datamodul.q_1,['nummer'],[null]); //material
  13:nr:=neuer_datensatz(datamodul.q_1,['nummer','i_firma'],[null,i_firma]); //arbeitsplatz
  14:nr:=neuer_datensatz(datamodul.q_1,['nummer'],[null]); //gef�hrdung
  18:nr:=neuer_datensatz(datamodul.q_1,['nummer','datum','sperrdatum','kommentar'],[null,d,'01.01.2000',' ']); //jahresabschluss)
  20:nr:=neuer_datensatz(datamodul.q_1,['nummer','reihenfolge'],[null,0]);//konto
	else
		nr:=neuer_datensatz(datamodul.q_1,['nummer'],[null]);
	end;


  	datamodul.q_1.post;
    if datamodul.q_1.findfield('reihenfolge')<>nil then
    begin
        datamodul.q_1.edit;
       datamodul.q_1.findfield('reihenfolge').asinteger:=datamodul.q_1.findfield('nummer').asinteger;
       datamodul.q_1.post;
    end;

	form_main.entsperren(notebook);
     toolbutton_neu.enabled:=false;
     toolbutton_del.Enabled:=true;

    toolbutton_edit.enabled:=false;
  	toolbutton_save.enabled:=true;
  

end;

procedure TForm_tabellen.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
DBgrid_tabellen.bmp_Verzeichnis:=bildverzeichnis;
grid_suchen(sender);
//dbcombo_taet_branche.lookup_laden;
//form_main.combolesen( dbcombo_taet_branche,'branche','branche','branche','alle');
//grid_einstellen(dbgrid_tabellen);
//datamodul.q_1.BeforeScroll:=form_tabellen.dbchange;
//datamodul.q_1.afterScroll:=form_tabellen.db_afterscroll;

end;

procedure TForm_tabellen.ToolButton_editClick(Sender: TObject);
begin
//tabelle.edit;
  form_main.entsperren(notebook);
  
  toolbutton_neu.enabled:=false;
  toolbutton_edit.enabled:=false;

  toolbutton_save.enabled:=true;
  case notebook.PageIndex of
    16: toolbutton_del.Enabled:=false;
  else
     toolbutton_del.Enabled:=true;
  end;
end;

procedure TForm_tabellen.ToolButton_saveClick(Sender: TObject);
begin
  alles_speichern;     
end;

procedure tform_tabellen.alles_speichern;
begin
//direktspeichern:=true;
  datamodul.sql_post(datamodul.q_1);
  form_main.sperren(Form_tabellen.notebook);

  toolbutton_edit.enabled:=true;

  toolbutton_del.Enabled:=false;
  toolbutton_save.enabled:=false;

  case notebook.PageIndex of
    16: toolbutton_neu.enabled:=false;
  else
    toolbutton_neu.enabled:=true;
  end;
end;

procedure TForm_tabellen.ToolButton_delClick(Sender: TObject);
begin

	dateiloeschen(datamodul.q_1,false,false);
end;


procedure TForm_tabellen.ToolButton_quitClick(Sender: TObject);
begin
if datamodul.q_1.state in [dsedit, dsinsert] then datamodul.q_1.post;
form_main.sperren(notebook);
modalresult:=mrok;
end;

procedure TForm_tabellen.BitBtn_bwahl1Click(Sender: TObject);

begin
   if inidir<>null then  form_main.OpenpictureDialog.InitialDir:=inidir;
   form_main.OpenpictureDialog.DefaultExt:='bmp';
	 if not form_main.OpenpictureDialog.Execute then exit;
   datamodul.q_1.Edit;
   dbimage.picture.Bitmap.LoadFromFile(form_main.OpenpictureDialog.FileName) ;
   //dbimage.Picture.LoadFromFile(form_main.OpenpictureDialog.FileName);
   datamodul.sql_post(datamodul.q_1);
end;

procedure TForm_tabellen.BitBtn_bmpClick(Sender: TObject);
begin
	form_bmp_wahl:=Tform_bmp_wahl.create(self);
	datamodul.sql(false,datamodul.q_2,'select * from bitmaps','');
	form_bmp_wahl.DBgridEXT1.DataSource:=datamodul.ds_2;
	form_bmp_wahl.DBGridext1.Columns[0].fieldname:='BMP';
	form_bmp_wahl.DBGridext1.Columns[0].width:=34;
	form_bmp_wahl.DBGridext1.Columns[1].fieldname:='Beschreibung';
	form_bmp_wahl.DBGridext1.Columns[1].width:=64;
	if form_bmp_wahl.showmodal=mrok then
	begin
		datamodul.q_1.edit;
		datamodul.q_1['bmp_nr']:=datamodul.q_2['nummer'];
     datamodul.q_1.post;
   end;

   form_bmp_wahl.release;
end;

procedure TForm_tabellen.FormShow(Sender: TObject);
begin
  form_main.form_positionieren(tform(sender));
  //if f_query='' then  edit_such.visible:=false;
  if datamodul.q_1.findfield('reihenfolge')<>nil then
  begin
  	speedbutton_up.Visible:=true;
    speedbutton_down.Visible:=true;
  end;
  form_main.sperren(notebook);
  toolbutton_edit.enabled:=true;
  toolbutton_del.Enabled:=false;
  toolbutton_save.enabled:=false;

  datamodul.q_1.BeforeScroll:=form_tabellen.dbchange;

  case notebook.PageIndex of
    16: toolbutton_neu.enabled:=false;
    20: datamodul.q_1.afterScroll:=form_tabellen.db_afterscroll;
  else
    toolbutton_neu.enabled:=true;
  end;
  db_afterscroll(datamodul.q_1);

end;

procedure TForm_tabellen.BitBtn_colorClick(Sender: TObject);
begin
if not form_main.colordialog.Execute then exit;
	datamodul.q_1.edit;
	datamodul.q_1['farbe']:=form_main.colordialog.Color;
	datamodul.q_1.post;
end;

procedure TForm_tabellen.Button1Click(Sender: TObject);
begin
	datamodul.q_1.edit;
	datamodul.q_1['farbe']:=0;
	datamodul.q_1.post;
end;



procedure TForm_tabellen.DBgrid_tabellenKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
label_such.caption:=DBgrid_tabellen.eingabetext;
end;

procedure TForm_tabellen.DBgrid_tabellenKeyPress(Sender: TObject; var Key: Char);
var ordkey: integer;
begin
ordkey:=ord(key);
case
ordkey of 48..256 :
  begin
	label_such.caption:=DBgrid_tabellen.eingabetext;
	//datamodul.q_1.first;DBgrid_tabellen.eingabetext
	if datamodul.q_1.locate(suchfeld,DBgrid_tabellen.eingabetext,[loCaseInsensitive,loPartialKey]) then
  begin
		dbgrid_tabellen.SetFocus;
     dbgrid_tabellen.Refresh;
  end;
  end;
end;

end;

procedure TForm_tabellen.BitBtn_taetig_lesenClick(Sender: TObject);
var i_branche:integer;
begin
with datamodul do
begin
	i_branche:= getbigint(q_firma,'i_branche');

  sql(true,q_2,format(' select taetigkeit from mitarbeiter left join firma on (mitarbeiter.i_firma = firma.nummer) where firma.i_branche=%d order by taetigkeit',[i_branche]),'');
	//sql(true,q_2,format('select taetigkeit from mitarbeiter where i_branche=%d order by taetigkeit',[i_branche]),'');
  q_2.first;
  while not q_2.eof do
  begin
   if q_2['taetigkeit']<>null then
   if not q_1.Locate('taetigkeit',q_2['taetigkeit'],[loCaseInsensitive]) then
   begin
   	 neuer_datensatz(q_1,['nummer','i_branche','taetigkeit'],[null,i_branche,q_2['taetigkeit']]);
      q_1.post;
   end;
   q_2.next;
  end;
  q_1.refresh;
end;
end;


procedure TForm_tabellen.BitBtn_okClick(Sender: TObject);
begin
datamodul.sql_post(datamodul.q_1);
modalresult:=mrok;
end;

procedure TForm_tabellen.BitBtn_escClick(Sender: TObject);
begin
datamodul.sql_post(datamodul.q_1);
modalresult:=mrabort;
end;

procedure TForm_tabellen.DBgrid_tabellenDblClick(Sender: TObject);
begin
	 BitBtn_okClick(Sender);
end;



procedure TForm_tabellen.Button_goaeClick(Sender: TObject);
//goae.nummer sollte besser goae.goae heissen Probelme mit neuer_datensatz nummer=0 wird gel�scht
//20101227 hieisst jetzt goa_nummer
var
goae,query:string;

begin
with datamodul do
begin
	query:='select untersuchung, goae from typ where (goae<>"0" and goae<>"")';
	sql(false,q_3,query,'');
	q_3.First;
	while not q_3.Eof do
	begin
		goae:=q_3['goae'];
		if not q_1.Locate('goae_nummer',goae,[loCaseInsensitive])
		//then q_1.AppendRecord([goae,q_3['untersuchung']]);
		then neuer_datensatz(datamodul.q_1,['goae_nummer','goae_text' ],[goae,q_3.findfield('untersuchung').asstring]); //goae
		q_3.next;
	end;
	query:='select name, goae from bereich where (goae<>"0" and goae<>"") ';
	sql(false,q_3,query,'');
	q_3.First;
	while not q_3.Eof do
	begin
		goae:=q_3['goae'];
		if not q_1.Locate('goae_nummer',goae,[loCaseInsensitive])
		//then q_1.AppendRecord([goae,q_3['name']]);
		then neuer_datensatz(datamodul.q_1,['goae_nummer','goae_text' ],[goae,q_3.findfield('name').asstring]); //goae
		q_3.next;
	end;
	if q_1.state in [dsinsert, dsedit] then q_1.post;

	q_1.refresh;
end;
end;

procedure TForm_tabellen.BitBtn_apClick(Sender: TObject);
begin
     //form_ap:=tform_ap.create(self);
try
  // form_ap.ini(getbigint_str(datamodul.q_1,'nummer'));
  // form_ap.ShowModal;
finally
   //form_ap.release;
end;
end;

procedure tform_tabellen.filtern(wort:string);
var q,tab,sf1,sf:string;
  p:integer;
begin
tab:=tabellen_name(datamodul.q_1.sql.text);
sf:=suchfeld;
if wort<>'' then
begin
  wort:='"%'+wort+'%"';
  q:='';
  p:=pos(',',sf);
  while p>0 do
  begin
    sf1:=copy(sf,0,p-1);
    sf:=copy(sf,p+1,length(sf));
    p:=pos(',',sf);
    if q<>'' then q:=q +' or ';
    q:=q+ format(' %s like %s ',[sf1,wort]);
  end;
    if q<>'' then q:=q +' or ';
    q:=q+ format(' %s like %s ',[sf,wort]);
    q:=format('select * from %s where ',[tab])+q;

	//q:=format('select * from %s where %s like %s',[tab,suchfeld,wort]);

end
else
	q:=format('select * from %s ',[tab]);

datamodul.sql_new(false,datamodul.q_1,q,suchfeld);
end;

procedure TForm_tabellen.button_zusammenfuehrenClick(Sender: TObject);
//gleiche namen m�ssen untereinander stehen
//wenn zwei aufeinanderfolgende gleich sind und auch die Branche gleich ist, werden sie ineinander �berf�hrt
var
t1,t2:string;
query:string;
i1,i2:string;
i_b1,i_b2:integer;
begin
with datamodul do
begin
     q_1.Refresh;
     q_1.first;
     t1:=trim(lowercase(q_1.findfield('taetigkeit').asstring));
	  i1:=getbigint_str(q_1,'nummer');
     i_b1:=q_1.findfield('i_branche').asinteger;
     q_1.next;
     while not q_1.eof do
     begin
        t2:=trim(lowercase(q_1.findfield('taetigkeit').asstring));
        i2:=getbigint_str(q_1,'nummer');;
        i_b2:=q_1.findfield('i_branche').asinteger;
        if (t1=t2) and (i_b1=i_b2) then
        begin
          query:=format('update mitarbeiter set i_taetigkeit=%s where i_taetigkeit=%s',[i1,i2]);
          mysql_d.sql_exe(query);
          q_1.edit;
          q_1.findfield('storno').asinteger:=1;
          q_1.post;
        end
        else
        begin
             t1:=t2;
             i1:=i2;
             i_b1:=i_b2;
        end;
        q_1.next;
     end;
     q_1.refresh;
end;
end;

procedure TForm_tabellen.SpeedButton_firma_suchClick(Sender: TObject);
begin
filtern(edit_such.text);
end;

procedure TForm_tabellen.Edit_suchKeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then  filtern(edit_such.text);
end;

procedure TForm_tabellen.dbchange(DataSet: TDataSet);
begin
	 Form_tabellen.alles_speichern;
end;

procedure TForm_tabellen.db_afterscroll(DataSet: TDataSet);
var
query,snr:string;
begin
snr:=datamodul.q_1.findfield('nummer').AsString;
case notebook.PageIndex of
20:  if snr<>'' then
      begin
        query:=format('select abr_anlass_krit.*,abr_konto_kriterium.nummer as kk_nummer  from abr_anlass_krit left join abr_konto_kriterium on (abr_anlass_krit.nummer = abr_konto_kriterium.i_kriterium ) where abr_konto_kriterium.i_konto=%s and abr_konto_kriterium.storno=0',[snr]);
        datamodul.sql(false,datamodul.q_4,query,'');
      end;

end;
end;


procedure TForm_tabellen.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	datamodul.q_1.BeforeScroll:=nil;
end;

procedure TForm_tabellen.SpeedButton_upClick(Sender: TObject);
begin
reihenfolge(0);
end;

procedure Tform_tabellen.reihenfolge(richtung:integer);
var
reihenfolge1,reihenfolge2:integer;
nummer1,i:integer;
begin
with datamodul do
begin

	if (richtung=0) and (q_1.Bof) then exit;
	if (richtung=1) and (q_1.eof) then exit;
	nummer1:=q_1.findfield('nummer').asinteger;
   i:=1;
  q_1.DisableControls;
  q_1.First;
  while not q_1.Eof do
  begin
     q_1.edit;
     q_1.findfield('reihenfolge').asinteger:=i;
     q_1.post;
     inc(i);
     q_1.Next;
  end;
  q_1.locate('nummer',nummer1,[]);
  q_1.EnableControls;


  reihenfolge1:=q_1.findfield('reihenfolge').asinteger;
    if reihenfolge1=0 then
    begin
       q_1.edit;
       q_1.findfield('reihenfolge').asinteger:=nummer1;
       q_1.post;
       q_1.refresh;
	   q_1.Locate( 'nummer', nummer1,[]);
       exit;
    end;
	if (richtung=0) then q_1.Prior else q_1.Next;

	reihenfolge2:=q_1.findfield('reihenfolge').asinteger;
    if reihenfolge2=reihenfolge1 then reihenfolge2:=reihenfolge1+1;
	q_1.edit;
	q_1.FindField('reihenfolge').AsInteger:=reihenfolge1;
	q_1.post;
	//q_berlist.first;
	q_1.locate('nummer',nummer1,[]);
	q_1.edit;
	q_1.FindField('reihenfolge').asinteger:=reihenfolge2;
	q_1.post;
	q_1.refresh;
	q_1.Locate( 'nummer', nummer1,[]);
end;
end;


procedure TForm_tabellen.SpeedButton_downClick(Sender: TObject);
begin
reihenfolge(1);
end;

procedure TForm_tabellen.SpeedButton5Click(Sender: TObject);
var dat:real;
begin
if form_kalender.showmodal =mrok then
	begin
    dat:=(form_kalender.auswahlkalender.date );
     if dat> now()-30 then
     begin
      showmessage('Das Sperrdatum muss mindestens 30 Tage in der Vergangenheit liegen!');
     end
     else
     begin
       if not (datamodul.q_1.state in [dsedit, dsinsert])  then  datamodul.q_1.Edit;
       datamodul.q_1['sperrdatum'] :=(form_kalender.auswahlkalender.date);
     end;
  end;

end;

procedure TForm_tabellen.BitBtn2Click(Sender: TObject);
var
liste,zeile:tstringlist;
dat,l,a,b:string;
i,p,t,e,u,ad,n,no:integer;
begin
  liste:=nil;
  zeile:=nil;
  OpenDialog.execute;
  dat:=opendialog.FileName;

if not fileexists(dat) then
  begin
    showmessage('Die Datei "'+dat+'" ist nicht vorhanden');
    exit;
  end;

try
  zeile:=tstringlist.create;
  liste:=tstringlist.Create;
  liste.LoadFromFile(dat);
   datamodul.q_1.edit;
   t:=0;
   e:=0;
   u:=0;
   ad:=0;
   n:=0;
   no:=0;
  for i:=0 to liste.Count-1 do
  begin
    p:=pos(':',liste[i]);
    a:=liste[i];
    b:=copy(liste[i],p+1,length(liste[i]));
    explode( b,';',zeile);

    if (copy(a,1,2)='N;') or (copy(a,1,2)='N:') then
    begin
       if n=0 then
       try
        dbedit_aname.Text:= zeile[0];
        dbedit_avorname.Text:=zeile[1];
        if zeile[2]<>'' then dbedit_avorname.Text:=dbedit_avorname.Text+' '+zeile[2];
        dbedit_atitel.Text:=zeile[3];
       except
       end
       else dbmemo_amemo.Lines.Add(a);
       inc(n);
    end;

    if copy(a,1,3)='TEL'  then
    begin
       try
          if t=0 then dbedit_atelefon.Text:= zeile[0];
          if t=1 then dbedit_afax.text:=zeile[0];
          if t>1 then dbmemo_amemo.Lines.Add(a);
          inc(t);
       except
       end;
    end;

    if copy(a,1,3)='ADR'  then
    begin
         if ad=0 then
         try
          dbedit_astrasse.Text:=zeile[2];
          dbedit_aort.Text:=zeile[3];
          dbedit_aplz.Text:=zeile[5];
        except
       end
       else dbmemo_amemo.Lines.Add(a);
       inc(ad);
    end;

    if copy(a,1,3)='EMAIL'  then
    begin
      try
      if e=0 then dbedit_aemail.Text:=zeile[0]
      else dbmemo_amemo.Lines.Add(a);
      inc(e);
      except
       end;

    end;

    if copy(a,1,3)='NOTE'  then
    begin
    try
      if no=0 then dbmemo_amemo.Text:=zeile[0]
      else dbmemo_amemo.Lines.Add(a);
      inc(no);
      except
       end;

    end;

    if copy(a,1,3)='URL'  then
    begin
      try
      if u=0 then dbedit_awww.Text:=zeile[0]
      else dbmemo_amemo.Lines.Add(a); 
      inc(u);
      except
       end;

    end;
  end;


finally
  liste.Free;
  zeile.Free;
end;
end;

procedure TForm_tabellen.DBRadioGroup_gdt_versionChange(Sender: TObject);
begin
 case  DBRadioGroup_gdt_version.ItemIndex of
    0,1:
     begin
       Label_idempfaenger.Caption:='ID des Empf�ngers';
       Label_gdtdatei.Visible:=true;
       DBEdit_gdtdatei.Visible:=true;
       DBRadioGroup_gdtparameter.Visible:=true;
     end;
     2:
     begin
       Label_idempfaenger.Caption:='Pfad f�r die PDF-Vorlagen';
       Label_gdtdatei.Visible:=false;
       DBEdit_gdtdatei.Visible:=false;
       DBRadioGroup_gdtparameter.Visible:=false;
     end;
 end;
end;

procedure TForm_tabellen.DBEdit_zeitkontoChange(Sender: TObject);
begin;
end;




procedure TForm_tabellen.BitBtn_zeitkonto_kriterienClick(Sender: TObject);
var
query,s_krit_nummer,s_konto_nummer:string;
begin
try

   with datamodul do
	 begin
    	query:='select nummer,name,reihenfolge from abr_anlass_krit where storno =0 order by reihenfolge';

	   sql(false,q_7,query,'');
		 form_liste:=tform_liste.create(self);

		 form_liste.dbgridext.DataSource:=ds_7;
		 form_liste.s_name:='name';
		 form_liste.DBgridEXT.Columns[0].fieldname:='name';
		 //form_liste.DBgridEXT.Options:=form_liste.DBgridEXT.Options - [dgMultiSelect]	 ;

     form_liste.toolbar.Visible:=true;
     form_liste.Panel_b.Visible:=false;
     form_liste.Height:=form_tabellen.Height;
		 if form_liste.showmodal=mrok then
       begin
       	s_krit_nummer:=datamodul.q_7.findfield('nummer').asstring;
        s_konto_nummer:=datamodul.q_1.findfield('nummer').asstring;
        query:=format('select nummer from abr_konto_kriterium where i_konto=%s and i_kriterium=%s',[s_konto_nummer,s_krit_nummer]);
        if mysql_d.Feldinhalt(query,0)='' then
        begin
            query:=format ('insert abr_konto_kriterium  set i_kriterium="%s", i_konto="%s" ',[s_krit_nummer,s_konto_nummer]);
            mysql_d.sql_exe(query);
            datamodul.q_4.Refresh;
        end
        else  //schon vorhanden aber storno
        begin
          query:=format('update abr_konto_kriterium set storno=0 where nummer=%s',[s_krit_nummer]);
          mysql_d.sql_exe(query);
          datamodul.q_4.Refresh;
        end;
       end;
	 end;
finally
	form_liste.release;

end;
end;

procedure TForm_tabellen.BitBtn3Click(Sender: TObject);
var
query,s_nummer,s_konto_nummer,s_krit_nummer:string;
begin
  if (Messagedlg('Soll das Kriterium aus der Liste entfernt werden?',mtConfirmation, mbOkCancel	,0)<>mrOK ) then  exit;
 //s_krit_nummer:=datamodul.q_7.findfield('nummer').asstring;
  //s_konto_nummer:=datamodul.q_1.findfield('nummer').asstring;
  s_nummer:=datamodul.q_4.findfield('kk_nummer').asstring;
  if s_nummer<>'' then
  begin
    query:=format('update abr_konto_kriterium set storno=1 where nummer=%s  ',[s_nummer]);
    mysql_d.sql_exe(query);
    datamodul.q_4.Refresh;
  end;
end;

procedure TForm_tabellen.FormDestroy(Sender: TObject);
begin
  datamodul.q_1.BeforeScroll:=nil;
  datamodul.q_1.afterScroll:=nil;
end;

end.

