unit icd_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, 
  StdCtrls, Buttons,registry,math, ZConnection, ExtCtrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset;

type
  TForm_main = class(TForm)
    ds_1: TDataSource;
    OpenDialog: TOpenDialog;
    Edit_file_1: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn_konvert: TBitBtn;
    Label_fortschritt: TLabel;
    ZConnection: TZConnection;
    q_1: TZQuery;
    q_2: TZQuery;
    ds_2: TDataSource;
    RadioGroup: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn_konvertClick(Sender: TObject);
  private
    { Private-Deklarationen }
   db_host,db_login,db_port,db_datenbank,db_passwort, pfad_temp:string;
   infile:textfile;

   procedure konvertieren;

   public
    { Public-Deklarationen }
  end;

var
	Form_main: TForm_main;

implementation

{$R *.DFM}







procedure TForm_main.BitBtn1Click(Sender: TObject);
begin
  if opendialog.execute then
  begin
  	edit_file_1.text:=form_main.opendialog.FileName;
  end;
end;

procedure TForm_main.BitBtn_konvertClick(Sender: TObject);
begin
konvertieren;
end;

procedure TForm_main.konvertieren;
var
zaehler,ebene,nr,p:integer;
zeile,zk,schluessel,sternnummer,zusatznummer, text:string;
a:boolean;





function folgezeile:string;

begin
     result:='';
     readln(infile,zeile);
     if (trim(copy(zeile,1,2))='') and (not eof(infile)) then
     result:=' '+copy(zeile,13,length(zeile))+folgezeile;

end;

function wort:string;
var
p:integer;
begin
p:=pos(';',zeile);
if p>0 then
begin
     result:=copy(zeile,1,p-1);
     zeile:=copy(zeile,p+1,length(zeile));
end
else wort:=zeile;

end;
//######################################
begin
try
   assignfile(infile,edit_file_1.text);
   reset(infile);

   zaehler:=0;

   readln(infile,zeile);
   while not eof(infile) do
   begin

     zk:=copy(zeile,1,2);
     a:=false;
     schluessel:='';
     sternnummer:='';
     zusatznummer:='';
     ebene:=0;
     case radiogroup.ItemIndex of
     0: begin
        if zk='0T' then
           begin
                readln(infile,zeile);
                text:=copy(zeile,4,length(zeile));

                text:=text+folgezeile;

                //readln(infile,zeile);

                schluessel:=copy(zeile,5,7);
                schluessel:=stringreplace(schluessel,')','',[rfReplaceAll]);
                ebene:=1;
                a:=true;
                readln(infile,zeile);
           end;
         if zk='1T' then
           begin
                text:=copy(zeile,4,length(zeile));
                text:=text+folgezeile;
                //readln(infile,zeile);
                schluessel:=copy(zeile,5,7);
                schluessel:=stringreplace(schluessel,')','',[rfReplaceAll]);
                ebene:=2;
                a:=true;
                readln(infile,zeile);
           end;
         if zk='3T' then
           begin
                text:=copy(zeile,13,length(zeile));
                schluessel:=copy(zeile,4,3);
                text:=text+folgezeile;
                ebene:=3;
                a:=true;
           end;
          if pos(zk,'0T1T3T')=0 then readln(infile,zeile);
        end;
     1: begin
         wort;
         wort;
         wort;
         schluessel:=wort;
         sternnummer:=wort;
         zusatznummer:=wort;
         text:=wort;
         ebene:=4;
         readln(infile,zeile);
        a:= schluessel<>'' ;
        end;
     end;
     if a then
     begin
        if not q_1.Locate( 'icd_schluessel,sternnummer,zusatznummer,icd_text,icd_ebene',vararrayof([schluessel,sternnummer,zusatznummer,text,ebene]),[loCaseInsensitive	])
        then
        q_1.AppendRecord([null,schluessel,sternnummer,zusatznummer,text,ebene]);

          { f�r icd und icd thesaurus
          if not q_1.Locate( 'primaerschluessel,sternnummer,zusatznummer,ebene',vararrayof([schluessel,sternnummer,zusatznummer,ebene]),[loCaseInsensitive	])
          then
          begin
               q_1.AppendRecord([null,schluessel,sternnummer,zusatznummer,ebene]);
               //q_1.Post;

          end;
          nr:=q_1.findfield('nummer').asinteger;
          if not q_2.Locate('i_icd,icd_text',vararrayof([nr,text]),[loCaseInsensitive ]) then
          begin
               q_2.AppendRecord([null,nr,text]);
               //q_2.Post;
          end;}

     end;
     label_fortschritt.Caption:= inttostr(zaehler);
     inc(zaehler);
     application.ProcessMessages;
   end;
finally
 closefile(infile);
end;
end;


end.
