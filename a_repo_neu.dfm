�
 TFORM_REPO_NEU 0�  TPF0TForm_repo_neuForm_repo_neuLeft�TopWidth�Height�Caption"   Rechnungsposition neu / verändernColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TSpeedButtonSpeedButton_besonderheitenLeft� Top7WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton_besonderheitenClick  TLabelLabel1LeftrTop<WidthHeightCaptionDatum  TLabelLabel2LeftHTopjWidthHHeightCaptionRechnungstext  TLabelLabel_preisLeft^Top� Width2HeightCaptionEinzelpreis  TLabelLabel3Left� Top� Width	HeightCaption     �   TLabelLabel4Left� Top� WidthHeightCaptionFaktor  TLabelLabel5Left�TopjWidthHeightCaption   GOÄ  TLabelLabel6Left"Top6Width� HeightCaption!   Achtung, nicht neben GOÄ-Ziffern  TLabelLabel7LeftgTop6WidthAHeightCaptionabzurechnen.  TLabelLabel8Left#Top� WidthiHeightCaptionzugeordneter Proband  TLabelLabel9Left�Top� WidthHeightCaptionUSt  TLabelLabel10LeftTop� WidthHeightCaption%  TPanelPanel_bLeft TopZWidthtHeight%AlignalBottom
BevelOuterbvNoneTabOrder TBitBtn	BitBtn_okLeft� TopWidth� HeightCaptionOKDefault	TabOrder OnClickBitBtn_okClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtn
BitBtn_escLeft�TopWidth� HeightCaptionAbbruchModalResultTabOrder
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs   	TMaskEditMaskEdit_datLeft� Top8WidthBHeightEditMask!99/99/9999;1;_	MaxLength
ReadOnly	TabOrder Text
  .  .      TEdit	Edit_reteLeft� TopfWidth� HeightTabOrder  TEdit_waehrung
Edit_preisLeft� Top� WidthBHeightTabOrder  TEdit_waehrungEdit_faktorLeftTop� Width#HeightTabOrderText1  TBitBtnBitBtn_listeLefttTopdWidth� HeightCaptionAuswahl Untersuchungs-BefundeTabOrderOnClickBitBtn_listeClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 033333337�333333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337��?���3   0 33www7w33�33333333333�33333s�s3333303333337s333333	NumGlyphs  TEdit	Edit_goaeLeft�TopfWidthQHeightTabOrder  TEditEdit_goae_nnLeft� Top2Width� Height
BevelInnerbvNone
BevelOuterbvNoneColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.Color
clMenuTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder
  TBitBtnBitBtn_goaeLefttTop� Width� HeightCaption   Auswahl GOÄ-ListeTabOrderOnClickBitBtn_goaeClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 033333337�333333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337��?���3   0 33www7w33�33333333333�33333s�s3333303333337s333333	NumGlyphs  TEditEdit_probandLeft� Top� Width�HeightReadOnly	TabOrder	  TBitBtnBitBtn_probandLefttTop� Width� HeightCaptionAuswahl ProbandTabOrderOnClickBitBtn_probandClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 033333337�333333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337��?���3   0 33www7w33�33333333333�33333s�s3333303333337s333333	NumGlyphs  TBitBtnBitBtn_zuordnung_wegLefttTop� Width� HeightCaptionZuordnung Proband entfernenTabOrderOnClickBitBtn_zuordnung_wegClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333333���33339��33333�ww��339����3337w37w�33�333��33w�337�9��339�37w�33w�99�333�37�w�337��3��339�w�w�37�39�339�w�7w�37�33��39�w�3w�7�339�39�w�37w�7�333��9�w�33w�s9�339�937?�37w��9�333��37s�33ww33��339�33w?���s339����3337w37w33339��333337ww333	NumGlyphs  	TComboBoxComboBox_ustLeft�Top� Width5Height
ItemHeightTabOrderText0   