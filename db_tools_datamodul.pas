unit db_tools_datamodul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZSqlProcessor, ZConnection, DB, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, ZAbstractConnection;

type
  TDataModule1 = class(TDataModule)
    ZConnection_db: TZConnection;
    ZSQLProcessor_db: TZSQLProcessor;
    DataSource_trigger: TDataSource;
    ZQuery_trigger: TZQuery;
    ZConnection1: TZConnection;
    procedure DataModule1Create(Sender: TObject);
    procedure DataModule1Destroy(Sender: TObject);
    procedure connect;
    procedure disconnect;
    procedure ZConnection_dbAfterConnect(Sender: TObject);

  private
    { Private-Deklarationen }
    function ZLoadLibrary_test(Location: String): Boolean;
  public
    { Public-Deklarationen }
  end;

 { controls_cp=GET_ACP
AutoEncodeStrings=ON
}

var
  DataModule1: TDataModule1;

implementation

uses abackup_main;

{$R *.DFM}

procedure TDataModule1.DataModule1Create(Sender: TObject);
begin
  //connect;
  aform_main.verbinden_mit_db;
end;

procedure TDataModule1.DataModule1Destroy(Sender: TObject);
begin
 disconnect;

end;

{procedure TDataModule1.connect;
begin
  datamodule1.ZConnection1.Connect;
  	  datamodule1.ZConnection_db.Connect;
end;}

procedure TDataModule1.connect;
begin
 try
 datamodule1.ZConnection1.Connect;
    aform_main.showserver;
    aform_main.StatusBar.panels[2].text:='Host: '+aform_main.edit_host.text;
	  //datamodule1.ZConnection_db.Database :=aform_main.edit_db.text;
	  //datamodule1.ZConnection_db.HostName:= aform_main.edit_host.text;
	  datamodule1.ZConnection_db.Connect;
    //aform_main.StatusBar.panels[3].text:='Datenbank: '+aform_main.edit_db.text;

except
	begin

  	showmessage('keine Verbindung zur Datenbank. Starten Sie gegebenenfalls die Datenbankengine oder w�hlen Sie eine DB aus.');
    aform_main.StatusBar.panels[3].text:='Datenbank: '+'keine Verbindung';
    application.BringToFront;
    application.ProcessMessages;
    
    end;
end;
end;

procedure TDataModule1.disconnect;
begin
  datamodule1.ZConnection_db.disConnect;
end;

function TDataModule1.ZLoadLibrary_test(Location: String): Boolean;
begin

end;

procedure TDataModule1.ZConnection_dbAfterConnect(Sender: TObject);
begin
  //showmessage('Verbunden mit der Datenbank: '+zconnection_db.Database);
  aform_main.StatusBar.panels[3].text:='Datenbank: '+zconnection_db.Database;
end;

end.
