unit a_uebernahme_jahr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Spin, Mask;

type
  TForm_uebername_jahr = class(TForm)
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    SpinEdit_y: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    SpeedButton_von: TSpeedButton;
    SpeedButton_bis: TSpeedButton;
    MaskEdit_von: TMaskEdit;
    MaskEdit_bis: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton_bisClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_uebername_jahr: TForm_uebername_jahr;

implementation

uses a_main, a_kalender;

{$R *.dfm}

procedure TForm_uebername_jahr.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

procedure TForm_uebername_jahr.SpeedButton_bisClick(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
 begin
	if sender =speedbutton_von then  maskedit_von.Text:=form_kalender.MaskEdit1.text
	else maskedit_bis.Text:=form_kalender.MaskEdit1.text;
 end;
end;

end.
