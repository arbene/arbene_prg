{================ BSSGrid DISTRIBUTION NOTES ============================================}


{BSSGrid is a simple, but very effective TStringGRID descendant that can display
each cell in the grid with either a raised, or lowered, 3-D appearance; in addition
to the usual 'flat' appearance. }

{COPYRIGHT NOTICE}
{BSSGrid is distributed as FREEWARE, but remains the COPYRIGHT of
BUSINESS SOFTWARE (UK) (email  ebinfo@compuserve.com ). Business Software grants you the right
to include this compiled component in your DELPHI application, whether COMMERCIAL, SHAREWARE, or
FREEWARE, BUT YOU MAY NOT DISTRIBUTE THIS SOURCE CODE OR ITS COMPILED .DCU  IN ANY FORM OTHER
THAN AS IT EXISTS HERE; COMPLETE WITH THIS NOTICE AND ALL THE TEXT BELOW. BSSGrid may be included
in any shareware or freeware libraries or compilation disks, provided no charge other than the
usual media cost recovery is made.}

{IF YOU HAVE ANY DOUBTS ABOUT WHETHER YOU MAY LEGALLY USE OR DISTRIBUTE THIS COMPONENT,
CONTACT BUSINESS SOFTWARE BY E-MAIL.}

{VISIT BUSINESS SOFTWARE'S WEB SITE AT HTTP://OURWORLD.COMPUSERVE.COM/HOMEPAGES/EBINFO/  for
more interesting components and applications}

{WARRANTY / ACCEPTANCE OF LIABILITY / INDEMNITY}
{ABSOLUTELY NONE WHATSOEVER}


{INSTALLATION}
{1.Copy this file into your DELPHI/LIB directory, or wherever your library files are kept}
{2.Select OPTIONS|INSTALL COMPONENTS|ADD...}
{3.ADD This file; BSSGrid.PAS to the INSTALLED UNITS listbox}
{4.Click OK}

{USING BSSGrid}
{In order to set the effects, a new PROPERTY 'Cellstyle', is published. Cellstyle may
have one of three possible values: csNONE (default), csRAISED, and csLOWERED.
Cellstyle may be set at design time, and will have a global effect on the Grid display,
however, a much more interesting effect can be achieved if the property is altered dynamically
at run time.

For example; to LOWER any cells that contain a numerical value less than zero, and RAISE
any cells with a value greater than 100: in the 'ondrawcell' event handler,
add the code to raise or lower the cell.
e.g.

procedure TForm1.BSSGrid1DrawCell(Sender: TObject; Col, Row: Longint;
  Rect: TRect; State: TGridDrawState);
var
x:longint;

begin
bssgrid1.cellstyle:=csNone;
x:=strtointdef(bssgrid1.cells[col,row],0);
if x<0 then bssgrid1.cellstyle:=csraised;
if x>100 then bssgrid1.cellstyle:=cslowered;
end;



{=============== END OF DISTRIBUTION NOTES ===================================================}

unit BSSGrid;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Grids, DBGrids;

type
Tcellstyle=(csRaised,csLowered,csNone);

type
  TBSSGrid = class(TStringGrid)
  private
    { Private declarations }
    Fcellstyle:Tcellstyle;
    procedure setcellstyle(value:Tcellstyle);
  protected
    { Protected declarations }
    procedure DrawCell(ACol, ARow: Longint;ARect: TRect; AState: TGridDrawState); override;
  public
    { Public declarations }
    gridrow:longint;
    constructor Create(AOwner : TComponent); override;
    destructor destroy; override;
  published
    { Published declarations }
    property Cellstyle:tcellstyle read fcellstyle write setcellstyle;
  end;

procedure Register;

implementation

constructor TBSSGrid.Create(AOwner : TComponent);
begin
inherited Create(AOwner);
color:=clSilver;
cellstyle:=csnone;
end;

procedure TBSSGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
var p1,p2:Tcolor;

procedure drawrect;
begin
with ARect do
  begin
  Canvas.Pen.Color :=p1 ;
  Canvas.PolyLine([Point(Left+1, Bottom - 1), Point(Left+1, Top), Point(Right, Top)]);
  Canvas.Pen.Color :=p2;
  Canvas.PolyLine([Point(Left+1, Bottom - 1), Point(right,bottom-1), Point(Right, Top)]);
  end;
end;

procedure cellraised;
begin
p1:=clWindow;
p2:=clBtnShadow;
drawrect;
end;

procedure celllowered;
begin
p2:=clBtnShadow;
p1:=clWindow;
drawrect;
end;

begin
inherited DrawCell(ACol, ARow,ARect,AState);
gridrow:=row;
  case fcellstyle of
  csraised:       cellraised;
  cslowered:      celllowered;
  end
end;


procedure TBSSGrid.Setcellstyle(Value:Tcellstyle);
begin
if (fcellstyle<>value) then fcellstyle:=value;
if (csDesigning in componentstate) then invalidate;
end;


destructor TBSSGrid.destroy;
begin
inherited destroy;
end;



procedure Register;
begin
RegisterComponents('BSoft', [TBSSGrid]);
end;

end.
