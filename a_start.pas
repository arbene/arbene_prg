unit a_start;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ExtCtrls, StdCtrls, Buttons, a_main,a_data, ComCtrls,db, jpeg,shellapi;


type
	TForm_start = class(TForm)
	  Panel_bild: TPanel;
	  Image1: TImage;
	  StatusBar: TStatusBar;
	  Notebook: TNotebook;
    BitBtn_beantragen: TBitBtn;
    BitBtn_cancel: TBitBtn;
	  Label1: TLabel;
	  Label2: TLabel;
	  Edit1: TEdit;
	  Edit2: TEdit;
	  BitBtn_anmelden: TBitBtn;
	  BitBtn2: TBitBtn;
	  Image2: TImage;
    Label3: TLabel;
    Image3: TImage;
    Memo: TMemo;
    Image4: TImage;
    Label4: TLabel;
    ComboBox_check: TComboBox;
	  procedure FormCreate(Sender: TObject);
	  procedure BitBtn_anmeldenClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
	private
	  { Private-Deklarationen }
	public
	  { Public-Deklarationen }
	  procedure info(text:string);
	  function anmelden: boolean;
  end;

var
  Form_start: TForm_start;

implementation

{$R *.DFM}

procedure TForm_start.FormCreate(Sender: TObject);
begin
       image1.Visible:=false;
       image2.Visible:=false;
		  image3.visible:=false;
		  image4.visible:=false;
  caption:=application.Title;
   case prg_typ  of
			1:image1.visible:=true;
         2:begin
               image1.Visible:=false;
               image2.Visible:=false;
               image3.visible:=true;
               height:=725;
               width:=979;
          end;
         3:image2.visible:=true;
         4:image4.visible:=true;
   end;
  form_main.form_positionieren(tform(sender));

  //Top:=((screen.height-15) div 2)-(Height div 2);
	//Left:= (screen.width div 2) - (width div 2);

   //scaleby(150,100);
   //width:=round(width*1.5);
   //font.Height:=22;
end;

procedure TForm_start.BitBtn_anmeldenClick(Sender: TObject);
begin
	if anmelden then modalresult:=mrok;
end;

function tform_start.anmelden: boolean;
begin
result:=false;
datamodul.q_schluessel.first;
if ( datamodul.q_schluessel.locate('name',edit1.text,[loCaseInsensitive]))
	//and  (form_main.CipherManager.encodeString(edit2.text)=datamodul.q_schluessel['passwort'] ) then
  and  ((edit2.text)=form_main.string_entschluesseln_pr(datamodul.q_schluessel['passwort'] )) then
	begin
		user_id:=datamodul.q_schluessel['nummer'];
    
		if mysql_d.ds_sperren('user',user_id,1) then result:=true //user wird eingetragen
		else
		begin
			if MessageDlg('Der Benutzer ist schon angemeldet, trotzdem starten?', mtConfirmation	,[mbYes, mbNo], 0)=mryes then result:=true;
		end;

	end
	else
   	begin

      	showmessage('Name oder Passwort ist falsch');
      end;
end;

procedure tform_start.info(text:string);
begin
	statusbar.Panels[0].Text:=trim(text);
	application.ProcessMessages;
end;



procedure TForm_start.FormShow(Sender: TObject);
begin
  //Application.Restore;
 //application.BringToFront;
 //application.ProcessMessages;
 arbene_vor;
  form_start.BringToFront;
  application.ProcessMessages;
end;

end.
