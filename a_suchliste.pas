unit a_suchliste;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, Grids, DBGrids, dbgridEXT, ExtCtrls,db, 
  ComCtrls, TreeView_ext;

type
  Tform_suchliste = class(TForm)
    Panel_li: TPanel;
    Panel_re: TPanel;
    Tree: TTreeView_ext;
    Panel4: TPanel;
    Panel_left: TPanel;
    SpeedButton_filter_taet: TSpeedButton;
    SpeedButton_filter_aufgabe: TSpeedButton;
    Panel_bottom: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_esc: TBitBtn;
    Panel_li_unten: TPanel;
    CheckBox_aufgabenfeld: TCheckBox;
    CheckBox_parent: TCheckBox;
    Panel_li_top: TPanel;
    SpeedButton4: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Label1: TLabel;
    Edit_such_tree: TEdit;
    Splitter1: TSplitter;
    Panel__re_bottom: TPanel;
    Label_such: TLabel;
    CheckBox_taetigkeit: TCheckBox;
    Panel_re_top: TPanel;
    SpeedButton_ag_refresh: TSpeedButton;
    Label2: TLabel;
    Edit_such: TEdit;
    DBgrid: TDBgridEXT;
    Panel_re_right: TPanel;
    Panel_re_left: TPanel;
    procedure SpeedButton_ag_refreshClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBgridKeyPress(Sender: TObject; var Key: Char);
    procedure DBgridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton_filter_taetClick(Sender: TObject);
    procedure TreeDblClick(Sender: TObject);
    procedure SpeedButton_filter_aufgabeClick(Sender: TObject);
    procedure Edit_suchKeyPress(Sender: TObject; var Key: Char);
    procedure Edit_suchKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit_such_treeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure filtern;
  public
    { Public-Deklarationen }
    s_name:string;
    startparameter:string;
  end;

var
  form_suchliste: Tform_suchliste;

implementation

uses a_data, a_main, a_unt_wahl;

{$R *.dfm}

procedure Tform_suchliste.SpeedButton_ag_refreshClick(Sender: TObject);
begin
	filtern;
end;

procedure Tform_suchliste.filtern;
var query,wort:string;
begin
   wort:=trim(edit_such.text);
   query:='select nummer, left(name,250) as name_f  from aufwand where aufwand.name like "%'+wort+'%" order by name';
   datamodul.sql_new(true, datamodul.q_7 ,query,'');

end;

procedure Tform_suchliste.FormShow(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
panel_li.Width:=form_suchliste.Width div 2;
dbgrid.columns[0].Width:=dbgrid.Width-20;
 if startparameter='aufgabe' then Edit_such_tree.SetFocus else Edit_such.SetFocus;

end;

procedure Tform_suchliste.DBgridKeyPress(Sender: TObject; var Key: Char);
var
ordkey:integer;
begin
ordkey:=ord(key);
case  ordkey of
13: modalresult:=mrok;
48..256 :
  begin
    edit_such.text:=DBgrid.eingabetext;
    filtern;
    dbgrid.SetFocus;
	end;

end;
end;

procedure Tform_suchliste.DBgridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
edit_such.text:=DBgrid.eingabetext;
end;

procedure Tform_suchliste.SpeedButton5Click(Sender: TObject);
begin
tree.FullExpand ;
end;

procedure Tform_suchliste.SpeedButton7Click(Sender: TObject);
begin
tree.FullCollapse;

end;

procedure Tform_suchliste.SpeedButton4Click(Sender: TObject);
begin
tree.suche_datensatz(edit_such_tree.text);
end;

procedure Tform_suchliste.SpeedButton_filter_taetClick(Sender: TObject);
var
i:integer;
nr:int64;
sel:ttreenode;
daten:pnodepointer;
nummern,query:string;
begin
 sel:=Tree.Selected;

        for i:=0 to Tree.Items.Count-1 do
           begin
           		if (( Tree.Items[i].StateIndex=3) or (Tree.Items[i]=sel)) then
               	begin
                     daten:=Tree.Items[i].data;
                     nr:=daten^.nummer;
                     nummern:=nummern+inttostr(nr)+',';
           			end;
				end;
        nummern:=copy (nummern,1,length(nummern)-1);

     query:=format('select aufwand.nummer, left(name,250) as name_f  from aufwand left join anlass_aufwand on (aufwand.nummer= anlass_aufwand.i_aufwand)  where anlass_aufwand.i_anlass in  (%s) and anlass_aufwand.storno=0 group by aufwand.nummer order by name_f',[nummern]);
     datamodul.sql_new(false, datamodul.q_7 ,query,'');
     checkbox_taetigkeit.Checked:=true;
    checkbox_aufgabenfeld.Checked:=true;
 end;

procedure Tform_suchliste.TreeDblClick(Sender: TObject);
begin

	if tree.selected.stateindex=-1 then tree.selected.stateindex:=3
	else tree.selected.stateindex:=-1;
{end
else
   if tree.selected.stateindex=-1 then tree.markieren(parent_markieren,3)
   else tree.markieren(parent_markieren,-1); }

tree.selected.Expand(true);

end;

procedure Tform_suchliste.SpeedButton_filter_aufgabeClick(Sender: TObject);
var
i:integer;
nr: int64;
nummern,query: string;
begin
if  dbgrid.SelectedRows.Count>0 then
   begin
       for I := 0 to dbgrid.SelectedRows.Count - 1 do
         begin
              datamodul.q_7.GotoBookmark(Pointer(DBGrid.SelectedRows.Items[I]));
              nummern:=nummern+datamodul.q_7.findfield('nummer').asstring+',';
         end;
   end
   else
   begin
    datamodul.q_7.First;
    while not datamodul.q_7.eof do
    begin
       nummern:=nummern+datamodul.q_7.findfield('nummer').asstring+',';
       datamodul.q_7.next;
    end;
   end;
   nummern:=copy (nummern,1,length(nummern)-1);
   query:=format('select * from abrechnung_anlass left join anlass_aufwand on (abrechnung_anlass.nummer= anlass_aufwand.i_anlass)  where anlass_aufwand.i_aufwand in  (%s) and anlass_aufwand.storno=0 group by abrechnung_anlass.nummer order by abrechnung_anlass.reihenfolge ',[nummern]);
   datamodul.sql_new(false, datamodul.q_3 ,query,'');
   tree.liste_lesen;
   if tree.Items.Count>=1 then  tree.Select(tree.Items[0]);
   checkbox_aufgabenfeld.Checked:=true;
   checkbox_taetigkeit.checked:=true;
end;

procedure Tform_suchliste.Edit_suchKeyPress(Sender: TObject;
  var Key: Char);
var
ordkey:integer;
begin
ordkey:=ord(key);
case  ordkey of
13: filtern;
48..256 :
  begin
    //DBgrid.eingabetext:=edit_such.text;
    //filtern;
    //edit_such.SetFocus;
	end;

end;
end;



procedure Tform_suchliste.Edit_suchKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
ordkey:integer;
begin
ordkey:=ord(key);
case  ordkey of
13: filtern;
48..256 :
  begin
    DBgrid.eingabetext:=edit_such.text;
    filtern;
    edit_such.SetFocus;
	end;

end;

end;

procedure Tform_suchliste.Edit_such_treeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if key=vk_return then tree.suche_datensatz(edit_such_tree.text);
end;

procedure Tform_suchliste.FormDestroy(Sender: TObject);
begin
tree.liste_loeschen;
end;

end.
