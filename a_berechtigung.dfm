�
 TFORM_BERECHTIGUNG 0I'  TPF0TForm_berechtigungForm_berechtigungLeftWTop� WidthqHeight�HelpContext3BorderIcons CaptionBerechtigungs-VerwaltungColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TToolBarToolBarLeft Top WidthaHeight(AutoSize	ButtonHeight$ButtonWidth7CaptionToolBarImagesForm_main.ImageList_mainShowCaptions	TabOrder  TToolButtonToolButton_newLeft TopHintNeue Berechtigung vergebenCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_newClick  TToolButtonToolButton3Left7TopWidthCaptionToolButton3
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeft?TopCaption   Ändern
ImageIndexOnClickToolButton_editClick  TToolButtonToolButton2LeftvTopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeft~TopCaption	Speichern
ImageIndexOnClickToolButton_saveClick  TToolButtonToolButton5Left� TopWidthCaptionToolButton5
ImageIndexStyletbsSeparator  TToolButtonToolButton_delLeft� TopHint   Berechtigung löschenCaption   Löschen
ImageIndexParentShowHintShowHint	OnClickToolButton_delClick  TToolButtonToolButton7Left� TopWidthCaptionToolButton7
ImageIndexStyletbsSeparator  TToolButtonToolbutton_VerlassenLeftTopHintBeendenCaption	Verlassen
ImageIndexParentShowHintShowHint	OnClickToolbutton_VerlassenClick   TPageControlPageControlLeft� Top(Width�Height�
ActivePageTabSheet_b_firmaAlignalClientTabOrderOnChangePageControlChange
OnChangingPageControlChanging 	TTabSheetTabSheet_passwortCaptionPasswort 	TNotebookNotebookLeft Top Width�HeightkAlignalClient	PageIndexTabOrder  TPage Left Top Captionneu TLabelLabel2LeftTop|Width+HeightCaptionPasswort  TLabelLabel3LeftTop� Width+HeightCaptionPasswort  TLabelLabel7LeftTopWidth\HeightCaptionNeue Berechtigung  TLabelLabel13LeftTop� Width� HeightCaption-Passwort muss mindestens 5 Zeichen lang sein!  TLabelLabel1LeftTopLWidth5HeightCaption	User-Name  TEdit	Edit_new2LeftNTopyWidth� HeightCharCaseecLowerCasePasswordChar#TabOrder  TEdit	Edit_new3LeftNTop� Width� HeightCharCaseecLowerCasePasswordChar#TabOrder  TEdit	Edit_new1LeftNTopIWidth� HeightCharCaseecLowerCaseTabOrder    TPage Left Top Captionedit TLabelLabel5LeftPTop� Width@HeightCaptionPasswort neu  TLabelLabel6LeftPTop� Width@HeightCaptionPasswort neu  TLabelLabel8LeftTopWidth�HeightCaptionQ   User und Passwort ändern (Es müssen sowohl User wie Passwort eingegeben werden)  TLabelLabel14LeftPTop� Width� HeightCaption1Das Passwort muss mindestens 5 Zeichen lang sein!  TLabelLabel_p_altLeftOTopWidth9HeightCaptionPasswort alt  TLabelLabel4LeftOTopBWidth5HeightCaption	User-Name  TLabelLabel9LeftTop Width� HeightCaption#   zugehörigen Untersucher auswählen  TLabelLabel12LeftPTop� WidthHHeightCaption(Wiederholung)  TLabelLabel16LeftTop8Width� HeightCaption%   (wird erst nach Neustart übernommen)  TEdit
Edit_edit2Left� Top� Width� HeightCharCaseecLowerCasePasswordChar#TabOrder  TEdit
Edit_edit3Left� Top� Width� HeightCharCaseecLowerCasePasswordChar#TabOrder  TEdit
Edit_p_altLeft� TopzWidth� HeightCharCaseecLowerCasePasswordChar#TabOrder  TEditEdit_p_userLeft� TopAWidth� HeightCharCaseecLowerCaseTabOrder   TDBCombo_numDBCombo_num_untersucherLeft� TopWidth� Heightalle_einfuegenkeines_einfuegen	DataFieldi_untersucher
DataSourceDataModul.ds_schluesselzconnectionDataModul.connection_main	ztab_nameuntersucherzfield_textuntersucherzfield_indexnummerzorderuntersucherzstorno
ItemHeightTabOrder     	TTabSheetTabSheet_b_firmaCaptionBerechtigungen Firmen
ImageIndex TCheckListBoxCheckListBox_firmenLeftTopWidth�Height:AlignalClient
ItemHeightTabOrder   TPanelPanel1Left Top Width�HeightAlignalTop
BevelOuterbvNoneTabOrder  TPanelPanel4Left TopHWidth�Height#AlignalBottom
BevelOuterbvNoneTabOrder TLabelLabel15LeftTopWidth�HeightCaptionDNeue Berechtigungen werden erst nach Neustart des Programms wirksam!Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TPanelPanel9Left TopWidthHeight:AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel10Left�TopWidth� Height:AlignalRight
BevelOuterbvNoneTabOrder TBitBtnBitBtn_checkLeft&Top� Width{HeightCaption   alle auswählenTabOrder OnClickBitBtn_checkClick  TBitBtnBitBtn_decheckLeft'Top� WidthzHeightCaptionalle entfernenTabOrderOnClickBitBtn_decheckClick    	TTabSheetTabSheet_b_aktionenCaptionBerechtigung Aktionen
ImageIndex TPanelPanel2Left Top Width!Height�AlignalLeft
BevelOuterbvNoneTabOrder   TPanelPanel_abLeft�Top Width� Height�AlignalRight
BevelOuterbvNoneCaptionPanel_abTabOrder 
TDBgridEXTDBgridEXT_bLeft Top!Width� Height�AlignalClientDefaultDrawing	OptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT_bDblClickOnEnterDBgridEXT_bEnterkeine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameobjektVisible	    TDBRadioGroupDBRadioGroup_aktionLeft Top�Width� HeightAAlignalBottomCaption&   Berechtigung für die gewählte Aktion	DataFieldberechtigungItems.Strings&Lesen   &Ändern   &Neu / Löschen / Ändern TabOrderValues.Strings012   TPanelPanel12Left Top Width� Height!AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel10LeftTopWidtheHeightCaptionerlaubte AktionenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBEditDBEdit_changeLeft� TopWidth=Height	DataFieldnummerTabOrder VisibleOnChangeDBEdit_changeChange    TPanelPanel7Left�Top Width'Height�AlignalRight
BevelOuterbvNoneTabOrder TSpeedButtonSpeedButton_entfernenLeftTop� WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333�3333337D333333�s333337DG33333�w�33337DDC3333�s733337DDD3333�s3s3337DDDG333�s37�337DDDDC33�s337337DDDDD337�������������s7wwwwws3�����337s�33s�333�����337s�37�3333����3337s�7?33333���33337s�s�33333���33337s��333333��333337w�3333333�3333337s	NumGlyphsOnClickSpeedButton_entfernenClick  TSpeedButtonSpeedButton_hinzufuegenLeftTop� WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�333333Ds333333w��33333tDs333337w?�33334DDs33337?w?�3333DDDs3333s�w?�333tDDDs3337�3w?�334DDDDs337?33w?�33DDDDDs?����w���������wwwwww3w33�����333s33?w33<����3337�3?w333<���333373?w3333���33333s?w3333<��333337�w33333<�3333337w333333�3333333w3333333	NumGlyphsOnClickSpeedButton_hinzufuegenClick   TPanelPanel6Left�Top WidthHeight�AlignalRight
BevelOuterbvNoneTabOrder  TPanelPanel_mbLeft!Top Width~Height�AlignalClient
BevelOuterbvNoneCaptionPanel_mbTabOrder 
TDBgridEXTDBgridEXT_vLeft Top!Width~Height�AlignalClient
DataSourceDataModul.ds_1DefaultDrawing	Options	dgEditingdgColumnResize
dgColLines
dgRowLinesdgTabsdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgridEXT_vDblClickOnEnterDBgridEXT_vEnterkeine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameobjektVisible	    TPanelPanel13Left Top Width~Height!AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel11Left TopWidth� HeightCaptionnicht erlaubte AktionenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBEditDBEdit_change1Left� TopWidth.Height	DataFieldnummerTabOrder VisibleOnChangeDBEdit_change1Change    TPanelPanel5Left Top�Width�HeightmAlignalBottom
BevelOuterbvNoneTabOrder 	TGroupBox	GroupBox2Left$Top WidtheHeightmAlignalClientCaption   Info zur ausgewählten AktionTabOrder  TMemoMemo1LeftTopWidthaHeight\AlignalClientLines.StringsMemo1 TabOrder    TPanelPanel8Left Top Width$HeightmAlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel11Left�Top WidthHeightmAlignalRight
BevelOuterbvNoneTabOrder     TPanel
Panel_leftLeft Top(Width� Height�AlignalLeftBorderWidthCaption
Panel_leftTabOrder 
TDBgridEXTDBgridEXT_nameLeftTopWidth� HeightAlignalClient
DataSourceDataModul.ds_schluesselDefaultDrawing	OptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style keine_farbeZeitdiff_L_B darf_ziehenColumnsExpanded	FieldNamenameWidth� Visible	      