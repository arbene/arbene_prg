�
 TFORM_IMP_AUSNAHMEN 0�  TPF0TForm_imp_ausnahmenForm_imp_ausnahmenLeft�Top� Width�Height�Caption   Sonderfälle beim ImportColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�HeightAAlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel5Left TopWidth$HeightCaption[   Falls sich innerhalb des Aktualisierungszeitraumes  Änderungen am gleichen Datensatz von  Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6Left Top Width�HeightCaptionQ   Haupt- und Satellitensystem ergeben haben, muss ein Datensatz ausgewählt werden.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   TPanelPanel2Left Top�Width�Height� AlignalBottom
BevelOuterbvNoneTabOrder TLabelLabel4LeftPTop>WidthwHeightCaptionX   Soll der Datensatz importiert werden und der bestehende Datensatz überschrieben werden?Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel_achtungLeftPTop&WidthfHeightCaptionLabel_achtungFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TBitBtn	BitBtn_okLeft� TopeWidthvHeightCaptionJaModalResultTabOrder 
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtn
bitbtn_escLeftTopeWidthvHeightTabOrderKindbkNo  	TCheckBoxCheckBox_neuLeftPTopWidthHeightCaption,   Der neuere Datensatz  wird immer übernommenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickCheckBox_neuClick   TPanelPanel3Left TopAWidthHeight�AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel4Left�TopAWidthHeight�AlignalRight
BevelOuterbvNoneTabOrder  TPanelPanel5LeftTopAWidthnHeight�AlignalClientCaptionPanel5TabOrder 	TSplitter	Splitter3LeftiTopWidthHeight�ColorclGrayParentColor  TPanelPanel6LeftTopWidthhHeight�AlignalLeftTabOrder  TPanelPanel7LeftTopWidthfHeight)AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel1Left(TopWidth� HeightCaption'Dieser Datensatz soll importiert werdenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TListBoxListBox1LeftTop*WidthfHeightAlignalClient
ItemHeightTabOrderOnClickListBox1Click  TMemoMemo1LeftTopBWidthfHeightPAlignalBottomLines.Stringsbedarfsweise Zeile anklicken ReadOnly	
ScrollBarsssBothTabOrder   TPanelPanel8LeftmTopWidth Height�AlignalClientTabOrder TPanelPanel9LeftTopWidth�Height)AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel2Left$TopWidth� HeightCaption"Dieser Datensatz befindet sich aufFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3Left"TopWidthHeightCaption-   dieser Datenbank und wird ggf. überschriebenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TListBoxListBox2LeftTop*Width�HeightAlignalClient
ItemHeightTabOrderOnClickListBox2Click  TMemoMemo2LeftTopBWidth�HeightPAlignalBottomLines.Stringsbedarfsweise Zeile anklicken ReadOnly	
ScrollBarsssBothTabOrder     