object Form_csv_import: TForm_csv_import
  Left = 454
  Top = 193
  Width = 1127
  Height = 675
  Caption = 'CSV-Import'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid: TStringGrid
    Left = 38
    Top = 41
    Width = 1019
    Height = 503
    Align = alClient
    ColCount = 15
    TabOrder = 0
    OnClick = StringGridClick
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object Panel1: TPanel
    Left = 0
    Top = 585
    Width = 1111
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BitBtn_ok: TBitBtn
      Left = 259
      Top = 3
      Width = 199
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn_esc: TBitBtn
      Left = 574
      Top = 3
      Width = 199
      Height = 25
      Caption = 'Abbruch'
      ModalResult = 3
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1111
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Panel3: TPanel
    Left = 0
    Top = 41
    Width = 38
    Height = 503
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
  end
  object Panel4: TPanel
    Left = 1057
    Top = 41
    Width = 54
    Height = 503
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 4
  end
  object Panel5: TPanel
    Left = 0
    Top = 544
    Width = 1111
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    object ProgressBar: TProgressBar
      Left = 40
      Top = 12
      Width = 1017
      Height = 17
      Max = 60
      TabOrder = 0
    end
  end
  object CSVReader: TCSVReader
    QuoteDelimiter = '"'
    CommaSeparator = ';'
    Grid = StringGrid
    AddNumbersColumn = False
    TrimBlanks = True
    OnBeforeCopyToGrid = CSVReaderBeforeCopyToGrid
    Left = 968
    Top = 8
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'csv'
    Filter = 'CSV-Datei | *.csv'
    Left = 1056
    Top = 8
  end
  object MainMenu1: TMainMenu
    Left = 1016
    object m_Importieren: TMenuItem
      Caption = 'Importieren'
      OnClick = m_ImportierenClick
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 936
    Top = 8
  end
end
