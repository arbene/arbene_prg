unit a_uebersicht_arbeitsunfall;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,Variants;

type
  Tform_ueb_arbunfall = class(TForm)
    ComboBox_sortieren: TComboBox;
    MaskEdit_von: TMaskEdit;
    SpeedButton1: TSpeedButton;
    MaskEdit_bis: TMaskEdit;
    SpeedButton2: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ComboBox_koerperteil: TComboBox;
    CheckBox_alle: TCheckBox;
    CheckBox_anonym: TCheckBox;
    Label3: TLabel;
    ComboBox_verletzungsart: TComboBox;
    BitBtn_uebersicht_excel: TBitBtn;
    Memo: TMemo;
    BitBtn_uebersicht_memo: TBitBtn;
    BitBtn2: TBitBtn;
    ProgressBar: TProgressBar;
    BitBtn_graphik: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn_uebersicht_excelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private-Deklarationen }
    abbruch:boolean;
    function suche_parent(nummer:string;ebene,bereich:integer):string;
    function suche_begriff(text:string;ebene,bereich:integer): string ;
    procedure klassifizieren;
  public
    { Public-Deklarationen }
    nur_abt:boolean;
    liste:tstringlist;
  end;

var
  form_ueb_arbunfall: Tform_ueb_arbunfall;

implementation

uses a_main, a_data, a_kalender;

{$R *.DFM}

procedure Tform_ueb_arbunfall.FormCreate(Sender: TObject);
var
   dat:textfile;
   datname,s:string;
begin
     form_main.form_positionieren(tform(sender));
     abbruch:=false;
     maskedit_von.EditText:=datetostr(date);
     maskedit_bis.EditText:=datetostr(date);

     combobox_sortieren.ItemIndex:=0;
     combobox_koerperteil.ItemIndex:=1;
     combobox_verletzungsart.ItemIndex:=1;
     liste:=tstringlist.Create;
     datname:=w_pfad+'\ambulanz_fuellworte.txt';
     if not fileexists(datname) then
     begin
          //repl_master:=' re, rechts, rechte, rechter, li, links, linke, linker, beidseitig, siehe, vorbefund,(,),.,-,';
          exit;
     end;
     assignfile(dat,datname );
     reset(dat);
     while not eof(dat) do
     begin
          readln(dat,s);
          s:=trim(stringreplace(  s,chr(9),' ',[rfReplaceAll]));
          liste.Add(s);
     end;

     closefile(dat);

end;

//procedure Tform_ueb_arbunfall.BitBtn_uebersichtClick(Sender: TObject);


function Tform_ueb_arbunfall.suche_parent(nummer:string;ebene,bereich:integer):string;
var query,s1,s2,s3:string;
    sa: array[0..3] of string;
    numa: array of string;
    s_texta: array of string;
    reihe:array of string;

    i:integer;
    nr:int64;
begin

     //query:=format('select  from ambulanz_insert where nummer=%s',[inttostr(nummer)]);
     i:=1;
     query:=format('select nummer,parent,s_text,reihenfolge from ambulanz_insert where nummer=%s and bereich=%d',[nummer,bereich]);
     mysql_d.Feldarray( query,[0,1,2,3],sa);
     SetLength(numa, i);
     SetLength(s_texta, i);
     SetLength(reihe, i);

      numa[i-1]:=sa[0];
      s_texta[i-1]:=sa[2];
      reihe[i-1]:=sa[3];
      inc(i);
     //gehe zu root
     //sa[1] : parent
     while sa[1]<>'0' do
     begin
      query:=format('select nummer,parent,s_text,reihenfolge from ambulanz_insert where nummer=%s and bereich=%d',[sa[1],bereich]);
      mysql_d.Feldarray( query,[0,1,2,3],sa);
      SetLength(numa, i);
      SetLength(s_texta, i);
      SetLength(reihe, i);
      numa[i-1]:=sa[0];
      s_texta[i-1]:=sa[2];
      reihe[i-1]:=sa[3];

      inc(i);
     end;

     if i-2-ebene<0 then result:=reihe[0]+'|'+s_texta[0] else result:=reihe[i-2-ebene]+'|'+s_texta[i-2-ebene];
end;


function Tform_ueb_arbunfall.suche_begriff(text:string;ebene,bereich:integer): string ;
var
query,wort,c,repl:string;
i,p:integer;
res: array [0..2] of string;
begin
  text:=' '+lowercase(text)+' ';

 text:=stringreplace(  text,',',' ',[rfReplaceAll]);
  text:=stringreplace(  text,';',' ',[rfReplaceAll]);
  text:=stringreplace(  text,'-',' ',[rfReplaceAll]);
  text:=stringreplace(  text,'_',' ',[rfReplaceAll]);
  text:=stringreplace(  text,'.',' ',[rfReplaceAll]);
  text:=stringreplace(  text,'?',' ',[rfReplaceAll]);
   text:=stringreplace(  text,'!',' ',[rfReplaceAll]);
   text:=stringreplace(  text,'/',' ',[rfReplaceAll]);
   text:=stringreplace(  text,'\',' ',[rfReplaceAll]);
   text:=stringreplace(  text,'=',' ',[rfReplaceAll]);


 text:=stringreplace(  text,chr(13),' ',[rfReplaceAll]);
 text:=stringreplace(  text,chr(10),' ',[rfReplaceAll]);
 text:=stringreplace(  text,chr(9),' ',[rfReplaceAll]);
 for i:=0 to liste.Count-1 do
 begin
    wort:=' '+liste[i]+' ';
    text:=stringreplace(  text,wort,'  ',[rfReplaceAll]);
 end;

 text:=stringreplace(  text,'   ',' ',[rfReplaceAll]);
 text:=stringreplace(  text,'  ',' ',[rfReplaceAll]);
 text:=trim(text);
  // suche 1. wort
 i:=pos(' ',text);
 if i>0 then wort:=copy(text,1,i-1) else wort:=text;
 if wort<>'' then
 begin
       query:=format('select nummer,parent,s_text from ambulanz_insert where s_text like "%s" and bereich=%d and storno=0',['%'+wort+'%',bereich]);
       mysql_d.Feldarray(query,[0,1,2],res);

  //wenn vorhanden dann parent sonst begriff
    if res[2]<>'' then
      result:=suche_parent(res[0],ebene,bereich) else result:='|'+wort;
 end
 else result:=''; // leeres feld
end;



procedure Tform_ueb_arbunfall.klassifizieren;
var s1,s2:string;
i:int64;
s: string;
rf,p,anz,z:integer;
begin
with datamodul do
begin
     z:=1;
     anz:=q_1.RecordCount;
     q_1.first;
     while not q_1.eof do
     begin
       try
         q_1.edit;
         s:=suche_begriff(q_1['s00'],combobox_koerperteil.ItemIndex,1);
         p:=pos('|',s);
         q_1['klass_00']:=copy(s,p+1,length(s));
         if p>1 then rf:=strtoint(copy(s,1,p-1)) else rf:=0;
         if combobox_sortieren.ItemIndex=0 then q_1['reihenfolge']:=rf;

         s:=suche_begriff(q_1['s10'],combobox_verletzungsart.ItemIndex,2);
         p:=pos('|',s);
         q_1['klass_10']:=copy(s,p+1,length(s));
         if p>1 then rf:=strtoint(copy(s,1,p-1)) else rf:=0;
         if combobox_sortieren.ItemIndex=1 then q_1['reihenfolge']:=rf;

         q_1.post;

         progressbar.position:=(z *100) div anz;
         inc(z);


         application.ProcessMessages;
         if abbruch then exit;
       except
       end;
       q_1.next;

     end;
end;
end;


procedure Tform_ueb_arbunfall.BitBtn_uebersicht_excelClick(Sender: TObject);
var

i,zeile,z:integer;
s0,s1,s2,s3,s4,s5,s6,query,datname:string;
ebene:integer;
mm:integer;
begin

if tbitbtn(sender).name='BitBtn_uebersicht_memo' then mm:=0;
if tbitbtn(sender).name='BitBtn_uebersicht_excel' then mm:=1;
if tbitbtn(sender).name='BitBtn_graphik' then
begin
     mm:=2;
     combobox_sortieren.ItemIndex:=0;
     checkbox_alle.Checked:=false;
end;




with datamodul, form_main do
try
//  query:=format('drop table IF EXISTS temp_%s',[user_name]);
  //query:=format('drop table temp_%s',[user_name]);
  //mysql_d.sql_exe(query);
  mysql_d.drop_table('temp_'+user_name);

  s1:=sql_datetostr(strtodate(maskedit_von.edittext));
  s2:=sql_datetostr(strtodate(maskedit_bis.edittext));
  s3:=inttostr(datamodul.q_firma['nummer']);
  s4:=getbigint_str(datamodul.q_ap3,'nummer');
  query:=format('create table temp_%s select mitarbeiter.name, mitarbeiter.vorname, mitarbeiter.geb_dat,mitarbeiter.i_abteilung, ambulanz.*, arbeitsunfall.u_datum, arbeitsunfall.u_zeit,RPAD("",40," ") as klass_00, RPAD("",40," ") as klass_10, ',[user_name])
         + ' abteilung.kuerzel, abteilung.langtext  from ambulanz left join arbeitsunfall on(ambulanz.nummer=arbeitsunfall.nummer) left join mitarbeiter on(ambulanz.i_mitarbeiter=mitarbeiter.nummer) left join abteilung on (abteilung.nummer=mitarbeiter.i_abteilung) '
         + format(' where b_arbeitsunfall=1 and u_datum>=%s and u_datum<=%s and mitarbeiter.i_firma=%s',[s1,s2,s3]);

  if nur_abt then query:=query+ format(' and i_abteilung=%s', [s4]);
  mysql_d.sql_exe(query);
  query:=format('alter table temp_%s add reihenfolge integer(11)',[user_name]);
  mysql_d.sql_exe(query);
  query:=format('select * from temp_%s',[user_name]);
  datamodul.sql(false,q_1,query,'');

  klassifizieren;

  memo.Clear;

  if mm=1 then
  begin
	com.excel_connect('Arbeitsunfälle');
        //com.excel_addworkbook('Arbeitsunfälle');

   end;

   if mm=2 then
   begin
      form_main.Arbeitsmedizin.CreateObjectFromFile(w_pfad+'\unfallgraphik.doc',false);    //pfad
      form_main.Arbeitsmedizin.DoVerb(0);
      com.wordobjekterstellen(form_main.Arbeitsmedizin);
   end;

  s1:=format('Übersicht Arbeitsunfälle von %s bis %s',[maskedit_von.edittext, maskedit_bis.edittext ]);
  case mm of
  0:memo.Lines.Add(s1);
  1:com.excel_PutStrAt(1,1,s1);
  2:;
  end;

  //q_1.Last;
  s1:='Anzahl der Arbeitsunfälle: '+ inttostr(q_1.RecordCount);
  zeile:=2;
  case mm of
  0:memo.Lines.Add(s1);
  1:com.excel_PutStrAt(zeile,1,s1);
  2:;
  end;

  inc(zeile);
  if combobox_sortieren.itemindex=0 then s1:='Sortiert nach Lokalisation' else s1:='Sortiert nach Verletzungsart';
  s2:=inttostr(combobox_koerperteil.itemindex);
  s3:=inttostr(combobox_verletzungsart.itemindex);
  case mm of
  0:memo.Lines.Add(s1);
  1:com.excel_PutStrAt(zeile,1,s1);
  2:;
  end;

  inc(zeile);
  case mm of
  0:memo.Lines.Add('Hierarchie Körperteil: '+s2);
  1:com.excel_PutStrAt(zeile,1,'Hierarchie Körperteil: '+s2);
  2:;
  end;

  case mm of
  0:memo.Lines.Add('Hierarchie Verletzungsart: '+s3);
  1:com.excel_PutStrAt(zeile,3,'Hierarchie Verletzungsart: '+s3);
  2:;
  end;

  inc(zeile);
  inc(zeile);


  if combobox_sortieren.itemindex=0 then s2:='klass_00' else s2:='klass_10';
  query:=format('select * from temp_%s order by reihenfolge, %s',[user_name,s2]);
  datamodul.sql(false,q_1,query,'');

  q_1.first;
  while not q_1.eof do
  begin
    if checkbox_alle.Checked then
    begin
      s1:=q_1['klass_00'];
      s2:=q_1['klass_10'];
      if q_1['u_datum']<>null then s3:=q_1['u_datum'] else s3:='';
      if q_1['u_zeit']<>null then s4:=q_1['u_zeit'] else s4:='';
      if q_1['kuerzel']<>null then s5:=q_1['kuerzel'] else s5:='';
      if q_1['langtext']<>null then s6:=q_1['langtext'] else s6:='';
      case mm of
      0:memo.Lines.Add(s1+#9+s2+#9+s3+#9+s4+#9+s5+#9+s6+#9);
      1:begin
        com.excel_PutStrAt(zeile,1,s1);
        com.excel_PutStrAt(zeile,2,s2);
        com.excel_PutStrAt(zeile,3,s3);
        com.excel_PutStrAt(zeile,4,s4);
        com.excel_PutStrAt(zeile,5,s5);
        com.excel_PutStrAt(zeile,6,s6);
      end;
      2:;
      end;

      if not checkbox_anonym.Checked then
      begin
        s1:=q_1['name'];
        s2:=q_1['vorname'];
        s3:=q_1['geb_dat'];
        case mm of
        0:;
        1:begin
        com.excel_PutStrAt(zeile,7,s1);
        com.excel_PutStrAt(zeile,8,s2);
        com.excel_PutStrAt(zeile,9,s3);
        end;
        2:;
        end;

      end;
      q_1.Next;
    end
    else
    begin
      s1:=q_1['klass_00'];
      s2:=q_1['klass_10'];


      q_1.next;
      z:=1;
      if combobox_sortieren.ItemIndex=0 then    //Körperteil
      begin

        while (q_1['klass_00']=s1)  and (not q_1.eof) do
        begin
             inc(z);
             q_1.next;
        end;
        s0:=s1;
      end
      else  //Verletzungsart
      begin

        while (q_1['klass_10']=s2) and (not q_1.eof) do
        begin
             inc(z);
             q_1.next;
        end;
        s0:=s2;
      end;

      case mm of
      0:memo.Lines.Add(s0+': '+inttostr(z));
      1:begin
      com.excel_PutStrAt(zeile,2,s0);
      com.excel_PutStrAt(zeile,3,'Anzahl:');
      com.excel_PutStrAt(zeile,4,inttostr(z));
      end;
      2:
      begin
           //s2:='#'+trim(lowercase(s1))+'#';
           s2:=trim(lowercase(s1));
           com.bookmark_einfuegen(s2, s1+': '+inttostr(z),true);
      end;
      end;


    end;

    inc(zeile);
    //q_1.next;
  end;


finally
       case mm of
       1: com.excel_disconnect;
       2:
         begin

         end;
       end;
       //query:=format('drop table IF EXISTS temp_%s',[user_name]);
       //query:=format('drop table temp_%s',[user_name]);
       //mysql_d.sql_exe(query);
       mysql_d.drop_table('temp_'+user_name);
       modalresult:=mrok;
end;
end;

procedure Tform_ueb_arbunfall.FormDestroy(Sender: TObject);
begin
liste.Clear;
liste.Free;
end;

procedure Tform_ueb_arbunfall.BitBtn2Click(Sender: TObject);
begin
abbruch:=true;
end;

procedure Tform_ueb_arbunfall.SpeedButton1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 maskedit_von.text :=datetostr(int(form_kalender.auswahlkalender.date));

end;

procedure Tform_ueb_arbunfall.SpeedButton2Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	 maskedit_bis.text :=datetostr(int(form_kalender.auswahlkalender.date));

end;

end.
