unit Bersonderheit_edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TForm_bes_edit = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit: TEdit;
    CheckBox: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_bes_edit: TForm_bes_edit;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_bes_edit.FormActivate(Sender: TObject);
begin
edit.SetFocus;
end;

procedure TForm_bes_edit.FormCreate(Sender: TObject);
begin
form_main.form_positionieren(tform(sender));
end;

end.
