�
 TFORM_FIRMENDATEN 0�,  TPF0TForm_firmendatenForm_firmendatenLeft� Top� BorderIcons BorderStylebsDialogCaptionFirmendatenClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStylefsStayOnTopOldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TToolBarToolBar_firmaLeft Top Width�HeightCaptionToolBar_firmaImagesForm_main.ImageList_mainTabOrder  TToolButtonToolButton_neuLeft TopHint
NeuanlegenCaptionToolButton_neu
ImageIndex OnClickToolButton_neuClick  TToolButtonToolButton2LeftTopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeftTopHint	Ver�ndernCaptionToolButton_edit
ImageIndexParentShowHintShowHint	OnClickToolButton_editClick  TToolButtonToolButton4Left6TopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeft>TopHint	SpeichernCaptionToolButton_save
ImageIndexParentShowHintShowHint	OnClickToolButton_saveClick  TToolButtonToolButton6LeftUTopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_delLeft]TopHintL�schenCaptionToolButton_del
ImageIndexParentShowHintShowHint	OnClickToolButton_delClick  TToolButtonToolButton8LefttTopWidthCaptionToolButton8
ImageIndexStyletbsSeparator  TToolButtonToolButton_cancelLeft|TopHint	VerwerfenCaptionToolButton_cancel
ImageIndexParentShowHintShowHint	OnClickToolButton_cancelClick  TToolButtonToolButton11Left� TopWidthCaptionToolButton11
ImageIndexStyletbsSeparator  TToolButtonToolButton_exitLeft� TopHint	Schlie�enCaptionToolButton_exit
ImageIndexParentShowHintShowHint	OnClickToolButton_exitClick   TPageControlPageControl_mainLeft Top9Width�Height�
ActivePageTabSheet_AnsprechpartnerAlignalClientTabOrderOnChangePageControl_mainChange
OnChangingPageControl_mainChanging 	TTabSheetTabSheet_AdresseCaptionAdresse 	TGroupBoxGroupBox_firmaLeft Top Width� Height�AlignalLeftCaptionFirmaTabOrder  TPanel
Panel_auswLeftTopmWidth� HeightAlignalBottomTabOrder   
TDBgridEXT
DBgridEXT1LeftTopWidth� Height^AlignalClientColorclInfoBk
DataSourceDataModule1.DataSource_FirmaDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameFirmaWidth� Visible	     TPanelPanel5Left� Top Width�Height�AlignalClientTabOrder TLabelLabel1Left(TopWidthHeightCaptionFirma  TLabelLabel2Left Top� WidthHeightCaptionFAX:  TLabelLabel3Left%Top� Width$HeightCaptionTelefon  TLabelLabel4Left� TopkWidthHeightCaptionOrt  TLabelLabel5Left2TopmWidthHeightCaptionPLZ  TLabelLabel6Left TopBWidth#HeightCaptionStrasse  TLabelLabel7LeftTop� WidthHeightCaptionwww  TLabelLabel8Left.Top� WidthHeightCaptione-mail  TDBEditDBEdit_firmaLeftVTopWidth� Height	DataFieldFirma
DataSourceDataModule1.DataSource_FirmaTabOrder   TDBEditDBEdit_telefonLeftVTop� Width� Height	DataFieldTelefon
DataSourceDataModule1.DataSource_FirmaTabOrder  TDBEditDBEdit_homepageLeft Top� Width� Height	DataFieldH_page
DataSourceDataModule1.DataSource_FirmaTabOrder  TDBEdit
DBEdit_plzLeftVTophWidthAHeight	DataFieldPlz
DataSourceDataModule1.DataSource_FirmaTabOrder  TDBEditDBEdit_strasseLeftVTop@Width� Height	DataFieldStrasse
DataSourceDataModule1.DataSource_FirmaTabOrder  TDBEdit
DBEdit_ortLeft� TophWidth� Height	DataFieldOrt
DataSourceDataModule1.DataSource_FirmaTabOrder  TDBEdit
DBEdit_faxLeft Top� Width� Height	DataFieldFax
DataSourceDataModule1.DataSource_FirmaTabOrder  TDBEditDBEdit_emailLeftVTop� Width� Height	DataFieldEmail
DataSourceDataModule1.DataSource_FirmaTabOrder  	TGroupBox	GroupBox1LeftTop� Width�Height� AlignalBottomCaptionAnmerkungenTabOrder TDBMemoDBMemo1LeftTopWidth�Height� AlignalClient	DataFieldMemo
DataSourceDataModule1.DataSource_FirmaTabOrder      	TTabSheetTabSheet_AnsprechpartnerCaptionAnsprechpartner
ImageIndex 	TGroupBoxGroupBox_ansprechLeft Top Width� Height�AlignalLeftCaptionAnsprechpartnerTabOrder  TPanelPanel3LeftTopeWidth� Height!AlignalBottomTabOrder   
TDBgridEXT
DBgridEXT2LeftTopWidth� HeightVAlignalClientColorclInfoBk
DataSource%DataModule1.DataSource_firma_ansprechDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameFunktionWidthNVisible	 Expanded	FieldNameNameWidth� Visible	     TPanelPanel4Left� Top Width�Height�AlignalClientTabOrder TLabelLabel9Left(Top Width,HeightCaption	Funktion:  TLabelLabel11Left(TopxWidth$HeightCaptionTelefon  TLabelLabel12Left(TopPWidthHeightCaptionName  TDBEditDBEdit_funktionLeftnTop Width� Height	DataFieldFunktion
DataSource%DataModule1.DataSource_firma_ansprechTabOrder   	TGroupBoxGroupBox_ansp_anmerkLeftTop� Width�Height� AlignalBottomCaptionAnmerkungenTabOrder TDBMemoDBMemo_an_memoLeftTopWidth�Height� AlignalClient	DataFieldMemo
DataSource%DataModule1.DataSource_firma_ansprechTabOrder    TDBEditDBEdit_nameLeftpTopPWidth� Height	DataFieldName
DataSource%DataModule1.DataSource_firma_ansprechTabOrder  TDBEditDBEdit_an_telefonLeftpTopxWidth� Height	DataFieldTelefon
DataSource%DataModule1.DataSource_firma_ansprechTabOrder    	TTabSheetTabSheet_zeitCaptionEinsatzzeiten
ImageIndex TPageControlPageControl_zeitLeft Top Width�HeightI
ActivePageTabSheet_sollAlignalClientTabOrder  	TTabSheetTabSheet_sollCaptionSoll 	TGroupBox	GroupBox2Left Top Width� Height-AlignalLeftTabOrder  
TDBgridEXT
DBgridEXT3LeftTopWidth� HeightAlignalClientColorclInfoBk
DataSource&DataModule1.DataSource_firma_zeit_sollDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameDatumWidth_Visible	 Expanded	FieldNameStundenWidthRVisible	     TPanelPanel6Left� Top Width�Height-AlignalClient
BevelOuterbvNoneTabOrder TLabelLabel10Left8Top WidthHeightCaptionDatum  TLabelLabel13Left8Top^Width;HeightCaptionStundenzahl  TSpeedButtonSpeedButton1LeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton1Click  TLabelLabel16Left� TopHWidth(HeightCaptionStunden  TLabelLabel17Left� TopHWidth&HeightCaptionMinuten  TDBEditDBEdit1Left� TopWidthyHeight	DataFieldDatum
DataSource&DataModule1.DataSource_firma_zeit_sollTabOrder   	TGroupBox	GroupBox5Left Top� Width�Height� AlignalBottomCaption	AnmerkungTabOrder TDBMemoDBMemo3LeftTopWidth�HeightpAlignalClient	DataFieldMemo
DataSource&DataModule1.DataSource_firma_zeit_sollTabOrder    TDBEditDBEdit_sollhLeft� TopXWidth)Height	DataFieldStunden
DataSource&DataModule1.DataSource_firma_zeit_sollTabOrder  TDBEditDBEdit_sollmLeft� TopXWidth)Height	DataFieldMinuten
DataSource&DataModule1.DataSource_firma_zeit_sollTabOrder    	TTabSheetTabSheet_istCaptionIst
ImageIndex 	TGroupBox	GroupBox3Left Top Width� Height-AlignalLeftTabOrder  
TDBgridEXT
DBgridEXT4LeftTopWidth� HeightAlignalClientColorclInfoBk
DataSource%DataModule1.DataSource_firma_zeit_istDefaultDrawing	OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldNameDatumWidthMVisible	 Expanded	FieldNameStundenWidth`Visible	     TPanelPanel7Left� Top Width�Height-AlignalClient
BevelOuterbvNoneTabOrder TLabelLabel14Left0Top WidthHeightCaptionDatum  TLabelLabel15Left(TopXWidthRHeightCaptionAnwesenheitszeit  TSpeedButtonSpeedButton2LeftTop!WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton2Click  TLabelLabel18Left� TopHWidth&HeightCaptionMinuten  TLabelLabel19Left� TopHWidth(HeightCaptionStunden  	TGroupBox	GroupBox4Left Top� Width�Height� AlignalBottomCaption	AnmerkungTabOrder  TDBMemoDBMemo2LeftTopWidth�HeightxAlignalClient	DataFieldMemo
DataSource%DataModule1.DataSource_firma_zeit_istTabOrder    TDBEditDBEdit2Left� Top WidthyHeight	DataFieldDatum
DataSource%DataModule1.DataSource_firma_zeit_istTabOrder  TDBEditDBEdit_isthLeft� TopXWidth)Height	DataFieldStunden
DataSource%DataModule1.DataSource_firma_zeit_istTabOrder  TDBEditDBEdit_istmLeft� TopXWidth)Height	DataFieldMinuten
DataSource%DataModule1.DataSource_firma_zeit_istTabOrder     	TGroupBox	GroupBox6Left TopIWidth�Height?AlignalBottomCaptionZeit-AbrechnungTabOrder TLabelLabel_sollgesamtLeftTopWidth?HeightCaption                       TLabelLabel_istgesamtLeft� TopWidth?HeightCaption                       TLabelLabel_fehltLeft�TopWidth?HeightCaption                          TPanelPanel2Left TopWidth�HeightAlignalTop
BevelInnerbvRaised
BevelOuterbvNoneBorderStylebsSingleTabOrder TLabelLabel_firmaLeftTopWidth6HeightCaptionLabel_firma    