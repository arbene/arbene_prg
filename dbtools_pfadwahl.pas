unit dbtools_pfadwahl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl;

type
  TForm_dbpfadwahl = class(TForm)
    DriveComboBox: TDriveComboBox;
    DirectoryListBox: TDirectoryListBox;
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    Edit_uv: TEdit;
    Button_anlegen: TButton;
    Label1: TLabel;
    procedure Button_anlegenClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_dbpfadwahl: TForm_dbpfadwahl;

implementation

{$R *.dfm}

procedure TForm_dbpfadwahl.Button_anlegenClick(Sender: TObject);
var
dir:string;
begin
	dir:=getcurrentdir;
	if setcurrentdir(directorylistbox.Directory) then createdir(edit_uv.text);
	setcurrentdir(dir);
  directorylistbox.update;
end;

end.
