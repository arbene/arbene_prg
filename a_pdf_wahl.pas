unit a_pdf_wahl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl;

type
  TForm_pdfwahl = class(TForm)
    FileListBox: TFileListBox;
    Panel1: TPanel;
    BitBtn_ok: TBitBtn;
    BitBtn_abb: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ini(pfad,maske,titel:string);
    procedure FileListBoxDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_pdfwahl: TForm_pdfwahl;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_pdfwahl.FormCreate(Sender: TObject);
var
s:string;
begin
form_main.form_positionieren(tform(sender));
//s:=format('%0:s*.pdf; %0:s*.html ',[infoverzeichnis]);   {; %0:s*.html; %0:s*.doc; %0:s*.xls}

//filelistbox.Directory:=pfad_infotexte;
filelistbox.mask:='*.pdf;*.html;*.doc;*.xls';
//label1.Caption:='Die Dateien finden sich unter: '+pfad_infotexte;

end;

procedure TForm_pdfwahl.FileListBoxDblClick(Sender: TObject);
begin
     modalresult:=mrok;
end;

procedure TForm_pdfwahl.FormShow(Sender: TObject);
begin
filelistbox.itemindex:=0;
end;

procedure TForm_pdfwahl.ini(pfad,maske,titel:string);
begin
   filelistbox.Directory:=pfad;
   label1.Caption:='Die Dateien finden sich unter: '+pfad;
   filelistbox.mask:=maske;//'*.pdf;*.html;*.doc;*.xls';
   self.caption:=titel;
end;


end.
