unit a_verschieb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_data,
  Grids, DBGrids, dbgridEXT, ExtCtrls, StdCtrls, Buttons;

type
  TForm_verschieb = class(TForm)
    Panel1: TPanel;
    DBgridEXT_firma: TDBgridEXT;
    Panel2: TPanel;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel4: TPanel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DBgridEXT_firmaDblClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_verschieb: TForm_verschieb;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_verschieb.FormCreate(Sender: TObject);
begin
	grid_einstellen(dbgridext_firma);
	form_main.form_positionieren(tform(sender));
	if prg_typ=3 then
	begin
		label1.Caption:='Bitte neue Kunden-Gruppe w�hlen f�r:    '+form_main.ComboBox_history.Text;
		dbgridext_firma.Columns[0].title.caption:='Kunden-Gruppe';
	end
	else
	label1.Caption:='Bitte neue Firma w�hlen f�r:    '+form_main.ComboBox_history.Text;
end;

procedure TForm_verschieb.DBgridEXT_firmaDblClick(Sender: TObject);
begin
	modalresult:=mrok;
end;

end.
