unit a_schicht;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons, ExtCtrls,a_main;


type
  TForm_schicht = class(TForm)
    Grid: TStringGrid;
    Panel_bottom: TPanel;
    BitBtn_exit_ok: TBitBtn;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
  private
    { Private-Deklarationen }
    schicht_dat:array[1..31] of string;
    datum, datum_synk,monatsbeginn:tdate;
    s_name, s_vorname:string;
    s_schicht:string  ;
    first:integer;
	  function max_dat(datum:tdatetime):integer;
	  function wochentag(datum:tdatetime): integer;
    procedure schicht_berechnen;
    function zellendatum(x,y:integer):tdate;
  public
    { Public-Deklarationen }
    sel_date:tdate;
    procedure ini(nname,vorname:string;datum_jetzt, datum_s:tdate;string_schicht:string);
    procedure zeichne;
  end;

var
  Form_schicht: TForm_schicht;
  days: array[1..7] of string = ('Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag','Sonntag');
implementation

{$R *.DFM}

procedure TForm_schicht.FormCreate(Sender: TObject);
begin
 //drawgrid1.CellRect(

 form_main.form_positionieren(tform(sender));
end;


function TForm_schicht.max_dat(datum:tdatetime):integer;
var
year, month, day:word;
tage: array[1..12] of integer;
monatalt:integer;
begin
tage[1]:=31;
tage[2]:=28;
tage[3]:=31;
tage[4]:=30;
tage[5]:=31;
tage[6]:=30;
tage[7]:=31;
tage[8]:=31;
tage[9]:=30;
tage[10]:=31;
tage[11]:=30;
tage[12]:=31;
decodedate(datum, year, month,day);
result:=tage[month];
if month=2 then
begin
	datum:=encodedate(year, month, 28);
	monatalt:=month;
	datum:=datum+1;
	decodedate(datum, year, month,day);
	if month=monatalt then result:=29;
end;

end;

procedure tform_schicht.ini(nname,vorname:string;datum_jetzt, datum_s:tdate;string_schicht:string);
var day, month, year:word;
begin
s_name:=nname;
s_vorname:=vorname;
decodedate(datum_jetzt,year, month, day);
datum:=encodedate(year,month,1);
datum_synk:=datum_s;
s_schicht:=string_schicht;
end;


procedure tform_schicht.zeichne;
var i,j,k,last, tagebismbeginn,woche:integer;
day, month, year:word;
jahresbeginn:tdate;
begin

caption:='Schichtplan f�r '+s_name+' , '+s_vorname +'    '+formatdatetime('mmmm  -  yyyy',datum);

schicht_berechnen;

decodedate(datum,year, month, day);
jahresbeginn:=encodedate(year,1,1);
monatsbeginn:=encodedate(year,month,1);
tagebismbeginn:=trunc(monatsbeginn-jahresbeginn);
woche:=tagebismbeginn div 7 +1;
first:=wochentag(monatsbeginn);
last:=max_dat(datum);

for i:=1 to 7 do
	for j:=1 to 6 do grid.Cells[i,j]:='';

for i:=1 to 7 do
begin
	grid.Cells[i,0]:=days[i];
end;
for i:=1 to 6 do
begin
  grid.Cells[0,i]:=format('KW: %d',[woche]);
  inc(woche);
end;

j:=1;
k:=first-1;
for i:=1 to last do
begin
  grid.Cells[i+k,j]:=format('%d - %s',[i,schicht_dat[i]]);
  if i+k>=7 then
  begin
  		inc(j);
        k:=k-7;
  end;
end;

end;


function tform_schicht.wochentag(datum:tdatetime): integer;
begin
result:=dayofweek(datum)-1;
if result=0 then result:=7;

end;

procedure tform_schicht.schicht_berechnen;
var max_day_mon_vorher,laenge,dday, day_bis_monat,i,j:integer;
    zyk_beginn:tdate;
    day, month,year:word;
begin
laenge:=length(s_schicht);
while datum< datum_synk do datum_synk:=datum_synk-laenge;
dday:=trunc(datum-datum_synk);
zyk_beginn:=datum_synk+laenge*(dday div laenge);
max_day_mon_vorher:=max_dat(zyk_beginn);
decodedate(zyk_beginn,year,month,day);
day_bis_monat:=max_day_mon_vorher-day;
j:=day_bis_monat+2;
for i:=1 to 31 do
begin
   if j>laenge then j:=1;
   Schicht_dat[i]:=copy(s_schicht,j,1);
   inc(j);
end;

end;

procedure TForm_schicht.SpeedButton1Click(Sender: TObject);

var day, month, year:word;
begin
decodedate(datum,year, month, day);
if month=12 then
begin
	month:=0;
  inc(year);
end;
datum:=encodedate(year,month+1,1);
zeichne;
end;

procedure TForm_schicht.SpeedButton2Click(Sender: TObject);
var day, month, year:word;
begin
decodedate(datum,year, month, day);
if month=1 then
begin
	month:=13;
  dec(year);
end;
datum:=encodedate(year,month-1,1);
zeichne;

end;

procedure TForm_schicht.GridDblClick(Sender: TObject);
var
x,y:integer;
begin
  y:=grid.Row;
  x:=grid.Col;
  if (x<1) or (y<1 ) then exit;
  if grid.Cells[x,y]='' then exit;

  sel_date:=zellendatum(x,y );
  modalresult:=mryes;
end;

function tform_schicht.zellendatum(x,y:integer):tdate;
begin
   if y=1 then result:=monatsbeginn+ (x-first)
   else result:=monatsbeginn+  (7-first) + (y-2)*7+x;
end;

end.
