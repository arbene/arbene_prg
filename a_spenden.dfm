object Form_spenden: TForm_spenden
  Left = 715
  Top = 244
  Width = 682
  Height = 679
  Caption = 'Arbene freischalten'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 52
    Top = 393
    Width = 420
    Height = 13
    Caption = 
      '2.) Geben Sie den Freischaltcode ein, der Ihnen nach der Paypal-' +
      'Spende angezeigt wird.'
  end
  object Label2: TLabel
    Left = 52
    Top = 312
    Width = 269
    Height = 13
    Caption = '1.) Spenden Sie '#252'ber PayPal oder per Bank'#252'berweisung.'
  end
  object Label3: TLabel
    Left = 66
    Top = 416
    Width = 277
    Height = 13
    Caption = 'Der Code muss auf dem Hauptsystem eingegeben werden.'
  end
  object Button_paypal: TButton
    Left = 52
    Top = 344
    Width = 433
    Height = 25
    Caption = 'Informationen '#252'ber die Projekte und Spendenm'#246'glichkeiten'
    TabOrder = 0
    OnClick = Button_paypalClick
  end
  object Edit: TEdit
    Left = 65
    Top = 437
    Width = 265
    Height = 21
    TabOrder = 1
  end
  object Panel_b: TPanel
    Left = 0
    Top = 604
    Width = 666
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object BitBtn_ok: TBitBtn
      Left = 115
      Top = 7
      Width = 118
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtn_esc: TBitBtn
      Left = 333
      Top = 7
      Width = 118
      Height = 25
      Caption = 'Abbruch'
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object RichEdit1: TRichEdit
    Left = 52
    Top = 20
    Width = 441
    Height = 267
    Color = cl3DLight
    Lines.Strings = (
      
        'Die Nutzungslizenz von Arbene ist Charityware und nicht Freeware' +
        ', '
      ''
      
        'Dies bedeutet, dass Sie selber festlegen, wie viel Ihnen die Nut' +
        'zung von Arbene '
      'wert ist und diesen Betrag dann spenden. '
      ''
      'Die Spende kommt  den Gr'#252'nhelmen zu gute.'
      ''
      'Die Spende gilt immer f'#252'r ein Kalenderjahr.'
      ''
      
        'Nach der Paypal-Spende , bzw. falls Sie den Betrag '#252'berweisen, e' +
        'rhalten Sie einen '
      
        'Freischaltcode, um das Warte-Fenster beim Programmstart f'#252'r das ' +
        'aktuelle Jahr  zu '
      'verbergen. Der Code muss auf dem Hautpsystem  eingeben werden.'
      ''
      '')
    ScrollBars = ssBoth
    TabOrder = 3
  end
end
