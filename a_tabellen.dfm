�
 TFORM_TABELLEN 0��  TPF0TForm_tabellenForm_tabellenLeft�Top� BorderIconsbiSystemMenu BorderStylebsDialogCaptionTabellenClientHeightmClientWidth-Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight TToolBarToolBarLeft Top Width-Height(AutoSize	ButtonHeight$ButtonWidth5CaptionToolBarImagesForm_main.ImageList_mainShowCaptions	TabOrder  TToolButtonToolButton_neuLeft TopHintNeuCaptionNeu
ImageIndex ParentShowHintShowHint	OnClickToolButton_neuClick  TToolButtonspace1Left5TopWidthCaptionspace1
ImageIndexStyletbsSeparator  TToolButtonToolButton_editLeft=TopHint
   VerändernCaption   Ändern
ImageIndexParentShowHintShowHint	OnClickToolButton_editClick  TToolButtonspace2LeftrTopWidthCaptionspace2
ImageIndexStyletbsSeparator  TToolButtonToolButton_saveLeftzTopHint	SpeichernCaptionSichern
ImageIndexParentShowHintShowHint	OnClickToolButton_saveClick  TToolButtonspace3Left� TopWidthCaptionspace3
ImageIndexStyletbsSeparator  TToolButtonToolButton_delLeft� TopHint   LöschenCaption   LöschenEnabled
ImageIndexParentShowHintShowHint	OnClickToolButton_delClick  TToolButtonToolButton2Left� TopWidthCaptionToolButton2
ImageIndexStyletbsSeparator  TToolButtonToolButton_quitLeft� TopHint	VerlassenCaption	Verlassen
ImageIndexParentShowHintShowHint	OnClickToolButton_quitClick  TToolButtonToolButton1Left4TopWidthOCaptionToolButton1
ImageIndexStyletbsSeparator   	TNotebookNotebookLeftITop(Width�Height AlignalClient	PageIndexTabOrder TPage Left Top Captionbitmaps TSpeedButtonSpeedButton6Left�Top�WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsVisibleOnClickSpeedButton_upClick  TSpeedButtonSpeedButton7Left�Top�WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsVisibleOnClickSpeedButton_downClick  TBitBtnBitBtn_ladenLeftpTop� WidthqHeightCaptionGraphik ladenTabOrder OnClickBitBtn_bwahl1Click  TDBImageDBImageLeftxToppWidthQHeightI
DataSourceDataModul.ds_1Stretch	TabOrder  TDBEditDBEdit_bild_beschreibungLeft(Top8Width� Height
DataSourceDataModul.ds_1TabOrder   TPage Left Top CaptionUntersucher TPanelPanel3Left Top Width�Height AlignalClient
BevelOuterbvNoneTabOrder  TLabelLabel1LeftTop@Width� HeightCaptionUntersucher  Stempel Zeile 1  TSpeedButtonSpeedButton_upLeft�Top�WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsVisibleOnClickSpeedButton_upClick  TSpeedButtonSpeedButton_downLeft�Top�WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsVisibleOnClickSpeedButton_downClick  TLabelLabel69LeftTop~WidthZHeightCaptionAbrechnungskonto  TLabelLabel79LeftTopnWidth� HeightCaptionUntersucher Stempel Zeile 2  TLabelLabel80LeftTop� Width� HeightCaptionUntersucher Stempel Zeile 3  TLabelLabel81LeftTop� Width� HeightCaptionUntersucher Stempel Zeile 4  TLabelLabel82LeftTop� WidthdHeightCaptionemail Stempel Zeile 5  TLabelLabel83LeftTop$WidthpHeightCaptionTelefon Stempel Zeile 6  TLabelLabel84Left_TopAWidth=HeightCaptionFormatierung  TLabelLabel85Left_TopoWidth=HeightCaptionFormatierung  TLabelLabel86Left_Top� Width=HeightCaptionFormatierung  TLabelLabel87Left_Top� Width=HeightCaptionFormatierung  TLabelLabel88Left_Top� Width=HeightCaptionFormatierung  TLabelLabel89Left_Top%Width=HeightCaptionFormatierung  TDBCombo_numDBCombo_num_zeitkontoLeftTop�Width� Heightalle_einfuegenkeines_einfuegen
DataSourceDataModul.ds_1zconnectionDataModul.connection_main	ztab_nameabr_zeitkontozfield_textnamezfield_indexnummerzorderreihenfolgezstorno	
ItemHeightTabOrder  TDBEditDBEdit_untersucherLeftTopOWidth9Height
DataSourceDataModul.ds_1TabOrder   TBitBtnBitBtn_bwahlLeftTop�WidthiHeightCaption   Bild auswählenTabOrderOnClickBitBtn_bmpClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333   33��wwws�0  wwwp37ww?���3ww   3??wwws?��p 33 ss7w?3wwww�0�3333�7���� p?��3w7  ���ww337�����33337www� �3?�3ws�� �0�s�w�s770   33 7wsww33w30�333333�7333330333337ss33333�333333s��333330�3333337733333333333333s333333	NumGlyphs  TDBEditDBEdit_untersucher2LeftTop|Width9Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_untersucher3LeftTop� Width9Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_untersucher4LeftTop� Width9Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_untersucher_emailLeftTopWidth9Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_untersucher_telefonLeftTop2Width9Height
DataSourceDataModul.ds_1TabOrder
  TDBComboBoxDBComboBox_u1Left_TopNWidth;Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsH1H2H3H4H5H6 TabOrder  TDBComboBoxDBComboBox_u2Left_Top|Width;Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsH1H2H3H4H5H6 TabOrder  TDBComboBoxDBComboBox_u3Left_Top� Width;Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsH1H2H3H4H5H6 TabOrder  TDBComboBoxDBComboBox_u4Left_Top� Width;Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsH1H2H3H4H5H6 TabOrder  TDBComboBoxDBComboBox_u5Left_TopWidth;Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsH1H2H3H4H5H6 TabOrder	  TDBComboBoxDBComboBox_u6Left_Top2Width;Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsH1H2H3H4H5H6 TabOrder    TPage Left Top CaptionUntersuchungsart TLabelLabel2LeftTop8WidtheHeightCaptionArt der Untersuchung  TSpeedButtonSpeedButton1LeftTop� WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsVisibleOnClickSpeedButton_upClick  TSpeedButtonSpeedButton2LeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsVisibleOnClickSpeedButton_downClick  TDBEditDBEdit_untersuchungLeftTop_Width� Height
DataSourceDataModul.ds_1TabOrder   TBitBtnBitBtn1LeftTop� WidthjHeightCaption   Bild auswählenTabOrderOnClickBitBtn_bmpClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333   33��wwws�0  wwwp37ww?���3ww   3??wwws?��p 33 ss7w?3wwww�0�3333�7���� p?��3w7  ���ww337�����33337www� �3?�3ws�� �0�s�w�s770   33 7wsww33w30�333333�7333330333337ss33333�333333s��333330�3333337733333333333333s333333	NumGlyphs   TPage Left Top CaptionFristen TLabelLabel3LeftTopHWidthGHeightCaptionFrist in  Worten  TLabelLabel4LeftTop� Width@HeightCaptionFrist in Tagen  TDBEditDBEdit_frist1LeftTop_Width� Height
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_frist2LeftTop� WidthAHeight
DataSourceDataModul.ds_1TabOrder   TPage Left Top CaptionStatus TLabelLabel5LeftTop8WidthHeightCaptionStatus  TDBEditDBEdit_statusLeftTopWWidthHeight
DataSourceDataModul.ds_1TabOrder   TBitBtnBitBtn_colorLeftTop� Width� HeightCaption   Farbe wählenTabOrderOnClickBitBtn_colorClick
Glyph.Data
:  6  BM6      6  (                                      �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� ����������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������  TButtonButton1Left� Top� WidthKHeightCaptionfarblosTabOrderOnClickButton1Click   TPage Left Top Captionschicht TLabelLabel6LeftTop8WidthCHeightCaptionSchicht-Name  TLabelLabel7LeftTop� WidthHHeightCaptionSchicht-Verlauf  TLabelLabel8LeftTop� Width� HeightCaptionz-B- "FFFFF--SSSSS--NNNNN"Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont  TLabelLabel24LeftTopxWidthDHeightCaption   Schicht-Kürzel  TDBEditDBEdit_s_nameLeftTopHWidthHeight
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_s_schichtLeftTop� WidthHeight
DataSourceDataModul.ds_1Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTabOrder  TDBEditDBEdit_s_kuerzelLeftTop� WidthYHeight
DataSourceDataModul.ds_1TabOrder   TPage Left Top Caption	Abteilung  TPage Left Top CaptionAdressen TLabelLabel11LeftTopWidthHeightCaptionName  TLabelLabel12LeftTop:Width#HeightCaptionStrasse  TLabelLabel13LeftTop8WidthHeightCaptionMemo  TLabelLabel14LeftTop� WidthHeightCaptione-mail  TLabelLabel15Left� Top� WidthHeightCaptionFax  TLabelLabel16LeftTop� Width$HeightCaptionTelefon  TLabelLabel17Left`TopbWidthHeightCaptionOrt  TLabelLabel18LeftTopbWidthHeightCaptionPLZ  TLabelLabel19LeftTopWidth4HeightCaptionHomepage  TLabelLabel54Left� Top:Width*HeightCaptionPostfach  TLabelLabel_kundennummerLeftTop� WidthJHeightCaptionKundennummer  TDBEditDBEdit_nameLeftTop Width9Height
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_strasseLeftTopHWidth� Height
DataSourceDataModul.ds_1TabOrder  TDBEdit
DBEdit_plzLeftToppWidthAHeight
DataSourceDataModul.ds_1TabOrder  TDBEdit
DBEdit_ortLeft`ToppWidth� Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_telefonLeftTop� Width� Height
DataSourceDataModul.ds_1TabOrder  TDBEdit
DBEdit_faxLeft� Top� Width� Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_emailLeftTop� Width9Height
DataSourceDataModul.ds_1TabOrder  TDBMemoDBMemo_memoLeftTopHWidth9Height9
DataSourceDataModul.ds_1TabOrder
  TDBEdit
DBEdit_wwwLeftTopWidth9Height
DataSourceDataModul.ds_1TabOrder	  TDBEditDBEdit_postfachLeft� TopHWidthiHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_kundennummerLeftTop� Width9Height
DataSourceDataModul.ds_1TabOrder   TPage Left Top CaptionAbfragen TLabelLabel22Left Top/WidthVHeightCaptionName der Abfrage  TLabelLabel23Left TopcWidthAHeightCaptionBeschreibung  TDBEditDBEdit_abfragen_nameLeft Top?WidthHeight
DataSourceDataModul.ds_1TabOrder   TDBMemoDBMemo_abfragenLeft!Top{WidthHeight� 
DataSourceDataModul.ds_1TabOrder   TPage Left Top CaptionTaetigkeiten TLabelLabel25LeftTop@Width)HeightCaption
   Tätigkeit  TLabelLabel26LeftTop� Width[HeightCaptionEinsatzzeit pro Jahr  TLabelLabel99LeftTop� Width(HeightCaptionBranche  TBitBtnBitBtn_taetig_lesenLeftTop^WidthHeightCaption.   Tätigkeiten in den Probanden-Stammdaten lesenTabOrderOnClickBitBtn_taetig_lesenClick  TDBEditDBEdit_taetigkeitLeftTopPWidth!Height
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_einsatzzeitLeftTop� WidthTHeight
DataSourceDataModul.ds_1TabOrder  TDBCombo_numDBCombo_taet_brancheLeftTop� Width� Heightalle_einfuegen	keines_einfuegen
DataSourceDataModul.ds_1zconnectionDataModul.connection_main	ztab_namebranchezfield_textbranchezfield_indexnummerzorderbranchezstorno	
ItemHeightTabOrder  TButtonbutton_zusammenfuehrenLeftTop�WidthHeightCaption*   Namensgleiche Tätigkeiten zusammenführenTabOrderOnClickbutton_zusammenfuehrenClick   TPage Left Top Captionaerzte TPanelPanel5Left Top!Width�HeightAlignalClient
BevelOuterbvNoneTabOrder  TLabelLabel27Left6Top� Width*HeightCaptionVorname  TLabelLabel28Left?TopWidth#HeightCaptionStrasse  TLabelLabel29LeftCTop�WidthHeightCaptionMemo  TLabelLabel30LeftJTop]WidthHeightCaptione-mail  TLabelLabel31LeftSTopzWidthHeightCaptionFax  TLabelLabel32LeftCTopBWidth$HeightCaptionTelefon  TLabelLabel33Left� Top$WidthHeightCaptionOrt  TLabelLabel34LeftPTop$WidthHeightCaptionPLZ  TLabelLabel35Left2Top�Width4HeightCaptionHomepage  TLabelLabel36LeftCTop� WidthHeightCaptionName  TLabelLabel37LeftNTop� WidthHeightCaptionTitel  TLabelLabel38Left.Top|Width5HeightCaption
Fachgebiet  TLabelLabel59LeftKTop� WidthHeightCaptionz.Hd.  TLabelLabel90LeftTopWidthAHeightCaptionPraxisadresse  TDBEditDBEdit_avornameLeftkTop� WidthCHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_astrasseLeftkTopWidthEHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_aplzLeftkTop WidthAHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_aortLeft� Top Width� Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_atelefonLeftkTop?WidthEHeight
DataSourceDataModul.ds_1TabOrder	  TDBEditDBEdit_afaxLeftkTopwWidthEHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_aemailLeftkTopZWidthEHeight
DataSourceDataModul.ds_1TabOrder
  TDBMemoDBMemo_amemoLeftjTop�WidthDHeight7
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_awwwLeftkTop�WidthEHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_anameLeftkTop� WidthEHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_atitelLeftlTop� Width� Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_afachLeftkTopyWidthEHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_azhdLeftkTop� WidthEHeight
DataSourceDataModul.ds_1TabOrder  TDBMemoDBMemo_p_nameLeftjTopWidthDHeightO
DataSourceDataModul.ds_1TabOrder    TPanelPanel_VCARDLeft Top Width�Height!AlignalTop
BevelOuterbvNoneTabOrder TBitBtnBitBtn2LeftTop
WidthHHeightCaptionVcard lesenTabOrder OnClickBitBtn2Click    TPage Left Top Captiongoae TLabelLabel9Left#TopWidthHeightCaption   GOÄ  TLabelLabel21Left#Top� Width8HeightCaptionnicht neben  TLabelLabel40Left� TopWidth"HeightCaptionPunkte  TLabelLabel10Left#TopSWidth/HeightCaption	   GOÄ-Text  TDBEditDBEdit_goae_nrLeft#Top.WidthAHeight
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_goae_nnLeft"Top� Width� Height
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_goae_punkteLeft� Top.WidthFHeight
DataSourceDataModul.ds_1TabOrder  TDBRadioGroupDBRadioGroup_goae_kategorieLeft Top� Width� HeightyCaption	Kategorie
DataSourceDataModul.ds_1Items.Strings   Ärztliche LeistungTechnische LeistungLabor Leistung1,0-facher Satz TabOrderValues.Strings0123   TDBEditDBEdit_goae_textLeft"TopfWidth� Height
DataSourceDataModul.ds_1TabOrder  TButtonButton_goaeLeft ToppWidthqHeightHintf   In Untersuchung, Impfung und Labor werden noch nicht aufgeführte GOÄ-Ziffern gesucht und eingetragenCaption   neue GOÄ suchenParentShowHintShowHint	TabOrderOnClickButton_goaeClick   TPage Left Top Captionmaterialstammm TLabelLabel20LeftTopWidthBHeightCaptionArtikelnummer  TLabelLabel39LeftTop4Width7HeightCaptionArtikelname  TLabelLabel41LeftTopaWidthAHeightCaptionBeschreibung  TLabelLabel42LeftTop� Width)HeightCaption	Lieferant  TLabelLabel43LeftTop� WidthHeightCaptionInhalt  TLabelLabel44LeftTop� Width?HeightCaptionEinkaufspreis  TLabelLabel45Left� Top� Width]HeightCaptionempf. Verkaufspreis  TLabelLabel46Left� Top� Width@HeightCaptionVerkaufspreis  TLabelLabel48LeftTop#WidthAHeightCaptionLagerbestand  TLabelLabel49Left� Top"WidthKHeightCaptionMindestbestand  TLabelLabel50Left� Top"Width!HeightCaptionbestellt  TLabelLabel51LeftTopRWidthcHeightCaptionMindestbestellmenge  TLabelLabel52Left� TopRWidth^HeightCaptionVerpackungseinheit  TLabelLabel53Left� TopWidth2HeightCaptionEAN-CodeVisible  TLabelLabel47LeftoTopWidthHeightCaption   �   TLabelLabel55Left� TopWidthHeightCaption   �   TLabelLabel56LeftATopWidthHeightCaption   �   TDBEditDBEdit_artnrLeftTopWidthyHeight
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_ar_nameLeftTopCWidthIHeight
DataSourceDataModul.ds_1TabOrder  TDBMemo
DBMemo_artLeftTopoWidthIHeight*
DataSourceDataModul.ds_1
ScrollBars
ssVerticalTabOrder  TDBCombo_numDBCombo_lieferantLeftTop� WidthIHeightalle_einfuegenkeines_einfuegen
DataSourceDataModul.ds_1zconnectionDataModul.connection_main	ztab_namelieferantenzfield_textnamezfield_indexnummerzordernamezstorno
ItemHeightTabOrder  TDBEditDBEdit_inhaltLeftTop� WidthqHeight
DataSourceDataModul.ds_1TabOrder  TDBEdit
DBEdit_ekpLeftTopWidthSHeight
DataSourceDataModul.ds_1TabOrder  TDBComboBoxDBComboBox_einheitLeft� Top� Width� Height
DataSourceDataModul.ds_1
ItemHeightItems.StringsPackung   Stück	TablettenBrausetabletteLutschtabletteDrageeKapselGranulatTubeDosieraerosolcmmlLiterGramm TabOrder  TDBEditDBEdit_evkpLeft� TopWidthSHeight
DataSourceDataModul.ds_1TabOrder  TDBEdit
DBEdit_eanLeft� TopWidthyHeight
DataSourceDataModul.ds_1TabOrderVisible  TDBEditDBEdit_lagerLeftTop2WidthUHeight
DataSourceDataModul.ds_1TabOrder
  TDBEditDBEdit_lagermindLeft� Top1WidthUHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_lagerbestLeft� Top1WidthUHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_mindbestLeftTopaWidthYHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_verpackungseinheitLeft� TopaWidthYHeight
DataSourceDataModul.ds_1TabOrder  TDBEdit
DBEdit_vkpLeft� TopWidthSHeight
DataSourceDataModul.ds_1TabOrder	  	TGroupBoxGroupBox_artLeft Top�Width�Height&AlignalBottomTabOrder 	TCheckBoxCheckBox_frei_artikelTagLeftTopWidth� HeightCaptionfreie Artikeleingabe TabOrder     TPage Left Top Captionarbeitsplatz TLabelArbeitsplatzLeftTopWidth6HeightCaptionArbeitsplatz  TDBEditDBEdit_ap_nameLeftTop Width9Height
DataSourceDataModul.ds_1TabOrder   TBitBtn	BitBtn_apLeftTopXWidthaHeightCaptionDetailsTabOrderOnClickBitBtn_apClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�333333Ds333333w��33333tDs333337w?�33334DDs33337?w?�3333DDDs3333s�w?�333tDDDs3337�3w?�334DDDDs337?33w?�33DDDDDs?����w���������wwwwww3w33�����333s33?w33<����3337�3?w333<���333373?w3333���33333s?w3333<��333337�w33333<�3333337w333333�3333333w3333333LayoutblGlyphRight	NumGlyphs   TPage Left Top CaptionGefaehrdung TLabelLabel57LeftTop WidthHeightCaptionName  TLabelLabel58LeftTophWidthHeightCaptionInfo  TDBEdit
DBEdit_gefLeftTop0Width�Height	DataFieldnameTabOrder   TDBMemo
DBMemo_gefLeftTopxWidth�Height� 	DataFieldmemo
ScrollBars
ssVerticalTabOrderWantTabs	   TPage Left Top Captiongdt TPanelPanel2Left Top Width�Height AlignalClientTabOrder  TLabelLabel60LeftTopJWidth� HeightCaption6   Geräteprogramm (kompletter Pfad) (evtl mit Parameter)  TLabelLabel_gdtdateiLeftTop� Width� HeightCaption2   GDT-Datei an die Gerätesoftware (kompletter Pfad)  TLabelLabel63LeftTopWidthHeightCaptionName  TLabelLabel62LeftTopWidthHeightCaption=   Pfad, der auf die PDF-Datei als Rückantwort überprüft wird  TLabelLabel_idempfaengerLeftTop� WidthZHeightCaption   ID des Empfängers  TLabelLabel_parameterLeftTopxWidth� HeightCaption+   Aufrufparameter für obiges Geräteprogramm  TDBEditDBEdit_gdtprogrammLeftTopZWidth�Height	DataFieldprogrammTabOrder  TDBEditDBEdit_gdtnameLeftTop*Width�Height	DataFieldnameTabOrder   TDBRadioGroupDBRadioGroup_gdtparameterLeftTop�Width�Height5Caption	Parameter	DataField	parameterItems.Strings$   Satzart 6301 Stammdaten übermitteln(Satzart 6302 Neue Untersuchung anfordern TabOrderValues.Strings12   TDBEditDBEdit_gdtdateiLeftTop� Width�Height	DataFielddateiTabOrder  TDBEditDBEdit_gdtrueckantwortLeftTop"Width�Height	DataFieldrueckantwortTabOrder  TDBEditDBEdit_gdtidemfaengerLeftTop� Width�Height	DataFieldid_empfaengerTabOrder  TDBEditDBEdit_gdtparameterLeftTop� Width�Height	DataFields1TabOrder  TDBRadioGroupDBRadioGroup_gdt_versionLeftTopYWidth�HeightHCaptionGDT-Version	DataFieldi1Items.StringsGDT 1.00GDT 2.10StepOver TabOrderValues.Strings103 OnChangeDBRadioGroup_gdt_versionChange  TDBCheckBoxDBCheckBox_gdtwartenLeftTop?WidthAHeightCaption:   Warten auf Rückantwort (pdf-Datei mit Ergebnisübersicht)	DataFieldwartenTabOrderValueChecked1ValueUnchecked0    TPage Left Top Captionanrede TLabelLabel66Left0Top8Width"HeightCaptionAnrede  TLabelLabel67Left0Top� Width6HeightCaptionBriefanrede  TLabelLabel68Left0Top� Width>HeightCaptionAnrede Dativ  TDBEditDBEdit_an_nameLeft0TopHWidthyHeight
DataSourceDataModul.ds_1TabOrder   TDBEditDBEdit_an_briefLeft0Top� WidthaHeight
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_an_dativLeft0Top� WidthaHeight
DataSourceDataModul.ds_1TabOrder   TPage Left Top Captionambulanz_weiter TPanelPanel4Left Top Width�Height AlignalClientTabOrder  TSpeedButtonSpeedButton3LeftTop� WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsOnClickSpeedButton_upClick  TSpeedButtonSpeedButton4LeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsOnClickSpeedButton_downClick  TLabelLabel70LeftTopXWidthWHeightCaptionWeiterbehandlung  TDBEditDBEdit_amb_weiterLeftToppWidth�Height
DataSourceDataModul.ds_1TabOrder     TPage Left Top Captionjahresabschluss TLabelLabel71Left.TopfWidth;HeightCaptionSperr-Datum  TLabelLabel72Left� TopxWidth� HeightCaption0   vor diesem Datum sind keine Veränderungen mehr   TLabelLabel73Left� Top� Width� HeightCaption*an Firmen- oder Probandenbezogenen Listen,  TLabelLabel74Left� Top� Width*HeightCaption
    möglich.  TLabelLabel75Left2Top�WidthHeightCaption>   Achtung: dies Funktion lässt sich nicht  rückgängig machen.  TLabelLabel76Left/Top*Width5HeightCaption	Kommentar  TLabelLabel77Left.Top� WidthbHeightCaptionDatum der Erstellung  TSpeedButtonSpeedButton5Left� TopuWidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsOnClickSpeedButton5Click  TLabelLabel78Left'Top4WidthqHeightCaptionI   Nach dem Speichern sind keine Veränderungen am Sperrdatum mehr möglich!  TDBEditDBEdit_abschluss_sperrdatumTagLeft.TopwWidthQHeight
DataSourceDataModul.ds_1ReadOnly	TabOrder   TDBMemoDBMemo_abschluss_kommentarLeft.Top8WidthfHeightH
DataSourceDataModul.ds_1TabOrder  TDBEditDBEdit_abschluss_erstelldatumTagLeft.Top� WidthQHeight
DataSourceDataModul.ds_1ReadOnly	TabOrder   TPage Left Top CaptionString TPanelpanel_stringLeft Top Width�Height AlignalClientTabOrder  TSpeedButtonSpeedButton_stringhochLeftTop� WidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsVisibleOnClickSpeedButton_upClick  TSpeedButtonSpeedButton_stringrunterLeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsVisibleOnClickSpeedButton_downClick  TLabelLabel_stringLeftTop@WidthHeightCaptionstring  TDBEditDBEdit_stringLeftTop\Width9Height
DataSourceDataModul.ds_1TabOrder     TPage Left Top Caption	zeitkonto TPanelPanel_zeitkontoLeft Top Width�Height AlignalClient
BevelOuterbvNoneTabOrder  TLabelLabel61Left1TopPWidth-HeightCaption	Zeitkonto  TSpeedButtonSpeedButton8LeftTopOWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsOnClickSpeedButton_upClick  TSpeedButtonSpeedButton9LeftTophWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsOnClickSpeedButton_downClick  TDBEditDBEdit_zeitkontoLeft1TopcWidthxHeight
DataSourceDataModul.ds_1TabOrder OnChangeDBEdit_zeitkontoChangeOnClickDBEdit_zeitkontoChange  	TGroupBox	GroupBox1LeftTop� Width�Height� Caption   ausgewählte KriterienTabOrder TDBGridDBGrid1LeftTopWidth�Height� AlignalClient
DataSourceDataModul.ds_4OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNamenameTitle.Caption	KriteriumWidth{Visible	     TBitBtnBitBtn_zeitkonto_kriterienLeftTop�Width� HeightCaption   Kriterien hinzufügenTabOrderOnClickBitBtn_zeitkonto_kriterienClick  TBitBtnBitBtn3Left� Top�Width� HeightCaptionKriterien entfernenTabOrderOnClickBitBtn3Click     TPanelPanel_bLeft TopHWidth-Height%AlignalBottom
BevelOuterbvNoneTabOrderVisible TLabel
Label_suchLeftTop
WidthHeight  TBitBtn	BitBtn_okLeft� TopWidthvHeightCaptionOKTabOrder OnClickBitBtn_okClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtn
BitBtn_escLeft�TopWidthvHeightCaptionAbbruchTabOrderOnClickBitBtn_escClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs   TPanelPanel1Left Top(WidthIHeight AlignalLeft
BevelOuterbvNoneCaptionPanel1TabOrder 
TDBgridEXTDBgrid_tabellenLeft Top(WidthIHeight�AlignalClient
DataSourceDataModul.ds_1DefaultDrawing	OptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBgrid_tabellenDblClick	OnKeyDownDBgrid_tabellenKeyDown
OnKeyPressDBgrid_tabellenKeyPresskeine_farbeZeitdiff_L_B darf_ziehenColumnsExpandedWidthVisible	 ExpandedWidth�Visible	    TPanelPanel_suchenLeft Top WidthIHeight(AlignalTop
BevelOuterbvNoneCaptionPanel_suchenTabOrderVisible TLabelLabel207LeftTopWidth9HeightCaption
Suchen:   Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontLayouttlCenter  TSpeedButtonSpeedButton_firma_suchTagLeftTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsOnClickSpeedButton_firma_suchClick  TEdit	Edit_suchTagLeftLTop	Width� Height
AutoSelectTabOrder 
OnKeyPressEdit_suchKeyPress    TOpenDialog
OpenDialogFiltervCard|*.vcfLeftPTop   