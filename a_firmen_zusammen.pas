unit a_firmen_zusammen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  Tform_firmen_zusammen = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    SpeedButton1: TSpeedButton;
    Edit_kuerzel1: TEdit;
    Edit_name1: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    SpeedButton2: TSpeedButton;
    Edit_kuerzel2: TEdit;
    Edit_name2: TEdit;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    i_firma1, i_firma2:integer;
  end;

var
  form_firmen_zusammen: Tform_firmen_zusammen;

implementation

uses a_listwahl, a_data, a_main;

{$R *.DFM}

procedure Tform_firmen_zusammen.SpeedButton1Click(Sender: TObject);
begin
try
   with datamodul do
	 begin
		 form_liste:=tform_liste.create(self);
		 form_liste.dbgridext.DataSource:=ds_5;
		 form_liste.s_name:='firma';
		 form_liste.DBgridEXT.Columns[0].fieldname:='firma';
       form_liste.DBgridEXT.Columns[1].fieldname:='kuerzel';
       form_liste.DBgridEXT.Columns[0].Width:=250;
       form_liste.DBgridEXT.Columns[1].Width:=80;
       form_liste.DBgridEXT.Columns[0].Title.caption:='Firma';
       form_liste.DBgridEXT.Columns[1].Title.caption:='K�rzel';
		 //form_liste.DBgridEXT.Options:=form_liste.DBgridEXT.Options 	 ;
       {  form_liste.toolbutton_neu.Visible:=false;
      	 form_liste.toolbutton_edit.Visible:=false;
     	 form_liste.toolbutton_del.Visible:=false; }
       form_liste.Panel_b.Visible:=true;
       form_liste.ToolBar.Visible:=false;
       form_liste.height:= form_main.Height-170;
		 if form_liste.showmodal<>mrok then exit;

         if sender =speedbutton1 then
         begin
		 	edit_name1.Text:=q_5['firma'];
            edit_kuerzel1.text:= q_5['kuerzel'] ;
            i_firma1:=q_5['nummer'] ;
         end
         else
         begin
         	edit_name2.Text:=q_5['firma'];
            edit_kuerzel2.text:= q_5['kuerzel'] ;
            i_firma2:=q_5['nummer'] ;
         end;
	 end;
finally
	form_liste.release;

end;
end;

procedure Tform_firmen_zusammen.FormShow(Sender: TObject);
begin
   form_main.form_positionieren(tform(sender));
end;

procedure Tform_firmen_zusammen.BitBtn1Click(Sender: TObject);
begin
if (edit_kuerzel1.text='') or (edit_kuerzel2.text='')
  then showmessage('Bitte beide Firmen eingeben.')
  else modalresult:=mrok;
end;

end.
