unit mysql_direkt_small;

interface
uses SysUtils, Windows, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Menus, ComCtrls, ClipBrd,
	ToolWin ,RichEdit, ShellAPI, {zplainmysql323,}zcompatibility,ZDbcDbLibStatement,ZDbcDBLib, ZDbcIntfs,db,ZDataset;

type
	Tmysql_d = class(tobject)
	private
	  { Private-Deklarationen }

    public
  		Connection_d: IZConnection;
  		ResultSet: IZResultSet;
  		LastRowNr: Integer;//This is to detect row nr change
  		InsertState: Boolean;

		hk:string;
		MyHost: string;
     MyProtocol:string;
		MyPort: integer;
		MYdb:string;
		MyUser: string;
		MyPass: string;
		MyTime: longword;
		MyComp: integer;
		MyDebg: boolean;
		MyShut: boolean;
		constructor create();//(host,protocol,port,db,user,pass:string);
     destructor free;
		//function connect(var msql:mysql):boolean;
		//function disconnect(var msql:mysql):boolean;
		function neue_nummer(tabelle:string):int64;

		function ds_sperren(tabelle:string; nummer:int64;user_id:integer):boolean; //schreiben in die Sperrtabelle
		function ds_entsperren(tabelle:string; nummer:int64;user_id:integer):boolean; //freigebne in der Sperrtabelle
		function sql_exe(sql:string):boolean;
      function sql_exe_still(sql:string):boolean;
		function sql_exe_grant(sql:string):boolean;
		function sql_exe_rowsvorhanden(sql:string):boolean;
		function Feldinhalt(sql:string;nummer:integer):string;
		function sql_pfad(pfad:string):string;
		procedure Feldarray(sql:string;nummer:array of integer; var result: array of string);
		function mysql_getbyindex(tabelle,indexfeld,index,feldname:string):string;
		function get_user(nummer:integer):string;
     function connect(host,protocol,port,db,user,pass:string):boolean;
     function disconnect:boolean;
     procedure zeile(sql:string;liste:tstringlist);
     function zeilen_zahl(sql:string):integer;
     function db_von_db(quelle,ziel:string;leer:boolean):boolean;
     function struktur_von_db (quelle, ziel:string): boolean;
     function struktur_von_db_history (quelle, ziel:string): boolean;
     function create_sql_von_db (quelle:string): string;
     function drop_table(tab_name:string):boolean;
     function anzahl_datensaetze (tabelle:tzquery):string;

	end;
implementation


constructor Tmysql_d.create();//(host,protocol,port,db,user,pass:string);
begin
inherited create;

	hk:=chr(39);
   //connect_d(host,protocol,port,db,user,pass);
end;




destructor Tmysql_d.free;
begin
   //disconnect;
   //Connection_d.Close;
   //resultset.Close;
   //inherited free;
end;




function Tmysql_d.get_user(nummer:integer):string;
var query:string;
begin
	query:='select name from schluessel where nummer='+inttostr(nummer);
	result:=trim(feldinhalt(query,0));

end;




function tmysql_d.sql_pfad(pfad:string):string;
var
position:integer;
begin
position:=pos('\',pfad);
while position>0 do
begin
	delete(pfad,position,1);
	insert('/',pfad,position);
  position:=pos('\',pfad);
end;
result:=pfad;
end;


function tmysql_d.mysql_getbyindex(tabelle,indexfeld,index,feldname:string):string;
var query:string;
begin
query:=format('select %s from %s where %s="%s"',[feldname, tabelle, indexfeld, index]);
result:=feldinhalt(query,0);
end;

function tmysql_d.connect(host,protocol,port,db,user,pass:string):boolean;
var
  Url,db_port: string;
begin
  if db_port <> '' then
  begin
    Url := Format('zdbc:%s://%s:%s/%s?UID=%s;PWD=%s', [protocol, host,
      port, db, user, pass]);
  end
  else
  begin
    Url := Format('zdbc:%s://%s/%s?UID=%s;PWD=%s', [protocol, host,
      db, user, pass]);
  end;
  Connection_d :=DriverManager.GetConnectionWithParams(Url, nil);
  Connection_d.SetAutoCommit(True);
  Connection_d.SetTransactionIsolation(tiReadCommitted);
  Connection_d.Open;
end;

function tmysql_d.disconnect:boolean;
begin
if Assigned(Connection_d) then
    if not Connection_d.IsClosed then
      Connection_d.Close;
end;

procedure Tmysql_d.zeile(sql:string;liste:tstringlist);
var
Statement: IZStatement;
//zResultSet: IZResultSet;
i:integer;
value:string;
begin
try
  //sql_normieren(sql);
  Statement := Connection_d.CreateStatement;  //##############
  //This is not neccesseary if you do not want to modify the data
 // Statement.SetResultSetConcurrency(rcUpdatable);
  ResultSet := Statement.ExecuteQuery(sql);
  //Was any resultset returned?
  if Assigned(ResultSet) then
  begin
      //nur erste Zeile
      if resultset.Next=false then
      begin
           liste.Add('');
           exit;
      end;

      for I := 1 to ResultSet.GetMetadata.GetColumnCount do
      begin

	  //read out the proper value for the column
        try
        if ResultSet.IsNull(I) then  Value := ''
        else
          case ResultSet.GetMetadata.GetColumnType(I) of
            stBoolean: if ResultSet.GetBoolean(I) then Value := 'True' else Value := 'False';
            stByte: Value := IntToStr(ResultSet.GetByte(I));
            stShort: Value := IntToStr(ResultSet.GetShort(I));
            stInteger: Value := IntToStr(ResultSet.GetInt(I));
            stLong: Value := IntToStr(ResultSet.GetLong(I));
            stFloat: Value := FloatToStr(ResultSet.GetFloat(I));
            stDouble: Value := FloatToStr(ResultSet.GetDouble(I));
            stBigDecimal: Value := FloatToStr(ResultSet.GetBigDecimal(I));
            stBytes, stBinaryStream: Value := ResultSet.GetBlob(I).GetString;
            stDate: Value := DateToStr(ResultSet.GetDate(I));
            stTime: Value := TimeToStr(ResultSet.GetTime(I));
            stTimeStamp: Value := DateTimeToStr(ResultSet.GetTimeStamp(I));
            stAsciiStream, stUnicodeStream: Value := ResultSet.GetBlob(I).GetString;
          else
            Value := ResultSet.GetString(I);
          end;


        except
        Value := ''
        end;

        liste.Add( Value);
      end;
  end;
finally
  statement.Close;
end;
end;

function Tmysql_d.Feldinhalt(sql:string;nummer:integer):string;
var
 liste:tstringlist;
begin
try
 liste:=tstringlist.create;
 zeile(sql,liste);
 result:=liste.Strings[nummer];
finally
 liste.Free;
end;
end;


procedure Tmysql_d.Feldarray(sql:string;nummer:array of integer; var result: array of string);
var
 liste:tstringlist;
 i:integer;
begin
try
 liste:=tstringlist.create;
 zeile(sql,liste);
 for i:=0 to liste.count-1 do result[i]:=liste.Strings[nummer[i]];
finally
 liste.Free;
end;
end;

function Tmysql_d.sql_exe_rowsvorhanden(sql:string):boolean;
begin
     if feldinhalt(sql,0)='' then result:=false else result:=true;
end;

function Tmysql_d.zeilen_zahl(sql:string):integer;
var
Statement: IZStatement;
i,j:integer;
value:string;
begin
  //sql_normieren(sql);
	Statement := Connection_d.CreateStatement;
  ResultSet := Statement.ExecuteQuery(sql);
  resultset.last;
  result:=resultset.GetRow;

  statement.Close;

end;


function Tmysql_d.sql_exe(sql:string):boolean;
var
i:integer;
begin

 //sql_normieren(sql);
{ datamodul.sql_processor.Clear;
 datamodul.sql_processor.Script.Clear ;
 if pos('#',sql) >0 then showmessage (sql);
 datamodul.sql_processor.Script.Add(sql);
 try

 	datamodul.sql_processor.Execute;
 	result:=true;
 except
   if modus_debug then showmessage(sql+#10+'konnte nicht ausgef�hrt werden');
 	result:=false;  //hier bei  fehler
 end;   }
end;

function Tmysql_d.sql_exe_still(sql:string):boolean;
var
i:integer;
begin

 {sql_normieren(sql);
 datamodul.sql_processor.Clear;
 datamodul.sql_processor.Script.Clear ;
 if pos('#',sql) >0 then showmessage (sql);
 datamodul.sql_processor.Script.Add(sql);
 try

 	datamodul.sql_processor.Execute;
 	result:=true;
 except
  if modus_debug then showmessage(sql+#10+'konnte nicht ausgef�hrt werden');
   result:=false;  //hier bei  fehler
 end;  }
end;

function Tmysql_d.sql_exe_grant(sql:string):boolean;
begin
 {sql_normieren(sql);
 datamodul.connection_grant.Connect;
 datamodul.sql_processor_grant.Clear;
 datamodul.sql_processor_grant.Script.Clear ;
 datamodul.sql_processor_grant.Script.Add(sql);
 try

 datamodul.sql_processor_grant.Execute;
 result:=true;
 except
 if modus_debug then showmessage(sql+#10+'konnte nicht ausgef�hrt werden');
 result:=false;  //hier bei  fehler
 end;
 datamodul.connection_grant.Disconnect; }
end;

function Tmysql_d.ds_entsperren(tabelle:string; nummer:int64;user_id:integer):boolean;
var
	sql,s:string;

begin
	result:=false;
	tabelle:=hk+tabelle+hk;
  sql:='select count(*) from sperren where sperrobjekt= '+tabelle+' and sperrobjekt_id= '+inttostr(nummer)+' and user_id= '+inttostr(user_id);
  s:=feldinhalt(sql,0);
  if s<>'' then
    begin
	    sql:='delete from sperren where sperrobjekt= '+tabelle+' and sperrobjekt_id= '+inttostr(nummer)+' and user_id= '+inttostr(user_id);
      sql_exe(sql);
      result:=true;
    end
  else
    result:=false ;


end;

function Tmysql_d.ds_sperren(tabelle:string; nummer:int64; user_id:integer):boolean;
var
	query,sql:string;

begin
 { if tabelle='history' then
  begin
    result:=true;
    exit;
  end;
	result:=false;
	tabelle:=hk+tabelle+hk;
   query:=format('select sperrobjekt from sperren where (sperrobjekt=%s and sperrobjekt_id=%d and user_id=%d )',[tabelle,nummer,user_id]);
   try
      if mysql_d.Feldinhalt(query,0)='' then
      begin
         sql:=format('insert into sperren values (%s ,%d,%d)',[tabelle,nummer,user_id]);    //index �ber tabelle nummer user_id
         if sql_exe(sql) then result :=true
      end;
   except
    
   	result:=false;
   end;  }
end;


function Tmysql_d.neue_nummer(tabelle:string):int64;
var
sql:string;
zeit:ttime;
j:int64 ;
i:integer;
felder:array [0..1] of string;
begin
	{result:=0;
	//datensatz mit index 0 l�schen
		sql:=format('delete from %s where nummer=0',[tabelle]);
		sql_exe(sql);

		sql:='select aktuell from nummern_kreis where tab_name='+hk+tabelle+hk;

		j:=zeilen_zahl(sql);
      //showmessage('j='+inttostr(j));
		if j<1 then  //keine zeilen
		begin
			result:=-2; //tabelle nicht vorhanden;
                        //showmessage('Tabelle '+tabelle+' in nummern_kreis nicht vorhanden');
			exit;
		end;

		zeit:=time();
    i:=0;
		sql:='UPDATE nummern_kreis SET gesperrt=1 WHERE tab_name='+hk+tabelle+hk;
     try
       while (not sql_exe(sql) ) do  //
          begin
             application.ProcessMessages;
             if (time()>zeit+0.0001)  then
             begin
               showmessage('Bitte warten bis der Nummernkeis wieder freigegeben ist');
               application.ProcessMessages;
               zeit:=time();
               if i>5 then exit;//raise exception.Create('neue_nummer');//schleife wenn gesperrt
               inc(i);
             end;
          end;
       sql:='update nummern_kreis set aktuell=aktuell+1 where tab_name='+hk+tabelle+hk;
       if  not sql_exe(sql) then raise exception.Create('neue_nummer');//schleife wenn gesperrt

       sql:='select aktuell,bis from nummern_kreis where tab_name='+hk+tabelle+hk;
       mysql_d.Feldarray(sql,[0,1],felder);

       result:=strtoint64(felder[0]);
       j:=strtoint64(felder[1]);

	  finally

		sql:='update nummern_kreis set gesperrt=0 where tab_name='+hk+tabelle+hk;
		sql_exe(sql);
     if (result>0) and (((lowercase (tabelle)='firma') and ((j-result)<20)) or ((lowercase (tabelle)<>'firma') and ((j-result)<200))) then showmessage('Neuer Nummernkreis notwendig - bitte mit dem Support in Verbindung setzen! Tabelle/nummer: '+tabelle +'/'+ inttostr(result));
		 if result=0 then showmessage('Fehler beim Anlegen eines Datensatzes in Tabelle: '+tabelle);
	end;   }

end;

function tmysql_d.drop_table(tab_name:string):boolean;
var query,q1:string;
begin
try
      //query:=format('DELETE FROM %s',[tab_name]);
	  //result:=sql_exe(query);
      q1:=format('SHOW TABLE STATUS like "%s"',[tab_name]);

      while sql_exe_rowsvorhanden(q1) do

      begin
      	query:=format('drop table if exists %s',[tab_name]);
	  		result:=sql_exe(query);
      end;

except
end;
end;

function tmysql_d.struktur_von_db (quelle, ziel:string): boolean;
var
weiter:boolean;
query,re,fehler:string;
begin
{result:=false;
weiter:=false;
//erzeugt die Tabelle ziel nach dem Muster von quelle

   //hier erst wenn sicher gel�scht
   while  not weiter do
   begin
      try
         drop_table(ziel);
         query:=format('show create table %s',[quelle]);
         pause(1);
         re:=feldinhalt(query,1);

         re:= stringreplace(re,#$A,'',[rfReplaceAll]);
         re:=stringreplace(re,quelle,ziel,[rfIgnoreCase]);

         result:=sql_exe(re); //tabellenstruktur erzeugen
         weiter:=true;
       except
         fehler:=' Tabellenstruktur konnte nicht erzeugt werden - nochmals versuchen?' ;
         if Messagedlg(fehler,mtConfirmation, [mbOK,mbcancel],0)=mrcancel then weiter:=true;
       end

   end;     }

end;

function tmysql_d.struktur_von_db_history (quelle, ziel:string): boolean;
var
weiter:boolean;
query,re,fehler:string;
begin
result:=false;
weiter:=false;
//erzeugt die Tabelle ziel nach dem Muster von quelle

   //hier erst wenn sicher gel�scht
   while  not weiter do
   begin
      try
         drop_table(ziel);
         query:=format('show create table %s',[quelle]);
         //pause(1);
         re:=feldinhalt(query,1);

         re:= stringreplace(re,#$A,'',[rfReplaceAll]);
         re:=stringreplace(re,quelle,ziel,[rfIgnoreCase]);

         result:=sql_exe(re); //tabellenstruktur erzeugen
         weiter:=true;
       except
         fehler:=' Tabellenstruktur konnte nicht erzeugt werden - nochmals versuchen?' ;
         if Messagedlg(fehler,mtConfirmation, [mbOK,mbcancel],0)=mrcancel then weiter:=true;
       end

   end;

end;

function tmysql_d.db_von_db(quelle,ziel:string;leer:boolean):boolean;
var
query,re:string;
erfolg:boolean;
begin
erfolg:=false;

while not erfolg do
begin
	erfolg:=struktur_von_db(quelle,ziel); //nur struktur
    if not leer then
    begin
	    query:=format('insert into %s select * from %s',[ziel,quelle]);
		erfolg:=sql_exe(query) and erfolg;
    end;
end;

end;

function tmysql_d.create_sql_von_db (quelle:string): string;
var
query,re:string;
p:integer;
begin
   query:=format('show create table %s',[quelle]);
   result:=feldinhalt(query,1);
   p:=pos(chr(10),result);
   result:=copy(result,p-1,length(result));

end;


function tmysql_d.anzahl_datensaetze (tabelle:tzquery):string;
var q :string;
    p:integer;
begin
  {q:=tabelle.SQL.Text;
  p:=pos('from',q);
  q:=copy(q,p,length(q));
  q:='select count(*) '+q;
  result:=mysql_d.Feldinhalt( q,0);  }
end;






end.
