//minimaler timesatmp: "1970010101001" bzw  "1970-01-01 01:00:01"


unit a_abgleich;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,a_data,
	Grids, DBGrids, dbgridEXT, StdCtrls, ComCtrls, Buttons, ExtCtrls, mysql_direkt,
	Mask, ToolWin,FileCtrl, variants,ZipMstr19,shellapi, DBCtrls;

type
	TForm_abgleich = class(TForm)
    PageControl: TPageControl;
    TabSheet_export: TTabSheet;
    TabSheet_import: TTabSheet;
    BitBtn_dat_export: TBitBtn;
	  GroupBox1: TGroupBox;
    DBgridEXT1: TDBgridEXT;
    GroupBox2: TGroupBox;
	  DBgridEXT2: TDBgridEXT;
    BitBtn_dat_import: TBitBtn;
    Memo_unzip: TMemo;
	  Memo_export: TMemo;
	  GroupBox_firma: TGroupBox;
	  Button_firma: TButton;
    DBgridEXT_firma: TDBgridEXT;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    OpenDialog: TOpenDialog;
    tabsheet_protokoll: TTabSheet;
    DBgridEXT3: TDBgridEXT;
    Label3: TLabel;
    TabSheet_dirabgl: TTabSheet;
    GroupBox3: TGroupBox;
    DBgridEXT4: TDBgridEXT;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    MaskEdit_dat1: TMaskEdit;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    GroupBox6: TGroupBox;
    Memo_abgleich: TMemo;
    Label11: TLabel;
    Label12: TLabel;
    SpeedButton3: TSpeedButton;
    MaskEdit_dat3: TMaskEdit;
    Label14: TLabel;
    Label13: TLabel;
    Panel_mitte: TPanel;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    SpeedButton2: TSpeedButton;
    Label5: TLabel;
    Label16: TLabel;
    CheckBox2: TCheckBox;
    MaskEdit_dat2: TMaskEdit;
    GroupBox4: TGroupBox;
    Label6: TLabel;
    Label29: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit_iphaupt: TEdit;
    Edit_pflocal: TEdit;
    Edit_pfhaupt: TEdit;
    BitBtn_lokal: TBitBtn;
    Edit_dbhaupt: TEdit;
    GroupBox7: TGroupBox;
    FileListBox: TFileListBox;
    Panel1: TPanel;
    CheckBox_loeschen: TCheckBox;
    checkbox_neu: TCheckBox;
    Button_abgleichstart: TButton;
    Panel2: TPanel;
    CheckBox_alle: TCheckBox;
    Label15: TLabel;
    Button1: TButton;
    Timer_pm: TTimer;
    Label18: TLabel;
    CheckBox_personal: TCheckBox;
    GroupBox_auswahl: TGroupBox;
    CheckBox_personaldaten: TCheckBox;
    CheckBox_bescheinigung: TCheckBox;
    CheckBox_inisat: TCheckBox;
    CheckBox_user: TCheckBox;
    DBGrid_user: TDBGrid;
    Label10: TLabel;
    CheckBox_nur_export: TCheckBox;
    BitBtn_abbruch: TBitBtn;
    Label17: TLabel;
    Label19: TLabel;
    CheckBox_vg: TCheckBox;
    Label20: TLabel;
    Edit_exportpfad: TEdit;
    BitBtn1: TBitBtn;
	  procedure FormCreate(Sender: TObject);
	  procedure BitBtn_dat_exportClick(Sender: TObject);
	  procedure BitBtn_dat_importClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
	  procedure Button_firmaClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure BitBtn_lokalClick(Sender: TObject);
    procedure Button_abgleichstartClick(Sender: TObject);
    function exportieren(modus,host:string;sql_d:tmysql_d;nur_personal:boolean):boolean;
    function importieren(modus:string;sql_d:tmysql_d):boolean;
    procedure CheckBox2Click(Sender: TObject);
    function   alle_firmennummern:string;
    function   alle_firmennummern_berechtigt:string;
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function direktabgleich:boolean;
    procedure serververzeichnis_lesen(komplett:boolean);
    procedure Edit_pflocalChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure tabellenvergleich(var liste: tlist;modus: integer);
    procedure vergleichen;
    procedure Timer_pmTimer(Sender: TObject);
    procedure CheckBox_personalClick(Sender: TObject);
    procedure CheckBox_userClick(Sender: TObject);
    procedure BitBtn_abbruchClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	private
	  ff:string;
	  { Private-Deklarationen }
 	  //haupt:integer;
	  schluessel:string;
     firmen_nummern_wahl, firmen_nummern,lastexport:string;
     fehler:boolean;
     zaehler:integer;
     liste_tabellen:tstringlist;
     b_abbruch:boolean;
    function db_create_normieren(sql:string):string;
    procedure liste_tabellen_schreiben_all ;
    procedure liste_tabellen_schreiben_auswahl ;
    function abbruchcheck:boolean;
	public
	  { Public-Deklarationen }

  end;

var
  Form_abgleich: TForm_abgleich;

implementation
uses a_main, a_kalender, a_f_wahl, a_abgleich_ausnahme, a_pfadwahl;

{$R *.DFM}

procedure TForm_abgleich.FormCreate(Sender: TObject);
var
query:string;
dat:tdate;
begin
with datamodul do
begin

  CheckBox_loeschen.checked:= abgleich_sat_anpassen;
  checkbox_neu.Checked:=abgleich_neugewinnt;

   liste_tabellen:=tstringlist.Create;
   zaehler:=0;
   firmen_nummern:='-1';
   firmen_nummern_wahl:='-1';
   b_abbruch:=false;
	form_main.form_positionieren(tform(sender));

    query:='select nummer from firma';
    if not  mysql_d.sql_exe_rowsvorhanden(query) then
    begin
    	checkbox2.Checked:=true;
    	checkbox2.Enabled:=false;
      checkbox_alle.Checked:=true;
    	checkbox_alle.Enabled:=false;
      button1.enabled:=false;
    end;



   if  system_modus=1 then //hauptsystem
   	TabSheet_dirabgl.TabVisible:=false else pagecontrol.ActivePage:= TabSheet_dirabgl;

     edit_iphaupt.text:=abgl_iphaupt;
     edit_dbhaupt.text:=abgl_dbhaupt;
     edit_pflocal.text:=pfad_abgleich_lokal;
     edit_pfhaupt.text:=pfad_abgleich_haupt;
	 mysql_d.sql_exe('update export_import set kommentar="" where kommentar<>""');
	 sql_new(false,q_1,'select * from haupt_tabelle','');
	 //haupt:=q_1['haupt'];
	 schluessel:=q_1['s1'];
   //q_2 exportieren
   //q_3 importieren
	 if q_1['haupt']=1 then
	 begin
		 sql_new(true,q_2,'select * from export_import where main>=0 order by tab_name',''); //order by tab_name
		 sql_new(true,q_3,'select * from export_import where main=0 order by tab_name','');
	 end
	 else
	 begin   //satellit
		sql_new(true,q_2,'select * from export_import where main=0 order by tab_name','');
		sql_new(true,q_3,'select * from export_import where main>=0 order by tab_name','');
	 end;
	 sql_new(true,q_4,'select * from satelliten','');
	 query:='select firma from firma where nummer =-1';
	 sql_new(true,q_5,query,'firma');
	 sql_new( false,q_6,'select * from abgleich_prot order by zeitpunkt desc','');
	 if kein_datensatz(q_6) then
      begin
        dat:=strtodate('01.01.1970');
        maskedit_dat1.EditText:='01.01.1970';
      end
     else
     begin
       try
        dat:=q_6.findfield('bis').AsDateTime;   //vorher q_1
       except
        dat:=strtodate('01.01.1970');
       end;
       dat:=dat-0;  //hier vorziehen um eine �berschneidung zu erreichen
       lastexport:=datetostr(dat);

     end;
     ff:='';

     CheckBox2Click(self);   //hier maskedit2
     maskedit_dat3.edittext:=lastexport;
     maskedit_dat1.EditText:=lastexport;
     if  maskedit_dat3.edittext='' then maskedit_dat3.edittext:= '01.01.1970';
     edit_exportpfad.Text:=pfad_temp;
end;
end;

procedure TForm_abgleich.BitBtn_dat_exportClick(Sender: TObject);
begin
if ( checkbox_personal.checked) and (not datensatz(datamodul.q_5))  then
begin
showmessage('Bitte Firma ausw�hlen');
exit;
end;

if not directoryexists(edit_exportpfad.Text) then
begin
  showmessage('Der Exportpfad ist ung�ltig');
  exit;
end;

timer_pm.Enabled:=  true;

exportieren('dat','lokal',mysql_d,checkbox_personal.Checked);
timer_pm.Enabled:=false;
end;



function tform_abgleich.exportieren(modus,host:string;sql_d:tmysql_d;nur_personal:boolean):boolean;
var
pf_ha,pf_lo:string;
pfad_sql, pfad_lokal,pfad,query,datname_sql, datname_lokal,datname_temp, ziptemp,datei,a_name,
 ts,es,zipdat,s,s_d,ma_join,ma_join_temp,nrs:string;
filelist:tstringlist;
absender:textfile;
memo:tmemo;
maskedit:tmaskedit;
system_modus_1:integer;
t:ttime;
//zeit:ttime;
begin
form_main.ZipMaster.FSpecArgs.Clear;

pf_ha:= edit_pfhaupt.text;
pf_lo:= edit_pflocal.text;

if modus ='dat' then
begin
	memo:= memo_export;
	maskedit:=maskedit_dat1;
    memo.lines.Clear;
    system_modus_1:=system_modus;
end
else   //abgleich
begin
	memo:= memo_abgleich;
	maskedit:=maskedit_dat2;

    if host='lokal' then
    begin
    	system_modus_1:=0;
        memo.Lines.Add('---------------------------------');
        memo.Lines.Add('Exportieren vom Satellitensystem');
        memo.Lines.Add('---------------------------------');
    end
    else
    begin
    	system_modus_1:=1;
        memo.Lines.Add('---------------------------------');
        memo.Lines.Add('Exportieren vom Hauptsystem');
        memo.Lines.Add('---------------------------------');
    end;
end;


with datamodul do
begin
es:=maskedit.edittext;
try
strtodate(es);
except
showmessage('Kein g�ltiges Datum');
exit;
end;

if (pfad_abgleich_lokal='') or (pfad_abgleich_haupt='') then
begin
showmessage('Bitte Pfadangaben in Optionen - Dateipfade vervollst�ndigen');
exit;
end;



if (not checkbox_alle.checked) and (not datensatz(datamodul.q_5))  then
begin
showmessage('Bitte Firma ausw�hlen');
exit;
end;

if modus='dat' then
begin
	q_1.edit;
	q_1['last_export']:=date(); //es
	q_1.post;
end;
pfad:=pfad_temp;
ts:=copy(es,7,4)+copy(es,4,2)+copy(es,1,2)+'010001';
if system_modus_1=0 then s_d:='_Sat_'else s_d:='_Haupt_';

try
	 screen.cursor:=crhourglass;
	 filelist:=tstringlist.Create;

     if modus='dat' then
     try


          pfad_sql:=sql_d.sql_pfad(pfad_temp); //aus sicht der datenbank
           pfad_lokal:=pfad_temp;
           //zipdat:=pfad_lokal+'arb'+s_d+copy(sql_datetostr(date()),2,10)+'.arb';//savedialog.FileName;
           zipdat:=edit_exportpfad.Text+'arb'+s_d+copy(sql_datetostr(date()),2,10)+'_'+ersetzesonderzeichen(timetostr(now()))+'.arb';//savedialog.FileName;


           ziptemp:=copy(pfad_temp,1,length(pfad_temp))+'Arbene_zip_temp.zip';

           a_name:=pfad_lokal+'absender.txt';    //absenderkennung

           assignfile(absender,a_name );
          except
            showmessage('Fehler, sind die Dateipfade unter Optionen korrekt?');
            exit;
      end;

      if modus='abgleich' then
      begin

           if host='lokal' then
           begin
             //pfad_sql:=sql_d.sql_pfad(pf_lo);
             pfad_sql:=sql_d.sql_pfad(pfad_temp); //aus sicht der datenbank

           end
           else
           begin
            pfad_sql:=sql_d.sql_pfad(pf_ha); //aus sicht der datenbank
           end;

           //pfad:=pfad_temp;
           //pfad_sql:=sql_d.sql_pfad(pf_ha); //aus sicht der datenbank

           pfad_lokal:=edit_pflocal.Text;
      end;

      //if false then    //##++      nur test
  	  begin
         try
          	a_name:=pfad_lokal+'absender.txt';    //absenderkennung
          	assignfile(absender,a_name );
          except
          	showmessage('Fehler, sind die Dateipfade unter Optionen korrekt?');
          	exit;
          end;


	 	rewrite(absender);
	 	s:=q_1['hs1']; //zeile1
	 	writeln(absender,s);
	 	s:=q_1['hs2'];  //2
	 	writeln(absender,s);
	 	s:=q_1['ln'];
	 	writeln(absender,s); //3   //abgeich der Schl�ssel

	 	writeln(absender,es); //4  //abgleich-Daten
	 	writeln(absender,datetimetostr(now)); //5

	 	writeln(absender,inttostr(system_modus_1) );//4  /System-Typ

	 	if system_modus_1=1 then    //hauptsystem
	 	begin
		 q_4.First;
		 while not q_4.Eof do
		 begin
			s:=q_4['schluessel'];
			writeln(absender,s);
			q_4.next;
		 end;
	 	end
	 	else
	 	writeln(absender,schluessel);
	 	closefile(absender);
        //showmessage('absender geschrieben');
	 	filelist.Add(a_name);

        form_main.ZipMaster.FSpecArgs.Add(a_name);
     end;
	 memo.lines.Add('Exportpfad aus Sicht von MySQL');

   serververzeichnis_lesen(true);

   if nur_personal then
   begin
    if (checkbox_personaldaten.Checked or checkbox_bescheinigung.Checked) then
      liste_tabellen_schreiben_auswahl
      else
      liste_tabellen_schreiben_all;
   end;


	 q_2.first;       //schreiben der einzelnen Abgleichdateinen
	 while (not q_2.eof) and (not abbruchcheck) do begin

     serververzeichnis_lesen(false);
		 datei:=lowercase(q_2['tab_name']);
		 datname_sql:=pfad_sql+datei+'.sql';
		 datname_lokal:=pfad_lokal+datei+'.sql';

     if not b.b_hat_tab_ber(datei,0) then
     begin
      memo.Lines.Add(format('# keine Berechtigung f�r  %s, kein Export ',[datei]));
     end;
     if b.b_hat_tab_ber(datei,0) then // nur die Tabellen exportieren f�r die eine Berechtigung besteht
     if (liste_tabellen.count=0) or (liste_tabellen.IndexOf(datei)>=0) then // (tabellen_liste.count=0)
     begin
         if fileexists(datname_lokal) then deletefile(datname_lokal);
         if fileexists(datname_lokal) then
         begin
         	showmessage ('Die Datei ' +datname_lokal+ 'kann nicht gel�scht werden - Abgeleich wird abgebrochen');
         	exit;
         end;
         datname_temp:=pfad_temp+datei+'.sql';
         if fileexists(datname_temp) then deletefile(datname_temp);
         filelist.Add(datname_lokal); //lokal

         form_main.ZipMaster.FSpecArgs.Add(datname_lokal);

         memo.lines.Add(format('Datei %s wird nach %s exportiert '+timetostr(time()),[datei, datname_sql]));
         application.ProcessMessages;
         //######## bisher keine Firmenauswahl, sollte aber leicht sein
         //select dokumente.nummer ... from dokumente left join mitarbeiter on (dokumente.i_mitarbeiter=mitarbeiter.nummer) left join firma on (mitarbeiter.i_firma=firma.nummer) where firma.nummer in (2,3..)
         //m�sste so asusehen damit die Frimenauswahl Sinn macht

         //in der datei export_import muss stehen welcher Join, dann brauch ich noch die gesamten Feldnamen generisch erzeugt.(k�nnen sich ja mal �ndern)
         // oder mit describe tabname bzuw show columns form tabname die Spalten auslesen, wenn i_firma drin .... wenn i_mitarbeiter .... entsprechende Einschr�nkung sonst komplette Tabelle, Tabelle Firma ist gesondert zu bearbeiten
         ma_join:=format('left join mitarbeiter on (%s.i_mitarbeiter=mitarbeiter.nummer) left join firma on (mitarbeiter.i_firma=firma.nummer) where firma.nummer in (%s)',[datei,firmen_nummern]);
         if datei ='texte' then
         begin
         	//beep();
         end;

         if datei ='firma' then
         	query:=format('select * from %s where ts>="%s" and nummer in (%s) into outfile "%s"',[datei,ts,firmen_nummern,datname_sql])
         else
         if datei='mitarbeiter' then
            query:=format('select * from %s where ts>="%s" and i_firma in (%s) into outfile "%s"',[datei,ts,firmen_nummern,datname_sql])
         //rechnung i_firma  und alle mit i_firma=-1
         else
         if datei='rechnung' then
            query:=format('select * from %s %s or %s.i_firma in (%s) and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,firmen_nummern,datei,ts,datname_sql])
            //query:=format('select * from %s where  %s.ts>="%s" and i_firma in (%s) into outfile "%s"',[datei,ts,firmen_nummern,datname_sql])
         else
          //texte i_master
         if datei='texte' then
         query:=format('select * from %s where ts>="%s" and i_master in (%s) into outfile "%s"',[datei,ts,firmen_nummern,datname_sql])

         //firma _ansprech  i_firma
         else
         if datei='firma_ansprech' then
         query:=format('select * from %s where ts>="%s" and i_firma in (%s) into outfile "%s"',[datei,ts,firmen_nummern,datname_sql])
         //firma_sollist
         else
         if datei='firma_sollist' then
         query:=format('select * from %s where ts>="%s" and i_firma in (%s) into outfile "%s"',[datei,ts,firmen_nummern,datname_sql])
         //akt_untersuchung
         //re_positonen
         //untersuchung,
         else
         if datei='anamnese' then
         begin
            ma_join_temp:=stringreplace(ma_join,'i_mitarbeiter','nummer',[rfreplaceall]);
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join_temp,datei,ts,datname_sql]) ;
         end
         else
         if datei='untersuchung' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='impfung' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='labor' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='dokumente' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='besonderheiten' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='diagnosen' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='befunde' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
         if datei='ambulanz' then
            query:=format('select * from %s %s and %s.ts>="%s"  into outfile "%s"',[datei,ma_join,datei,ts,datname_sql])
         else
        if datei='schluessel' then
            //query:=format('select nummer, name, passwort,i_untersucher,ts,storno from %s  into outfile "%s"',[datei,datname_sql])
            if checkbox_user.Checked then
            begin
               nrs:=datamodul.q_schluessel.findfield('nummer').asstring;

               query:=format('select nummer, name, passwort, i_untersucher, ts, null, storno from %s where nummer in ("%s")  and ts>="%s"  into outfile "%s"',[datei,nrs,ts,datname_sql])
            end
            else
              query:=format('select nummer, name, passwort, i_untersucher, ts, null, storno from %s where ts>="%s" into outfile "%s"',[datei,ts,datname_sql])
            //die optionen sollen bei einer �nderung nicht �berschrieben werden
            //query:=format('select * from %s where ts>="%s" into outfile "%s"',[datei,ts,datname_sql])
         else
          if (datei='berechtigung_firma') and (host<>'lokal') then query:=format('select * from %s  into outfile "%s"',[datei,datname_sql])    //beim HS alle
         else
          if (datei='berechtigung_objekt') and (host<>'lokal') then query:=format('select * from %s  into outfile "%s"',[datei,datname_sql])   //beim HS alle
         else
		 	    query:=format('select * from %s where ts>="%s" into outfile "%s"',[datei,ts,datname_sql]);


          //memo.lines.Add(format('wird ausgef�hrt: %s '+timetostr(time()),[query]));

          q_2.edit;
          if sql_d.sql_exe(query) then q_2['kommentar']:='OK' else  q_2['kommentar']:='Fehler';
          q_2.post;

          //memo.lines.Add(format('wurde ausgef�hrt: %s '+timetostr(time()),[query]));
       


         if (modus ='abgleich') and (host= 'lokal') then
         begin
            t:=time();
            //if not FileExists(datname_temp) then
            while (not FileExists(datname_temp)) or (time()>t+0.0004) do//34 sec
            begin
               serververzeichnis_lesen(true);
               pause(500);
           end;
            //86400

            memo.lines.Add(format(' %s wird nach %s kopiert 1',[datname_temp,datname_lokal]));
            if not copyfile(pchar(datname_temp),pchar(datname_lokal),false)then 	memo.lines.Add(SysErrorMessage(GetLastError));
            t:=time();
            repeat
               Application.ProcessMessages
            until FileExists(datname_lokal) or (time()>t+0.0004);//10 sec

            if FileExists(datname_lokal) then
            	memo.lines.Add(format(' %s wurde nach %s kopiert ',[datname_temp,datname_lokal]))
            else
              begin
              memo.lines.Add(format('FEHLER: %s wurde nicht nach %s kopiert ',[datname_temp,datname_lokal])) ;
              fehler:=true;
              end;
         end;

         if (modus ='abgleich') and (host= 'haupt') then
         begin
            t:=time();

            while ( not FileExists(datname_lokal)) or (time()>t+0.0004) do    /////liest nicht immer   korrekt
            begin
               serververzeichnis_lesen(true);
               pause(500);

            end;

            if FileExists(datname_lokal) then
            	memo.lines.Add(format(' %s wurde erzeugt '+timetostr(time()),[datname_sql]))     //memo.lines.Add(format('wird ausgef�hrt: %s '+timetostr(time()),[query]));
            else
              begin
              memo.lines.Add(format('FEHLER: %s wurde nicht erzeugt '+timetostr(time()),[datname_sql])) ;
              fehler:=true;
              end;
         end;
         
       end; //(tabellen_liste.count=0)
       if checkbox_vg.Checked then application.BringToFront;
       application.ProcessMessages;
    q_2.next;
	 end;   //while schleife

   //query:=format('select * from haupt_tabelle into outfile "%s"',[datei,ts,datname_sql]);

   if modus='dat' then
   begin
     form_main.ZipMaster.ZipFileName:=ziptemp;
     form_main.ZipMaster.RootDir:= pfad_lokal;
     form_main.ZipMaster.AddOptions:= form_main.ZipMaster.AddOptions+[addmove];
     form_main.ZipMaster.Add;  //hier zippen
	   memo.lines.Add(format('Datei %s wird erstellt ',[ziptemp]));

     form_main.ZipMaster.AddOptions:= form_main.ZipMaster.AddOptions-[addmove];

     if form_main.datei_verschluesseln_key(ziptemp,zipdat) then
        memo.lines.Add(format('Datei %s wurde verschl�sselt erstellt',[zipdat]))
     else
     begin
          memo.lines.Add(format('Fehler beim Verschl�sseln der Datei %s oder Abbruch',[zipdat]));
          showmessage('Die Exportdatei wurde nicht erzeugt, Fehler beim Verschl�sseln oder Abbruch durch Benutzer');
          exit;
     end;

     memo.lines.add('#########################');
     memo.lines.Add('Export abgeschlossen nach');
     memo.lines.Add(zipdat);
     memo.lines.add('##########################');

   	if pos('warning',memo.text)>0 then
   	begin
	    memo_export.lines.Add('Es ist ein FEHLER aufgetreten --> Dateipfade �berpr�fen');
   	  showmessage('die Exportdatei konnte vermutlich nicht korrekt erzeugt werden ->siehe Protokoll');
   	  exit;
    end;

    showmessage ('Export abgeschlossen nach '+zipdat);

   end
   else    //jetzt abgleich modus ist nicht dat
   begin
     memo.lines.add('#########################');
     memo.lines.Add('Export abgeschlossen nach');
     memo.lines.Add(pfad_sql);
     memo.lines.add('##########################');
     //showmessage ('Export abgeschlossen nach '+pfad_sql);
  end;
finally
	filelist.Free;
	screen.cursor:=crdefault;
  deletefile(ziptemp);
end;

	if modus='dat' then q_6.InsertRecord([null,'EXPORT',es,now,now]);     //protokoll lokal
    result:=true;
end;
end;



procedure TForm_abgleich.BitBtn_dat_importClick(Sender: TObject);
var
query:string;
ini:boolean;
begin
  timer_pm.Enabled:=true;
  ini:=false;
  importieren('dat',mysql_d);
  if checkbox_inisat.checked then
  begin
    query:='select kommentar from export_import where tab_name="schluessel"';
    if mysql_d.Feldinhalt(query,0)='OK' then
    begin
      query:='delete from schluessel';
      mysql_d.sql_exe(query);
      ini:=true;
    end;
    query:='select kommentar from export_import where tab_name="untersucher"';
    if mysql_d.Feldinhalt(query,0)='OK' then
    begin
      query:='delete from untersucher';
      mysql_d.sql_exe(query);
      ini:=true;
    end;
    query:='select kommentar from export_import where tab_name="berechtigung_objekt"';
    if mysql_d.Feldinhalt(query,0)='OK' then
    begin
      query:='delete from berechtigung_objekt';
      mysql_d.sql_exe(query);
     ini:=true;
    end;

    query:='select kommentar from export_import where tab_name="berechtigung_firma"';
    if mysql_d.Feldinhalt(query,0)='OK' then
    begin
      query:='delete from berechtigung_firma';
      mysql_d.sql_exe(query);
      ini:=true;
    end;

    if ini then
    begin
      showmessage('bitte nochmals die gleiche Datei importieren');
      importieren('dat',mysql_d);
    end
    else showmessage('Die Importdatei enth�lt nicht genug Tabellen (Beim Export NUR "Datenableich f�r die Personalabteilung" ausw�hlen. )');
  end;

  timer_pm.Enabled:=false;
end;



function tform_abgleich.importieren(modus:string;sql_d:tmysql_d): boolean;
var
pfad,pf_zip,pfad_sql,pfad_lokal,query,datname_lokal,ziptemp,datname_sql,datname_temp, datei,a_name,a_haupt,s,sql1,sql2:string;
erfolg:boolean;
pfad_neu,s1,s2,s3,sql_von,sql_bis,abs_name:string;
von,bis:tdatetime;
absender:textfile;
gf,f:boolean;
memo:tmemo;
tab_status,i,z:integer;
t:ttime;

function tstodt(s:string):string;
begin
result:=copy(s,7,2)+'.'+copy(s,5,2)+'.'+copy(s,1,4)+'  '+copy(s,9,2)+':'+copy(s,11,2)+':'+copy(s,13,2);
end;



function ausnahmefaelle:boolean;
var
	nr_s,proband,s1,s2,f1,f2:string;
	i:integer;
	l1,l2:tstringlist;
    w,hs_daten,storno_daten,h1,h2:boolean;
begin
result:=true;

with datamodul do
try
l1:=tstringlist.Create;
l2:=tstringlist.Create;
form_imp_ausnahmen.ListBox1.Clear;
form_imp_ausnahmen.ListBox2.Clear;

query:='select count(*) from imp_tab3';
sql_new(false,q_7,query,'');
q_7.first;
		 if not q_7.eof then
			begin

        query:='select * from imp_tab3 order by nummer';
        sql_new(false,q_7,query,'');
        q_7.first;
			   while not q_7.eof do
         begin
                 //ermitteln welche felder unterschiedlich
						//nr_s:=q_7.FindField('nummer').asstring; //gilt jetzt auch f�r goae -nicht mehr n�tig da goae nur auf dem hauptsystem ge�ndert werden kann

				 		nr_s:=getbigint_str(q_7,'nummer');
                 //probanden zuordnen

				 		query:=format('select * from %s where nummer=%s',[datei,nr_s]);
                 sql_new(false,q_8,query,'');
                 if modus='haupt' then
                 begin
                  l1.Add('Datenbank: SATELLITENSYSTEM');
                  l2.Add('Datenbank: HAUPTSYSTEM');
                 end;
                 if modus='local' then
                 begin
                  l2.Add('Datenbank: SATELLITENSYSTEM');
                  l1.Add('Datenbank: HAUPTSYSTEM');
                 end;

                 l1.Add('�nderungsdatum: '+q_7.findfield('ts').asstring);
				 		l2.Add('�nderungsdatum: '+q_8.findfield('ts').asstring);  //ohne tstodt
                 l1.Add('Datei: '+datei);
                 l2.Add('Datei: '+datei);
                 l1.Add('Datensatznummer: '+q_7.findfield('nummer').asstring);
				  		l2.Add('Datensatznummer: '+q_8.findfield('nummer').asstring);

             hs_daten:=false;
             storno_daten:=false;

				 for i:=0 to q_7.FieldDefs.Count-1 do
				 begin
                  s1:=q_7.Fields[i].asstring;
                  s2:=q_8.fields[i].asstring;

                   f1:=q_7.FieldDefs.items[i].name;
                   f2:=q_8.FieldDefs.items[i].name;

                   w:=true;

                   if f1='storno' then
                   begin
                   	 if s1<>s2 then storno_daten:=true; //gilt immer
                     if (modus='haupt') and (s1='1') then h1:=true  else h2:=true;
                   end;

                   //if (f1='i_firma') or (f1='i_master') then hs_daten:=true;    2017-01-27
                   //if f1='i_mitarbeiter' then hs_daten:=true;                  ?????????????########################

                   
                   {
                   datei =firma dann
                   f1=i_firma  dann wurde geswitcht
                   wenn firma noch vorhanden dann ?bernehmen
                   wenn firma storniert dann nicht ?bernehmen

                   texte , i_master,
                   dokumente, bereich .... i_mitarbeiter
                   f1=i_mitarbeiter

                   f1=storno  das geht immer vor
                   ?bernommen wird der mit storno=1
                   }

                 if (f1='i_firma') or (f1='i_master') or (f1='i_mitarbeiter' ) then    //wenn sich die Zuordnung ge�ndert hat dann �bernahme HS
                    if s1<>s2 then hs_daten:=true;

                 if f1='ts' then w:=false;
                 if (datei='schluessel') and (f1='einstellungen') then w:=false;
                 if s1=s2 then w:=false;

                  if w    then      //nur ge�nderte
                  begin

                         l1.add('Feldname: '+f1);
                         l1.add('Feldinhalt: '+s1);
                            //l1.Add('-->'+s1);
                         l2.add('Feldname: '+f2);
                         l2.add('Feldinhalt: '+s2);
                            //l2.add('-->'+s2);
                  end;
               end;
         // ###########  verschiedene F�lle
               if storno_daten then
                 begin
                    //da wo storno
                    if  h1 then //storno auf sat
                    begin
                    //sat �bernehmen
                       if modus='haupt' then
                       begin
                          //query:=format('replace into %s select * from imp_tab3 where nummer=%s',[datei,nr_s]);
                          query:=mysql_d.replace_query('imp_tab3',datei,nr_s);

                          result:=sql_d.sql_exe(query) and result ;
                        end;
                    end
                  else
                    begin
                    //hs �bernehmen
                        if modus='local' then
                        begin
                           //query:=format('replace into %s select * from imp_tab3 where nummer=%s',[datei,nr_s]);
                           query:=mysql_d.replace_query('imp_tab3',datei,nr_s);
                           result:=sql_d.sql_exe(query) and result ;
                        end;
                    end;
                 end

               else //stornodaten
               if hs_daten then
               begin
                // HS erhalten, sat kopieren
                  if modus='local' then
                  begin
                    //query:=format('replace into %s select * from imp_tab3 where nummer=%s',[datei,nr_s]);
                    query:=mysql_d.replace_query('imp_tab3',datei,nr_s);
                    result:=sql_d.sql_exe(query) and result ;
                  end;

               end
               //###########################
               else
                //normale �bernahme
              if l1.count >4 then  //4
              begin
             form_imp_ausnahmen.ini(l1,l2);
							 //Form_imp_ausnahmen.CheckBox_neu.Checked:=true; //checkbox immer checked damit keine Abfrage erfolgt
							 // hier noch nihgt so ganz korrekt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!###

							 if Form_imp_ausnahmen.CheckBox_neu.Checked then //automatisch
							 begin
								//alle neueren �bernehmen    e
								 try
									//if strtodatetime(tstodt(q_7.findfield('ts').asstring))> strtodatetime(q_8.findfield('ts').asstring) then
                    if strtodatetime((q_7.findfield('ts').asstring))> strtodatetime(q_8.findfield('ts').asstring) then  //sollte niemas auftreten da oben in imptab2 abgefespert
                    begin
                     //kopieren
                     //query:=format('replace into %s select * from imp_tab3 where nummer=%s',[datei,nr_s]);
                     query:=mysql_d.replace_query('imp_tab3',datei,nr_s);
                     result:=sql_d.sql_exe(query) and result ;
                    end;
								 except
									//showmessage((tstodt(q_7.findfield('ts').asstring))+' '+datei+' '+(q_8.findfield('ts').asstring));
                  showmessage(((q_7.findfield('ts').asstring))+' '+datei+' '+(q_8.findfield('ts').asstring));
								 end;
							 end


							 else //manuell
              begin
                   if strtodatetime((q_7.findfield('ts').asstring))< strtodatetime(q_8.findfield('ts').asstring) then
                     Form_imp_ausnahmen.label_achtung.caption:=' Achtung!! Der Datensatz, der �bernommen wird ist �lter!'
                   else
                     Form_imp_ausnahmen.label_achtung.caption:=' Der Datensatz, der �bernommen wird ist aktueller!';

                  if Form_imp_ausnahmen.showmodal=mrok then     //�bernehmen
                  begin

                    //kopieren
                      //query:=format('replace into %s select * from imp_tab3 where nummer=%s',[datei,nr_s]);
                      query:=mysql_d.replace_query('imp_tab3',datei,nr_s);
                      result:=sql_d.sql_exe(query) and result ;

                  end
                else  //wenn H�kchen dann neueren �bernehmen
                   if Form_imp_ausnahmen.CheckBox_neu.Checked then
                   begin
                   //alle neueren �bernehmen
                     try
                        if strtodatetime((q_7.findfield('ts').asstring))> strtodatetime(q_8.findfield('ts').asstring) then
                        begin
                         //kopieren
                           //query:=format('replace into %s select * from imp_tab3 where nummer=%s',[datei,nr_s]);
                           query:=mysql_d.replace_query('imp_tab3',datei,nr_s);
                           result:=sql_d.sql_exe(query) and result ;
                        end;
                     except
                      showmessage(((q_7.findfield('ts').asstring))+' '+datei+' '+(q_8.findfield('ts').asstring));
                     end;
                    end
                    else  //der Vorschlag wird abgelehtn keine �bernahme, der bestehende ds ist der richtige!!
                     begin
                        query:=format('update %s set ts=localtimestamp where nummer=%1:s',[datei,nr_s]);
                        result:=sql_d.sql_exe(query) and result ;    // damit der j�ngere

                        //konflikte werden beim Einlesen in das HS behandelt, beim einlesen in das Sat gilt j�nger gewinnt
                     end;
                  end;
               end;

              l1.Clear;
              l2.Clear;
             q_7.Next;
          end;

      end;
finally
l1.Free;
l2.Free;
end;
end;

/// main ##############################################################################################################################

begin

    //pf_ha:= edit_pfhaupt.text;   //Abgeleichpfad aus sich HS
    //pf_lo:= edit_pflocal.text;
    if pagecontrol.activepage=tabsheet_dirabgl then
    begin
    	von:=strtodate(maskedit_dat2.text);
    	bis:=now();
    end
    else
    begin
       von:=strtodate(maskedit_dat3.text);
    	 bis:=now();
    end;

    pfad_abgleich_lokal:=edit_pflocal.text;
    pfad_abgleich_haupt:=edit_pfhaupt.text;

	sql_von:=sql_datetostr(tdate(von));
    sql_von:=stringreplace(sql_von,'-','',[rfreplaceall]);
    insert( '010001',sql_von,10);

    sql_bis:=sql_datetostr(tdate(bis));
    sql_bis:=stringreplace(sql_bis,'-','',[rfreplaceall]);
    insert( '010001',sql_bis,10);

if modus='dat' then
begin
	memo:=memo_export;
    //pfad_sql:=sql_d.sql_pfad(pfad_abgleich_haupt);
    pfad_sql:=sql_d.sql_pfad(pfad_temp);
    pfad_lokal:=pfad_temp;//pfad_abgleich_lokal;
end
else
begin
	memo:=memo_abgleich;
    if modus ='local' then
       //pfad_sql:= sql_d.sql_pfad(pfad_abgleich_lokal)
       pfad_sql:=sql_d.sql_pfad(pfad_temp)
    else
    	pfad_sql:=sql_d.sql_pfad(pfad_abgleich_haupt);

    pfad_lokal:=pfad_abgleich_lokal;

    memo.Lines.Add('---------------------------------');
    if modus='local' then memo.Lines.Add('Importieren ins Satellitensystem') else  memo.Lines.Add('Importieren ins Hautptsystem');
    memo.Lines.Add('---------------------------------');
end;


try
screen.Cursor:=crhourglass;
form_imp_ausnahmen:=tform_imp_ausnahmen.Create(self);

form_imp_ausnahmen.CheckBox_neu.Checked:=checkbox_neu.Checked;

   with datamodul do
    begin
    f:=false;
	  if (pfad_abgleich_lokal='') or (pfad_abgleich_haupt='') then
	  begin
    showmessage('Bitte Pfadangaben in Optionen vervollst�ndigen');
    exit;
    end;

  if modus='dat' then
  begin  // entschl�ssen, entzippen

    if not OpenDialog.Execute then exit;
    pfad:=OpenDialog.FileName; //verschl�sslete dat.
    if not fileexists(pfad) then
    begin
      showmessage('Datei "'+pfad+'"nicht vorhanden');
      exit;
    end;


    screen.Cursor:=crhourglass;

    memo_unzip.lines.Clear;
    //pfad_sql:=sql_d.sql_pfad(pfad_abgleich_haupt);
    //pfad_lokal:=pfad_abgleich_lokal;
    pf_zip:=copy(pfad_lokal,1,length(pfad_lokal));

    ziptemp:=copy(pfad_temp,1,length(pfad_temp))+'Arbene_zip_temp.zip';
    memo_unzip.Lines.Add(format('Datei %s wird entschl�sselt',[OpenDialog.FileName]));
    //form_main.CipherManager.DecodeFile(pfad,ziptemp);
    if form_main.datei_entschluesseln_key(pfad,ziptemp) then
       memo_unzip.lines.Add(format('Entschl�sseln abgeschlossen',[OpenDialog.FileName]))
       else
       begin
            memo_unzip.lines.Add(format('Fehler beim Entschl�sseln der Datei %s',[OpenDialog.FileName]));
            showmessage('Fehler beim Entschl�sseln der Datei');
            exit;
       end;
    //com.unzipfile(ziptemp, pf_zip ,memo_unzip);
    memo_unzip.lines.Add(format('Entpacken der Daten',[]));
    form_main.ZipMaster.FSpecArgs.Clear;
    form_main.ZipMaster.FSpecargs.add('*.*');
    form_main.ZipMaster.ZipFileName:=ziptemp;
    form_main.ZipMaster.RootDir:=pf_zip;
    form_main.ZipMaster.ExtrBaseDir:=  pf_zip;


    try
    form_main.ZipMaster.Extract;
    except
      ShowMessage('Error in Extract;  DLL Exception ');
    End ;

    deletefile(ziptemp);
     memo_unzip.lines.Add(format('Entpacken abgeschlossen',[]));
    memo_unzip.lines.Add(format('Import startet aus dem Verzeichnis %s',[pf_zip]));

	abs_name:=datamodul.q_4.findfield('name').asstring;

    if (a_haupt='1') and (system_modus=0) then //Absender muss haupt-System sein; nur beim Satelliten-System erlaubt
    begin
      q_1.edit;
      q_1['hs1']:=s1;
      q_1['hs2']:=s2;
      q_1['ln']:=s3;
      q_1.post;
    end;

    memo_unzip.Lines.Add('Dateiimport aus Sicht des MySQL-Servers');
    end;
    //ende modus dat #######################################################

    //################  �berpr�fen ob absender ok ###############

    gf:=false;
    a_name:=pfad_lokal+'absender.txt';
    if not fileexists(a_name) then
    begin
         showmessage('Datei Absender.txt nicht gefunden. Bitte Dateipfade �berpr�fen. Import abgebrochen');
         exit;
    end;

    assignfile(absender,a_name );
    reset(absender);
    //q_1.Edit;
    readln(absender,s1);
    //q_1['hs1']:=s1;
    readln(absender,s2);
    //q_1['hs2']:=s2
    readln(absender,s3);
    //s:=q_1['ln']
    readln(absender,s);
    von:=strtodatetime(s);
    readln(absender,s);
    bis:=strtodatetime(s);

    readln(absender,a_haupt );
    if a_haupt='1' then //absender haupt_system
    begin   //ist die aktelle anwendung gemeint
        while not eof(absender) do
        begin
         readln(absender,s);
         if s=schluessel then gf:=true;
        end;

    end
	  else  //ist der absender als satellit registriert
	  begin
        readln(absender,s);
		    gf:=q_4.Locate('schluessel',s,[]);

     end;

    closefile(absender);
    deletefile(a_name);


    if not gf then
    begin
      if system_modus =1 then //haupt-system
         showmessage('Die Abgleich-Datei geh�rt nicht zu diesem System oder stammt nicht von einem Satelliten.')
      else
         showmessage('Die Abgleich-Datei geh�rt nicht zu diesem System. '+#13+'Ist die Datenbank des Hauptsystems korrekt eingetragen?');  //nur bei direktem Abgleich
      screen.Cursor:=crdefault;
      //if user_name<>'arbene_master12' then exit; // ###
        button_abgleichstart.enabled:=false;
        memo_abgleich.Lines.Add('');
        memo_abgleich.Lines.Add('Bitte das Formular schlie�en und erneut abgeleichen');
       exit;
    end; 

    // ############ ende �berpr�fung #######################

    memo_unzip.lines.Add(format('�berpr�fung des Absenders abgeschlossen',[]));




    //schluessel l�schen  wird wieder neu erstellt   aber nur beim gesamtabgleich
    if modus='local' then
    begin
    	//query:='delete from schluessel';
      if (liste_tabellen.count=0) or (liste_tabellen.IndexOf('berechtigung_firma')>=0)   then
      begin
        query:='drop table if exists berechtigung_firma_old ' ;
        sql_d.sql_exe(query);

        query:='create table berechtigung_firma_old like berechtigung_firma' ;
        sql_d.sql_exe(query);

        query:='insert into berechtigung_firma_old select * from berechtigung_firma' ;
        sql_d.sql_exe(query);

        query:='delete from berechtigung_firma';
        sql_d.sql_exe(query);
      end;

      if (liste_tabellen.count=0) or (liste_tabellen.IndexOf('berechtigung_objekt')>=0)   then
      begin
        query:='drop table if exists berechtigung_objekt_old ' ;
        sql_d.sql_exe(query);

        query:='create table berechtigung_objekt_old like berechtigung_objekt' ;
        sql_d.sql_exe(query);

        query:='insert into berechtigung_objekt_old select * from berechtigung_objekt' ;
        sql_d.sql_exe(query);

        query:='delete from berechtigung_objekt';
        sql_d.sql_exe(query);
      end;
    end;
    
    i:=0;
    q_3.first; // export_import lokal
    while not q_3.eof do
    begin
      q_3.edit;
    	q_3['kommentar']:='';
    	q_3.next;
        inc(i);
    end;
    memo.Lines.Add(format('Es werden  %d Dateien importiert',[i]));
    memo.Lines.Add(format('Pfad sql: %s',[pfad_sql]));
    memo.Lines.Add(format('Pfad temp: %s',[pfad_temp]));
    memo.Lines.Add(format('Pfad lokal: %s',[pfad_lokal]));
    application.ProcessMessages;
    //showmessage('import ');
    q_3.first;   //###########################schleife  ################################################
    while (not q_3.eof) and ( not abbruchcheck) do
    begin
       //Form_imp_ausnahmen.CheckBox_neu.Checked:=CheckBox_neu.Checked;
       erfolg:=true;
       datei:=q_3.findfield('tab_name').asstring;
       //### hier abfragen ob aktualisiert werden muss


       tab_status:=q_3.findfield('main').asinteger;
       datname_lokal:=pfad_lokal+datei+'.sql';   //lokale sicht
       datname_sql:=pfad_sql+datei+'.sql';       //sicht des sql-servers
       datname_temp:=pfad_temp+ datei+'.sql';

     if not b.b_hat_tab_ber(datei,0) then
     begin
      memo.Lines.Add(format('# keine Berechtigung f�r  %s, kein Import ',[datei]));
     end;

     if b.b_hat_tab_ber(datei,0) then
     if (liste_tabellen.count=0) or (liste_tabellen.IndexOf(datei)>=0) then // (tabellen_liste.count=0)
      begin

           memo.Lines.Add(format('Datei %s wird von %s importiert (lokale Sicht)',[datei, datname_lokal]));
           application.ProcessMessages;

           if ((modus ='local') or (modus='dat') ) then
           begin
             memo.Lines.Add(format(' %s wird nach  %s kopiert 2',[datname_lokal, datname_temp]));

             t:=time();
             repeat
             Application.ProcessMessages
            until FileExists(datname_lokal) or (time()>t+0.00001);//1 sec  //wenn abgleichfehler hier wieder l�nger!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            if not  FileExists(datname_lokal) and (liste_tabellen.count=0) then memo.Lines.Add('FEHLER: Die Datei "'+datname_lokal+'" ist nicht vorhanden' );

            if not windows.copyfile(pchar(datname_lokal),pchar(datname_temp),false) then
            begin
                if (liste_tabellen.count=0) then
                begin
                  memo.Lines.Add('copyfile('+datname_lokal+' , '+ datname_temp);
                  memo.Lines.Add(SysErrorMessage(GetLastError));
                  f:=true;
                  fehler:=true;
                end;
            end
            else
                memo.Lines.Add(format(' %s wurde nach  %s kopiert 2',[datname_lokal, datname_temp]));

            application.ProcessMessages;
          end;
          application.ProcessMessages;

          //##############################################
          if fileexists(datname_lokal) then     //##++
           begin
            memo.Lines.Add(format('Datei %s wird von %s importiert',[datei, datname_sql]));
            application.ProcessMessages;

                  erfolg:=false;
                  z:=0;
                  while not erfolg do
                  begin
                      // #####################hier krachts gerne

                      erfolg:=sql_d.struktur_von_db(datei,'imp_tab'); //nur struktur
                      //#####################################
                      memo.Lines.Add(format('%s1 Struktur von  %s erzeugt %s',[modus,datei, booltostr(erfolg,true	)]));

                      application.ProcessMessages;

                      query:=format('load data infile "%s" REPLACE into table imp_tab',[datname_sql]);   //imptab f�llen
                      erfolg:=sql_d.sql_exe(query) and erfolg;
                      memo.Lines.Add(format('1 %s ausgef�hrt: %s',[query,booltostr(erfolg,true	)]));
                      if not erfolg then memo.Lines.Add(format('Laden der Datei %s fehlgeschlagen',[query]));
                      application.ProcessMessages;

                      inc(z);
                      if z>10 then
                      begin
                         showmessage ('Fehler beim Erzeugen der Importdatei 1'+#10+'Existiert '+datname_sql+' ?'+#10+'Importiert wird in das '+modus+'-System');
                         exit;
                      end;

               end;


                erfolg:=false;
                z:=0;
                while not erfolg do
                begin
                      erfolg:=sql_d.struktur_von_db(datei,'imp_tab2'); //nur struktur
                      memo.Lines.Add(format('2 Struktur von  %s erzeugt: %s',[datei,booltostr(erfolg,true)]));
                      application.ProcessMessages;
                       // in der Imptab sind nur die Datens�tze also >sql_von
                       // eingelesen werden d�rfen nur die s�tze mit datei.ts
                      //query:=format('insert into imp_tab2 select imp_tab.* from imp_tab left join %0:s on imp_tab.nummer=%0:s.nummer where (imp_tab.ts>= %0:s.ts) or (%0:s.ts is null)',[datei]);
                        //neuere Dateien nach imptab_2     also in der zieldat vor sqldat ge�ndert und in der quelle nach sqldat ge�ndert oder nur in der quelle vorhanden

                      //hier kommen nur die rein, die entweder neuer sind oder auf dem importierenden system nicht ge�ndert wurden , der rest wird in imp_tab3
                      if (checkbox_neu.Checked) or (modus='local') then   // wenn auf dem sat eingelesen wird dann gewinnt immer dern neueste
                        query:=format('insert into imp_tab2 select imp_tab.* from imp_tab left join %0:s on imp_tab.nummer=%0:s.nummer where ((imp_tab.ts> %0:s.ts) ) or (%0:s.ts is null)',[datei,sql_von])  //korrektur ohne        and (%0:s.ts<%1:s) notwendit
                      else
                        query:=format('insert into imp_tab2 select imp_tab.* from imp_tab left join %0:s on imp_tab.nummer=%0:s.nummer where ( (%0:s.ts<%1:s) or (%0:s.ts is null))',[datei,sql_von]);  //korrektur ohne        and (%0:s.ts<%1:s) notwendit

                      if tab_status=1 then query:='insert into imp_tab2 select imp_tab.* from imp_tab'; //hautpdateitabellen


                      erfolg:=sql_d.sql_exe(query) and erfolg;
                      memo.Lines.Add(format('3 %s ausgef�hrt: %s',[query ,booltostr(erfolg,true )]));
                      application.ProcessMessages;
                      inc(z);
                      if z>10 then
                      begin
                         showmessage ('Fehler beim Erzeugen der Importdatei 2- Ungleiche Programmversion?');
                         exit;
                      end;
                end;


            //if form_main.dateistruktur(datei)<>form_main.dateistruktur('imp_tab2') then
               sql1:=sql_d.create_sql_von_db(datei);
               sql1:=db_create_normieren(sql1);
               sql2:=sql_d.create_sql_von_db('imp_tab2');
               sql2:=db_create_normieren(sql2);
               if sql1<>sql2 then
               begin
                    showmessage ('Dateistrukturen von '+ datei+' entsprechen sich nicht - Ungleiche Programmversion?'+#13#10 +
                   sql1+#13#10+sql2);
                    exit;
               end;

               if sql1=sql2 then
               begin
                  //query:=format('replace into %s select * from imp_tab2 ',[datei]); //hier importieren   //ts wird �bernommen
                  query:=mysql_d.replace_query('imp_tab2',datei,'') ;
                  erfolg:=sql_d.sql_exe(query) and erfolg;
                  memo.Lines.Add(format('4 %s ausgef�hrt: %s',[query ,booltostr(erfolg,true )]));
                  //pause(1);


                  // jetzt ausnahmef�lle   alle die in der imp_tab sind und nicht eingelesen wurden  also datei.ts > startdatum
                 if (tab_status=0) or (tab_status=2) then   // nicht die Hauptsystemtabellen status =1
                 begin
                    //sql_d.drop_table('imp_tab3');
                    erfolg:=false;
                    while not erfolg do
                       begin
                        erfolg:=sql_d.struktur_von_db(datei,'imp_tab3'); //rezeugt imp_tab3 nach dem muster von datei

                       query:=format('insert into imp_tab3 select imp_tab.* from imp_tab left join %0:s on imp_tab.nummer=%0:s.nummer where (imp_tab.ts<> %0:s.ts) and (%0:s.ts >=%1:s  ) ',[datei,sql_von,sql_bis]);
                       erfolg:=sql_d.sql_exe(query) and erfolg;
                       memo.Lines.Add(format('5 %s ausgef�hrt: %s',[query ,booltostr(erfolg,true )]));
                       //if not erfolg then showmessage(query );
                      end;
                    erfolg:=ausnahmefaelle  and erfolg;
                    memo.Lines.Add(format('6 Ausnahmef�lle bearbeitet: %s',[booltostr(erfolg,true )]));
                 end
                 else memo.Lines.Add(format('5,6 Hauptsystemtabelle importiert: %s',[booltostr(erfolg,true )]));;
                 //pause(1);
                 deletefile(datname_lokal);
                 deletefile(datname_temp);
                 q_3.edit;
                 if erfolg then q_3['kommentar']:='OK' else
                 begin
                      memo.Lines.Add('#########');
                      memo.Lines.Add(query);
                      memo.Lines.Add('#########');
                      q_3['kommentar']:='Fehler';
                      f:=true;
                      fehler:=true;
                 end;

                 q_3.post;
               end; //sql1=sql2
           end;
        end;
     //exit; //20160214
     if checkbox_vg.Checked then application.BringToFront;
     application.ProcessMessages;
    q_3.next;
	  end;
      // importieren abfragedateien
      // f�r alle abfragen
      // imp_dat ab_*.sql

	  abs_name:= 'IMPORT von '+abs_name ;
      if modus= 'dat' then
	  	q_6.insertRecord([null,abs_name,von,bis,now]); //von,bis ,zeitpunkt   protokoll lokal
      //q_6.refresh;
     memo.lines.add('###################');
     memo.Lines.Add('Import abgeschlossen');
     memo.lines.add('###################');

    if f then memo.Lines.Add('Es ist ein FEHLER aufgetreten --> Dateipfade �berpr�fen');

    if modus='dat' then
    begin
    memo_unzip.lines.Add(format('Import abgeschlossen',[]));
    //showmessage('Import abgeschlossen');
    end;
    //form_main.tag:=2;

    end;
    result:=true;
finally
   form_imp_ausnahmen.release;
   screen.Cursor:=crdefault;
end;


end;


function TForm_abgleich.db_create_normieren(sql:string):string;
var
p:integer;
rest:string;
begin
   p:=pos('ENGINE',sql);
   if p>0 then
   begin
   	result:=copy(sql,1,p-1);
      //showmessage(result);
      //beep();
   end
   else result:=sql;
end;

procedure TForm_abgleich.SpeedButton1Click(Sender: TObject);
var s:string;
begin
if form_kalender.showmodal =mrok then
	 begin

     s:=datetostr(int(form_kalender.auswahlkalender.date));
     if strtodate(s)<strtodate('01.01.1970') then    s:='01.01.1970';

     if sender=speedbutton1 then maskedit_dat1.Text:=s
     else maskedit_dat2.Text:=s;
	 end;
end;

procedure TForm_abgleich.Button_firmaClick(Sender: TObject);
var
query:string;
begin
try
    dbgridext4.Visible:=false;
	form_f_wahl:=tform_f_wahl.create(self);
	if form_f_wahl.ShowModal=mrok then
	begin
	  ff:=form_f_wahl.firmen_nummern;
	  query:=format('select firma from firma where nummer in (%s) ',[ff]);
	  datamodul.sql(true,datamodul.q_5,query,'firma');
	end
	else
	begin
		datamodul.sql(true,datamodul.q_5,'select firma from firma where nummer=-1','');
	end;
    dbgridext4.Visible:=true;
	firmen_nummern:= form_f_wahl.firmen_nummern;
   firmen_nummern_wahl:=form_f_wahl.firmen_nummern;
finally
	form_f_wahl.Release;

end;
end;

procedure TForm_abgleich.ToolButton1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_abgleich.CheckBox1Click(Sender: TObject);
begin
if checkbox1.Checked then
begin
	maskedit_dat1.editText:='01.01.1970';
    maskedit_dat1.enabled:=false;
    speedbutton1.Enabled:=false;
end
    else
begin
    maskedit_dat1.edittext:=lastexport;
    maskedit_dat1.enabled:=true;
    speedbutton1.Enabled:=true;
end;
end;

procedure TForm_abgleich.CheckBox2Click(Sender: TObject);
begin
if checkbox2.Checked then
begin
	  maskedit_dat2.editText:='01.01.1970';
    maskedit_dat2.enabled:=false;
    speedbutton2.Enabled:=false;
    //if Messagedlg('Soll wirklich �ber den gesamten Zeitraum abgeglichen werden?'+#10+'Dies ist in der Regel nur beim ersten Abgleich notwendig.',mtConfirmation, [mbyes,mbno],0)<>mryes then
    //checkbox2.Checked:=false;
end
else
begin
    maskedit_dat2.edittext:=lastexport;
    maskedit_dat2.enabled:=true;
    speedbutton2.Enabled:=true;
end;
end;

procedure TForm_abgleich.BitBtn_lokalClick(Sender: TObject);
var
pfad:string;
position:integer;
begin
  form_pfadwahl:=tform_pfadwahl.create(self);
  if form_pfadwahl.showmodal =mrok then
  begin
       pfad:=form_pfadwahl.DirectoryListBox.Directory;
       if copy(pfad,length(pfad),1)<>'\'then pfad:=pfad+'\';

       if tbitbtn(sender)=BitBtn_lokal then edit_pflocal.Text:=pfad;
  end;
  form_pfadwahl.Release;

  serververzeichnis_lesen(false);
end;



procedure TForm_abgleich.Button_abgleichstartClick(Sender: TObject);
begin
  if b_abbruch then
  begin
    showmessage('bitte schlie�en Sie das Formular');
    exit;
  end;
  timer_pm.Enabled:=true;


	if direktabgleich and (not checkbox_nur_export.Checked) then   vergleichen;   //20160214

  timer_pm.Enabled:=false;
end;


function TForm_abgleich.direktabgleich:boolean;
var
t,abs_name,pf_lo,pf_ha,h_lastupdate,s_lastupdate,query,s,s1,sperrdat_name,h_last_impor,server,laufwerk,spfad,schluessel_vomhs,nummern_kreis:string;
von,bis,befehl:string;
//hmysql_d:tmysql_d;
sperrdatei:textfile;
i:integer;
//d_h_last_import: tdate;
begin
result:=false;
spfad:=edit_pfhaupt.Text;
spfad:=copy(spfad,3,length(spfad));
server:=edit_iphaupt.Text;
server:=format('\\%s%s',[server,spfad]) ;
if copy(server,length(server),1)='\' then server :=copy(server,1,length(server)-1);
laufwerk:=copy(edit_pflocal.Text,1,1)+':';
b_abbruch:=true;


with datamodul do
begin
//datum ts �berpr�fen darf nich kleiner als 1970 sein
if strtodate(maskedit_dat2.text)<strtodate('01.01.1970') then  maskedit_dat2.text:='01.01.1970';
//zuerst pfade und DB �berpr�fen
fehler:=false;
pf_lo:=edit_pflocal.text ;
pf_ha:= edit_pfhaupt.text;
//befehl:=format('/e,%s, /select, "%s*.*"',[pf_lo,pf_lo]);
befehl:=format('/e,%s, /expore, "%s*.*"',[pf_lo,pf_lo]);

//ShellExecute_se( self.Handle,NIL,PChar('explorer.exe'),PChar(befehl),nil,SW_SHOWNOACTIVATE );
application.ProcessMessages;
pause(100);
application.BringToFront;
if not DirectoryExists(pf_lo)then
begin
  ShellExecute_se( self.Handle,NIL,PChar('explorer.exe'),PChar(befehl),nil,SW_SHOWNOACTIVATE );
  pause(2000);
  if not DirectoryExists(pf_lo)then
  begin
    showmessage ('Der Pfad zum gemeinsamen Verzeichnis '+pf_lo+' existiert nicht.'+#13#10+'Bitte im Explorer �ffnen und dann noch mal abgleichen.');

    b_abbruch:=true;
    exit;
  end;
end;

sperrdat_name:=pf_lo+'sperrdatei.txt';

if fileexists(sperrdat_name) then
begin
   assignfile(sperrdatei,sperrdat_name );
   reset(sperrdatei);
   readln(sperrdatei,s);
   closefile(sperrdatei);
   if Messagedlg('Es wird gerade ein Abgleich durch '+s+' durchgef�hrt. Trotzdem weiter?',mtConfirmation, mbOkCancel	,0)<>mrOK
   then exit;
   if Messagedlg('Wirklich weiter?',mtConfirmation, mbOkCancel	,0)<>mrOK
   then
   begin
    b_abbruch:=true;
    exit;
   end;
end;


assignfile(sperrdatei,sperrdat_name );
rewrite(sperrdatei);
s:=user_name;
writeln(sperrdatei,s);
closefile(sperrdatei);

if lowercase(copy(pf_ha,1,1))=lowercase(copy(pf_lo,1,1)) then
begin
showmessage ('Die Laufwerksbuchstaben vom Satelliten und Server d�rfen nicht gleich sein.');
if not ((user_name ='klawitter') or (user_name ='kla'))   then exit;
end;

if trim(lowercase(pfad_temp))='c:\' then
begin
	showmessage('Der tempor�re Pfad darf nicht "c:\" sein.'+ chr(13) + 'Bitte unter Men�->Optionen ab�ndern.');
   exit;
end;

if trim(lowercase(pfad_temp))=trim(lowercase(pfad_abgleich_lokal)) then
begin
	showmessage('Der tempor�re Pfad darf nicht gleich dem Abgeleichpfad sein sein.'+ chr(13) + 'Bitte unter Men�->Optionen ab�ndern.');
   exit;
end;

if not directoryexists(pfad_temp) then showmessage(format('Achtung, der Pfad %s existiert nicht. siehe Optionen.12', [ pfad_temp]));

try
strtodate (maskedit_dat2.editText);
except
showmessage('ung�ltiges Datum');
exit;
end;

pf_ha:=trim(pf_ha);
if copy(pf_ha,length(pf_ha),1)<>'\' then pf_ha:=pf_ha+'\';
edit_pfhaupt.Text:=pf_ha;
datamodul.connection_hs.Disconnect;
datamodul.Connection_hs.HostName:=edit_iphaupt.Text;
datamodul.Connection_hs.Database:=edit_dbhaupt.text;
datamodul.connection_hs.User:=db_login;
datamodul.connection_hs.Password:=db_passwort;
datamodul.connection_hs.Protocol:=db_protocol;
datamodul.connection_hs.Port:=strtoint(db_port);



if (not checkbox_alle.Checked) and (not datensatz(datamodul.q_5))  then
begin
	showmessage('Bitte Firma ausw�hlen');
	exit;
end;
memo_abgleich.Lines.add ( '#### �berpr�fen der Einstellungen ###');

try
datamodul.Connection_hs.Connect;
except
showmessage ('Verbindung mit der Hautpdatenbank konnte nicht hergestellt werden');
exit;
end;
memo_abgleich.Lines.add ('Verbindung mit der Hautpdatenbank wurde hergestellt');
//pr�fen ob Pfad existiert
//testdatei schreiben, lese vom HS wenn ok dann weiter
try

hmysql_d.connect( edit_iphaupt.Text,db_protocol,db_port,edit_dbhaupt.text,db_login,db_passwort);


// hat die Hauptdatenbank die richtige Version

query:='select i1 from haupt_tabelle';
h_lastupdate:=(hmysql_d.Feldinhalt(query,0));

query:='select i1 from haupt_tabelle';
s_lastupdate:=(mysql_d.Feldinhalt(query,0));

if s_lastupdate<>h_lastupdate then
begin
messagedlg( 'Die Hautpdatenbank und Satellitendatenbank sind nicht auf dem selben Aktualisierungsstand,'+chr(13)+' Arbene.exe auf dem Haupt- und dem Satelliten-System updaten. ',mtInformation	, [mbok, mbhelp],69);
//showmessage ('Die Hautpdatenbank und Satellitendatenbank sind nicht auf dem selben Aktualisierungsstand,'+chr(13)+' Arbene.exe auf dem Haupt- und dem Satelliten-System updaten. ');
exit;
end;
memo_abgleich.Lines.add ('Beide Datenbanken haben den gleichen Aktualisierungsstand: '+s_lastupdate);


// trage den user  in die Haupttabelle HS ein
query:=format('update haupt_tabelle set i2=%s',[inttostr(user_id)]);
hmysql_d.sql_exe(query);

// lies den schl�ssel aus der Hauptdatenbank

query:='select nummern_kreis from haupt_tabelle';
nummern_kreis:=mysql_d.Feldinhalt(query,0);

query:=format('select schluessel from satelliten where nummern_kreis="%s"',[nummern_kreis]);
schluessel_vomhs:=hmysql_d.Feldinhalt(query,0);

query:=format('update haupt_tabelle set s1="%s" where length(s1)<4',[schluessel_vomhs]);
mysql_d.sql_exe(query);

query:='select s1 from haupt_tabelle';  //nochmals neu lesen
schluessel:=mysql_d.Feldinhalt(query,0);


memo_abgleich.Lines.add ('Das Laufwerk '+pf_lo +'wird �berpr�ft');
// Datei vom Server in das gemeinsame Laufwerk schreiben und dies testen.
s:=pf_lo+'server.txt';
while fileexists(s) do
begin
   deletefile(s);
   application.ProcessMessages;
end;
s:= pf_ha+'server.txt';
//if fileexists(s) then deletefile(s);

s1:=datetimetostr(now());
s:= stringreplace(s,'\','/',[rfreplaceall]);
query:= format('SELECT "%s" INTO DUMPFILE "%s"',[s1,s]);
//showmessage(  query);
try
  sql_processor.Connection:=datamodul.Connection_hs;
  sql_processor.Clear;
  sql_processor.Script.Add(query);
  sql_processor.Execute;
  except
    showmessage('1 Die gemeinsamen Pfade von Satellit ('+pf_lo+') und Hauptsystem ('+pf_ha+') weisen nicht auf das selbe Verzeichnis ');
    b_abbruch:=true;
    exit;
  end;
  //hmysql_d.sql_exe(query);
  memo_abgleich.Lines.add ( 'Die Datei server.txt wurde auf dem Server auf'+pf_ha+'geschrieben');
  sql_processor.Connection:=datamodul.connection_main ;

  s:=pf_lo+'server.txt';
  i:=0;
  while (not fileexists(s)) or (I>10 ) do
  begin
     pause(1000);
     inc(i);
  end;

  if not fileexists(s) then //kann auch irgendwo anders liegen
  begin
    showmessage('2 Die gemeinsamen Pfade von Satellit ('+pf_lo+') und Hauptsystem ('+pf_ha+') weisen nicht auf das selbe Verzeichnis ');
    exit;
  end;
  memo_abgleich.Lines.add ( 'Die Datei server.txt konnte von Lokal ('+pf_lo+') gelesen werden.');
  memo_abgleich.Lines.add ( '#### Einstellungen wurden �berpr�ft ###');
  memo_abgleich.Lines.add ( '');
  deletefile(s);

  //

  //jetzt kanns los gehen
  //zuerst Schreiben der lokalen sql_dateien in das Verzeichnis

  b_abbruch:=false;

  abs_name:=datamodul.q_1.findfield('name').asstring;
  von:=maskedit_dat2.editText;

  if  checkbox_alle.Checked then firmen_nummern:=alle_firmennummern_berechtigt;  //lokal

  serververzeichnis_lesen(false);
  if abbruchcheck then exit;

  if not exportieren('abgleich','lokal',mysql_d,false) then exit;  //#####################################
  serververzeichnis_lesen(false);
  //exit;//20160214
  //importieren in die Hauptdatenbank
  q_1.Connection:=datamodul.Connection_hs;
  q_4.Connection:=datamodul.Connection_hs;
  q_6.Connection:=datamodul.Connection_hs;   //protokolldatei
  q_7.Connection:=datamodul.Connection_hs;
  q_8.Connection:=datamodul.Connection_hs;
  q_9.Connection:=datamodul.Connection_hs;   //f�r dateistruktur
  sql_processor.Connection:=datamodul.Connection_hs;

  //if  checkbox_alle.Checked then firmen_nummern:=alle_firmennummern; //mit q_7


  sql_new(false,q_1,'select * from haupt_tabelle',''); //hauptdatenbank
  sql_new(true,q_4,'select * from satelliten','');
  sql_new( false,q_6,'select * from abgleich_prot order by zeitpunkt desc','');

  sql_new(true,q_2,'select * from export_import where main>=0 order by tab_name',''); //order by tab_name
  sql_new(true,q_3,'select * from export_import where main=0 order by tab_name','');

  serververzeichnis_lesen(false);
  if abbruchcheck then exit;
  if not importieren('haupt',hmysql_d) then
  begin
   b_abbruch:=true;
   exit;    //###############################################
  end;
  
  if abbruchcheck then exit;
  if checkbox_nur_export.Checked then
  begin
     memo_abgleich.Lines.Add('');
      memo_abgleich.Lines.Add('Es wurden alle Daten auf das Hauptsystem �bertragen');
       memo_abgleich.Lines.Add('Es wurdne keine Daten vom Hauptsystem auf diesen Rechner �bertragen ');
    exit;
  end;
  /////////////////////////////////halbzeit#/////////////////////////////////////////////////////////////

  //serververzeichnis_lesen;
  //exportieren aus der hauptdatenbank in das lokale Verzeichnis
  //exit;//20160214
  if  checkbox_alle.Checked then firmen_nummern:=alle_firmennummern_berechtigt; //hauptsystem
  if abbruchcheck then exit;
  if not exportieren('abgleich','haupt',hmysql_d,false) then exit;    //##########################################
  serververzeichnis_lesen(false);
  t:='Abgleich von '+abs_name;
  q_6.insertRecord([null,t,von,now,now]);

  //einlesen in die lokale Datenbank
  q_1.Connection:=connection_main ;
  q_4.Connection:=connection_main ;
  q_6.Connection:=connection_main ;
  q_7.Connection:=Connection_main;
  q_8.Connection:=Connection_main;
  q_9.Connection:=Connection_main;
  sql_processor.Connection:=Connection_main;


  sql_new(false,q_1,'select * from haupt_tabelle',''); //sat
  sql_new(true,q_4,'select * from satelliten','');
  sql_new( false,q_6,'select * from abgleich_prot order by zeitpunkt desc','');

  sql_new(true,q_2,'select * from export_import where main=0 order by tab_name','');
  sql_new(true,q_3,'select * from export_import where main>=0 order by tab_name','');

  if checkbox2.checked then  //gesamtzeit gew�hlt
  begin
    //l�schen von berechtigung_firma

     //vorher sicherung erstellen, wenn am ende keine Aktualisierung dann sollten die alten Dateien �bernommen werden
      if MessageDlg('Sollen die vorhandenen Einstellungen �berschrieben werden? '+chr(13)+'Bei der Einrichtung des Satelliten ->JA sonst NEIN', mtConfirmation	,[mbYes, mbNo], 0)=mryes then
      begin
         query:='delete from berechtigung_firma';
         mysql_d.sql_exe(query);
         //l�schen von berechtigung_objekt, untersucher, schluessel
         query:='delete from berechtigung_objekt';
         mysql_d.sql_exe(query);
         query:='delete from untersucher';
         mysql_d.sql_exe(query);
         query:='delete from schluessel';
         mysql_d.sql_exe(query);
         form_main.tag:=2;
         showmessage('�berpr�fen Sie nach dem n�chsten Neustart von Arbene bitte unter Optionen die Dateipfade.');
      end;


  end;

  serververzeichnis_lesen(false);
  if abbruchcheck then exit;
  if not importieren('local',mysql_d) then exit;   //###############################################################################
  memo_abgleich.Lines.Add('');
  
  if fehler then
    memo_abgleich.Lines.Add('FEHLER: Es trat ein Fehler auf! siehe Protokoll ')
  else
  begin
     memo_abgleich.Lines.Add('Der Datenabgleich zwischen Satelliten und Hauptsystem');
     memo_abgleich.Lines.Add('wurde vollst�ndig durchgef�hrt.');
     memo_abgleich.Lines.Add('Alle Datens�tze wurden �bertragen');
  end;
finally
  t:='Abgleich mit Hauptsystem '+abs_name;
  if checkbox_nur_export.checked then t:='Abgleich auf Hauptsystem '+abs_name;
  if b_abbruch then t:='Abbruch des Abgleichs mit Hauptsystem '+abs_name;

  if not b_abbruch then
  begin
    q_6.insertRecord([null,t,von,now,now]);  //gef�hrlich

    q_1.edit;
    q_1['last_export']:=date(); //es
    q_1.post;
    result:=true;
  end;

  if fileexists(sperrdat_name) then  deletefile(sperrdat_name);

  datamodul.Connection_hs.disConnect;

  if  not b_abbruch then serververzeichnis_lesen(false);
  
  if b_abbruch then result:=false;
end;
end;
end;


function TForm_abgleich.alle_firmennummern:string;
var
query:string;
begin
with datamodul do
begin
    query:='select nummer from firma order by nummer';
    sql_new(false,q_7,query,'');
    result:=',-2';
    q_7.first;
	while not q_7.eof do
	begin
		result:=result+','+q_7.findfield('nummer').asstring;
		q_7.next;
	end;
	result:=copy(result, 2,length(result)); //erstes komma weg

end;
end;


function TForm_abgleich.alle_firmennummern_berechtigt:string;
var
query:string;
begin
with datamodul do
begin
    query:=format('select * from berechtigung_firma where benutzer=%d and storno=0',[user_id]);
    sql_new(false,q_7,query,'');
    result:=',-2';
    q_7.first;
	while not q_7.eof do
	begin
		result:=result+','+q_7.findfield('i_firma').asstring;
		q_7.next;
	end;
	result:=copy(result, 2,length(result)); //erstes komma weg

end;
end;

procedure TForm_abgleich.SpeedButton3Click(Sender: TObject);
begin
maskedit_dat3.Text:=datetostr(int(form_kalender.auswahlkalender.date));
end;

procedure TForm_abgleich.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
with datamodul do
begin
    connection_hs.Disconnect;
    liste_tabellen.Free;
		q_1.Connection:=connection_main ;
    q_1.close;
    q_1.SQL.Text:='';
    //q_1.Active:=true;
   	q_4.Connection:=connection_main ;
    q_4.close;
    q_4.SQL.Text:='';
    //q_4.Active:=true;
		q_6.Connection:=connection_main ;
    q_6.close;
    q_6.SQL.Text:='';
    //q_6.Active:=true;
		q_7.Connection:=Connection_main;
    q_7.close;
    q_7.SQL.Text:='';
    //q_7.Active:=true;
		q_8.Connection:=Connection_main;
    q_8.close;
    q_8.SQL.Text:='';
    //q_8.Active:=true;
		q_9.Connection:=Connection_main;
    q_9.close;
    q_9.SQL.Text:='';
    //q_9.Active:=true;
		sql_processor.Connection:=Connection_main;
    abgleich_sat_anpassen:=CheckBox_loeschen.checked;
    abgleich_neugewinnt:=checkbox_neu.Checked;
end;
end;

procedure tform_abgleich.serververzeichnis_lesen(komplett:boolean);
var
pfad,dn:string;
datei:textfile;
begin
try
if komplett then
begin
   pfad:=trim(edit_pflocal.Text);
   dn:=pfad+'schreibtest.txt';
   assignfile(datei,dn );
	rewrite(datei);
	writeln(datei,'schreibtest, kann gel�scht werden');
	closefile(datei);
end;

   filelistbox.Directory:=pfad;
   filelistbox.update;
   filelistbox.ItemIndex:=filelistbox.count;
except
end;

application.ProcessMessages;
end;

procedure TForm_abgleich.Edit_pflocalChange(Sender: TObject);
begin
serververzeichnis_lesen(true);
end;

procedure TForm_abgleich.Button2Click(Sender: TObject);
begin
	vergleichen
end;

procedure TForm_abgleich.vergleichen;
var
liste:tlist;
//fliste:tstringlist;
daten:ptabvergleich;
i,e,i_firma:integer;
s,query,s_firma,datei,ma_join,ma_join_temp,datname_sql,pfad_sql:string;
fehler,vonanbeginn:boolean;
d:tdate;
begin
try

vonanbeginn:=false;
//auf den aktuellen stand bringen
      //refresh_all;
      b.berechtigungen_lesen;
      form_main.berechtigung_firma;
      //form_main.berechtigungen_einstellen;


  checkbox2.Checked:=false;
   memo_abgleich.Lines.Add('');
    memo_abgleich.Lines.Add('Die Anzahl der Datens�tze jeder Tabelle wird ');
    memo_abgleich.Lines.Add('auf dem Hauptsystem und dem Satelliten auf ');
    memo_abgleich.Lines.Add('gleiche Anzahl �berpr�ft ');
    memo_abgleich.Lines.Add(' ');
    memo_abgleich.Lines.Add('(Unterschiede k�nnen sich dadurch ergeben, dass mehrere Satelliten-Systeme  ');
    memo_abgleich.Lines.Add('zu unterschiedlichen Zeiten und �ber unterschiedliche Zeitr�ume synchronisieren.');
    memo_abgleich.Lines.Add('Relevant wird dies nur bei Abfragen �ber alle User');

  fehler:=false;
  liste:=tlist.Create;
  //fliste:=tstringlist.Create;
  if  checkbox_alle.Checked then firmen_nummern:=alle_firmennummern_berechtigt else firmen_nummern:=firmen_nummern_wahl;
  //if  checkbox_alle.Checked then firmen_nummern:=firmen_berechtigung else firmen_nummern:=firmen_nummern_wahl;  //aktuelle berechtigung
  //memo_abgleich.Lines.Add('Beginn Liste der zu �berpr�fenden Tabellen');
  datamodul.q_3.first;       //schreiben der einzelnen Abgleichdateinen
	while not datamodul.q_3.eof do begin
		 datei:=lowercase(datamodul.q_3['tab_name']);
     if b.b_hat_tab_ber(datei,0) then
     begin
         //######## bisher keine Firmenauswahl, sollte aber leicht sein
         //select dokumente.nummer ... from dokumente left join mitarbeiter on (dokumente.i_mitarbeiter=mitarbeiter.nummer) left join firma on (mitarbeiter.i_firma=firma.nummer) where firma.nummer in (2,3..)
         //m�sste so asusehen damit die Frimenauswahl Sinn macht

         //in der datei export_import muss stehen welcher Join, dann brauch ich noch die gesamten Feldnamen generisch erzeugt.(k�nnen sich ja mal �ndern)
         // oder mit describe tabname bzuw show columns form tabname die Spalten auslesen, wenn i_firma drin .... wenn i_mitarbeiter .... entsprechende Einschr�nkung sonst komplette Tabelle, Tabelle Firma ist gesondert zu bearbeiten
         ma_join:=format('left join mitarbeiter on (%s.i_mitarbeiter=mitarbeiter.nummer) left join firma on (mitarbeiter.i_firma=firma.nummer) where firma.nummer in (%s)',[datei,firmen_nummern]);

         if datei ='firma' then
         	query:=format('select count(*) from %s where  nummer in (%s) ',[datei,firmen_nummern])
         else
         if datei='mitarbeiter' then
            query:=format('select count(*) from %s where  i_firma in (%s) ',[datei,firmen_nummern])
         //rechnung i_firma  und alle mit i_firma=-1
         else
         if datei='rechnung' then
            query:=format('select count(*) from %s %s   ',[datei,ma_join,datei])
         else
          //texte i_master
         if datei='texte' then
         query:=format('select count(*) from %s where  i_master in (%s) ',[datei,firmen_nummern])

         //firma _ansprech  i_firma
         else
         if datei='firma_ansprech' then
         query:=format('select count(*) from %s where i_firma in (%s) ',[datei,firmen_nummern])
         //firma_sollist
         else
         if datei='firma_sollist' then
         query:=format('select count(*) from %s where i_firma in (%s) ',[datei,firmen_nummern])

         //untersuchung,
         else
         if datei='akt_untersuchung' then
         begin
            query:='select count(*) from akt_untersuchung left join  befunde on (befunde.nummer = akt_untersuchung.i_untersuchung) ';
         	query:=query+format(' left join mitarbeiter on (befunde.i_mitarbeiter=mitarbeiter.nummer) left join firma on (mitarbeiter.i_firma=firma.nummer) where firma.nummer in (%s) ',[firmen_nummern]);
         end
         else
         if datei='anamnese' then
         begin
            ma_join_temp:=stringreplace(ma_join,'i_mitarbeiter','nummer',[rfreplaceall]);
            query:=format('select count(*) from %s %s ',[datei,ma_join_temp]);
         end
         else
         if datei='untersuchung' then
            query:=format('select count(*) from %s %s ',[datei,ma_join])
         else
         if datei='impfung' then
            query:=format('select count(*) from %s %s ',[datei,ma_join])
         else
         if datei='labor' then
            query:=format('select count(*) from %s %s ',[datei,ma_join])
         else
         if datei='dokumente' then
            query:=format('select count(*) from %s %s ',[datei,ma_join])
         else
         if datei='besonderheiten' then
            query:=format('select count(*) from %s %s ',[datei,ma_join])
         else
         if datei='diagnosen' then
            query:=format('select count(*) from %s %s   ',[datei,ma_join])
         else
         if datei='befunde' then
            query:=format('select count(*) from %s %s  ',[datei,ma_join])
         else
         if datei='ambulanz' then
            query:=format('select count(*) from %s %s ',[datei,ma_join])
         else
         if datei='rechnung' then
            query:=format('select count(*) from %s %s  ',[datei,ma_join])  //and i_firma="-1"
         //impfung, labor, dokumente, besonderheiten,diagnosen,rechnung wenn i_fimra=-1, befunde

         else
         if datei='re_positionen' then
         begin
         	//query:='';
            //query:='select count(*) from re_positionen left join  rechnung on (rechnung.nummer = re_positionen.i_rechnung) ';
       		//query:=query+format(' left join mitarbeiter on (untersuchung.i_mitarbeiter=mitarbeiter.nummer) left join firma on (mitarbeiter.i_firma=firma.nummer) where firma.nummer in (%s) ',[firmen_nummern]);
            query:=format('select count(*) from %s %s  or (re_positionen.i_firma in (%s))  ',[datei,ma_join,firmen_nummern]);
         end
         else
         if datei='schluessel' then  query:=''
         else
          if datei='berechtigung_firma' then query:=''
         else
          if datei='berechtigung_objekt' then query:=''
         else
         query:=format('select count(*) from %s ',[datei]);  //alle anderen


        if (query<>'')  then
        begin
          new(daten);
          daten^.tabelle:=datei;
          daten^.main:=datamodul.q_3.findfield('main').asinteger;
          daten^.query:=query;
          liste.Add(daten);
          //tlist
       end;
       //memo_abgleich.Lines.Add(datei);
    end;
    datamodul.q_3.next;
	 end;   //while schleife
     ////////
   //end;

  //memo_abgleich.Lines.Add('Ende Liste der Tabellen');


  //exit; //20160214
  tabellenvergleich(  liste,0);

  liste_tabellen.clear;
  for i:=0 to liste.Count-1 do
  begin

     daten:=liste[i];
     if daten^.count_sat<>daten^.count_hs then
     begin
      if (daten^.count_sat=0) or (daten^.count_hs=0) then vonanbeginn:=true;

      s:=format('ungleiche Tabellenl�nge "%s"  Hauptsystem: %d, Satellitensystem: %d',[daten^.tabelle, daten^.count_hs, daten^.count_sat]);
     	memo_abgleich.lines.Add(s);
      fehler:=true;

      liste_tabellen.add(daten^.tabelle);

      if (daten^.count_sat>daten^.count_hs) and (daten^.main=1)then //   showmessage('Auf dem Satelitten ist die Tabelle '+daten.tabelle+ ' l�nger als auf dem Hauptsystem. F�hren Sie einen Abgleich ab dem 01.01.1970 durch');
      begin     // zu gef�hrlich

      	if Messagedlg('Auf dem Satelitten ist die Tabelle '+daten^.tabelle+ ' l�nger als auf dem Hauptsystem. F�hren Sie einen Abgleich ab dem 01.01.1970 durch'
                      +#10+'Ein Backup der Tabelle wird erstellt'
                      +#10+'Wenn die Meldung weiter erfolgt reparieren Sie die Tabellen mit den Datenbanktools. ',mtConfirmation, [mbyes,mbno],0)=mryes then
         begin
           zaehler:=2;
           pfad_sql:=mysql_d.sql_pfad(pfad_backupverzeichnis);
           datname_sql:=pfad_sql+daten^.tabelle+sql_dat+'.sql';
           query:=format('select * from %s into outfile "%s"',[daten^.tabelle,datname_sql]);
           mysql_d.sql_exe(query);
           //nur wenn eport_import =1
           query:= format('select main from export_import where tab_name="%s"', [daten^.tabelle ]);
           if mysql_d.Feldinhalt(query,0)='1'
           then
           begin
             query:=format('delete from %s', [daten^.tabelle ]);
             mysql_d.sql_exe(query);
           end;
         end;
      end;
     end;
  end;
  memo_abgleich.Lines.Add('');
  memo_abgleich.Lines.Add('Alle Tabellen wurden abgeglichen.');
  if fehler then
  begin
       inc(zaehler);
       if vonanbeginn then zaehler:=3;
  		 memo_abgleich.Lines.Add('Mindestens eine Tabelle auf dem Satellitensysten hat eine andere L�nge als auf dem Haptsystem');
       case zaehler of
       1: begin
            d:=strtodate(maskedit_dat2.Text);
            d:=d-60;
            maskedit_dat2.Text:=datetostr(d);
             memo_abgleich.Lines.Add(format(' F�hren Sie nochmals einen Abgleich ab dem %s durch !',[maskedit_dat2.Text]));

          end;
       2: begin
            d:=strtodate(maskedit_dat2.Text);
            d:=d-365;
            maskedit_dat2.Text:=datetostr(d);
             memo_abgleich.Lines.Add(format(' F�hren Sie nochmals einen Abgleich ab dem %s durch !',[maskedit_dat2.Text]));

          end;
       3: begin
            memo_abgleich.Lines.Add(' F�hren Sie nochmals einen Abgleich ab dem 01.01.1970 durch !');
       		maskedit_dat2.Text:='01.01.1970';
          end;
       4: begin
            showmessage('Es liegt ein Fehler beim Abgleich vor.');
          end;
       end;   
   end
  else
  begin
   memo_abgleich.Lines.Add('     Alle Tabellen auf dem Satellitensystem haben die gleiche L�nge ') ;
   memo_abgleich.Lines.Add('     wie auf dem Hauptsystem.') ;
   memo_abgleich.Lines.Add('     Der Abgleich war erfolgreich.')

 end;
finally
 {while liste.count>0 do begin
	daten:= liste.items[liste.count-1];
	liste.delete(liste.count-1);
	dispose(daten);
 end;}

  for i := 0 to (liste.Count - 1) do
   begin
     daten := liste.Items[i];
     Dispose(daten);
   end;


  {for i:=0 to liste.Count-1 do
  begin
     daten:=liste[i];
     liste.Delete(i-1);
     dispose(liste[i]);
  end; }
  liste.Free;
  //fliste.free;
end;
end;

procedure TForm_abgleich.tabellenvergleich(var liste: tlist; modus:integer);
var
	daten:ptabvergleich;
   //hmysql_d:tmysql_d;
   i:integer;
   query,h_lastupdate, s_lastupdate,s:string;
begin
   try
   hmysql_d.connect( edit_iphaupt.Text,db_protocol,db_port,edit_dbhaupt.text,db_login,db_passwort);
   except
   	showmessage('keine Verbindung zur Hauptdatenbank');
      exit;
   end;

   // hat die Hauptdatenbank die richtige Version

   query:='select i1 from haupt_tabelle';
   h_lastupdate:=(hmysql_d.Feldinhalt(query,0));

   query:='select i1 from haupt_tabelle';
   s_lastupdate:=(mysql_d.Feldinhalt(query,0));

   if s_lastupdate<>h_lastupdate then
   begin
   messagedlg( 'Die Hautpdatenbank und Satellitendatenbank sind nicht auf dem selben Aktualisierungsstand,'+chr(13)+' Arbene.exe auf dem Haupt- und dem Satelliten-System updaten. ',mtInformation	, [mbok, mbhelp],69);
   //showmessage ('Die Hautpdatenbank und Satellitendatenbank sind nicht auf dem selben Aktualisierungsstand,'+chr(13)+' Arbene.exe auf dem Haupt- und dem Satelliten-System updaten. ');
   exit;
   end;
   //20160214
  for i:=0 to liste.Count-1 do
  begin

     daten:=liste[i];
     query:=daten^.query;
     s:=hmysql_d.Feldinhalt(query,0);
     daten^.count_hs:=strtoint(s);
     s:=mysql_d.Feldinhalt(query,0);
     daten^.count_sat:=strtoint(s);
  end;
end;

procedure TForm_abgleich.Timer_pmTimer(Sender: TObject);
begin
  application.ProcessMessages;
end;


procedure TForm_abgleich.liste_tabellen_schreiben_all ;
var
query,tabname:string;
sperrlist:tstringlist;
begin
  sperrlist:=tstringlist.Create;

  sperrlist.Add('befunde');
  sperrlist.Add('akt_untersuchung');
  sperrlist.Add('ambulanz');
  sperrlist.Add('anamnese');
  sperrlist.Add('arbeitsunfall');
  //sperrlist.Add('a_gefaehrdung');
  //sperrlist.Add('a_projekte');
  //sperrlist.Add('a_platz');
  sperrlist.Add('befunde');
  sperrlist.Add('besonderheiten');
  sperrlist.Add('diagnosen');
  sperrlist.Add('dokumente');
  sperrlist.Add('email');
  sperrlist.Add('firma_sollist');
  sperrlist.Add('firma_zeiten_soll');
  sperrlist.Add('f_gruppe');
  sperrlist.Add('impfung');
  sperrlist.Add('kontakte');
  sperrlist.Add('labor');
  sperrlist.Add('lieferanten');
  sperrlist.Add('materialstamm');
  sperrlist.Add('rechnung');
  sperrlist.Add('re_positionen');
  sperrlist.Add('termine');
  sperrlist.Add('texte');
  sperrlist.Add('textbausteine');
  sperrlist.Add('vorlagen');






  query:='select tab_name from export_import where main>=0';
  datamodul.sql(false,datamodul.q_12,query,'');
  liste_tabellen.Clear;
  datamodul.q_12.first;
  while not datamodul.q_12.Eof do
  begin
    tabname:=datamodul.q_12.findfield('tab_name').asstring;
    if sperrlist.IndexOf (tabname)=-1 then liste_tabellen.Add(tabname);
    datamodul.q_12.Next;
  end;

  sperrlist.Clear ;
  sperrlist.Free;
end;



procedure TForm_abgleich.liste_tabellen_schreiben_auswahl ;
begin
  liste_tabellen.Clear;
  if checkbox_personal.Checked then liste_tabellen.Add('mitarbeiter');
  if checkbox_bescheinigung.Checked then liste_tabellen.Add('untersuchung');
end;

procedure TForm_abgleich.CheckBox_personalClick(Sender: TObject);
begin
  groupbox_auswahl.Visible :=checkbox_personal.Checked;
  checkbox_personaldaten.Checked:=false;
  checkbox_bescheinigung.Checked:=false;
end;

procedure TForm_abgleich.CheckBox_userClick(Sender: TObject);
begin
 dbgrid_user.Visible :=checkbox_user.Checked;

end;

procedure TForm_abgleich.BitBtn_abbruchClick(Sender: TObject);
begin
    if Messagedlg('Soll der Abgleich abgebrochen werden?',mtConfirmation, [mbyes,mbno],0)=mryes then
    b_abbruch:=true;
    fehler:=true;
end;

function tform_abgleich.abbruchcheck():boolean;
begin
  result:=false;
  if b_abbruch then
  begin
    memo_abgleich.Lines.add ('');
    memo_abgleich.Lines.add ('Der Abgleich wurde abgebrochen');
    memo_abgleich.Lines.add (' ');

    result:=true;
  end;
end;

procedure TForm_abgleich.BitBtn1Click(Sender: TObject);
var
pfad:string;
position:integer;
begin
  form_pfadwahl:=tform_pfadwahl.create(self);
  if form_pfadwahl.showmodal =mrok then
  begin
       pfad:=form_pfadwahl.DirectoryListBox.Directory;
       if copy(pfad,length(pfad),1)<>'\'then pfad:=pfad+'\';

       edit_exportpfad.Text:=pfad;
  end;
  form_pfadwahl.Release;

end;


end.
