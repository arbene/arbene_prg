library arb_inst_first;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: ShareMem muss sich in der
  ersten Unit der unit-Klausel der Bibliothek und des Projekts befinden (Projekt-
  Quelltext anzeigen), falls die DLL Prozeduren oder Funktionen exportiert, die
  Strings als Parameter oder Funktionsergebnisse �bergeben. Das gilt f�r alle
  Strings, die von oder an die DLL �bergeben werden -- sogar f�r diejenigen, die
  sich in Records und Klassen befinden. Sharemem ist die Schnittstellen-Unit zur
  Verwaltungs-DLL f�r gemeinsame Speicherzugriffe, BORLNDMM.DLL.
  Um die Verwendung von BORLNDMM.DLL zu vermeiden, k�nnen Sie String-
  Informationen als PChar- oder ShortString-Parameter �bergeben. }
  


uses
  SysUtils,
  Classes,
  GIPlugins,
  CUIApi,
  dialogs,
  forms,
  arb_inst_first_form_main in 'arb_inst_first_form_main.pas' {Form_main};

type dword=longword;
var
  Engine: TPluginStartupInfo;
  trigger: boolean;
  i:integer;
function EventsHandler(Msg: dword; MsgName: PChar;
  InParam, OutParam: dword): dword; stdcall;
var a:DWORD;
    b: string;
begin
//beep;

    a:=Engine.CreateParamsBlock;
    Engine.AddParamStr(a,'arb_inst_first');
      if (MsgName=SEV_CUIAFTERCREATEWINDOW) and (not trigger) then begin
        trigger:=true;
        Engine.SendMsgName(Engine.HPlugin,SCM_CUISHOWWINDOW,a,0,false,false);
    end;
    if trigger then begin
        Engine.SetParamCount(a,1);
        //b:='SetProp("Sample.List.Items[1000].Value","'+MsgName+'")';
        //Engine.SetParamStr(a,0,PChar(b));
        Engine.SendMsgName(Engine.HPlugin,SCM_CUIDOACTION,a,0,false,false);
    end;


  if MsgName='EV_CALCFUNCTION' then
    if Engine.GetParamStr(InParam,1)='MyFunction' then
    begin
      if Engine.GetParamStr(InParam,4)='Close' then
      begin
        a:=Engine.CreateParamsBlock();
        Engine.AddParamStr(a,'Sample');
        Engine.SendMsgName(Engine.HPlugin,'CM_CUICLOSEWINDOW',a,0,false,false);
        Engine.DestroyParamsBlock(a);
      end;
      if Engine.GetParamStr(InParam,4)='Abort' then
        Engine.SendMsgName(Engine.HPlugin,'CM_ABORTINSTALL',0,0,false,false);
    end;
  Case Msg of
    EV_BEFOREINSTALL :
    begin
      Application.CreateForm(TForm_main, Form_main);

      Form_main.ShowMODAL;
    end;
    EV_LOAD:begin
        trigger:=false;
        a:=Engine.CreateParamsBlock();
        Engine.AddParamStr(a,'MyFunction');
        Engine.AddParamInt(a,-1);
        Engine.SendMsg(Engine.HPlugin,CM_REGISTERFUNCTION,a,0,false,false);
        Engine.DestroyParamsBlock(a);
    end;
    EV_AFTERINSTALL:
    begin

    end;
    EV_START: begin
    end;
    EV_UNLOAD: begin
    end;
  end;
  Result:=0;
end;

procedure GetPluginInfo(var PluginInfo: TPluginInfo); stdcall;
const
  {CPluginDescription = 'Ghost Installer plug-in sample for Delphi.';
  CPluginID          = 'Sample';
  CPluginName        = 'Sample';
  CPluginAuthor      = 'Ethalone Solutions, Inc.';
  CPluginCopyright   = 'Copyright � 1999-2004 Ethalone Solutions, Inc. and its licensors. All rights reserved.';}
  CPluginDescription = 'abene_install_first';
	CPluginID          = 'arb_inst_first';
	CPluginName        = 'arbene_install_first';
	CPluginAuthor      = 'klawitter';
  CPluginCopyright   = 'Copyright � 2016 Arbene';
begin
 //beep;
 //beep;
  with PluginInfo do begin
    if InfoVersion >= VER_PLUGININFO_100 then begin
      RequiredGIVersion:=VER_GI_400;
      Flags:=0;
      Move(CPluginID,PluginID,length(CPluginID));
      Move(CPluginName,PluginName,length(CPluginName));
      PluginVersion:=$10000;
      Move(CPluginAuthor,PluginAuthor,length(CPluginAuthor));
      Move(CPluginDescription,PluginDescription,length(CPluginDescription));
      Move(CPluginCopyright,PluginCopyright,length(CPluginCopyright));
      ProcessMsg:=@EventsHandler;
    end;
  end;
end;

function SetStartupInfo (StartupInfo: PPluginStartupInfo):longint; stdcall;
begin
  Engine.InfoVersion:=(Startupinfo).InfoVersion;
  Engine.GIVersion:=(StartupInfo).GIVersion;
  Engine.HPlugin:=(StartupInfo).HPlugin;
  Engine.RegisterMsg:=(StartupInfo).RegisterMsg;
  Engine.SendMsg:=(StartupInfo).SendMsg;
  Engine.SendMsgName:=(StartupInfo).SendMsgName;
  Engine.SendPrivateMsg:=(StartupInfo).SendPrivateMsg;
  Engine.SendPrivateMsgName:=(StartupInfo).SendPrivateMsgName;
  Engine.CreateParamsBlock:=(StartupInfo).CreateParamsBlock;
  Engine.DestroyParamsBlock:=(StartupInfo).DestroyParamsBlock;
  Engine.GetParamCount:=(StartupInfo).GetParamCount;
  Engine.SetParamCount:=(StartupInfo).SetParamCount;
  Engine.GetParamType:=(StartupInfo).GetParamType;
  Engine.GetParamStr:=(StartupInfo).GetParamStr;
  Engine.GetParamInt:=(StartupInfo).GetParamInt;
  Engine.GetParamBool:=(StartupInfo).GetParamBool;
  Engine.SetParamStr:=(StartupInfo).SetParamStr;
  Engine.SetParamInt:=(StartupInfo).SetParamInt;
  Engine.SetParamBool:=(StartupInfo).SetParamBool;
  Engine.AddParamStr:=(StartupInfo).AddParamStr;
  Engine.AddParamInt:=(StartupInfo).AddParamInt;
  Engine.AddParamBool:=(StartupInfo).AddParamBool;
  Result:=0;
end;




exports
  GetPluginInfo,
  SetStartupInfo;
begin
end.
