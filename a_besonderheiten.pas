unit a_besonderheiten;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, dbgridEXT,a_data,
  DBCtrls, Mask, Menus, ComCtrls, ToolWin, TreeView_ext,ZDataset,Variants;

type
	TForm_besonderheiten = class(TForm)
	  GroupBox_besonderh: TGroupBox;
    ToolBar: TToolBar;
    ToolButton_new: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
	  ToolButton4: TToolButton;
    ToolButton_del: TToolButton;
    Panel_bottom: TPanel;
    BitBtn_exit_ok: TBitBtn;
    BitBtn_exit_abort: TBitBtn;
    Tree: TTreeView_ext;
    ToolButton1: TToolButton;
    ToolButton_save: TToolButton;
    Panel1: TPanel;
    Label_suchen: TLabel;
    SpeedButton1: TSpeedButton;
    Edit_such: TEdit;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    procedure ToolButton_newClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ini(q: tzquery; f: string);
    procedure ToolButton1Click(Sender: TObject);
    procedure TreeDblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edit_suchEnter(Sender: TObject);
    procedure Edit_suchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TreeKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
	private
	  { Private-Deklarationen}
	public
	  { Public-Deklarationen}
	  multiselect:boolean;
      tquery:tzquery;
  end;

var
  Form_besonderheiten: TForm_besonderheiten;

implementation

uses a_Line_edit, a_main;

{$R *.DFM}

procedure TForm_besonderheiten.ToolButton_newClick(Sender: TObject);
var
nr:int64;
b:boolean;
rf,p:integer;
begin
try
	 form_line_edit:=tform_line_edit.create(self);
	 form_line_edit.edit.text:='';
	{ if  activecontrol=dbgridext_main then form_line_edit.CheckBox.Visible:=true
	  else form_line_edit.CheckBox.Visible:=false;}
      if tquery=datamodul.q_firma_filter_vorlage then  form_line_edit.CheckBox.visible:=false;

	  if form_line_edit.showmodal=mrok then
	  begin
       b:=form_line_edit.CheckBox.Checked;
       rf:=tquery.findfield('reihenfolge').asinteger;
       p:=tquery.findfield('parent').asinteger;

       if tquery=datamodul.q_besonderheitenListe_tree then
       nr:=neuer_datensatz(tquery,['nummer','besonderheit','parent','reihenfolge','b_beurteilung'],[null,form_line_edit.edit.text,p,rf,b]);           //ohne tree    statt rf -1

       if tquery=datamodul.q_firma_filter_vorlage then
       nr:=neuer_datensatz(tquery,['nummer','name','parent','reihenfolge'],[null,form_line_edit.edit.text,p,rf]);

       tquery.post;
       nr:=tquery.findfield('nummer').asinteger;
       tree.liste_lesen;
       tree.reihenfolge_neu;
       tree.suche_nummer(nr);

		  //tree.neu(form_line_edit.edit.text,1);
	  end;

finally
	form_line_edit.release;
end;
end;



procedure TForm_besonderheiten.ToolButton_editClick(Sender: TObject);

begin
form_line_edit:=tform_line_edit.create(self);
if tquery=datamodul.q_firma_filter_vorlage then  form_line_edit.CheckBox.visible:=false;
try
{if  activecontrol=dbgridext_main then form_line_edit.CheckBox.Visible:=true
 else form_line_edit.CheckBox.Visible:=false;}
try
if tquery=datamodul.q_besonderheitenListe_tree then
begin
		 form_line_edit.edit.text:=tquery.findfield('besonderheit').asstring;
		 form_line_edit.CheckBox.Visible:=true;
		 form_line_edit.CheckBox.Checked:=(tquery.findfield('b_beurteilung').asinteger=-1);
		 if form_line_edit.showmodal=mrok then
		 begin
		 tquery.edit;
		 tquery.findfield('besonderheit').asstring:=form_line_edit.edit.text;
		 if form_line_edit.CheckBox.Checked then tquery.findfield('b_beurteilung').asinteger:=-1 else tquery.findfield('b_beurteilung').asinteger:=0;
		 //datamodul.q_besonderheitenListe_tree.post;
		 tree.tabelle_post;
		 end;
end;

if tquery=datamodul.q_firma_filter_vorlage then
begin
		 form_line_edit.edit.text:=tquery.findfield('name').asstring;
		 form_line_edit.CheckBox.Visible:=false;
		 if form_line_edit.showmodal=mrok then
		 begin
		 tquery.edit;
		 tquery.findfield('name').asstring:=form_line_edit.edit.text;
		 tree.tabelle_post;
		 end;
end;

except
	showmessage('Zuerst Datensatz anlegen!');
end;
finally
form_line_edit.release;
end;
end;

procedure TForm_besonderheiten.ToolButton_delClick(Sender: TObject);
var
query:string;
n:string;
begin
n:=tquery.findfield('nummer').asstring;
if tquery=datamodul.q_besonderheitenListe_tree then
begin
	query:=format('select nummer from besonderheiten where i_besonderheit=%s and storno=0',[n]);
end;

if tquery=datamodul.q_firma_filter_vorlage then
begin
	query:=format('select nummer from firma_filter where i_firma_filter_vorlage=%s and storno=0',[n]);
end;

if mysql_d.sql_exe_rowsvorhanden(query) then
begin
	showmessage ('Es sind abh�ngige Datens�tze vorhanden, L�schen ist nicht m�glich');
    exit;
end;
    tree.loeschen;
end;

procedure TForm_besonderheiten.ToolButton11Click(Sender: TObject);
begin
modalresult:=mrabort;
end;

procedure TForm_besonderheiten.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
	//datamodul.sql_new(datamodul.q_besonderheitenListe_tree,'select * from besonderheitenliste', 'reihenfolge');
   //tquery:=datamodul.q_besonderheitenListe_tree;
	//tree.liste_lesen;
end;

procedure TForm_besonderheiten.ini(q: tzquery; f: string);
begin
tquery:=q;
tree.mysqlquery:=q;
tree.mysql_feld:=f;
tree.liste_lesen;
end;

procedure TForm_besonderheiten.ToolButton1Click(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_besonderheiten.TreeDblClick(Sender: TObject);
begin
if not multiselect then exit;
if tree.selected.stateindex=-1 then tree.markieren(false,3)
else tree.markieren(false,-1);
end;

procedure TForm_besonderheiten.FormDestroy(Sender: TObject);
begin
tree.liste_loeschen;
end;

procedure TForm_besonderheiten.SpeedButton1Click(Sender: TObject);
begin
tree.suche_datensatz(edit_such.text);
end;

procedure TForm_besonderheiten.Edit_suchEnter(Sender: TObject);
begin
if tree.items.count=0 then exit;
tree.Selected:=tree.Items[0];
tree.change(tree.selected);
end;

procedure TForm_besonderheiten.Edit_suchKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if key<>vk_return then exit;
     tree.suche_datensatz(edit_such.text);
end;

procedure TForm_besonderheiten.TreeKeyPress(Sender: TObject;
  var Key: Char);
begin
     edit_such.Text:=key;
     edit_such.SetFocus;
     edit_such.SelStart:=99;
end;

procedure TForm_besonderheiten.SpeedButton7Click(Sender: TObject);
begin
tree.FullCollapse;
end;

procedure TForm_besonderheiten.SpeedButton5Click(Sender: TObject);
begin
tree.FullExpand ;
end;

end.
