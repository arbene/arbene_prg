unit soundex;
{-------------------------------------------------------------------}
{   Soundex Component for Delphi                                    }
{                                                                   }
{   Copyright 1995 JaNUS Software Laboratories                      }
{                                                                   }
{                  Keith Latham                                     }
{                  73 Morgan St.                                    }
{                  Glen Huntly 3163                                 }
{                  Victoria Australia                               }
{                                                                   }
{                  EMail: 100026.2515@compuserve.com                }
{                     or: keith@acslink.net.au                      }
{-------------------------------------------------------------------}
{ sample use                                                        }
{                                                                   }
{       procedure SoundexDemo;                                      }
{       var x   : tSoundex;                                         }
{           s,t : string;                                           }
{       begin                                                       }
{       t := 'sometoken';                                           }
{       x := tSoundex.Create(5);                                    }
{       x.keyword := t;                          (line 1)           }
{       writeln 'Token = ' + x.keyword                              }
{             + ' Code = ' + x.soundex;                             }
{       end;                                                        }
{                                                                   }
{ alternatively line 1 can be replaced with                         }
{       s := x.encode(t);                                           }
{ or                                                                }
{       x.soundex := t;                                             }
{                                                                   }
{-------------------------------------------------------------------}
interface uses classes;
type
  tSoundex = class(tComponent)
    private
      codemax     : word;
      fixed       : boolean;
      token       : string;
      sdex        : string;
      digraphlist : string;
      procedure   soundexer (t  :string);
      procedure   setdigraph(dig:string);
    public
      constructor Create(aOwner:tComponent); override;
      destructor  Destroy;
      function    Encode(t:string) : string;
    protected
    published
      property digraph  : string  read digraphlist write setdigraph;
      property codelen  : word    read codemax     write codemax;
      property fixedlen : boolean read fixed       write fixed;
      property keyword  : string  read token       write soundexer;
      property soundex  : string  read sdex        write soundexer;
    end;
{-------------------------------------------------------------------}
//procedure register;
{-------------------------------------------------------------------}
implementation uses sysutils;

{procedure register;
begin
  RegisterComponents('JSLTools',[tSoundex]);
end;}

{-------------------------------------------------------------------}


constructor tSoundex.Create(aOwner:tComponent);
begin
  inherited create(aOwner);
  codelen  := 4;                 {default output code length}
  fixedlen := true;              {default code always exactly codelen}
  digraph  := '/GH=F/LD=D/PH=F'; {default digraph list      }
end;

destructor tSoundex.Destroy;
begin
  inherited destroy;
end;

function tSoundex.Encode(t:string) : string;
begin
  soundex := t;
  Encode  := soundex;
end;

procedure tSoundex.SetDigraph(dig:string);
begin
  digraphlist := dig;
end;

procedure tSoundex.Soundexer(t:string);
var  temp : string;              {temporary adjusted target token}
        i : integer;             {index counter}
{This function inspects a two character string and encodes digraphs }
      function checkdigraph(pair:string):string;
      var   index : integer;
      begin
        {dig string looks like: /aa=b/cc=d/ee=f}
        index := pos('/'+uppercase(pair),digraphlist);
        if index = 0
           then checkdigraph := pair
           else checkdigraph := digraphlist[index+4];
      end;
{This procedure checks for special cases for the first two characters}
      procedure checkfirst;
      begin
        i := 2;
        temp := checkdigraph(copy(token,1,2));
        if length(temp) = 2         {i.e. it wasn't a digraph}
           then temp[0] := char(1)  {just keep the first char}
           else i       := 3;       {skip second char for encode}
      end;
{This procedure checks for special cases for the last two characters}
      procedure checklast;
      var twochar : string[2];
      begin
        twochar := copy(token,length(token)-1,2);
        if length(token) > i+2
           then temp := temp + checkdigraph(twochar)
           else temp := temp + twochar;
      end;
 {This function returns the soundex code for a given character}
     function encodechar(aChar:char):char;
      begin
        case upCase(aChar) of
             'A','E','H','I','O','U','W','Y' : encodechar := '0';
             'B','F','P','V'                 : encodechar := '1';
             'C','G','J','K','Q','S','X','Z' : encodechar := '2';
             'D','T'                         : encodechar := '3';
             'L'                             : encodechar := '4';
             'M','N'                         : encodechar := '5';
             'R'                             : encodechar := '6';
        end;
      end;
 {This procedure sets up the temp version of the target token]}
      procedure InitializeTemp;
      begin;
        CheckFirst;     {checks for leading digraph; inits temp and i}
        temp := temp + copy(token,i,length(token)-(i+1));
        CheckLast;      {checks for trailing digraph; completes temp}
      end;
{--------------------------------------------------------------------------}
{Soundexer Function Main Code                                                }
{--------------------------------------------------------------------------}
begin
  Token := t;
  InitializeTemp;  {initialzes temp string and starting point}
{convert temp string to soundex string}
  for i := 2 to length(temp)
      do temp[i] := encodechar(temp[i]);
{remove doublecodes and vowels; truncate at codemax}
  SDex := copy(temp,1,1); {first character is always kept}
  i    := 2;
  while (length(SDex) <  codemax)
    and (i            <= length(temp)) do
       begin
         if  (temp[i] <> '0')            {reject vowels}
         and (temp[i] <> temp[i-1])      {reject double letters}
             then SDex := SDex + temp[i];
         inc(i);
       end;
  if fixedlen
     then begin
            SDex    := SDex + '000000000000000000000000000000000000';
            SDex[0] := char(codemax);
          end;
end;
{-----------------------------------------------------------------------}
end.
