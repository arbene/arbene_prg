unit editor; //kapselt die Funktionen f�r das Men�band

interface
uses
SysUtils, Windows, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Menus, ComCtrls, ClipBrd,
	ToolWin ,RichEdit, ShellAPI;

type
	Trtfedit = class(tobject)
	private
	  { Private-Deklarationen }
    //f_CopyButton:ttoolbutton;
    toolbar:ttoolbar;
    f_fontname: tcombobox;
    f_fontsize:tedit;
    f_openbutton, f_savebutton, f_undobutton,f_copybutton, f_pastebutton, f_boldbutton,f_italicbutton,f_underlinebutton ,f_toolbutton_color,f_leftalign ,f_centeralign ,f_rightalign,f_bulletsbutton:ttoolbutton;
    //fett,kursiv,unterstrichen, bullet, farbe, links, rechts, mitte, laden, speichern, undo, toclip, fromclip:ttoolbutton;
	public
	  { Public-Deklarationen }
	  fupdating: boolean;
	  es: teditstream;
    akt_editor:trichedit;
    formular:tform;
    
	  function CurrText: TTextAttributes;

	  procedure UpdateCursorPos;
	  function textstring : string;
	  procedure textspeichern(datname:string);
	  procedure textladen(const datname: string);
	  procedure FileSaveAs;
	  procedure fileopen;
	  procedure FilePrint(Sender: TObject);
	  procedure GetFontNames;
    procedure EditUndo(Sender: TObject);
	  procedure EditCopy(Sender: TObject);
    procedure Editpaste(Sender: TObject);
    procedure RichEditChange(Sender: TObject);
    procedure SelectionChange(Sender: TObject);

	  constructor create(ed:trichedit;bar:ttoolbar);

	procedure SetEditRect;
	end;

  
	function editstreamcallbacksave(dwcookie: longint; pbbuff: pchar;
											cb: longint; var pcb: longint): longint; stdcall;

	function editstreamcallbackload(dwcookie: longint; pbbuff: pbyte;
											cb: longint; var pcb: longint): longint; stdcall;

	function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
	FontType: Integer; Data: Pointer): Integer; stdcall;


var
	text: string;
implementation

uses a_main;

const
	gutterwid =6;
	readerror= $0001;
	writeerror= $0002;
	noerror= $0000;
	s_fehler='\'+#$A+'\'+#$A;
	s_ersatz= #$D+#$A+'\par ';

function editstreamcallbacksave(dwcookie: longint; pbbuff: pchar;
					cb: longint; var pcb: longint): longint; stdcall;
var
	zeile: string;
	position:integer;
	//buffer: pchar;
begin
try

	//text:=string(dwcookie);
	//text:=string(dwcookie)+strpas(pbbuff);

	//cb:=strend(pbbuff)-pbbuff;

	//buffer:=stralloc(cb);
	//dwcookie:=longint(@buffer);
	//move(pbbuff^,buffer^,cb);
	pcb:=0;
	result:=noerror;
	zeile:=string(pbbuff);
	zeile:=copy(zeile,1,cb);
	position:=pos(s_fehler, zeile);    //interner Fehler von rtfedit
	while position>0 do
	begin
		delete(zeile,position,length(s_fehler));
		insert(s_ersatz,zeile,position);
		position:=pos(s_fehler, zeile);
	end;
	pcb:=cb;//length(zeile);
	text:=text+zeile;
	//strdispose(buffer);
except
	showmessage('Fehler bei streamsave');

	result:=error;
end;
end;


function editstreamcallbackload(dwcookie: longint; pbbuff: pbyte;
											cb: longint; var pcb: longint): longint; stdcall;
var
	buffer: pchar;
begin
	result:=noerror;
	try
	pcb:=length(text);
	if pcb>0 then
	begin
		if pcb<cb then
		  begin
			 buffer:=pchar(text);
			 move(buffer^,pbbuff^,pcb);
			end
		else
			begin
			  buffer:=pchar(copy(text,1,cb));
			  //buffer[cb]:=#0;
			  //if buffer[cb-1]=#13 then buffer[cb-1]:=#0;
			  text:=copy(text,cb+1,length(text));
			  move(buffer^,pbbuff^,cb);
			  pcb:=cb;
		end;
			//strdispose(buffer);
		END;
	except
		showmessage('Fehler bei streamload');
		result:=readerror;
	end;
end;


function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
	FontType: Integer; Data: Pointer): Integer; stdcall;
begin
	TStrings(Data).Add(LogFont.lfFaceName);
	Result := 1;
end;



constructor trtfedit.create(ed:trichedit;bar:ttoolbar);
var
i:integer;
begin
	inherited create;

  akt_editor:=ed;
  //f_copybutton:=cb;
  toolbar:=bar;



  for i:=0 to bar.ControlCount-1 do
  begin
    if lowercase( bar.Controls[i].Name)='fontname' then f_fontname:=tcombobox(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='fontsize' then f_fontsize:=tedit(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='openbutton' then f_openbutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='savebutton' then f_savebutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='undobutton' then f_undobutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='copybutton' then f_copybutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='pastebutton' then f_pastebutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='boldbutton' then f_boldbutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='italicbutton' then f_italicbutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='underlinebutton' then f_underlinebutton:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='toolbutton_color' then f_toolbutton_color:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='leftalign' then f_leftalign:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='centeralign' then f_centeralign:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='rightalign' then f_rightalign:=ttoolbutton(bar.Controls[i]);
    if lowercase(bar.Controls[i].Name)='bulletsbutton' then f_bulletsbutton:=ttoolbutton(bar.Controls[i]);
    
  end;

  getfontnames;
end;



function trtfedit.CurrText: TTextAttributes;
begin
	if akt_editor.SelLength > 0 then Result := akt_editor.SelAttributes
	else Result := akt_editor.DefAttributes;
end;


procedure trtfedit.UpdateCursorPos;
var
	CharPos: TPoint;
begin
	CharPos.Y := SendMessage(akt_editor.Handle, EM_EXLINEFROMCHAR, 0,
	  akt_editor.SelStart);
	CharPos.X := (akt_editor.SelStart -
	  SendMessage(akt_editor.Handle, EM_LINEINDEX, CharPos.Y, 0));
	Inc(CharPos.Y);
	Inc(CharPos.X);
	//StatusBar.Panels[0].Text := Format('Line: %3d   Col: %3d', [CharPos.Y, CharPos.X]);

	// update the status of the cut and copy command


	//form_main.CopyButton.Enabled := akt_editor.SelLength > 0;
  //f_CopyButton.Enabled := akt_editor.SelLength > 0;
  f_copybutton.Enabled:=akt_editor.SelLength > 0;

end;


function trtfedit.textstring :string;
var
i: integer;
buffer: pchar;
begin
	try
	i:=sendmessage(akt_editor.handle,wm_gettextlength,0,0);
	buffer:=stralloc(i+1);
	sendmessage(akt_editor.handle,wm_gettext,i+1,longint(buffer));
	result:=strpas(buffer);
finally
 strdispose(buffer);
end;
end;


procedure trtfedit.textspeichern(datname: string);
//var
//datei: textfile;
//temp: longint;
begin

	//screen.Cursor:=crHourglass;
	//text:='';
	akt_editor.Lines.SaveToFile(datname);

	{es.dwError:=0;
	es.dwCookie:=0;
	es.pfnCallback:=@editstreamcallbacksave;

	SendMessage(akt_editor.Handle, em_streamout,sf_rtf,longint(@es));

	assignfile(datei, datname);
	rewrite(datei);
	write(datei,text);
	closefile(datei);}
	akt_editor.modified:=false;
	//screen.Cursor:=crdefault;;
end;

procedure trtfedit.textladen(const datname: string);
begin
  try
	akt_editor.Lines.LoadFromFile(datname);
  except
  //akt_editor.lines.Add('');
  end;
	akt_editor.Modified:=false;

end;

procedure trtfedit.FileSaveAs;
var
endung,datname: string;

begin
	if form_main.SaveDialog.Execute then
	begin

	  datname:= form_main.SaveDialog.FileName;
	  endung:=form_main.dateiendung(datname);
	  if endung='' then datname:=datname+'.txt';
	  if FileExists(datname) then
		 if MessageDlg(Format('Soll %s �berschrieben werden?', [datname]),
			mtConfirmation, mbYesNoCancel, 0) <> idYes then Exit;
	  akt_editor.Lines.SaveToFile(datname);
end;
end;

procedure trtfedit.FilePrint(Sender: TObject);
begin
	
end;


procedure trtfedit.GetFontNames;
var
	DC: HDC;
  index: integer;
begin
	DC := GetDC(0);
  EnumFonts(DC, nil, @EnumFontsProc, Pointer(f_FontName.Items));
	ReleaseDC(0, DC);

	f_FontName.Sorted := True;


  index:=f_FontName.Items.IndexOf('Arial');
  if index<0 then index:=0;
	f_FontName.ItemIndex:=index;
	


end;

procedure trtfedit.fileopen;
begin
	form_main.OpenDialog.filter:='RTF |*.rtf';
if form_main.opendialog.execute and ( Messagedlg('Sollen der aktuelle Memotext �berschrieben werden?',mtConfirmation, mbOkCancel	,0)=mrOK ) then
	begin
		//akt_editor.Clear ;
		//akt_editor.lines.Clear;
		//akt_editor.plaintext:=false;
		akt_editor.lines.LoadFromFile(form_main.opendialog.FileName);
	//akt_editor.PlainText:=false;
	end;
end;



procedure trtfedit.SetEditRect;
var
	R: TRect;
begin
	with akt_editor do
	begin
	  R := Rect(GutterWid, 0, ClientWidth-GutterWid, ClientHeight);
	  SendMessage(Handle, EM_SETRECT, 0, Longint(@R));
	end;
end;


procedure trtfedit.EditUndo(Sender: TObject);
begin
	with akt_editor do
    if HandleAllocated then SendMessage(Handle, EM_UNDO, 0, 0);
end;


procedure trtfedit.EditCopy(Sender: TObject);
begin
  akt_editor.CopyToClipboard;
end;

procedure trtfedit.EditPaste(Sender: TObject);
begin
  akt_editor.PasteFromClipboard;
end;

procedure trtfedit.RichEditChange(Sender: TObject);
begin
  f_UndoButton.Enabled := SendMessage(akt_editor.Handle, EM_CANUNDO, 0, 0) <> 0;
end;


procedure Trtfedit.SelectionChange(Sender: TObject);
begin
  fupdating:=true;


    f_fontsize.Enabled:=akt_editor.SelLength>0;
    f_fontname.Enabled:=akt_editor.SelLength>0;
    f_toolbutton_color.Enabled:=akt_editor.SelLength>0;

  with akt_editor.Paragraph do
  try
    f_FontSize.Text := IntToStr(akt_Editor.SelAttributes.Size);
    f_FontName.Text := akt_Editor.SelAttributes.Name;

    f_BoldButton.Down := fsBold in akt_Editor.SelAttributes.Style;
    f_ItalicButton.Down := fsItalic in akt_Editor.SelAttributes.Style;
    f_UnderlineButton.Down := fsUnderline in akt_Editor.SelAttributes.Style;
    f_BulletsButton.Down := Boolean(Numbering);

    case Ord(Alignment) of
      0: f_LeftAlign.Down := True;
      1: f_RightAlign.Down := True;
      2: f_CenterAlign.Down := True;
    end;
    UpdateCursorPos;
  finally
    FUpdating := False;
  end;
end;


end.
