unit a_abgleich_ausnahme;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TForm_imp_ausnahmen = class(TForm)
    Panel1: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Panel2: TPanel;
    Label4: TLabel;
    BitBtn_ok: TBitBtn;
    CheckBox_neu: TCheckBox;
    Label_achtung: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Splitter3: TSplitter;
    Label1: TLabel;
    ListBox1: TListBox;
    Label2: TLabel;
    Label3: TLabel;
    ListBox2: TListBox;
    bitbtn_esc: TBitBtn;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure CheckBox_neuClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure ini(l1,l2:tstringlist);
  end;

var
  Form_imp_ausnahmen: TForm_imp_ausnahmen;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_imp_ausnahmen.FormCreate(Sender: TObject);
begin
     form_main.form_positionieren(tform(sender));
     

end;

procedure tform_imp_ausnahmen.ini(l1,l2:tstringlist);
begin
listbox1.items:=l1;
listbox2.items:=l2;
end;

procedure TForm_imp_ausnahmen.CheckBox_neuClick(Sender: TObject);
begin
if CheckBox_neu.Checked then
begin
	   bitbtn_ok.Enabled:=false;
     bitbtn_esc.kind:=bkretry;
     bitbtn_esc.caption:='&Weiter';
     label4.Visible:=false;
     label_achtung.Visible:=false;
end
else
begin
      bitbtn_ok.Enabled:=true;
      bitbtn_esc.kind:=bkno;
      label4.Visible:=true;
      label_achtung.Visible:=true;
end;
end;

procedure TForm_imp_ausnahmen.ListBox1Click(Sender: TObject);
begin
	memo1.Text:=listbox1.Items.strings[listbox1.itemindex];
end;

procedure TForm_imp_ausnahmen.ListBox2Click(Sender: TObject);
begin
   memo2.Text:=listbox2.Items.strings[listbox2.itemindex];
end;

procedure TForm_imp_ausnahmen.FormShow(Sender: TObject);
begin
//checkbox_neu.Checked:=true;
end;

end.
