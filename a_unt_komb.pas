unit a_unt_komb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, dbgridEXT, StdCtrls, ExtCtrls, Buttons, ComCtrls, ToolWin,a_data,
  Mask ;

type
  TForm_Unt_komb = class(TForm)
    Panel_main: TPanel;
    ToolBar: TToolBar;
    ToolButton_neu: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_edit: TToolButton;
    ToolButton4: TToolButton;
    ToolButton_save: TToolButton;
    ToolButton9: TToolButton;
    ToolButton_del: TToolButton;
    ToolButton6: TToolButton;
    ToolButton_quit: TToolButton;
    Panel_ok: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox_child: TGroupBox;
    DBgridEXT_child: TDBgridEXT;
    GroupBox_main: TGroupBox;
    DBgridEXT_main: TDBgridEXT;
    Notebook: TNotebook;
    RadioGroup: TRadioGroup;
    GroupBox_planen: TGroupBox;
    SpeedButton1: TSpeedButton;
    Datum: TLabel;
    untersucher: TLabel;
    MaskEdit: TMaskEdit;
    ComboBox_untersucher: TComboBox;
    procedure ToolButton_neuClick(Sender: TObject);
    procedure RadioGroupClick(Sender: TObject);
    procedure ToolButton_editClick(Sender: TObject);
    procedure ToolButton_delClick(Sender: TObject);
    procedure ToolButton_quitClick(Sender: TObject);
    procedure DBgridEXT_mainEnter(Sender: TObject);
    procedure DBgridEXT_mainExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton_saveClick(Sender: TObject);
    procedure DBgridEXT_childEnter(Sender: TObject);
    procedure DBgridEXT_childExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    bereich:integer;
  end;

var
  Form_Unt_komb: TForm_Unt_komb;

implementation

uses a_line_edit, a_main, a_listwahl, a_kalender;

{$R *.DFM}

procedure TForm_Unt_komb.ToolButton_neuClick(Sender: TObject);
begin

	  if  activecontrol=dbgridext_main then
			begin
			form_line_edit:=tform_line_edit.create(self);
			form_line_edit.CheckBox.Visible:=false;

			if form_line_edit.showmodal=mrok then
			  neuer_datensatz(datamodul.q_unt_komb_main,[null,form_line_edit.edit.text,bereich]);
			  datamodul.sql_post(datamodul.q_unt_komb_main);
			form_line_edit.release;
			end
	  else
			begin
			form_liste:=tform_liste.create(self);
			datamodul.sql_new(datamodul.q_3,'select * from typ where bereich= '+inttostr(bereich), 'reihenfolge');
			if form_liste.showmodal=mrok then
				neuer_datensatz(datamodul.q_unt_komb_child,[null,datamodul.q_unt_komb_main['nummer'], datamodul.q_3['nummer']]);
			datamodul.sql_post(datamodul.q_unt_komb_child);
			form_liste.release;

			end;
end;

procedure TForm_Unt_komb.RadioGroupClick(Sender: TObject);
begin
bereich:=radiogroup.ItemIndex+1;
datamodul.sql(datamodul.q_unt_komb_main, 'select * from unt_komb_main where uil ='+inttostr(bereich),'');
dbgridext_main.SetFocus;
end;

procedure TForm_Unt_komb.ToolButton_editClick(Sender: TObject);
begin
	if  activecontrol=dbgridext_main then
			begin
			form_line_edit:=tform_line_edit.create(self);
			form_line_edit.CheckBox.Visible:=false;
        form_line_edit.edit.text:=datamodul.q_unt_komb_main['name'];

			if form_line_edit.showmodal=mrok then
			begin
				datamodul.q_unt_komb_main.edit;
				datamodul.q_unt_komb_main['name']:=form_line_edit.edit.text;
				datamodul.sql_post(datamodul.q_unt_komb_main);
			end;
        form_line_edit.release;
        end
end;

procedure TForm_Unt_komb.ToolButton_delClick(Sender: TObject);
begin
	  if  activecontrol=dbgridext_main then
			begin
			dateiloeschen(datamodul.q_unt_komb_main,false);
        end
    else
        begin
        dateiloeschen(datamodul.q_unt_komb_child,false);
			end;
end;

procedure TForm_Unt_komb.ToolButton_quitClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_Unt_komb.DBgridEXT_mainEnter(Sender: TObject);
begin
tdbgridext(sender).color:=clinfobk;
end;

procedure TForm_Unt_komb.DBgridEXT_mainExit(Sender: TObject);
begin
	tdbgridext(sender).color:=clbtnface;

end;

procedure TForm_Unt_komb.FormCreate(Sender: TObject);
begin
	form_main.form_positionieren(tform(sender));
	grid_suchen(tform(sender));
	form_main.combolesen(ComboBox_untersucher,datamodul.q_untersucher,'untersucher','');

end;

procedure TForm_Unt_komb.ToolButton_saveClick(Sender: TObject);
begin
    if  activecontrol=dbgridext_main then
		  datamodul.sql_post(datamodul.q_unt_komb_main)
	  else
			datamodul.sql_post(datamodul.q_unt_komb_child);

end;

procedure TForm_Unt_komb.DBgridEXT_childEnter(Sender: TObject);
begin
	tdbgridext(sender).color:=clinfobk;
	toolbutton_edit.Enabled:=false;
end;

procedure TForm_Unt_komb.DBgridEXT_childExit(Sender: TObject);
begin
	tdbgridext(sender).color:=clbtnface;
	toolbutton_edit.Enabled:=true;
end;

procedure TForm_Unt_komb.FormShow(Sender: TObject);
begin
	//RadioGroupClick(Sender);
	datamodul.sql(datamodul.q_unt_komb_main, 'select * from unt_komb_main where uil ='+inttostr(bereich),'');
	dbgridext_main.SetFocus;
	dbgridext_child.color:=clbtnface;
	if notebook.PageIndex=1 then maskedit.SetFocus;
end;

procedure TForm_Unt_komb.SpeedButton1Click(Sender: TObject);
begin
if form_kalender.showmodal =mrok then
	begin
	 maskedit.Text:=datetostr(form_kalender.auswahlkalender.Date);
	end;
end;
														  
end.
