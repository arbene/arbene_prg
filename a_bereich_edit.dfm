�
 TFORM_BEREICH 0;  TPF0TForm_bereichForm_bereichLeftTopYHelpContext BorderIconsbiSystemMenu BorderStylebsDialogCaptionBereichs-VorgabenClientHeight�ClientWidthDColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBox	GroupBox1Left TopWidth� Height�AlignalLeftTabOrder  TLabelLabel1LeftTop WidthHHeightCaptionBereichs-Name  TEdit	Edit_nameLeftTop0Width� HeightTabOrder   TRadioGroupRGLeftTop}Width� HeightAlignalBottomCaptionErscheinungsbild	ItemIndex Items.Stringsnur AnmerkungenZahlenwert  und AnmerkungenAuswahlliste  und Anmerkungen
AudiogrammSehtest	Blutdruck TabOrderOnClickRGClick   TPanelPanel2Left� TopWidthkHeight�AlignalClient
BevelOuterbvNoneTabOrder 	TGroupBoxGroupBox_memoLeftTop� Width7Height� AlignalClientCaptionMaske f�r AnmerkungenTabOrder  	TRichEditRichEdit_bereichLeftTopWidth3Height� AlignalClientFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier
Font.Style Lines.Strings  
ParentFontTabOrder WantTabs	   	TNotebookNotebookLeft Top WidthkHeight� AlignalTopCtl3D		PageIndexParentCtl3DTabOrder TPage Left Top Captionleer  TPage Left Top Captionzahl TLabelLabel2Left Top0Width5HeightCaption
Bezeichner  TLabelEinheitLeft TopxWidth HeightCaptionEinheit  TEditEdit_bezLeft Top@Width� HeightTabOrder TextEdit_bez  TEditEdit_bez_dimLeft Top� Width� HeightTabOrderTextEdit_bez_dim   TPage Left Top Captionliste TSpeedButtonSpeedButton1TagLeftTTop� WidthHeight>
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphs  TSpeedButtonSpeedButton2TagLeftTTop� WidthHeightA
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphs  
TDBgridEXTDBgridEXT_berLeft!Top Width)Height� AlignalClientColorclInfoBk
DataSourceDataModul.ds_berlistDefaultDrawing	OptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Zeitdiff_L_B darf_ziehenColumnsExpanded	FieldName
ListenfeldWidth`Visible	    TPanelPanel3Left Top� WidthkHeight0AlignalBottom
BevelOuterbvNoneTabOrder TSpeedButtonSpeedButton_upTagLeft� TopWidth(Height
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� <333�33t7��333�<�33�34D7w?337w3��3�7DG3s�?33��ÓDDC3s�sw3s3<�̔DDs37�7w37�3<�̔DD337?337333�̔DG33333333�̔DC333s�3s333<̔Ds3337�7�333<̔D33337?733333̔G3333333333̔C33333ss33333<�s3333377�33333<�333333773333333�33333333333333�3333333s333	NumGlyphsOnClickSpeedButton_upClick  TSpeedButtonSpeedButton_downTagLeft(TopWidth(Height
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333933333337�3333339s3333337�333333�C333333s333333�G333333ss�3333<�D333337���3333<�Ds333377�?3333��DC33337�3333��DG3333s7�s�33<��DD3337�7�7�33<��DDs33737�7?33���DDC3337�333���DDG33s?ws�s�<��94DD37�w7�s��<�393tDs7�37�77���3933DCws37�3w�339337Cs337333s	NumGlyphsOnClickSpeedButton_downClick  TBitBtnBitBtn3LeftTopWidthAHeightCaption
Hinzuf�genTabOrder OnClickBitBtn3Click  TBitBtnBitBtn_veraendernLeftXTopWidthAHeightCaption	Ver�ndernTabOrderOnClickBitBtn_veraendernClick  TBitBtnBitBtn4Left� TopWidthKHeightCaptionL�schenTabOrderOnClickBitBtn4Click   TPanelPanel4Left Top Width!Height� AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel5LeftJTop Width!Height� AlignalRight
BevelOuterbvNoneTabOrder   TPage Left Top Caption
Audiogramm Taudiogrammaudiogramm1Left Top WidthkHeight� AlignalClientCaptionaudiogramm1TabOrder    TPage Left Top CaptionSehtest TLabelLabel3LefthTophWidthsHeightCaptionFormular f�r den Sehtest   TPage Left Top CaptionBludruck TLabelLabel4LefthToppWidthdHeightCaptionFormular f�r Bludruck    TPanelPanel6Left Top� WidthHeight� AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel7LeftPTop� WidthHeight� AlignalRight
BevelOuterbvNoneTabOrder   TToolBarToolBar1Left Top WidthDHeightCaptionToolBar1ImagesForm_main.ImageList_mainTabOrder TToolButtonToolButton9Left TopWidthCaptionToolButton9
ImageIndexStyletbsSeparator  TToolButtonToolButton_cancelLeftTopHintSchlie�en ohne zu speichernCaptionToolButton_cancel
ImageIndexParentShowHintShowHint	OnClickToolButton_cancelClick  TToolButtonToolButton6LeftTopWidth"CaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonToolButton_quitLeftATopHintSchlie�en und speichernCaptionToolButton_quit
ImageIndexParentShowHintShowHint	OnClickToolButton_quitClick   TPanelPanel1Left Top�WidthDHeightAlignalBottom
BevelOuterbvNoneTabOrder   