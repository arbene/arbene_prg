unit a_unistall;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,registry,shellapi,
	StdCtrls, Buttons,ShlObj;

type
	TForm_main = class(TForm)
	  Button1: TButton;
	  Memo1: TMemo;
	  BitBtn1: TBitBtn;
	  CheckBox2: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ComboBox: TComboBox;
    Label5: TLabel;
	  procedure Button1Click(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
	  procedure BitBtn1Click(Sender: TObject);
	private
	  { Private-Deklarationen }
	  betriebssystem:word;
	  befehl,parameter:string;
    function GetSpecialFolderPath(Folder: Integer; CanCreate: Boolean): string;
	  procedure winrun(pfad, parameter: string);
	  function wurzelpfad(pfad: string): string;
	  function stringwenden(wort: string): string;
	public
    { Public-Deklarationen }
  end;

var
  Form_main: TForm_main;

implementation

{$R *.DFM}

procedure TForm_main.Button1Click(Sender: TObject);
var
pfad,cmd_path,befehl,parameter,uninst,dat:string;
t:ttime;
reg:tregistry;
position:integer;

begin


if checkbox2.checked then
begin
    screen.Cursor:=crhourglass;


    befehl:='net.exe';
    parameter:='stop mysql';

	  shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_hide);
	  memo1.Lines.Add('Mysql wurde angehalten');

	  t:=time();
	  while t+0.0001 >time() do;


      if combobox.ItemIndex=0 then
          befehl:='c:\mysql\bin\mysqld-nt.exe'
        else
          befehl:='c:\mariadb55\bin\mysqld.exe';

        parameter:='--remove';
        shellexecute(form_main.handle,'open',pchar(befehl),pchar(parameter),nil,SW_hide);
        memo1.Lines.Add('Der Dienst Mysql wurde entfernt');
      memo1.Lines.Add('');


 
end;

       dat:=GetSpecialFolderPath(28,false)+'\arbene';  //user
   dat:=dat+'\'+application.title+'.ini';

   if  fileexists(dat) then
   begin
      deletefile(dat);
      if fileexists(dat) then showmessage ('Die Datei konnte nicht gel�scht werden: '+dat);
   end;
   dat:=GetSpecialFolderPath(35,false)+'\arbene';   //common
   dat:=dat+'\'+application.title+'.ini';
   if fileexists(dat) then
   begin
     deletefile(dat);
     if fileexists(dat) then showmessage ('Die Datei konnte nicht gel�scht werden: '+dat);
   end;

    pfad:=wurzelpfad(application.ExeName);
     memo1.Lines.Add('Bitte l�schen Sie die Programmdateien im Verzeichnis '+pfad+' von Hand' );
	  screen.Cursor:=crdefault;


end;


function Tform_main.GetSpecialFolderPath(Folder: Integer; CanCreate: Boolean): string;

// Gets path of special system folders
//
// Call this routine as follows:
// GetSpecialFolderPath (CSIDL_PERSONAL, false)
//        returns folder as result
//
var
   FilePath: array [0..255] of char;

begin
 SHGetSpecialFolderPath(0, @FilePath[0], FOLDER, CanCreate);
 Result := FilePath;
end;

procedure Tform_main.winrun(pfad, parameter: string);
var

 erzeugt: boolean;
 datei,command :pchar;
 startinfo: tstartupinfo;
 processinfo: tprocessinformation;
begin

	datei:= pchar(pfad);
	command:=pchar(parameter);
	//if (processinfo.hProcess=0) then //processinfo:=runexe(datei,nil);
	begin
	startinfo.cb:=68;
	startinfo.lpReserved:=nil;
	startinfo.lpDesktop:=nil;
	startinfo.lpTitle:=nil;
	startinfo.dwX:=0;
	startinfo.dwY:=0;
	startinfo.dwXSize:=0;
	startinfo.dwYsize:=0;
	startinfo.dwXCountChars:=0;
	startinfo.dwYCountChars:=0;
	startinfo.dwFillAttribute:=0;
	startinfo.dwFlags:=129;
	startinfo.wShowWindow:=9;
	startinfo.cbReserved2:=0;
	startinfo.lpReserved2:=nil;
	startinfo.hStdInput:=0;
	startinfo.hStdOutput:=0;
	startinfo.hStdError:=0;
	try
	erzeugt:=createprocess(datei,command,nil,nil,false,create_default_error_mode,nil,nil,startinfo,processinfo);
	if not erzeugt then processinfo.hProcess:=0;
	except
	 processinfo.hProcess:=0;
	end;

  end;
end;


procedure TForm_main.FormCreate(Sender: TObject);
var
	versionsinfo: OSVERSIONINFO;
begin
	Top:=(screen.height div 2)-(Height div 2);
	Left:= (screen.width div 2) - (width div 2);
	versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;

end;

procedure TForm_main.BitBtn1Click(Sender: TObject);
begin
application.Terminate;
end;


function tform_main.wurzelpfad(pfad: string): string;
var
back: integer;
begin
pfad:=copy(pfad, 1, length(pfad)-1);
pfad:=stringwenden(pfad);
back:=pos('\', pfad);
pfad:=copy(pfad, back+1, length(pfad));
result:=stringwenden(pfad);
if  pos('\', result)=0 then result:=result+'\';
end;


function tform_main.stringwenden(wort: string): string;
var
 laenge, i: integer;
begin
laenge:=length(wort);
result:='';
for i:=laenge downto 1 do
begin
	result:=result+copy(wort,i,1);
end;
end;


end.
