’
 TFORM_BACKUP 0³  TPF0TForm_backupForm_backupLeftlTopč Width4HeightQHelpContext8CaptionDatensicherungColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TPanelPanel1Left TopßWidth$HeightKAlignalBottom
BevelOuterbvNoneTabOrder  TBitBtn	BitBtn_okLeftHTopWidthį HeightCaptionBeendenDefault	ModalResultParentShowHintShowHint	TabOrderVisible
Glyph.Data
ā  Ž  BMŽ      v   (   $            h                                   ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 333333333333333333  333333333333ó33333  334C3333333833333  33B$33333338ó3333  34""C333338333333  3B""$33333338ó333  4"*""C3338ó8ó3333  2"£¢"C3338ó3333  :*3:"$3338ų38ó8ó33  3£33¢"C33333333  3333:"$3333338ó8ó3  33333¢"C33333333  33333:"$3333338ó8ó  333333¢"C3333333  333333:"C3333338ó  3333333¢#3333333  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn_abbruchLeftHTopWidthį HeightCaption	AbbrechenTabOrder OnClickBitBtn_abbruchClick
Glyph.Data
ā  Ž  BMŽ      v   (   $            h                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 333333333333333333  3333333333?333333  39333333ó33?33  3939338ó3?ó3  39338ó8óų33  33338ó338ó  3393333833ų3  33333338ó33?3  33313333333833  3339333338ó333  3333333383333  339333333333  33333838ó8ó3  3339333333  33933333ų38ó8ó  3333339333833’  33333333333333383  333333333333333333  	NumGlyphs  TProgressBarProgressBar1LeftHTop2Width¹HeightTabOrder   TPanelPanel2Left Top Width$Height” AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel16LeftGTop"WidthaHeightCaption   Pfad fĆ¼r das Backup  TEdit	Edit_pfadLeftHTop.Width©HeightReadOnly	TabOrder Text	Edit_pfad  TBitBtnBitBtn_tempLeft Top,WidthqHeightCaptionDatei AuswahlTabOrderOnClickBitBtn_tempClick
Glyph.Data
z  v  BMv      v   (                                                     æææ   ’  ’   ’’ ’   ’ ’ ’’  ’’’ 033333337ó333333033333337ó?’’?ó’03   07’www7s0 »3337w333303°3337ós÷s33303033337ó7s3333033333337ó?’’?ó’03   07’www7s0 »3337w333303°3337ós÷s33303033337ó7s3333033333337’’?ó’ó3   0 33www7w33»33333333333°33333s÷s3333303333337s333333	NumGlyphs  TBitBtnBitBtn_backupschreibenLeftHToptWidthŃ HeightCaptionDatensicherung schreibenTabOrderOnClickBitBtn_backupschreibenClick  	TCheckBoxCheckBox_endeLeftHTopPWidthHeightCaption+Nach der Datensicherung automatisch beendenChecked	State	cbCheckedTabOrder   TPanelPanel3Left Top” WidthIHeight>AlignalLeft
BevelOuterbvNoneTabOrder  TPanelPanel4LeftĶTop” WidthWHeight>AlignalRight
BevelOuterbvNoneTabOrder  TMemoMemoLeftITop” WidthHeight>AlignalClient
ScrollBars
ssVerticalTabOrder  TTimer
Timer_endeEnabledInterval NOnTimerBitBtn_abbruchClickLeftTop  TTimerTimer_progressEnabledInterval2OnTimerTimer_progressTimerLeftpTop   