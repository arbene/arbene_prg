unit zip;

interface
uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls,Zip32,UnZip32,math;//,excels;


		procedure ZipFiles(FileName : string; FileList: TStrings;mmemo:tmemo);
		procedure unzipfile(zipfile,unzipdir:string;var mmemo:tmemo);

		procedure SetDummyInitFunctions(var Z:TZipUserFunctions);
		procedure Set_ZipDllOptions;
		procedure unzip_Set_UserFunctions(var Z:TUserFunctions);
		procedure Set_UnZipOptions(var Opt: TDCL;zipfile, zipdir:string);

//end;
	 //zip

	 function DummyPrint(Buffer: PChar; Size: ULONG): integer; stdcall ;
	 function DummyPassword(P: PChar; N: Integer; M, Name: PChar): integer; stdcall ;
	 function DummyComment(Buffer: PChar): PChar; stdcall ;
	 function DummyService(Buffer: PChar; Size: ULONG): integer; stdcall;
	 //unzip
	function DllPrnt(Buffer: PChar; Size: ULONG): integer; stdcall;
	function DllPassword(P: PChar; N: Integer; M, Name: PChar): integer; stdcall;
	function DllService(CurFile: PChar; Size: ULONG): integer; stdcall;
	function DllReplace(FileName: PChar): integer; stdcall;
	procedure DllMessage(UnCompSize : ULONG;
							 CompSize   : ULONG;
							 Factor     : UINT;
							 Month      : UINT;
							 Day        : UINT;
							 Year       : UINT;
							 Hour       : UINT;
							 Minute     : UINT;
							 C          : Char;
							 FileName   : PChar;
							 MethBuf    : PChar;
							 CRC        : ULONG;
							 Crypt      : Char); stdcall;

var
memo:tmemo;
implementation


//*******************************************************************************************
//zip
//*******************************************************************************************

procedure ZipFiles(FileName : string; FileList: TStrings;mmemo:tmemo);
var
  i        : integer;
  ZipRec   : TZCL;
	ZUF      : TZipUserFunctions;
  FNVStart : PCharArray;
begin

	{ precaution }
	if Trim(FileName) = '' then Exit;
	if FileList.Count <= 0 then Exit;
	memo:=mmemo;
	if not IsExpectedZipDllVersion then exit;
	Set_ZipDllOptions;

	{ initialize the dll with dummy functions }
	SetDummyInitFunctions(ZUF);

	{ number of files to zip }
  ZipRec.argc := FileList.Count;

  { name of zip file - allocate room for null terminated string  }
  GetMem(ZipRec.lpszZipFN, Length(FileName) + 1 );
  ZipRec.lpszZipFN := StrPCopy( ZipRec.lpszZipFN, FileName);
  try
    { dynamic array allocation }
    GetMem(ZipRec.FNV, ZipRec.argc * SizeOf(PChar));
    FNVStart := ZipRec.FNV;
	  try
      { copy the file names from FileList to ZipRec.FNV dynamic array }
		 for i := 0 to ZipRec.argc - 1 do
      begin
        GetMem(ZipRec.FNV^[i], Length(FileList[i]) + 1 );
        StrPCopy(ZipRec.FNV^[i], FileList[i]);
      end;
      try
        { send the data to the dll }
        ZpArchive(ZipRec);
      finally
        { release the memory for the file list }
        ZipRec.FNV := FNVStart;
        for i := (ZipRec.argc - 1) downto 0 do
          FreeMem(ZipRec.FNV^[i], Length(FileList[i]) + 1 );
      end;

    finally
      { release the memory for the ZipRec.FNV dynamic array }
      ZipRec.FNV := FNVStart;
      FreeMem(ZipRec.FNV, ZipRec.argc * SizeOf(PChar));
    end;
  finally
	  { release the memory for the FileName }
    FreeMem(ZipRec.lpszZipFN, Length(FileName) + 1 );
	end;

end;

procedure SetDummyInitFunctions(var Z:TZipUserFunctions);
begin
  { prepare ZipUserFunctions structure }
  with Z do
  begin
    @Print     := @DummyPrint;
    @Comment   := @DummyComment;
    @Password  := @DummyPassword;
    @Service   := @DummyService;
  end;
  { send it to dll }
  ZpInit(Z);
end;


function DummyPrint(Buffer: PChar; Size: ULONG): integer;
begin
	Memo.Lines.Add(Buffer);
  Result := Size;
end;
{----------------------------------------------------------------------------------}
function DummyPassword(P: PChar; N: Integer; M, Name: PChar): integer;
begin
  Result := 1;
end;
{----------------------------------------------------------------------------------}
function DummyComment(Buffer: PChar): PChar;
begin
  Result := Buffer;
end;
{----------------------------------------------------------------------------------}
function DummyService(Buffer: PChar; Size: ULONG): integer;
begin
  //frmMain.Memo.Lines.Add('> '+StrPas(Buffer));
  Result := 0;
end;


procedure Set_ZipDllOptions;
var
	ZipOptions : TZPOpt;
begin
(*
  ZipOptions.Date           := PChar(edtDate.Text);
  ZipOptions.szRootDir      := PChar(edtRootDir.Text);
  ZipOptions.szTempDir      := PChar(edtTempDir.Text);
*)
  ZipOptions.Date           := nil;
  ZipOptions.szRootDir      := nil;
  ZipOptions.szTempDir      := nil;
  ZipOptions.fSuffix        := false;
  ZipOptions.fEncrypt       := false;
  ZipOptions.fSystem        := false;
  ZipOptions.fVolume        := false;
  ZipOptions.fExtra         := false;
	ZipOptions.fNoDirEntries  := true;
  ZipOptions.fExcludeDate   := false;
  ZipOptions.fIncludeDate   := false;
  ZipOptions.fVerbose       := false;
  ZipOptions.fQuiet         := false;
  ZipOptions.fCRLF_LF       := false;
  ZipOptions.fLF_CRLF       := false;
  ZipOptions.fJunkDir       := false;
	ZipOptions.fGrow          := false;
  ZipOptions.fForce         := false;
	ZipOptions.fMove          := false;
  ZipOptions.fDeleteEntries := false;
  ZipOptions.fUpdate        := false;
  ZipOptions.fFreshen       := false;
  ZipOptions.fJunkSFX       := false;
  ZipOptions.fLatestTime    := false;
  ZipOptions.fComment       := false;
  ZipOptions.fOffsets       := false;
	ZipOptions.fPrivilege     := false;
  ZipOptions.fEncryption    := false;
  ZipOptions.fRecurse       := 0;
  ZipOptions.fRepair        := 0;
  ZipOptions.fLevel := '6';

  { send the options to the dll }
  if not ZpSetOptions(ZipOptions) then ShowMessage('Error setting Zip Options')
end;


procedure unzipfile(zipfile,unzipdir:string;var mmemo:tmemo);
var
	UF   : TUserFunctions;
  Opt     : TDCL;
begin
  memo:=mmemo;
	if not IsExpectedUnZipDllVersion then exit;
	if Trim(zipfile) = '' then Exit;

  { set user functions }
  unzip_Set_UserFunctions(UF);

  { set unzip operation options }
  Set_UnZipOptions(Opt,zipfile,unzipdir);


  Wiz_SingleEntryUnzip(0,    { number of file names being passed }
                       nil,  { file names to be unarchived }
                       0,    { number of "file names to be excluded from processing" being  passed }
                       nil,  { file names to be excluded from the unarchiving process }
                       Opt,  { pointer to a structure with the flags for setting the  various options }
                       UF);  { pointer to a structure that contains pointers to user functions }

end;

procedure Set_UnZipOptions(var Opt: TDCL;zipfile, zipdir:string);
begin
	with Opt do
  begin
    ExtractOnlyNewer  := Integer(false);
    SpaceToUnderscore := Integer(false);
    PromptToOverwrite := Integer(false);
    fQuiet            := 0;
    nCFlag            := Integer(false);
    nTFlag            := Integer(false);
    nVFlag            := Integer(false);
    nUFlag            := Integer(false);
    nZFlag            := Integer(false);
    nDFlag            := Integer(false);
    nOFlag            := Integer(true);
    nAFlag            := Integer(false);
    nZIFlag           := Integer(false);
    C_flag            := Integer(false);
	  fPrivilege        := 1;
    lpszExtractDir    := PChar(zipdir);
    lpszZipFN         := PChar(zipfile);
  end;
end;

procedure unzip_Set_UserFunctions(var Z:TUserFunctions);
begin
  { prepare TUserFunctions structure }
  with Z do
  begin
    @Print                  := @DllPrnt;
    @Sound                  := nil;
    @Replace                := @DllReplace;
    @Password               := @DllPassword;
    @SendApplicationMessage := @DllMessage;
    @ServCallBk             := @DllService;
  end;      
end;


function DllPrnt(Buffer: PChar; Size: ULONG): integer;
begin
  memo.Lines.Add(Buffer);
  Result := Size;
end;
{----------------------------------------------------------------------------------}
function DllPassword(P: PChar; N: Integer; M, Name: PChar): integer;
begin
  Result := 1;
end;
{----------------------------------------------------------------------------------}
function DllService(CurFile: PChar; Size: ULONG): integer;
begin
  Result := 0;
end;
{----------------------------------------------------------------------------------}
function DllReplace(FileName: PChar): integer;
begin
  Result := 1;
end;
{----------------------------------------------------------------------------------}
procedure DllMessage(UnCompSize : ULONG;
                     CompSize   : ULONG;
                     Factor     : UINT;
                     Month      : UINT;
                     Day        : UINT;
                     Year       : UINT;
                     Hour       : UINT;
                     Minute     : UINT;
                     C          : Char;
							 FileName   : PChar;
                     MethBuf    : PChar;
							 CRC        : ULONG;
							 Crypt      : Char);
const
  sFormat = '%7u  %7u %4s  %02u-%02u-%02u  %02u:%02u  %s%s';
  cFactor = '%s%d%%';
  cFactor100 = '100%%';
var
  S       : string;
  sFactor : string;
  Sign    : Char;
begin

  if (CompSize > UnCompSize) then Sign := '-' else Sign := ' ';

  if (Factor = 100)
	then sFactor := cFactor100
	else sFactor := Format(cFactor, [Sign, Factor]);

	//S := Format(sFormat, [UnCompSize, CompSize, sFactor, Month, Day, Year, Hour, Minute, C, FileName]);

  memo.Lines.add(filename);
end;

end.
