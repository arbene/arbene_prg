unit arb_inst_first_form_main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,registry, ExtCtrls;

type
  TForm_main = class(TForm)
    BitBtn1: TBitBtn;
    Memo: TMemo;
    Timer: TTimer;
    procedure FormCreate(Sender: TObject);

    procedure TimerTimer(Sender: TObject);
  private
    { Private-Deklarationen }
      Betriebssystem:integer;
	    command,dt:string;
      procedure pruefe_arbeneexe;
      procedure pruefe_arbenedb;
  public
    { Public-Deklarationen }
  end;

var
  Form_main: TForm_main;

implementation

{$R *.dfm}

procedure TForm_main.FormCreate(Sender: TObject);
var
	versionsinfo: OSVERSIONINFO;
begin
	Top:=(screen.height div 2)-(Height div 2);
	Left:= (screen.width div 2) - (width div 2);

	versionsinfo.dwOSVersionInfoSize:=sizeof(versionsinfo);
	getversionex(versionsinfo);
	Betriebssystem:=versionsinfo.dwPlatformId;

  dt:=datetimetostr(now());
  dt:=stringreplace(dt,' ','_',[rfReplaceAll	]);
  dt:=stringreplace(dt,'.','_',[rfReplaceAll	]);
  dt:=stringreplace(dt,':','_',[rfReplaceAll	]);
  dt:='_'+copy(dt,7,5)+copy(dt,4,3)+copy(dt,1,3)+copy(dt,11,20);
  pruefe_arbeneexe;
  pruefe_arbenedb;

end;

procedure tForm_main.pruefe_arbeneexe;
var
pfad:string;
reg:tregistry;
 f : file;

begin
try

  reg := Tregistry.create;
//key="HKCU\Software\klawitter\arbene" param="version" value="%version%"/>
//<reg key="HKCU\Software\klawitter\arbene" param="path" value="%InstallPath%"/>
		 reg.RootKey:=hkey_current_user;
     if not reg.KeyExists('software\klawitter\arbene\') then
     begin

      timer.Enabled:=true;
      exit;
     end;

		 reg.openkey('software\klawitter\arbene\',false);
		 pfad:=reg.readstring('path');

     if pfad='' then
     begin
      timer.Enabled:=true;
      exit;
     end;

     if not fileexists(pfad+'\arbene.exe')  then
     begin
      //reg.Free;
      timer.Enabled:=true;
      exit;
     end;

		 memo.Lines.Add('bisheriger Installationspfad: '+pfad);
     memo.Lines.Add('');
     if MessageDlg('Sollen die folgenden Dateien umbenannt werden? '+#13+
                    pfad+'\arbene.exe'+#13+
                    pfad+'\change_db.txt', mtconfirmation, [mbyes,mbno], 0)=mrno then
                    begin
                      //reg.Free;
                      timer.Enabled:=true;
                      exit;
                   end;
    try
      AssignFile(f, pfad+'\arbene.exe');
      rename( f,pfad+'\arbene'+dt+'.exe');
      memo.Lines.Add('Arbene.exe wurde in ' +pfad+'\arbene'+dt+'.exe'+' umbenannt');
    except
      memo.Lines.Add('Arbene.exe konnte nicht umbenannt werden - bitte manuell l�schen');
    end;

    try
      AssignFile(f, pfad+'\change_db.txt');
      rename( f,pfad+'\change_db'+dt+'.txt');
      memo.Lines.Add('change_db.txt wurde in ' +pfad+'\change_db'+dt+'.txt'+' umbenannt');
    except
      memo.Lines.Add('change_db.txt konnte nicht umbenannt werden - bitte manuell l�schen');
    end;



finally
		 reg.Free;
end;

end;


procedure tForm_main.pruefe_arbenedb;
begin
//if fileexissts(c:\mariadb55\bin

end;



procedure TForm_main.TimerTimer(Sender: TObject);
begin
  modalresult:=mrok;
end;

end.
procedure TForm_main.BitBtn1Click(Sender: TObject);
begin

end;


